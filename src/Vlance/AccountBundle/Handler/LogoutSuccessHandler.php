<?php
namespace Vlance\AccountBundle\Handler;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
 
class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    
    public function onLogoutSuccess(Request $request) 
    {
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    
}
