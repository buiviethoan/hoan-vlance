<?php
namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Vlance\JobBundle\Entity\Message;
use Vlance\JobBundle\Entity\Job;
use Vlance\PaymentBundle\Entity\CreditAccount;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\AccountRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Account extends BaseUser 
{
    const USER_VLANCE_SYS = 1;
    
    const TYPE_FREELANCER = 'freelancer';
    const TYPE_CLIENT = 'client';
    const TYPE_VLANCE_ADMIN = 'vlance_admin';
    const TYPE_VLANCE_SUPER_ADMIN = 'vlance_superadmin';
    const TYPE_VLANCE_SYSTEM = 'vlance_system';
    
    const TYPE_VIP_MINI     = 0;
    const TYPE_VIP_NORMAL   = 1;
    
    const LEVEL_BEGINNER = 'beginner';
    const LEVEL_PROFESSIONAL = 'professional';
    const LEVEL_EXPERT = 'expert';
    
    const ROLE_VLANCE_SYSTEM = 'ROLE_VLANCE_SYSTEM';
    const ROLE_VLANCE_SUPER_ADMIN = 'ROLE_VLANCE_SUPER_ADMIN';
    const ROLE_VLANCE_ADMIN = 'ROLE_VLANCE_ADMIN';
    const ROLE_VLANCE_USER = 'ROLE_VLANCE_USER';
    
    const CERTIFICATED = true;
    const CAN_NOTIFY = 0;
    const WILL_NOTIFY = 1;
    const NOTIFIED = 2;
    const CAN_NOT_NOTIFY = 3;
    const REQUESTED = true;
    const NOT_SHOW_NEW_FEATURE = 0;
    const SHOWED_NEW_FEATURE = 1;
    const HAVE_NEW_FEATURE = 2;
    const WILL_NOTIFY_NEW_FEATURE = 3;
    
    const WORK_RHYTHM_PARTTIME = 0;
    const WORK_RHYTHM_FULLTIME = 1;
    
    const WORK_AVAILIBILITY_NO  = FALSE;
    const WORK_AVAILIBILITY_YES = TRUE;
    
    /**
     * Define additional data field
     */
    const ANTOREE_SCORE = 'antoree_score';
    const ANTOREE_URL = 'antoree_url';
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="full_name", type="string", length=255)
     */
    protected $fullName;

    /**
     * @var \DateTime
     * @Assert\Date()
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    protected $birthday = null;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title = null;
    
    /**
     * 
     * @Assert\Image(maxSize ="1024k")
     */
    protected $file = null;
    
    /**
     * @var string
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path;
    
    /**
    * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\PersonId", mappedBy="account")
    */
    protected $personId;
    
    /**
    * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\TaxId", mappedBy="account")
    */
    protected $taxId;
    
    /**
     * @var decimal
     * @ORM\Column(name="score", type="decimal", scale=2, options={"default" : "0.00"})
     */
    protected $score = 0.00;
    
    /**
     * @var integer
     * @ORM\Column(name="num_job", type="integer", options={"default" : "0"})
     */
    protected $numJob = 0;
    
    /**
     * @var integer
     * @ORM\Column(name="num_job_deposit", type="integer", options={"default" : "0"})
     */
    protected $numJobDeposit = 0;
    
     /**
     * @var integer
     * @ORM\Column(name="num_job_finish", type="integer", options={"default" : "0"})
     */
    protected $numJobFinish = 0;
    
    /**
     * TODO Will make obsolet
     * @var integer
     * @ORM\Column(name="num_bid", type="integer", options={"default" : "0"})
     */
    protected $numBid = 0;
    
    /**
     * @var integer
     * @ORM\Column(name="num_reviews", type="integer", options={"default" : "0"})
     */
    protected $numReviews = 0;
    
   /**
     * Number bids current
     * @var integer
     * @ORM\Column(name="num_curr_bids", type="integer", options={"default" : "15"})
     */
    protected $numCurrBids = 15;
    
    /**
     * Number bid need reset (/mounths) 
     * @var integer
     * @ORM\Column(name="monthly_bids", type="integer", options={"default" : "2"})
     */
    protected $monthlyBids = 2;
    
     /**
     * @var integer
     * @ORM\Column(name="in_come", type="integer", options={"default" : "0"})
     */
    protected $inCome = 0;
    
    /**
     * @var integer
     * @ORM\Column(name="out_come", type="integer", options={"default" : "0"})
     */
    protected $outCome = 0;
    
    /**
     * @var string
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('freelancer', 'client', 'vlance_admin', 'vlance_super_admin', 'vlance_system')", nullable=true)
     */
    protected $type = self::TYPE_FREELANCER;
    
    /**
     * @var string
     * @ORM\Column(name="option_view", type="string", columnDefinition="ENUM('freelancer', 'client')", nullable=true)
     */
    protected $option_view;
    
    /**
     * @var string
     * @ORM\Column(name="level", type="string", columnDefinition="ENUM('beginner', 'professional', 'expert')", nullable=true)
     */
    protected $level = null;
    
    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;

    /**
     * @var string
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address = null;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\BaseBundle\Entity\City", inversedBy="accounts")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $city = null;

    /**
     * @var integer
     * @ORM\Column(name="district", type="integer", nullable=true)
     */
    protected $district = null;
    
    /**
     * @var string
     * @ORM\Column(name="telephone", type="string", length=15, nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9\-\+]{8,15}$/",
     *     message="account.form.telephone.valid_phone"
     * )
     * @Assert\Length(
     *      min = "8",
     *      max = "15",
     *      minMessage = "account.form.telephone.min",
     *      maxMessage = "account.form.telephone.max"
     * )
     */
    protected $telephone = null;
    
    /**
     * @var string
     * @ORM\Column(name="skype", type="string", length=50, nullable=true)
     */
    protected $skype = null;
    
    /**
     * @var string
     * @ORM\Column(name="id_card_number", type="string", length=15, nullable=true)
     */
    protected $idCardNumber = null;
    
    /**
     * @var string
     * @ORM\Column(name="google_id", type="string", length=50, nullable=true)
     */
    protected $googleId = null;
    
    /**
     * @var string
     * @ORM\Column(name="access_token_google", type="text", nullable=true)
     */
    protected $accessTokenGG = null;
   
    /**
     * @var string
     * @ORM\Column(name="facebook_id", type="string", length=20, nullable=true)
     */
    protected $facebookId = null;
    
    /**
     * @var string
     * @ORM\Column(name="linkedin_id", type="string", length=20, nullable=true)
     */
    protected $linkedinId = null;
    
    /**
     * @var string
     * @ORM\Column(name="access_token_link", type="text", nullable=true)
     */
    protected $accessTokenLink = null;
    
    /**
     * @var string
     * @ORM\Column(name="access_token_face", type="text", nullable=true)
     */
    protected $accessTokenFace = null;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="telephone_verified_at", type="datetime", nullable=true)
     */
    protected $telephoneVerifiedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="accounts")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $category;

    /**
     * @var string
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    protected $website = null;
    
    /**
     * @ORM\OneToOne(targetEntity="Vlance\PaymentBundle\Entity\UserCash", mappedBy="account")
     */
    protected $cash;
    
    /**
     * @ORM\OneToOne(targetEntity="Vlance\PaymentBundle\Entity\CreditAccount", mappedBy="account")
     */
    protected $credit;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="account")
     */
    protected $jobs;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\JobActivity", mappedBy="editor")
     */
    protected $jobActivities;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="worker")
     */
    protected $jobsWorked;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Bid", mappedBy="account")
     */
    protected $bids;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Message", mappedBy="receiver")
     */
    protected $messagesReceiver;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\WorkshopBundle\Entity\FeedBack", mappedBy="receiver")
     */
    protected $feedBacksReceiver;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\Portfolio", mappedBy="account")
     */
    protected $portfolios;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\JobInvite", mappedBy="account")
     */
    protected $invites;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\AccountTelephone", mappedBy="account")
     */
    protected $telephones;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Skill", inversedBy="accounts")
     * @ORM\JoinTable(name="skill_account",
     * joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $skills;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Service", inversedBy="accounts")
     * @ORM\JoinTable(name="service_account",
     * joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $services;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\PaymentBundle\Entity\UserPaymentMethod", mappedBy="account")
     */
    protected $paymentMethods;
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"fullName"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @var boolean
     * @ORM\Column(name="certificated", type="boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    protected $isCertificated = false;
    
    /**
     * @var int
     * @ORM\Column(name="notified", type="integer", options={"default":"3"})
     */
    protected $notify = 3;
    
    /**
     * @var int
     * @ORM\Column(name="notification_job", type="integer", options={"default":"1"})
     */
    protected $notificationJob = 1;
    
    /**
     * @var boolean
     * @ORM\Column(name="requested", type="boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    protected $isRequested = false;
    
    /**
     * @var boolean
     * @ORM\Column(name="work_availability", type="boolean", options={"default":true})
     * @Assert\Type(type="boolean")
     */
    protected $workAvailability = self::WORK_AVAILIBILITY_YES;
    
    /**
     * @var int
     * @ORM\Column(name="work_rhythm", type="integer", options={"default":"0"})
     */
    protected $workRhythm = self::WORK_RHYTHM_PARTTIME;
    
    /**
     * @var \DateTime
     * @Assert\Date()
     * @ORM\Column(name="last_request", type="datetime", nullable=true)
     */
    protected $lastRequest = null;
    
    /**
     * @var \DateTime
     * @Assert\Date()
     * @ORM\Column(name="date_request", type="datetime", nullable=true)
     */
    protected $dateRequest = null;
    
    /**
     * This user show new feature or not
     * Default is 1 - showed
     * @var integer
     * @ORM\Column(name="new_feature", type="integer", options={"default":"1"})
     */
    protected $newFeature = 1;
    
    /**
     * This user show feature email invite or not
     * Default is 1 - showed
     * @var integer
     * @ORM\Column(name="feature_invite", type="integer", options={"default":"1"})
     */
    protected $featureInvite = 1;
    
    /**
     * @var \DateTime
     * @Assert\Date()
     * @ORM\Column(name="banned_to", type="datetime", nullable=true)
     */
    protected $bannedTo = null;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="email_verified", type="boolean", options={"default":"1"})
     * @Assert\Type(type="boolean")
     */
    protected $emailVerified = true;
    
    /**
     * @var string
     * @ORM\Column(name="additional_data", type="text", nullable=true)
     */
    protected $additionalData;
    
    protected $addtionalDataArray;
    
    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(name="featured_until", type="datetime", nullable=true)
     */
    private $featuredUntil;
    
    /**
     * @var boolean
     * @ORM\Column(name="provider", type="boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    protected $isProvider = false;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\AccountSetting", mappedBy="account")
     */
    protected $accountSettings;

    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\BidRate", mappedBy="sender")
     */
    protected $bidRates;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="expired_date_vip", type="datetime", nullable=true)
     */
    protected $expiredDateVip;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="accounts")
     * @ORM\JoinColumn(name="category_id_vip", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $categoryVIP;
    
    /**
     * @var integer
     * @ORM\Column(name="type_vip", type="integer", nullable=true)
     */
    protected $typeVIP = null;
    
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\AccountAdminJob", mappedBy="account")
     */
    protected $admin_jobs;
    
    /**
     * @var integer
     * @ORM\Column(name="extra_bid", type="integer", options={"default":"0"})
     */
    protected $extraBid;

    public function setExtraBid($extraBid){
	$this->extraBid = $extraBid;
	
	return $this;
    }
    
    public function getExtraBid(){
	return $this->extraBid;
    }

        /**
     * Set fullName
     *
     * @param string $fullName
     * @return Account
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Get fullName but show only the first letter of name
     *
     * @return string 
     */
    public function getFullNameHidden()
    {
        $wordlist = mb_split("\s", $this->fullName);
        $fullNameHidden = "";
        end($wordlist);
        $last_key = key($wordlist);
        foreach ($wordlist as $key => $value){
            if($last_key != $key){
                $fullNameHidden .= mb_substr(mb_convert_case($value, MB_CASE_TITLE, "UTF-8"), 0, 1,'UTF8') . ". ";
            } else {
                $fullNameHidden .= mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
            }
        }
        return $fullNameHidden;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getHiddenFullName()
    {
        $full_name = explode(" ", $this->fullName);
        end($full_name);
        $last_key = key($full_name);
        foreach($full_name as $k => $word){
            if($k != $last_key){
                $full_name[$k] = $word[0] . "."; // Bị lỗi với ký tự Đ biến thành ?
            }
        }
        return mb_convert_case(implode(" ", $full_name), MB_CASE_TITLE, "UTF-8");
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Account
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    
        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Account
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return Account
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Account
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Account
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set district
     *
     * @param \stdClass $district
     * @return Account
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    
        return $this;
    }

    /**
     * Get district
     *
     * @return \stdClass 
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set idCardNumber
     *
     * @param string $idCardNumber
     * @return Account
     */
    public function setIdCardNumber($idCardNumber)
    {
        $this->idCardNumber = $idCardNumber;
    
        return $this;
    }

    /**
     * Get idCardNumber
     *
     * @return string 
     */
    public function getIdCardNumber()
    {
        return $this->idCardNumber;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Account
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    
        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set category
     *
     * @param \Vlance\AccountBundle\Entity\Category $category
     * @return Account
     */
    public function setCategory(\Vlance\AccountBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Vlance\AccountBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->transactionsSent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->transactionsReceipt = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bids = new \Doctrine\Common\Collections\ArrayCollection();
//        $this->file = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projectsOwned = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projectsWorked = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workshopsOwned = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workshopsWorked = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addtionalDataArray = array();
    }
    
    /**
     * Set cash
     *
     * @param \Vlance\PaymentBundle\Entity\UserCash $cash
     * @return Account
     */
    public function setCash(\Vlance\PaymentBundle\Entity\UserCash $cash = null)
    {
        $this->cash = $cash;
    
        return $this;
    }

    /**
     * Get cash
     *
     * @return \Vlance\PaymentBundle\Entity\UserCash 
     */
    public function getCash()
    {
        return $this->cash;
    }

    /**
     * Add transactionsSent
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactionsSent
     * @return Account
     */
    public function addTransactionsSent(\Vlance\PaymentBundle\Entity\Transaction $transactionsSent)
    {
        $this->transactionsSent[] = $transactionsSent;
    
        return $this;
    }

    /**
     * Remove transactionsSent
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactionsSent
     */
    public function removeTransactionsSent(\Vlance\PaymentBundle\Entity\Transaction $transactionsSent)
    {
        $this->transactionsSent->removeElement($transactionsSent);
    }

    /**
     * Get transactionsSent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransactionsSent()
    {
        return $this->transactionsSent;
    }

    /**
     * Add transactionsReceipt
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactionsReceipt
     * @return Account
     */
    public function addTransactionsReceipt(\Vlance\PaymentBundle\Entity\Transaction $transactionsReceipt)
    {
        $this->transactionsReceipt[] = $transactionsReceipt;
    
        return $this;
    }

    /**
     * Remove transactionsReceipt
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactionsReceipt
     */
    public function removeTransactionsReceipt(\Vlance\PaymentBundle\Entity\Transaction $transactionsReceipt)
    {
        $this->transactionsReceipt->removeElement($transactionsReceipt);
    }

    /**
     * Get transactionsReceipt
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransactionsReceipt()
    {
        return $this->transactionsReceipt;
    }

    /**
     * Add jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     * @return Account
     */
    public function addJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    
    /**
     * Get jobs publish (posted) and not violated
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobsPublish()
    {
        $jobs = $this->getJobs()->filter(
                    function($entry) {
                        if($entry->getPublish() == true && $entry->getIsViolated() == false) {
                            return true;
                        }
                            return false;
                    }
                );
        
        return $jobs;
    }
    
    /**
     * Get jobs open for bid
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobsOpenForBid()
    {
        $jobs = $this->getJobsPublish()->filter(
                /* @var $entry /Vlance/JobBundle/Entity/Job */
                function($entry) {
                    if ($entry->getCloseAt()->getTimestamp() > time() && $entry->getWorker() == null ) {
                        return true;
                    }
                    return false;
                }
        );
        return $jobs;
    }
    
    /**
     * Get jobs which successfully hired freelancer, so: funded & done, with or without feedback
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobsHired()
    {
        $jobs = $this->getJobsPublish()->filter(
                /* @var $entry /Vlance/JobBundle/Entity/Job */
                function($entry) {
                    if (!is_null($entry->getWorker())
                            && $entry->getPaymentStatus() == Job::PAIED
                            && $entry->getStatus() == Job::JOB_FINISHED) {
                        return true;
                    }
                    return false;
                }
        );
        return $jobs;
    }
    
    /**
     * Get jobs publish and not hired
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobsNotHired()
    {
        $jobs = $this->getJobs()->filter(
            function($entry) {
                if($entry->getPublish() == true 
                        && $entry->getIsViolated() == false
                        && $entry->getPaymentStatus() !== Job::PAIED 
                        && $entry->getStatus() !== Job::JOB_FINISHED) {
                    return true;
                }
                    return false;
            }
        );
        
        return $jobs;
    }

    /**
     * Add workshopsOwned
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsOwned
     * @return Account
     */
    public function addWorkshopsOwned(\Vlance\WorkshopBundle\Entity\Workshop $workshopsOwned)
    {
        $this->workshopsOwned[] = $workshopsOwned;
        return $this;
    }

    /**
     * Remove workshopsOwned
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsOwned
     */
    public function removeWorkshopsOwned(\Vlance\WorkshopBundle\Entity\Workshop $workshopsOwned)
    {
        $this->workshopsOwned->removeElement($workshopsOwned);
    }

    /**
     * Get workshopsOwned
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopsOwned()
    {
        return $this->workshopsOwned;
    }

    /**
     * Add workshopsWorked
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsWorked
     * @return Account
     */
    public function addWorkshopsWorked(\Vlance\WorkshopBundle\Entity\Workshop $workshopsWorked)
    {
        $this->workshopsWorked[] = $workshopsWorked;
        return $this;
    }

    /**
     * Remove workshopsWorked
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsWorked
     */
    public function removeWorkshopsWorked(\Vlance\WorkshopBundle\Entity\Workshop $workshopsWorked)
    {
        $this->workshopsWorked->removeElement($workshopsWorked);
    }

    /**
     * Get workshopsWorked
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWorkshopsWorked()
    {
        return $this->workshopsWorked;
    }

    /**
     * Add messagesReceiver
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesReceiver
     * @return Account
     */
    public function addMessageReceiver(\Vlance\JobBundle\Entity\Message $messagesReceiver)
    {
        $this->messagesReceiver[] = $messagesReceiver;
    
        return $this;
    }

    /**
     * Remove messagesReceiver
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesReceiver
     */
    public function removeMessage(\Vlance\JobBundle\Entity\Message $messagesReceiver)
    {
        $this->messagesReceiver->removeElement($messagesReceiver);
    }

    /**
     * Get messagesReceiver
     *
     * @return \Vlance\JobBundle\Entity\Message
     */
    public function getMessagesReceiver()
    {
        return $this->messagesReceiver;
    }

    /**
     * Get bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * Add bids
     *
     * @param \Vlance\JobBundle\Entity\Bid $bids
     * @return Account
     */
    public function addBid(\Vlance\JobBundle\Entity\Bid $bids)
    {
        $this->bids[] = $bids;
    
        return $this;
    }

    /**
     * Remove bids
     *
     * @param \Vlance\JobBundle\Entity\Bid $bids
     */
    public function removeBid(\Vlance\JobBundle\Entity\Bid $bids)
    {
        $this->bids->removeElement($bids);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * check expiresAt
     * @return boolean
     */
    public function isExpiresAt()
    {
        return $this->expiresAt;
    }
    
    public function isCredentialsExpireAt() {
        return $this->credentialsExpireAt;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Account
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Account
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set city
     *
     * @param \Vlance\BaseBundle\Entity\City $city
     * @return Account
     */
    public function setCity(\Vlance\BaseBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \Vlance\BaseBundle\Entity\City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set numJob
     *
     * @param integer $numJob
     * @return Account
     */
    public function setNumJob($numJob)
    {
        $this->numJob = $numJob;
    
        return $this;
    }

    /**
     * Get numJob
     *
     * @return integer 
     */
    public function getNumJob()
    {
        return $this->numJob;
    }
    
    /**
     * Get numJobDeposit
     *
     * @return integer 
     */
    public function getNumJobDeposit() {
        return $this->numJobDeposit;
    }
    
    /**
     * Set numJobDeposit
     *
     * @param integer $numJobDeposit
     * @return Account
     */
    public function setNumJobDeposit($numJobDeposit) {
        $this->numJobDeposit = $numJobDeposit;
    }

    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Account
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Account
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Set telephoneVerifiedAt
     *
     * @param \DateTime $telephoneVerifiedAt
     * @return Account
     */
    public function setTelephoneVerifiedAt($telephoneVerifiedAt)
    {
        $this->telephoneVerifiedAt = $telephoneVerifiedAt;
    
        return $this;
    }

    /**
     * Get telephoneVerifiedAt
     *
     * @return \DateTime 
     */
    public function getTelephoneVerifiedAt()
    {
        return $this->telephoneVerifiedAt;
    }

    /**
     * Set inCome
     *
     * @param integer $inCome
     * @return Account
     */
    public function setInCome($inCome)
    {
        $this->inCome = $inCome;
    
        return $this;
    }

    /**
     * Get inCome
     *
     * @return integer 
     */
    public function getInCome()
    {
        return $this->inCome;
    }

    /**
     * Set numJobFinish
     *
     * @param integer $numJobFinish
     * @return Account
     */
    public function setNumJobFinish($numJobFinish)
    {
        $this->numJobFinish = $numJobFinish;
    
        return $this;
    }

    /**
     * Get numJobFinish
     *
     * @return integer 
     */
    public function getNumJobFinish()
    {
        return $this->numJobFinish;
    }

    /**
     * Set numBid
     *
     * @param integer $numBid
     * @return Account
     */
    public function setNumBid($numBid)
    {
        $this->numBid = $numBid;
    
        return $this;
    }

    /**
     * Get numBid
     *
     * @return integer 
     */
    public function getNumBid()
    {
        return $this->numBid;
    }
    
    /**
     * Set testimonial
     *
     * @param \Vlance\CmsBundle\Entity\Testimonial $testimonial
     * @return Account
     */
    public function setTestimonial(\Vlance\CmsBundle\Entity\Testimonial $testimonial = null)
    {
        $this->testimonial = $testimonial;
    
        return $this;
    }

    /**
     * Get testimonial
     *
     * @return \Vlance\CmsBundle\Entity\Testimonial 
     */
    public function getTestimonial()
    {
        return $this->testimonial;
    }
    
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setVlanceUserRole() {
        if ($this->getType() !== self::TYPE_VLANCE_SYSTEM) {
            $this->addRole(self::ROLE_VLANCE_USER);
        }
    }

    /**
     * Add jobsWorked
     *
     * @param \Vlance\JobBundle\Entity\Job $jobsWorked
     * @return Account
     */
    public function addJobsWorked(\Vlance\JobBundle\Entity\Job $jobsWorked)
    {
        $this->jobsWorked[] = $jobsWorked;
    
        return $this;
    }

    /**
     * Remove jobsWorked
     *
     * @param \Vlance\JobBundle\Entity\Job $jobsWorked
     */
    public function removeJobsWorked(\Vlance\JobBundle\Entity\Job $jobsWorked)
    {
        $this->jobsWorked->removeElement($jobsWorked);
    }

    /**
     * Get jobsWorked
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobsWorked()
    {
        return $this->jobsWorked;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     * @return Account
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Account
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Get full path
     *
     * @return string 
     */
    public function getFullPath()
    {
        return $this->getUploadDir() . DS . $this->path;
    }

    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR . "/account";
    }
    
   /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL. "/account";
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if($this->getFile() !== null) {
            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->getClientOriginalExtension();
        }
    }
 
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        if (!file_exists($this->getUploadDir())) {
            mkdir($this->getUploadDir(), 0777, true);
        }
        $this->getFile()->move($this->getUploadDir(), $this->path);
        $this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if($this->path === null) {
            return;
        }
        $file = $this->path;
        if(file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * Set outCome
     *
     * @param integer $outCome
     * @return Account
     */
    public function setOutCome($outCome)
    {
        $this->outCome = $outCome;
    
        return $this;
    }

    /**
     * Get outCome
     *
     * @return integer 
     */
    public function getOutCome()
    {
        return $this->outCome;
    }


    /**
     * Add portfolios
     *
     * @param \Vlance\AccountBundle\Entity\Portfolio $portfolios
     * @return Account
     */
    public function addPortfolio(\Vlance\AccountBundle\Entity\Portfolio $portfolios)
    {
        $this->portfolios[] = $portfolios;
    
        return $this;
    }

    /**
     * Remove portfolios
     *
     * @param \Vlance\AccountBundle\Entity\Portfolio $portfolios
     */
    public function removePortfolio(\Vlance\AccountBundle\Entity\Portfolio $portfolios)
    {
        $this->portfolios->removeElement($portfolios);
    }

    /**
     * Get portfolios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPortfolios()
    {
        return $this->portfolios;
    }
    
    /**
     * Get telephones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTelephones()
    {
        return $this->telephones;
    }
    
    public function getNumberMessages() {
        $messages = $this->getMessagesReceiver();
        $number_message = 0;
        foreach($messages as $message) {
            if($message->getStatus() == Message::NEW_MESSAGE) {
                $number_message ++;
            }
        }
        return $number_message;
    }

    /**
     * Add messagesSender
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesSender
     * @return Account
     */
    public function addMessagesSender(\Vlance\JobBundle\Entity\Message $messagesSender)
    {
        $this->messagesSender[] = $messagesSender;
    
        return $this;
    }

    /**
     * Remove messagesSender
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesSender
     */
    public function removeMessagesSender(\Vlance\JobBundle\Entity\Message $messagesSender)
    {
        $this->messagesSender->removeElement($messagesSender);
    }

    /**
     * Add messagesReceiver
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesReceiver
     * @return Account
     */
    public function addMessagesReceiver(\Vlance\JobBundle\Entity\Message $messagesReceiver)
    {
        $this->messagesReceiver[] = $messagesReceiver;
    
        return $this;
    }

    /**
     * Remove messagesReceiver
     *
     * @param \Vlance\JobBundle\Entity\Message $messagesReceiver
     */
    public function removeMessagesReceiver(\Vlance\JobBundle\Entity\Message $messagesReceiver)
    {
        $this->messagesReceiver->removeElement($messagesReceiver);
    }

    /**
     * Add skills
     *
     * @param \Vlance\AccountBundle\Entity\Skill $skills
     * @return Account
     */
    public function addSkill(\Vlance\AccountBundle\Entity\Skill $skills)
    {
        $this->skills[] = $skills;
    
        return $this;
    }

    /**
     * Remove skills
     *
     * @param \Vlance\AccountBundle\Entity\Skill $skills
     */
    public function removeSkill(\Vlance\AccountBundle\Entity\Skill $skills)
    {
        $this->skills->removeElement($skills);
    }

    /**
     * Get skills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return Account
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    
        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set numReviews
     *
     * @param integer $numReviews
     * @return Account
     */
    public function setNumReviews($numReviews)
    {
        $this->numReviews = $numReviews;
    
        return $this;
    }

    /**
     * Get numReviews
     *
     * @return integer 
     */
    public function getNumReviews()
    {
        return $this->numReviews;
    }

    /**
     * Add paymentMethods
     *
     * @param \Vlance\PaymentBundle\Entity\UserPaymentMethod $paymentMethods
     * @return Account
     */
    public function addPaymentMethod(\Vlance\PaymentBundle\Entity\UserPaymentMethod $paymentMethods)
    {
        $this->paymentMethods[] = $paymentMethods;
    
        return $this;
    }

    /**
     * Remove paymentMethods
     *
     * @param \Vlance\PaymentBundle\Entity\UserPaymentMethod $paymentMethods
     */
    public function removePaymentMethod(\Vlance\PaymentBundle\Entity\UserPaymentMethod $paymentMethods)
    {
        $this->paymentMethods->removeElement($paymentMethods);
    }

    /**
     * Get paymentMethods
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPaymentMethods()
    {
        return $this->paymentMethods;
    }
    
    /**
     * Check if account is type of freelancer
     */
    public function isTypeFreelancer(){
        return ($this->getType() == 'freelancer');
    }
    
    /**
     * Check if account is type of Client
     */
    public function isTypeClient(){
        return ($this->getType() == 'client');
    }

    /**
     * Add feedBacksSender
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksSender
     * @return Account
     */
    public function addFeedBacksSender(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksSender)
    {
        $this->feedBacksSender[] = $feedBacksSender;
    
        return $this;
    }

    /**
     * Remove feedBacksSender
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksSender
     */
    public function removeFeedBacksSender(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksSender)
    {
        $this->feedBacksSender->removeElement($feedBacksSender);
    }

    /**
     * Get feedBacksSender
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeedBacksSender()
    {
        return $this->feedBacksSender;
    }

    /**
     * Add feedBacksReceiver
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksReceiver
     * @return Account
     */
    public function addFeedBacksReceiver(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksReceiver)
    {
        $this->feedBacksReceiver[] = $feedBacksReceiver;
    
        return $this;
    }

    /**
     * Remove feedBacksReceiver
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksReceiver
     */
    public function removeFeedBacksReceiver(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksReceiver)
    {
        $this->feedBacksReceiver->removeElement($feedBacksReceiver);
    }

    /**
     * Get feedBacksReceiver
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeedBacksReceiver()
    {
        return $this->feedBacksReceiver;
    }

    /**
     * Set isCertificated
     *
     * @param boolean $isCertificated
     * @return Account
     */
    public function setIsCertificated($isCertificated)
    {
        $this->isCertificated = $isCertificated;
    
        return $this;
    }

    /**
     * Get isCertificated
     *
     * @return boolean 
     */
    public function getIsCertificated()
    {
        return $this->isCertificated;
    }

    /**
     * Set notify
     *
     * @param integer $notify
     * @return Account
     */
    public function setNotify($notify)
    {
        $this->notify = $notify;
    
        return $this;
    }

    /**
     * Get notify
     *
     * @return integer 
     */
    public function getNotify()
    {
        return $this->notify;
    }
    
     /**
     * Set notificationJob
     *
     * @param integer $notificationJob
     * @return Account
     */
    public function setNotificationJob($notificationJob)
    {
        $this->notificationJob = $notificationJob;
    
        return $this;
    }

    /**
     * Get notificationJob
     *
     * @return integer 
     */
    public function getNotificationJob()
    {
        return $this->notificationJob;
    }

    /**
     * Set isRequested
     *
     * @param boolean $isRequested
     * @return Account
     */
    public function setIsRequested($isRequested)
    {
        $this->isRequested = $isRequested;
    
        return $this;
    }

    /**
     * Get isRequested
     *
     * @return boolean 
     */
    public function getIsRequested()
    {
        return $this->isRequested;
    }
    
    /**
     * Get is valid to make a bid
     * @return boolean
     */
    public function getIsVerifiedPersonId()
    {
        if(!is_object($this->getPersonId())){
            return false;
        }
        return $this->getPersonId()->getIsVerified();
    }
    
    /**
     * Get is valid to make a bid
     * @return boolean
     */
    public function getIsRequireVerifyId()
    {
        if($this->hasRole(self::ROLE_VLANCE_ADMIN) || $this->hasRole(self::ROLE_VLANCE_SUPER_ADMIN) || $this->hasRole(self::ROLE_VLANCE_SYSTEM)){
            return false;
        }
        
        $now = new \DateTime("now");
        $milestone = array(
            'm1'    => array(
                'start'     => new \DateTime("2016-07-11 14:00:00"),
                'user_from' => new \DateTime("2016-04-01 00:00:00"),
            ),
            'm2'    => array(
                'start'     => new \DateTime("2016-07-18 00:00:00"),
                'user_from' => new \DateTime("2016-01-01 00:00:00"),
            ),
            'm3'    => array(
                'start'     => new \DateTime("2016-07-25 00:00:00"),
                'user_from' => new \DateTime("2015-01-01 00:00:00"),
            ),
            'm4'    => array(
                'start'     => new \DateTime("2016-08-01 00:00:00"),
                'user_from' => new \DateTime("2013-12-01 00:00:00"),
            )
        );
        if($now >= $milestone['m4']['start']){
            if($this->getCreatedAt() >= $milestone['m4']['user_from']){
                return true;
            }
        }
        elseif($now >= $milestone['m3']['start']){
            if($this->getCreatedAt() >= $milestone['m3']['user_from']){
                return true;
            }
        }
        elseif($now >= $milestone['m2']['start']){
            if($this->getCreatedAt() >= $milestone['m2']['user_from']){
                return true;
            }
        }
        elseif($now >= $milestone['m1']['start']){
            if($this->getCreatedAt() >= $milestone['m1']['user_from']){
                return true;
            }
        }
        return false;
    }

    /**
     * Set lastRequest
     *
     * @param \DateTime $lastRequest
     * @return Account
     */
    public function setLastRequest($lastRequest)
    {
        $this->lastRequest = $lastRequest;
    
        return $this;
    }

    /**
     * Get lastRequest
     *
     * @return \DateTime 
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }
    
    /**
     * Set dateRequest
     *
     * @param \DateTime $dateRequest
     * @return Account
     */
    public function setDateRequest($dateRequest)
    {
        $this->dateRequest = $dateRequest;
    
        return $this;
    }

    /**
     * Get dateRequest
     *
     * @return \DateTime 
     */
    public function getDateRequest()
    {
        return $this->dateRequest;
    }
    
    /**
     * Set newFeature
     *
     * @param integer $newFeature
     * @return Account
     */
    public function setNewFeature($newFeature)
    {
        $this->newFeature = $newFeature;
    
        return $this;
    }

    /**
     * Get newFeature
     *
     * @return integer 
     */
    public function getNewFeature()
    {
        return $this->newFeature;
    }
    
    /**
     * Get newFeature
     *
     * @return integer 
     */
    public function getFeatureInvite() {
        return $this->featureInvite;
    }
    
    /**
     * Set featureInvite
     *
     * @param integer $featureInvite
     * @return Account
     */
    public function setFeatureInvite($featureInvite) {
        $this->featureInvite = $featureInvite;
    }

        
    public function getFieldText($field) {
        $fieldsText = array(
            'Category' => 'account.fieldtext.category',
            'City' => 'account.fieldtext.city',
            'Email' => 'account.fieldtext.email',
            'FullName' => 'account.fieldtext.fullname',
            'Title' => 'account.fieldtext.title',
            'Birthday' => 'account.fieldtext.birthday',
            'Description' => 'account.fieldtext.description',
            'Address' => 'account.fieldtext.address',
            'Path' => 'account.fieldtext.path',
            'Telephone' => 'account.fieldtext.telephone',
        );
        return $fieldsText[$field];
    }

    /**
     * Set score
     *
     * @param float $score
     * @return Account
     */
    public function setScore($score)
    {
        $this->score = $score;
    
        return $this;
    }

    /**
     * Get score
     *
     * @return float 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set bannedTo
     *
     * @param \DateTime $bannedTo
     * @return Account
     */
    public function setBannedTo($bannedTo)
    {
        $this->bannedTo = $bannedTo;
    
        return $this;
    }

    /**
     * Get bannedTo
     *
     * @return \DateTime 
     */
    public function getBannedTo()
    {
        return $this->bannedTo;
    }
    
    /**
     * Check if an account is banned at the moment
     * 
     * @return boolean
     */
    public function isBanned() {
        // dont use timestamp since on 32 bits system, the date is overflow at 2038-01-19.
        $now = new \DateTime("NOW");
        return ($this->getBannedTo() && $this->getBannedTo() > $now);
    }

    /**
     * Set emailVerified
     *
     * @param boolean $emailVerified
     * @return Account
     */
    public function setEmailVerified($emailVerified)
    {
        $this->emailVerified = $emailVerified;
    
        return $this;
    }

    /**
     * Get emailVerified
     *
     * @return boolean 
     */
    public function getEmailVerified()
    {
        return $this->emailVerified;
    }
    
    /**
     * Set accessTokenFace
     *
     * @param string $accessTokenFace
     * @return Account
     */
    public function setAccessTokenFace($accessTokenFace)
    {
        $this->accessTokenFace = $accessTokenFace;
    
        return $this;
    }

    /**
     * Get accessTokenFace
     *
     * @return string 
     */
    public function getAccessTokenFace()
    {
        return $this->accessTokenFace;
    }
    /**
     * Set linkedinId
     *
     * @param string $linkedinId
     * @return Account
     */
    public function setLinkedinId($linkedinId)
    {
        $this->linkedinId = $linkedinId;
    
        return $this;
}

    /**
     * Get LinkedinId
     *
     * @return string 
     */
    public function getLinkedinId()
    {
        return $this->linkedinId;
    }
    /**
     * Set accessTokenLink
     *
     * @param string $accessTokenLink
     * @return Account
     */
    public function setAccessTokenLink($accessTokenLink)
    {
        $this->accessTokenLink = $accessTokenLink;
    
        return $this;
    }

    /**
     * Get accessTokenLink
     *
     * @return string 
     */
    public function getAccessTokenLink()
    {
        return $this->accessTokenLink;
    }

    public function getGoogleId() {
        return $this->googleId;
    }

    public function getAccessTokenGG() {
        return $this->accessTokenGG;
    }

    public function setGoogleId($googleId) {
        $this->googleId = $googleId;
    }

    public function setAccessTokenGG($accessTokenGG) {
        $this->accessTokenGG = $accessTokenGG;
    }
        
    /**
     * Get numCurrBids
     *
     * @return int
     */
    public function getNumCurrBids() {
        return $this->numCurrBids;
    }

    /**
     * Get monthlyBids
     *
     * @return int 
     */
    public function getMonthlyBids() {
        return $this->monthlyBids;
    }

    /**
     * Set numCurrBids
     *
     * @param int $numCurrBids
     * @return Account
     */
    public function setNumCurrBids($numCurrBids) {
        $this->numCurrBids = $numCurrBids;
    }

    /**
     * Set monthlyBids
     *
     * @param int $monthlyBids
     * @return Account
     */
    public function setMonthlyBids($monthlyBids) {
        $this->monthlyBids = $monthlyBids;
    }
    
    /**
     * Get missing information of account
     *
     * @return array
     */
    public function getMissingInfo()
    {
        $miss_info = array();
        if (is_null($this->getPath()))        { $miss_info[] = 'avatar'; }
        if (is_null($this->getType()))        { $miss_info[] = 'type'; }
        if (is_null($this->getCategory()))    { $miss_info[] = 'category'; }
        if (is_null($this->getTitle()))       { $miss_info[] = 'title'; }
        if (is_null($this->getCity()))        { $miss_info[] = 'city'; }
        if (is_null($this->getTelephone()))   { $miss_info[] = 'telephone'; }
        if (is_null($this->getBirthday()))    { $miss_info[] = 'birthday'; }
        //if (is_null($this->getAddress()))     { $miss_info[] = 'address'; }
        if ($this->getFullName()== "")        { $miss_info[] = 'name'; }
        if ($this->getEmail()== "")           { $miss_info[] = 'email'; }
        if (is_null($this->getDescription())) { $miss_info[] = 'description'; }
        //if (is_null($this->getIdCardNumber())){ $miss_info[] = 'idcardnumber'; }
//        if (count($this->getSkills()) < 2)   { $miss_info[] = 'skills'; }
        return $miss_info;
    }
    
    /**
     * Get missing verification information of account
     *
     * @return array
     */
    public function getMissingVerifyInfo()
    {
        $miss_info = array();
        if (is_null($this->getPath()))          { $miss_info[] = 'avatar'; }
        if (is_null($this->getType()))          { $miss_info[] = 'type'; }
        if (is_null($this->getCategory()))      { $miss_info[] = 'category'; }
        if (is_null($this->getTitle()))         { $miss_info[] = 'title'; }
        if (is_null($this->getCity()))          { $miss_info[] = 'city'; }
        if (is_null($this->getTelephone()))     { $miss_info[] = 'telephone'; }
        if (is_null($this->getDescription()))   { $miss_info[] = 'description'; }
        if (is_null($this->getWebsite()))       { $miss_info[] = 'website'; }
//        if (count($this->getSkills()) < 2)      { $miss_info[] = 'skills'; }
        if (count($this->getPortfolios()) < 3)  { $miss_info[] = 'portfolios'; }
        return $miss_info;
    }
    
    /**
     * Get verification success percent of account
     *
     * @return array
     */
    public function getVerifyInfoPercent()
    {
        $miss_info = $this->getMissingVerifyInfo();
        return (1-count($miss_info)/8)*100;
    }
    
    /**
     * Get missing verification information of Client
     *
     * @return array
     */
    public function getMissingClientInfo()
    {
        $miss_info = array();
        if (is_null($this->getPath()))          { $miss_info[] = 'avatar'; }
        if (is_null($this->getCategory()))      { $miss_info[] = 'category'; }
        if (is_null($this->getTitle()))         { $miss_info[] = 'title'; }
        if (is_null($this->getCity()))          { $miss_info[] = 'city'; }
        if (is_null($this->getTelephone()))     { $miss_info[] = 'telephone'; }
        if (is_null($this->getDescription()))   { $miss_info[] = 'description'; }
        return $miss_info;
    }
    
    /**
     * Get is missing verification information of Client
     *
     * @return boolean
     */
    public function getIsMissingClientInfo()
    {
        if(count($this->getMissingClientInfo()) > 0){
            return true;
        } else{
            return false;
        }
    }
    
    /**
     * Check if this account is admin
     * @return boolean
     */
    public function isAdmin()
    {
        if($this->getId() < 100)
            return true;
        else
            return false;
    }
 
    
    /**
     * Set additionalData
     *
     * @param mixed $additionalData json string or array of data
     * @return Category
     */
    public function setAdditionalData($additionalData)
    {
        if (is_array($additionalData)) {
            $this->addtionalDataArray = $additionalData;
            $this->additionalData = json_encode($additionalData);
        } else {
            $this->additionalData = $additionalData;
            $this->addtionalDataArray = json_decode($additionalData);
        }
        return $this;
    }

    /**
     * 
     * @param string $field field name or * to get all additional data, empty to get json text
     * @return mixed
     */
    public function getAdditionalData($field = "")
    {
        if (((is_null($this->addtionalDataArray)) || empty($this->addtionalDataArray)) && $this->additionalData) {
            $this->addtionalDataArray = json_decode($this->additionalData);
        }
        if ($field) {
            if ($field != "*") {
                if (isset($this->addtionalDataArray->{$field})) {
                    return $this->addtionalDataArray->{$field};
                } else {
                    return "";
                }
            } else {
                return $this->addtionalDataArray;
            }
        } else {
            return $this->additionalData;
        }
    }
    
    /**
     * Add a field to additional data
     * 
     * @param string $field
     * @param type $value
     * @return Category
     */
    public function addAdditionalData($field, $value) {
        $this->addtionalDataArray[$field] = $value;
        $this->additionalData = json_encode($this->addtionalDataArray);
        return $this;
    }
    
    /**
     * 
     * @param mixed $fields array of field to remove or field name
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function removeAdditionalData($fields) {
        if (is_array($fields)) {
            foreach ($fields as $field) {
                if (is_string($field) && isset($this->addtionalDataArray->{$field})) {
                    unset($this->addtionalDataArray->{$field});
                }
            }
        } else if (is_string($field) && isset($this->addtionalDataArray->{$field})) {
            unset($this->addtionalDataArray->{$field});
        }
        return $this;
    }
    
    /**
     * Set featuredUntil
     *
     * @param \DateTime $featuredUntil
     * @return Account
     */
    public function setFeaturedUntil($featuredUntil) {
        $this->featuredUntil = $featuredUntil;
        return $this;
    }

    /**
     * Get featuredUntil
     *
     * @return \DateTime 
     */
    public function getFeaturedUntil() {
        return $this->featuredUntil;
    }
    

    /**
     * Add telephones
     *
     * @param \Vlance\AccountBundle\Entity\AccountTelephone $telephones
     * @return Account
     */
    public function addTelephone(\Vlance\AccountBundle\Entity\AccountTelephone $telephones)
    {
        $this->telephones[] = $telephones;
    
        return $this;
    }

    /**
     * Remove telephones
     *
     * @param \Vlance\AccountBundle\Entity\AccountTelephone $telephones
     */
    public function removeTelephone(\Vlance\AccountBundle\Entity\AccountTelephone $telephones)
    {
        $this->telephones->removeElement($telephones);
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Account
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set workAvailability
     *
     * @param boolean $workAvailability
     * @return Account
     */
    public function setWorkAvailability($workAvailability)
    {
        $this->workAvailability = $workAvailability;
    
        return $this;
    }

    /**
     * Get workAvailability
     *
     * @return boolean 
     */
    public function getWorkAvailability()
    {
        return $this->workAvailability;
    }

    /**
     * Set workRhythm
     *
     * @param integer $workRhythm
     * @return Account
     */
    public function setWorkRhythm($workRhythm)
    {
        $this->workRhythm = $workRhythm;
    
        return $this;
    }

    /**
     * Get workRhythm
     *
     * @return integer 
     */
    public function getWorkRhythm()
    {
        return $this->workRhythm;
    }

    /**
     * Set credit
     *
     * @param \Vlance\PaymentBundle\Entity\CreditAccount $credit
     * @return Account
     */
    public function setCredit(\Vlance\PaymentBundle\Entity\CreditAccount $credit = null)
    {
        $this->credit = $credit;
    
        return $this;
    }

    /**
     * Get credit
     *
     * @return \Vlance\PaymentBundle\Entity\CreditAccount 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Add services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     * @return Account
     */
    public function addService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    
        return $this;
    }

    /**
     * Remove services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     */
    public function removeService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set personId
     *
     * @param \Vlance\AccountBundle\Entity\PersonId $personId
     * @return Account
     */
    public function setPersonId(\Vlance\AccountBundle\Entity\PersonId $personId = null)
    {
        $this->personId = $personId;
    
        return $this;
    }

    /**
     * Get personId
     *
     * @return \Vlance\AccountBundle\Entity\PersonId 
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set taxId
     *
     * @param \Vlance\AccountBundle\Entity\TaxId $taxId
     * @return Account
     */
    public function setTaxId(\Vlance\AccountBundle\Entity\TaxId $taxId = null)
    {
        $this->taxId = $taxId;
    
        return $this;
    }

    /**
     * Get taxId
     *
     * @return \Vlance\AccountBundle\Entity\TaxId 
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * Add invites
     *
     * @param \Vlance\JobBundle\Entity\JobInvite $invites
     * @return Account
     */
    public function addInvite(\Vlance\JobBundle\Entity\JobInvite $invites)
    {
        $this->invites[] = $invites;
    
        return $this;
    }

    /**
     * Remove invites
     *
     * @param \Vlance\JobBundle\Entity\JobInvite $invites
     */
    public function removeInvite(\Vlance\JobBundle\Entity\JobInvite $invites)
    {
        $this->invites->removeElement($invites);
    }

    /**
     * Get invites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvites()
    {
        return $this->invites;
    }

    /**
     * Set skype
     *
     * @param string $skype
     * @return Account
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
    
        return $this;
    }

    /**
     * Get skype
     *
     * @return string 
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Add jobActivities
     *
     * @param \Vlance\JobBundle\Entity\JobActivity $jobActivities
     * @return Account
     */
    public function addJobActivitie(\Vlance\JobBundle\Entity\JobActivity $jobActivities)
    {
        $this->jobActivities[] = $jobActivities;
    
        return $this;
    }

    /**
     * Remove jobActivities
     *
     * @param \Vlance\JobBundle\Entity\JobActivity $jobActivities
     */
    public function removeJobActivitie(\Vlance\JobBundle\Entity\JobActivity $jobActivities)
    {
        $this->jobActivities->removeElement($jobActivities);
    }

    /**
     * Get jobActivities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobActivities()
    {
        return $this->jobActivities;
    }
    
    /**
     * Update 20160718
     * @return boolean
     */
    public function getIsVerifiedTaxId()
    {
        if(!is_object($this->getTaxId())){
            return false;
        }
        return $this->getTaxId()->getIsVerified();
    }

    /**
     * Set isProvider
     *
     * @param boolean $isProvider
     * @return Account
     */
    public function setIsProvider($isProvider)
    {
        $this->isProvider = $isProvider;
    
        return $this;
    }

    /**
     * Get isProvider
     *
     * @return boolean 
     */
    public function getIsProvider()
    {
        return $this->isProvider;
    }

    /**
     * Set option_view
     *
     * @param string $optionView
     * @return Account
     */
    public function setOptionView($optionView)
    {
        $this->option_view = $optionView;
    
        return $this;
    }

    /**
     * Get option_view
     *
     * @return string 
     */
    public function getOptionView()
    {
        if(!$this->option_view){
            if(!$this->type){
                return 'freelancer';
            }
            if($this->type == "freelancer" || $this->type == "client"){
                return $this->type;
            }
        }
        
        if($this->option_view == "freelancer" || $this->option_view == "client"){
            return $this->option_view;
        } else{
            return "freelancer";
        }
    }

    /**
     * Add accountSettings
     *
     * @param \Vlance\AccountBundle\Entity\AccountSetting $accountSettings
     * @return Account
     */
    public function addAccountSetting(\Vlance\AccountBundle\Entity\AccountSetting $accountSettings)
    {
        $this->accountSettings[] = $accountSettings;
    
        return $this;
    }

    /**
     * Remove accountSettings
     *
     * @param \Vlance\AccountBundle\Entity\AccountSetting $accountSettings
     */
    public function removeAccountSetting(\Vlance\AccountBundle\Entity\AccountSetting $accountSettings)
    {
        $this->accountSettings->removeElement($accountSettings);
    }

    /**
     * Get accountSettings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccountSettings()
    {
        return $this->accountSettings;
    }

    /**
     * Get value of the specific property
     *
     * @return int
     */
    public function getAccountSettingByProperty($property_name)
    {
        if(!$property_name || $property_name == ""){
            return -1;
        }
        if(!$this->getAccountSettings()){
            return -1;
        }
        if(count($this->getAccountSettings()) == 0){
            return -1;
        }
        /* @var $setting \Vlance\AccountBundle\Entity\AccountSetting */
        foreach($this->getAccountSettings() as $setting){
            if($setting->getProperty()){
                if($setting->getProperty()->getTitle() == $property_name){
                    if($setting->getValue()){
                        return 1;
                    } else{
                        return 0;
                    }
                }
            }
        }
        
        // Nếu qua được foreach, tức là setting ngày chưa được set
        return -1;
    }

    /**
     * Add bidRates
     *
     * @param \Vlance\JobBundle\Entity\BidRate $bidRates
     * @return Account
     */
    public function addBidRate(\Vlance\JobBundle\Entity\BidRate $bidRates)
    {
        $this->bidRates[] = $bidRates;
    
        return $this;
    }

    /**
     * Remove bidRates
     *
     * @param \Vlance\JobBundle\Entity\BidRate $bidRates
     */
    public function removeBidRate(\Vlance\JobBundle\Entity\BidRate $bidRates)
    {
        $this->bidRates->removeElement($bidRates);
    }

    /**
     * Get bidRates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBidRates()
    {
        return $this->bidRates;
    }

    /**
     * Set expiredDateVip
     *
     * @param \DateTime $expiredDateVip
     * @return Account
     */
    public function setExpiredDateVip($expiredDateVip)
    {
        $this->expiredDateVip = $expiredDateVip;
    
        return $this;
    }

    /**
     * Get expiredDateVip
     *
     * @return \DateTime 
     */
    public function getExpiredDateVip()
    {
        return $this->expiredDateVip;
    }

    /**
     * Set categoryVIP
     *
     * @param \Vlance\AccountBundle\Entity\Category $categoryVIP
     * @return Account
     */
    public function setCategoryVIP(\Vlance\AccountBundle\Entity\Category $categoryVIP = null)
    {
        $this->categoryVIP = $categoryVIP;
    
        return $this;
    }

    /**
     * Get categoryVIP
     *
     * @return \Vlance\AccountBundle\Entity\Category 
     */
    public function getCategoryVIP()
    {
        return $this->categoryVIP;
    }

    /**
     * Set typeVIP
     *
     * @param integer $typeVIP
     * @return Account
     */
    public function setTypeVIP($typeVIP)
    {
        $this->typeVIP = $typeVIP;
    
        return $this;
    }

    /**
     * Get typeVIP
     *
     * @return integer 
     */
    public function getTypeVIP()
    {
        return $this->typeVIP;
    }

    /**
     * Add admin_jobs
     *
     * @param \Vlance\AccountBundle\Entity\AccountAdminJob $adminJobs
     * @return Account
     */
    public function addAdminJob(\Vlance\AccountBundle\Entity\AccountAdminJob $adminJobs)
    {
        $this->admin_jobs[] = $adminJobs;
    
        return $this;
    }

    /**
     * Remove admin_jobs
     *
     * @param \Vlance\AccountBundle\Entity\AccountAdminJob $adminJobs
     */
    public function removeAdminJob(\Vlance\AccountBundle\Entity\AccountAdminJob $adminJobs)
    {
        $this->admin_jobs->removeElement($adminJobs);
    }

    /**
     * Get admin_jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdminJobs()
    {
        return $this->admin_jobs;
    }
}