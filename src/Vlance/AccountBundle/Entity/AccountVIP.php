<?php

namespace Vlance\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\JobBundle\Entity\Job;
use Vlance\AccountBundle\Entity\Account;

/**
 * AccountVIP
 *
 * @ORM\Table(name="account_vip")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\AccountVIPRepository")
 * 
 */
class AccountVIP
{
    const PAYMENT_METHOD_ONEPAY_OTP     = 10;
    const PAYMENT_METHOD_ONEPAY_BANK    = 20;
    const PAYMENT_METHOD_ONEPAY_VISA    = 30;
    const PAYMENT_METHOD_BANK_TRANSFER  = 40;
    const PAYMENT_METHOD_PAYPAL         = 50;
    const PAYMENT_METHOD_CASH           = 60;
    const PAYMENT_METHOD_USERCASH       = 70;
    const PAYMENT_METHOD_OTHER          = 80;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $account;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @var integer
     * @ORM\Column(name="package", type="decimal")
     */
    protected $package;
    
    /**
     * @var integer
     * @ORM\Column(name="price", type="integer")
     */
    private $price;
    
    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    protected $comment = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_method", type="smallint")
     */
    private $paymentMethod;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="accountVips")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $category;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AccountVIP
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return AccountVIP
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set package
     *
     * @param float $package
     * @return AccountVIP
     */
    public function setPackage($package)
    {
        $this->package = $package;
    
        return $this;
    }

    /**
     * Get package
     *
     * @return float 
     */
    public function getPackage()
    {
        return $this->package;
    }

    public function getPackageText()
    {
        if($this->package == 1){
            return "Vip 3";
        } elseif($this->package == 2){
            return "Vip 6";
        } elseif($this->package == 3){
            return "Vip 12";
        } elseif($this->package == 4){
            return "Mini Vip 3";
        } elseif($this->package == 5){
            return "Mini Vip 6";
        } elseif($this->package == 6){
            return "Mini Vip 12";
        }
    }


    /**
     * Set price
     *
     * @param integer $price
     * @return AccountVIP
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return AccountVIP
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return AccountVIP
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     * @return AccountVIP
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    
        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    public function getPaymentMethodText()
    {
        if($this->paymentMethod == self::PAYMENT_METHOD_ONEPAY_OTP){
            return "PAYMENT_METHOD_ONEPAY_OTP";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_ONEPAY_BANK){
            return "PAYMENT_METHOD_ONEPAY_BANK";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_ONEPAY_VISA){
            return "PAYMENT_METHOD_ONEPAY_VISA";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_BANK_TRANSFER){
            return "PAYMENT_METHOD_BANK_TRANSFER";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_PAYPAL){
            return "PAYMENT_METHOD_PAYPAL";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_CASH){
            return "PAYMENT_METHOD_CASH";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_USERCASH){
            return "PAYMENT_METHOD_USERCASH";
        } elseif($this->paymentMethod == self::PAYMENT_METHOD_OTHER){
            return "PAYMENT_METHOD_OTHER";
        }
    }

    /**
     * Set category
     *
     * @param \Vlance\AccountBundle\Entity\Category $category
     * @return AccountVIP
     */
    public function setCategory(\Vlance\AccountBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Vlance\AccountBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}