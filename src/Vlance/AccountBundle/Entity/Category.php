<?php

namespace Vlance\AccountBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\JobBundle\Entity\Job;
use Vlance\AccountBundle\Entity\Account;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\CategoryRepository")
 * 
 */
class Category implements \Serializable
{
    const JOB_TITLE = 'job_title';
    const JOB_KEYWORD = 'job_keyword';
    const JOB_DESCRIPTION = 'job_description';
    const FREELANCER_TITLE = 'freelancer_title';
    const FREELANCER_KEYWORD = 'freelancer_keyword';
    const FREELANCER_DESCRIPTION = 'freelancer_description';
    
    const AVAILABLE_CONTEST_NO = 0;
    const AVAILABLE_CONTEST_YES = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    protected $shortDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=true)
     */
    protected $metaTitle = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="text", nullable=true)
     */
    protected $metaKeyword = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_image", type="text", nullable=true)
     */
    protected $metaImage = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="seo_url", type="text", nullable=true)
     */
    protected $seoUrl = null;
    
    /**
     * Update 20160525: Added to database
     * @var integer
     * @ORM\Column(name="available_contest", type="smallint")
     */
    private $contest = self::AVAILABLE_CONTEST_NO;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", options={"default" : false})
     */
    protected $online = false;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="cat_order", type="integer", options={"default" : "0"})
     */
    protected $order = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path = null;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="childs")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", unique=false, onDelete="CASCADE", nullable=true)
     */
    protected $parent;
    
    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     */
    protected $childs;
    
    /**
     * @var string
     * 
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @ORM\OneToMany(targetEntity="Account", mappedBy="category")
     */
    protected $accounts;
    
    /**
     * @ORM\OneToMany(targetEntity="AccountVIP", mappedBy="category")
     */
    protected $accountVips;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="category")
     */
    protected $jobs;
    
    /**
     * @var string
     *
     * @ORM\Column(name="suggest_job", type="string", options={"default" : ""})
     */
    protected $suggestJob = "";
    
    /**
     * @var string
     *
     * @ORM\Column(name="suggest_bid", type="string", options={"default" : ""})
     */
    protected $suggestBid = "";
    
    /**
     * @var string
     *
     * @ORM\Column(name="suggest_portfolio", type="string", options={"default" : ""})
     */
    protected $suggestPortfolio = "";
    
    
    /**
     * @var string
     * @ORM\Column(name="additional_data", type="text", nullable=true)
     */
    protected $additionalData;
    
    protected $addtionalDataArray;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * Update 20160628
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Service", inversedBy="categories")
     * @ORM\JoinTable(name="category_service",
     * joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $services;
    
    /**
     * Set id
     * @param integer $id
     * @return Category
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return Category
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parent = null;
        $this->childs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addtionalDataArray = array();
    }
    
    /**
     * Add accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     * @return Category
     */
    public function addAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts[] = $accounts;
    
        return $this;
    }

    /**
     * Remove accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     */
    public function removeAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
    
    public function __toString()
    {
        if ($this->parent) {
            return '' . $this->title;
        }
        return '+ ' . $this->title;
    }

    /**
     * Add jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     * @return Category
     */
    public function addJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    
    /**
     * Get num job open in category
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getfilterJobs () {
        $num = $this->getJobs()->filter(
                    function($entry) {
                        if($entry->getPublish() == Job::PUBLISH) {
                            return true;
                        }
                        return false;
                    }
                );
        return count($num);
    }
    
    /**
     * Get num account type = freelancer in category
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getfilterFreelancer () {
        $num = $this->getAccounts()->filter(
                    function($entry) {
                        if($entry->getType() == Account::TYPE_FREELANCER) {
                            return true;
                        }
                        return false;
                    }
                );
        return count($num);
    }

    /**
     * Set parent
     *
     * @param \Vlance\AccountBundle\Entity\Category $parent
     * @return Category
     */
    public function setParent(\Vlance\AccountBundle\Entity\Category $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \Vlance\AccountBundle\Entity\Category 
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    public function getParentName() {
        if(null !== $this->getParent()) {
            return ' - '.$this->getTitle();
        }
       return $this->getTitle();
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Category
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Add childs
     *
     * @param \Vlance\AccountBundle\Entity\Category $childs
     * @return Category
     */
    public function addChild(\Vlance\AccountBundle\Entity\Category $childs)
    {
        $this->childs[] = $childs;
    
        return $this;
    }

    /**
     * Remove childs
     *
     * @param \Vlance\AccountBundle\Entity\Category $childs
     */
    public function removeChild(\Vlance\AccountBundle\Entity\Category $childs)
    {
        $this->childs->removeElement($childs);
    }

    /**
     * Get childs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChilds()
    {
        return $this->childs;
    }
    
    /**
     * Set path
     *
     * @param string $path
     * @return Category
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    public function serialize() {
        return serialize(array(
            'id'                => $this->id,
            'title'             => $this->title,
            'description'       => $this->description,
            'shortDescription'  => $this->shortDescription,
            'metaTitle'         => $this->metaTitle,
            'metaDescription'   => $this->metaDescription,
            'metaKeyword'       => $this->metaKeyword,
            'metaImage'         => $this->metaImage
        ));
    }

    public function unserialize($serialized) {
        $data = unserialize($serialized);
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->shortDescription = $data['shortDescription'];
        $this->description = $data['description'];
        $this->metaTitle = $data['metaTitle'];
        $this->metaDescription = $data['metaDescription'];
        $this->metaKeyword = $data['metaKeyword'];
        $this->metaImage = $data['metaImage'];
        return $this;        
    }

    /**
     * Set suggestJob
     *
     * @param string $suggestJob
     * @return Category
     */
    public function setSuggestJob($suggestJob)
    {
        $this->suggestJob = $suggestJob;
    
        return $this;
    }

    /**
     * Get suggestJob
     *
     * @return string 
     */
    public function getSuggestJob()
    {
        return $this->suggestJob;
    }

    /**
     * Set suggestBid
     *
     * @param string $suggestBid
     * @return Category
     */
    public function setSuggestBid($suggestBid)
    {
        $this->suggestBid = $suggestBid;
    
        return $this;
    }

    /**
     * Get suggestBid
     *
     * @return string 
     */
    public function getSuggestBid()
    {
        return $this->suggestBid;
    }

    /**
     * Set suggestPortfolio
     *
     * @param string $suggestPortfolio
     * @return Category
     */
    public function setSuggestPortfolio($suggestPortfolio)
    {
        $this->suggestPortfolio = $suggestPortfolio;
    
        return $this;
    }

    /**
     * Get suggestPortfolio
     *
     * @return string 
     */
    public function getSuggestPortfolio()
    {
        return $this->suggestPortfolio;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Category
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    
        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Category
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Category
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     * @return Category
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->metaKeyword = $metaKeyword;
    
        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string 
     */
    public function getMetaKeyword()
    {
        return $this->metaKeyword;
    }

    /**
     * Set metaImage
     *
     * @param string $metaImage
     * @return Category
     */
    public function setMetaImage($metaImage)
    {
        $this->metaImage = $metaImage;
    
        return $this;
    }

    /**
     * Get metaImage
     *
     * @return string 
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * Get full path metaImage
     *
     * @return string 
     */
    public function getFullPathMetaImage()
    {
        if($this->getMetaImage()){
            return $this->metaImage;
        } else {
            $path = ROOT_DIR . "/media/category/";
            return  $path . $this->id . ".jpg";
        }
    }

    /**
     * Set seoUrl
     *
     * @param string $seoUrl
     * @return Category
     */
    public function setSeoUrl($seoUrl)
    {
        $this->seoUrl = $seoUrl;
    
        return $this;
    }

    /**
     * Get seoUrl
     *
     * @return string 
     */
    public function getSeoUrl()
    {
        return $this->seoUrl;
    }

    /**
     * Set online
     *
     * @param boolean $online
     * @return Category
     */
    public function setOnline($online)
    {
        $this->online = $online;
    
        return $this;
    }

    /**
     * Get online
     *
     * @return boolean 
     */
    public function getOnline()
    {
        return $this->online;
    }
    
    /**
     * Get filters to building link to jobs list
     * 
     * @return string
     */
    public function getJobListFilters(){
        if($this->getParent()){
            $chash = $this->getHash();
            $cpath = $this->getParent()->getHash();
            $filters = 'cpath_'.$cpath.'_chash_'.$chash ;
        } else{
            $filters = 'cpath_'.$cpath;
        }
        return $filters;
    }
    
    /**
     * Set additionalData
     *
     * @param mixed $additionalData json string or array of data
     * @return Category
     */
    public function setAdditionalData($additionalData)
    {
        if (is_array($additionalData)) {
            $this->addtionalDataArray = $additionalData;
            $this->additionalData = json_encode($additionalData);
        } else {
            $this->additionalData = $additionalData;
            $this->addtionalDataArray = json_decode($additionalData);
        }
        return $this;
    }

    /**
     * 
     * @param string $field field name or * to get all additional data, empty to get json text
     * @return mixed
     */
    public function getAdditionalData($field = "")
    {
        if (((is_null($this->addtionalDataArray)) || empty($this->addtionalDataArray)) && $this->additionalData) {
            $this->addtionalDataArray = json_decode($this->additionalData);
        }
        if ($field) {
            if ($field != "*") {
                if (isset($this->addtionalDataArray->{$field})) {
                    return $this->addtionalDataArray->{$field};
                } else {
                    return "";
                }
            } else {
                return $this->addtionalDataArray;
            }
        } else {
            return $this->additionalData;
        }
    }
    
    /**
     * Add a field to additional data
     * 
     * @param string $field
     * @param type $value
     * @return Category
     */
    public function addAdditionalData($field, $value) {
        $this->addtionalDataArray[$field] = $value;
        $this->additionalData = json_encode($this->addtionalDataArray);
        return $this;
    }
    
    /**
     * 
     * @param mixed $fields array of field to remove or field name
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function removeAdditionalData($fields) {
        if (is_array($fields)) {
            foreach ($fields as $field) {
                if (is_string($field) && isset($this->addtionalDataArray->{$field})) {
                    unset($this->addtionalDataArray->{$field});
                }
            }
        } else if (is_string($field) && isset($this->addtionalDataArray->{$field})) {
            unset($this->addtionalDataArray->{$field});
        }
        return $this;
    }
    
    /**
     * Set title for category in view page for job
     * @param string $jobTitle
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setJobTitle($jobTitle) {
        $this->addAdditionalData(self::JOB_TITLE, $jobTitle);
        return $this;
    }

    /**
     * Get title display in view page for job
     * @return string
     */
    public function getJobTitle() {
        return $this->getAdditionalData(self::JOB_TITLE);
    }
    
    /**
     * Set keyword for category in view page for job
     * @param string $jobKeyword
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setJobKeyword($jobKeyword) {
        $this->addAdditionalData(self::JOB_KEYWORD, $jobKeyword);
        return $this;
    }

    /**
     * Get keyword display in view page for job
     * @return string
     */
    public function getJobKeyword() {
        return $this->getAdditionalData(self::JOB_KEYWORD);
    }
    
    /**
     * Set description for category in view page for job
     * @param string $jobDescription
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setJobDescription($jobDescription) {
        $this->addAdditionalData(self::JOB_DESCRIPTION, $jobDescription);
        return $this;
    }

    /**
     * Get description display in view page for job
     * @return string
     */
    public function getJobDescription() {
        return $this->getAdditionalData(self::JOB_DESCRIPTION);
    }
    
    /**
     * Set title for category in view page for job
     * @param string $freelancerTitle
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setFreelancerTitle($freelancerTitle) {
        $this->addAdditionalData(self::FREELANCER_TITLE, $freelancerTitle);
        return $this;
    }

    /**
     * Get title display in view page for job
     * @return string
     */
    public function getFreelancerTitle() {
        return $this->getAdditionalData(self::FREELANCER_TITLE);
    }
    
    /**
     * Set keyword for category in view page for job
     * @param string $freelancerKeyword
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setFreelancerKeyword($freelancerKeyword) {
        $this->addAdditionalData(self::FREELANCER_KEYWORD, $freelancerKeyword);
        return $this;
    }

    /**
     * Get keyword display in view page for job
     * @return string
     */
    public function getFreelancerKeyword() {
        return $this->getAdditionalData(self::FREELANCER_KEYWORD);
    }
    
    /**
     * Set description for category in view page for job
     * @param string $freelancerDescription
     * @return \Vlance\AccountBundle\Entity\Category
     */
    public function setFreelancerDescription($freelancerDescription) {
        $this->addAdditionalData(self::FREELANCER_DESCRIPTION, $freelancerDescription);
        return $this;
    }

    /**
     * Get description display in view page for job
     * @return string
     */
    public function getFreelancerDescription() {
        return $this->getAdditionalData(self::FREELANCER_DESCRIPTION);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Category
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Category
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set contest
     *
     * @param integer $contest
     * @return Category
     */
    public function setContest($contest)
    {
        $this->contest = $contest;
    
        return $this;
    }

    /**
     * Get contest
     *
     * @return integer 
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Add services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     * @return Category
     */
    public function addService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    
        return $this;
    }

    /**
     * Remove services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     */
    public function removeService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Get services
     *
     * @return array 
     */
    public function getServicesSorted()
    {
        $sorted_services = array();
        foreach($this->getServices() as $service){
            $sorted_services[$service->getTitle()] = $service;
        }
        ksort($sorted_services);
        return array_values($sorted_services);
    }

    /**
     * Add accountVips
     *
     * @param \Vlance\AccountBundle\Entity\AccountVIP $accountVips
     * @return Category
     */
    public function addAccountVip(\Vlance\AccountBundle\Entity\AccountVIP $accountVips)
    {
        $this->accountVips[] = $accountVips;
    
        return $this;
    }

    /**
     * Remove accountVips
     *
     * @param \Vlance\AccountBundle\Entity\AccountVIP $accountVips
     */
    public function removeAccountVip(\Vlance\AccountBundle\Entity\AccountVIP $accountVips)
    {
        $this->accountVips->removeElement($accountVips);
    }

    /**
     * Get accountVips
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccountVips()
    {
        return $this->accountVips;
    }
}