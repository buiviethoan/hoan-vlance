<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\ServiceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Service
{
    const AVAILABLE_CONTEST_NO = 0;
    const AVAILABLE_CONTEST_YES = 1;
    
    /**** Base information ****/
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * 
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="online", type="boolean", options={"default" : false})
     */
    protected $online = false;
    
    /**
     * Update 20160525: Added to database
     * @var integer
     * @ORM\Column(name="available_contest", type="smallint")
     */
    private $contest = self::AVAILABLE_CONTEST_NO;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="cat_order", type="integer", options={"default" : "0"})
     */
    protected $order = 0;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Account", mappedBy="services")
     */
    private $accounts;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="service")
     */
    protected $jobs;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Portfolio", mappedBy="services")
     */
    protected $portfolios;
    
    /**
     * Update 20160628
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Category", mappedBy="services")
     * @ORM\JoinTable(name="service_category",
     * joinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $categories;
    /**** END Base information ****/
    
    
    
    /**** Service information ****/
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique = true, nullable=true)
     */
    protected $title = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="text", nullable=true)
     */
    protected $shortDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contest_pricing", type="text", nullable=true)
     */
    protected $contestPricing = null;
    /**** END Service information ****/
    
    
    
    /**** SEO section ****/
    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=true)
     */
    protected $metaTitle = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="text", nullable=true)
     */
    protected $metaKeyword = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_image", type="text", nullable=true)
     */
    protected $metaImage = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="seo_url", type="text", nullable=true)
     */
    protected $seoUrl = null;
    /**** END SEO section ****/
    
    public function __construct() {
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Skill
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * @param integer $id
     * @return Skill
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * Add jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     * @return Skill
     */
    public function addJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     * @return Skill
     */
    public function addAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts[] = $accounts;
    
        return $this;
    }

    /**
     * Remove accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     */
    public function removeAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
    
    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash() {
        return $this->hash;
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return Skill
     */
    public function setHash($hash) {
        $this->hash = $hash;
    }



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Skill
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Skill
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set online
     *
     * @param boolean $online
     * @return Service
     */
    public function setOnline($online)
    {
        $this->online = $online;
    
        return $this;
    }

    /**
     * Get online
     *
     * @return boolean 
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Service
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     * @return Service
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    
        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string 
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Service
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Service
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     * @return Service
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->metaKeyword = $metaKeyword;
    
        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string 
     */
    public function getMetaKeyword()
    {
        return $this->metaKeyword;
    }

    /**
     * Set metaImage
     *
     * @param string $metaImage
     * @return Service
     */
    public function setMetaImage($metaImage)
    {
        $this->metaImage = $metaImage;
    
        return $this;
    }

    /**
     * Get metaImage
     *
     * @return string 
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * Set seoUrl
     *
     * @param string $seoUrl
     * @return Service
     */
    public function setSeoUrl($seoUrl)
    {
        $this->seoUrl = $seoUrl;
    
        return $this;
    }

    /**
     * Get seoUrl
     *
     * @return string 
     */
    public function getSeoUrl()
    {
        return $this->seoUrl;
    }

    /**
     * Add portfolios
     *
     * @param \Vlance\AccountBundle\Entity\Portfolio $portfolios
     * @return Service
     */
    public function addPortfolio(\Vlance\AccountBundle\Entity\Portfolio $portfolios)
    {
        $this->portfolios[] = $portfolios;
    
        return $this;
    }

    /**
     * Remove portfolios
     *
     * @param \Vlance\AccountBundle\Entity\Portfolio $portfolios
     */
    public function removePortfolio(\Vlance\AccountBundle\Entity\Portfolio $portfolios)
    {
        $this->portfolios->removeElement($portfolios);
    }

    /**
     * Get portfolios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPortfolios()
    {
        return $this->portfolios;
    }

    /**
     * Set contest
     *
     * @param integer $contest
     * @return Service
     */
    public function setContest($contest)
    {
        $this->contest = $contest;
    
        return $this;
    }

    /**
     * Get contest
     *
     * @return integer 
     */
    public function getContest()
    {
        return $this->contest;
    }

    /**
     * Add categories
     *
     * @param \Vlance\AccountBundle\Entity\Category $categories
     * @return Service
     */
    public function addCategorie(\Vlance\AccountBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Vlance\AccountBundle\Entity\Category $categories
     */
    public function removeCategorie(\Vlance\AccountBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set contestPricing
     *
     * @param string $contestPricing
     * @return Service
     */
    public function setContestPricing($contestPricing)
    {
        $this->contestPricing = $contestPricing;
    
        return $this;
    }

    /**
     * Get contestPricing
     *
     * @return string 
     */
    public function getContestPricing()
    {
        return $this->contestPricing;
    }
}