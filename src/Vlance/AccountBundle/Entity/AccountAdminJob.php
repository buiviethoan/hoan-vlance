<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Account Admin Job
 *
 * @ORM\Table(name="account_admin_job")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\AccountAdminJobRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AccountAdminJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="admin_jobs")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;
    
    /**
     * @var string
     * @ORM\Column(name="telephone", type="string", length=15)
     */
    protected $telephone;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return AccountAdminJob
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AccountAdminJob
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return AccountAdminJob
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}