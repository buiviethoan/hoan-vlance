<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AccountSettingProperty
 *
 * @ORM\Table(name="account_setting_property")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\AccountSettingPropertyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AccountSettingProperty
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", unique=true, length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;

    
    /*** Lifecycle Attributes ***/
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    
    /*** Mapped Attributes ***/
    /**
    * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\AccountSetting", mappedBy="property")
    */
    protected $accountSettings;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accountSettings = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return AccountSettingProperty
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return AccountSettingProperty
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AccountSettingProperty
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return AccountSettingProperty
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add accountSettings
     *
     * @param \Vlance\AccountBundle\Entity\AccountSetting $accountSettings
     * @return AccountSettingProperty
     */
    public function addAccountSetting(\Vlance\AccountBundle\Entity\AccountSetting $accountSettings)
    {
        $this->accountSettings[] = $accountSettings;
    
        return $this;
    }

    /**
     * Remove accountSettings
     *
     * @param \Vlance\AccountBundle\Entity\AccountSetting $accountSettings
     */
    public function removeAccountSetting(\Vlance\AccountBundle\Entity\AccountSetting $accountSettings)
    {
        $this->accountSettings->removeElement($accountSettings);
    }

    /**
     * Get accountSettings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccountSettings()
    {
        return $this->accountSettings;
    }
}