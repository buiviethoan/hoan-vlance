<?php

namespace Vlance\AccountBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;
//use Pagerfanta\Adapter\DoctrineORMAdapter;
use Vlance\BaseBundle\Paginator\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Doctrine\ORM\QueryBuilder;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;



/**
 * AccountRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AccountRepository extends EntityRepository
{
    
    public function findByMissingCash() {
        $query = $this->getEntityManager()->createQuery('SELECT a FROM VlanceAccountBundle:Account a WHERE a.id NOT IN (SELECT IDENTITY (c.account) FROM VlancePaymentBundle:UserCash c)');
        return $query->execute();
    }
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null) {
        $criteria['type'] = array(Account::TYPE_FREELANCER, Account::TYPE_CLIENT);
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }
    public function findType($type , $enabled) {
        return parent::findBy(array('type' => $type,'enabled' => $enabled));
    }
    
     /**
     * Search account
     */
//    public function searchAccounts($search = '', array $filters = null) {
//        $currentPage = 1;
//        $pageSize = 10;
//        $orderBy = '';
//        $order = 'ASC';
//        /* @var $queryBuilder Doctrine\ORM\QueryBuilder */
//        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
//        $queryBuilder->select('a')->from('VlanceAccountBundle:Account','a');
//        $queryBuilder->leftJoin('a.category', 'cat','WITH','cat.id = a.category');
//        $queryBuilder->leftJoin('a.skills', 'skill');
////        $queryBuilder->addSelect("MATCH_AGAINST (j.title, j.description, cat.title , :search 'IN BOOLEAN MODE') as score");
////        $queryBuilder->add('where', 'MATCH_AGAINST(j.title, j.description, :search) > 0');
//        $queryBuilder->orWhere('MATCH_AGAINST(a.title, a.description, a.fullName, a.address, :search) > 0');
//        $queryBuilder->orWhere('MATCH_AGAINST(cat.title, :search ) > 0');
//        $queryBuilder->orWhere('MATCH_AGAINST(skill.title, :search) > 0');
////        $queryBuilder->andWhere('a.type = '.Account::TYPE_FREELANCER.'');
//         $queryBuilder->orderBy('a.updatedAt','DESC');       
//        $queryBuilder->setParameter('search', $search);
//        
//       
//        if (!is_null($filters) && is_array($filters) && count($filters)) {
//            foreach ($filters as $field => $value) {
//                if ('page' == $field) {
//                    $currentPage = $value;
//                    continue;
//                }
//                if ('pagesize' == $field) {
//                    $pageSize = $value;
//                    continue;
//                }
//                if ('orderby' == $field) {
//                    $orderBy = $value;
//                    continue;
//                }
//                if ('order' == $field) {
//                    $order = $value;
//                    continue;
//                }
//            }
//        }
////        if($andCondition->getParts()) {
////            $queryBuilder->Where($andCondition);
////        }
////        echo $queryBuilder->getQuery()->getSQL();
////        $queryBuilder->orderBy('j.updatedAt','DESC');
//        $adapter = new DoctrineORMAdapter($queryBuilder);
//        $pager = new Pagerfanta($adapter);
//        $pager->setMaxPerPage($pageSize);
//        try {
//            $pager->setCurrentPage($currentPage);
//        } catch (\Pagerfanta\Exception\NotValidCurrentPageException $e) {
//            $pager->setCurrentPage(1);
//        }
//        return $pager;
//    
//    }
    /**
     * Return path category parent
     */
     public function convertUrl($path) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('cat.path')->from('VlanceAccountBundle:Category','cat');
        $queryBuilder->where('cat.hash = :hash');
        $queryBuilder->setParameter('hash', $path);
        $result = $queryBuilder->getQuery()->getOneOrNullResult();
        return $result;
    }
    
    public function unsubscribeAccount($encryption) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account','a');
        $queryBuilder->where("md5(concat(a.email,'@vlance.vn')) = :email");
        $queryBuilder->setParameter('email', $encryption);
        $account = $queryBuilder->getQuery()->getResult();
        return $account;
        
    }

        /**
     * Get pager with a filter array
     * @param array $filters
     * Filters is an array with structure like:
     * if field is: page, pagesize, orderby, order. we don't use this field to filter
     * field => value: the criteria is field = value
     * field => array(condition => value)
     * condition is: in, like, !=, neq, >, gt, >=, gte, <, lt, <=, lte, notIn, isNull, isNotNull
     * @return \Pagerfanta\Pagerfanta
     */
    public function getPager (array $filters = null) {
        $currentPage = 1;
        $pageSize = 10;
        $orderBy = '';
        $order = 'ASC';

        $allowedFields = array('a.email', 'a.full_name', 'a.city', 'cat.id', 'cat.path','cat.hash' , 'a.title', 'a.score', 'a.numJob', 'a.numJobFinish', 'a.inCome', 'a.description', 'a.address', 'a.telephone', 'city.hash', 'cpath','chash','city', 'city.id','kynang');
        //$allowedFields = array('a.full_name', 'a.title', 'a.numJobFinish', 'a.inCome', 'city.hash','kynang');
        $allowedFields_addon = array('a.certificated', 'status'); // Certificated account
        
        /* @var $queryBuilder Doctrine\ORM\QueryBuilder */
//        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        //$queryBuilder->select('a', 'cat', 'city', 'cash')->from('VlanceAccountBundle:Account','a');
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account','a');
        $queryBuilder->leftJoin('a.category', 'cat');
        $queryBuilder->leftJoin('a.city', 'city');
        $queryBuilder->leftJoin('a.skills', 'skill');
        $andCondition = $queryBuilder->expr()->andX();
        
        //Display only the account type freelancer
        $andCondition->add($queryBuilder->expr()->isNotNull('a.type'));
        $isVerifyCondition = $queryBuilder->expr()->orX();
        //Display only the activeated account
        //$andCondition->add($queryBuilder->expr()->eq('a.enabled', "1")); //TODO: s2-number-freelancer
        $i = 0;
        if (!is_null($filters) && is_array($filters) && count($filters)) {
            foreach ($filters as $field => $value) {
                if ('page' == $field) {
                    $currentPage = $value;
                    continue;
                }
                if ('pagesize' == $field) {
                    $pageSize = $value;
                    continue;
                }
                if ('orderby' == $field) {
                    $orderBy = $value;
                    if (!in_array($orderBy, $allowedFields)) {
                        throw(new \Exception('wrong orderby field'));
                    }
                    continue;
                }
                if ('order' == $field) {
                    $order = $value;
                    continue;
                }
                //After that, the field is used to filter result so we need to check this field is allowed to use or not
                if (!in_array($field, $allowedFields) && !in_array($field, $allowedFields_addon)) {
                    throw(new \Exception('wrong field: ' . $field));
                }
                if (is_array($value)) {
                    $condition = reset($value);
                    $i++;
                    $orCondition{$i} = $queryBuilder->expr()->orX();
                    if($field == 'chash') {
                        foreach($value as $val) {
                            $orCondition{$i}->add($queryBuilder->expr()->eq("cat.hash", "'" . $val . "'"));
                        }
                    }
                    if($field == 'city') {
                        foreach($value as $val) {
                            $orCondition{$i}->add($queryBuilder->expr()->eq("city.hash", "'" . $val . "'"));
                        }
                    }
                    if($field == 'kynang') {
                        foreach($value as $val) {
                            $orCondition{$i}->add($queryBuilder->expr()->eq("skill.hash", "'" . $val . "'"));
                        }
                    }
                    $queryBuilder->andWhere($orCondition{$i});
                    
                } else {
                    if($field == 'cpath') {
                        $values = $this->convertUrl($value);
                        $andCondition->add($queryBuilder->expr()->like("cat.path", "'" . $values['path'] . "%'"));
                    }
                    elseif($field == 'chash') {
                        $andCondition->add($queryBuilder->expr()->eq("cat.hash", "'" . $value . "'"));
                    }
                    elseif($field == 'city') {
                        $andCondition->add($queryBuilder->expr()->eq("city.hash", "'" . $value . "'"));
                    }
                    elseif($field == 'kynang') {
                        $andCondition->add($queryBuilder->expr()->eq("skill.hash", "'" . $value . "'"));
                    }
                    elseif($field == 'status'){
                        if($value == 'xac-thuc') {
                            $isVerifyCondition->add($queryBuilder->expr()->eq("a.isCertificated", "1"));
                            $isVerifyCondition->add($queryBuilder->expr()->isNotNull("a.telephoneVerifiedAt"));
                            $andCondition->add($isVerifyCondition);
                        }
                    }
                    else{
                        $andCondition->add($queryBuilder->expr()->eq($field, "'" . $value . "'"));
                    }
                    
                }
            }
        }

        $queryBuilder->andWhere($andCondition);

        // Sort by Income
        $queryBuilder->orderBy('a.inCome', 'DESC');
        $queryBuilder->addOrderBy('a.numJobFinish', 'DESC');
        $queryBuilder->addOrderBy('a.isCertificated','DESC');
        $queryBuilder->addOrderBy('a.updatedAt','DESC');
        
        //echo $queryBuilder->getQuery()->getSQL();
        
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage($pageSize);
        try {
            $pager->setCurrentPage($currentPage);
        } catch (\Pagerfanta\Exception\NotValidCurrentPageException $e) {
            $pager->setCurrentPage(1);
        }
        return $pager;
    }
    
    /**
     * 
     * @param array $fields $fields = array('field1', 'field2',...)
     * @param array $conditions $conditions = array('field' => array(0 => 'operator', 1 => 'value'))
     * Operator is <i>eq</i>, <i>gte</i>, <i>lte</i>, <i>in</i>, <i>isNull</i>, <i>isNotNull</i>
     * @param int $limit max results
     * @param int $offset the first result
     */
    public function getFields($fields, array $conditions, $limit = 0, $offset = 0) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select($fields);
        $queryBuilder->from('VlanceAccountBundle:Account', 'a');
        $where = $queryBuilder->expr()->andX();
        foreach ($conditions as $field => $condition) {
            if (in_array($condition[0], array("isNull", "isNotNull"))) {
                $where->add($queryBuilder->expr()->{$condition[0]}("a.$field"));
            } else {
                $where->add($queryBuilder->expr()->{$condition[0]}("a.$field", $condition[1]));
            }
        }
        $queryBuilder->where($where);
        if ($limit > 0) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }
        return $queryBuilder->getQuery()->getResult();
    } 

    
    /**
     * 
     * @param array $fields $fields = array('field' => 'operator')
     * Operator is <i>sum</i>, <i>count</i>
     * @param array $conditions $conditions = array('field' => array(0 => 'operator', 1 => 'value'))
     * Operator is <i>eq</i>, <i>gte</i>, <i>lte</i>, <i>in</i>, <i>isNull</i>, <i>isNotNull</i>
     */
    public function statistic(array $fields, array $conditions) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $select = array();
        foreach ($fields as $field => $operator) {
            $select[] = "$operator(a.$field) $field";
        }
        $queryBuilder->select(implode(",", $select))->from('VlanceAccountBundle:Account', 'a');
        $where = $queryBuilder->expr()->andX();
        foreach ($conditions as $field => $condition) {
            if (in_array($condition[0], array("isNull", "isNotNull"))) {
                $where->add($queryBuilder->expr()->{$condition[0]}("a.$field"));
            } else {
                $where->add($queryBuilder->expr()->{$condition[0]}("a.$field", $condition[1]));
            }
        }
        $queryBuilder->where($where);
//        echo $queryBuilder->getDQL();
        return $queryBuilder->getQuery()->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
    }
    
    public function getAccountByIdHash($idHash, $hashString = '@vlance.vn') {
        $dql = "SELECT a FROM VlanceAccountBundle:Account a WHERE MD5(CONCAT(a.id, '$hashString')) = '$idHash'";
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->getOneOrNullResult();
    }
    
    /**
     * 
     * @param array $fields $fields = array('field' => 'operator')
     */
    public function getAllFields(array $fields) {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $select = array();
        $queryBuilder->select($fields);
        $queryBuilder->from('VlanceAccountBundle:Account', 'a');
        
        return $queryBuilder->getQuery()->getResult();
    }
    
    /**
     * Select the best freelancers for certains criterials
     * @param array $fields
     */
    public function getSuggestedFreelancers(array $fields)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account','a');
        $queryBuilder->leftJoin('a.category', 'cat');
        $queryBuilder->leftJoin('a.cash', 'cash');
        $andCondition = $queryBuilder->expr()->andX();
        if(isset($fields['skills'])){
            foreach($fields['skills'] as $key => $skill){
                $queryBuilder->leftJoin('a.skills', 'skill' . $key, 'WITH', 'skill'.$key.'.id = ' . $skill);
                $andCondition->add($queryBuilder->expr()->in('skill'.$key.'.id', $fields['skills']));
            }
        }
        $andCondition->add($queryBuilder->expr()->eq("a.type", "'freelancer'"));
        $andCondition->add($queryBuilder->expr()->eq('a.enabled', "1"));
        $andCondition->add($queryBuilder->expr()->eq('cat.id', $fields['category']));
        $queryBuilder->andWhere($andCondition);
        
        // Sort by Income
        $queryBuilder->orderBy('a.inCome', 'DESC');
        $queryBuilder->addOrderBy('a.isCertificated','DESC');
        $queryBuilder->addOrderBy('a.numReviews', 'DESC');
        $queryBuilder->addOrderBy('a.score', 'DESC');
        $queryBuilder->addOrderBy('a.numJobFinish', 'DESC');        
        $queryBuilder->addOrderBy('a.updatedAt','DESC');

        //echo $queryBuilder->getQuery()->getSQL() . "\n\n";
        
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage(10);
        
        $result = array();
        foreach($pager->getCurrentPageResults() as $key => $entity){
            $result[$key] = $entity;
        }
        return $result;
    }
    
    /**
     * Get random n best freelancers for certains criterials
     * @param array $fields
     * @param int   $qty
     */
    public function getRandSuggestedFreelancers(array $fields, $qty = 8)
    {
        $freelancers = array();
        // If there are skills, will select the freelancers who have similars the most
        if(count($fields['skills']) > 0){
            $freelancerCount = 0;
            while ($freelancerCount < ($qty * 4) && count($fields['skills']) > 0){
                $freelancers = $this->getSuggestedFreelancers(array(
                    'category'  => $fields['category'],
                    'skills'    => $fields['skills']
                ));
                // Remove freelancer who has 0 < rating < 4. NOTE: rating 0 is sometime better than 1,2 or 3
                foreach($freelancers as $key => $fl){
                    if($fl->getNumReviews() > 0 && $fl->getScore()/$fl->getNumReviews() < 4){
                        unset($freelancers[$key]);
                    }
                }
                $freelancerCount = count($freelancers);
                array_pop($fields['skills']);
            }
        } 
        // If there is not skill
        else{
            $freelancers = $this->getSuggestedFreelancers(array(
                'category'  => $fields['category'],
            ));
            // Remove freelancer who has 0 < rating < 4. NOTE: rating 0 is sometime better than 1,2 or 3
            foreach($freelancers as $key => $fl){
                if($fl->getNumReviews() > 0 && $fl->getScore()/$fl->getNumReviews() < 4){
                    unset($freelancers[$key]);
                }
            }
        }

        // Get N random freelancers among them
        shuffle($freelancers);
        $freelancers = array_splice($freelancers, 0, $qty);
        // But then still sort again so the top is 1st
        ksort($freelancers);
        
        return $freelancers;
    }
    
    /**
     * Get random n best freelancers for certains criterials
     * @param array $job
     * @param int   $qty
     */
    public function getSuggestedFreelancersForJob($job, $qty = 15)
    {
        $em = $this->getEntityManager();
        /* @var $job Job */
        // Check $job
        //get skill
        $skill_list = array();
        foreach($job->getSkills() as $s){
            $skill_list[] = $s->getId();
        }
        
        $case_when = "";
        foreach($skill_list as $skill){
            $case_when .= ($case_when == "" ? "" : " + ") . '(CASE WHEN sa.skill_id = ' . strtolower($skill) . " THEN 2 ELSE 0 END)";
        }
        
        $case_cat = 'a.category_id='. $job->getCategory()->getId();
        
        $day_to_seek_from = 0;
        $day_to_seek_to = 14;
        $account_to_recommend = array();
        
        while (count($account_to_recommend) < 20 && $day_to_seek_to < 5*365) {
            $sql_query = 'SELECT a.id, a.last_login, '
                            . '('
                                . ($case_when ? $case_when : '0')
                                . '+(CASE WHEN a.certificated = 1 THEN 3 ELSE 0 END)'
                                . '+(CASE WHEN a.telephone_verified_at IS NOT NULL THEN 2 ELSE 0 END)'
                                . '+(CASE WHEN ' .$case_cat. ' THEN 5 ELSE 0 END)'
                            . ') '
                            . '*(CASE WHEN a.num_reviews = 0 THEN 0.49 ELSE a.score/a.num_reviews/5 END) AS rel_score, '
                            . '(a.score/a.num_reviews) AS rate, a.num_reviews '
                        . 'FROM account a '
                            . 'LEFT JOIN skill_account sa ON sa.account_id = a.id '
                        . 'WHERE a.category_id IS NOT NULL '
                            . 'AND a.type IN("freelancer","client") '
                            . 'AND a.city_id IS NOT NULL '
                            . 'AND a.description IS NOT NULL '
                            . 'AND a.enabled = 1 '
                            . 'AND a.locked = 0 '
                            . 'AND a.banned_to IS NULL '
                            . ($day_to_seek_from >0 ? 'AND (a.last_login <= NOW() - INTERVAL ' . $day_to_seek_from . ' DAY) ' : '')
                            . 'AND (a.last_login > NOW() - INTERVAL ' . $day_to_seek_to . ' DAY) '
                            . 'AND a.id NOT IN (SELECT account_id FROM job_invite WHERE job_id = ' . $job->getId() . ') '
                        . 'GROUP BY a.id HAVING (rate > 4 AND num_reviews > 0) OR a.num_reviews = 0 '
                        . 'ORDER BY rel_score DESC, rate DESC, last_login DESC limit ' . (int)($qty);
            $rsm = new ResultSetMapping;
            $rsm->addEntityResult('Vlance\AccountBundle\Entity\Account', 'a');
            $rsm->addFieldResult('a', 'id', 'id');
            $rsm_query = $this->getEntityManager()->createNativeQuery($sql_query, $rsm);
            $account_ids = $rsm_query->getArrayResult();    

            // Query all accounts information from db, without change rel_score order
            $acc_array = array();
            foreach($account_ids as $acc)
            {
                $acc_array[] = $acc['id'];
            }

            $account_array = count($acc_array) > 0 ? $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $acc_array)) : array();

            $account_tmp = array();
            foreach($account_array as $a){
                $account_tmp[$a->getId()] = $a;
            }

            foreach($acc_array as $k => $at){
                $account_to_recommend[$k] = $account_tmp[$at];
            }
            
            $day_to_seek_from = $day_to_seek_to;
            $day_to_seek_to *= 2;
            
            //break;
            // END Query all accounts information from db, without change rel_score order
        }
        
        return $account_to_recommend;
    }
    
    /**
     * Select the feature freelancers
     * @param array $fields
     */
    public function getFeatureFreelancers(/*array $fields*/)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account','a');
        $queryBuilder->andWhere($queryBuilder->expr()->isNotNull('a.featuredUntil'));
        $result = $queryBuilder->getQuery()->getResult();
        
        $freelancers = array();
        foreach ($result as $k => $fl){
            $freelancers[$k] = $fl;
        }
        shuffle($freelancers);
        $freelancers = array_splice($freelancers, 0, 2);
        return $freelancers;
    }
    
    public function findClientsWorkedWith(Account $worker){
        $jobs_worked = $this->getEntityManager()->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($worker);
        $jobs_cancelled = $this->getEntityManager()->getRepository('VlanceJobBundle:Job')->findJobsCancelledBy($worker);
        $jobs = array_merge($jobs_cancelled, $jobs_worked);
        if(count($jobs) == 0){
            return array();
        }
        
        $clients = array();
        foreach($jobs as $j){
            $clients[] = $j->getAccount()->getId();
        }
        
        if(count($clients) == 0){
            return array();
        }

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account', 'a');
        $where = $queryBuilder->expr()->andX();
        $where->add($queryBuilder->expr()->in('a.id', $clients));
        $queryBuilder->where($where);
        return $queryBuilder->getQuery()->getArrayResult();
    }
    
    public function findClientsRepeatWorkedWith(Account $worker){
        $jobs_worked = $this->getEntityManager()->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($worker);
        $jobs_cancelled = $this->getEntityManager()->getRepository('VlanceJobBundle:Job')->findJobsCancelledBy($worker);
        $jobs = array_merge($jobs_cancelled, $jobs_worked);
        if(count($jobs) == 0){
            return array();
        }
        
        $clients = array();
        foreach($jobs as $j){
            if(!isset($clients[$j->getAccount()->getId()])){
                $clients[$j->getAccount()->getId()] = array(
                    'id'    => $j->getAccount()->getId(),
                    'count' => 1
                );
            } else{
                $clients[$j->getAccount()->getId()]['count'] += 1;
            }
        }
        
        $client_ids = array();
        foreach($clients as $id => $cl){
            if($clients[$id]['count'] > 1){
                $client_ids[] = $id;
            }
        }
        if(count($client_ids) == 0){
            return array();
        }

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a')->from('VlanceAccountBundle:Account', 'a');
        $where = $queryBuilder->expr()->andX();
        $where->add($queryBuilder->expr()->in('a.id', $client_ids));
        $queryBuilder->where($where);
        return $queryBuilder->getQuery()->getArrayResult();
    }
    
    public function findFreelancerWith($service){
        $em = $this->getEntityManager();
        
        $sql_query = 'SELECT a.id, (a.score/a.num_reviews) AS rate, a.num_reviews '
                    . 'FROM account a '
                    . 'LEFT JOIN service_account sa ON sa.account_id = a.id '
                    . 'WHERE sa.service_id = '. $service->getId() . ' '
                    . 'AND a.enabled = 1 AND a.locked=0 AND a.banned_to IS NULL '
                    . 'GROUP BY a.id HAVING (rate > 4 AND num_reviews > 0) OR a.num_reviews = 0 '
                    . 'ORDER BY rate DESC, last_login DESC limit 10';
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Vlance\AccountBundle\Entity\Account', 'a');
        $rsm->addFieldResult('a', 'id', 'id');
        $rsm_query = $this->getEntityManager()->createNativeQuery($sql_query, $rsm);
        $account_ids = $rsm_query->getArrayResult();
        
        // Query all accounts information from db, without change rel_score order
        $acc_array = array();
        foreach($account_ids as $acc)
        {
            $acc_array[] = $acc['id'];
        }
        $account_array = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $acc_array));
        
        $account_tmp = array();
        foreach($account_array as $a){
            $account_tmp[$a->getId()] = $a;
        }
        
        $account_new = array();
        foreach($acc_array as $k => $at){
            $account_new[$k] = $account_tmp[$at];
        }
        // END Query all accounts information from db, without change rel_score order
        
        return $account_new;
    }
    public function getNewAccount(){
        $sql_query = 'SELECT a.id, a.full_name FROM account a WHERE a.created_at > date_sub(now(), interval 15 minute)';
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Vlance\AccountBundle\Entity\Account', 'a');
        $rsm->addFieldResult('a', 'id', 'id');
        $rsm_query = $this->getEntityManager()->createNativeQuery($sql_query, $rsm);
        $account_ids = $rsm_query->getArrayResult();
        
        // Query all accounts information from db, without change rel_score order
        if(count($account_ids) > 0){
            $em = $this->getEntityManager();
            $acc_array = array();
            foreach($account_ids as $acc)
            {
                $acc_array[] = $acc['id'];
            }
            $account_array = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $acc_array));

            $account_tmp = array();
            foreach($account_array as $a){
                $account_tmp[$a->getId()] = $a;
            }

            $account_new = array();
            foreach($acc_array as $k => $at){
                $account_new[$k] = $account_tmp[$at];
            }
        } else {
            return $account_ids;
        }
        // END Query all accounts information from db, without change rel_score order
        
        return $account_new;
    }
    
    public function getTopFreelancerByCategory($id){
        $sql_query = 'SELECT a.id, ( '
                . '(CASE WHEN a.id IN (SELECT id FROM account WHERE category_id = ' . $id . ' ) THEN 1.1 ELSE 1 END) '
                . '*(CASE WHEN (SELECT COUNT(id) FROM job WHERE worker_id = a.id AND category_id = ' . $id . ' AND status = 6) > 0 '
                . 'THEN (SELECT COUNT(id) FROM job WHERE worker_id = a.id AND category_id = ' . $id . ' AND status = 6) * 1.9 ELSE 1 END) '
                . '*(CASE WHEN (SELECT COUNT(id) FROM bid WHERE account_id = a.id) > 0 THEN 1.1 ELSE 1 END)) as rate '
                . 'FROM account a '
                . 'WHERE a.path IS NOT NULL '
                . 'ORDER BY rate DESC limit 6 ';
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Vlance\AccountBundle\Entity\Account', 'a');
        $rsm->addFieldResult('a', 'id', 'id');
        $rsm_query = $this->getEntityManager()->createNativeQuery($sql_query, $rsm);
        $account_ids = $rsm_query->getArrayResult();
        
        // Query all accounts information from db, without change rel_score order
        if(count($account_ids) > 0){
            $em = $this->getEntityManager();
            $acc_array = array();
            foreach($account_ids as $acc)
            {
                $acc_array[] = $acc['id'];
            }
            $account_array = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $acc_array));

            $account_tmp = array();
            foreach($account_array as $a){
                $account_tmp[$a->getId()] = $a;
            }

            $account_new = array();
            foreach($acc_array as $k => $at){
                $account_new[$k] = $account_tmp[$at];
            }
        } else {
            return $account_ids;
        }
        return $account_new;
    }
    
    /** get JSON responses **/
    
    /**
     * 
     * @param type $string
     * @return type
     */
    public function getTypeahead($string)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('a.id', 'a.email')->from('VlanceAccountBundle:Account','a');
        $orCondition = $queryBuilder->expr()->orX();
        $orCondition->add($queryBuilder->expr()->like("a.email", "'%" . $string . "%'"));
        $orCondition->add($queryBuilder->expr()->like("a.id", "'%" . $string . "%'"));
        $queryBuilder->andWhere($orCondition);
        
        $queryBuilder->setMaxResults(11);
        
        return $queryBuilder->getQuery()->getResult();
    }
}
