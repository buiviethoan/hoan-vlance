<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * PersonId
 *
 * @ORM\Table(name="account_person_id")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\PersonIdRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PersonId
{
    const TYPE_GIAY = '1';
    const TYPE_NHUA = '2';
    const TYPE_MOI  = '3';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="personId")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;
    
    /**
     * @var string
     * @ORM\Column(name="full_name", type="string", length=255)
     */
    private $fullName;
    
    /**
     * @var string
     * @ORM\Column(name="id_number", type="string", length=15) 
     */
    private $idNumber;
    
    /**
     * @var \DateTime
     * @Assert\Date()
     * @ORM\Column(name="issued_date", type="date")
     */
    private $issuedDate;
    
    /**
     * @var string
     * @ORM\Column(name="issued_location", type="string")
     */
    private $issuedLocation;
    
    /**
     * @var string
     * @ORM\Column(name="path", type="string")
     */
    private $path;
    
    /**
     * @var float
     * @ORM\Column(name="size_front", type="decimal")
     */
    private $sizeFront;
    
    /**
     * @var float
     * @ORM\Column(name="size_back", type="decimal")
     */
    private $sizeBack;
    
    /**
     * @var string
     * @ORM\Column(name="mime_type_front", type="string")
     */
    private $mimetypeFront;
    
    /**
     * @var string
     * @ORM\Column(name="mime_type_back", type="string")
     */
    private $mimetypeBack;
    
    /**
     * 
     * @Assert\Image(maxSize ="10752k")
     */
    private $fileFront = null;
    
    /**
     * 
     * @Assert\Image(maxSize ="10752k")
     */
    private $fileBack = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="filename_front", type="string", length=255)
     */
    private $filenameFront;
    
    /**
     * @var string
     *
     * @ORM\Column(name="filename_back", type="string", length=255)
     */
    private $filenameBack;
    
     /**
     * @var string
     *
     * @ORM\Column(name="storedname_front", type="string", length=255)
     */
    private $storednameFront;
    
     /**
     * @var string
     *
     * @ORM\Column(name="storedname_back", type="string", length=255)
     */
    private $storednameBack;
    
    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment = null;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="requested_at", type="datetime", nullable=true)
     */
    protected $requestedAt;
    
    /**
     *
     * @var boolean
     * @ORM\Column(name="requested" , type = "boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    private $requested = false;
    
    /**
     *
     * @var boolean
     * @ORM\Column(name="is_verified" , type = "boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    private $isVerified = false;

    /**
     * @var \DateTime
     * @ORM\Column(name="verify_at", type="datetime", nullable=true)
     */
    private $verifyAt;
   
    /**
     * @var string
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('1', '2', '3')")
     */
    protected $type;
    
    protected $path_old = '';
    
    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return PersonId
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    
        return $this;
    }

    /**
     * Get fullName
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Set idNumber
     *
     * @param integer $idNumber
     * @return PersonId
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    
        return $this;
    }

    /**
     * Get idNumber
     *
     * @return integer 
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * Set issuedDate
     *
     * @param \DateTime $issuedDate
     * @return PersonId
     */
    public function setIssuedDate($issuedDate)
    {
        $this->issuedDate = $issuedDate;
    
        return $this;
    }

    /**
     * Get issuedDate
     *
     * @return \DateTime 
     */
    public function getIssuedDate()
    {
        return $this->issuedDate;
    }

    /**
     * Set issuedLocation
     *
     * @param string $issuedLocation
     * @return PersonId
     */
    public function setIssuedLocation($issuedLocation)
    {
        $this->issuedLocation = $issuedLocation;
    
        return $this;
    }

    /**
     * Get issuedLocation
     *
     * @return string 
     */
    public function getIssuedLocation()
    {
        return $this->issuedLocation;
    }

    /**
     * Set fileFront.
     *
     * @param UploadedFile $fileFront
     * @return PersonId
     */
    public function setFileFront(UploadedFile $fileFront = null)
    {
        $this->fileFront = $fileFront;
        if($this->getFileFront() !== null) {
            $this->generateFilename("front");
        }
    }

    /**
     * Get fileFront.
     *
     * @return UploadedFile
     */
    public function getFileFront()
    {
        return $this->fileFront;
    }

    /**
     * Set fileBack.
     *
     * @param UploadedFile $fileBack
     * @return PersonId
     */
    public function setFileBack(UploadedFile $fileBack = null)
    {
        $this->fileBack = $fileBack;
        if($this->getFileBack() !== null) {
            $this->generateFilename("back");
        }
    }

    /**
     * Get fileBack.
     *
     * @return UploadedFile
     */
    public function getFileBack()
    {
        return $this->fileBack;
    }
    
    /**
     * Set comment
     *
     * @param string $comment
     * @return PersonId
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Get comment
     *
     * @return array
     */
    public function getArrayComment()
    {
        $comment = json_decode($this->comment, true);
        return $comment;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return PersonId
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR .DS."personid";
    }
    
    protected function generateFilename($side) {
        if($side == "front"){
            if($this->path && $this->storednameFront){
                $this->path_old = $this->getUploadDir().DS.$this->storednameFront;
            }
            $this->storednameFront = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFileFront()->getClientOriginalExtension();
            $this->path = date('m-y', time());
            $this->filenameFront = $this->getFileFront()->getClientOriginalName();
            $this->sizeFront = $this->getFileFront()->getClientSize();
            $this->mimetypeFront = $this->getFileFront()->getClientMimeType();
        } else if($side == "back"){
            if($this->path && $this->storednameBack){
                $this->path_old = $this->getUploadDir().DS.$this->storednameBack;
            }
            $this->storednameBack = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFileBack()->getClientOriginalExtension();
            $this->path = date('m-y', time());
            $this->filenameBack = $this->getFileBack()->getClientOriginalName();
            $this->sizeBack = $this->getFileBack()->getClientSize();
            $this->mimetypeBack = $this->getFileBack()->getClientMimeType();
        }    
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadFront() {
        if (null === $this->getFileFront()) {
            return;
        }
        if (!file_exists($this->getUploadDir() . DS . $this->path)) {
            mkdir($this->getUploadDir() . DS . $this->path, 0777, true);
        }
        if(file_exists($this->path_old)) {
            @unlink($this->path_old);
        }
        $this->getFileFront()->move($this->getUploadDir() . DS . $this->path, $this->storednameFront);
        $this->fileFront = null;
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadBack() {
        if (null === $this->getFileBack()) {
            return;
        }
        if (!file_exists($this->getUploadDir() . DS . $this->path)) {
            mkdir($this->getUploadDir() . DS . $this->path, 0777, true);
        }
        if(file_exists($this->path_old)) {
            @unlink($this->path_old);
        }
        $this->getFileBack()->move($this->getUploadDir() . DS . $this->path, $this->storednameBack);
        $this->fileBack = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUploadFront()
    {
        if($this->path === null){
            return;
        }
        
        $fileFront = $this->getUploadDir().DS.$this->path.DS.$this->storednameFront;
        if(file_exists($fileFront)) {
            unlink($fileFront);
        }
        $this->path = '';
        $this->filenameFront = '';
        $this->storednameFront = '';
        $this->mimetypeFront = '';
        $this->sizeFront = 0;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUploadBack()
    {
        if($this->path === null){
            return;
        }
               
        $fileBack = $this->getUploadDir().DS.$this->path.DS.$this->storednameBack;
        if(file_exists($fileBack)) {
            unlink($fileBack);
        }
        $this->path = '';
        $this->filenameBack = '';
        $this->storednameBack = '';
        $this->mimetypeBack = '';
        $this->sizeBack = 0;
    }
    
    /**
     * Set path
     *
     * @param string $path
     * @return PersonId
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set filenameFront
     *
     * @param string $filenameFront
     * @return PersonId
     */
    public function setFilenameFront($filenameFront)
    {
        $this->filenameFront = $filenameFront;
    
        return $this;
    }

    /**
     * Get filenameFront
     *
     * @return string 
     */
    public function getFilenameFront()
    {
        return $this->filenameFront;
    }

    /**
     * Set filenameBack
     *
     * @param string $filenameBack
     * @return PersonId
     */
    public function setFilenameBack($filenameBack)
    {
        $this->filenameBack = $filenameBack;
    
        return $this;
    }

    /**
     * Get filenameBack
     *
     * @return string 
     */
    public function getFilenameBack()
    {
        return $this->filenameBack;
    }

    /**
     * Set storednameFront
     *
     * @param string $storednameFront
     * @return PersonId
     */
    public function setStorednameFront($storednameFront)
    {
        $this->storednameFront = $storednameFront;
    
        return $this;
    }

    /**
     * Get storednameFront
     *
     * @return string 
     */
    public function getStorednameFront()
    {
        return $this->storednameFront;
    }

    /**
     * Set storednameBack
     *
     * @param string $storednameBack
     * @return PersonId
     */
    public function setStorednameBack($storednameBack)
    {
        $this->storednameBack = $storednameBack;
    
        return $this;
    }

    /**
     * Get storednameBack
     *
     * @return string 
     */
    public function getStorednameBack()
    {
        return $this->storednameBack;
    }

    /**
     * Set sizeFront
     *
     * @param float $sizeFront
     * @return PersonId
     */
    public function setSizeFront($sizeFront)
    {
        $this->sizeFront = $sizeFront;
    
        return $this;
    }

    /**
     * Get sizeFront
     *
     * @return float 
     */
    public function getSizeFront()
    {
        return $this->sizeFront;
    }

    /**
     * Set sizeBack
     *
     * @param float $sizeBack
     * @return PersonId
     */
    public function setSizeBack($sizeBack)
    {
        $this->sizeBack = $sizeBack;
    
        return $this;
    }

    /**
     * Get sizeBack
     *
     * @return float 
     */
    public function getSizeBack()
    {
        return $this->sizeBack;
    }

    /**
     * Set mimetypeFront
     *
     * @param string $mimetypeFront
     * @return PersonId
     */
    public function setMimetypeFront($mimetypeFront)
    {
        $this->mimetypeFront = $mimetypeFront;
    
        return $this;
    }

    /**
     * Get mimetypeFront
     *
     * @return string 
     */
    public function getMimetypeFront()
    {
        return $this->mimetypeFront;
    }

    /**
     * Set mimetypeBack
     *
     * @param string $mimetypeBack
     * @return PersonId
     */
    public function setMimetypeBack($mimetypeBack)
    {
        $this->mimetypeBack = $mimetypeBack;
    
        return $this;
    }

    /**
     * Get mimetypeBack
     *
     * @return string 
     */
    public function getMimetypeBack()
    {
        return $this->mimetypeBack;
    }

    /**
     * Set verifyAt
     *
     * @param \DateTime $verifyAt
     * @return PersonId
     */
    public function setVerifyAt($verifyAt)
    {
        $this->verifyAt = $verifyAt;
    
        return $this;
    }

    /**
     * Get verifyAt
     *
     * @return \DateTime 
     */
    public function getVerifyAt()
    {
        return $this->verifyAt;
    }

    /**
     * Set requested
     *
     * @param string $requested
     * @return PersonId
     */
    public function setRequested($requested)
    {
        $this->requested = $requested;
    
        return $this;
    }

    /**
     * Get requested
     *
     * @return string 
     */
    public function getRequested()
    {
        return $this->requested;
    }

    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     * @return PersonId
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    
        return $this;
    }

    /**
     * Get isVerified
     *
     * @return boolean 
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PersonId
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PersonId
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set requestedAt
     *
     * @param \DateTime $requestedAt
     * @return PersonId
     */
    public function setRequestedAt($requestedAt)
    {
        $this->requestedAt = $requestedAt;
    
        return $this;
    }

    /**
     * Get requestedAt
     *
     * @return \DateTime 
     */
    public function getRequestedAt()
    {
        return $this->requestedAt;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PersonId
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
}