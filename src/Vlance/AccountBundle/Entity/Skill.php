<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Skill
 *
 * @ORM\Table(name="skill")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\SkillRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title")
 */
class Skill
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique = true, nullable=true)
     */
    protected $title = null;
    
    /**
     * @var string
     * 
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Account", mappedBy="skills")
     */
    private $accounts;

    /**
     * @ORM\ManyToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="skills")
     **/
    private $jobs;
    
    public function __construct() {
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Skill
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set id
     * @param integer $id
     * @return Skill
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * Add jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     * @return Skill
     */
    public function addJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     * @return Skill
     */
    public function addAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts[] = $accounts;
    
        return $this;
    }

    /**
     * Remove accounts
     *
     * @param \Vlance\AccountBundle\Entity\Account $accounts
     */
    public function removeAccount(\Vlance\AccountBundle\Entity\Account $accounts)
    {
        $this->accounts->removeElement($accounts);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
    
    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash() {
        return $this->hash;
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return Skill
     */
    public function setHash($hash) {
        $this->hash = $hash;
    }



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Skill
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Skill
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}