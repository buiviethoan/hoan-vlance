<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vlance\AccountBundle\Entity\Skill;

/**
 * Portfolio
 *
 * @ORM\Table(name="portfolio")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\PortfolioRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Portfolio
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="portfolios")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $account;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title = null;
    
    /**
     * @Assert\File(maxSize = "6144k")
     * 
     */
    private $file;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path;
    
    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;
    
     /**
     * @var string
     *
     * @ORM\Column(name="stored_name", type="string", length=255)
     */
    protected $storedName;
    
     /**
     * @var string
     * @ORM\Column(name="mime_type", type="string")
     */
    protected $mimeType;

    /**
     * @var float
     * @ORM\Column(name="size", type="decimal")
     */
    protected $size;
    
    protected $path_old = '';
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Service", inversedBy="portfolios")
     * @ORM\JoinTable(name="service_portfolio",
     * joinColumns={@ORM\JoinColumn(name="portfolio_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $services;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }
   
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Porfolio
     */
    
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Porfolio
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Porfolio
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return Porfolio
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
    
     /**
     * Sets file.
     *
     * @param UploadedFile $file
     * @return Fortfolio
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if($this->getFile() !== null) {
            $this->generateFilename();
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
     /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return File
     */
    public function setMimeType($mimeType) {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string 
     */
    public function getMimeType() {
        return $this->mimeType;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR .DS."portfolio";
    }
    
   /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL .DS."portfolio";
    }
    
    protected function generateFilename() {
        if($this->path && $this->storedName) {
            $this->path_old = $this->getUploadDir().DS.$this->path.DS.$this->storedName;
        }
        $this->storedName = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->getClientOriginalExtension();
        $this->path = date('m-Y', time());
        $this->filename = $this->getFile()->getClientOriginalName();
        $this->size = $this->getFile()->getClientSize();
        $this->mimeType = $this->getFile()->getClientMimeType();
    }
 
     /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        
        if (null === $this->getFile()) {
                return;
         }
        if (!file_exists($this->getUploadDir() . DS . $this->path)) {
                mkdir($this->getUploadDir() . DS . $this->path, 0777, true);
        }
        if(file_exists($this->path_old)) {
            @unlink($this->path_old);
        }
        $this->getFile()->move($this->getUploadDir() . DS . $this->path, $this->storedName);
        $this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        
        if($this->path === null) {
            return;
        }
        $file = $this->getUploadDir().DS.$this->path.DS.$this->storedName;
        if(file_exists($file)) {
            unlink($file);
        }
        $this->path = '';
        $this->filename = '';
        $this->storedName = '';
        $this->mimeType = '';
        $this->size = 0;
    }

    /**
     * Set path
     * 
     * @param string $path
     * @return Portfolio
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set filename
     * 
     * @param string $filename
     * @return Portfolio
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set storedName
     *
     * @param string $storedName
     * @return Portfolio
     */
    public function setStoredName($storedName)
    {
        $this->storedName = $storedName;
    
        return $this;
    }

    /**
     * Get storedName
     *
     * @return string 
     */
    public function getStoredName()
    {
        return $this->storedName;
    }
    
    /**
     * Get full storedName
     *
     * @return string 
     */
    public function getFullStoredName()
    {
        return $this->getUploadDir() . DS . $this->getPath() . DS . $this->storedName;
    }
    
    /**
     * Set size
     *
     * @param float $size
     * @return Portfolio
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return float 
     */
    public function getSize()
    {
        return $this->size;
    }
    

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Portfolio
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Portfolio
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     * @return Portfolio
     */
    public function addService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    
        return $this;
    }

    /**
     * Remove services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     */
    public function removeService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }
}