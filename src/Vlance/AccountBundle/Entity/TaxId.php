<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TaxId
 *
 * @ORM\Table(name="account_tax_id")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\TaxIdRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaxId
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="taxId")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;
    
    /**
     * @var string
     * @ORM\Column(name="tax_id", type="string", length=15) 
     */
    private $idNumber;
    
    /**
     * @var string
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment = null;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="requested_at", type="datetime", nullable=true)
     */
    protected $requestedAt;
    
    /**
     *
     * @var boolean
     * @ORM\Column(name="requested" , type = "boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    private $requested = false;
    
    /**
     *
     * @var boolean
     * @ORM\Column(name="is_verified" , type = "boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    private $isVerified = false;

    /**
     * @var \DateTime
     * @ORM\Column(name="verified_at", type="datetime", nullable=true)
     */
    private $verifiedAt;
    
    public function __construct()
    {
        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idNumber
     *
     * @param string $idNumber
     * @return TaxId
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    
        return $this;
    }

    /**
     * Get idNumber
     *
     * @return string 
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return TaxId
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return TaxId
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return TaxId
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set requestedAt
     *
     * @param \DateTime $requestedAt
     * @return TaxId
     */
    public function setRequestedAt($requestedAt)
    {
        $this->requestedAt = $requestedAt;
    
        return $this;
    }

    /**
     * Get requestedAt
     *
     * @return \DateTime 
     */
    public function getRequestedAt()
    {
        return $this->requestedAt;
    }

    /**
     * Set requested
     *
     * @param boolean $requested
     * @return TaxId
     */
    public function setRequested($requested)
    {
        $this->requested = $requested;
    
        return $this;
    }

    /**
     * Get requested
     *
     * @return boolean 
     */
    public function getRequested()
    {
        return $this->requested;
    }

    /**
     * Set isVerified
     *
     * @param boolean $isVerified
     * @return TaxId
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
    
        return $this;
    }

    /**
     * Get isVerified
     *
     * @return boolean 
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * Set verifiedAt
     *
     * @param \DateTime $verifiedAt
     * @return TaxId
     */
    public function setVerifiedAt($verifiedAt)
    {
        $this->verifiedAt = $verifiedAt;
    
        return $this;
    }

    /**
     * Get verifiedAt
     *
     * @return \DateTime 
     */
    public function getVerifiedAt()
    {
        return $this->verifiedAt;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return TaxId
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
}