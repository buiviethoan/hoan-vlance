<?php

namespace Vlance\AccountBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vlance\AccountBundle\Entity\Skill;

/**
 * Portfolio
 *
 * @ORM\Table(name="email_invite")
 * @ORM\Entity(repositoryClass="Vlance\AccountBundle\Entity\EmailInviteRepository")
 */
class EmailInvite
{
    const EMAIL_INVITE_NEW = 0;
    const EMAIL_INVITE_SENT = 1;
    const EMAIL_INVITE_ERROR = 2;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $account;
    
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email = null;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="verified", type="integer", options={"default" : "0"})
     */
    private $verified = false;
    
    /**
     * @var string
     *
     * @ORM\Column(name="messageId", type="string", nullable=true)
     */
    private $messageId = null;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="lastsent_at", type="datetime")
     */
    protected $lastsentAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EmailInvite
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set verified
     *
     * @param int $verified
     * @return EmailInvite
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
    
        return $this;
    }

    /**
     * Get verified
     *
     * @return int 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return EmailInvite
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
    
    /**
     * Get messageId
     *
     * @return string 
     */
    public function getMessageId() {
        return $this->messageId;
    }
    
    /**
     * Set messageId
     *
     * @param string $messageId
     * @return EmailInvite
     */
    public function setMessageId($messageId) {
        $this->messageId = $messageId;
    }
    
    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    /**
     * Get lastsentAt
     *
     * @return \DateTime 
     */
    public function getLastsentAt() {
        return $this->lastsentAt;
    }
    
     /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailInvite
     */
    public function setCreatedAt(\DateTime $createdAt) {
        $this->createdAt = $createdAt;
    }
    
     /**
     * Set lastsentAt
     *
     * @param \DateTime $lastsentAt
     * @return EmailInvite
     */
    public function setLastsentAt(\DateTime $lastsentAt) {
        $this->lastsentAt = $lastsentAt;
    }

}