<?php

namespace Vlance\AccountBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper as BaseHelper;
use Doctrine\ORM\EntityManager;

class Helper extends BaseHelper {
    protected $_em;
    protected $_container;
    
    public function __construct(ContainerInterface $container, EntityManager $em) {
        $this->_em = $em;
        $this->_container = $container;
    }
    
    public function getName() {
        return 'vlance_account';
    }
    
    public function getNumberClient(\Vlance\AccountBundle\Entity\Account $account) {
        
    }
    
    /**
     * Retrieve Category list
     * 
     * @return array
     */
    public function getCategoryList(){
        $repo = $this->_em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $list = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                $list[$category->getTitle()][$sub_cat->getId()] = $sub_cat;
            }
        }
        return $list;
    } 
    
    /**
     * Retrieve Category list
     * 
     * @return array
     */
    public function getCategoryAdminList(){
        $repo = $this->_em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $list = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                $list[] = $sub_cat;
            }
        }
        return $list;
    } 
    
    /**
     * Category list for contest
     * 
     * @return array
     */
    public function getCategoryContest(){
        $repo = $this->_em->getRepository('VlanceAccountBundle:Category');
        $list = $repo->findBy(array('contest' => 1));
        
        return $list;
    }
    
    /**
     * Retrieve Service list
     * 
     * @return array
     */
    public function getServiceList($other_value = false){
        $repo = $this->_em->getRepository('VlanceAccountBundle:Service');
        $services = $repo->findBy(array(), array('title' => 'ASC'));
        
        $list = array();
        foreach($services as $sv){
            $list[$sv->getTitle()] = $sv;
        }
        return $list;
    } 
    
    /**
     * Service list for contest
     * 
     * @return array
     */
    public function getServiceContest(){
        $repo = $this->_em->getRepository('VlanceAccountBundle:Service');
        $list = $repo->findBy(array('contest' => 1));
        
        return $list;
    }
    
    public function emailNotFacebook($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == true) {
            $pos = strpos($email, "@");
            $string = substr($email, $pos+1); 
            if($string == "facebook.com"){
                return false;
            }
            return true;
        }
        return false;
    }
    
    public function getTimeVerifyPersonId($time_request){
        //CN = 0, Monday = 1 ...
        $day    = (int)date('w', strtotime($time_request->format('d-m-Y')));
        $hour   = (int)$time_request->format('H');
        
        if($day == 0 || $day == 6){ //T7 or CN
            $mess = "11 giờ tuần sau";
        } else {
            if($hour < 11){
                $mess = "11 giờ hôm nay";
            } elseif($hour >= 11 && $hour < 17) {
                $mess = "17 giờ hôm nay";
            } else {
                $mess = "17 giờ ngày mai";
            }
        }
        return $mess;
    }
}