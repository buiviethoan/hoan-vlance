<?php

namespace Vlance\AccountBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;

class AccountListener implements EventSubscriberInterface
{
    private $_router;

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $_container;

    public function __construct(ContainerInterface $container, Router $router)
    {
        $this->_container = $container;
        $this->_router = $router;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onUpdateAccount', 18)),
        );
    }

    public function onUpdateAccount(GetResponseEvent $event)
    {
        if($event->getRequestType() == HttpKernelInterface::SUB_REQUEST) {
            return;
        } 
        $accountRouteName = 'account_login_after';
        $routeName = $this->_container->get('request')->get('_route');
        
        if ($routeName == $accountRouteName || $routeName = 'email_invite' || $routeName == 'account_update_info' || (strstr($routeName, 'vlance_base_block') !== false)) {
            return;
        }
        $parentRoute = $this->_container->get('request')->get('routeName');
        if ($parentRoute == $accountRouteName) {
            return;
        }
        
        if (false === $this->_container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return;
        } else {
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $account = $this->_container->get('security.context')->getToken()->getUser();
            if ($account->getType() == \Vlance\AccountBundle\Entity\Account::TYPE_VLANCE_SYSTEM || $account->getType() == \Vlance\AccountBundle\Entity\Account::TYPE_VLANCE_ADMIN) {
                return;
            }
            $cat = $account->getCategory();
            $type = $account->getType();
            $city = $account->getCity();
            if(!is_object($cat) || !is_object($city) || !$type) {
                $response = new RedirectResponse($this->_router->generate($accountRouteName));
                $this->_container->get('session')->getFlashBag()->clear();
                $this->_container->get('session')->getFlashBag()->add('notice', 'Bạn cần cập nhật thông tin tài khoản để chúng tôi hỗ trợ bạn tốt hơn.');
                $event->setResponse($response);
            }
            return;
        }
    }
    
}
