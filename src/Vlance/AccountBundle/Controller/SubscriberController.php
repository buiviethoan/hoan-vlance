<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\AccountBundle\Entity\Subscriber;

/**
 * Subscriber controller.
 * 
 * @Route("/subscriber")
 */
class SubscriberController extends Controller {
    /**
     * 
     * @Route("/signup/{source}", name="account_subscriber_email")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function signupAction(Request $request, $source)
    {
        $referer = $this->generateUrl('vlance_homepage');

        try{
            $em = $this->getDoctrine()->getManager();
            $subscripber = new Subscriber();
            $subscripber->setEmail($request->query->get('email'));
            $subscripber->setSource($source);
            $em->persist($subscripber);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.subscriber.signup.success', array(), 'vlance'));
        } catch(\Exception $e){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.subscriber.signup.error', array(), 'vlance'));
        }
        
        return $this->redirect($referer);
    }

}
