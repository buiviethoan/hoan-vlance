<?php
namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Vlance\AccountBundle\Entity\Account;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;

/**
 * Linkin controller.
 * 
 * @Route("/linkedin")
 */
class LinkedinController extends Controller {
     /**
     * 
     * @Route("/login", name="login_linkedin")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function loginLinkedinAction(Request $request){
        $api_key    = $this->container->parameters['vlance_system']['linkedin']['api_key'];
        $secret_key = $this->container->parameters['vlance_system']['linkedin']['secret_key'];
        // $state      = $this->container->parameters['vlance_system']['linkedin']['state'];
        $referer    = $request->headers->get('referer');
        
        if($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
        
        // Step 1: Got authentication code,
        if($request->query->get('error')) {
            // LinkedIn returned an error
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.linkedin.error',array(),'vlance'));
            return new RedirectResponse($referer);
        }
        
        if($request->query->get('code')){
            try {
                $this->_log("Got code", $request->query->get('code'));
                $data = array(
                    'grant_type'    => 'authorization_code',
                    'code'          => $request->query->get('code'), 
                    'redirect_uri'  => $this->container->get('router')->generate('login_linkedin', array(), true),
                    'client_id'     => $api_key,
                    'client_secret' => $secret_key,
                );

                $this->_log(print_r($data));
                $json_res = $this->execPostRequest('https://www.linkedin.com/oauth/v2/accessToken', $data);
                $this->_log("JSON res", $json_res);
                
                $res = json_decode($json_res, true);
                
                $access_token = $res['access_token'];
                $profile = $this->fetch('GET', $access_token, '/v1/people/~:(id,email-address,first-name,last-name,headline,summary,picture-url,public-profile-url,specialties,positions,site-standard-profile-request,industry,location)?format=json');
                $this->_log("User data", $profile);
                
                $em = $this->getDoctrine()->getManager();
                /* @var $entity \Vlance\AccountBundle\Entity\Account */
                
                /* Validating $profile */
                // email
                if(!isset($profile->emailAddress)){
                    $this->_log("Khong lay duoc email");
                    return new RedirectResponse($referer);
                }
                $email = $profile->emailAddress;
                if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
                    $this->_log("Email khong hop le", $email);
                    return new RedirectResponse($referer);
                }                
                $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($email);
                if(!$entity){
                    $this->_log("Khong tim duoc account co email phu hop", $email);
                    $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneByUsername($email);
                }
                
                // id
                if(!isset($profile->id)){
                    $this->_log("Khong lay duoc id");
                    return new RedirectResponse($referer);
                }
                $linkedinId = $profile->id;
                if(!$entity){
                    $this->_log("Khong tim duoc account co username phu hop", $email);
                    $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneByLinkedinId($linkedinId);
                }
                
                // fullname
                $name = array();
                if(isset($profile->lastName)){
                    $name[] = $profile->lastName;
                }
                if(isset($profile->firstName)){
                    $name[] = $profile->firstName;
                }
                $fullname = implode(" ", $name);
                
                /* In case cannot find the account, then create new one */ 
                if(!$entity){
                    $this->_log("Khong tim duoc account co linkedinId phu hop", $linkedinId);
                    
                    // Need to create new account into the system
                    $user = new Account();
                    $user->setLinkedinId($linkedinId);
                    $user->setAccessTokenLink($access_token);
                    $user->setFullName($fullname);
                    $user->setEmail($email);
                    $user->setUsername($email);
                    $user->setTitle(isset($user->headline) ? $user->headline : "");
                    $user->setDescription(isset($user->summary) ? $user->summary : "");
                    $user->setWebsite(isset($user->publicProfileUrl) ? $user->publicProfileUrl : "");
                    $user->setPlainPassword($linkedinId);
                    $user->setEnabled(true);
                    
                    $em->persist($user);
                    $em->flush();
                    
                    /** Tracking Event "Registration" **/
                    // Mixpanel
                    $this->get('vlance_base.helper')->trackAll($user, "Registration", array("channel" => "Linkedin"));
                    // Google Adwords
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Google Code for Registration Conversion Page -->
                            <script type="text/javascript">
                            /* <![CDATA[ */
                            var google_conversion_id = 925105271;
                            var google_conversion_language = "en";
                            var google_conversion_format = "3";
                            var google_conversion_color = "ffffff";
                            var google_conversion_label = "hRPxCO68hFoQ9_iPuQM";
                            var google_remarketing_only = false;
                            /* ]]> */
                            </script>
                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                            </script>
                            <noscript>
                            <div style="display:inline;">
                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?label=hRPxCO68hFoQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                            </div>
                            </noscript>'
                        )
                    ));
                    // Facebook
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Facebook Conversion Code for Registrations -->
                            <script>(function() {
                              var _fbq = window._fbq || (window._fbq = []);
                              if (!_fbq.loaded) {
                                var fbds = document.createElement("script");
                                fbds.async = true;
                                fbds.src = "//connect.facebook.net/en_US/fbds.js";
                                var s = document.getElementsByTagName("script")[0];
                                s.parentNode.insertBefore(fbds, s);
                                _fbq.loaded = true;
                              }
                            })();
                            window._fbq = window._fbq || [];
                            window._fbq.push(["track", "6023541940285", {"value":"0.00","currency":"VND"}]);
                            </script>
                            <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023541940285&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                        )
                    ));
                    // GA
                    @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=account&ea=register&el=linkedin&cs=web&cm=none&cn=none');
                    /** END Tracking Event "Registration" **/

                    $_SESSION['after_new_acc'] = TRUE;
                    
                    // Call event REGISTRATION_COMPLETED which is used to create user cash
                    /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                    $dispatcher = $this->container->get('event_dispatcher');
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $this->getRequest(), new Response()));
                    
                    // Do the login into recently created account
                    $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
                    $this->get("security.context")->setToken($token);

                    // Fire the login event
                    // Logging the user in above the way we do it doesn't do this automatically
                    $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);
                }
                /* In case the account exists */
                else {
                    $entity->setLinkedinId($linkedinId);
                    $entity->setAccessTokenLink($access_token);
                    $em->persist($entity);
                    $em->flush();
                    
                    $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.linkedin.success',array(),'vlance'));

                    // Do the login into recently created account
                    $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());
                    $this->get("security.context")->setToken($token);

                    // Fire the login event
                    // Logging the user in above the way we do it doesn't do this automatically
                    $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);
                }
                
                // Redirect to after login page
                return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));                
            } catch (\Exception $e) {
               // LinkedIn returned an error
                $this->_log("Create linkedin error", $e->getMessage());
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.linkedin.error',array(),'vlance'));
                return new RedirectResponse($referer);
            }
        }
        
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.linkedin.error', array(), 'vlance'));
        // Redirect user to his account's profile page
        return new RedirectResponse($referer);
    }
    
    /**
     * Execute the post request
     *
     * @Method("POST")
     */
    protected function execPostRequest($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->encapDataRequest($data));
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);        
        curl_close($ch);
        $this->_log("$result", $result);
        return $result;
    }
    
    /**
     * Encapsulate data
     */
    protected function encapDataRequest($data)
    {
        $data_str = "";
        foreach ($data as $key => $val){
            $data_str .= ($data_str=="" ? "" : "&") . $key . "=" . $val;
        }
        
        return $data_str;
    }
    
    function fetch($method, $token, $resource, $body = '')
    {
        $opts = array(
            'http' => array(
                'method' => $method,
                'header' => "Authorization: Bearer " . 
                $token . "\r\n" . 
                "x-li-format: json\r\n"
            )
        );
        $url = 'https://api.linkedin.com' . $resource;
        $context = stream_context_create($opts);
        $response = file_get_contents($url, false, $context);
        return json_decode($response);
    }
    
    protected function _log($message, $object = null, $type = 'info')
    {
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger = $this->get("monolog.logger.linkedin");
        if(!in_array($type, array('info', 'error', 'critical', 'debug'))){
            $type = 'info';
        }
        
        if($message && $object){
            $logger->$type($message . ": " . print_r($object, true));
        } else{
            $logger->$type($message);
        }
    }
}
