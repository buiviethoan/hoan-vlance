<?php
namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Common\Persistence\Event\PreUpdateEventArgs;
use Pagerfanta\View\DefaultView;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\PersonId;
use Vlance\JobBundle\Form\FilterStatusType;
use Vlance\JobBundle\Entity\MessageFile;
use Vlance\AccountBundle\Entity\Category;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\BaseBundle\Event\VlanceEvents;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;
use Vlance\JobBundle\Entity\Message;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\BaseBundle\Utils\HasPermission;

/**
 * Admin Account controller.
 *
 * @Route("/")
 */
class AdminAccountController extends Controller {
    /**
     * @Route("/admin-account/block/dashboard/{_route_params}", name="admin_account_block_dashboard")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Admin:dashboard.html.php",engine="php")
     */
    public function dashboardAction($_route_params) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        $_params = unserialize($_route_params['_route_params']);
        
        if(isset($_params["hash"])){
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $acc = $em->getRepository("VlanceAccountBundle:Account")->findOneByHash($_params["hash"]);
            if (!$acc) {
                return new Response("");
            }

            $vips = $em->getRepository('VlanceAccountBundle:AccountVIP')->findBy(array('account' => $acc));
            
            return array(
                'acc'           => $acc,
                'vips'          => $vips,
                '_route_params' => $_route_params
            );
        }
        return new Response("");
    }
}
