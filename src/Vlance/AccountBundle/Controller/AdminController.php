<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Pagerfanta\View\DefaultView;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\AccountVIP;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\PaymentBundle\Entity\UserCash;
use Vlance\PaymentBundle\Entity\OnePay;
use Vlance\PaymentBundle\Entity\Transaction;

/**
 * Admin controller.
 * 
 * @Route("/")
 */
class AdminController extends Controller {
    /**
     * 
     * @Route("/admin/dashboard", name="admin_dashboard")
     * @Method("GET")
     * @Template("VlanceBaseBundle:Block/admin:dashboard.html.php",engine="php")
     */
    public function dashboardAction(Request $request) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }
        
        return array();
    }

    /**
     * @Route("/admin/upgrade-vip/accout}", name="submit_admin_account")
     * @Method("POST")
     * @Template("VlanceBaseBundle:Block/admin:dashboard.html.php",engine="php")
     */
    public function getAccountAction(Request $request) {
        $acc_login = $this->get('security.context')->getToken()->getUser();
        if(!is_object($acc_login)){
            return new JsonResponse(array('error' => 101));
        }
        
        if(\Vlance\BaseBundle\Utils\HasPermission::hasAdminPermission($acc_login) === FALSE){
            return new JsonResponse(array('error' => 102));
        }
        
        $value = $request->request->get('account');
        if($value === ""){
            return new JsonResponse(array('error' => 103));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $account = "";
        if(filter_var($value, FILTER_VALIDATE_EMAIL)){
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email' => $value));
        } else {
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $value));
        }
        
        if((!is_object($account)) || $account === ""){
            return new JsonResponse(array('error' => 104));
        }
        
        $now =  new \DateTime('now');
        $color = '#27ff27';
        if(!is_null($account->getExpiredDateVip())){
            if((JobCalculator::ago($account->getExpiredDateVip(), $now)) === false){
                $color = 'red';
            }
        } else {
            $color = 'red';
        }
        
        return new JsonResponse(array(
            'error'     => 0,
            'time'      => !is_null($account->getExpiredDateVip()) ? date_format($account->getExpiredDateVip(), 'd/m/Y H:i:s') : "NULL",
            'type'      => !is_null($account->getTypeVIP()) ? $account->getTypeVip() : "NULL",
            'category'  => !is_null($account->getCategoryVIP()) ? $account->getCategoryVIP()->getTitle() : "NULL",
            'color'     => $color
        ));
    }

    /**
     * @Route("/admin/upgrade-vip-cancel/submit}", name="submit_admin_upgrade_cancel")
     * @Method("POST")
     * @Template("VlanceBaseBundle:Block/admin:dashboard.html.php",engine="php")
     */
    public function submitAdminUpgradeVIPCancelAction(Request $request) {
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        if(!is_object($acc_login)){
            return new JsonResponse(array('error' => 101));
        }
        
        if(\Vlance\BaseBundle\Utils\HasPermission::hasAdminPermission($acc_login) === FALSE){
            return new JsonResponse(array('error' => 102));
        }
        
        $value = $request->request->get('account');
        if($value === ""){
            return new JsonResponse(array('error' => 103));
        }
                
        $account = "";
        if(filter_var($value, FILTER_VALIDATE_EMAIL)){
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email' => $value));
        } else {
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $value));
        }
        
        if((!is_object($account)) || $account === ""){
            return new JsonResponse(array('error' => 104));
        }
        
        $acc_vip = $em->getRepository('VlanceAccountBundle:AccountVIP')->findOneBy(array('account' => $account->getId()), array('id' => 'DESC'));
        if(!is_object($acc_vip)){
            return new JsonResponse(array('error' => 105));
        }
        
        $helper = $this->get('vlance_payment.helper');
        $service_id = OnePay::SERV_OTP_UPGRADE_ACCOUNT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $acc_vip->getPackage());
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $acc_vip->getPackage() . '.amount'));
        $package['qty'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $acc_vip->getPackage() . '.qty'));
        
        if($acc_vip->getPrice() !== $package['amount']){
            return new JsonResponse(array('error' => 106));
        }
        
        /* @var $usercash \Vlance\PaymentBundle\Entity\UserCash */
        $usercash = $em->getRepository('VlancePaymentBundle:UserCash')->findOneBy(array('id' => $account->getCash()->getId()));
        if(!$usercash->getId()){
            return new JsonResponse(array('error' => 107));
        }
        
        $isRefund = 'not';
        if($request->request->get('refund') != ""){
            $isRefund = $request->request->get('refund');
        }
        
        $account->setTypeVIP(NULL);
        $account->setCategoryVIP(NULL);
        $account->setExpiredDateVip(NULL);
        $em->persist($account);
        
        if($isRefund == 'refund'){
            $transaction = new Transaction();
            $transaction->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $transaction->setReceiver($account);
            $transaction->setAmount($package['amount']);
            $transaction->setComment('Hoàn tiền mua gói VIP' .$package['amount'] . '');
            $transaction->setType(Transaction::TRANS_TYPE_REFUND);
            $transaction->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($transaction);

            $usercash_balance = $usercash->getBalance() + $transaction->getAmount();
            $usercash->setBalance($usercash_balance);
            $em->persist($usercash);
        }
            
        $em->flush();
        
        $from_vlance = $this->container->getParameter('from_email');
        try{
            $this->get('vlance_base.helper')->trackAll(NULL, 'Cancel VIP');

            // Send confirmation mail to payer
            $context_payer = array(
                'amount'        => number_format($acc_vip->getPrice(),0,',','.'),
                'account'       => $account,
                'isRefund'      => $isRefund,
            );
            $tpl_payer = 'VlanceAccountBundle:Email/Upgrade:freelancer_upgrade_vip_cancel.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $account->getEmail());
            
            // Send confirmation mail to Admin
            $context_admin = array(
                'amount'        => number_format($acc_vip->getPrice(),0,',','.'),
                'AV'            => $acc_vip,
            );
            $admin_tpl_payer = 'VlanceAccountBundle:Email/Upgrade:admin_upgrade_vip_cancel.html.twig';
            $admin1 = "tuan.tran@vlance.vn";
            $admin3 = "vlanceantispam@gmail.com";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin1);
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin3);
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 301));
        }
        
        return new JsonResponse(array(
            'error'     => 0,
            'time'      => !is_null($account->getExpiredDateVip()) ? date_format($account->getExpiredDateVip(), 'd/m/Y H:i:s') : "NULL",
            'type'      => !is_null($account->getTypeVIP()) ? $account->getTypeVip() : "NULL",
            'category'  => !is_null($account->getCategoryVIP()) ? $account->getCategoryVIP()->getTitle() : "NULL",
            'color'     => 'red',
            'refund'    => $isRefund
        ));
    }

    /**
     * @Route("/admin/upgrade-vip/submit}", name="submit_admin_upgrade_code")
     * @Method("POST")
     * @Template("VlanceBaseBundle:Block/admin:dashboard.html.php",engine="php")
     */
    public function submitAdminUpgradeVIPAction(Request $request) {
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        if(!is_object($acc_login)){
            return new JsonResponse(array('error' => 101));
        }
        
        if(\Vlance\BaseBundle\Utils\HasPermission::hasAdminPermission($acc_login) === FALSE){
            return new JsonResponse(array('error' => 102));
        }
        
        $value = $request->request->get('account');
        if($value === ""){
            return new JsonResponse(array('error' => 103));
        }
        
        $package_id = $request->request->get('package');
        if($package_id === ""){
            return new JsonResponse(array('error' => 104));
        }
        
        $method = $request->request->get('method');
        if($method === ""){
            return new JsonResponse(array('error' => 105));
        }
        
        $cat = $request->request->get('category');
        if($cat === ""){
            $cat = NULL;
        }
        if(!is_null($cat)){
            $category = $em->getRepository('VlanceAccountBundle:Category')->findOneById($cat);
            if(!is_object($category)){
                return new JsonResponse(array('error' => 106));
            }
        }
        
        $account = "";
        if(filter_var($value, FILTER_VALIDATE_EMAIL)){
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email' => $value));
        } else {
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $value));
        }
        
        if((!is_object($account)) || $account === ""){
            return new JsonResponse(array('error' => 107));
        }
        
        $helper = $this->get('vlance_payment.helper');
        $service_id = OnePay::SERV_OTP_UPGRADE_ACCOUNT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        $package['qty'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.qty'));
        
        $userCash = $account->getCash();
        if($method == AccountVIP::PAYMENT_METHOD_USERCASH){
            if($userCash->getActualBalance() < $package['amount']){
                return new JsonResponse(array('error' => 108));
            }
            
            $usercash_balance = $userCash->getBalance() - $package['amount'];
            $userCash->setBalance($usercash_balance);
            $em->persist($userCash);
        }
        
        $acc_vip = new AccountVIP();
        $acc_vip->setAccount($account);
        $acc_vip->setPackage($package_id);
        $acc_vip->setPrice($package['amount']);
        $acc_vip->setPaymentMethod($method);
        $em->persist($acc_vip);
        
        $transaction = new Transaction();
        $transaction->setSender($account);
        $transaction->setAmount($package['amount']);
        $transaction->setType(Transaction::TRANS_TYPE_UPGRADE_VIP);
        $transaction->setStatus(Transaction::TRANS_TERMINATED);
        $em->persist($transaction);
        
        if($package_id > 3){
            $account->setTypeVIP(Account::TYPE_VIP_MINI);
        } else {
            $account->setTypeVIP(Account::TYPE_VIP_NORMAL);
        }
        $account->setCategoryVIP(NULL);
        $em->persist($account);
        
        if((int)$package_id > 3){
            $transaction->setComment('Nâng cấp MINI VIP ' . $package['amount'] . 'VNĐ');
        } else {
            $transaction->setComment('Nâng cấp VIP ' . $package['amount'] . 'VNĐ');
        }
        $em->persist($transaction);
        
        $now = new \DateTime('now');
        if(is_null($account->getExpiredDateVip())){
//            if(!is_null($cat)){
//                $account->setCategoryVIP($category);
//            }
            $expired = $now->modify('+'.$package['qty'].' months');    
            $account->setExpiredDateVip($expired);
            $em->persist($account);
        } else {
            if(JobCalculator::ago($account->getExpiredDateVip(), $now) === false){//Da het han
//                if(!is_null($cat)){
//                    $account->setCategoryVIP($category);
//                }
                $expired = $now->modify('+'.$package['qty'].' months');
                $account->setExpiredDateVip($expired);
                $em->persist($account);
            } else {//Van con han
//                if(!is_null($cat)){
//                    $account->setCategoryVIP($category);
//                }
                $account->setExpiredDateVip($account->getExpiredDateVip()->modify('+'.$package['qty'].' months'));
                $em->persist($account);
            }
        }
        $em->flush();
        
        $from_vlance = $this->container->getParameter('from_email');
        try{
            // Send tracking event to Mixpanel
            $tracking_event_name = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.tracking_event');
            $this->get('vlance_base.helper')->trackAll(NULL, $tracking_event_name);
            // End Mixpanel

            // Send confirmation mail to payer
            $context_payer = array(
                'amount'        => number_format($acc_vip->getPrice(),0,',','.'),
                'account'       => $account,
            );
            $tpl_payer = 'VlanceAccountBundle:Email/Upgrade:freelancer_upgrade_vip.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $account->getEmail());
            
            // Send confirmation mail to Admin
            $context_admin = array(
                'amount'        => number_format($acc_vip->getPrice(),0,',','.'),
                'AV'            => $acc_vip,
            );
            $admin_tpl_payer = 'VlanceAccountBundle:Email/Upgrade:admin_upgrade_vip.html.twig';
            $admin1 = "tuan.tran@vlance.vn";
            $admin3 = "vlanceantispam@gmail.com";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin1);
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin3);
//            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_admin, $from_vlance, $admin1);
//            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_admin, $from_vlance, $admin2);
//            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_admin, $from_vlance, $admin3);
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 301));
        }
        
        return new JsonResponse(array(
            'error'     => 0,
            'time'      => !is_null($account->getExpiredDateVip()) ? date_format($account->getExpiredDateVip(), 'd/m/Y H:i:s') : "-------",
            'color'     => '#27ff27',
            'type'      => !is_null($account->getTypeVIP()) ? $account->getTypeVIP() : "NULL",
        ));
    }
}
