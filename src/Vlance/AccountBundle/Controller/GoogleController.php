<?php
namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Pagerfanta\View\DefaultView;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\AccountBundle\Entity\Skill;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\AccountBundle\Form\AccountType;
use Vlance\AccountBundle\Form\Type\AccountRegistrationFullType;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;
use Vlance\AccountBundle\Entity\EmailInvite;
use Vlance\BaseBundle\Utils\ResizeImage;

/**
 * Google controller.
 * 
 * @Route("/gg")
 */
class GoogleController extends Controller
{ 
    
    /**
     * 
     * @Route("/login", name="login_google")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function loginGoogleAction(Request $request){
        $session = $request->getSession();
        if(isset($_GET['error']) && $session->get('service') == 'contact') {
           $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.gmail.error',array(),'vlance'));
           return $this->redirect($this->generateUrl('email_invite'));
        }
        // Step 1: Ask for authentication code
        $config = array();
        $config['appId'] = $this->container->parameters['vlance_system']['google']['client_id'];
        $config['secret'] = $this->container->parameters['vlance_system']['google']['app_secret'];
        $referer = $request->headers->get('referer');
        
        if($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
        // Step 2: Got authentication code,
        if(isset($_GET['code'])){
            //include google api file
            require_once __DIR__.'/../Lib/Google/Client.php';
            require_once __DIR__.'/../Lib/Google/Service/Oauth2.php';
            require_once __DIR__.'/../Lib/Google/Service/Books.php';
            
            $client_id = $config['appId'];
            $client_secret = $config['secret'];
            $redirect_uri = $this->container->get('router')->generate('login_google', array(), true);

            $client = new \Google_Client();
            $client->setClientId($client_id);
            $client->setClientSecret($client_secret);
            $client->setRedirectUri($redirect_uri);
            $client->authenticate($_GET['code']);
            //Gmail contact
            if($session->get('service') == 'contact') {
                $token = json_decode($client->getAccessToken());
                return $this->redirect($this->generateUrl('email_invite', array('accesstoken' => $token->access_token)));
             }
            //Google login
            try{
                if ($client->getAccessToken()) {
                  $oauth2Service = new \Google_Service_Oauth2($client);
                  $me = $oauth2Service->userinfo_v2_me->get();
                  $array_me = json_decode(json_encode($me), true);
                  $em = $this->getDoctrine()->getManager();
                  // Get vLance account that matches this google id
                    $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('googleId' => $array_me['id']));
//                    print_r($entity);
                    // If Facebook account has not been linked to any account on vLance, link with or create new one.
                    if(!$entity){
                        $em = $this->getDoctrine()->getManager();
                        // If google account is created by phone number (without email),
                        // use google email address instead.
                        $email = $array_me['email'];

                        $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email' => $email));                    
                        // There is no account matchs fb account's email.
                        if(!$entity){
                            // Create new account from fb account information
    //                        echo "Đang tạo tài khoản";
                            $user = new Account();
                            $user->setGoogleId($array_me['id']);
                            $user->setAccessTokenGG($client->getAccessToken());
                            $user->setFullName($array_me['name']);
                            $user->setEmail($email);
                            $user->setUsername($email);
                            if(isset($array_me['link'])){
                                $user->setWebsite($array_me['link']);
                            }
                            $user->setPlainPassword($array_me['id']);
                            // $user->setType('freelancer');
                            $user->setEnabled(true);

                            $em->persist($user);
                            
                            $em->flush();
                            
                            /** Tracking Event "Registration" **/
                            // Mixpanel
                            $this->get('vlance_base.helper')->trackAll($user, "Registration", array("channel" => "Google"));
                            // Google Adwords
                            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                                array('type' => 'html', 'html' =>
                                    '<!-- Google Code for Registration Conversion Page -->
                                    <script type="text/javascript">
                                    /* <![CDATA[ */
                                    var google_conversion_id = 925105271;
                                    var google_conversion_language = "en";
                                    var google_conversion_format = "3";
                                    var google_conversion_color = "ffffff";
                                    var google_conversion_label = "hRPxCO68hFoQ9_iPuQM";
                                    var google_remarketing_only = false;
                                    /* ]]> */
                                    </script>
                                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                    </script>
                                    <noscript>
                                    <div style="display:inline;">
                                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?label=hRPxCO68hFoQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                                    </div>
                                    </noscript>'
                                )
                            ));
                            // Facebook
                            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                                array('type' => 'html', 'html' =>
                                    '<!-- Facebook Conversion Code for Registrations -->
                                    <script>(function() {
                                      var _fbq = window._fbq || (window._fbq = []);
                                      if (!_fbq.loaded) {
                                        var fbds = document.createElement("script");
                                        fbds.async = true;
                                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                                        var s = document.getElementsByTagName("script")[0];
                                        s.parentNode.insertBefore(fbds, s);
                                        _fbq.loaded = true;
                                      }
                                    })();
                                    window._fbq = window._fbq || [];
                                    window._fbq.push(["track", "6023541940285", {"value":"0.00","currency":"VND"}]);
                                    </script>
                                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023541940285&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                                )
                            ));
                            /** END Tracking Event "Registration" **/

                            // Call event REGISTRATION_COMPLETED which is used to create user cash
                            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                            $dispatcher = $this->container->get('event_dispatcher');

                            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $this->getRequest(), new Response()));
                            $_SESSION['after_new_acc'] = TRUE;
                    
                            

                            // Do the login into recently created account
                            $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
                            $this->get("security.context")->setToken($token);

                            // Fire the login event
                            // Logging the user in above the way we do it doesn't do this automatically
                            $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);

                            //take GA url for analytics
                            $tmp = file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=account&ea=register&el=google&cs=web&cm=none&cn=none');
                            // Redirect user to the account edit screen to complete his account information
                            
                            // Redirect to after login page
                            return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                        }// Link fb account with current account (of which using the same email)
                        else{
                            // Update account with google id
                            $entity->setGoogleId($array_me['id']);
                            $entity->setAccessTokenGG($client->getAccessToken());
                            $em->persist($entity);
                            $em->flush();

                            // Do the login into recently created account
                            $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());
                            $this->get("security.context")->setToken($token);

                            // Fire the login event
                            // Logging the user in above the way we do it doesn't do this automatically
                            $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);
                            $this->get("event_dispatcher")->dispatch("onImplicitLogin", $event_login);  //HIEP đăng nhập facebook sau khi đã đăng kí

                            // Redirect to after login page
                            return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                        }
                    }else{
                        if( $entity->getAccessTokenGG()!= $client->getAccessToken() ){
                            $entity->setAccessTokenGG($client->getAccessToken());
                            $em->persist($entity);
                            $em->flush();
                        }
                        // Else, fb is already linked with acc, do login with this facebook account
                        // Do the login into recently created account
                        $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());
                        $this->get("security.context")->setToken($token);

                        // Fire the login event
                        // Logging the user in above the way we do it doesn't do this automatically
                        $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);
                        
                        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.google.success', array(), 'vlance'));
                        
                        // Redirect to after login page
                        return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                    }
                }
                    
            } catch (\Exception $e) {
                /* @var $logger \Psr\Log\LoggerInterface */
                $logger = $this->get("monolog.logger.google");
                $logger->info("Create google error");
                $logger->info(print_r($e->getMessage(), true));
                
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.google.error',array(),'vlance'));
                return new RedirectResponse($referer);
            }
        }
        
        return new RedirectResponse($referer);
    }
    
     /**
     * 
     * @Route("/invite-friend", name="invite_google")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function inviteGoogleAction(Request $request){
         //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return new JsonResponse(array('error' => 100)); 
        }
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_use = $this->get('security.context')->getToken()->getUser();
        
        // Check email list
        $emails_json = $request->request->get('emails');

        if($emails_json == ""){
            return new JsonResponse(array('error' => 101));
        }
        $emails = json_decode($emails_json, true);
        if(count($emails) == 0){
            return new JsonResponse(array('error' => 102));
        }
        
        // Send email to account invited
        $em = $this->getDoctrine()->getManager();
        $email_from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
        $bcc[] = $this->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
        
        $avatar = ResizeImage::resize_image($current_use->getFullPath(), '88x88', 88, 88, 1);
        if(!$avatar){
            $avatar = ROOT_URL . "/" .'img/unknown.png';
        }
        
        /* Tracking */
        $tracking_properties = array(
            // for mixpanel
            'invitor_id'    => $current_use->getId(),
            // for ga
            'ec'            => 'inviteemail',   // event category
            'ea'            => 'open',          // event action
            'el'            => 'invitee',       // event label
        );

        $mixpanel_tracking  = $this->get('vlance_base.helper')->buildTrackingCodeEmailMixpanel("Invitee Open Email", $tracking_properties, $current_use->getId());
        $ga_tracking = $this->get('vlance_base.helper')->buildTrackingCodeEmailGa($tracking_properties, $current_use->getId());
        /* END Tracking */
        
        $email_context = array(
            'account'   => $current_use, 
            'avatar'    => $avatar,
            'url_ai'    => $this->generateUrl('account_register_by_referrer', array('referrer_id' => $current_use->getId()), true),
            'pixel_tracking'=> array(
                            'mixpanel'  => $mixpanel_tracking,
                            'ga'        => $ga_tracking
                            ),
                );
        $template = "VlanceAccountBundle:Email:account_invite.html.twig";
        
        //mẫu biểu thức email
        $pattern = "/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/";
        
        $done = 0;
        foreach ($emails as $email) {
            //nếu chuỗi so sánh hợp lệ, hàm preg_match() trả về 1, ngược lại trả về 0
            if (preg_match($pattern, $email) == 1) {
//                    kiem tra trong acc da co email nay chua
                $condition_acc = array(
                    'email' => array('0' => 'eq', '1' => "'" . $email . "'"),
                );
                $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.email'), $condition_acc);
//                    kiem tra trong email invite da co email nay chua
                $condition = array(
                    'email' => array('0' => 'eq', '1' => "'" . $email . "'"),
                );
                $entity_email_invite = $em->getRepository('VlanceAccountBundle:EmailInvite')->getFields(array('e.email'), $condition);
                if(!$entity_email_invite && !$entity_acc){
                    $done = $done + 1;
                    $newEmail = new EmailInvite();
                    $newEmail->setAccount($current_use);
                    $newEmail->setEmail($email);
                    //send email
                    $result = $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_from, $email, $bcc);
                    $MessageId = $result->get('MessageId');
                    if (isset($MessageId)) {
                        $newEmail->setMessageId($result->get('MessageId'));
                    }
                    $em->persist($newEmail);
                    
                    //Mixpanel
                    $this->get('vlance_base.helper')->trackAll(NULL, "Invitee Receive Email");
                }
            }
        }
        $em->flush();

        // Mixpanel
        //$this->get('vlance_base.helper')->trackAll(NULL, "Invited friends", array('type' => 'Gmail', 'quantity_done' => $done));
        return new JsonResponse(array('sent' => $done, 'total' => count($emails)));
    }
    
}
