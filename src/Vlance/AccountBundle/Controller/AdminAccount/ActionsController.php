<?php

namespace Vlance\AccountBundle\Controller\AdminAccount;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Vlance\AccountBundle\Entity\Account;
use Admingenerated\VlanceAccountBundle\BaseAdminAccountController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    public function attemptBatchCertificate(array $ids) {
        $em = $this->getDoctrine()->getManager();
        /* @var $account Vlance\AccountBundle\Entity\Account */
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $ids));
        foreach ($accounts as $account) {
            $account->setIsRequested(!Account::REQUESTED);
            $account->setIsCertificated(Account::CERTIFICATED);
            $account->setNewFeature(\Vlance\AccountBundle\Entity\Account::HAVE_NEW_FEATURE);
            $account->setLastRequest(new \DateTime());
            $em->persist($account);
            $em->flush();
            // Need to save to database before send email
            $this->get('vlance.mailer')->sendCertificate($account, "", 'accept');
        }
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        "admin.account.certificate.accept.batch_success",
                        array(),
                        'vlance'
                    )
                );
        return $this->redirect($this->generateUrl('Vlance_AccountBundle_AdminAccount_list'));
    }
    
    public function attemptObjectCertificate($id) {
        /* @var $account Vlance\AccountBundle\Entity\Account */
        $account = $this->getObject($id);
        if (!$account->getIsRequested()) {
            return $this->redirect($this->generateUrl("Vlance_AccountBundle_AdminAccount_list"));
        }
        $account->setIsRequested(!Account::REQUESTED);
        $account->setIsCertificated(Account::CERTIFICATED);
        $account->setNewFeature(\Vlance\AccountBundle\Entity\Account::HAVE_NEW_FEATURE);
        $account->setLastRequest(new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();
        
        // send email to user
        $message = $this->get('translator')->trans('admin.account.certificate.accept.message', array(), 'vlance');
        $this->get('vlance.mailer')->sendCertificate($account, $message, 'accept');
        $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        "admin.account.certificate.accept.success",
                        array('%name%' => $account->getFullName()),
                        'vlance'
                    )
                );
        return $this->redirect($this->generateUrl('Vlance_AccountBundle_AdminAccount_list'));
    }
    
    public function attemptObjectProfile($id) {
        $account = $this->getObject($id);
        return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $account->getHash())));
    }
    
    public function attemptObjectRefuse($id) {
        try {
            $account = $this->getObject($id);
//            if (!$account->getIsRequested()) {
//                return $this->redirect($this->generateUrl("Vlance_AccountBundle_AdminAccount_list"));
//            }

            if ('POST' == $this->get('request')->getMethod()) {
            // Check CSRF token for action
                $intention = $this->getRequest()->getRequestUri();
                $this->isCsrfTokenValid($intention);

                $message = $this->get('request')->request->get('message');
                $account->setIsRequested(!Account::REQUESTED);
                $account->setLastRequest(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($account);
                $em->flush();
                // send email to user
                $this->get('vlance.mailer')->sendCertificate($account, $message, 'refuse');

                $this->get('session')->getFlashBag()->add(
                    'success',
                    $this->get('translator')->trans(
                        "admin.account.refuse.success",
                        array('%name%' => $account->getFullName()),
                        'vlance'
                    )
                );

                return new RedirectResponse($this->generateUrl("Vlance_AccountBundle_AdminAccount_list"));
            }

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'error',
                $this->get('translator')->trans(
                    "admin.account.refuse.error",
                    array('%name%' => $account->getFullName()),
                    'vlance'
                )
            );

            return new RedirectResponse($this->generateUrl("Vlance_AccountBundle_AdminAccount_list"));
        }

        return $this->render(
            'VlanceAccountBundle:AdminAccountActions:refuse.html.twig',
            $this->getAdditionalRenderParameters($account, 'refuse') + array(
                "Account" => $account,
                "title" => $this->get('translator')->trans(
                    "admin.account.refuse.title",
                    array('%name%' => $account->getFullName()),
                    'vlance'
                ),
                "actionRoute" => "Vlance_AccountBundle_AdminAccount_object",
                "actionParams" => array("pk" => $id, "action" => "refuse")
            )
        );
    }
}
