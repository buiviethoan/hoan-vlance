<?php

namespace Vlance\AccountBundle\Controller\AdminCategory;

use Admingenerated\VlanceAccountBundle\BaseAdminCategoryController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    public function preSave(\Symfony\Component\Form\Form $form, \Vlance\AccountBundle\Entity\Category $Category) {
        if ($Category->getParent()) {
            $Category->setPath($Category->getParent()->getId() . '/' . $Category->getId());
        } else {
            $Category->setPath($Category->getId() . '/');
        }
    }
}
