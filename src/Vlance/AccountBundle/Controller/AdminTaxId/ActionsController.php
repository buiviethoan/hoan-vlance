<?php

namespace Vlance\AccountBundle\Controller\AdminTaxId;

use Admingenerated\VlanceAccountBundle\BaseAdminTaxIdController\ActionsController as BaseActionsController;
use Vlance\AccountBundle\Entity\TaxId;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * ActionsController
 */
class ActionsController extends BaseActionsController
{
    public function attemptObjectVerify($id)
    {
       /* @var $taxid Vlance\AccountBundle\Entity\TaxId */
        $taxid = $this->getObject($id);
        if (!$taxid->getRequested()) {
            return new JsonResponse(array("error" => 101));
        }
        
        if($taxid->getIsVerified() == true){
            return new JsonResponse(array("error" => 102));
        }
        $taxid->setIsVerified(true);
        $taxid->setVerifiedAt (new \DateTime());
        $em = $this->getDoctrine()->getManager();
        $em->persist($taxid);
        $em->flush();
               
        /* sent mail */
        $from_vlance = $this->container->getParameter('from_email');

        $context = array(
            'fullname'   => $taxid->getAccount()->getFullName(),
        );
        $template = 'VlanceAccountBundle:Email/VerifyId:verify_tax_id_success.html.twig';
        
        try{
            // Send confirmation mail to payer
            $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance, $taxid->getAccount()->getEmail());
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 103));
        }
        
        return new JsonResponse(array("error" => 0));
    }
    
    public function attemptObjectDecline($id)
    {
        /* @var $taxid Vlance\AccountBundle\Entity\TaxId */
        $taxid = $this->getObject($id);
        if (!$taxid->getRequested()) {
            return new JsonResponse(array("error" => 101));
        }
        
        if($taxid->getIsVerified()){
            return new JsonResponse(array("error" => 102));
        }
        
        //Luu ly do khong xac thuc
        //NOTE: phần lưu comment chưa được gọn gàng tối ưu. Sẽ sửa lại sau.
        $reason = $_GET['comment'];
        if($taxid->getComment() !== NULL){
            $old_arr = json_decode($taxid->getComment(), true);
            $count = count($old_arr);
            $new_array = array('verify' => 'refuse', 'reason' => $reason, 'count' => $count + 1);
            if($count < 2){
                $arr = array();
                array_push($arr, $old_arr, $new_array);
                $taxid->setComment(json_encode($arr));
            } else {
                array_push($old_arr, $new_array);
                $taxid->setComment(json_encode($old_arr));
            } 
        } else {
            $count = 1;
            $taxid->setComment(json_encode(array('verify' => 'refuse', 'reason' => $reason, 'count' => $count)));
        }
        
        $taxid->setRequested(false);
        $taxid->setRequestedAt(NULL);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($taxid);
        $em->flush();
        
        /* sent mail */
        $from_vlance = $this->container->getParameter('from_email');

        $context = array(
            'fullname'      => $taxid->getAccount()->getFullName(),
            'reason'        => $reason,
        );
        $template = 'VlanceAccountBundle:Email/VerifyId:verify_tax_id_failed.html.twig';

        try{
            // Send confirmation mail to payer
            $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance, $taxid->getAccount()->getEmail());
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 103));
        }
        
        return new JsonResponse(array("error" => 0));
    }
}
