<?php

namespace Vlance\AccountBundle\Controller\AdminPersonId;

use Admingenerated\VlanceAccountBundle\BaseAdminPersonIdController\ActionsController as BaseActionsController;
use Vlance\AccountBundle\Entity\PersonId;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * ActionsController
 */
class ActionsController extends BaseActionsController
{   
    public function attemptObjectVerify($id)
    {
       /* @var $personid Vlance\AccountBundle\Entity\PersonId */
        $personid = $this->getObject($id);
        $em = $this->getDoctrine()->getManager();
        
        // Temporarily disabled
        /*if (!$personid->getRequested()) {
            return new JsonResponse(array("error" => 101));
        }*/
        
        if($personid->getIsVerified() == true){
            return new JsonResponse(array("error" => 102));
        }
        
        $number_existed = $em->getRepository('VlanceAccountBundle:PersonId')->findBy(array('idNumber' => $personid->getIdNumber()));
        if($number_existed){
            foreach($number_existed as $ne){
                if($ne->getVerifyAt()){
                    return new JsonResponse(array("error" => 103));
                }
            }
        }
        
        $personid->setRequested(true);
        $personid->setIsVerified(true);
        $personid->setVerifyAt (new \DateTime());
        $em->persist($personid);
        $em->flush();
               
        /* sent mail */
        $from_vlance = $this->container->getParameter('from_email');

        $context = array(
            'fullname'   => $personid->getFullName(),
        );
        $template = 'VlanceAccountBundle:Email/VerifyId:verify_id_success.html.twig';
        
        try{
            // Send confirmation mail to payer
            $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance, $personid->getAccount()->getEmail());
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 104));
        }
        
        return new JsonResponse(array("error" => 0));
    }
    
    public function attemptObjectDecline($id)
    {
        /* @var $personid Vlance\AccountBundle\Entity\PersonId */
        $personid = $this->getObject($id);
        
        // Temporarily disabled
        /*if (!$personid->getRequested()) {
            return new JsonResponse(array("error" => 101));
        }*/
        
        if($personid->getIsVerified()){
            return new JsonResponse(array("error" => 102));
        }
        
        //Luu ly do khong xac thuc
        $reason = $_GET['comment'];
        if(!$reason){
            return new JsonResponse(array("error" => 103));
        }
        $now = new \DATETIME('now');
        if($personid->getComment()){
            $old_arr = $personid->getArrayComment();
            //$old_arr = json_decode($personid->getComment(), true);
            $new_array = array('date' => $now->format('Y-m-d H:i:s'), 'verify' => 'refuse', 'reason' => $reason);
            array_push($old_arr, $new_array);
            $personid->setComment(json_encode($old_arr));
        } else {
            $personid->setComment(json_encode(array(array('date' => $now->format('Y-m-d H:i:s'), 'verify' => 'refuse', 'reason' => $reason))));
        }

        $personid->setRequested(false);
        $personid->setRequestedAt(NULL);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($personid);
        $em->flush();
        
        /* sent mail */
        $from_vlance = $this->container->getParameter('from_email');

        $context = array(
            'fullname'      => $personid->getFullName(),
            'reason'        => $reason,
        );
        $template = 'VlanceAccountBundle:Email/VerifyId:verify_id_failed.html.twig';

        try{
            // Send confirmation mail to payer
            $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance, $personid->getAccount()->getEmail());
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 104));
        }
        
        return new JsonResponse(array("error" => 0));
    }
}
