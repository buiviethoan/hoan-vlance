<?php

namespace Vlance\AccountBundle\Controller\AdminPersonId;

use Admingenerated\VlanceAccountBundle\BaseAdminPersonIdController\ShowController as BaseShowController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;
use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\PersonId;

/**
 * ShowController
 */
class ShowController extends BaseShowController
{
    /* overwrite index action */
    public function indexAction($pk)
    {
        $PersonId = $this->getObject($pk);

        if (!$PersonId) {
            throw new NotFoundHttpException("The Vlance\AccountBundle\Entity\PersonId with id $pk can't be found");
        }
        
        $em = $this->getDoctrine()->getManager();
        $person = $em->getRepository('VlanceAccountBundle:PersonId')->findBy(array('idNumber' => $PersonId->getIdNumber(), 'isVerified' => true));
        $existed_verified_id = "";
        if($person){
            $existed_verified_id = " * Số CMND đã được xác thực";
        }

        return $this->render('VlanceAccountBundle:AdminPersonIdShow:index.html.twig', $this->getAdditionalRenderParameters($PersonId) + array(
            "PersonId" => $PersonId,
            "existed_verified_id" => $existed_verified_id,
        ));
    }
}
