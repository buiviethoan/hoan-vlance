<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Service controller.
 *
 * @Route("/")
 */
class ServiceController extends Controller
{
   
    /**
     * 
     * @Route("/service", name="service_list")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
       
        $entities = $em->getRepository('VlanceAccountBundle:Service')->findAll();
        $response = new JsonResponse();
        $data = array();
        foreach($entities as $entity) {
            $data[] = $entity->getTitle();
        }
        
        return $response->setData($data);
    }
    
    /**
     * Finds and displays a Category entity.
     *
     * @Route("/dich-vu/{seoUrl}", name="service_landing_page")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($seoUrl)
    {
        $em = $this->getDoctrine()->getManager();
        
        /* @var $service Service */
        $service = $em->getRepository('VlanceAccountBundle:Service')->findOneBy(array('seoUrl' => $seoUrl));
        // Tim service theo seourl, hoac theo hash        
        if(!is_object($service)){
            $service = $em->getRepository('VlanceAccountBundle:Service')->findOneBy(array('hash' => $seoUrl));
            if(!is_object($service)){
                return $this->redirect($this->generateUrl("404_page"), 301);
            }
        }
        
        if($service->getOnline() == 0){
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        $template = "VlanceAccountBundle:Service:show.html.php";
        
        // freelancers cung cap dich vu
        $list_fl = $em->getRepository('VlanceAccountBundle:Account')->findFreelancerWith($service);
        $freelance = array();
        foreach($list_fl as $key => $fl){
                array_push($freelance, array('entity'=> $fl, 'jobworked' => count($em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($fl))));
            }
        
        //statistic
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
            
        $var_array = array(
            'absoluteUrl'       => $this->generateUrl('service_landing_page', array('seoUrl' => $seoUrl), true),
            'service'           => $service,
            'nl2pDesc'          => $this->get('vlance_base.helper')->nl2p($service->getDescription()),
            'topFreelancer'     => $freelance,
            'numFl'             => count($freelance),
            'jobStatistic'     => $jobStatistic,
            'accountStatistic' => $accountStatistic,
        );
        
        return $this->container->get('templating')->renderResponse($template, $var_array);
    }
}
