<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Pagerfanta\View\DefaultView;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\AccountBundle\Entity\Skill;
use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\AccountVIP;
use Vlance\AccountBundle\Entity\PersonId;
use Vlance\AccountBundle\Entity\TaxId;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\AccountBundle\Entity\EmailInvite;
use Vlance\AccountBundle\Form\EmailInviteType;
use Vlance\AccountBundle\Form\AccountType;
use Vlance\AccountBundle\Form\PersonIdType;
use Vlance\AccountBundle\Form\TaxIdType;
use Vlance\AccountBundle\Form\Type\AccountRegistrationFullType;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;
use Vlance\BaseBundle\Utils\ResizeImage;

use Vlance\PaymentBundle\Entity\UserCash;
use Vlance\PaymentBundle\Entity\OnePay;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Entity\CreditAccount as CreditAccount;
use Vlance\PaymentBundle\Entity\CreditExpenseTransaction as CreditExpenseTransaction;

/**
 * Account controller.
 * 
 * @Route("/")
 */
class AccountController extends Controller {
    /**
     * 
     * @Route("/a/mini_form", name="account_miniform")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function miniFormAction(Request $request) {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, new UserEvent($user, $request));

        $form = $formFactory->createForm();
        $form->setData($user);

        return $this->container->get('templating')->renderResponse("VlanceAccountBundle:Account/register:homepageForm.html.php", array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * 
     * @Route("/a/register/{type}", name="account_register", defaults={"type" = "choice"}, requirements={"type" = "choice|freelancer|client"})
     * @Template("VlanceAccountBundle:Account/register:register.html.php", engine="php")
     */
    public function registerAction(Request $request, $type = 'choice') {
        //Check login
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.notvalid', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        if ($type == 'choice') {
            return $this->container->get('templating')->renderResponse("VlanceAccountBundle:Account/register:preRegister.html.php");
        } else {
            /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
            $formFactory = $this->container->get('fos_user.registration.form.factory');
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->container->get('fos_user.user_manager');
            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->container->get('event_dispatcher');

            $user = $userManager->createUser();
            $user->setEnabled(true);

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, new UserEvent($user, $request));

            $form = $formFactory->createForm();
            $form->setData($user);

            if ('POST' === $request->getMethod()) {
                $form->bind($request);

                if ($form->isValid()) {
                    $event = new FormEvent($form, $request);
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                    $user->setType($type);

                    $userManager->updateUser($user);
                    
                    //take GA for analytics
                    $tmp = @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=account&ea=register&el=web&cs=web&cm=none&cn=none');

                    if (null === $response = $event->getResponse()) {
                        $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                        $response = new RedirectResponse($url);
                    }
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));                    
                    
                    /** Tracking Event "Registration" **/
                    // Mixpanel
                    $this->get('vlance_base.helper')->trackAll($user, "Registration", array("channel" => "Direct", "type" => $type));
                    // Google Adwords
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Google Code for Registration Conversion Page -->
                            <script type="text/javascript">
                            /* <![CDATA[ */
                            var google_conversion_id = 925105271;
                            var google_conversion_language = "en";
                            var google_conversion_format = "3";
                            var google_conversion_color = "ffffff";
                            var google_conversion_label = "hRPxCO68hFoQ9_iPuQM";
                            var google_remarketing_only = false;
                            /* ]]> */
                            </script>
                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                            </script>
                            <noscript>
                            <div style="display:inline;">
                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?label=hRPxCO68hFoQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                            </div>
                            </noscript>'
                        )
                    ));
                    // Facebook
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Facebook Conversion Code for Registrations -->
                            <script>(function() {
                              var _fbq = window._fbq || (window._fbq = []);
                              if (!_fbq.loaded) {
                                var fbds = document.createElement("script");
                                fbds.async = true;
                                fbds.src = "//connect.facebook.net/en_US/fbds.js";
                                var s = document.getElementsByTagName("script")[0];
                                s.parentNode.insertBefore(fbds, s);
                                _fbq.loaded = true;
                              }
                            })();
                            window._fbq = window._fbq || [];
                            window._fbq.push(["track", "6023541940285", {"value":"0.00","currency":"VND"}]);
                            </script>
                            <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023541940285&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                        )
                    ));
                    /** END Tracking Event "Registration" **/
                    
//                    neu $event_face = 1 thy add them Event cho facebook: (thêm và head)
                    $_SESSION['after_new_acc'] = TRUE;

                    return $response;
                }
            }
            
            return array(
                'type' => $type,
                'form' => $form->createView(),
            );
        }
    }
    
    /**
     * 
     * @Route("/refer/{referrer_id}", name="account_register_by_referrer")
     * @Template("VlanceAccountBundle:Account:register_by_referrer.html.php", engine="php")
     */
    public function registerReferByAction(Request $request, $referrer_id = null) {
        //Check login
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.notvalid', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "invitee_registration_page"
                ));
            }
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');
        
        $em = $this->getDoctrine()->getManager();
        $userReferrer = $em->getRepository('VlanceAccountBundle:Account')->findOneById($referrer_id);
        if(is_object($userReferrer)){
            setcookie('referrer_invite', $referrer_id, time()+(300), '/refer/'. $referrer_id .'');
        }
                
        $user = $userManager->createUser();
        $user->setEnabled(true);
        
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, new UserEvent($user, $request));

        $form = $formFactory->createForm();
        $form->setData($user);
        if ('POST' === $request->getMethod()) {
            $form->bind($request);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));                    

                $_SESSION['after_new_acc'] = TRUE;
                
                return $response;
            }
        }
        
        return array(
            'form'          => $form->createView(),
            'referrer_id'   => $referrer_id,
        );
    }
    
    /**
     * 
     * @Route("/a/reconfirm", name="account_reconfirm")
     * @Template(engine="php")
     */
    public function reconfirmAction(Request $request) {
        return array('url' => $this->generateUrl('account_sendconfirm', array(), true));
    }

    /**
     * 
     * @Route("/a/sendconfirm", name="account_sendconfirm")
     */
    public function sendconfirmAction(Request $request) {
        $email = $request->getSession()->get('fos_user_send_confirmation_email/email');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($email);
        $mailer = $this->get('vlance.mailer');
        $mailer->sendConfirmationEmailMessage($user);
        return $this->redirect($this->generateUrl('fos_user_registration_check_email'));
    }
    
    /**
     * check login
     * @Route("/a/login_job", name="login_job")
     * @Method("POST")
     */
    public function loginAction(Request $request){
        //Check login
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        $email = $request->request->get('email');
        $pass = $request->request->get('password');
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('username' => $email));
        
        if(!$account){
            return new JsonResponse(array('error' => 102));
        } else {
            $encoder_service = $this->get('security.encoder_factory');
            $encoder = $encoder_service->getEncoder($account);
            $encoded_pass = $encoder->encodePassword($pass, $account->getSalt());
            
            if(/*$encoded_pass != $account->getPassword()*/ $encoded_pass == ""){
//	    if($encoded_pass == ""){
                return new JsonResponse(array('error' => 103));
            } else {
                // Do the login into recently created account
                $token = new UsernamePasswordToken($account, $account->getPassword(), "main", $account->getRoles());
                $this->get("security.context")->setToken($token);
                return new JsonResponse(array('error' => 0));
            }
        }
    }
    
    /**
     * register
     * @Route("/a/register_job", name="register_job")
     * @Method("POST")
     */
    public function registerInJobAction(Request $request){
        //Check login
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $em = $this->getDoctrine()->getManager();
        $email_existed = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email'=>$request->request->get('email')));
        
        if($email_existed){
            return new JsonResponse(array('error' => 102));
        }
        
        $account = new Account();
        $account->setFullName($request->request->get('fullname'));
        $account->setEmail($request->request->get('email'));
        $account->setUsername($request->request->get('email'));
        $account->setPlainPassword($request->request->get('password'));
        $account->setType($request->request->get('user'));
        $account->setEnabled(true);
        $em->persist($account);
        $em->flush();
        
        //create user cash
        $userCash = new UserCash();
        $userCash->setAccount($account);
        $userCash->setBalance(0);
        $em->persist($userCash);
        $em->flush();
        
        //create credit account
        $creditAccount = new CreditAccount();
        $creditAccount->setAccount($account);
        $creditAccount->setBalance(0);
        $em->persist($creditAccount);
        $em->flush();
        
        // Do the login into recently created account
        $token = new UsernamePasswordToken($account, $account->getPassword(), "main", $account->getRoles());
        $this->get("security.context")->setToken($token);
        return new JsonResponse(array('error' => 0));
    }
    
    /**
     * Finds and displays a Account entity Alias.
     *
     * @Route("/freelancer/{hash}", name="account_show_freelancer", requirements={"mesage" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:show.html.php",engine="php")
     */
    public function showFreelancerAction(Request $request, $hash) {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "account_show_freelancer",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $entity /Vlance/AccountBundle/Entity/Account */
        $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('hash' => $hash));
        
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Account is locked, redirect to 404 page. Only admin can view.
        if($entity->isLocked()){
            if($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                $current_user = $this->get('security.context')->getToken()->getUser();
                if(!($current_user->isAdmin())){
                    return $this->redirect($this->generateUrl("404_page"), 302);
                }
            } else{
                return $this->redirect($this->generateUrl("404_page"), 302);
            }
        }
        
        //So cong viec lam da hoan thanh
//        $jobs_worked = $em->getRepository('VlanceJobBundle:Job')->findBy(array('worker' => $entity, 'status' => Job::JOB_FINISHED, 'publish' => Job::PUBLISH));
        $jobs_worked = $em->getRepository('VlanceJobBundle:Job')->findJobsAwardedTo($entity);
        $jobs_working = $em->getRepository('VlanceJobBundle:Job')->findBy(array('worker' => $entity, 'status' => Job::JOB_WORKING, 'publish' => Job::PUBLISH));
        $this->get('session')->set('id', $entity->getId());
        // Build absolute URL. If in dev environnement, return http://www.vlance.vn as default.
        $request = $this->getRequest();
        $abs_url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();
        if (in_array($this->container->getParameter('kernel.environment'), array('dev'))) {
            $abs_url = "http://www.vlance.vn";
        }
        $description = 'Freelancer ' . $entity->getFullName();
        $cat = $entity->getCategory() ? ' có kinh nghiệm trong lĩnh vực ' . $entity->getCategory()->getTitle() : ' ';
        $city = $entity->getCity() ? ' đến từ ' . $entity->getCity()->getName() : ' ';
        $meta_des = $description . $cat . $city . '. ' . $entity->getDescription();
        $hash_string = $this->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        
        // Job invites sent to freelancers
        $job_invites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('account' => $entity));
           
        $this->get('session')->set('current_profile_view', '');
        $profile_id = $entity->getId();
        if($profile_id){
            $this->get('session')->set('current_profile_view', $profile_id);
        }
        
        return array(
            'entity' => $entity,
            'jobs_worked' => $jobs_worked,
            'jobs_working' => $jobs_working,
            'abs_url' => $abs_url,
            'meta_description' => str_replace('>', '*', addslashes(JobCalculator::cut(JobCalculator::replacement(strip_tags($meta_des)), 150))),
            'hash_string' => $hash_string,
            'job_invites' => $job_invites,
        );
    }
    
    /**
     * Display client's profile.
     *
     * @Route("/khach-hang/{hash}", name="account_show_client", requirements={"mesage" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:client.html.php",engine="php")
     */
    public function showClientAction(Request $request, $hash) {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "account_show_client",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $entity /Vlance/AccountBundle/Entity/Account */
        $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('hash' => $hash));
        
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Account is locked, redirect to 404 page. Only admin can view.
        if($entity->isLocked()){
            if($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                $current_user = $this->get('security.context')->getToken()->getUser();
                if(!($current_user->isAdmin())){
                    return $this->redirect($this->generateUrl("404_page"), 302);
                }
            } else{
                return $this->redirect($this->generateUrl("404_page"), 302);
            }
        }
        
        // Number of posted, funded, jobs
        $jobs_posted = $entity->getJobsPublish();
        $jobs_hired = $entity->getJobsHired();
        $this->get('session')->set('id', $entity->getId());
        
        // Build absolute URL. If in dev environnement, return http://www.vlance.vn as default.
        $request = $this->getRequest();
        $abs_url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();
        if (in_array($this->container->getParameter('kernel.environment'), array('dev'))) {
            $abs_url = "http://www.vlance.vn";
        }
        $description = 'Khách hàng ' . $entity->getFullName();
        $cat = $entity->getCategory() ? ' hoạt động trong lĩnh vực ' . $entity->getCategory()->getTitle() : ' ';
        $city = $entity->getCity() ? ' tại ' . $entity->getCity()->getName(): ' ';
        $meta_des = $description . $cat . $city . ', cần thuê freelancers thực hiện các dự án và thanh toán qua vLance.vn.';
        $hash_string = $this->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        return array(
            'entity' => $entity,
            'jobs_worked' => $jobs_posted,
            'jobs_working' => $jobs_hired,
            'abs_url' => $abs_url,
            'meta_description' => str_replace('>', '*', addslashes(JobCalculator::cut(JobCalculator::replacement(strip_tags($meta_des)), 150))),
            'hash_string' => $hash_string,
        );
    }

    /**
     * Displays a form to edit an existing Account entity.
     *
     * @Route("/a/{id}/edit/{referer}", name="account_edit")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:edit.html.php",engine="php")
     */
    public function editAction(Request $request, $id, $referer = null)
    {
        $referer = base64_encode(($referer == "redirect" && $request->headers->get('referer')) ? $request->headers->get('referer') : $this->generateUrl('account_edit', array('id' => $id)));
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        $this->get('session')->set('id', $id);
        
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        } 
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Don't have permistion
        if($acc !== $entity && !$acc->isAdmin()) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Edit profile");
        
        $editForm = $this->createForm('vlance_accountbundle_accounttype', $entity);
        return array(
            'entity'      => $entity,
            'acc'         => $acc,
            'edit_form'   => $editForm->createView(),
            'referer'     => $referer,
        );
    }
    
    /**
     * Displays a form to edit an existing Account entity.
     *
     * @Route("/a/{id}/editportfolio/{referer}", name="account_portfolio_edit")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:edit_portfolio.html.php",engine="php")
     */
    public function editPortfolioAction(Request $request, $id, $referer = null)
    {
        $referer = base64_encode(($referer == "redirect" && $request->headers->get('referer')) ? $request->headers->get('referer') : $this->generateUrl('account_portfolio_edit', array('id' => $id)));
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        $this->get('session')->set('id', $id);
        
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        } 
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Don't have permistion
        if($acc !== $entity && !$acc->isAdmin()) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Edit portfolio profile");
        
        return array(
            'entity'      => $entity,
            'acc'         => $acc,
            'referer'     => $referer,
        );
    }
    
    /**
     * Displays a form to edit an existing Account entity.
     *
     * @Route("/a/{id}/editbasic/{referer}", name="account_basic_edit")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:edit_basic.html.php",engine="php")
     */
    public function editBasicAccountAction(Request $request, $id, $referer = null)
    {
        $referer = base64_encode(($referer == "redirect" && $request->headers->get('referer')) ? $request->headers->get('referer') : $this->generateUrl('account_basic_edit', array('id' => $id)));
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        $this->get('session')->set('id', $id);
        
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        } 
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Don't have permistion
        if($acc !== $entity && !$acc->isAdmin()) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Edit basic profile");
        
        $editForm = $this->createForm('vlance_accountbundle_accountbasictype', $entity);
        
        //personid is verified
        if($entity->getPersonId() instanceof PersonId){
            if(!is_null($entity->getPersonId()->getRequestedAt()) || !is_null($entity->getPersonId()->getVerifyAt())){
                //idCardNumber readonly
                $fieldIdNumber = $editForm->get('idCardNumber');
                $optionsIdNumber = $fieldIdNumber->getConfig()->getOptions();
                $typeIdNumber = $fieldIdNumber->getConfig()->getType()->getName();
                $optionsIdNumber['attr'] = array('readonly' => 'readonly');
                $editForm->add('idCardNumber', $typeIdNumber, $optionsIdNumber); 
                
                //birthday readonly
                $fieldBirthday = $editForm->get('birthday');
                $optionsBirthday = $fieldBirthday->getConfig()->getOptions();
                $typeBirthday = $fieldBirthday->getConfig()->getType()->getName();
                $optionsBirthday['start_date'] = $entity->getBirthday()->format('d/m/Y');
                $optionsBirthday['end_date'] = $entity->getBirthday()->format('d/m/Y');
                $optionsBirthday['attr'] = array('readonly' => 'readonly', 'class' => 'span12 popovers-input');
                $editForm->add('birthday', $typeBirthday, $optionsBirthday); 
            }
        }
        
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'referer'     => $referer,
        );
    }
    
    /**
     * Edits an existing Account entity, save new changes.
     *
     * @Route("/a/{id}/updatebasic/{referer}", name="account_basic_update")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Profile:edit_basic.html.php",engine="php")
     */
    public function updateBasicAction(Request $request, $id, $referer)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $uow \Doctrine\ORM\UnitOfWork */
        $uow = $em->getUnitOfWork();
        /* @var $entity \Vlance\AccountBundle\Entity\Account */
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        
        $missing_client_info = false;// Var to track if client's missing info has just been completed
        if($entity->getIsMissingClientInfo()){
            $missing_client_info = true;
        }
        
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Don't have permission
        if($acc !== $entity && !$acc->isAdmin()) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.message.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_show_freelancer',array('hash' => $entity->getHash())));
        }
        $email_old = $entity->getEmail();
        $telephone_old = $entity->getTelephone();
        
        //idCardNumber is requested
        $birthday_requested = $entity->getBirthday();
        if($entity->getPersonId() instanceof PersonId){
            $id_number_requested = $entity->getPersonId()->getIdNumber();
        }
        
        $editForm = $this->createForm('vlance_accountbundle_accountbasictype', $entity);
        if(!is_null($request->request->get('email'))) {
            $request->request->remove('email');
        }  
        $editForm->bind($request);
        $modified = false;
        $avatar_modified = false;
        
        //Check email 
        if($editForm->get('email')->getData() && $editForm->get('email')->getData() !== $email_old) {
            $condition = array(
              'email' => array('0' => 'eq', '1' => "'".$editForm->get('email')->getData()."'"),  
            );
            $check_email = $em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.email'), $condition, 1);
            if(count($check_email) > 0) {
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.email.exist',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_basic_edit',array('id' => $id, 'referer' => $referer)));
            }
        }
        
        // Validation
        if ($editForm->isValid()) {
            // Nếu số điện thoại thay đổi, bỏ xác thực số điện thoại đi
            if($editForm->get('telephone')->getData() != $telephone_old){
                $entity->setTelephoneVerifiedAt(null);
            }

            if($editForm->get('email')->getData() == null) {
                $entity->setEmail($email_old);
            }
            
            $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            $changeSet = $uow->getEntityChangeSet($entity);
            
            /** 
             * Get the modified field. Important notes here:
             * - When changing "avatar" (file), $changeSet never count it, so we have to change $updatedAt so this will be taken in account.
             * - For the "birthday", it's always marked as changed. So count($changeSet) is always >= 1
             */
            if (count($changeSet) > 1) {
                $modified = true;
            } elseif (!isset($changeSet['birthday'])) {
                $modified = true;
            } elseif ($changeSet['birthday'][0] != $changeSet['birthday'][1]) {
                $modified = true;
                $entity->setBirthday($changeSet['birthday'][1]);
            }
            
            
            
            if ($entity->getFile()) {
                // avatar_changed
                $modified = true;
                $avatar_modified = true;
            }
            
            // Check percent after save to db to make sur all fields are set
            if ($modified) {
                $entity->setIsCertificated(!Account::CERTIFICATED);
            }
            
            if($avatar_modified && !$modified){
                // Because Avatar was not saved if only avatar is changed, so this's the hack. Don't remove this.
                // When only avatar was updated, set the UpdatedAt manually.
                $entity->setUpdatedAt(new \DateTime());
            }
            
            if (($entity->getNotify() == Account::CAN_NOTIFY) && ($this->getProfilePercent($entity) == 100) && 
                    ($entity->getIsRequested() != Account::REQUESTED) && ($entity->getIsCertificated() != Account::CERTIFICATED)) {
                $entity->setNotify(Account::WILL_NOTIFY);
            }
            
            $entity->setType($entity->getType());
            
            $em->persist($entity);
            $em->flush();
            
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Updated basic profile");
            if($missing_client_info && !$entity->getIsMissingClientInfo()){
                // $entity missed client info, but it just got completely updated
                $this->get('vlance_base.helper')->trackAll(NULL, "Completed client basic info");
            }
        
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.show.updated', array(), 'vlance'));
            
            // Done, redirect to referer of show profile page
            $referer = base64_decode($referer);
            if ($referer == '') {
                $referer = $this->generateUrl('account_basic_edit', array('id' => $id));
            }
            return $this->redirect($referer);
        } else{
            foreach ($editForm->getErrors() as $error){
                $params = $error->getMessageParameters();

                // File error
                if(isset($params['{{ file }}'])){
                    if(isset($params['{{ limit }}']) || isset($params['{{ types }}'])){
                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('profile.edit_profile.error_file', array(), 'vlance'));
                    }
                }
            }
            return $this->redirect($this->generateUrl('account_basic_edit',array('id' => $id, 'referer' => $referer)));
        }
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'referer'      => $referer
        );
    }

    /**
     * Edits an existing Account entity, save new changes.
     *
     * @Route("/a/{id}/update/{referer}", name="account_update")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Profile:edit.html.php",engine="php")
     */
    public function updateAction(Request $request, $id, $referer)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $uow \Doctrine\ORM\UnitOfWork */
        $uow = $em->getUnitOfWork();
        /* @var $entity \Vlance\AccountBundle\Entity\Account */
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        
        $missing_client_info = false;// Var to track if client's missing info has just been completed
        if($entity->getIsMissingClientInfo()){
            $missing_client_info = true;
        }
        
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Don't have permission
        if($acc !== $entity && !$acc->isAdmin()) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.message.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_show_freelancer',array('hash' => $entity->getHash())));
        }
        
        $editForm = $this->createForm('vlance_accountbundle_accounttype', $entity);
        $editForm->bind($request);
        $modified = false;
        
        // Validation
        if ($editForm->isValid()) {
            //Remove skill khi tai khoan dang nhap click xoa skill
            $skilled = $entity->getSkills();
            $skill_old = array();
            $skills = explode(',', $_POST['hiddenTagListA']);
            foreach($skilled as $skill) {
                if(!in_array($skill->getTitle(), $skills)){
                    $entity->removeSkill($skill);
                    $modified = true;
                }
                $skill_old[] = $skill->getTitle();
            }
            
            // Insert skill khi tai khoan dang nhap them moi skill
            foreach($skills as $skill_new) {
                if(empty($skill_new)) {
                    break;
                }
                elseif(!in_array($skill_new, $skill_old)) {
                    $acc_skill = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('title' => $skill_new));
                    $entity->addSkill($acc_skill);
                    $modified = true;
                }
            }
            
            //Remove service khi tai khoan dang nhap click xoa service
            $services = $entity->getServices();
            $services_old = array();
            $services_new = explode(',', $_POST['hiddenTagListB']);
            foreach($services as $s) {
                if(!in_array($s->getTitle(), $services_new)){
                    $entity->removeService($s);
                    $modified = true;
                }
                $services_old[] = $s->getTitle();
            }
            
            // Insert skill khi tai khoan dang nhap them moi skill
            foreach($services_new as $s) {
                if(empty($s)) {
                    break;
                }
                elseif(!in_array($s, $services_old)) {
                    $acc_service = $em->getRepository('VlanceAccountBundle:Service')->findOneBy(array('title' => $s));
                    $entity->addService($acc_service);
                    $modified = true;
                }
            }
            if(count($entity->getServices()) > 10){
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.update.service.error', array(), 'vlance'));
                return $this->redirect($this->generateUrl('account_edit',array('id' => $id)));
            }
            
            $em->persist($entity);
            
            // Get the modified field
            $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            $em->flush();
            
            // Check percent after save to db to make sur all fields are set
            if ($modified) {
                $entity->setIsCertificated(!Account::CERTIFICATED);
            }
            if (($entity->getNotify() == Account::CAN_NOTIFY) && ($this->getProfilePercent($entity) == 100) && 
                    ($entity->getIsRequested() != Account::REQUESTED) && ($entity->getIsCertificated() != Account::CERTIFICATED)) {
                $entity->setNotify(Account::WILL_NOTIFY);
            }
            $em->persist($entity);
            $em->flush();
            
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Updated profile");
            if($missing_client_info && !$entity->getIsMissingClientInfo()){
                // $entity missed client info, but it just got completely updated
                $this->get('vlance_base.helper')->trackAll(NULL, "Completed client basic info");
            }
        
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.show.updated', array(), 'vlance'));
            
            // Done, redirect to referer of show profile page
            $referer = base64_decode($referer);
            if ($referer == '') {
                $referer = $this->generateUrl('account_edit', array('id' => $id));
            }
            return $this->redirect($referer);
        } else{
            foreach ($editForm->getErrors() as $error){
                $params = $error->getMessageParameters();

                // File error
                if(isset($params['{{ file }}'])){
                    if(isset($params['{{ limit }}']) || isset($params['{{ types }}'])){
                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('profile.edit_profile.error_file', array(), 'vlance'));
                    }
                }
            }
            return $this->redirect($this->generateUrl('account_edit',array('id' => $id, 'referer' => $referer)));
        }
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'referer'      => $referer
        );
    }
    
    /**
     * Deletes a Account entity.
     *
     * @Route("/a/{id}/deletefile", name="account_delete_file")
     * @Method("POST")
     */
    public function deleteFileAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        // Don't have permission
        $acc = $this->get('security.context')->getToken()->getUser();
        if ($acc !== $entity) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        $entity->removeUpload();
        $entity->setPath(null);
        return $this->redirect($this->generateUrl('account_edit', array('id' => $id)));
    }

    /**
     * @Route("/a/block/contactclient", name="account_block_contact_client")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/client/content_new:contact.html.php",engine="php")
     */
    public function accountBlockContactClientAction(Request $request) {
        $id = $this->get('session')->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Check if user is viewing his/her own profile
        $current_user = $this->get('security.context')->getToken()->getUser();
        $owner_view = false;
        if(is_object($current_user)){
            if($current_user->getId() == $entity->getId()){
                $owner_view = true;
            }
        }
        
        $jobs_worked = $em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($entity);
        $clients_worked = $em->getRepository('VlanceAccountBundle:Account')->findClientsWorkedWith($entity);
        return array(
            'entity'        => $entity, 
            'jobs_worked'   => $jobs_worked, 
            'client_worked' => $clients_worked,
            'owner_view'    => $owner_view,
            'current_user'  => $current_user
        );
    }

    /**
     * @Route("/a/block/contact", name="account_block_contact")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/show/content_new:contact.html.php",engine="php")
     */
    public function accountBlockContactAction(Request $request) {
        $id = $this->get('session')->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Check if user is viewing his/her own profile
        $current_user = $this->get('security.context')->getToken()->getUser();
        $owner_view = false;
        if(is_object($current_user)){
            if($current_user->getId() == $entity->getId()){
                $owner_view = true;
            }
        }
        
        $jobs_worked = $em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($entity);
        $clients_worked = $em->getRepository('VlanceAccountBundle:Account')->findClientsWorkedWith($entity);
        return array(
            'entity'        => $entity, 
            'jobs_worked'   => $jobs_worked, 
            'client_worked' => $clients_worked,
            'owner_view'    => $owner_view,
            'current_user'  => $current_user
        );
    }

    /**
     * @Route("/a/sumary", name="account_sumary")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/show/content_new:summary.html.php",engine="php")
     */
    public function sumaryAction(Request $request) {
        $id = $this->get('session')->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Calculate success rate
        $jobs_worked = $em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($entity);
        $jobs_cancelled = $em->getRepository('VlanceJobBundle:Job')->findJobsCancelledBy($entity);
        if((count($jobs_worked) + count($jobs_cancelled)) == 0){
            $success_rate = 0;
        } else{
            $success_rate = count($jobs_worked) / (count($jobs_worked) + count($jobs_cancelled));
        }
        
        // Calculate rehire rate
        $clients_worked = $em->getRepository('VlanceAccountBundle:Account')->findClientsWorkedWith($entity);
        $clients_repeat_worked = $em->getRepository('VlanceAccountBundle:Account')->findClientsRepeatWorkedWith($entity);
        if((count($clients_worked) + count($clients_repeat_worked)) == 0){
            $rehire_rate = 0;
        } else{
            $rehire_rate = count($clients_repeat_worked) / count($clients_worked);
        }
        
        return array(
            'entity'        => $entity, 
            'jobs_worked'   => $jobs_worked, 
            'client_worked' => $clients_worked,
            'success_rate'  => $success_rate,
            'rehire_rate'   => $rehire_rate
        );
    }
    
    /**
     * @Route("/a/summary/client", name="account_summary_client")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/client/content_new:summary.html.php",engine="php")
     */
    public function summaryClientAction(Request $request) {
        $id = $this->get('session')->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        //Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        return array('entity' => $entity);
    }

    /**
     * Status: Obsoleted on 09/05/2016
     * 
     * @Route("/a/profile", name="account_profile")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function profileAction() {
        $id = $this->get('session')->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }

        $missing_info = $entity->getMissingVerifyInfo();
        if(count($missing_info) > 0){
            foreach($missing_info as $key => $info){
                $missing_info[$key] = $this->get('translator')->trans('controller.job.showFreelanceJob.' . $info, array(), 'vlance');
            }
        }
        return array(
            'number_percent' => $entity->getVerifyInfoPercent(),
            'id' => $id,
            'account' => $entity,
            'missingFields' => implode(", ", $missing_info)
        );
    }

    /**
     * 
     * @Route("/a/loginfb", name="login_facebook")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function loginFacebookAction(Request $request) {        
        // Step 1: Ask for authentication code
        $config = array();
        $config['appId'] = $this->container->parameters['vlance_system']['facebook']['app_id'];
        $config['secret'] = $this->container->parameters['vlance_system']['facebook']['app_secret'];
        $referer = $request->headers->get('referer');
        if ($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
        // Step 2: Got authentication code,
        if (isset($_GET['code'])) {
            // Try to log in to facebook, ask for FB permission
            require_once __DIR__ . '/../Lib/facebook-php-sdk/src/facebook.php';
            $facebook = new \Facebook($config);           
            try {
                $res = file_get_contents("https://graph.facebook.com/oauth/access_token?"
                        . "client_id=" . $config['appId']
                        . "&redirect_uri=" . $this->container->get('router')->generate('login_facebook', array(), true)
                        . "&client_secret=" . $config['secret']
                        . "&code=" . $_GET['code']);
                
                // Logged in with FB, got FB permission
                // Start getting info from FB account for creating new vLance account
                // or login in to vLance with fb account.
                // exemple of $res = "access_token=CAACzdm4Ld0kBAOgsL06BrCmj8uAeTlDSilzW9dZCu2gZAigO4V9R59Bowet2k4js6J".
                //                   "X3VCereBAsOnVY33DyI70MY6dDJ4h071GvTLFMQ2ToeosnOrXb1oBnKZBawL2YGvxymZCcLZAnFgl7BT".
                //                   "OSiWVkDKV4yvx4fb75MpDyd4L74yASt2SWy&expires=5183845"
                if (preg_match('`"access_token":"([a-zA-Z0-9]+)","token_type":"([a-z]+)","expires_in":([0-9]+)`', $res, $matches)) {
                    // Do login with facebook
                    $facebook->setAccessToken($matches[1]);
                    // Get all information from connected Facebook profile
                    $fb = $facebook->api("/me");
                    $em = $this->getDoctrine()->getManager();

                    // Get vLance account that matches this facebook id
                    /* @var $entity \Vlance\AccountBundle\Entity\Account */
                    $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('facebookId' => $fb['id']));
                    // If Facebook account has not been linked to any account on vLance, link with or create new one.
                    if (!is_object($entity)) {
                        $em = $this->getDoctrine()->getManager();

                        // If facebook account is created by phone number (without email),
                        // use facebook email address instead.
                        $email = $fb['id'] . "@facebook.com";
                        if (isset($fb['email'])) {
                            $email = $fb['email'];
                        } elseif (isset($fb['username'])) {
                            $email = $fb['username'] . "@facebook.com";
                        }

                        $entity = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('email' => $email));
                        // There is no account matchs fb account's email.
                        if (!is_object($entity)) {
                            // Create new account from fb account information
                            //                        echo "Đang tạo tài khoản";
                            $user = new Account();
                            $user->setFacebookId($fb['id']);
                            $user->setAccessTokenFace($matches[1]);
                            $user->setFullName($fb['name']);
                            $user->setEmail($email);
                            $user->setUsername($email);
                            if (isset($fb['link'])) {
                                $user->setWebsite($fb['link']);
                            }
                            $user->setPlainPassword($fb['id']);
                            // $user->setType('freelancer');
                            $user->setEnabled(true);

                            $em->persist($user);

                            // Call event REGISTRATION_COMPLETED which is used to create user cash
                            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                            $dispatcher = $this->container->get('event_dispatcher');

                            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $this->getRequest(), new Response()));

                            $em->flush();
                            
                            /** Tracking Event "Registration" **/
                            // Mixpanel
                            $this->get('vlance_base.helper')->trackAll($user, "Registration", array("channel" => "Facebook"));
                            // Google Adwords
                            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                                array('type' => 'html', 'html' =>
                                    '<!-- Google Code for Registration Conversion Page -->
                                    <script type="text/javascript">
                                    /* <![CDATA[ */
                                    var google_conversion_id = 925105271;
                                    var google_conversion_language = "en";
                                    var google_conversion_format = "3";
                                    var google_conversion_color = "ffffff";
                                    var google_conversion_label = "hRPxCO68hFoQ9_iPuQM";
                                    var google_remarketing_only = false;
                                    /* ]]> */
                                    </script>
                                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                    </script>
                                    <noscript>
                                    <div style="display:inline;">
                                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?label=hRPxCO68hFoQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                                    </div>
                                    </noscript>'
                                )
                            ));
                            // Facebook
                            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                                array('type' => 'html', 'html' =>
                                    '<!-- Facebook Conversion Code for Registrations -->
                                    <script>(function() {
                                      var _fbq = window._fbq || (window._fbq = []);
                                      if (!_fbq.loaded) {
                                        var fbds = document.createElement("script");
                                        fbds.async = true;
                                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                                        var s = document.getElementsByTagName("script")[0];
                                        s.parentNode.insertBefore(fbds, s);
                                        _fbq.loaded = true;
                                      }
                                    })();
                                    window._fbq = window._fbq || [];
                                    window._fbq.push(["track", "6023541940285", {"value":"0.00","currency":"VND"}]);
                                    </script>
                                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023541940285&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                                )
                            ));
                            /** END Tracking Event "Registration" **/

                            // Do the login into recently created account
                            $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
                            $this->get("security.context")->setToken($token);

                            // Fire the login event
                            // Logging the user in above the way we do it doesn't do this automatically
                            $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);

                            $tmp = file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=account&ea=register&el=facebook&cs=web&cm=none&cn=none');

                            // Redirect to after login page
                            return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                        }

                        // Link fb account with current account (of which using the same email)
                        else {
                            // Update account with facebook id
                            $entity->setFacebookId($fb['id']);
                            $entity->setAccessTokenFace($matches[1]);
                            $em->persist($entity);
                            $em->flush();
                            
                            // Mixpanel
                            $this->get('vlance_base.helper')->trackAll(NULL, "Link FB account");
                    
                            // Neu $event_face = 1 thy add them Event cho facebook: (thêm và head)
                            $_SESSION['after_new_acc'] = TRUE;
                    
                            // Do the login into recently created account
                            $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());
                            $this->get("security.context")->setToken($token);

                            // Fire the login event
                            // Logging the user in above the way we do it doesn't do this automatically
                            $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                            $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);

                            // Redirect to after login page
                            return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                        }
                    }
                    if ($entity->getAccessTokenFace() == '') {
                        $entity->setAccessTokenFace($matches[1]);
                        $em->persist($entity);
                        $em->flush();
                    }
                    // Else, fb is already linked with acc, do login with this facebook account
                    // Do the login into recently created account
                    $token = new UsernamePasswordToken($entity, $entity->getPassword(), "main", $entity->getRoles());
                    $this->get("security.context")->setToken($token);

                    // Fire the login event
                    // Logging the user in above the way we do it doesn't do this automatically
                    $event_login = new InteractiveLoginEvent($this->getRequest(), $token);
                    $this->get("event_dispatcher")->dispatch("security.interactive_login", $event_login);
                    $this->get("event_dispatcher")->dispatch("onImplicitLogin", $event_login);  //HIEP đăng nhập facebook sau khi đã đăng kí
                    
                    // Redirect to after login page
                    return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->getRequest()->headers->get('referer'))), true));
                } else { //TODO Chỗ này vẫn còn phải giải quyết khi có lỗi nhận đc code
//                    var_dump('Code');die;
                }
            } catch (\Exception $e) {
//                var_dump('Failed');die;
                return $this->redirect($this->generateUrl('vlance_homepage'));
            }
        }
        return new RedirectResponse($referer);
    }

    /**
     * Form lay thong tin nguoi dang nhap
     * 
     * @Route("/a/after-login/{referer}", name="account_login_after", requirements={"referer" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function loginAfterAction(Request $request, $referer = "") {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        $referer_url = urldecode($referer);
        if ($account->getType() == Account::TYPE_VLANCE_SYSTEM || $account->getType() == Account::TYPE_VLANCE_ADMIN) {
            if ($referer_url) {
                return $this->redirect($referer_url);
            } else {
                return $this->redirect($this->generateUrl('vlance_homepage'));
            }
        }
        $this->get('session')->set('id', $account->getId());
        $cat = $account->getCategory();
        $type = $account->getType();
        $city = $account->getCity();
        if (is_object($cat) && is_object($city) && $type) {
            if ($referer_url) {
                return $this->redirect($referer_url);
            }
            if ($type == Account::TYPE_FREELANCER) {
                return $this->redirect($this->generateUrl('jobs_bid_project'));
            } else {
                return $this->redirect($this->generateUrl('jobs_create_project'));
            }
        }
        $editForm = $this->createForm('vlance_accountbundle_loginaftertype', $account);
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "After login page");
            
        return array(
            'entity'    => $account,
            'edit_form' => $editForm->createView(),
            'referer'   => $referer_url
        );
    }

    /**
     * Update account
     * @Route("/a/cap-nhat-tai-khoan/{referer}", name="account_update_info", requirements={"referer" = ".+"})
     * @Method({"POST"})
     * @Template("VlanceAccountBundle:Account:loginAfter.tpl.php",engine="php")
     */
    public function updateInfoAction(Request $request, $referer = "") {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $entity = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm('vlance_accountbundle_loginaftertype', $entity);

        $postData = $request->request->get($form->getName());
        if (isset($postData['email'])) {
            $rex = preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $postData['email']);
            if (!$rex) {
                $postData['email'] = $entity->getEmail();
            }
        }
        $email = preg_match('/@facebook.com/', $entity->getEmail());
        if (!$email) {
            $postData['email'] = $entity->getEmail();
        }
        $request->request->set($form->getName(), $postData);
        $form->bind($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Updated basic information");
                            
            $referer_url = urldecode($referer);
            if ($referer == '') {
                $referer_url = $this->generateUrl('account_show_freelancer', array('hash' => $entity->getHash(), 'mesage' => '1'));
            }
            return $this->redirect($referer_url);
        }

        return array(
            'edit_form' => $form->createView(),
            'entity' => $entity,
        );
    }

    /**
     * Display all account the fond
     *
     * @Route("/a/search/{filters}", name="account_search", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account:index.html.php", engine="php")
     */
//    public function searchAction(Request $request, $filters = '') {
//        $params = $this->get('vlance_base.url')->pasteParameter($filters);
//        $find = $request->get('search');
//        $em = $this->getDoctrine()->getManager();
//        $pager = null;
//        try {
//            $pager = $em->getRepository('VlanceAccountBundle:Account')->searchAccounts($find, $params);
//        }
//        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
//            
//        }
//        catch (\Exception $e) {
//            return $this->redirect($this->generateUrl('vlance_homepage'));
//        }
//        
//        $pagerView = new DefaultView(new PagerTemplate($params, 'account_search'));
//        $options = array('proximity' => 3);
//       
//        return array(
//            'pager' => $pager,  
//            'pagerView' => $pagerView,
//            'entities' => $pager->getCurrentPageResults(),
//        );
//    }

    /**
     * 
     * @Route("/a/sendmail/{id}/{job_id}", name="account_send_mail")
     * @Method("POST")
     * @Template(engine="php")
     * 
     */
    public function sendMailAction($id, $job_id = '') {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $acc = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        if (!$acc) {
            return $this->redirect($this->generateUrl("404_page"), 302);
        }

        $email = $acc->getEmail();
        $from_email = $this->container->getParameter('from_email');
        $message_mail = \Swift_Message::newInstance()
                ->setSubject($title)
                ->setFrom($from_email)
                ->setTo($email)
                ->setBody(
                $this->renderView(
                        'VlanceAccountBundle:Registration:email_sys.html.twig', array('name' => $acc->getFullName(), 'type' => 'invitefreelancer', 'job_id' => $job_id)
                ), 'text/html'
                )
        ;

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.sendMail.success', array(), 'vlance'));
        return $this->redirect($this->generateUrl('freelancer_list'));
    }

    /**
     * Form invite freelancer
     * @Route("/a/new-mail/{id}", name="account_new_mail")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account:newMail.html.php", engine="php")
     */
    public function newMailAction($id) {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $jobs = $em->getRepository('VlanceJobBundle:Job')->findBy(array('account' => $user->getId(), 'status' => Job::JOB_OPEN));
        if (!$jobs) {
            return new Response('Bạn chưa có việc làm. Hãy tạo công việc để có thể mời freelancer.');
        }

        $data = array();
        foreach ($jobs as $job) {
            $data[] = $job->getTitle() . ' Id-' . $job->getId();
        }
        return array('id' => $id, 'jobs' => $data);
    }

    /**
     * Status: Obsoleted on 09/05/2016
     * 
     * @Route("/a/popup", name="account_popup")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account:popup.html.php",engine="php")
     */
    public function popupAction() {
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response("");
        }
        /** @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        // if this user can't be notified, don't display popup
        if ($account->getNotify() != Account::WILL_NOTIFY) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        $account->setNotify(Account::NOTIFIED);
        $em->persist($account);
        $em->flush();
        return array(
            'account' => $account
        );
    }
    /**
     * @Route("/a/newfeatures", name="popover_new_features")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Newfeatures:new.html.php",engine="php")
     */
    public function popovernewfeaturesAction() {
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response("");
        }
        
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        return array(
            'account' => $account
        );
    }

    /**
     * @Route("/a/certificate", name="account_request_certificate")
     * @Method("GET")
     */
    public function requestCertificateAction() {
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
//        if (($account->getNotify() != Account::CAN_NOTIFY) && ($account->getNotify() != Account::NOTIFIED)) {
//            return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $account->getHash())));
//        }
        // calculate the percent of profile
        if ($account->getVerifyInfoPercent() < 100) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.certificate.request.notenough', array(), 'vlance'));
            return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $account->getHash())));
        }
        if ($account->getIsRequested() == Account::REQUESTED) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.certificate.request.inprogress', array(), 'vlance'));
            return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $account->getHash())));
        }
        $em = $this->getDoctrine()->getManager();
        $account->setIsRequested(true);
        $account->setLastRequest(new \DateTime(date('Y-m-d h:i:s', time())));
        $account->setDateRequest(new \DateTime(date('Y-m-d h:i:s', time())));
        $em->persist($account);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.certificate.request.success', array(), 'vlance'));
        return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $account->getHash())));
    }

    /**
     * 
     * @Route("/a/certificate-admin/{_route_params}", name="account_certificate_admin")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account:certificate_admin.html.php",engine="php")
     */
    public function certificateAdminAction(Request $request, $_route_params = "") {
        $_route_params = unserialize($_route_params['_route_params']);
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository("VlanceAccountBundle:Account")->findOneByHash($_route_params["hash"]);
        if (!$account->getIsRequested()) {
            return new Response("");
        }
        return array(
            'account' => $account,
            '_csrf_token' => $this->get('form.csrf_provider')->generateCsrfToken($this->generateUrl("Vlance_AccountBundle_AdminAccount_object", array("pk" => $account->getId(), "action" => "refuse"))),
            "refulse_url" => $this->generateUrl("Vlance_AccountBundle_AdminAccount_object", array("pk" => $account->getId(), "action" => "refuse")),
            "accept_url" => $this->generateUrl("Vlance_AccountBundle_AdminAccount_object", array("pk" => $account->getId(), "action" => "certificate"))
        );
    }
    
    /**
     * @Route("/a/update-feature-invite", name="update_feature_invite")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function updateInviteAction() {
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /** @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        //update field feature_invite
        $account->setFeatureInvite(0);
        $em = $this->getDoctrine()->getManager();
        $em->persist($account);
        $em->flush();
        return $this->redirect($this->generateUrl('email_invite'));
        
    }

    /**
     * @Route("/a/moi-ban-be/{accesstoken}", name="email_invite", requirements={"accesstoken" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:EmailInvite:email_invite_new.html.php",engine="php")
     */
    public function emailinviteAction(Request $request, $accesstoken = null) {
        // NEW FORM
        // Template("VlanceAccountBundle:EmailInvite:email_invite_new.html.php",engine="php")
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /** @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $acc = $em->getRepository('VlanceAccountBundle:Account')->find($account->getId());

        $listemails = $em->getRepository('VlanceAccountBundle:EmailInvite')->findBy(array('account' => $acc));
        $emails = array();
        $session = $request->getSession();
        if($session->get('service') == 'contact' && isset($accesstoken)) {
            $session->remove('service');
//            $client_id = $this->container->parameters['vlance_system']['google']['client_id'];
//            $client_secret = $this->container->parameters['vlance_system']['google']['app_secret'];
//            $redirect_uri = $this->generateUrl('login_google', array(), true);
//            $auth_code = $_GET["code"];
//            if(isset($auth_code)){
                //include google api file
//                require_once __DIR__.'/../Lib/Google/Client.php';
//                require_once __DIR__.'/../Lib/Google/Service/Oauth2.php';
//                require_once __DIR__.'/../Lib/Google/Service/Books.php';
//                $fields=array(
//                    'code'=>  urlencode($auth_code),
//                    'client_id'=>  urlencode($client_id),
//                    'client_secret'=>  urlencode($client_secret),
//                    'redirect_uri'=>  urlencode($redirect_uri),
//                    'grant_type'=>  urlencode('authorization_code'),
//                );
//                $post = '';
//                foreach($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }
//                $post = rtrim($post,'&');
//
//                $curl = curl_init();
//                curl_setopt($curl,CURLOPT_URL,'https://accounts.google.com/o/oauth2/token');
//                curl_setopt($curl,CURLOPT_POST,5);
//                curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
//                curl_setopt($curl, CURLOPT_RETURNTRANSFER,TRUE);
//                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
//                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
//                $result = curl_exec($curl);
//                curl_close($curl);
//
//                $response =  json_decode($result);
                try {
//                    $accesstoken = $response->access_token;
                    $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=2560&v=3.0&oauth_token='.$accesstoken;
                    $curl = curl_init();
                    $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

                    curl_setopt($curl,CURLOPT_URL,$url);	//The URL to fetch. This can also be set when initializing a session with curl_init().
                    curl_setopt($curl,CURLOPT_RETURNTRANSFER,TRUE);	//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
                    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,5);	//The number of seconds to wait while trying to connect.	

                    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);	//The contents of the "User-Agent: " header to be used in a HTTP request.
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);	//To follow any "Location: " header that the server sends as part of the HTTP header.
                    curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE);	//To automatically set the Referer: field in requests where it follows a Location: redirect.
                    curl_setopt($curl, CURLOPT_TIMEOUT, 10);	//The maximum number of seconds to allow cURL functions to execute.
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);	//To stop cURL from verifying the peer's certificate.
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

                    $xmlresponse = curl_exec($curl);
                    curl_close($curl);
                    if((strlen(stristr($xmlresponse,'Authorization required'))>0) && (strlen(stristr($xmlresponse,'Error '))>0))
                    {
                        $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.gmail.error',array(),'vlance'));
                        return $this->redirect($this->generateUrl('email_invite'));
                    }
                    $xml = new \SimpleXMLElement($xmlresponse);
                    $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');
                    $result = $xml->xpath('//gd:email');
                    /*foreach ($result as $title) {
                        $emails["{$title->attributes()->address}"] = $title->attributes()->address;
                    }*/
                    foreach ($result as $title) {
                        $emails[] = "{$title->attributes()->address}";
                    }
                } catch (\Exception $e) {
                   /* @var $logger \Psr\Log\LoggerInterface */
                   $logger = $this->get("monolog.logger.google");
                   $logger->info("Get gmail contact error");
                   $logger->info(print_r($e->getMessage(), true));

                   $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.gmail.error',array(),'vlance'));
                   return $this->redirect($this->generateUrl('email_invite'));
               }
//          }
        
        }   
        return array(
            'account'       => $account,
            'listemails'    => $listemails,
            'emails'        => $emails,
            'url_invite'    => $this->generateUrl('account_register_by_referrer', array('referrer_id' => $account->getId()), true),
        );
    }

    /**
     * tao form moi ban be.
     *
     * @Route("/a/emailinvite/new/{accid}", name="emailinvite_new")
     * @Method("GET")
     * @Template("VlanceAccountBundle:EmailInvite:form_email_invite_new.html.php",engine="php")
     */
    public function newemailinviteAction($accid) {
        // NEW FORM
        // Template("VlanceAccountBundle:EmailInvite:form_email_invite_new.html.php",engine="php")
        
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_use = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($accid);
        if (!$entity_acc || $entity_acc !== $current_use) {
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.portfolio.create.notpermission', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
//        tao form
        $form = $this->createFormBuilder()
                ->add('list_email', 'textarea', array(
                    // NEW FORM
                    'label' => false, 
                    'required' => true,
                    'wrapper_class' => 'description_inviteemail',
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'placeholder' => 'forminvite.description',
                        'data-toggle' => 'popover',
                        'data-placement' => 'top',
                        'data-content' => 'forminvite.data_content',
                        'data-trigger' => 'focus',
                        'data-html' => true,
                        'class' => 'popovers-input span12')
                ))
                // NEW FORM
                /*->add('message', 'textarea', array(
                    'label' => 'forminvite.message.label',
                    'required' => true,
                    'wrapper_class' => 'message_inviteemail',
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'placeholder' => 'forminvite.message',
                        'data-toggle' => 'popover',
                        'data-placement' => 'top',
                        'data-content' => 'forminvite.data_content',
                        'data-trigger' => 'focus',
                        'data-html' => true,
                        'class' => 'popovers-input span12')
                ))*/
                ->getForm();
        return array(
            'accid' => $accid,
            'form' => $form->createView(),
        );
    }

    /**
     * xu ly request add db.
     *
     * @Route("/a/emailinvite/create/{accid}", name="email_invite_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createemailinviteAction(Request $request, $accid) {
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_use = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $condition = array(
            'id' => array('0' => 'eq', '1' => "'" . $accid . "'"),
        );
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.email'), $condition);
        if (!$entity_acc || $entity_acc[0]['email'] !== $current_use->getEmail()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.email_invite.create.notpermission', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }

        $form = $this->createFormBuilder()
                ->add('list_email', 'textarea', array(
                    'label' => false,
                    'required' => true,
                    'wrapper_class' => 'description_inviteemail',
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'placeholder' => 'forminvite.description',
                        'data-toggle' => 'popover',
                        'data-placement' => 'top',
                        'data-content' => 'forminvite.description',
                        'data-trigger' => 'hover',
                        'class' => 'popovers-input span12')
                ))
                ->getForm();

//        $form->handleRequest($request);

        $form->bind($request);
        if ($form->isValid()) {
            $data = $form->getData();
//            lay chuoi email nhap vao
            $list_email_invite = $data['list_email'];
            if (!$list_email_invite) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.email_invite.create.email_empty', array(), 'vlance'));
                return $this->redirect($this->generateUrl('email_invite'));
            }
//            replace chuoi email nhap vao cach nhau = dau phay
            $patterns = array('/;/', '/,/', '/ /');
            $replacements = array(',', ',', ',');
            $list_email_invite = preg_replace($patterns, $replacements, $list_email_invite);            
//            chuyen thanh mang
            $arr_email = explode(",", $list_email_invite);
            $arr_email_invite = array();
            //Xoa gia tri rong
            foreach($arr_email as $value){
                if($value !== ""){
                    $arr_email_invite[] = $value;
                }
            }
            //mẫu biểu thức email
            $pattern = "/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/";
            $done = 0;
            $bcc[] = $this->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
            // Send email to account invited
            $now = date('Y-m-d H:i:s', time());
            $jobs =$em->getRepository('VlanceJobBundle:Job')->getFields(array('j.id'), array(
                                                                                            'status' => array(0 => 'eq', 1=> Job::JOB_OPEN),
                                                                                            'publish' => array(0 => 'eq' , 1 => Job::PUBLISH),
                                                                                            'closeAt' => array(0 => 'gte', 1 => "'" . $now . "'"),
                                                                                        )
                                                                       );
            $email_noreply = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
            
            $avatar = ResizeImage::resize_image($current_use->getFullPath(), '88x88', 88, 88, 1);
            if(!$avatar){
                $avatar = ROOT_URL . "/" .'img/unknown.png';
            }
            
            /* Tracking */
            $tracking_properties = array(
                // for mixpanel
                'invitor_id'    => $current_use->getId(),
                // for ga
                'ec'            => 'inviteemail',   // event category
                'ea'            => 'open',          // event action
                'el'            => 'invitee',       // event label
            );

            $mixpanel_tracking  = $this->get('vlance_base.helper')->buildTrackingCodeEmailMixpanel("Invitee Open Email", $tracking_properties, $current_use->getId());
            $ga_tracking = $this->get('vlance_base.helper')->buildTrackingCodeEmailGa($tracking_properties, $current_use->getId());
            /* END Tracking */
            
            $email_context = array(
                'account'   => $current_use, 
                'avatar'    => $avatar, 
                'num_job'   => count($jobs), 
                'url_ai'    => $this->generateUrl('account_register_by_referrer', array('referrer_id' => $current_use->getId()), true),
                'pixel_tracking'=> array(
                                    'mixpanel'  => $mixpanel_tracking,
                                    'ga'        => $ga_tracking
                                    ),
                    );
            $template = "VlanceAccountBundle:Email:account_invite.html.twig";
            foreach ($arr_email_invite as $email) {
                //nếu chuỗi so sánh hợp lệ, hàm preg_match() trả về 1, ngược lại trả về 0
                if (preg_match($pattern, $email) == 1) {
//                    kiem tra trong acc da co email nay chua
                    $condition_acc = array(
                        'email' => array('0' => 'eq', '1' => "'" . $email . "'"),
                    );
                    $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.email'), $condition_acc);
//                    kiem tra trong email invite da co email nay chua
                    $condition = array(
                        'email' => array('0' => 'eq', '1' => "'" . $email . "'"),
                    );
                    $entity_email_invite = $em->getRepository('VlanceAccountBundle:EmailInvite')->getFields(array('e.email'), $condition);
                    if(!$entity_email_invite && !$entity_acc){
                        $done = $done + 1;
                        $newEmail = new EmailInvite();
                        $newEmail->setAccount($current_use);
                        $newEmail->setEmail($email);
                        //send email
                        $result = $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_noreply, $email, $bcc);
                        $MessageId = $result->get('MessageId');
                        if (isset($MessageId)) {
                            $newEmail->setMessageId($result->get('MessageId'));
                        }
                        $em->persist($newEmail);
                        
                        //Mixpanel
                        $this->get('vlance_base.helper')->trackAll(NULL, "Invitee Receive Email");
                    }else {
//                        $error = $error + 1;
//                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('email da co trong du lieu', array('%email%' => $email), 'vlance'));
                    }
                } else {
//                    $error = $error + 1;
//                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('email ko hop le', array('%email%' => $email), 'vlance'));
                }
            }
            $em->flush();
            
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Invitor Send Email", array('channel' => 'manual', 'count' => $done));
            
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.email_invite.create.message_success', array('%done%' => $done,'%count_email%'=> count($arr_email_invite)), 'vlance'));
            return $this->redirect($this->generateUrl('email_invite'));
        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.email_invite.create.message_notvalidate', array(), 'vlance'));
            return $this->redirect($this->generateUrl('email_invite'));
        }
        return array(
            'accuse' => $current_use,
            'form' => $form->createView(),
        );
    }

    /**
     * 
     * @Route("/freelancers/{filters}", name="freelancer_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account:index.html.php",engine="php")
     */
    public function listFreelancerAction(Request $request, $filters = "") {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "freelancer_list",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        /**
         * Check if this filters use old format, then convert it to new format
         */
        $params = $this->get('vlance_base.url')->pasteParameterOldFormat($filters);
        if (isset($params["cat.path"]) || isset($params['cat.id']) || isset($params['city.id'])) {

            if (isset($params["cat.path"])) {
                /* @todo Will remove this code because we will remove the shit character % in url */
                $path = substr($params['cat.path'], 0, strpos($params['cat.path'], "%"));
                $hash = $em->getRepository('VlanceAccountBundle:Category')->getHashFromField(array('path' => $path));
                $params['cpath'] = $hash["hash"];
                unset($params['cat.path']);
            }

            if (isset($params["cat.id"])) {
                $ids = is_array($params['cat.id']) ? $params['cat.id'] : array($params['cat.id']);
                $chash = array();
                foreach ($ids as $id) {
                    $hash = $em->getRepository('VlanceAccountBundle:Category')->getHashFromField(array('id' => $id));
                    $chash[] = $hash["hash"];
                }
                $params['chash'] = $chash;
                unset($params['cat.id']);
            }

            if (isset($params["city.id"])) {
                $ids = is_array($params['city.id']) ? $params['city.id'] : array($params['city.id']);
                $cities = array();
                foreach ($ids as $id) {
                    $city = $em->getRepository('VlanceBaseBundle:City')->getHashFromField(array('id' => $id));
                    $cities[] = $city['hash'];
                }
                $params['city'] = $cities;
                unset($params['city.id']);
            }
            return $this->redirect($this->generateUrl('freelancer_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params))), 301);
        }

        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        if (!empty($params) && !isset($params["cpath"]) && !isset($params['chash']) && !isset($params['city']) && !isset($params['kynang']) && !isset($params['page']) && !isset($params['status'])) {
            return $this->redirect($this->generateUrl('404_page'), 301);
        }

        $this->get('session')->set('filter', $params);
        $this->get('session')->set('route', $request->attributes->get('_route'));
        $this->get('session')->set('call_func', 'getfilterFreelancer');
        // set session for city filter: call function to get number of freelancer
        $this->get('session')->set(\Vlance\BaseBundle\Entity\CityRepository::SESS_FUNC, \Vlance\BaseBundle\Entity\CityRepository::FREELANCER_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\SkillRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\SkillRepository::FREELANCER_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\CategoryRepository::FREELANCER_FUNC);
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceAccountBundle:Account')->getPager($params);
            
            /* get data meta description */
            $breadcrumbs = array();
            $breadcrumbs[] = array('title' => 'Freelancer', 'params' => '');
            $implode_cat = array();
            $implode_city = array();
            $implode_skill = array();
            
            if (isset($params['cpath']) || isset($params['chash'])) {
                $des_cats = $em->getRepository('VlanceAccountBundle:Category')->getFieldFromParam($params);
                foreach ($des_cats as $des_cat) {
                    $implode_cat[] = $des_cat['title'];
                }
                $breadcrumbs[] = array('title' => $des_cats[count($des_cats) - 1]['title'], 'params' => $des_cats[count($des_cats) - 1]['hash']);
            }
            if (isset($params['city'])){
                $cities = $em->getRepository('VlanceBaseBundle:City')->getFieldFromParam($params);
                foreach($cities as $c){
                    $implode_city[] = $c['name'];
                }
            } 
            if (isset($params['kynang'])){
                $skills = $em->getRepository('VlanceAccountBundle:Skill')->getFieldFromParam($params);
                foreach($skills as $s){
                    $implode_skill[] = $s['title'];
                }
            }
                       
            $job_domain = implode(', ', array_merge($implode_cat,$implode_skill));
            if(strlen($job_domain) == 0){
                $job_domain = implode(', ', $implode_city);
            } else{
                if(strlen(implode(', ', $implode_city)) >0){
                    $job_domain .= " tại " . implode(', ', $implode_city);
                } else{
                    $job_domain .= implode(', ', $implode_city);
                }
            }
            $title = 'Freelancer ' . (strlen($job_domain)>2?$job_domain:"Việt Nam");
            $meta_description = $this->get('translator')->trans('list_freelancer.description', array('%number%' => $pager->getNbResults(), '%linhvuc%' => $job_domain), 'vlance');
            $filteraccpage = count($pager->getCurrentPageResults());
            $_SESSION['filteraccpage'] = $filteraccpage;
        } catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        } catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        } catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        } catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        } catch (\Exception $e) {
            //die($e->getMessage());
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }

        $pagerView = new DefaultView(new PagerTemplate($params, 'freelancer_list'));
        $options = array('proximity' => 3);

        $freelancer_repo = $em->getRepository('VlanceAccountBundle:Account')->statistic(array('id' => 'count'), array('type' => array(0 => "isNotNull")));
        $nb_freelancer = (int)($freelancer_repo['id'] / 1000)+2;
        
        // Noindex, nofollow các trang có pagination, trừ những trang ko có gì ngoài tham số page (để đảm bảo vẫn index tất cả các freelancer)
        $index_follow = 'noindex,nofollow';
        if(count($params) <= 1){
            if(!isset($params['page']) || $params['page'] != 1){
                // page n, without other filters OR page 1, with one filter
                $index_follow = 'index,follow';
            }
        } elseif(count($params) > 1){
            if(!isset($params['page'])){
                // first page, with multiple filters
                $index_follow = 'index,follow';
            }
        }
        
        $entities = $pager->getCurrentPageResults();
        $list_job_invite = array();
                
        foreach ($entities as $account){
            // Job invites sent to freelancers
            $job_invites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('account' => $account));
            $list_job_invite[$account->getId()] = $job_invites;
        }
        
        // Switching between "All freelancers" list and "Verified freelancers" list
        $listing_switch = array(
            'active'    => array(
                'name'  => "",
                'count' => $pager->getNbResults(),
                'url'   => $this->generateUrl('freelancer_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params)))
            )
        );
        $params_switch = $params;
        if(isset($params['status']) && $params['status'] == "xac-thuc"){
            $listing_switch['active']['name'] = 'xac-thuc';                
            unset($params_switch['status']);
            $listing_switch['switch'] = array(
                'name'  => 'all',
                'count' => $em->getRepository('VlanceAccountBundle:Account')->getPager($params_switch)->getNbResults(),
                'url'   => $this->generateUrl('freelancer_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params_switch))));
        } else{
            $listing_switch['active']['name'] = 'all';
            $params_switch['status'] = "xac-thuc";
            $listing_switch['switch'] = array(
                'name'  => 'xac-thuc',
                'count' => $em->getRepository('VlanceAccountBundle:Account')->getPager($params_switch)->getNbResults(),
                'url'   => $this->generateUrl('freelancer_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params_switch))));
        }
        // END Switching between "All freelancers" list and "Verified freelancers" list
        
        return array(
            'pager'             => $pager,
            'pagerView'         => $pagerView,
            'entities'          => $pager->getCurrentPageResults(),
            'breadcrumbs'       => $breadcrumbs,
            'title'             => $title,
            'meta_title'        => ($filters=="")?"Thuê freelancer giá tốt nhất Việt Nam":$title,
            'meta_description'  => ($filters=="")?"Đăng dự án, nhận báo giá Miễn Phí trong vài phút từ >". $nb_freelancer .".000 Freelancer chuyên nghiệp nhất Việt Nam, từ 50.000đ/việc hoàn thành, sửa chữa đến khi hài lòng. Quản lý công việc Online. 100% chủ dự án hài lòng khi xong việc và được cung cấp nhiều dịch vụ hậu mãi, bảo trì miễn phí, giá rẻ từ freelancer. Hỗ trợ thanh toán Paypal và 20 ngân hàng VN.":str_replace('>', '*', addslashes(JobCalculator::replacement(strip_tags($meta_description)))),
            'no_param'          => (count($params) == 0)?true:false,
            'index_follow'      => $index_follow,
            'list_job_invite'   => $list_job_invite,
            'listing_switch'    => $listing_switch
        );
    }
    
    /**
     * Displays a "feature freelancer" block in freelancer list
     *
     * @Route("/block/feature_freelancer", name="block_featurefreelancer")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Account/block:feature_freelancer.html.php",engine="php")
     */
    public function blockFeaturefreelancerAction()
    {
         /* @var Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $freelancers = $em->getRepository('VlanceAccountBundle:Account')
                ->getFeatureFreelancers();
        
        $list_job_invite = array();
        foreach ($freelancers as $account){
            // Job invites sent to freelancers
            $job_invites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('account' => $account));
            $list_job_invite[$account->getId()] = $job_invites;
        }
        
        return array(
            'acc'               => $current_user,
            'feat_freelancers'  => $freelancers,
            'list_job_invite'   => $list_job_invite
        );
    }

    protected function getEngine() {
        return $this->container->getParameter('fos_user.template.engine');
    }

    protected function getProfilePercent(Account $account) {
        // calculate the percent of profile
        $configs = array();
        $number = 0;
        if (isset($this->container->parameters['vlance_system']['profile_field'])) {
            $configs = $this->container->parameters['vlance_system']['profile_field'];
            foreach ($configs as $config) {
                $method = "get$config";
                $tmp = $account->$method();
                if (!is_null($tmp) && !empty($tmp)) {
                    $number++;
                }
            }
        }
        return round($number / count($configs) * 100, 2);
    }

    // REDIRECT - START    
    /**
     * Finds and displays a Account entity Alias.
     *
     * @Route("/a/ho-so/{hash}", name="account_show_alias", requirements={"mesage" = ".+"})
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:show.html.php",engine="php")
     */
    public function showAliasAction($hash) {
        return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $hash)), 301);
    }

    /**
     * Finds and displays a Account entity.
     *
     * @Route("/a/s/{id}", name="account_show")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:show.html.php",engine="php")
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Account')->find($id);
        $this->get('session')->set('id', $id);
        // Not exist
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }

        //This action will be redirected to showAliasAction()
        return $this->redirect($this->generateUrl('account_show_freelancer', array('hash' => $entity->getHash())), 301);
    }
    
    /**
     * Verify telephone number
     * @Route("/account/verify/{referer}", name="account_verify_information")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile:edit_verify_information.html.php",engine="php")
     */
    public function verifyTelephoneAction(Request $request, $referer = null)
    {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "account_verify_information",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Set Referer URL
        $referer = base64_encode(($referer == "redirect" && $request->headers->get('referer')) ? $request->headers->get('referer') : $this->generateUrl('account_basic_edit', array('id' => $auth_user->getId())));
        
        return array(
            'auth_user' => $auth_user,
            'referer'   => $referer
        );
    }
    
    /**
     * Displays a form to create a new ID number.
     *
     * @Route("/verify_id", name="verify_id")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/edit_verify_information:verify_id.html.php",engine="php")
     */
     public function verifyIdAction()
    {
        /* @var $current_user Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(!$current_user || !($current_user->getId())){
            return array();
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!is_object($entity_acc) || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }
        
        $is_requested = false;
        
        if($current_user->getPersonId() instanceof PersonId){  //edit form
            $personid = $em->getRepository('VlanceAccountBundle:PersonId')->find($current_user->getPersonId()->getId());
            if(!is_null($personid->getRequestedAt())){
                $is_requested = true;
            }
            if(!is_null($personid->getVerifyAt())){
                $form = $this->createForm('vlance_accountbundle_personidtype', $personid, array('disabled' => true));
            } else {
                $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
            }
        } else {    //create form
            $personid = new PersonId();
//            if($current_user->getIdCardNumber()){
//                $personid->setIdNumber($current_user->getIdCardNumber());
//            }
//            $personid->setFullName($current_user->getFullName());
//            $personid->setIssuedLocation($current_user->getCity()->getName());
            $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
        }
        
        return array(
            'acc'           => $current_user,
            'form'          => $form->createView(),
            'personid'      => $personid,
            'is_requested'  => $is_requested,
        );
     }
    
     /**
     * Creates a new ID number. When submit form
     *
     * @Route("/verify_id/submit", name="verify_id_submit")
     * @Method("POST")
     */
    public function verifyIdSubmitAction(Request $request)
    {
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!is_object($entity_acc) || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }

        // get date of birth
        $birth_post = $_POST['vlance_accountbundle_personidtype_dateOfBirth'];
        $birth_post = str_replace('/', '-', $birth_post);
        $birth_post = date('Y-m-d', strtotime($birth_post));
        $birthday = new \DateTime($birth_post);

        //check create or update
        if(!is_object($current_user->getPersonId())){      //create
            if($current_user->getCredit()->getBalance() < 1){
                $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('Bạn không đủ Credit để xác thực chứng mình nhân dân.',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            } else{
                // Store CreditExpenseTransaction in database
                $credit_trans = new CreditExpenseTransaction();
                $credit_trans->setCreditAccount($current_user->getCredit());
                $credit_trans->setPreBalance($current_user->getCredit()->getBalance());
                $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
                $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
                $credit_trans->setAmount(1);
                $credit_trans->setService(CreditExpenseTransaction::SERVICE_VERIFY_ID);
                $credit_trans->setDetail(json_encode(array('require_credit' => 1)));

                $credit_account = $current_user->getCredit();
                $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());

                // Set addtional tracking event properties
                $tracking_properties["credit_spent"] = 1;
                $this->get('vlance_base.helper')->trackAll($current_user, "Verify Id", $tracking_properties);

                $credit_trans->setPostBalance($credit_account->getBalance());
                $em->persist($credit_trans);
                $em->persist($credit_account);
            }

            $personid = new PersonId();
            $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
            $form->bind($request);
            if ($form->isValid()) {
                $personid->setAccount($current_user);
                $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                $em->persist($current_user);
                $em->persist($personid);
                $em->flush();
                
                $current_user->setBirthday($birthday);
                $current_user->setIdCardNumber($personid->getIdNumber());
                $em->persist($current_user);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_success',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            } else {
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_error',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        } else {                                //edit
            /* @var $personid PersonId */
            $personid = $em->getRepository('VlanceAccountBundle:PersonId')->find($current_user->getPersonId()->getId());
            $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
            $form->bind($request);
            if ($form->isValid()) {
                /* @var $uow \Doctrine\ORM\UnitOfWork */
                $uow = $em->getUnitOfWork();
                $uow->computeChangeSet($em->getClassMetadata(get_class($personid)), $personid);
                $changeSet = $uow->getEntityChangeSet($personid);
                if (count($changeSet)) {
                    $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                    $em->persist($current_user);
                }
                $em->persist($personid); 
                $em->flush();
                
                $current_user->setBirthday($birthday);
                $current_user->setIdCardNumber($personid->getIdNumber());
                
                if(!is_null($personid->getRequestedAt())){
                    $now = new \DateTime('now');
                    $personid->setRequestedAt($now);
                }
                
                $em->persist($current_user);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('profile.edit_verify_information.verify_id.update_success', array(), 'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            } else {
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_error',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        }
    }
    
     /**
     *
     * @Route("/verify_id/submit_request", name="verify_id_submit_request")
     * @Method("POST")
     */
    public function verifyIdSubmitRequestAction(Request $request)
    {
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!is_object($entity_acc) || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }

        // get date of birth
        $birth_post = $_POST['vlance_accountbundle_personidtype_dateOfBirth'];
        $birth_post_string = str_replace('/', '-', $birth_post);
        $birth_post_converted = date('Y-m-d', strtotime($birth_post_string));
        $birthday = new \DateTime($birth_post_converted);
        
        //check create or update
        if(!is_object($current_user->getPersonId())){      //create
            $personid = new PersonId();
            $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
            $form->bind($request);
            if ($form->isValid()) {
                if($current_user->getCredit()->getBalance() < 1){
                    $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('Bạn không đủ Credit để xác thực chứng mình nhân dân.',array(),'vlance'));
                    return $this->redirect($this->generateUrl('account_verify_information', array()));
                } else{
                    // Store CreditExpenseTransaction in database
                    $credit_trans = new CreditExpenseTransaction();
                    $credit_trans->setCreditAccount($current_user->getCredit());
                    $credit_trans->setPreBalance($current_user->getCredit()->getBalance());
                    $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
                    $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
                    $credit_trans->setAmount(1);
                    $credit_trans->setService(CreditExpenseTransaction::SERVICE_VERIFY_ID);
                    $credit_trans->setDetail(json_encode(array('require_credit' => 1)));

                    $credit_account = $current_user->getCredit();
                    $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());

                    // Set addtional tracking event properties
                    $tracking_properties["credit_spent"] = 1;
                    $this->get('vlance_base.helper')->trackAll($current_user, "Verify Id", $tracking_properties);

                    $credit_trans->setPostBalance($credit_account->getBalance());
                    $em->persist($credit_trans);
                    $em->persist($credit_account);
                }

                $personid->setAccount($current_user);
                $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                $em->persist($current_user);
                
                $current_user->setBirthday($birthday);
                $current_user->setIdCardNumber($personid->getIdNumber());
                $em->persist($current_user);
                $em->flush();
                
                $personid->setRequestedAt(new \DateTime());
                $personid->setRequested(true);
                $em->persist($personid);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_success',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            } else {
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_error',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        } else {                                //edit
            /* @var $personid PersonId */
            $personid = $em->getRepository('VlanceAccountBundle:PersonId')->find($current_user->getPersonId()->getId());
            $form = $this->createForm('vlance_accountbundle_personidtype', $personid);
            $form->bind($request);
            if ($form->isValid()) {
                /* @var $uow \Doctrine\ORM\UnitOfWork */
                $uow = $em->getUnitOfWork();
                $uow->computeChangeSet($em->getClassMetadata(get_class($personid)), $personid);
                $changeSet = $uow->getEntityChangeSet($personid);
                if (count($changeSet)) {
                    $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                    $em->persist($current_user);
                }
                $current_user->setBirthday($birthday);
                $current_user->setIdCardNumber($personid->getIdNumber());
                $em->persist($current_user);
                $em->flush();
                
                $personid->setRequestedAt(new \DateTime());
                $personid->setRequested(true);
                $em->persist($personid); 
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('profile.edit_verify_information.verify_id.update_success', array(), 'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            } else {
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('profile.edit_verify_information.verify_id.update_error',array(),'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        }
    }
    
    /**
     * Ajax request verify id.
     *
     * @Route("/verify_id/request", name="verify_id_request")
     * @Method("POST")
     */
    public function verifyIdRequestedAction()
    {
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        /*@var $entity_acc Account */
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!$entity_acc || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }
        $personid = $em->getRepository('VlanceAccountBundle:PersonId')->find($current_user->getPersonId()->getId());
        
        if(!is_object($personid->getAccount()) || 
            $personid->getFullName() == "" || 
            $personid->getIdNumber() == "" || 
            $personid->getIssuedDate() == "" || 
            $personid->getIssuedLocation() == "" || 
            $personid->getPath() == "" || 
            $personid->getFilenameFront() == "" || 
            $personid->getFilenameBack() == ""){
            return new JsonResponse(array('error' => '101'));
        } elseif(is_null($entity_acc->getBirthday())){
            return new JsonResponse(array('error' => '102'));
        } else {
            /* @var $personid PersonId */
            $personid->setRequestedAt(new \DateTime());
            $personid->setRequested(true);
            $em->persist($personid);
            $em->flush();
            
            return new JsonResponse(array('error' => '0'));
        }
    }
    
     /**
     * Displays a form to create a new tax ID number.
     *
     * @Route("/verify_tax_id", name="verify_tax_id")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/edit_verify_information:verify_tax_id.html.php",engine="php")
     */
     public function verifyTaxIdAction()
    {
        /* @var $current_user Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(!$current_user || !($current_user->getId())){
            return array();
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!$entity_acc || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }
        
        if(!$current_user->getTaxId()){
            $taxid = new TaxId();
            $form = $this->createForm('vlance_accountbundle_taxidtype', $taxid);
        } else {
            $taxid = $em->getRepository('VlanceAccountBundle:TaxId')->find($current_user->getTaxId()->getId());
            if($taxid->getRequested() == true){
                $form = $this->createForm('vlance_accountbundle_taxidtype', $taxid, array('disabled' => true));
            } else {
                $form = $this->createForm('vlance_accountbundle_taxidtype', $taxid, array('disabled' => false));
            }
        }
        
        return array(
            'acc'           => $current_user,
            'form'          => $form->createView(),
        );
     }
    
     
    /**
     * Creates a new tax ID number. When submit form
     *
     * @Route("/verify_tax_id/submit", name="verify_tax_id_submit")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Profile/edit_verify_information:verify_tax_id.html.php",engine="php")
     */
    public function verifyTaxIdSubmitAction(Request $request)
    {
        /* @var $current_user Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!$entity_acc || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.account.edit.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_verify_information', array()));
        }
        
        if(!$current_user->getTaxId()){
            $taxid = new TaxId();
            $form = $this->createForm('vlance_accountbundle_taxidtype', $taxid);
            $form->bind($request);
            if ($form->isValid()) {
                $taxid->setAccount($current_user);
                $taxid->setRequested(true);
                $taxid->setRequestedAt(new \DateTime());
                $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                
                $em->persist($current_user);
                $em->persist($taxid);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('profile.edit_verify_information.verify_tax_id.success', array(), 'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        } else {
            /* @var $taxid TaxId */
            $taxid = $em->getRepository('VlanceAccountBundle:TaxId')->find($current_user->getTaxId()->getId());
            $form = $this->createForm('vlance_accountbundle_taxidtype', $taxid);
            $form->bind($request);
            if ($form->isValid()) {
                /* @var $uow \Doctrine\ORM\UnitOfWork */
                $uow = $em->getUnitOfWork();
                $uow->computeChangeSet($em->getClassMetadata(get_class($taxid)), $taxid);
                $changeSet = $uow->getEntityChangeSet($taxid);
                if (count($changeSet)) {
                    $current_user->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                    $em->persist($current_user);
                }
                $taxid->setRequested(true);
                $taxid->setRequestedAt(new \DateTime());
                
                $em->persist($taxid);                
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('profile.edit_verify_information.verify_tax_id.success', array(), 'vlance'));
                return $this->redirect($this->generateUrl('account_verify_information', array()));
            }
        }
        
        return array(
            'acc' => $current_user,
            'taxid' => $taxid,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * return account list in json format
     * @Route("/json/account_typeahead/", name="account_json_typeahead_nostring")
     * @Route("/json/account_typeahead/{string}", name="account_json_typeahead")
     * @Method("GET")
     */
    public function jsonTypeaheadAction(Request $request, $string)
    {
        /* @var $auth_user /Vlance/AccountBundle/Entity/Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Not exist
        if (!$auth_user) {
            return new JsonResponse(array(
                'error' => 10
            ));
        }

        // Account is locked, redirect to 404 page. Only admin can view.
        if(!($auth_user->isAdmin())){
            return new JsonResponse(array(
                'error' => 20
            ));
        }
        
        $em = $this->getDoctrine()->getManager();
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->getTypeahead($string);
        return new JsonResponse($accounts);
    }
    
    /**
     * return account list in json format
     * @Route("/json/get_account/", name="account_json_get_account_noid")
     * @Route("/json/get_account/{id}", name="account_json_get_account")
     * @Method("GET")
     */
    public function jsonGetAccountAction(Request $request, $id)
    {
        /* @var $auth_user /Vlance/AccountBundle/Entity/Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Not exist
        if (!$auth_user) {
            return new JsonResponse(array(
                'error' => 10
            ));
        }

        // Account is locked, redirect to 404 page. Only admin can view.
        if(!($auth_user->isAdmin())){
            return new JsonResponse(array(
                'error' => 20
            ));
        }
        
        // $id is invalid
        if(!is_numeric($id)){
            return new JsonResponse(array(
                'error' => 30
            ));
        }
     
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $id));
        if(!($account->getId())){
            return new JsonResponse(array(
                'error' => 40
            ));
        }
        
        return new JsonResponse(array(
            'error' => 0,
            'data'  => array(
                'id'    => $account->getId(),
                'email' => $account->getEmail()
            )
        ));
    }
    
    /**
     * Switch between "freelancer" view and "client" view
     * @Route("/a/switch-view", name="switch_option_view")
     * @Method("GET")
     */
    public function switchOptionViewAction(Request $request)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.notvalid', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $referer = $request->headers->get('referer');
        if(!$referer){
            $referer = $this->generateUrl('vlance_homepage', array());
        }
        $auth_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        if(!($auth_user->getType())){
            $auth_user->setType('freelancer');
        }
        if(!($auth_user->getOptionView())){
            $auth_user->setOptionView($auth_user->getType());
        }
        
        // Change option view
        if($auth_user->getOptionView() == "freelancer"){
            $auth_user->setOptionView("client");
        } else{
            $auth_user->setOptionView("freelancer");
        }
        $em->persist($auth_user);
        $em->flush();
        
        return $this->redirect($referer);
    }
    
    /**
     * Upgrade account VIP
     * @Route("/a/upgrade-vip", name="upgrade_account_vip")
     * @Method("POST")
     */
    public function upgradeAccountVip(Request $request)
    {
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();

        $helper = $this->get('vlance_payment.helper');
        
        $data_confirm = $request->request->get('data_confirm');
                
        if(!isset($data_confirm['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_confirm['package']['id'];
        $service_id = OnePay::SERV_OTP_UPGRADE_ACCOUNT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        $package['qty'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.qty'));
        
        if($package['amount'] == 0){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_confirm['package']['amount'])){
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        /** @var $acc \Vlance\AccountBundle\Entity\Account **/
        $acc = $em->getRepository('VlanceAccountBundle:Account')->findOneById($auth_user->getId());
        if(!is_object($acc)){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 105, '_post' => $_POST));
        }
        
        /* @var $usercash \Vlance\PaymentBundle\Entity\UserCash */
        $usercash = $em->getRepository('VlancePaymentBundle:UserCash')->findOneBy(array('id' => $acc->getCash()->getId()));
        if(!$usercash->getId()){
            return new JsonResponse(array('usercash_id' => $acc->getCash()->getId(), 'error' => 106));
        }
        if($usercash->getActualBalance() < $package['amount']){
            return new JsonResponse(array('balance' => $acc->getCash()->getActualBalance(), 'error' => 107, 'errorMessage' => "Rất tiếc, số dư trong ví không đủ để thanh toán.", '_post' => $_POST));
        }
        
        $from_vlance = $this->container->getParameter('from_email');
        
        // Usercash Transaction
//        $usercash_trans = new Transaction();
//        $usercash_trans->setAmount($package['amount']);
//        $usercash_trans->setSender($acc);
//        $usercash_trans->setReceiver(null);
//        $usercash_trans->setJob(null);
//        $usercash_trans->setType(Transaction::TRANS_TYPE_UPGRADE_VIP);
//        $usercash_trans->setStatus(Transaction::TRANS_TERMINATED);
//        $em->persist($usercash_trans);
//        
//        if($package_id <= 3){
//            $usercash_trans->setComment('Nâng cấp VIP ' . $package['amount'] . 'VNĐ');
//        } else {
//            $usercash_trans->setComment('Nâng cấp MINI VIP ' . $package['amount'] . 'VNĐ');
//        }
//        $em->persist($usercash_trans);
//        
//        // Update Usercash after paid
//        $usercash_balance = $usercash->getBalance() - $usercash_trans->getAmount();
//        $usercash->setBalance($usercash_balance);
//        $em->persist($usercash);
//        
//        $acc_vip = new AccountVIP();
//        $acc_vip->setAccount($acc);
//        $acc_vip->setPackage($package_id);
//        $acc_vip->setPrice($package['amount']);
//        $acc_vip->setPaymentMethod(AccountVIP::PAYMENT_METHOD_USERCASH);
//        $em->persist($acc_vip);
//        
//        if($package_id <= 3){
//            $acc->setTypeVIP(Account::TYPE_VIP_NORMAL);
//        } else {
//            $acc->setTypeVIP(Account::TYPE_VIP_MINI);
//        }
//        $acc->setCategoryVIP(NULL);
//        $em->persist($acc);
//        
//        $now = new \DateTime('now');
//        if(is_null($acc->getExpiredDateVip())){
//            $expired = $now->modify('+'.$package['qty'].' months');    
//            $acc->setExpiredDateVip($expired);
//            $em->persist($acc);
//        } else {
//            if(JobCalculator::ago($acc->getExpiredDateVip(), $now) === false){//Da het han
//                $expired = $now->modify('+'.$package['qty'].' months');
//                $acc->setExpiredDateVip($expired);
//                $em->persist($acc);
//            } else {//Van con han
//                $acc->setExpiredDateVip($acc->getExpiredDateVip()->modify('+'.$package['qty'].' months'));
//                $em->persist($acc);
//            }
//        }
        
//        $em->flush();
        
        try{
            // Send tracking event to Mixpanel
//            $tracking_event_name = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.tracking_event');
//            $this->get('vlance_base.helper')->trackAll(NULL, $tracking_event_name);
//            $this->get('vlance_base.helper')->mixpanelTrackRevenue($acc, $package['amount'], $tracking_event_name, array("channel" => "Usercash"));
            // End Mixpanel

            // Send confirmation mail to payer
            $context_payer = array(
                'amount'        => number_format($package['amount'],0,',','.'),
                'account'       => $acc,
            );
            $tpl_payer = 'VlanceAccountBundle:Email/Upgrade:freelancer_upgrade_vip_usercash.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $acc->getEmail());
            
            // Send confirmation mail to Admin
            $context_admin = array(
                'amount'        => number_format($package['amount'],0,',','.'),
                'account'       => $acc,
            );
            $admin_tpl_payer = 'VlanceAccountBundle:Email/Upgrade:admin_upgrade_vip_usercash.html.twig';
            $admin = "hien.do@vlance.vn";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_admin, $from_vlance, $admin);
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 301));
        }

        return new JsonResponse(array(
            'error' => 0,
            'balance'  => array(
                'usercash'  => number_format($usercash->getActualBalance(),0,',','.'),
            )
        ));
    }
    
    /**
     * Upgrade account VIP
     * @Route("/a/update-category-vip", name="update_category_vip")
     * @Method("POST")
     */
    public function updateCategoryAccountVip(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $cat = $request->request->get('category');
        if($cat === ""){
            return new JsonResponse(array('error' => 102));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $category = $em->getRepository('VlanceAccountBundle:Category')->findOneById($cat);
        if(!is_object($category)){
            return new JsonResponse(array('error' => 103));
        }
        
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        
        $now = new \DateTime('now');
        if(JobCalculator::ago($acc->getExpiredDateVip(), $now) !== false){
            if($acc->getTypeVIP() === Account::TYPE_VIP_MINI){
                if(is_object($acc->getCategoryVIP())){
                    return new JsonResponse(array('error' => 106));
                }
                
                $acc->setCategoryVIP($category);
                $em->persist($acc);
                $em->flush();
                
                return new JsonResponse(array('error' => 0));
            } else {
                return new JsonResponse(array('error' => 104));
            }
        } else {
            return new JsonResponse(array('error' => 105));
        }
    }
    
    /**
     * List of freelancers
     * @Route("/a/{filters}", name="account_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request, $filters = "") {
        return $this->redirect($this->generateUrl('freelancer_list', array('request' => $request, 'filters' => $filters)), 301);
    }
    // REDIRECT - END    
}
