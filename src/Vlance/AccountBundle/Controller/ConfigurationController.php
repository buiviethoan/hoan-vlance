<?php
namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Form\AccountConfigType;

/**
 * Configuration user controller.
 * 
 * @Route("/options")
 */
class ConfigurationController extends Controller
{
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/general", name="config_user")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction() {
        // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm(new AccountConfigType(), $account);
        
        return array(
            'entity' => $account,
            'form' => $form->createView(),
        );
    }
    
     /**
     * Creates a new Job entity.
     *
     * @Route("/update", name="update_user")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Configuration:new.html.php",engine="php")
     */
    public function updateAction(Request $request) {
        // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm(new AccountConfigType(), $account);
        $form->bind($request);
        if ($form->isValid()) {
            $em->persist($account);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.update.success', array(), 'vlance'));
            return $this->redirect($this->generateUrl('config_user'));
        }
        
        return array(
            'entity' => $account,
            'form' => $form->createView(),
        );
    }
    
    /**
     * Account Unsubscribe email 
     * 
     * @Route("/unsubscribe/{encryption}", name="unsubscribe_email_user")
     * @Method("GET")
     * @Template(engine="php")
     */
      public function unsubscribeEmailAction($encryption) {
        $em = $this->getDoctrine()->getManager();
        $account = $em->getRepository('VlanceAccountBundle:Account')->unsubscribeAccount($encryption);
        if(count($account) < 1) {
            $message =  $this->get('translator')->trans('controller.account.unsubscribe.notpermission', array(), 'vlance');
        }
        elseif($account[0]->getNotificationJob() == 0) {
            $message = $this->get('translator')->trans('controller.account.unsubscribe.email', array(), 'vlance');
        } else {
            $account[0]->setNotificationJob(0);
            $em->persist($account[0]);
            $em->flush();
            $message = $this->get('translator')->trans('controller.account.unsubscribe.success', array(), 'vlance');
        }
        
        return array(
            'message' => $message,
        );
        
    }
    
    /**
     * Update facebookId in account
     * @Route("/faceId", name= "update_face_user")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function updateFaceAction(Request $request) {
         // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        if($account->getFacebookId()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.facebook.error', array(), 'vlance')); 
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        // Step 1: Ask for authentication code
        $config = array();
        $config['appId'] = $this->container->parameters['vlance_system']['facebook']['app_id'];
        $config['secret'] = $this->container->parameters['vlance_system']['facebook']['app_secret'];
        
        // Step 2: Got authentication code,
        if(isset($_GET['code'])){
            // Try to log in to facebook, ask for FB permission
            require_once __DIR__.'/../Lib/facebook-php-sdk/src/facebook.php';
            $facebook = new \Facebook($config);
//            echo "facebook object\n";
//            print_r($facebook);
            try {
                $res = file_get_contents("https://graph.facebook.com/oauth/access_token?"
                        ."client_id=".$config['appId']
                        ."&redirect_uri=" . $this->container->get('router')->generate('update_face_user', array(), true)
                        ."&client_secret=".$config['secret']
                        ."&code=".$_GET['code']);

                // Logged in with FB, got FB permission
                // Start getting info from FB account for creating new vLance account
                // or login in to vLance with fb account.
                // exemple of $res = "access_token=CAACzdm4Ld0kBAOgsL06BrCmj8uAeTlDSilzW9dZCu2gZAigO4V9R59Bowet2k4js6J".
                //                   "X3VCereBAsOnVY33DyI70MY6dDJ4h071GvTLFMQ2ToeosnOrXb1oBnKZBawL2YGvxymZCcLZAnFgl7BT".
                //                   "OSiWVkDKV4yvx4fb75MpDyd4L74yASt2SWy&expires=5183845"
//                print_r($res); die;
                if(preg_match('`access_token=([a-zA-Z0-9]+)&expires=([0-9]+)`', $res, $matches)){
    //                echo "matches\n";
    //                print_r($matches);

                    // Do login with facebook
                    $facebook->setAccessToken($matches[1]);
    //                echo "facebook /me\n";
    //                print_r($facebook->api("/me"));die;

                    // Get all information from connected Facebook profile
                    $fb = $facebook->api("/me");
                    //update account
                    $account->setFacebookId($fb['id']);
                    $account->setAccessTokenFace($matches[1]);
                    $em->persist($account);
                    $em->flush();
                    
                    $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.facebook.success', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('config_user'));
                }
                else{ //TODO Chỗ này vẫn còn phải giải quyết khi có lỗi nhận đc code
                }
            } catch (\Exception $e) {
                return $this->redirect($this->generateUrl('vlance_homepage'));
            }
            
        }
        return $this->redirect($this->generateUrl('vlance_homepage'));
    }
    
    /**
     * Update LinkedinId in account
     * @Route("/LinkedinId", name= "update_linkedin_user")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function updateLinkAction(Request $request) {
         // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
         if($account->getLinkedinId()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.linkedin.connected', array(), 'vlance')); 
            return $this->redirect($this->generateUrl("config_user"));
        }
        // Step 1: Ask for authentication code
        $api_key = $this->container->parameters['vlance_system']['linkedin']['api_key'];
        $secret_key = $this->container->parameters['vlance_system']['linkedin']['secret_key'];
        $state = $this->container->parameters['vlance_system']['linkedin']['state'];
        
        // Step 2: Got authentication code,
        if(isset($_GET['code'])){
            try {
                
                $url = 'https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code';
                $data = array(
                                'code' => $_GET['code'], 
                                'state' => $state,
                                'redirect_uri' => $this->container->get('router')->generate('update_linkedin_user', array(), true),
                                'client_id' => $api_key,
                                'client_secret' => $secret_key,
                            );

                $options = array(
                    'http' => array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($data),
                    ),
                );

                $context  = stream_context_create($options);
                $res = file_get_contents($url, false, $context);
                $res = json_decode($res);
                $access_token = $res->access_token;
                $content_xml = file_get_contents("https://api.linkedin.com/v1/people/~?oauth2_access_token="
                                                . $access_token
                                            ); 
                $email_xml = file_get_contents("https://api.linkedin.com/v1/people/~/email-address?oauth2_access_token="
                                            . $access_token
                                        ); 
            } catch (\Exception $e) {
                // LinkedIn returned an error
                /* @var $logger \Psr\Log\LoggerInterface */
                $logger = $this->get("monolog.logger.linkedin");
                $logger->info("Create linkedin error");
                $logger->info(print_r($e->getMessage(), true));
                // LinkedIn returned an error
                $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.linkedin.error',array(),'vlance'));
                return new RedirectResponse('config_user');
            }
            $data_content = new \SimpleXMLElement($content_xml);
            $fullName = $data_content->{'first-name'}.' '.$data_content->{'last-name'};
            $data_url = $data_content->{'site-standard-profile-request'};
            $url = $data_url->{'url'};
            
            $data_email = simplexml_load_string($email_xml);
            $email = (string)$data_email;
            
            $link_id = preg_match('`id=([0-9]*)`', $url, $id);
            
            $em = $this->getDoctrine()->getManager();
            $account->setLinkedinId($id[1]);
            $account->setAccessTokenLink($access_token);
            $em->persist($account);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.linkedin.success', array(), 'vlance'));
            return $this->redirect($this->generateUrl('config_user'));
            
        }
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.linkedin.error', array(), 'vlance'));
        return $this->redirect($this->generateUrl('config_user'));
    }
    
}