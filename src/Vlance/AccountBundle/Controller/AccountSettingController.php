<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\AccountBundle\Entity\AccountSetting;
use Vlance\AccountBundle\Entity\AccountSettingProperty;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * AccountSetting controller.
 *
 * @Route("/")
 */
class AccountSettingController extends Controller
{
   
    /**
     * 
     * @Route("/account_setting/set_account_setting", name="set_account_setting")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function setAccountSettingAction(Request $request) {
        if(is_null($request->request->get('setting'))){
            return new JsonResponse(array('error' => 10));
        }
        
        $account_setting = $request->request->get('setting');
        if(!isset($account_setting['property_name'])){
            return new JsonResponse(array('error' => 20));
        }
        if($account_setting['property_name'] == ""){
            return new JsonResponse(array('error' => 21));
        }
        $property_name = $account_setting['property_name'];
        
        if(!isset($account_setting['value'])){
            return new JsonResponse(array('error' => 30));
        }
        if($account_setting['value'] == ""){
            return new JsonResponse(array('error' => 31));
        }
        $setting_value = $account_setting['value'] ? true : false;
        
        $em = $this->getDoctrine()->getManager();
        $property = $em->getRepository('VlanceAccountBundle:AccountSettingProperty')->findOneByTitle($property_name);
        if(!$property){
            /* @var $property \Vlance\AccountBundle\Entity\AccountSettingProperty */
            $property = new AccountSettingProperty();
            $property->setTitle($property_name);
            $em->persist($property);
        }
        
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        if(!is_object($auth_user)){
            return new JsonResponse(array('error' => 50));
        }
        if(!($auth_user->getId())){
            return new JsonResponse(array('error' => 51));
        }
        
        foreach($auth_user->getAccountSettings() as $setting){
            if($setting->getProperty()->getId() == $property->getId()){
                $em->remove($setting);
            }
        }
        $new_setting = new AccountSetting();
        $new_setting->setProperty($property);
        $new_setting->setAccount($auth_user);
        $new_setting->setValue($setting_value);
        $em->persist($new_setting);
        $em->flush();
        
        return new JsonResponse(array('error' => 0));
    }
    
}
