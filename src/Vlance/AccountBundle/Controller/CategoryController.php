<?php
namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\AccountBundle\Entity\Category;
use Vlance\AccountBundle\Form\CategoryType;

/**
 * Category controller.
 *
 * @Route("/viec")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="category")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('VlanceAccountBundle:Category')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/", name="category_create")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Category:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Category();
        $form = $this->createForm(new CategoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('category_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Category entity.
     *
     * @Route("/new", name="category_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Category();
        $form   = $this->createForm(new CategoryType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Category entity.
     *
     * @Route("/{seoUrl}", name="category_show")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($seoUrl)
    {
        $em = $this->getDoctrine()->getManager();
        
        /* @var $category Category */
        $category = $em->getRepository('VlanceAccountBundle:Category')->findOneBy(array('seoUrl' => $seoUrl));
        // Tim category theo seourl, hoac theo hash        
        if(!is_object($category)){
            $category = $em->getRepository('VlanceAccountBundle:Category')->findOneBy(array('hash' => $seoUrl));
            if(!is_object($category)){
                return $this->redirect($this->generateUrl("404_page"), 301);
            }
        }
        
        if($category->getOnline() == 0){
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        $template = "VlanceAccountBundle:Category:show_level_1.html.php";
        
        if(is_null($category->getParent())){
            $subCategories = array();

            // Wrong category seo_url
            if (!$category || !$category->getId()) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.category.', array(), 'vlance'));
                return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
            }
            if ($category->getChilds()->count()) {
                $ids = array();
                foreach ($category->getChilds() as $child) {
                    $ids[] = $child->getId();
                    $subCat = $em->getRepository('VlanceJobBundle:Job')->statistic(array('id' => 'count', 'budget' => 'sum'), array('category' => array('eq', $child->getId()), 'publish' => array('eq', 1)));
                    if($child->getOnline() && !is_null($child->getHash())){
                        $subCategories[] = array(
                            'entity'    => $child,
                            'budget'    => $subCat['budget'],
                            'count'     => $subCat['id'],
                        );
                    }
                }
                $categoryJobStatistics = $em->getRepository('VlanceJobBundle:Job')->statistic(array('id' => 'count', 'budget' => 'sum'), array('category' => array('in', $ids), 'publish' => array('eq', 1)));
            } else {
                $categoryJobStatistics = $em->getRepository('VlanceJobBundle:Job')->statistic(array('id' => 'count', 'budget' => 'sum'), array('category' => array('eq', $category->getId()), 'publish' => array('eq', 1)));
            }

            /* count account */
            $statisticFields = array('id' => 'count');
            $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
            $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);

            /* count job, sum budget */
            $numJob = $categoryJobStatistics['id'];
            $totalBudget = $categoryJobStatistics['budget'];   
            
            $var_array = array(
                'absoluteUrl'       => $this->generateUrl('category_show', array('seoUrl' => $seoUrl), true),
                'category'          => $category,
                'nl2pDesc'          => $this->get('vlance_base.helper')->nl2p($category->getDescription()),
                'subCategories'     => $subCategories,
                'numJob'            => $this->get('vlance_base.helper')->formatNumber($numJob),
                'totalBudget'       => $this->get('vlance_base.helper')->formatNumber($totalBudget),
                'accountStatistic'  => $accountStatistic,
            );
            
        } else {
            // Level 2
            $template = "VlanceAccountBundle:Category:show_level_2.html.php";
            
            $categoryJobStatistics = $em->getRepository('VlanceJobBundle:Job')->statistic(array('id' => 'count', 'budget' => 'sum'), array('category' => array('eq', $category->getId()), 'publish' => array('eq', 1)));
            
            if ($category->getParent()->getChilds()->count()) {
                if($category->getOnline()){
                    $ids = array();
                    foreach ($category->getParent()->getChilds() as $child) {
                        $ids[] = $child->getId();
                        $subCat = $em->getRepository('VlanceJobBundle:Job')->statistic(array('id' => 'count', 'budget' => 'sum'), array('category' => array('eq', $child->getId()), 'publish' => array('eq', 1)));
                        if($child->getOnline() && !is_null($child->getHash())){
                            $subCategories[] = array(
                                'entity'    => $child,
                                'budget'    => $subCat['budget'],
                                'count'     => $subCat['id'],
                            );
                        }
                    }
                } else {
                    return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
                }
            }     
            
            /* count job, sum budget */
            $numJob = $categoryJobStatistics['id'];
            $totalBudget = $categoryJobStatistics['budget'];
            
            /* count account */
            $statisticFields = array('id' => 'count');
            $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
            $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
   
            /* job in SubCategory */
            $jobsSubCategory = $em->getRepository('VlanceJobBundle:Job')->findByCategory($category, array('createdAt' => 'DESC'));
            
            /* top freelancer */
            $topFreelancers = $em->getRepository('VlanceAccountBundle:Account')->getTopFreelancerByCategory($category->getId());
            
            $list_fl = array();
            foreach($topFreelancers as $key => $fl){
                array_push($list_fl, array('entity'=> $fl, 'jobworked' => count($em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($fl))));
            }
            
            $var_array = array(
                'absoluteUrl'       => $this->generateUrl('category_show', array('seoUrl' => $seoUrl), true),
                'category'          => $category,
                'nl2pDesc'          => $this->get('vlance_base.helper')->nl2p($category->getDescription()),
                'totalJob'          => $numJob,
                'numJob'            => $this->get('vlance_base.helper')->formatNumber($numJob),
                'totalBudget'       => $this->get('vlance_base.helper')->formatNumber($totalBudget),
                'accountStatistic'  => $accountStatistic,
                'jobsSubCategory'   => $jobsSubCategory,
                'topFreelancer'     => $list_fl,
                'subCategories'     => $subCategories,
            );
        }
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(null, "View category", array(
            "category"      => $category->getTitle(), 
            "level"         => ($category->getParent() ? "2" : "1"),
            "authenticated" => is_object($this->get('security.context')->getToken()->getUser()) ? "TRUE" : "FALSE"
        ));
        
        return $this->container->get('templating')->renderResponse($template, $var_array);
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="category_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VlanceAccountBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $editForm = $this->createForm(new CategoryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Category entity.
     *
     * @Route("/{id}", name="category_update")
     * @Method("PUT")
     * @Template("VlanceAccountBundle:Category:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VlanceAccountBundle:Category')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new CategoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('category_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VlanceAccountBundle:Category')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Category entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('category'));
    }

    /**
     * Creates a form to delete a Category entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
