<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\AccountBundle\Entity\Skill;
use Vlance\AccountBundle\Form\SkillType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Skill controller.
 *
 * @Route("/skill")
 */
class SkillController extends Controller
{
   
    /**
     * 
     * @Route("", name="skill_list")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
       
        $entities = $em->getRepository('VlanceAccountBundle:Skill')->findAll();
        $response = new JsonResponse();
        $data = array();
        foreach($entities as $entity) {
            $data[] = $entity->getTitle();
        }
        
        return $response->setData($data);
    }
    
}
