<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\AccountBundle\Entity\Portfolio;
use Vlance\AccountBundle\Form\PortfolioType;

/**
 * Portfolio controller.
 *
 * @Route("/portfolio")
 */
class PortfolioController extends Controller
{
   /**
     * Finds and displays a Portfolio entity.
     *
     * @Route("/s/{aid}", name="portfolio_show")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($aid)
    {
        $em = $this->getDoctrine()->getManager();
        $acc = $em->getRepository('VlanceAccountBundle:Account')->find($aid);
        $entities = $em->getRepository('VlanceAccountBundle:Portfolio')->findBy(array('account' => $acc));
   
        return array(
            'entities'   => $entities,
        );
    }
    
   /**
     * Finds and displays a Portfolio entity.
     *
     * @Route("/list/{aid}", name="portfolio_list")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/edit_portfolio:portfolio_list.html.php",engine="php")
     */
    public function listAction($aid)
    {
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $acc = $em->getRepository('VlanceAccountBundle:Account')->find($aid);
        $entities = $em->getRepository('VlanceAccountBundle:Portfolio')->findBy(array('account' => $acc));
   
        return array(
            'entities'      => $entities,
            'current_user'  => $current_user
        );
    }
    
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/add", name="portfolio_add")
     * @Method("GET")
     * @Template("VlanceAccountBundle:Profile/edit_portfolio:portfolio_add.html.php",engine="php")
     */
     public function addAction()
    {
         /* @var Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(!$current_user || !($current_user->getId())){
            return array();
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($current_user->getId());
        if(!$entity_acc || $entity_acc !== $current_user) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_portfolio_edit', array('id' => $current_user->getId())));
        }
        $entity = new Portfolio();
        $form = $this->createForm(new PortfolioType(), $entity);
       
        return array(
            'user'  => $entity_acc,
            'aid'   => $current_user->getId(),
            'form'  => $form->createView(),
        );
    }
    
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/new/{aid}", name="portfolio_new")
     * @Method("GET")
     * @Template(engine="php")
     */
     public function newAction($aid)
    {
         /* @var Vlance\AccountBundle\Entity\Account */
        $current_use = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($aid);
        if(!$entity_acc || $entity_acc !== $current_use) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $entity = new Portfolio();
        $form = $this->createForm(new PortfolioType(), $entity);
       
        return array(
            'aid' => $aid,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Creates a new Portfolio entity.
     *
     * @Route("/new/{aid}", name="portfolio_create")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Portfolio:new.html.php",engine="php")
     */
    public function createAction(Request $request, $aid)
    {
        /* @var Vlance\AccountBundle\Entity\Account */
        $current_use = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entity_acc = $em->getRepository('VlanceAccountBundle:Account')->find($aid);
        if(!$entity_acc || $entity_acc !== $current_use) {
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $entity  = new Portfolio();
        $form = $this->createForm(new PortfolioType(), $entity);
        $form->bind($request);
        if ($form->isValid()) {
            
            //insert service
            $services_new = explode(',', $_POST['hiddenTagListB']);
            foreach($services_new as $s) {
                if(empty($s)) {
                    break;
                }
                else {
                    $acc_service = $em->getRepository('VlanceAccountBundle:Service')->findOneBy(array('title' => $s));
                    $entity->addService($acc_service);
                }
            }
            $em->persist($entity);
            
            $entity->setAccount($current_use);
            $current_use->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
            $em->persist($current_use);
            $em->persist($entity);
           
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.portfolio.create.message_success',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_portfolio_edit',array('id' => $aid)));
        }
        return array(
            'aid' => $aid,
            'entity' => $entity,
            'form'   => $form->createView(),
        );

    }

    /**
     * Displays a form to edit an existing Portfolio entity.
     *
     * @Route("/{id}/edit", name="portfolio_edit")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Portfolio')->find($id);

        $acc = $this->get('security.context')->getToken()->getUser();
        if (!$entity) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.portfolio.show.message',array(),'vlance'));
            return $this->redirect($this->generateUrl('account_portfolio_edit',array('id' => $acc->getId())));
        } 
        
        if($acc !== $entity->getAccount()) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        //Hien service cua portfolio
        $services = $entity->getServices();
        $services_old = array();
        foreach($services as $s) {
            $services_old[] = $s->getTitle();
        }

        $em->persist($entity);
        $em->flush();
            
        $editForm = $this->createForm(new PortfolioType(), $entity);
        return array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'acc'       => $acc,
        );
    }

    /**
     * Edits an existing Portfolio entity.
     *
     * @Route("/{id}/update", name="portfolio_update")
     * @Method("POST")
     * @Template("VlanceAccountBundle:Portfolio:edit.html.php",engine="php")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $entity Portfolio*/
        $entity = $em->getRepository('VlanceAccountBundle:Portfolio')->find($id);
                
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.notfound',array(),'vlance'));
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        if($acc !== $entity->getAccount()) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $editForm = $this->createForm(new PortfolioType(), $entity);
        $editForm->bind($request);
        if ($editForm->isValid()) {  
            /* @var $uow \Doctrine\ORM\UnitOfWork */
            $uow = $em->getUnitOfWork();
            $uow->computeChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            $changeSet = $uow->getEntityChangeSet($entity);
            if (count($changeSet)) {
                $acc->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
                $em->persist($acc);
            }
            
            /* Service */
            $services = $entity->getServices();
            $services_old = array();
            $services_new = explode(',', $_POST['hiddenTagListB']);
            foreach($services as $s) {
                if(!in_array($s->getTitle(), $services_new)){
                    $entity->removeService($s);
                }
                $services_old[] = $s->getTitle();
            }
            
            foreach($services_new as $s) {
                if(empty($s)) {
                    break;
                }
                else if(!in_array($s, $services_old)){
                    $acc_service = $em->getRepository('VlanceAccountBundle:Service')->findOneBy(array('title' => $s));
                    $entity->addService($acc_service);
                }
            }
            /* End service */
            
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('portfolio.message.update_success', array(), 'vlance'));
            
            // Done, redirect to referer of show profile page 
            $referer = $this->generateUrl('account_portfolio_edit', array('id' => $acc->getId()));
            return $this->redirect($referer);
            // return $this->redirect($this->generateUrl('account_show', array('id' => $acc->getId())));
        }
        return array(
            'id'          => $id,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }
    
    /**
     * Delete a portfolio entity.
     *
     * @Route("/{id}/delete", name="portfolio_delete")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function deleteAction($id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceAccountBundle:Portfolio')->find($id);
        $acc = $this->get('security.context')->getToken()->getUser();
        //Khong ton tai portfolio
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.notfound',array(),'vlance'));
        }
        if($acc !== $entity->getAccount()) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.portfolio.create.notpermission',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }

        $em->remove($entity);
        $acc->setIsCertificated(!\Vlance\AccountBundle\Entity\Account::CERTIFICATED);
        $em->persist($acc);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.portfolio.delete.message_success',array(),'vlance'));
        return $this->redirect($this->generateUrl('account_portfolio_edit', array('id' =>$acc->getId())));
    }
}
