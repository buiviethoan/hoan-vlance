<?php

namespace Vlance\AccountBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends BaseController
{
    /**
     * Tell the user to check his email provider
     * 
     * 
     */
    public function checkEmailAction()
    {
        $email = $this->container->get('session')->get('fos_user_send_confirmation_email/email');
        $this->container->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);
        if (null === $user) {
            $url = $this->container->get('router')->generate('fos_user_security_login');
            return new RedirectResponse($url);
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:checkEmail.html.'.$this->getEngine(), array(
            'user' => $user,
        ));
    }
    
    public function confirmAction(Request $request, $token) {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $this->container->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "confirm-validate-email"
                ));
            }
        }
        $this->container->get('vlance_base.helper')->trackAll(NULL, "Confirm validated email");
        return parent::confirmAction($request, $token);
    }
}