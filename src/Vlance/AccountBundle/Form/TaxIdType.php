<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class TaxIdType extends AbstractType
{
    private $em;
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['disabled'] == 0){
            $isDisabled = false;
        } else {
            $isDisabled = true;
        }
        
        $builder
                ->add('idNumber', 'text', array(
                'label' => false,
                'wrapper_class' => 'idcard',
                'translation_domain' => 'vlance',
                'required' => true,
                'disabled' => $isDisabled,
                'attr' => array(
                    'placeholder' => 'profile.edit_verify_information.verify_tax_id.tax_id.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_verify_information.verify_tax_id.tax_id.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\TaxId'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_taxidtype';
    }
}