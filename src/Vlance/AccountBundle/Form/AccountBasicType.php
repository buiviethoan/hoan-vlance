<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;

class AccountBasicType extends AbstractType
{
    private $em;
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    private function getArrayOfCity(){
        $repo = $this->em->getRepository('VlanceBaseBundle:City');
        $cities = $repo->findAll();
        
        $list = array();
        foreach($cities as $city){
            $list[$city->getId()] = $city;
        }
        
        // Move HN, HCM, DN to top, and remove "Toan quoc"
        if(isset($list[0])){
            unset($list[0]);
        }
        
        if(isset($list[24]) && isset($list[31]) && isset($list[15])){
            $list = array(24 => $list[24], 31 => $list[31], 15 => $list[15]) + $list;
        }
        return $list;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'required' => false,
                'options' => array('translation_domain' => 'vlance'),
                'first_options' => array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'reset_password.reset.new_password',
                    )),
                'second_options' => array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'reset_password.reset.repeat_new_password',
                    )),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('fullName', 'text', array(
                'label' => false,
                'wrapper_class' => 'fullname',
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.full_name.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.full_name.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('idCardNumber', 'text', array(
                'label' => false,
                'wrapper_class' => 'idcard',
                'translation_domain' => 'vlance',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.id_card.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.id_card.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('skype', 'text', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.skype.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.skype.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('email','email', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.email.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => 'profile.edit_profile.email.popover',
                    'data-trigger' => 'hover',
                    )
            ))
            ->add('file', 'file', array(
                'label' => false,
                'required' => false,
                'translation_domain' => 'vlance',
                'wrapper_class' => 'offset2',
                'error_bubbling' => true,
                'attr' => array(
                    'error' => 'profile.edit_profile.error_file'
                )
            ))
            ->add('address', 'text', array(
                'label' => false,
                'wrapper_class' => 'address',
                'translation_domain' => 'vlance',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.address.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.address.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => "popovers-input"
                )
            ))
            ->add('city', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'required' => true,
                'choices' => $this->getArrayOfCity(),
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.city.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.city.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
            ->add('telephone', 'text', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'profile.edit_profile.telephone.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.telephone.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
            ->add('birthday', 'datepicker', array(
                'label' => false,
                'required' => true,
                'format' => 'd/M/y',
                'translation_domain' => 'vlance',
                'append' => 'site.append.date_icon',
                'wrapper_class' => 'wrapper birthday append-white combo-row-2nd',
                'start_date' => date('d/m/Y', time() - 86400 * 365 * 100), // last 100 years only :)
                'end_date' => date('d/m/Y', time() - 86400 * 365 * 15),
                'attr' => array(
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.birthday.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'placeholder' => 'profile.edit_profile.birthday.placeholder',
                    'class' => 'span12 popovers-input'
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_accountbasictype';
    }
}
