<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;
use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;

class AccountType extends AbstractType
{
    private $em;
    public function __construct(EntityManager $em) {
        
        $this->em = $em;
    }
    private function getArrayOfEntities(){
        $repo = $this->em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $list = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                $list[$category->getTitle()][$sub_cat->getId()] = $sub_cat;
            }
        }
        return $list;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'label' => false,
                'wrapper_class' => 'type',
                'required' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    'freelancer' => 'Freelancer',
                    'client' => 'Nhà tuyển dụng',
                ),
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.type.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.type.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('workRhythm', 'choice', array(
                'label' => false,
                'wrapper_class' => 'work_rhythm',
                'required' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    '0' => 'Bán thời gian (dưới 40h/tuần)',
                    '1' => 'Toàn thời gian (trên 40h/tuần)',
                ),
            ))
            ->add('workAvailability', 'choice', array(
                'label' => false,
                'wrapper_class' => 'work_availability',
                'required' => true,
                'translation_domain' => 'vlance',
                'choice_list' => new ChoiceList(
                    array(Account::WORK_AVAILIBILITY_NO, Account::WORK_AVAILIBILITY_YES),
                    array('Không, tôi đang bận', 'Có')
                ),
            ))
            ->add('level', 'choice', array(
                'label' => false,
                'wrapper_class' => 'level',
                'required' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    'beginner'      => '1 - Mới đi làm',
                    'professional'  => '2 - Đã có kinh nghiệm',
                    'expert'        => '3 - Chuyên gia'
                ),
                'empty_value' => 'Chọn mức kinh nghiệm phù hợp',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.level.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.level.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )

            ))
           ->add('category','entity',array(
                    'label' => false,
                    'class' => 'VlanceAccountBundle:Category',
                    'choices' => $this->getArrayOfEntities(),
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'placeholder' => 'profile.edit_profile.category.placeholder',
                        'data-toggle' => "popover",
                        'data-placement' => "right",
                        'data-content' => "profile.edit_profile.category.popover",
                        'data-trigger' => 'hover',
                        'class' => "popovers-input"
                        )
                    ))
            ->add('title', 'text', array(
                'label' => false,
                'wrapper_class' => 'title',
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.title.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.title.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => "popovers-input"
                    )
                ))
            ->add('website', 'url', array(
                'label' => false,
                'wrapper_class' => 'website',
                'required' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.website.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.website.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
            
            ->add('description', 'textarea', array(
                'label' => "profile.edit_profile.description.label",
                'wrapper_class' => 'description',
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.description.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "top",
                    'data-content' => "profile.edit_profile.description.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => "popovers-input"
                )
            ))
            
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_accounttype';
    }
}
