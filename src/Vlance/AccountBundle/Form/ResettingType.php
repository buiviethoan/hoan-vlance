<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResettingType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'vlance'),
            'first_options' => array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'reset_password.reset.new_password',
                )),
            'second_options' => array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'reset_password.reset.repeat_new_password',
                )),
            'invalid_message' => 'fos_user.password.mismatch',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'resetting',
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_resettingtype';
    }
}
