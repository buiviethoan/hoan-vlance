<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PortfolioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file','file',array(
                'label' => false,
                'required' => false,
                'wrapper_class' => 'file_upload',
                'translation_domain' => 'vlance',
                'attr' => array(
                        'placeholder' => 'profile.create_portfolio.file_format',
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-content' => 'profile.create_portfolio.file_attach',
                        'data-trigger' => 'hover',
                        'class' => 'popovers-input')
            ))
            ->add('title','text',array(
                'label' => false,
                'required' => true,
                'wrapper_class' => 'title_portfolio',
                'translation_domain' => 'vlance',
                'attr' => array(
                        'placeholder' => 'profile.create_portfolio.title',
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-content' => 'profile.create_portfolio.popover.title',
                        'data-trigger' => 'hover',
                        'class' => 'popovers-input')
            ))
            ->add('url','url',array(
                'label' => false,
                'required' => false,
                'wrapper_class' => 'url_portfolio',
                'translation_domain' => 'vlance',
                'attr' => array(
                        'placeholder' => 'profile.create_portfolio.url',
                        'data-toggle' => 'popover',
                        'data-placement' => 'right',
                        'data-content' => 'profile.create_portfolio.popover.url',
                        'data-trigger' => 'hover',
                        'class' => 'popovers-input')
            ))
            ->add('description','textarea',array(
                'label' => false,
                'required' => true,
                'wrapper_class' => 'description_portfolio',
                'translation_domain' => 'vlance',
                'attr' => array(
                        'placeholder' => 'profile.create_portfolio.description',
                        'data-toggle' => 'popover',
                        'data-placement' => 'top',
                        'data-content' => 'profile.create_portfolio.popover.description',
                        'data-trigger' => 'hover',
                        'class' => 'popovers-input')
            )) ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Portfolio'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_portfoliotype';
    }
}
