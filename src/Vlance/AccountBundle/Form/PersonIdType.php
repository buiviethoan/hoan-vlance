<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;
use Symfony\Component\Form\Extension\Core\Type\RangeType;

class PersonIdType extends AbstractType
{
    private $em;
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fileFront', 'file', array(
                'label' => false,
                'required' => false,
                'translation_domain' => 'vlance',
                'wrapper_class' => 'offset2',
                'error_bubbling' => true,
                'attr' => array(
                    'error' => 'profile.edit_profile.error_file'
                ),
            ))
            ->add('fileBack', 'file', array(
                'label' => false,
                'required' => false,
                'translation_domain' => 'vlance',
                'wrapper_class' => 'offset2',
                'error_bubbling' => true,
                'attr' => array(
                    'error' => 'profile.edit_profile.error_file'
                )
            ))
            ->add('idNumber', 'text', array(
                'label' => false,
                'wrapper_class' => 'idcard',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.id_card.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.id_card.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('fullName', 'text', array(
                'label' => false,
                'wrapper_class' => 'fullname',
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.full_name.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.full_name.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
            ->add('issuedLocation', null, array(
                'label' => false,
                'wrapper_class' => 'issuedlocation',
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_verify_information.verify_id.issued_location.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_verify_information.verify_id.issued_location.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => "popovers-input"
                )
            ))
            ->add('issuedDate', 'datepicker', array(
                'label' => false,
                'required' => true,
                'format' => 'd/M/y',
                'translation_domain' => 'vlance',
                'append' => 'site.append.date_icon',
                'wrapper_class' => 'wrapper birthday append-white combo-row-2nd',
                'start_date' => date('d/m/Y', time() - 86400 * 365 * 100), // last 100 years only :)
                'end_date' => date('d/m/Y', time()),
                'attr' => array(
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_verify_information.verify_id.issued_date.popover",
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'placeholder' => 'profile.edit_verify_information.verify_id.issued_date.placeholder',
                    'class' => 'span12 popovers-input'
                )
            ))
            ->add('type', 'choice', array(
                'label' => false,
                'wrapper_class' => 'type',
                'required' => true,
                'translation_domain' => 'vlance',
                'empty_value' => '- Chọn loại chứng minh nhân dân -',
                'choices' => array(
                    '1' => 'Chứng minh nhân dân (giấy - màu xanh)',
                    '2' => 'Chứng minh nhân dân (thẻ nhựa - màu vàng)',
                    '3' => 'Căn cước công dân',
                ),
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.type.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\PersonId'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_personidtype';
    }
}