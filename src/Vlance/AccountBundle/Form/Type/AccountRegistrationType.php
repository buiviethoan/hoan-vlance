<?php

namespace Vlance\AccountBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;

class AccountRegistrationType extends BaseType
{
    private $em;
    public function __construct(EntityManager $em) {
        
        $this->em = $em;
    }
    private function getArrayOfEntities(){
        $repo = $this->em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $list = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                $list[$category->getTitle()][$sub_cat->getId()] = $sub_cat;
            }
        }
        return $list;
    }
    
    private function getArrayOfCity(){
        $repo = $this->em->getRepository('VlanceBaseBundle:City');
        $cities = $repo->findAll();
        
        $list = array();
        foreach($cities as $city){
            $list[$city->getId()] = $city;
        }
        
        // Move HN, HCM, DN to top, and remove "Toan quoc"
        if(isset($list[0])){
            unset($list[0]);
        }
        
        if(isset($list[24]) && isset($list[31]) && isset($list[15])){
            $list = array(24 => $list[24], 31 => $list[31], 15 => $list[15]) + $list;
        }
        return $list;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'class' => 'span4',
                    'placeholder' => 'register.form.fullName',
                    )))
            ->add('username', 'hidden')
            ->add('email', 'email', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'class' => 'span4',
                    'placeholder' => 'register.form.email',
                    )))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'vlance'),
                'first_options' => array('label' => false,
                    'attr' => array(
                        'class' => 'span4',
                        'placeholder' => 'register.form.password',
                        )),
                'second_options' => array('label' => false,
                    'attr' => array(
                        'class' => 'span4',
                        'placeholder' => 'register.form.password_confirmation',
                        )),
                'invalid_message' => 'register.form.password.mismatch',
                ))
            ->add('birthday', 'datepicker', array(
                    'label' => 'register.form.birthday',
                    'translation_domain' => 'vlance',
                    'attr' => array(
                            'placeholder' => 'register.form.birthday',
                            'title' => 'register.form.birthday')))
            ->add('title', null, array(
                    'label' => false,
                    'translation_domain' => 'vlance',
                    'attr' => array(
                            'placeholder' => 'register.form.title',
                            'title' => 'register.form.title',
                            'maxlenth' => 255,
                            'x-moz-errormessage' => 'register.form.titleLong')))
            ->add('description', null, array(
                    'label' => false,
                    'translation_domain' => 'vlance',
                    'attr' => array(
                            'placeholder' => 'register.form.description',
                            'title' => 'register.form.description')))
            ->add('address', null, array(
                    'label' => false,
                    'translation_domain' => 'vlance',
                    'attr' => array(
                            'placeholder' => 'register.form.address',
                            'title' => 'register.form.address')))
            ->add('city', null, array(
                    'label' => false,
                    'property' => 'name',
                    'required' => true,
                    'choices' => $this->getArrayOfCity(),
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'class' => 'span4',
                        'placeholder' => 'register.form.city',
                        )))
             ->add('category','entity', array(
                    'label' => false,
                    'class' => 'VlanceAccountBundle:Category',
                    'choices' => $this->getArrayOfEntities(),
                    'required' => true,
                    'translation_domain' => 'vlance',
                    'attr' => array(
                        'placeholder' => 'profile.edit_profile.category.placeholder',
                        'class' => "span4"
                        )
                    ))
            ->add('telephone', null, array(
                'label' => false,
                'required' => true,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'class' => 'span4',
                    'placeholder' => 'register.form.telephone',
                    'title' => 'register.form.telephone',
                    'maxlenth' => 15,
                    'data-errormessage' => 'register.form.telephoneLong')
            ))
            ->add('type', 'choice', array(
                'expanded' => true,
                'multiple' => false,
                'label' => false,
                'wrapper_class' => 'type',
                'required' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    'freelancer' => 'Tìm việc',
                    'client' => 'Thuê Freelancer',
                )
            ))
            /* this field is not used in registration but must be added to remove the requirement when validate form */    
            ->add('file', 'hidden', array(
                'label' => false,
                'required' => false,
            ))
//            ->add('agreement', 'checkbox', array(
//                'label' => 'register.form.accept_agreement',
//                'translation_domain' => 'vlance',
//                'mapped' => false,
//                'attr' => array(
//                    'class' => 'checkbox',
//                    'required' => false)))
        ;
        
//        $builder->addValidator(new CallbackValidator(function(FormInterface $form){
//            if (!$form["agreement"]->getData()){
//                $form->addError(new FormError('register.form.accept_agreement_error'));
//            }
//        }));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_accountregistrationtype';
    }
}
