<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;

class LoginAfterType extends AbstractType
{
    private $em;
    public function __construct(EntityManager $em) {
        
        $this->em = $em;
    }
    private function getArrayOfEntities(){
        $repo = $this->em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $list = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                $list[$category->getTitle()][$sub_cat->getId()] = $sub_cat;
            }
        }
        return $list;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'label' => 'Tôi là',
                'wrapper_class' => 'type',
                'required' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    'freelancer' => 'Freelancer - tìm việc',
                    'client' => 'Nhà tuyển dụng',
                ),
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.type.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-content' => 'profile.edit_profile.type.popover',
                    'data-trigger' => 'hover',
                    'data-html' => 'true',
                    'class' => 'popovers-input'
                )

            ))
            
            ->add('email','text', array(
                'label' => 'Email',
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.email.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => 'profile.edit_profile.email.popover',
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
            
           ->add('category','entity',array(
                'label' => 'Tôi làm việc trong lĩnh vực',
                'class' => 'VlanceAccountBundle:Category',
                'choices' => $this->getArrayOfEntities(),
                'translation_domain' => 'vlance',
                'empty_value' => 'profile.edit_profile.category.placeholder',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.category.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.category.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                    )
                ))
            ->add('city', null, array(
                'label' => 'Tôi đang sống tại',
                'translation_domain' => 'vlance',
                'empty_value' => 'profile.edit_profile.city.country',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'profile.edit_profile.city.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.city.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
            ->add('telephone', 'text', array(
                'label' => 'Số điện thoại',
                'translation_domain' => 'vlance',
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'profile.edit_profile.telephone.placeholder',
                    'data-toggle' => "popover",
                    'data-placement' => "right",
                    'data-content' => "profile.edit_profile.telephone.popover",
                    'data-trigger' => 'hover',
                    'class' => "popovers-input"
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_loginaftertype';
    }
}
