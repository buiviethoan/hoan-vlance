<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Vlance\BaseBundle\Utils\Url;

class FilterVerificationType extends AbstractType {
    
    private $repository;
    private $filters;
    private $router;
    private $route;
    
    public function __construct(EntityManager $em, Session $session, RouterInterface $router) {
        $this->repository = $em->getRepository('VlanceAccountBundle:Account');
        $this->filters = $session->get('filter');
        $this->route = $session->get('route');
        $this->router = $router;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'choices' => array(),
            'multiple' => true,
            'expanded' => false,
        ));
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $tmp = $this->filters;
        $data = array();
        $default = array(
            array('id' => 1, 'title' => 'Tất cả', 'status' => '', 'checked' => ''),
            array('id' => 2, 'title' => 'Đã xác thực', 'status' => 'xac-thuc', 'checked' => ''),
        );
        if(isset($tmp['status'])) {
            $tmp_status = $tmp['status'];
            foreach ($default as $status) {
                if($status['status'] == $tmp_status) {
                    $status['checked'] = 'checked';
                    unset($tmp['status']);
                }
                $tmp['status'] = $status['status'];
                if($status['id'] == 1) {
                    unset($tmp['status']);
                }
                
                $status['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
                $data[] = $status;
            }
            
        } else {
            foreach ($default as $status) {
               $tmp['status'] = $status['status'];
               $status['checked'] = '';
               if($status['id'] == 1) {
                   unset($tmp['status']);
                   $status['checked'] = 'checked';
                }
                $status['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
               $data[] = $status;
            }
        }
        
        $view->vars = array_replace($view->vars, array(
            'status' => $data,
        ));
        
    }
    
    public function getName() {
        return 'filter_verification';
    }
    
    public function getParent() {
        return 'choice';
    }
    
}