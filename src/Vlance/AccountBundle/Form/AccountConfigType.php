<?php

namespace Vlance\AccountBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Vlance\AccountBundle\Entity\Account;
use Symfony\Component\Form\Extension\Core\ChoiceList\SimpleChoiceList;

class AccountConfigType extends AbstractType
{
        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notificationJob', 'choice', array(
                'label' => false,
                'wrapper_class' => 'notification',
                'required' => true,
                'expanded' => true,
                'translation_domain' => 'vlance',
                'choices' => array(
                    '1' => 'Có',
                    '0' => 'Không',
                ),
                'attr' => array(
                    'placeholder' => 'config.email',
                    'data-toggle' => 'popover',
                    'data-placement' => 'right',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input'
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\AccountBundle\Entity\Account'
        ));
    }

    public function getName()
    {
        return 'vlance_accountbundle_accountconfigtype';
    }
}
