<?php

namespace Vlance\AccountBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class VlanceAccountBundle extends Bundle
{
    public function getParent() {
        return 'AdmingeneratorUserBundle';
    }
}
