<?php //$view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<div class="login-choice">
    <img src="/img/line.png">
    <span> Hoặc </span>
    <img src="/img/line.png">
</div>
<div class="signup-invite-box">
    <form id="signup-invite-form" 
          action="<?php echo $view['router']->generate('account_register', array('type' => $type)) ?>" 
          method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->row($form['_token'])?>
        <?php echo $view['form']->row($form['username'])?>
        <?php echo $view['form']->row($form['fullName'])?>
        <?php echo $view['form']->row($form['email'])?>
        <?php echo $view['form']->row($form['plainPassword'])?>
        <div class="register-type">
            <p>Tôi đang muốn</p>
            <label class="radio" style="float:left;">
                <input type="radio" name="type" value="freelancer" id="register_type_01" checked="">
                <p>Tìm việc</p>
            </label>
            <label class="radio" style="float:right;">
                <input type="radio" name="type" value="client" id="register_type_02">
                <p>Thuê Freelancer</p>
            </label>
        </div>    
        <div class="row-fluid">
            <button id="register_submit" type="submit" class="btn btn-block btn-large btn-primary">
                <?php echo $view['translator']->trans('registration.submit', array(), 'FOSUserBundle'); ?>
            </button>
        </div>
        <div class="text-note">
            <?php echo $view['translator']->trans('register.common.agree_phrase', array(), 'vlance')?> <a target="_blank" href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung'));?>"><?php echo $view['translator']->trans('register.common.terms and conditions', array(), 'vlance')?></a>
        </div>
    </form>
</div>
<script type="text/javascript">
    /**
     * Because the username is required so we will put the email for username
     */
    jQuery(document).ready(function(){
        jQuery('#fos_user_registration_form_email').blur(function(){
            jQuery('#fos_user_registration_form_username').val(jQuery('#fos_user_registration_form_email').val());
        });
            $("#register_submit").click(function(event) {
                $("#signup-freelancer-form").valid();
                $('label.checkbox label.error').remove();
                <?php /* if ($('#accept-agreement:checked').val() == undefined){
                    $('label.checkbox').append('<label class="error" style="display: block !important"><?php echo $view['translator']->trans('register.form.error_checkbox', array(), 'vlance').'.' ?></label>');
                    return false;
                } */ ?>
            });
    });
</script>