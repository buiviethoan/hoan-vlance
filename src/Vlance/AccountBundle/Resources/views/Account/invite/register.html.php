<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_description', "Đăng ký tài khoản miễn phí. Cộng đồng freelancer chuyên nghiệp lớn nhất Việt Nam. Kết nối doanh nghiệp và freelancer, giúp hàng ngàn người tăng thu nhập.");?>
<?php $view['slots']->start('content')?>
<!-- Signup page -->
<div class="signup-email-invite">
    <div class='header-login-invite'>
        <p>Đăng ký ngay để <span>nhận 250.000 VNĐ</span> đầu tiên!</p>
    </div>
    <div class="signup-section container">
        <div class="row-fluid">
            <h1><?php echo $view['translator']->trans(($type=='freelancer')?'register.common.title':'register.client.title', array(), 'vlance')?></h1>
            <div class="login-socail-network">
                <a class="btn-facebook" onclick="fb_login()"><span><i class="fa fa-facebook"></i>Facebook</span></a>
                <a class="btn-google-plus" onclick="google_login()"><span><i class="fa fa-google-plus"></i>Google</span></a>
                <a class="btn-linkedin" onclick="linkedin_login()"><span><i class="fa fa-linkedin"></i>LinkedIn</span></a>
            </div>
            <?php echo $view->render('VlanceAccountBundle:Account/invite:detailForm.html.php', array('form' => $form, 'type' => $type)) ?>
        </div>
    </div>
</div>    
<!-- End Signup page -->
<?php $view['slots']->stop();?>