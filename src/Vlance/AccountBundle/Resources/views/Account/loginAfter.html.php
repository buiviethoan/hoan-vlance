<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Bổ sung thông tin của %username%', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->start('content')?>
<div class="cms-section container ">
    <div class="span12">
        <h1><?php echo $view['translator']->trans('profile.edit_profile.title_update', array(), 'vlance') ?></h1>
        <p class="info"><?php echo $view['translator']->trans('profile.edit_profile.information', array(), 'vlance') ?></p>
        <?php $referer = $referer ? $referer : $app->getRequest()->headers->get('referer'); ?>
        <div class="span5 offset3 edit-acc-wrapper">
        <form id="edit-acc" class="update-account edit-form" action="<?php echo $view['router']->generate('account_update_info', array('referer' => $referer)) ?>" method="post" <?php echo $view['form']->enctype($edit_form) ?>>
            <?php echo $view['form']->row($edit_form['type']); ?>
            <?php $email = preg_match('/@facebook.com/',$entity->getEmail()); ?>
            <?php if($email) : ?>
                <?php echo $view['form']->row($edit_form['email']); ?>
            <?php endif; ?>
            <?php echo $view['form']->row($edit_form['telephone']); ?>
            <?php echo $view['form']->row($edit_form['category']); ?>
            <?php echo $view['form']->row($edit_form['city']); ?>
            <?php echo $view['form']->row($edit_form['_token']); ?>
            <p class="update-acc">
                <button id="btn-update-acc" type="button" class="btn-flat btn-light-blue button update-account-login"><?php echo $view['translator']->trans('profile.edit_profile.submit', array(), 'vlance'); ?></button>
            </p>
        </form>
        </div>
    </div>
</div>
<?php $view['slots']->stop();?>
