<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('registration.reconfirmation.title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>
<?php /*
    <!--<h1><?php echo $view['translator']->trans('registration.reconfirmation.title', array(), 'vlance')?></h1>-->
    <!--<div>//<?php echo $view['translator']->trans('registration.reconfirmation.content', array('%url%' => $url), 'vlance') ?></div>-->
*/ ?>
<div class="confirm-section">
    <div class="container">
        <div class="jumbotext">
            <h1 class="lead"><?php echo $view['translator']->trans('registration.reconfirmation.title', array(), 'vlance')?></h1>
            <p><?php echo $view['translator']->trans('registration.reconfirmation.content', array('%url%' => $url), 'vlance') ?></p>
        </div>

        <div class="row-fluid">
             <div class="confirm-image">
            </div>
        </div>
    </div>
</div>
<?php $view['slots']->stop();?>