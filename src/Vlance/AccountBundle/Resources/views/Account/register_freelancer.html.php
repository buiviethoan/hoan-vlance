<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account') . " | vLance");?>
<?php $view['slots']->set('og_title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_description', "Chào mừng bạn đến với vLance! Đăng ký tài khoản trên vLance.vn hoàn toàn đơn giản và miễn phí.");?>
<?php $view['slots']->start('content')?>
    <h1><?php echo $view['translator']->trans('Account creation')?></h1>
    <?php echo $view->render('VlanceAccountBundle:Account:miniForm.html.php', array('form' => $form)) ?>
<?php $view['slots']->stop();?>