<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', $meta_title . " | vLance.vn"); ?>
<?php $view['slots']->set('og_title', $meta_title . " | vLance.vn");?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('img/og/danh-sach-freelancer.jpg'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('freelancer_list'));?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php // Set page index,follow or noindex,nofollow ?>
<?php $view['slots']->set('index_follow', $index_follow) ?>
<?php $view['slots']->start('content') ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<?php //Breadcrum ?>
<div style="display: none;">
<?php for($i = 0; $i < count($breadcrumbs); $i++) : ?>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => $breadcrumbs[$i]['params'])) ?>" itemprop="url"> 
        <span itemprop="title"><?php echo $breadcrumbs[$i]['title'] ?></span>
      </a>
      <?php if($i < count($breadcrumbs)- 1) : ?>
        ›
      <?php endif; ?>
    </div>  
<?php endfor; ?>
</div>
<?php // End Breadcrum ?>

<?php echo $view->render('VlanceAccountBundle:Account/list:top.html.php', array('acc' => $acc)) ?>

<div class="search-content row-fluid">
    <div class="layered-block search-results span12">
        <?php echo $view->render('VlanceAccountBundle:Account/list:content.html.php', array('acc' => $acc, 'pager' => $pager, 'pagerView' => $pagerView, 'listing_switch' => $listing_switch, 'entities' => $entities, 'list_job_invite' => $list_job_invite)) ?>
    </div>
</div>
<?php if(!$no_param):?> 
    <?php echo $view->render('VlanceAccountBundle:Account/list:bottom.html.php', array()) ?>
<?php endif;?>

<?php $view['slots']->stop(); ?>
