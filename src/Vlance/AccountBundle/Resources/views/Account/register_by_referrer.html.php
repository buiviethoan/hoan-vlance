<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_description', "Đăng ký tài khoản miễn phí. Cộng đồng freelancer chuyên nghiệp lớn nhất Việt Nam. Kết nối doanh nghiệp và freelancer, giúp hàng ngàn người tăng thu nhập.");?>
<?php $view['slots']->start('content')?>
<!-- Signup page -->
<div class="top-bg">
    <p>Nhận quà lên tới <span>30 Credit</span>*</p>
</div>
<div class="signup-section container">
    <div class="register-credit">
        <h1>Đăng kí nhanh bằng</h1>
        <?php echo $view->render('VlanceAccountBundle:Account/register_by_referrer:social_signup.html.php', array('form' => $form, 'referrer_id' => $referrer_id)) ?>
        <?php echo $view->render('VlanceAccountBundle:Account/register_by_referrer:form.html.php', array('form' => $form, 'referrer_id' => $referrer_id)) ?>
    </div>
    <div class="text-note">*Bạn sẽ nhận được 5 Credit khi đăng ký tài khoản và hoàn thành công việc đầu tiên, nhận 30 Credit khi thuê công việc đầu tiên (có giá trị ít nhất 5.000.000 VNĐ) trên vLance.vn.</div>
</div>
<!-- End Signup page -->
<?php $view['slots']->stop();?>