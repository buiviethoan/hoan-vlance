<?php $acc_login = $app->getSecurity()->getToken()->getUser();?>
<?php if(is_object($acc_login)):?>
    <?php $id_login = $acc_login->getID();?>
    <?php if($id_login == $id): ?>
        <div class="my-profile">
            <h2><?php echo $view['translator']->trans('profile.right.profile', array(), 'vlance') ?></h2>
            <div class="row-fluid percent-profile">
                <div class="percent-box">
                    <div class="percent" style="width:<?php echo $number_percent ?>%"></div>
                </div>
                <span><?php echo $number_percent ?>%</span>
            </div>
            <div class="suggestions"><?php echo $view['translator']->trans('profile.right.noteprofile1',array(),'vlance') ?> <a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'goi-y-hoan-thien-ho-so'));?>"><?php echo $view['translator']->trans('profile.right.note_in3', array(), 'vlance'); ?></a>.</div>
            <?php /*<div class="add-skills">
                <a href="#" title="<?php echo $view['translator']->trans('profile.right.add_skill', array(), 'vlance') ?>"><div class="i32 i32-add"></div><span><?php echo $view['translator']->trans('profile.right.add_skill', array(), 'vlance') ?></span></a>
                <div class="space-nicely">
                    <form class="form-inline" id="add-skills">
                        <input type="text" id="add-skill"/>
                    </form>
                </div>
            </div>
            <div class="after-add-kills"><?php echo $view['translator']->trans('profile.right.increase_after_add_skill', array(), 'vlance') ?></div>
             * */ ?>
            <?php if ($account->getIsCertificated() != \Vlance\AccountBundle\Entity\Account::CERTIFICATED && 
                    $account->getIsRequested() != \Vlance\AccountBundle\Entity\Account::REQUESTED &&
                    $account->getNotify() != Vlance\AccountBundle\Entity\Account::CAN_NOT_NOTIFY) : ?>
            <div class="clear text-center" style="margin-top: 10px;">
                <a id="btn_request_certificate" class="btn btn-primary btn-large" <?php if($number_percent == 100):?>href="<?php echo $view['router']->generate('account_request_certificate') ?>" <?php endif;?>>
                    <?php echo $view['translator']->trans('account.certificate.request', array(), 'vlance') ?>
                </a>
            </div>
            <?php endif;?>
            <?php if ($account->getIsCertificated() != \Vlance\AccountBundle\Entity\Account::CERTIFICATED && 
                    $account->getIsRequested() == \Vlance\AccountBundle\Entity\Account::REQUESTED &&
                    $account->getNotify() != Vlance\AccountBundle\Entity\Account::CAN_NOT_NOTIFY) : ?>
            <div class="clear text-center" style="margin-top: 10px;">
                <button id="btn_request_certificate" class="btn btn-primary btn-large disabled">
                    <?php echo $view['translator']->trans('account.certificate.requesting', array(), 'vlance') ?>
                </button>
            </div>
            <?php endif;?>
        </div>
    <?php endif; ?>
<?php endif; ?>
<div id="block_notify_profile_notenough" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3>Hồ sơ thiếu thông tin</h3>
    </div>
    <div class="modal-body" style="min-height: 0px">
        Hồ sơ của bạn chưa đầy đủ, vui lòng bổ sung thêm <strong><?php echo $missingFields;?></strong>
    </div>
    <div class="modal-footer">
        <a href="<?php echo $view['router']->generate('account_edit', array('id' => $account->getId())) ?>" class="btn btn-primary">Cập nhật hồ sơ</a>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        var profilePercent = <?php echo $number_percent; ?>;
        $('a#btn_request_certificate').click(function(e){
            if (profilePercent < 100) {
                e.preventDefault();
                $("#block_notify_profile_notenough").modal('show');
            }
        });
    });
</script>
