<?php /** @var $account \Vlance\AccountBundle\Entity\Account */ ?>
<!-- Modal -->
<div id="block_notify" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3>Xác thực hồ sơ</h3>
    </div>
    <div class="modal-body" style="min-height: 0px">
        Chúc mừng <b><?php echo $account->getFullName() ?></b>!
        Hồ sơ của bạn đã đủ điều kiện để được xác thực.
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Bỏ qua</a>
        <a href="<?php echo $view['router']->generate('account_request_certificate') ?>" class="btn btn-primary">Yêu cầu xác thực</a>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('#block_notify').modal('show');
    });
</script>
