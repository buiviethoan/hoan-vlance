<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', 'Create an account');?>
<?php $view['slots']->start('content')?>
    <h1>Account creation</h1>

    <form action="<?php echo $view['router']->generate('account_create') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->widget($form)?>
        <p>
            <button type="submit">Create</button>
        </p>
    </form>

        <ul class="record_actions">
    <li>
        <a href="<?php echo $view['router']->generate('account') ?>">
            Back to the list
        </a>
    </li>
</ul>
<?php $view['slots']->stop();?>
