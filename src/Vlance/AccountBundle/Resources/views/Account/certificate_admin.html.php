<form action="<?php echo $refulse_url?>" method="post" class="admin_form certificate-form">
    <input type="hidden" name="_csrf_token" value="<?php echo $_csrf_token;?>"/>
    <div class="form-actions">
        <div class="form-row">
            <label for="message">Phản hồi</label>
            <textarea name="message" id="message" cols="80" rows="4"></textarea>
        </div>
        <a class="btn btn-primary" href="<?php echo $accept_url?>">
            <i class="icon-ok"></i> <?php echo $view['translator']->trans('admin.account.certificate.accept.button', array(), 'vlance');?>
        </a>
        <button type="submit" class="btn">
            <i class="icon-remove"></i> <?php echo $view['translator']->trans('admin.account.certificate.refuse.button.confirm', array(), 'vlance');?>
        </button>
    </div>
</form>
