<?php $countResults = $pager->getNbResults(); ?>
<?php if($countResults != 0): ?>
    <div class="layered-heading results-counter">
        <ul class="nav nav-pills">
            <?php if($listing_switch['active']['name'] == 'all'):?>
                <li class="active">
                    <a href="#"><?php echo $view['translator']->trans('list_freelancer.right_content.all_job', array(), 'vlance') ?> <b>(<?php echo $countResults; ?>)</b></a>
                </li>
                <li>
                    <a href="<?php echo $listing_switch['switch']['url']?>"><?php echo $view['translator']->trans('list_freelancer.right_content.identified', array(), 'vlance') ?> <b>(<?php echo $listing_switch['switch']['count']?>)</b></a>
                </li>
            <?php else: ?>
                <li class="">
                    <a href="<?php echo $listing_switch['switch']['url']?>"><?php echo $view['translator']->trans('list_freelancer.right_content.all_job', array(), 'vlance') ?> <b>(<?php echo $listing_switch['switch']['count']?>)</b></a>
                </li>
                <li class="active">
                    <a href="#"><?php echo $view['translator']->trans('list_freelancer.right_content.identified', array(), 'vlance') ?> <b>(<?php echo $countResults; ?>)</b></a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
<?php else : ?>
    <?php // Set page noindex, nofollow ?>
    <?php $view['slots']->set('index_follow', 'noindex,nofollow') ?>
    <p class="results-counter">
        <?php echo $view['translator']->trans('list_freelancer.right_content.no_num_job_search', array(), 'vlance') ?> 
    </p>
<?php endif; ?>
