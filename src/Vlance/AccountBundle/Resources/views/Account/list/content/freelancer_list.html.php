<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\Url; ?>

<?php $i = 0; // For mixpanel ?>
<?php foreach ($entities as $entity) :?>
    <?php // For mixpanel ?>
    <?php $i++; ?>

    <?php // Find the correct job invites for this freelancer ?>
    <?php $job_invites = array(); ?>
    <?php foreach ($list_job_invite as $key => $value):?>
        <?php if ($key === $entity->getId()):?>
            <?php $job_invites = $value;?>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php echo $view->render('VlanceAccountBundle:Account:list/content/freelancer_list/item.html.php', array(
                'acc'           => $acc, 
                'entity'        => $entity, 
                'job_invites'   => $job_invites,
                'featured'      => false,
                'i'             => $i
            )) ?>
<?php endforeach; ?>
