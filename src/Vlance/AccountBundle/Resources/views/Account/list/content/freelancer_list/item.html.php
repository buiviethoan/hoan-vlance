<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>

<?php $is_owner = ($acc === $entity); ?>
<?php $job_invites = isset($job_invites) ? $job_invites : array() ; ?>

<div class="row-fluid row-result freelancer-profile <?php echo $featured ? "featured-profile" : ""; ?> " 
        <?php if(is_object($acc) && is_object($entity)):?> fid="<?php echo $entity->getId(); ?>" data-id='<?php echo $acc->getId() . "-" . $entity->getId() ?>' <?php endif;?>>
    <div class="fr-avatar span2">
        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getHash())) ?>">
            <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '99x99', 99, 99, 1);?>
            <?php if($resize) : ?>
            <img src="<?php echo $resize; ?>" alt="<?php echo $entity->getFullNameHidden(); ?>" title="<?php echo $entity->getFullNameHidden(); ?>" />
            <?php else: ?>
                <img width="99" height="99" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $entity->getFullNameHidden(); ?>"  title="<?php echo $entity->getFullNameHidden(); ?>" />
            <?php endif; ?>
        </a>
        <?php if(is_object($acc) && !$is_owner): // is logged in?>
            <?php if(count($acc->getJobsOpenForBid()) > 0): // has jobs open for bidding?>
                <div id="invite-button" class="btn-group invite-project">
                    <a class="btn btn-primary btn-small dropdown-toggle" data-toggle="dropdown" href="#"
                       onclick="vtrack('Click job invite dropdown', {'location':'freelancer list', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
                        <?php echo $view['translator']->trans('profile.center.invite_to_bid_full', array(), 'vlance') ?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach($acc->getJobsOpenForBid() as $jo):?>
                            <?php $checkJobInvite = FALSE;?>
                            <?php foreach ($job_invites as $jobInvite):?>
                                <?php if(($jobInvite->getAccount()==$entity)&&($jobInvite->getJob()==$jo)): ?>
                                <?php $checkJobInvite = TRUE;?>
                                <?php endif;?>
                            <?php endforeach;?>
                            <?php if($checkJobInvite): ?>
                                <li>
                                    <a class="invite-action tf200" style="cursor: context-menu;" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>">
                                        <div class="overflow-text da-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right">Đã mời</span>
                                    </a>
                                </li>
                                <?php $checkJobInvite = FALSE;?>
                            <?php else :?>
                                <li>
                                    <a class="invite-action tf200" title="<?php echo $jo->getTitle()?>" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>"
                                       onclick="vtrack('Click job invite', {'location':'freelancer list', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
                                        <div class="overflow-text chua-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right"></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                    <?php endforeach;?>
                    </ul>
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="fr-info span10">
        <div>
            <h3 class="fr-name" style="float: left">
            <?php if($entity->getFullname()): ?> 
                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getHash())) ?>"><?php echo $entity->getFullNameHidden()?></a>
            <?php endif; ?>  
                
            <?php if ($entity->getIsCertificated()) : ?>
            <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                  title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
            <?php endif;?> 
            
            <?php if (!is_null($entity->getTelephoneVerifiedAt())): ?>
                    <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
            <?php endif; ?>
            <?php if(!is_null($entity->getPersonId())): ?>
                <?php if (!is_null($entity->getPersonId()->getVerifyAt())): ?>
                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                <?php endif; ?>
            <?php endif; ?>
            <?php /* comment        
            <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
            */ ?>                
            <script type="text/javascript" language="javascript">
                jQuery(document).ready(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                    })        
                });
            </script>
            </h3>
        </div>        
        <?php if($entity->getTelephone() && $entity->getEmail() && !($entity->isBanned()) && $entity->isTypeFreelancer()):?>
        <div class="contact-block">
            <a class="btn btn-primary hidejs" style="display:none" data-toggle="modal" href="#pay-sms" 
                    data-request='{"fid":<?php echo $entity->getId();?>, "pid":<?php echo is_object($acc) ? $acc->getId() : -1;?>}'
                    onclick="vtrack('Click contact', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>','location': 'freelancer list', 'position':'<?php echo $i ?>', 'category':'<?php echo $entity->getCategory()?$entity->getCategory()->getTitle():''?>', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
                 <i class="fa fa-phone fa-lg"></i> <?php echo $view['translator']->trans('list_freelancer.right_content.contact', array(), 'vlance') ?></a>
        </div>
        <?php endif;?>
        <div class="clearfix"></div>
        <?php if($entity->getTitle()) : ?>
        <div class="fr-title">
            <span><?php echo $entity->getTitle(); ?></span>
        </div>
        <?php endif; ?>
        <div class="fr-summary row-fluid">
            <div class="history span9">
                <span class="location"><?php echo $entity->getCity() ? $entity->getCity()->getName() : $view['translator']->trans('common.country', array(), 'vlance') ?></span>
                <?php /* Nếu có category or số công việc thỳ hiển thị*/ if($entity->getCategory() || $entity->getNumBid()): ?>
                    <span class="separator">|</span>
                <?php endif; ?>
                <?php $category = $entity->getCategory() ?>
                <?php if($category): ?>
                        <span class="category"><?php echo $category->getTitle() ?></span>
                <?php endif; ?>

                <?php /* //Nếu có số công việc thỳ hiển thị if($entity->getNumBid() ): ?>
                    <span class="separator">|</span>
                <?php endif; */?>

                <?php if($entity->getInCome() > 0): ?>
                    <span class="separator hidejs" style="display:none">|</span>
                    <span class="history-job hidejs" style="display:none">
                        <?php if($entity->getFullname()): ?> 
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getHash())) ?>"><strong>
                                <?php echo number_format($entity->getInCome(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                                </strong></a>
                        <?php endif; ?>
                    </span>
                <?php endif; ?>

            </div>
            <div class="rating-block span3">
                <div class="rating-box">
                    <?php if($entity->getNumReviews() != 0): ?>
                        <?php $rating= ($entity->getScore() / $entity->getNumReviews()) * 20;?>
                    <div class="rating" style="width:<?php echo round($rating, 2).'%'; ?>"></div>
                    <?php else: ?>
                        <div class="rating" style="width:0%"></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="fr-profile row-fluid">
           <?php $skills = $entity->getSkills() ?>
           <?php if(count($skills) > 0): ?>
               <?php $counter = 0; ?>
               <div class="skill-list span12 row-fluid">
                   <span>
                       <?php foreach ($skills as $skill): ?>
                           <?php $counter++; ?>
                           <a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>"
                              title="Freelancer <?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle() ?></a>
                       <?php endforeach;?>
                   </span>
               </div>
           <?php endif; ?>
        </div>
    </div>
</div>

