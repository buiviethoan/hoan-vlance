<div>
    <?php echo $view->render('VlanceAccountBundle:Account/list/content:heading.html.php', array('pager' => $pager, 'listing_switch' => $listing_switch)) ?>
</div>
<div class="block-content">
    <?php echo $view['actions']->render($view['router']->generate('block_featurefreelancer', array())); ?>
</div>
<div class="block-content">
    <?php echo $view->render('VlanceAccountBundle:Account/list/content:freelancer_list.html.php', array('acc' => $acc, 'pager' => $pager, 'listing_switch' => $listing_switch, 'entities' => $entities, 'list_job_invite' => $list_job_invite)) ?>
</div>
<?php if($pager->getNbPages() != 1) : ?>
    <div class="layered-footer results-paging">
            <?php echo $pagerView->render($pager, $view['router']); ?>
    </div>
<?php endif; ?>

<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $acc, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>

<?php // invite freelancer join job ?>
<script type="text/javascript">
    $(document).ready(function(){
        jQuery('#invite-button a.invite-action').click(function(){
            that = $(this).parent();
            $.ajax({
                'url': $(this).attr('ajax-href')
            })
            .done(function(res){
                $('.navbar .upper-section .container #messages').remove();
                var alertClass = 'alert-error';
                if (res.error == 0) {
                    alertClass = 'alert-success';
                }
                that.find('div').removeClass("chua-moi");
                that.find('div').addClass("da-moi");
                that.find('span.label').html("Đã Mời");
                that.find('span.label').show();
                alertItem = '<div class="alert fade in ' + alertClass + '"><a class="close" data-dismiss="alert">×</a><div>' + res.errorMsg + '</div></div>';
                $("#messages .body-messages").html("");
                $("#messages .body-messages").append(alertItem);
                $("#messages").show();                    
                $(document).ready(function(){
                    setTimeout( "$('#messages').hide();", 5000);
                });

            })
            .fail(function(){
            })
            .always(function(){
            });
            return;
        });
    });
</script>
<?php // END invite freelancer join job ?>