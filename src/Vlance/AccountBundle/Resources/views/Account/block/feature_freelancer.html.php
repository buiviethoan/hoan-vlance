<?php if(count($feat_freelancers) == 0 && is_object($acc)): ?>
    <div class="feature-freelancer guider-block" data-display-track="show-feature-freelancer-block">
        <div class="row-fluid">
            <div class="content inner span7">
                <div class="container-image">
                    <img src="/img/credit/feature-freelancer.jpg" title="Freelancer nổi bật" alt="Freelancer nổi bật">
                </div>
            </div>
            <div class="content detail span5">
                <h4><?php echo $view['translator']->trans('list_freelancer.right_content.heading_guider_block', array(), 'vlance') ?></h4>
                <p><i class="fa fa-check"></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_one_description', array(), 'vlance') ?></p>
                <p><i class="fa fa-check"></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_two_description', array(), 'vlance') ?></p>
                <p><i class="fa fa-check"></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_three_description', array(), 'vlance') ?></p>
                <p class="cta-button">
                    <a class="btn btn-large btn-primary hidejs" style="display:none" data-toggle="modal" href="#pay-feature-freelancer" 
                        data-request='{"fid":<?php echo is_object($acc) ? $acc->getId() : -1;?>, "pid":<?php echo is_object($acc) ? $acc->getId() : -1;?>}'
                        onclick="vtrack('Click - Feature freelancer', {
                                    'case':'empty',
                                    'location': 'freelancer list',
                                    'authenticated':'<?php echo is_object($acc)?'true':'false';?>', 
                                    'category':'<?php echo $acc->getCategory()?$acc->getCategory()->getTitle():'';?>', 
                                    'freelancer':'<?php echo $acc->getFullname();?>', 
                                    'freelancer_id':'<?php echo $acc->getId();?>'})"><?php echo $view['translator']->trans('list_freelancer.right_content.featured', array(), 'vlance') ?></a>
                </p>
            </div>
        </div>
    </div>
<?php else: ?>
    <?php $i = 0; ?>
    <?php foreach ($feat_freelancers as $entity): ?>
        <?php $i++; ?>

        <?php // Find the correct job invites for this freelancer ?>
        <?php $job_invites = array(); ?>
        <?php foreach ($list_job_invite as $key => $value):?>
            <?php if ($key === $entity->getId()):?>
                <?php $job_invites = $value;?>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php // END Find the correct job invites for this freelancer ?>

        <div class="row-fluid banner-featured-freelancer">
            <div class="span8">
                <span>
                    <?php echo $view['translator']->trans('feature_freelancer.list_page.explain', array(), 'vlance'); ?>
                    <i class="fa fa-question-circle"
                        data-toggle="popover" 
                        data-placement="right" 
                        data-content="<?php echo $view['translator']->trans('feature_freelancer.list_page.icon_popup', array(), 'vlance'); ?>" 
                        data-trigger="hover"
                        data-html="true">
                    </i>
                </span>
            </div>
            <div class="span4 text-align-right tf200">
                <?php if(is_object($acc)): ?>
                    <?php if($acc->getFeaturedUntil() == NULL || $acc->getFeaturedUntil() < new \DateTime()): ?>
                    <a class="hidejs" data-toggle="modal" href="#pay-feature-freelancer"
                        data-request='{"fid":<?php echo is_object($acc) ? $acc->getId() : -1;?>, "pid":<?php echo is_object($acc) ? $acc->getId() : -1;?>}'
                        onclick="vtrack('Click - Feature freelancer', {
                                    'location': 'freelancer list',
                                    'authenticated':'<?php echo is_object($acc)?'true':'false';?>', 
                                    'category':'<?php echo $acc->getCategory()?$acc->getCategory()->getTitle():'';?>', 
                                    'freelancer':'<?php echo $acc->getFullname();?>', 
                                    'freelancer_id':'<?php echo $acc->getId();?>'})">
                        <?php echo $view['translator']->trans('feature_freelancer.list_page.ctr', array(), 'vlance'); ?>
                    </a>
                    <?php endif; ?>
                <?php else: ?>
                    <a class="hidejs" data-toggle="modal" href="#pay-feature-freelancer"
                            data-request='{"fid":<?php echo is_object($acc) ? $acc->getId() : -1;?>, "pid":<?php echo is_object($acc) ? $acc->getId() : -1;?>}'
                            onclick="vtrack('Click - Feature freelancer', {'authenticated':'<?php echo is_object($acc)?'true':'false';?>'})">
                        <?php echo $view['translator']->trans('feature_freelancer.list_page.ctr', array(), 'vlance'); ?>
                    </a>
                <?php endif; ?>
                
            </div>
        </div>
        <?php echo $view->render('VlanceAccountBundle:Account:list/content/freelancer_list/item.html.php', array(
                'acc'           => $acc, 
                'entity'        => $entity, 
                'job_invites'   => $job_invites,
                'featured'      => true, 
                'i'             => $i . "f"
            )) ?>
    <?php endforeach; ?>
    <script type="text/javascript">$(document).ready(function() {$('i[data-toggle=popover]').popover();});</script>
<?php endif; ?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_featurefreelancer.html.php', array('acc' => $acc, 'form_name' => 'pay-feature-freelancer')); ?>

