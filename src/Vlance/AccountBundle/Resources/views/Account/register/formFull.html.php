<?php //$view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<form action="<?php echo $view['router']->generate('account_register') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->widget($form)?>
    <div>
        <input type="submit" value="<?php echo $view['translator']->trans('registration.submit', array(), 'FOSUserBundle'); ?>"/>
    </div>
</form>
<script type="text/javascript">
    /**
     * Because the username is required so we will put the email for username
     */
    jQuery(document).ready(function(){
        jQuery('#fos_user_registration_form_email').blur(function(){
            jQuery('#fos_user_registration_form_username').val(jQuery('#fos_user_registration_form_email').val());
        });
    });
</script>