<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_description', "Đăng ký tài khoản miễn phí. Cộng đồng freelancer chuyên nghiệp lớn nhất Việt Nam. Kết nối doanh nghiệp và freelancer, giúp hàng ngàn người tăng thu nhập.");?>
<?php $view['slots']->start('content')?>
<!-- Login page -->
<div class="signup-section container">
    <div class="signup-box span8">
        <h1><?php echo $view['translator']->trans('register.common.create_account', array(), 'vlance')?></h1>
        <p><?php echo $view['translator']->trans('register.common.welcome_text', array('%url%' => $view['router']->generate('fos_user_security_login')), 'vlance')?></p>
        <?php echo $view->render('VlanceAccountBundle:Account/register:types.html.php');?>
    </div>
</div>
<!-- End Login page -->
<?php $view['slots']->stop();?>
