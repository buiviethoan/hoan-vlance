<?php //$view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<form id="register_home" action="<?php echo $view['router']->generate('account_register', array('type' => 'freelancer')) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->row($form['_token'])?>
    <?php echo $view['form']->row($form['username'])?>
    <?php echo $view['form']->row($form['fullName'])?>
    <?php echo $view['form']->row($form['email'])?>
    <?php echo $view['form']->row($form['city'])?>
    <?php echo $view['form']->row($form['plainPassword'])?>
    <div class="button-group">
        <input class="submit_register_home btn btn-large btn-primary" type="submit" value="<?php echo $view['translator']->trans('registration.home.create_account', array(), 'vlance'); ?>"/>
        <input class="btn btn-large btn-facebook" onclick="fb_login()" type="button" value="<?php echo $view['translator']->trans('registration.home.facebook', array(), 'vlance'); ?>"/>
    </div>
</form>
<script type="text/javascript">
    /**
     * Because the username is required so we will put the email for username
     */
    jQuery(document).ready(function(){
        jQuery('#fos_user_registration_form_email').blur(function(){
            jQuery('#fos_user_registration_form_username').val(jQuery('#fos_user_registration_form_email').val());
        });
        $("input.submit_register_home").click(function(event) {
                $("#register_home").valid();
                
            });
    });
</script>