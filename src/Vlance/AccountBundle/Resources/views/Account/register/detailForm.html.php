<?php //$view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>

<p><?php echo $view['translator']->trans('register.common.login_phrase', array('%url%' => $view['router']->generate('fos_user_security_login')), 'vlance'); ?></a></p>
<?php if ($type == 'freelancer'):?>
    <p><?php echo $view['translator']->trans('register.freelancer.switch_to_client', array('%url%' => $view['router']->generate('account_register', array('type' => 'client'))), 'vlance'); ?></p>
<?php else:?>
    <p><?php echo $view['translator']->trans('register.client.switch_to_freelancer', array('%url%' => $view['router']->generate('account_register', array('type' => 'freelancer'))), 'vlance'); ?></p>
<?php endif;?>
<div class="signup-freelancer-box">
    <form id="signup-freelancer-form" 
          action="<?php echo $view['router']->generate('account_register', array('type' => $type)) ?>" 
          method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->row($form['_token'])?>
        <?php echo $view['form']->row($form['username'])?>
        <?php echo $view['form']->row($form['fullName'])?>
        <?php echo $view['form']->row($form['email'])?>
        <?php echo $view['form']->row($form['telephone'])?>
        <?php echo $view['form']->row($form['city'])?>
        <?php echo $view['form']->row($form['category'])?>
        <?php echo $view['form']->row($form['plainPassword'])?>
        <label class="checkbox">
            <input type="checkbox" id="accept-agreement">
            <?php echo $view['translator']->trans('register.common.checkbox_phrase', array(), 'vlance')?> <a target="_blank" href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung'));?>"><strong><?php echo $view['translator']->trans('register.common.terms and conditions', array(), 'vlance')?></strong></a>
        </label>
        <div class="row-fluid">
            <button id="register_submit" type="submit" class="btn btn-large btn-primary">
                <?php echo $view['translator']->trans('registration.submit', array(), 'FOSUserBundle'); ?>
            </button>
        </div>
    </form>
</div>
<script type="text/javascript">
    /**
     * Because the username is required so we will put the email for username
     */
    jQuery(document).ready(function(){
        jQuery('#fos_user_registration_form_email').blur(function(){
            jQuery('#fos_user_registration_form_username').val(jQuery('#fos_user_registration_form_email').val());
        });
            $("#register_submit").click(function(event) {
                $("#signup-freelancer-form").valid();
                $('label.checkbox label.error').remove();
                <?php /* if ($('#accept-agreement:checked').val() == undefined){
                    $('label.checkbox').append('<label class="error" style="display: block !important"><?php echo $view['translator']->trans('register.form.error_checkbox', array(), 'vlance').'.' ?></label>');
                    return false;
                } */ ?>
            });
    });
</script>