<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->set('og_description', "Đăng ký tài khoản miễn phí. Cộng đồng freelancer chuyên nghiệp lớn nhất Việt Nam. Kết nối doanh nghiệp và freelancer, giúp hàng ngàn người tăng thu nhập.");?>
<?php $view['slots']->start('content')?>
<!-- Signup page -->
<div class="signup-section container">
    <div class="span5">
        <h1><?php echo $view['translator']->trans(($type=='freelancer')?'register.freelancer.title':'register.client.title', array(), 'vlance')?></h1>
        <?php echo $view->render('VlanceAccountBundle:Account/register:detailForm.html.php', array('form' => $form, 'type' => $type)) ?>
    </div>
    <div class="quick-guide span5 offset1">
        <?php if ($type=='freelancer'):?>
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('register.common.why_select_vlance', array(), 'vlance')?></h3>
            <?php /* <p><?php echo $view['translator']->trans('register.common.description_why_select_vlance', array(), 'vlance')?></p>*/?>
            <p><?php echo $view['translator']->trans('register.freelancer.whychoosen_explain1', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.freelancer.whychoosen_explain2', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.freelancer.whychoosen_explain3', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.freelancer.whychoosen_explain4', array(), 'vlance')?></p>
            
            <div class="small-pic">
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_cs_4.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_2.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_3.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_4.jpg') ?>" alt="" /></span>
            </div>
        <?php else:?>
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('register.common.why_business_select_vlance', array(), 'vlance')?></h3>
            <?php /*<p><?php echo $view['translator']->trans('register.common.description_business_why_select_vlance', array(), 'vlance')?></p> */ ?>
            <p><?php echo $view['translator']->trans('register.client.whychoosen_explain1', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.client.whychoosen_explain2', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.client.whychoosen_explain3', array(), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('register.client.whychoosen_explain4', array(), 'vlance')?></p>
            
            <div class="small-pic">
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_c_1.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_2.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_3.jpg') ?>" alt="" /></span>
                <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_c_4.jpg') ?>" alt="" /></span>
            </div>
        <?php endif;?>
    </div>
</div>
<!-- End Signup page -->
<?php $view['slots']->stop();?>