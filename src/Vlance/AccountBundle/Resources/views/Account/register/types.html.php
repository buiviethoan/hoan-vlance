<form id="register_type" action="<?php echo $view['router']->generate('account_register') ?>" method="get">
    <label class="radio">
        <input type="radio" name="type" value="freelancer" id="register_type_01" checked>
        <p><strong><?php echo $view['translator']->trans('register.type.freelancerTitle', array(), 'vlance');?></strong></p>
        <p><?php echo $view['translator']->trans('register.type.freelancerDesc', array(), 'vlance');?></p>
    </label>
    <label class="radio">
        <input type="radio" name="type" value="client" id="register_type_02">
        <p><strong><?php echo $view['translator']->trans('register.type.clientTitle', array(), 'vlance');?></strong></p>
        <p><?php echo $view['translator']->trans('register.type.clientDesc', array(), 'vlance');?></p>
    </label>
    <button type="submit" class="btn btn-large"><?php echo $view['translator']->trans('registration.submit', array(), 'FOSUserBundle'); ?></button>
</form>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#register_type').submit(function(){
            val = jQuery('input:radio[name="type"]:checked').val();
            window.location.href = jQuery('#register_type').attr('action') + '/' + val;
            return false;
        });
    });
</script>