<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->start('content')?>
    <h1><?php echo $view['translator']->trans('Create an account'); ?></h1>
<?php $view['slots']->stop();?>
