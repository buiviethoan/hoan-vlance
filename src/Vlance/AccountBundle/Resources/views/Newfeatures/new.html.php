<?php /* @var $current_user \Vlance\AccountBundle\Entity\Account */ ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $routeName = $app->getRequest()->get('routeName'); ?>
<?php if( $current_user->getFeatureInvite() == 1): ?>
<?php /** @var $account \Vlance\AccountBundle\Entity\Account */ ?>
<!-- Modal -->
<div id="block_invite_email" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3>Chức năng mới</h3>
    </div>
    <div class="modal-body" style="min-height: 0px">
        Bạn đang có <b>2 cơ hội</b> chào giá công việc yêu thích trên vLance.vn. <br>
        Chỉ 2 lần chào giá nữa thôi, bạn sẽ phải chờ <b>30 ngày tới</b> để tiếp tục mở chức năng bid.<br>
        <br>
        Vậy, dự án mới tốt hơn được đăng lên, bạn phải làm sao?<br>
        <br>
        Hãy <b>mời bạn bè</b> của bạn<br>
        Để nhận thêm nhiều bids & thỏa sức chào giá các công việc hấp dẫn.
        
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Xem tiếp</a>
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        <?php if( $current_user->getFeatureInvite() == 1 && $routeName != 'email_invite' ): ?>
                if ($.cookie('nextcookie') == null) {
                    $('#block_invite_email').modal('show');
                }
        <?php endif; ?>
       
    });
    
    $(document).ready(function() {
        $('#block_invite_email button.close').click(function(){
            window.location = '<?php echo $view['router']->generate('update_feature_invite')?>';
        })
    })
</script>

<div id="guide" class="hide">
    <div class="content-popover"></div>
    <div class="guide-control">
        <div class="popover-pre popover-pre-next btn tf200" onclick="return clickPrepopover(this);">< Quay lại</div>
        <div class="popover-next popover-pre-next btn btn-primary tf200" onclick="return clickNextpopover(this);">Tiếp ></div>
    </div>
</div>
<script type="text/javascript">
    $('#form_list_email').popover();
    $(document).ready(function() {
    
        $('#guide').guidding(
            {
        <?php /*1: {
                    data_target: '#popover-in',
                    data_content: 'Tăng số lượng <b>chào giá</b> cho mình.',
                    data_url: '',
                    data_nextstep: '2',
                    data_prestep: null,
                },
                tim kiem next_step_click de sua dieu kien hien thi menu(neu menu hien thi o buoc 2 thy dk la 2 mac dinh la 1)
         */?>
                1: {
                    data_target: '.account-menu .menu-top-invite',
                    data_content: 'Bạn hãy bấm vào <b>"Mời bạn bè"</b> để đến trang hướng dẫn.',
                    data_url: '<?php echo $view['router']->generate('email_invite')?>',
                    data_nextstep: '2',
                    data_prestep: null,
                },
                2: {
                    data_target: '.note-email-invited > div > b',
                    data_content: 'Bạn đang còn <b>quá ít lượt chào giá!!!</b>',
                    data_url: '',
                    data_nextstep: '3',
                    data_prestep: '1'
                },
                3: {
                    data_target: '.description_inviteemail',
                    data_content: 'Nhập email bạn bè với số lượng không hạn chế. Mỗi email cách nhau bằng dấu <b>"phẩy (,)"</b> hoặc dấu <b>"chấm phẩy(;)"</b> để ngăn cách giữa các email.',
                    data_url: '',
                    data_nextstep: '4',
                    data_prestep: '2'
                },
                4: {
                    data_target: '#form-invite-email .btn',
                    data_content: 'Ấn vào nút <b>"Mời bạn"</b> để gửi lời mời đến bạn bè của bạn.',
                    data_url: '<?php echo $view['router']->generate('update_feature_invite')?>',
                    data_nextstep: '5',
                    data_prestep: '3'
                }
                
            }
        );
    
    });

    function clickNextpopover(item) {
        var curent_step = $(item).attr('data-curentstep');
        var next_step_click = $(item).attr('data-nextstep');
        var data_url = $(item).attr('data-url');

        if (next_step_click != null) {
            if($.cookie('nextcookie') == null){ //Nếu cookie chưa có gì
                var curent_of_popover = $('body').find('[data-toggle="popover"]').attr('data-curentstep-popover');
                var next_of_popover = $('body').find('[data-toggle="popover"]').attr('data-next-popover');
    //                    add cookie
                $.cookie('nextcookie', JSON.stringify(next_of_popover) , { expires: 7, path: '/' });
                $.cookie('curentcookie', JSON.stringify(curent_of_popover) , { expires: 7, path: '/' });
            }else{
    //                    lay gia tri popover trong cookie + 1
                var curent_of_popover = parseInt($.parseJSON($.cookie('curentcookie'))) + 1;
                var next_of_popover = parseInt($.parseJSON($.cookie('nextcookie'))) + 1;
    //                    add lai cookie
                $.cookie('nextcookie', JSON.stringify(next_of_popover) , { expires: 7, path: '/' });
                $.cookie('curentcookie', JSON.stringify(curent_of_popover) , { expires: 7, path: '/' });
            }
    //        an step hien tai
                $('body').find('[data-curentstep-popover="' + curent_step + '"]').popover('destroy');
    //        show step tiep theo
            if (next_step_click == next_of_popover) {
//                kiem tra xem co fai pgae hien tai la trang invite email ko
                var action_page = '<?php echo $app->getRequest()->get('routeName') ?>';
//                if( action_page != 'email_invite'){
//                    
//                }
                if(data_url){
                    window.location = data_url;
                }
                
    //            hien thi menu top
                if(next_step_click == 1){
                    $('.account-menu').addClass('open_menu');
                    $('body').find('[data-curentstep-popover="' + next_step_click + '"]').addClass('hover');
                }else{
                    $('.account-menu').removeClass('open_menu');
                    $('body').find('[data-curentstep-popover="' + next_step_click + '"]').removeClass('hover');
                }
    //            end hien thi menu top
                $('body').find('[data-toggle="popover"]').popover('destroy');
                $('body').find('[data-curentstep-popover="' + next_step_click + '"]').popover('show');
                
            }
        } else {
            $('body').find('[data-toggle="popover"]').popover('destroy');
        }
    }
    function clickPrepopover(item) {
        var curent_step = $(item).attr('data-curentstep');
        var next_step_click = $(item).attr('data-prestep');
        
        if (next_step_click != null) {
            if($.cookie('nextcookie') == null){//Nếu cookie chưa có gì
                var curent_of_popover = $('body').find('[data-toggle="popover"]').attr('data-curentstep-popover');
                var next_of_popover = $('body').find('[data-toggle="popover"]').attr('data-pre-popover');
//                    add cookie
                $.cookie('nextcookie', JSON.stringify(next_of_popover) , { expires: 7, path: '/' });
                $.cookie('curentcookie', JSON.stringify(curent_of_popover) , { expires: 7, path: '/' });
            }else{
//                    lay gia tri popover trong cookie - 1 de ra cai preview dang trước
                var curent_of_popover = parseInt($.parseJSON($.cookie('curentcookie'))) - 1;
                var next_of_popover = parseInt($.parseJSON($.cookie('nextcookie'))) - 1;
//                    add lai cookie
                $.cookie('nextcookie', JSON.stringify(next_of_popover) , { expires: 7, path: '/' });
                $.cookie('curentcookie', JSON.stringify(curent_of_popover) , { expires: 7, path: '/' });
            }
//        an step hien tai
            $('body').find('[data-curentstep-popover="' + curent_step + '"]').popover('destroy');
//        show step tiep theo
            if (next_step_click == next_of_popover) {
//            hien thi menu top
                if(next_step_click == 1){
                    $('.account-menu').addClass('open_menu');
                    $('body').find('[data-curentstep-popover="' + next_step_click + '"]').addClass('hover');
                }else{
                    $('.account-menu').removeClass('open_menu');
                    $('body').find('[data-curentstep-popover="' + next_step_click + '"]').removeClass('hover');
                }
//            end hien thi menu top
                $('body').find('[data-toggle="popover"]').popover('destroy');
                $('body').find('[data-curentstep-popover="' + next_step_click + '"]').popover('show');
//                khi pre ve cai dau tien thi add class cho an nut pre
                if($.cookie('nextcookie') == 1){
                    $('body').find('.popover-pre').addClass('first');
                }
            }
        } else {
            $('body').find('[data-toggle="popover"]').popover('destroy');
        }
    }
</script>
<?php endif; ?>