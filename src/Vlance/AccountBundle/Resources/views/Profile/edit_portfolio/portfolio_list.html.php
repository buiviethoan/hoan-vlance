<?php use Vlance\BaseBundle\Utils\ResizeImage;?>

<?php if(empty($entities)) : ?>
    <div><?php echo $view['translator']->trans('profile.center.not_job_profile', array(), 'vlance') ?></div>
<?php else : ?>
    <table class="table table-striped" id="edit-portfolios-table">
        <thead>
            <tr>
                <th class="thumbnails">Hồ sơ</th>
                <th class="description">Thông tin</th>
                <th class="services">Dịch vụ</th>
            </tr>
        </thead>
    <tbody>
    <?php $i = 1; ?>
    <?php foreach($entities as $entity) : ?>
        <tr>
            <td>
                <?php if($entity->getMimeType() == 'application/msword' 
                        || $entity->getMimeType() == 'application/vnd.ms-works' 
                        || $entity->getMimeType()=='application/vnd.openxmlformats-officedocument.wordprocessingml.document'): ?>
                    <div class="img-profile">
                        <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $entity->getId()))?>" title="<?php echo $entity->getFilename() ?>">
                            <img src="<?php echo $view['assets']->getUrl('img/icon/icon_word.png');?>" 
                                 alt="<?php echo $entity->getFilename() ?>"
                                 title="<?php echo $entity->getFilename() ?>"/>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if($entity->getMimeType() == 'application/pdf' || $entity->getMimeType() == 'application/x-pdf'): ?>
                    <div class="img-profile">
                        <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $entity->getId()))?>" title="<?php echo $entity->getFilename() ?>">
                            <img src="<?php echo $view['assets']->getUrl('img/icon/icon_pdf.png');?>" 
                                 alt="<?php echo $entity->getFilename() ?>"
                                 title="<?php echo $entity->getFilename() ?>"/>
                        </a>
                    </div>
                <?php  endif; ?>

                <?php if($entity->getMimeType() == 'image/jpeg' 
                        || $entity->getMimeType() == 'image/png' 
                        || $entity->getMimeType() == 'image/gif') : ?>
                    <div class="img-profile">
                        <?php $path = $view['router']->generate('download_portfolio',array('portfolio_id'=>$entity->getId())); ?>
                        <a href="<?php echo $path; ?>"  
                           data-lightbox="lightbox-tq"
                           title="<?php echo $entity->getTitle(); ?> - <a target='_blank' href='<?php echo $entity->getUrl(); ?>'><?php echo $entity->getUrl(); ?></a>">
                            <?php $resize = $entity->getFullStoredName() ? ResizeImage::resize_image($entity->getFullStoredName(), '140x140', 140, 140, 1) : ""; ?>
                            <img src="<?php echo $resize ? $resize : ($view['assets']->getUrl('img/default.jpg')); ?>" 
                                 alt="<?php echo $entity->getFilename() ?>" width="140px" height="140px"
                                 title="<?php echo $entity->getFilename() ?>"/>
                        </a>
                    </div>
                <?php endif; ?>
                
                <?php if($current_user == $entity->getAccount()): ?>
                    <div class="actions">
                        <a href="<?php echo $view['router']->generate('portfolio_edit',array('id' => $entity->getId())); ?>" 
                           class="btn" title="<?php echo $view['translator']->trans('profile.center.edit_portfolio', array(), 'vlance'); ?>">
                            <i class="icon-pencil"></i>
                        </a>
                        <a data-url="<?php echo $view['router']->generate('portfolio_delete',array('id' => $entity->getId())) ?>" 
                           class="btn action-delete" title="<?php echo $view['translator']->trans('profile.edit_portfolio.delete', array(), 'vlance') ?>">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                <?php endif;?>
            </td>
            <td>
                <p><b><?php echo $entity->getTitle(); ?></b></p>
                <?php if($entity->getUrl()): ?>
                <p><a href="<?php echo $entity->getUrl(); ?>"><?php echo $entity->getUrl(); ?></a></p>
                <?php endif; ?>
                <?php foreach (explode("\n", htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                    <?php if (trim($line)): ?>
                        <p><?php echo $line; ?></p>
                    <?php endif; ?>
                <?php endforeach; ?>
            </td>
            <td>
                <span class="tm-tag tm-tag-portfolio">
                    <?php if($entity->getServices()): ?>
                    <?php foreach ($entity->getServices() as $s):?>
                        <p><?php echo $s->getTitle();?></p>
                    <?php endforeach; ?>
                <?php endif; ?>
                </span>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
    
    <script type="text/javascript">
    $(document).ready(function() {
        $('table .actions .action-delete').click(function(){
            if(confirm("Bạn có chắc chắn xóa Hồ sơ này?")){
                window.location.href = $(this).data('url');
            }
        });
    });
    </script>
<?php endif; ?>