<form id="form-create-portfolio" class="edit-form" action="<?php echo $view['router']->generate('portfolio_create',array('aid' => $aid)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.center.title_newfile', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['title']); ?>
                <?php echo $view['form']->widget($form['title']); ?>
                <div class="subtitle"><?php echo $view['translator']->trans('profile.center.title_newfile_explain', array(), 'vlance') ?></div>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt>
            <?php echo $view['translator']->trans('profile.edit_portfolio.img_represent', array(), 'vlance') ?><span class="text-red">*</span>
        </dt>
        <dd>
            <?php echo $view['form']->errors($form['file']) ?>
            <?php echo $view['form']->widget($form['file']) ?>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.center.image_explain', array(), 'vlance') ?></div>
        </dd>
        <div class="clear"></div>
        
        <dt>URL</dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['url']); ?>
                <?php echo $view['form']->widget($form['url']); ?>
                <div class="subtitle"><?php echo $view['translator']->trans('profile.center.link_explain', array(), 'vlance') ?></div>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt>
            <?php echo $view['translator']->trans('profile.center.describe_job', array(), 'vlance') ?><span class="text-red">*</span>
        </dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['description']); ?>
                <?php echo $view['form']->widget($form['description']); ?>
                <div class="subtitle"><?php echo $view['translator']->trans('profile.center.describe_job_explain', array(), 'vlance') ?></div>
            </div>
        </dd>
        <div class="clear"></div>
        
        <!-- Service --> 
        <dt><?php echo $view['translator']->trans('profile.edit_service.label_input_service_portfolio', array(), 'vlance') ?></dt>
        <dd>
            <div class="form-add-service">
                <div class="inner-service">
                    <input type="text" data-trigger="hover" data-content="Liệt kê các dịch vụ bạn có thể cung cấp cho khách hàng." data-placement="right" data-toggle="popover" placeholder="Tên dịch vụ (VD: Thiết kế banner facebook,...)" data-provide="typeahead" name="hiddenTagListB" placeholder="Tags" class="tm-input-cat"/>
                </div>
            </div>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_service.description_service', array(), 'vlance') ?></div>
            <div class="subtitle">Dịch vụ bạn muốn cung cấp không có trong danh sách trên? Hãy <a href="http://goo.gl/forms/dGdgljyzdm" target="_blank">gửi gợi ý cho chúng tôi</a> để trở thành người đầu tiên được cung cấp dịch vụ này trên vLance.vn.</div>
        </dd>
        <div class="clear"></div>
        
        <dt></dt>
        <dd>
            <div class="submit-form-edit-profile">
                <?php echo $view['form']->row($form['_token'])?>
                <input type="submit" class="btn btn-large btn-primary" value="<?php echo $view['translator']->trans('profile.create_portfolio.save', array(), 'vlance') ?>">
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
</form>

<script type="text/javascript">
    var cat_source = <?php echo $view['actions']->render($view['router']->generate('service_list'));?>;
</script>

<script type="text/javascript">
$(document).ready(function(){
    jQuery.validator.addMethod("filesize", function (val, element) {
        var size = element.files[0].size;
        if (size > 6291456)// checks the file more than 6 MB
        {
            return false;
        } else {
            return true;
        }
    }, "Kích thước file phải nhỏ hơn 6MB");

    $("#form-create-portfolio").validate({
        rules: {
            "vlance_accountbundle_portfoliotype[file]": {
                "required": true,
                "filesize": true
            },
            "vlance_accountbundle_portfoliotype[title]": "required",
            "vlance_accountbundle_portfoliotype[description]": "required"
        }
    });
    $("#form-create-portfolio .button").click(function() {
        var ext = $('#vlance_accountbundle_portfoliotype_file').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['gif','png','jpg','jpeg','doc','docx','pdf']) == -1) {
            if(ext == ''){
                $('#form-create-portfolio .file_upload label.error').remove();
                $('#form-create-portfolio .file_upload').append('<label for="vlance_accountbundle_portfoliotype_file" class="error" style="display: block !important"><?php echo $view['translator']->trans('profile.create_portfolio.error_file_empty', array(), 'vlance').'.' ?></label>');
                $('#form-create-portfolio .file_upload input').focus();  
                return false;
            }
            $('#form-create-portfolio .file_upload label.error').remove();
            $('#form-create-portfolio .file_upload').append('<label for="vlance_accountbundle_portfoliotype_file" class="error" style="display: block !important"><?php echo $view['translator']->trans('profile.create_portfolio.error_file', array(), 'vlance').'.' ?></label>');
            $('#form-create-portfolio .file_upload input').focus();
            return false;
        }else{
            $('#form-create-portfolio .file_upload label.error').remove();
            if($("#form-create-portfolio").valid()){
                $("#form-create-portfolio .button").submit();
            }else{
                return false;
            }
        }
        
        if ($('#form-create-portfolio').valid() === true) {}
        else {
            $('#form-create-portfolio').validate().focusInvalid();
        }
    })
});
</script>