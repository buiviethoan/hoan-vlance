<?php use Vlance\PaymentBundle\Entity\OnePay; ?>

<div class="subpage-content">
    <!-- Verify telephone -->
    <div class="row-fluid explain-content">
        <p>
            <b>Xác thực số điện thoại mang lại những lợi ích gì?</b><br/>
            <ul>
                <li>Tăng độ tin cậy khi gửi chào giá cho khách hàng.</li>
                <li>Được kích hoạt tính năng <b>Liên hệ trực tiếp</b>, được khách hàng liên lạc qua điện thoại ngay khi cần.</li>
            </ul>
        </p>
        <p>
            <b>Chi phí xác thực số điện thoại là bao nhiêu?</b><br/>
            <ul>
                <li>Bạn sẽ bị trừ <b><?php echo $view['vlance']->getParameter('payment_gateway.onepay.otp.105.amount') ?> VNĐ</b> từ tài khoản điện thoại sau khi xác thực thành công.</li>
                <li>Việc xác thực được thực hiện qua tin nhắn SMS, hỗ trợ 3 nhà mạng Viettel, Vinaphone, Mobifone.</li>
            </ul>
        </p>
    </div>
    <p><br/></p>
    <div class="row-fluid">
        <?php if(is_null($auth_user->getTelephoneVerifiedAt())):?>
            <p class="text-center">
                <a class="btn btn-large btn-primary verify-tel-menu" href="#pay-verifytel"
                        data-toggle="modal"
                        data-request='{"aid":"<?php echo $auth_user->getId();?>"}'
                        data-url-request="<?php echo $view['router']->generate('transaction_onepay_otp_request', array('service_id' => OnePay::SERV_OTP_VERIFY_TEL)); ?>"
                        data-url-confirm="<?php echo $view['router']->generate('transaction_onepay_confirm_verifytel'); ?>"
                        onclick="vtrack('Click verify telephone', {'location':'Information Verification Page', 'amount':<?php echo $view['vlance']->getParameter('payment_gateway.onepay.otp.105.amount') ?>})">
                    <?php echo $view['translator']->trans('profile.edit_verify_information.verify_telephone.cta', array(), 'vlance') ?>
                </a>
            </p>
        <?php else: ?>
            <p class="text-center">
                <a class="btn btn-large btn-primary" href="#pay-verifytel" disabled="disabled">
                    <i class="fa fa-phone"></i> <?php echo $view['translator']->trans('profile.edit_verify_information.verify_telephone.verified', array("%%telephone%%" => $auth_user->getTelephone()), 'vlance') ?>
                </a>
            </p>
            <p class="text-center">
                Bạn đã đổi số điện thoại?
                <b>
                    <a class="verify-tel-menu" href="#pay-verifytel"
                            data-toggle="modal"
                            data-request='{"aid":"<?php echo $auth_user->getId();?>"}'
                            data-url-request="<?php echo $view['router']->generate('transaction_onepay_otp_request', array('service_id' => OnePay::SERV_OTP_VERIFY_TEL)); ?>"
                            data-url-confirm="<?php echo $view['router']->generate('transaction_onepay_confirm_verifytel'); ?>"
                            onclick="vtrack('Click verify telephone', {'location':'Information Verification Page', 'again':'true', 'amount':<?php echo $view['vlance']->getParameter('payment_gateway.onepay.otp.105.amount') ?>})">
                        <?php echo $view['translator']->trans('profile.edit_verify_information.verify_telephone.verify_again', array(), 'vlance') ?>
                    </a>
                </b>
            </p>
        <?php endif; ?>
    </div>
    
            </div>
<?php echo $view->render('VlancePaymentBundle:Onepay:popup_pay_sms_verifytel.html.php', 
        array('acc'             => $auth_user, 
            'form_name'         => 'pay-verifytel', 
            'context_msg_path'  => 'paysms.verifytel'))?>
