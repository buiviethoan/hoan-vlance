<div class="clear"></div>
<div>
    <div class="row-fluid explain-content">
        <p>
            <b>Xác thực mã số thuế TNCN mang lại lợi ích gì?</b><br/>
            <ul>
                <li>Có cơ hội nhận được nhiều dự án từ khách hàng doanh nghiệp.</li>
                <li>Được khấu trừ thuế với các dự án khách hàng yêu cầu hóa đơn VAT.</li>
            </ul>
        </p>
        
        <?php 
            $showBtnSubmit = true;
            if(!$acc->getTaxId()){
                $showBtnSubmit = true;
            } else {
                if($acc->getTaxId()->getRequestedAt() != null){
                    $showBtnSubmit = false;
                }
            }
        ?>
        
        <?php if(!$acc->getTaxId()): ?>
        <p>
            <b>Làm cách nào để tra được mã số thuế TNCN?</b><br/>
            <ol type="1">
                <li>Truy cập vào trang <a href="http://tracuunnt.gdt.gov.vn/tcnnt/mstcn.jsp" target="_blank">www.tracuunnt.gdt.gov.vn/tcnnt/mstcn.jsp</a>, bấm vào phần <b>Thông tin người nộp thuế TNCN</b>.</li>
                <li>Chỉ cần điền <b>Số chứng minh thư, Mã xác nhận</b> và bấm nút <b>Tra cứu</b>.<br/>
                    Màn hình sẽ hiện ra giống hình minh họa sau:<br/><br/>
                    <img width="362" height="122" src="<?php echo $view['assets']->getUrl('img/pic_tax_id.jpg') ?>" /><br/><br/>
                </li>
                <li>Sao chép mã số thuế của bạn (như trong hình minh họa) và điền vào ô <b>Mã số thuế</b> bên dưới đây.</li>
                <li>Bấm nút <b>Yêu cầu xác thực</b>, vLance sẽ thực hiện yêu cầu của bạn.</li>
            </ol>
        </p>
        <?php endif; ?>
    </div>
    <div>
        <form id="form-tax-id" class="edit-form" action="<?php echo $view['router']->generate('verify_tax_id_submit',array()) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
            <dl class="dl-horizontal">
                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_tax_id.label', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div class="id_number">
                        <span>
                            <?php echo $view['form']->errors($form['idNumber']); ?>
                            <?php echo $view['form']->widget($form['idNumber']); ?>
                        </span>
                        <?php if($acc->getTaxId()): ?>
                            <?php if($acc->getTaxId()->getVerifiedAt() != null): ?>
                            <span class="verified-icon">
                                <i class="fa fa-check-circle"></i> <b>Đã xác thực</b>
                            </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </dd>
                <div class="clear"></div>
            </dl>

            <?php if($showBtnSubmit == true): ?>
            <dl class="dl-horizontal">
                <dt></dt>
                <dd>
                    <div class="submit-tax-id-number">
                        <?php echo $view['form']->row($form['_token'])?>
                        <button type="button" id="verify_tax_id" class="btn btn-large btn-primary" data-toggle="modal" data-target="#popup-verify-tax-id"><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.cta', array(), 'vlance') ?></button>
                    </div>
                </dd>
            </dl>
            <?php endif; ?>

            <?php if($showBtnSubmit == false): ?>
                <?php if($acc->getTaxId()->getVerifiedAt() == null): ?>
                <dl class="dl-horizontal">
                    <dt></dt>
                    <dd>
                        <div>
                            <p><i><b>vLance sẽ xác thực mã số thuế của bạn trong thời gian sớm nhất.</b></i></p>
                        </div>
                    </dd>
                </dl>
                <?php endif; ?>
            <?php endif; ?>
        </form>
    </div> 
</div>

<!-- popup confirm -->
<div id="popup-verify-tax-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <b id="myModalLabel">XÁC THỰC MÃ SỐ THUẾ</b>
    </div>
    <div class="modal-body">
        <p>
            Sau khi gửi yêu cầu xác thực bạn không thể thay đổi thông tin.<br/>
            Bạn chắc chắn muốn gửi yêu cầu xác thực?
        </p>
    </div>
    <div class="modal-footer">
        <a class="btn btn-link" data-dismiss="modal" aria-hidden="true">Quay lại</a>
        <a id="confirm-verify-tax-id" onclick="vtrack('Requested Verify Tax ID')" class="btn btn-primary btn-ok">Gửi yêu cầu</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (){
        $('#form-tax-id .popovers-input').popover();
        
        $("#form-tax-id").validate({
            rules: {
                "vlance_accountbundle_taxidtype[idNumber]": {
                    maxlength:  13,
                    minlength:  10,
                    isnumber:true,
                },
            }
        });
        
        //check idNumber is number
        jQuery.validator.addMethod("isnumber", function(val, element){
            if(isNaN(val)){
                return false;
            } else {
                return true;
            } 
        }, "Mã số thuế phải là chữ số");
        
        $("#verify_tax_id").click(function(event) {
            if($('#form-tax-id').valid() === false){
                $('#form-tax-id').validate().focusInvalid();
                return false;
            }
        });
        
        //gửi yêu cầu xác thực mã số thuế
        $("#confirm-verify-tax-id").click(function(){
            $("#popup-verify-tax-id").addClass("loading");
            $("#form-tax-id").submit();
        });
    });
</script>
