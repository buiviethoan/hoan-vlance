<?php use Vlance\BaseBundle\Utils\ResizeImage;?>
<?php /* @var $acc /Vlance/AccountBundle/Entity/Account */ ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<div class="clear"></div>

<?php 
    $time =  new \DateTime('now');
    if(is_object($acc->getPersonId())){
        if(!is_null($acc->getPersonId()->getRequestedAt())){
            $time = $acc->getPersonId()->getRequestedAt();
        }
    }
 ?>

<div>
    <div class="row-fluid explain-content">
        <div class="span6">
            <p>
                <b>Xác thực CMND mang lại những lợi ích gì?</b><br/>
                <ul>
                    <li>Đảm bảo hoàn toàn độ tin cậy đối với khách hàng và freelancer.</li>
                    <li><b style="color: black;">95%</b> tài khoản đã xác thực CMND qua vLance được thuê hoặc tìm được freelancer chất lượng.</li>
                    <li>Quá trình thanh toán nhanh hơn, các vấn đề tài khoản hay khó khăn khi làm việc có thể được giải quyết nhanh chóng.</li>
                </ul>
            </p>
        <?php if(!is_object($acc->getPersonId())): ?>
            <p>
                <b>Làm cách nào để xác thực CMND?</b><br/>
                <ol type="1">
                    <li>Scan và đăng lên <b>Ảnh CMND cả 2 mặt</b>.</li>
                    <li>Điền đầy đủ, chính xác <b>Số CMND, Ngày cấp, Nơi cấp</b>.</li>
                    <li>Bấm nút <b>Lưu thông tin CMND</b>.</li>
                    <li>Bấm nút <b>Xác thực CMND</b>.</li>
                    <li>VLance sẽ xác thực trong vòng 72 tiếng.</li>
                </ol>
            </p>
        </div>  
        <div class="span6">
            <p>
                <b>Chụp CMND như thế nào cho đúng cách?</b>
                <ol>
                    <li>CMND được chụp <b>chính diện, không nghiêng ngả.</b></li>
                    <li>Chụp trên <b>nền mầu trắng,</b> không bị xen lẫn chữ hay hình vẽ.</li>
                    <li>Ảnh chụp CMND phải thấy hết toàn bộ CMND, không bị cắt</li>
                    <li>Xoay ảnh đúng chiều đọc được khi chụp xong.</li>
                </ol>
            </p>
        </div>
        <div class="span6">
            <p>
                <b>Lưu ý khi xác thực CMND:</b><br/>
                <ul>
                    <li>vLance <b style="color: black;">KHÔNG BẮT BUỘC</b> xác thực CMND để hoạt động trên vLance.</li>
                    <li>Bạn sẽ mất <b style="color: black;">1 Credit</b> để xác thực CMND.</li>
                </ul>
            </p>
        <?php endif; ?>
        </div>
    </div>
    <div class="block-form span12">
        <form id="form-create-id-number" class="edit-form" action="<?php echo $view['router']->generate('verify_id_submit',array()) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
            <dl class="dl-horizontal">
                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step7', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div class="type_id_number">
                        <?php echo $view['form']->errors($form['type']); ?>
                        <?php echo $view['form']->widget($form['type']); ?>
                    </div>
                </dd>
                <div class="clear"></div>
            </dl>
            
            <br/>
            
            <dl class="dl-horizontal">
                <dt>
                    <div>
                        <?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step1', array(), 'vlance') ?> mặt trước<span class="text-red">*</span>
                    </div>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_title_id_number_image_explain', array(), 'vlance') ?></div>
                </dt>
                <dd>
                    <div class="images-id row-fluid" style="float: none;">
                        <?php $resize_front = ResizeImage::resize_image($personid->getUploadDir().DS.$personid->getPath().DS.$personid->getStorednameFront(), '150x100', 150, 100, 1);?>
                        <?php if($resize_front) : ?>
                            <div class="img-id">
                                <img src="<?php echo $resize_front; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            </div>
                        <?php else: ?>
                            <div class="img-id">
                                <img width="150" height="100" src = "<?php echo $view['assets']->getUrl('img/front.jpg') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            </div>
                        <?php endif; ?>
                        <?php if($is_requested == false): ?>
                            <div class="file_upload_front">
                                <?php echo $view['form']->errors($form['fileFront']); ?>
                                <?php echo $view['form']->widget($form['fileFront']); ?>
                            </div>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </div>
                </dd>
                
                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step2', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div class="id_number">
                        <span>
                            <?php echo $view['form']->errors($form['idNumber']); ?>
                            <?php echo $view['form']->widget($form['idNumber']); ?>
                        </span>
                        <?php if(is_object($acc->getPersonId())): ?>
                            <?php if(!is_null($acc->getPersonId()->getVerifyAt())): ?>
                                <span class="verified-icon">
                                    <i class="fa fa-check-circle"></i> <b>Đã xác thực</b>
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.label_id_number_explain', array(), 'vlance') ?></div>
                </dd>
                <div class="clear"></div>

                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step3', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div>
                        <?php echo $view['form']->errors($form['fullName']); ?>
                        <?php echo $view['form']->widget($form['fullName']); ?>
                    </div>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.label_title_info_fullname_announce', array(), 'vlance') ?></div>
                </dd>
                <div class="clear"></div>

                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step6', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div>
                        <div class="datepicker-widget input-append">
                            <input <?php if($acc->getPersonId()){
                                            if($acc->getPersonId()->getIsVerified() === true){echo "disabled";} 
                                        }
                                    ?> 
                                type="text" id="vlance_accountbundle_personidtype_dateOfBirth" name="vlance_accountbundle_personidtype_dateOfBirth" 
                                required="required" data-toggle="popover" data-placement="right" 
                                data-content="Ngày sinh của bạn. <br/><i>vLance.vn không tiết lộ thông tin này với bất kỳ ai</i>" 
                                data-trigger="hover" data-html="true" placeholder="VD: 22/08/1985" 
                                class="span12 popovers-input"
                                value="<?php if($acc->getBirthday() != NULL){echo date_format($acc->getBirthday(), "d/m/Y");} ?>"
                                style="background-image: none; background-position: 0% 0%; background-repeat: repeat;">
                            <span class="add-on"><i class="i32 i32-calendar"></i></span>
                        </div>
                    </div>
                </dd>
                <div class="clear"></div>
            </dl>
            
            <br/>
            
            <dl class="dl-horizontal">
                <dt>
                    <div>
                        <?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step1', array(), 'vlance') ?> mặt sau<span class="text-red">*</span>
                    </div>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_title_id_number_image_explain', array(), 'vlance') ?></div>
                </dt>
                <dd>
                    <div class="images-id">
                        <?php $resize_back = ResizeImage::resize_image($personid->getUploadDir().DS.$personid->getPath().DS.$personid->getStorednameBack(), '150x100', 150, 100, 1);?>
                        <?php if($resize_back) : ?>
                        <div class="img-id"><img src="<?php echo $resize_back; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" /></div>
                        <?php else: ?>
                            <div class="img-id">
                                <img width="150" height="100" src = "<?php echo $view['assets']->getUrl('img/back.jpg') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            </div>
                        <?php endif; ?>
                        
                        <?php if($is_requested == false): ?>
                        <div class="file_upload_back">
                            <?php echo $view['form']->errors($form['fileBack']); ?>
                            <?php echo $view['form']->widget($form['fileBack']); ?>
                        </div>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </dd>

                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step4', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div>
                        <?php echo $view['form']->errors($form['issuedDate']); ?>
                        <?php echo $view['form']->widget($form['issuedDate']); ?>
                    </div>
                </dd>
                <div class="clear"></div>

                <dt><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.label_step5', array(), 'vlance') ?><span class="text-red">*</span></dt>
                <dd>
                    <div>
                        <?php echo $view['form']->errors($form['issuedLocation']); ?>
                        <?php echo $view['form']->widget($form['issuedLocation']); ?>
                    </div>
                </dd>
                <div class="clear"></div>
            </dl>
            
            <?php //chua gui yeu cau xac nhan CMND ?>
            <?php if($is_requested == false): ?>
                <dl class="dl-horizontal">
                    <dt></dt>
                    <dd>
                        <div class="submit-id-number">
                            <?php echo $view['form']->row($form['_token'])?>
                            <input id="btn-submit-id-form" type="button" class="btn btn-large btn-verify-id" value="<?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.save', array(), 'vlance') ?>"/>
                        </div>
                    </dd>
                </dl>
                
                <?php if(is_object($acc->getPersonId())): ?>
                <dl class="dl-horizontal">
                    <dt></dt>
                    <dd>
                        <div class="block-verify">
                            <div>
                                <p>Bạn đã cập nhật đầy đủ thông tin CMND?<br/> Hãy xác thực CMND để tăng độ tin cậy của bạn.</p>
                            </div>
                            <div class="request-verify-id">
                                <button type="button" id="verify_id" class="btn btn-large btn-primary" data-toggle="modal" data-target="#popup-confirm-verify-id"><?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.cta', array(), 'vlance') ?></button>
                            </div>
                        </div>
                    </dd>
                </dl>
                <?php endif; ?>
            
            <?php //da gui yeu cau xac nhan CMND ?>
            <?php else: ?>
                <?php if(is_null($acc->getPersonId()->getVerifyAt())): ?>
                <dl class="dl-horizontal">
                    <dt></dt>
                    <dd>
                        <div class="submit-id-number">
                            <?php echo $view['form']->row($form['_token'])?>
                            <input id="btn-submit-requested" type="button" class="btn btn-large btn-verify-id" value="<?php echo $view['translator']->trans('profile.edit_verify_information.verify_id.save', array(), 'vlance') ?>"/>
                        </div>
                    </dd>
                </dl>
                <?php endif; ?>
            <?php endif; ?>
            
            <?php //da gui yeu cau xac nhan CMND ?>
            <?php if($is_requested == true): ?>
                <?php if(is_null($acc->getPersonId()->getVerifyAt())): ?>
                    <dl class="dl-horizontal">
                        <dt></dt>
                        <dd>
                            <div>
                                <p>
                                    <i>
                                        Bạn đã gửi yêu cầu xác thực <b><?php echo $acc->getPersonId()->getRequestedAt()->format('d/m/Y H:i:s') ?></b><br/>
                                        vLance sẽ xác thực CMND của bạn vào lúc <b><?php echo $view['vlance_account']->getTimeVerifyPersonId($time); ?></b>.
                                    </i>
                                </p>
                            </div>
                        </dd>
                    </dl>
                <?php endif; ?>
            <?php endif; ?>
        </form>
    </div> 
</div>

<!-- Modal confirm -->
<div id="popup-confirm-verify-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <b id="myModalLabel">XÁC NHẬN CHỨNG MINH NHÂN DÂN</b>
    </div>
    <div class="modal-body">
        <p>
            Sau khi gửi yêu cầu xác thực bạn không thể thay đổi thông tin CMND.<br/>
            Bạn chắc chắn muốn gửi yêu cầu xác thực?
        </p>
    </div>
    <div class="modal-footer">
        <a class="btn btn-link" data-dismiss="modal" aria-hidden="true">Quay lại</a>
        <a id="confirm-verify-id" onclick="vtrack('Requested Verify Person ID')" class="btn btn-primary btn-ok">Gửi yêu cầu</a>
    </div>
</div>

<div id="popup-submit-verify-id" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <b id="myModalLabel">XÁC NHẬN CHỨNG MINH NHÂN DÂN</b>
    </div>
    <div class="modal-body" style="text-align: center;">
        <p>Xác nhận Chứng minh nhân dân mất <b>1 Credit</b>.</p>
        <p>Bạn có muốn gửi yêu cầu xác thực CMND ngay bây giờ?</p>
        <p class="show-credit-user" style="font-size: 12px;">
            <span style="color: #999999; font-size: 12px;">Bạn hiện có: <?php echo $acc->getCredit()->getBalance();?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
        </p>
        <?php if($acc->getCredit()->getBalance() < 1): ?>
            <p style="font-size: 12px; color: red;">Bạn không đủ credit</p>
        <?php endif; ?>
    </div>
    <div class="modal-footer">
        <a id="btn-request-verify-id" data-dismiss="modal" aria-hidden="true" onclick="vtrack('Requested Verify Person ID')" class="btn btn-primary btn-ok">Lưu thông tin và gửi yêu cầu</a>
    </div>
</div>

<div id="popup-submit-requested" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <b id="myModalLabel">XÁC NHẬN CHỨNG MINH NHÂN DÂN</b>
    </div>
    <div class="modal-body">
        <p>CMND của bạn sẽ được xác thực lúc <b><?php echo $view['vlance_account']->getTimeVerifyPersonId($time); ?></b>. Bạn muốn lưu thông tin? </p>
    </div>
    <div class="modal-footer">
        <a class="btn btn-close" data-dismiss="modal" aria-hidden="true">Đóng</a>
        <a id="btn-submit-requested-id" class="btn" data-dismiss="modal" aria-hidden="true" >Lưu thông tin</a>
    </div>
</div>

<div id="popup-verified-id-noti" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <b id="myModalLabel">XÁC NHẬN CHỨNG MINH NHÂN DÂN</b>
    </div>
    <div class="modal-body">
        <div class="verified-id-status success">Bạn đã gửi thành công yêu cầu xác thực CMND.</div>
        <div class="verified-id-status fail">Gửi yêu cầu không thành công. Bạn hãy thử lại sau.</div>
        <div class="verified-id-status birthday">Bạn chưa điền thông tin ngày sinh.<br/> Hãy cập nhật <b><i>ngày sinh</i></b> của bạn trong <a href="<?php echo $view['router']->generate('account_basic_edit', array('id' => $acc->getId())) ?>">Cập nhật tài khoản</a> trước khi yêu cầu xác thực chứng minh nhân dân.</div>
    </div>
    <div class="modal-footer">
        <a class="btn btn-close" data-dismiss="modal" aria-hidden="true">Đóng</a>
    </div>
</div>

<script type="text/javascript">
    var file_required = <?php if(!$acc->getPersonId()){echo "true";} else {echo "false";} ?>;
    var url = "<?php echo $view['router']->generate('verify_id_request',array()); ?>";
    var action = "<?php echo $view['router']->generate('verify_id_submit_request',array()); ?>";
</script>