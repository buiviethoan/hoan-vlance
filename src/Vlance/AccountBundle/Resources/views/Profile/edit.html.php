<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Sửa hồ sơ của %username%', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->start('content')?>

<div class="profile-tab">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId())) ?>">Thông tin cá nhân</a></li>
        <li class="active"><a>Hồ sơ làm việc</a></li>
        <li><a href="<?php echo $view['router']->generate('account_portfolio_edit',array('id' => $entity->getId())) ?>">Hồ sơ năng lực</a></li>
        <li><a href="<?php echo $view['router']->generate('account_verify_information',array()) ?>"><?php echo $view['translator']->trans('profile.edit_verify_information.tab_title', array(), 'vlance') ?></a></li>
    </ul>
</div> 
<?php echo $view->render('VlanceAccountBundle:Profile/edit:content.html.php', array('form' => $edit_form,'entity' => $entity, 'referer' => $referer)) ?>
<?php $view['slots']->stop();?>