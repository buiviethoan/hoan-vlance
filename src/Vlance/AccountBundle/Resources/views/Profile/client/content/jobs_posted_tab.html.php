<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<?php /* Việc đã đăng */ ?>
<h2><?php echo $view['translator']->trans('profile.menu_left.posted_jobs', array(), 'vlance') ?></h2>
<?php if(count($jobs_create) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else: ?>
    <?php foreach ($jobs_create as $job) : ?>
        <?php echo $view->render('VlanceAccountBundle:Profile/client/content/item:job_posted.html.php', array('entity' => $entity, 'job_item' => $job, 'acc' => $acc)) ?>
    <?php endforeach; ?>
<?php endif; ?>