<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 1): ?> last<?php endif; */?>">
    <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
        <?php if($job_item->getIsPrivate()):?>
            <?php // If is admin, see job link ?>
            <?php if(is_object($acc)):?>
                <?php if($acc->isAdmin()):?>
                <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                    <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                </a> -
                <?php endif;?>
            <?php endif;?>
            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
            <i class="icon-lock"></i>
        <?php else:?>
            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
            </a>
        <?php endif;?>
    </h3>
    <div class="description">
        <?php // Started At Date ?>
        <?php if($job_item->getStartedAt()): ?>
            <span class="time"><?php echo $job_item->getStartedAt()->format('d/m/Y') ?></span>
            <span class="separator">|</span>
        <?php endif; ?>

        <?php // Category of job ?>
        <?php if($job_item->getCategory()):?>
            <span class="category"><?php echo $job_item->getCategory()->getTitle() ?></span>
            <span class="separator">|</span>
        <?php endif;?>

        <?php // Budget of job ?>
        <span class="price">
            <?php 
                if($job_item->getbudgetBided() > 0) :
                    $ngansach = $job_item->getbudgetBided();
                else:
                    $ngansach = $job_item->getBudget();
                endif;
            ?>
            <?php echo number_format($ngansach,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
        </span>

        <?php // Rating of job, given to freelancer?>
        <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
        <?php if(count($feedbacks) > 0) :?>
            <?php foreach ($feedbacks as $feedback) :?>
                <?php $assignee = $feedback->getReceiver();?>
                <?php if($feedback->getReceiver()->getId() == $assignee->getId()
                        && $assignee->getNumReviews() != 0): ?>
                    <span class="separator">|</span>
                    <span class="rating_pf">
                        <div class="rating-box">
                            <?php 
                            $score = ($feedback->getDegree() + $feedback->getDuration() + $feedback->getPrice() + $feedback->getProfessional() + $feedback->getQuality())/5;
                            $rating = $score * 20;
                            ?>
                            <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                        </div>
                    </span>
                <?php endif;?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php // else: ?>

    <?php // Feedback of job, given to client ?>
    <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
    <?php if(count($feedbacks) > 0) :?>
        <?php foreach ($feedbacks as $feedback) :?>
            <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
                <div class="comment comment-three comment-fd-<?php echo $feedback->getId(); ?>">
                    <div class="ds">
                        <strong><?php echo $view['translator']->trans('profile.right.review1',array(),'vlance'); ?> </strong>
                        "<?php echo htmlentities(JobCalculator::replacement($feedback->getComment()), ENT_SUBSTITUTE, "UTF-8") ?>"
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?> 
</div>