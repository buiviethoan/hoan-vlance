<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 2): ?> last<?php endif; */ ?>">
    <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
        <?php if($job_item->getIsPrivate()):?>
            <?php // If is admin, see job link ?>
            <?php if(is_object($acc)):?>
                <?php if($acc->isAdmin() || $acc->getId() == $job_item->getAccount()->getId()):?>
                <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                    <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                </a> -
                <?php endif;?>
            <?php endif;?>
            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
            <i class="icon-lock"></i>
        <?php else:?>
            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
            </a>
        <?php endif;?>
    </h3>
    <div class="description">
        <?php if($job_item->getStartedAt()): ?>
            <span class="time"><?php echo $job_item->getStartedAt()->format('d/m/Y') ?></span>
            <span class="separator">|</span>
        <?php endif; ?>
        <span class="category"><?php echo $job_item->getCategory() ? $job_item->getCategory()->getTitle() : '' ?></span>
        <span class="separator">|</span>

        <span class="price">
            <?php 
                if($job_item->getbudgetBided() > 0) :
                    $ngansach = $job_item->getbudgetBided();
                else:
                    $ngansach = $job_item->getBudget();
                endif;
            ?>
            <?php echo number_format($ngansach,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
        </span>
        <?php if($job_item->isEscrowed()): ?>
        <?php // Hien thi trang thai cong viec da dang ?>
        <span class="status">
            <?php echo $view['translator']->trans('profile_client.center.escrowed',array(),'vlance'); ?>
        </span>
        <?php endif; ?>
        <span class="separator">|</span>
        <?php // hien thi trang thai cong viec da dang ?>
        <span class="state">
            <?php if($job_item->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_OPEN): ?>
                <?php 
                    $now = date('Y-m-d H:i:s', time());
                    if($now > $job_item->getCloseAt()->format('Y-m-d H:i:s')) {
                       echo $view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance');
                    } else {
                        echo $view['translator']->trans($job_item->getStatusText(), array(), 'vlance');
                    }
                ?>
            <?php else : ?>
                <?php echo $view['translator']->trans($job_item->getStatusText(), array(), 'vlance') ?>
            <?php endif; ?>
        </span>
    </div>
    <?php // else: ?>
<!--                                    <div class="description">
        <?php // echo htmlentities(JobCalculator::cut(JobCalculator::replacement($job_item->getDescription()),150), ENT_SUBSTITUTE, "UTF-8") ?>
    </div>-->
    <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
    <?php if(count($feedbacks) > 0) :?>
        <?php foreach ($feedbacks as $feedback) :?>
            <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
<!--                                                <div class="review">
                    <div class="name">
                        <?php // echo ' '.$view['translator']->trans('profile.center.evaluated', array(), 'vlance') ?> 
                    </div>
                </div>-->

                <div class="comment comment-three comment-fd-<?php echo $feedback->getId(); ?>">
                    <div class="ds"><strong><?php echo $view['translator']->trans('profile.right.review1',array(),'vlance'); ?> </strong><?php echo htmlentities(JobCalculator::replacement($feedback->getComment()), ENT_SUBSTITUTE, "UTF-8") ?></div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?> 
</div>