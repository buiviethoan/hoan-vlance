<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<?php /* Việc đã giao */ ?>
<h2><?php echo $view['translator']->trans('profile_client.menu_left.hired_jobs', array(), 'vlance') ?></h2>
<?php if(count($jobs_create) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else : ?>
    <?php foreach($jobs_create as $job_item) : ?>
        <?php if(is_object($job_item)) : ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/client/content/item:job_hired.html.php', array('entity' => $entity, 'job_item' => $job_item, 'acc' => $acc)) ?>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>