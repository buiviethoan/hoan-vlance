<?php /* Việc đã giao */ ?>
<div class="profile-tab">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getHash())) ?>"><?php echo $view['translator']->trans('profile.center.freelancer', array(), 'vlance'); ?></a></li>
        <li class="active"><a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $entity->getHash())) ?>"><?php echo $view['translator']->trans('profile.center.client', array(), 'vlance'); ?></a></li>
    </ul>
</div>    
<div class="row-fluid job-list job-hired">
    <h2><?php echo $view['translator']->trans('profile_client.menu_left.hired_jobs', array(), 'vlance') ?></h2>
<?php if(count($jobs_hired) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else : ?>
    <?php $i = 0;?>
    <?php foreach($jobs_hired as $job_item) : ?>
        <?php if(is_object($job_item) && $i < 3) : ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/client/content/item:job_hired.html.php', array('entity' => $entity, 'job_item' => $job_item, 'acc' => $acc)) ?>
            <?php $i++;?>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if(count($jobs_hired) > 2) : ?>
        <div class="more">
            <a data-toggle="tab" onclick="scrollWindow('#tabs-profile li.job-hired a')" href="#viec-da-thue">
                <?php echo $view['translator']->trans('profile.center.read_more', array(), 'vlance') ?>
            </a>
        </div>
    <?php endif; ?>
  <?php endif; ?>
</div>