<?php /* Việc đã đăng */ ?>
<div class="row-fluid job-list job-posted">
    <h2><?php echo $view['translator']->trans('profile_client.menu_left.posted_jobs', array(), 'vlance') ?></h2>
<?php if(count($jobs_posted) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else : ?>
    <?php $i = 0; ?>
    <?php foreach($jobs_posted as $job) : ?>
        <?php if(is_object($job) && $i < 3) : ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/client/content/item:job_posted.html.php', array('entity' => $entity, 'job_item' => $job, 'acc' => $acc)) ?>
        <?php endif; ?>
        <?php $i++; ?>
     <?php endforeach; ?>
    <?php if(count($jobs_posted) > 2) : ?>
        <div class="more">
            <a data-toggle="tab" onclick="scrollWindow('#tabs-profile li.job-posted a')" href="#viec-da-dang">
                <?php echo $view['translator']->trans('profile.center.read_more', array(), 'vlance') ?>
            </a>
        </div>
    <?php endif; ?>
  <?php endif; ?>
</div>