<?php use Vlance\BaseBundle\Utils\ResizeImage;?>
<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<?php use Vlance\JobBundle\Entity\Job as Job;?>

<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 1): ?> last<?php endif; */?>">
    <div class="row-fluid">
        <div class="span2">
            <div class="avata-freelancer">
                <?php $freelancer = $job_item->getWorker();?>
                <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                <?php if(!is_object($freelancer)): ?>
                    <img itemprop="photo" src="<?php echo $profile_avatar ?>" />
                <?php else: ?>
                    <?php $real_avatar = ResizeImage::resize_image($freelancer->getFullPath(), '80x80', 80, 80, 1); ?>
                    <?php if($real_avatar) : ?>
                        <?php $profile_avatar = $real_avatar;?>
                    <?php endif;?>
                    <?php if($job_item->getIsPrivate()):?>
                        <?php $profile_avatar = $view['assets']->getUrl('img/private.png')?>
                            <img itemprop="photo" src="<?php echo $profile_avatar ?>" />
                    <?php else :?>
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer->getHash())) ?>">
                            <img itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $freelancer->getFullNameHidden(); ?>" title="<?php echo $freelancer->getFullNameHidden(); ?>" />
                        </a>    
                    <?php endif;?>
                <?php endif;?>
            </div>
        </div>
        
        <div class="job-comment span7">
            <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
                <?php if($job_item->getIsPrivate()):?>
                    <?php // If is admin, see job link ?>
                    <?php if(is_object($acc)):?>
                        <?php if($acc == $entity || $acc->isAdmin()):?>
                            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                                <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                            </a>
                            <i class="fa fa-lock" title="<?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>" alt="<?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>"></i>
                        <?php else:?>
                            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
                            <i class="fa fa-lock"></i>
                        <?php endif;?>
                    <?php else:?>    
                        <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
                        <i class="fa fa-lock"></i>    
                    <?php endif; ?>        
                <?php else:?>
                    <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                        <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                    </a>
                <?php endif;?>
            </h3>
            
            <?php // Feedback of job, given to client ?>
            <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
            <?php $flag_having_feedback = false; ?>
            <?php if(count($feedbacks) > 0) :?>
                <?php foreach ($feedbacks as $feedback) :?>
                    <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
                        <div class="comment comment-three comment-fd-<?php echo $feedback->getId(); ?>">
                            <div class="ds">
                                <?php echo htmlentities(JobCalculator::replacement($feedback->getComment()), ENT_SUBSTITUTE, "UTF-8") ?>
                            </div>
                            <?php if($job_item->getIsPrivate()):?>
                                <div class="freelancer-name"></div>
                            <?php else :?>
                                <div class="freelancer-name"><?php echo $freelancer->getFullNameHidden(); ?></div>
                            <?php endif; ?>    
                        </div>
                        <?php $flag_having_feedback = true; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?> 
            <?php if($flag_having_feedback == false): ?>
                <span style="font-style:italic;">Chưa có nhận xét</span>
            <?php endif; ?> 
        </div>    
        
        <div class="description span3">
            <?php // Status of job ?>
            <?php if ($job_item->getStatus() == Job::JOB_OPEN): ?>
                <?php // Check time bid-job ?>
                <?php $now = date('Y-m-d H:i:s', time()); ?>
                <?php if($now > $job_item->getCloseAt()->format('Y-m-d H:i:s')): ?>
                    <?php echo $view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance'); ?>
                <?php else: ?>
                    <?php echo $view['translator']->trans($job_item->getStatusText(), array(), 'vlance'); ?>
                <?php endif; ?>        
            <?php elseif ($job_item->getStatus() == Job::JOB_AWARDED 
                    || $job_item->getStatus() == Job::JOB_WORKING
                    || $job_item->getStatus() == Job::JOB_FINISH_REQUEST 
                    || $job_item->getStatus() ==Job::JOB_CANCEL_REQUEST): ?>
                <?php echo $view['translator']->trans('job.status.job_working', array(), 'vlance') ?>
            <?php elseif ($job_item->getStatus() == Job::JOB_FINISHED): ?>
                <div class="job_finished">
                    <?php echo $view['translator']->trans('job.status.job_finished', array(), 'vlance') ?>
                </div>
            <?php elseif ($job_item->getStatus() == Job::JOB_CANCELED): ?>
                <?php echo $view['translator']->trans('job.status.job_canceled', array(), 'vlance') ?> 
            <?php endif; ?>
            
            <?php // Budget of job ?>
            <div class="price">
                <?php 
                    if($job_item->getbudgetBided() > 0) {
                        $ngansach = $job_item->getbudgetBided();
                    }
                    else {
                        $ngansach = $job_item->getBudget();
                    }
                ?>
                <?php echo number_format($ngansach,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
            </div>
        </div>
        <?php // else: ?>
    </div>    
</div>