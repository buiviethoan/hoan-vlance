<?php /* @var $entity Account */ ?>
<?php if ($owner_view == true || (is_object($current_user) && $current_user->isAdmin())) : ?>
    <?php // When user view their own profile, show all information ?>
    <div class="summary-profile contact-client" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
        <h2><?php echo $view['translator']->trans('profile.edit_profile.label_title_contact', array(), 'vlance') ?></h2>
        <?php /* <!--Lien lac dien thoai--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-phone" style="font-size:24px;color:#23c3eb;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo htmlentities($entity->getTelephone(), ENT_SUBSTITUTE, "UTF-8") ?></dd>
            </dl>
        </div>

        <?php /* <!--Dia chi Email--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-envelope" style="font-size:20px;color:#23c3eb;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo htmlentities($entity->getEmail(), ENT_SUBSTITUTE, "UTF-8") ?></dd>
            </dl>
        </div>

        <?php /* <!--Lien lac skype--> 
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-skype" style="font-size:23px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo $view['translator']->trans('profile.right.hidden', array(), 'vlance') ?></dd>
            </dl>
        </div> 
        */ ?>

    </div>
<?php endif;?>

<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>