<?php use \Vlance\AccountBundle\Entity\Account; ?>
<?php /* @var $entity Account */ ?>
<?php $jobs_create = $entity->getNumJob() ?>
<?php $jobs_bid = $entity->getNumJobFinish() ?>
<div class="summary-profile" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
    <h2><?php echo $view['translator']->trans('profile.right.summary', array(), 'vlance') ?></h2>
    <?php /* <!--Viec lam--> */?>
    <?php /* div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.work_label', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7">
            <dt><a class="job-posted" title="<?php echo $jobs_create ?>"><?php echo $jobs_create ?></a></dt>
            <dd><?php echo $view['translator']->trans('profile_client.right.job_posted', array(), 'vlance') ?></dd>
            <dt><a class="job-done" title="<?php echo $entity->getNumJobDeposit() ?>"><?php echo $entity->getNumJobDeposit() ?></a></dt>
            <dd><?php echo $view['translator']->trans('profile_client.right.job_done', array(), 'vlance') ?></dd>
        </dl>
    </div */ ?>
    <?php /*<!--Danh gia-->*/?>
    <div class="list-summary-profile row-fluid">
        <?php $deposit = ($entity->getNumJob()!=0)?($entity->getNumJobDeposit()/$entity->getNumJob())*100:0; ?>
        <?php 
            $money_pay = $entity->getOutCome();
            $info_client = '<div>Tiền đã thuê : <b>'.number_format($money_pay,0,',','.').'</b> VNĐ</div>'
                    . '<div>Việc đã đăng : <b>'.$entity->getNumJob().'</b> việc</div>'
                    . '<div>Việc đã giao : <b>'.$entity->getNumJobDeposit().'</b> việc</div>';
            $ratio = 0;
            if($money_pay > 0 && $money_pay <= 1000000) {
                $ratio = 20;
            } elseif($money_pay > 1000000 && $money_pay <= 5000000) {
                $ratio = 40;
            } elseif($money_pay > 5000000 && $money_pay <= 10000000) {
                $ratio = 60;
            } elseif($money_pay > 10000000 && $money_pay <= 50000000) {
                $ratio = 80;
            }elseif($money_pay > 50000000 ) {
                $ratio = 100;
            }
        ?>
        <dt class="span8 job-awarded"><?php echo $view['translator']->trans('profile_client.right.review', array(), 'vlance') ?></dt>
        <dd class="dl-horizontal span4 rate">
            <div id="client-rating-box" class="list-job-rating">
                <div class="box-rating-client" data-toggle="popover" 
                    data-placement="right"  
                    data-content="<?php echo $info_client ?>"  
                    data-trigger="hover" data-html="true">
                    <div class="rating-box-client">
                        <div class="rating-client" style="width:<?php echo $ratio.'%'; ?>"></div>
                    </div>
                </div> 
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.box-rating-client').popover();
                    })
                </script>
            </div>
        </dd>
    </div>
    <div class="list-summary-profile row-fluid">
        <div class="span5 rent-money"><?php echo $view['translator']->trans('profile_client.right.paid_amount', array(), 'vlance') ?></div>
        <div class="dl-horizontal span7">
            <span class="money"><?php echo number_format($money_pay,0,',','.') ?> VNĐ</span>
        </div>
    </div>
</div>

<?php $acc_login = $app->getSecurity()->getToken()->getUser(); 
    if(is_object($acc_login)):
    $id_login = $acc_login->getID();
    $acc = $entity->getId();
?>
<?php // TODO. Táº¡m thá»�i táº¯t vÃ¬ chÆ°a cáº§n thiáº¿t ?>
<?php /*if($acc == $id_login) :?>
    <div class="verified-account row-fluid">
        <div class="span3"><div class="images-search-profile"></div></div>
        <div class="span9">
            <h2><?php echo $view['translator']->trans('profile.right.verify_account', array(), 'vlance') ?></h2>
            <div class="body-verified">
                <p><?php echo $view['translator']->trans('profile.right.verify_account_after', array('%verify%'=>'<a href="#" title="Kiá»ƒm chá»©ng tÃ i khoáº£n">Kiá»ƒm chá»©ng tÃ i khoáº£n</a>'), 'vlance') ?></p>
            </div>
        </div>
    </div>
<?php endif; */?>

    
<?php endif; ?>
<div class="adv-right-profile">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.Profile.Right.300x250 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:300px;height:250px"
         data-ad-client="ca-pub-7001499363485748"
         data-ad-slot="8854260515"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<div class="adv-300-250-profile-client-bottom">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.app profile client 300x250 (bottom) -->
    <ins class="adsbygoogle"
    style="display:inline-block;width:300px;height:250px"
    data-ad-client="ca-pub-7001499363485748"
    data-ad-slot="4219590517"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>