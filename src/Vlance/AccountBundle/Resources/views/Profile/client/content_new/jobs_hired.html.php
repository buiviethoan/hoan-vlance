<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<?php /* Việc đã giao */ ?>
<?php /* <div class="profile-tab">  //chuyen doi qua lai trang profile freelancer - client
    <ul class="nav nav-tabs">
        <li><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getHash())) ?>"><?php echo $view['translator']->trans('profile.center.freelancer', array(), 'vlance'); ?></a></li>
        <li class="active"><a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $entity->getHash())) ?>"><?php echo $view['translator']->trans('profile.center.client', array(), 'vlance'); ?></a></li>
    </ul>
</div> */ ?>    
<div class="row-fluid job-list job-hired">
    <h2><?php echo $view['translator']->trans('profile_client.menu_left.posted_jobs', array(), 'vlance') ?></h2>
    <div class="slide-job-hired">
    <?php if(count($jobs_hired) <= 0) : ?>
        <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
    <?php else : ?>
        <?php // Preparing job list ?>
        <?php $jobs_hired = array_reverse($jobs_hired->toArray()); ?>
        <?php $new_jobs_hired = array();?>
        <?php $k = 0; ?>
        <?php $new_jobs_hired[$k] = array(); ?>
        <?php foreach($jobs_hired as $key => $job_item) : ?>
            <?php if(count($new_jobs_hired[$k]) <3) : ?>
                <?php $new_jobs_hired[$k][] = $job_item; ?>
            <?php else: ?>
                <?php $k = $k + 1; ?>
                <?php $new_jobs_hired[$k] = array(); ?>
                <?php $new_jobs_hired[$k][] = $job_item; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php // Print out ?>
        <?php foreach($new_jobs_hired as $sublist_job_item) : ?>
            <div class="sublist-item">
                <?php foreach($sublist_job_item as $job_item) : ?>
                    <?php echo $view->render('VlanceAccountBundle:Profile/client/content_new/item:job_hired.html.php', array('entity' => $entity, 'job_item' => $job_item, 'acc' => $acc)) ?>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?> 
    </div>    
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.slide-job-hired').slick({
            speed:0,
            infinite: false
        });
    });
</script>