<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Sửa hồ sơ của %username%', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->start('content')?>

<div class="profile-tab">
    <ul class="nav nav-tabs">
        <li class="active"><a><?php echo $view['translator']->trans('profile.edit_profile.personal_information', array(), 'vlance') ?></a></li>
        <li><a href="<?php echo $view['router']->generate('account_edit',array('id' => $entity->getId())) ?>"><?php echo $view['translator']->trans('profile.edit_profile.work_profile', array(), 'vlance') ?></a></li>
        <li><a href="<?php echo $view['router']->generate('account_portfolio_edit',array('id' => $entity->getId())) ?>"><?php echo $view['translator']->trans('profile.edit_profile.ability_profile', array(), 'vlance') ?></a></li>
        <li><a href="<?php echo $view['router']->generate('account_verify_information',array()) ?>"><?php echo $view['translator']->trans('profile.edit_verify_information.tab_title', array(), 'vlance') ?></a></li>
    </ul>
</div> 
<?php echo $view->render('VlanceAccountBundle:Profile/edit_basic:content.html.php', array('form' => $edit_form,'entity' => $entity, 'referer' => $referer)) ?>
<?php $view['slots']->stop();?>