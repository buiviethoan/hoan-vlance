<form id="edit-acc" class="edit-form" action="<?php echo $view['router']->generate('account_update',array('id' => $entity->getId(), 'referer' => $referer)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <!-- Personal profile --> 
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.edit_profile.label_title_info', array(), 'vlance') ?></label></div>
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_one_title_info', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['type']); ?>
                <?php echo $view['form']->widget($form['type']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_two_title_info', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['title']); ?>
                <?php echo $view['form']->widget($form['title']); ?>
            </div>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.explain_one_title_info', array(), 'vlance') ?></div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_three_title_info', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div class="description">
                <?php // echo $view['form']->label($form['description']); ?>
                <?php echo $view['form']->errors($form['description']); ?>
                <?php echo $view['form']->widget($form['description']); ?>
            </div>
        </dd>
        <div class="clear"></div>

        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_four_title_info', array(), 'vlance') ?></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['website']); ?>
                <?php echo $view['form']->widget($form['website']); ?>
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <!-- Work profile --> 
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.edit_profile.label_title_experience', array(), 'vlance') ?></label></div>
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_one_title_experience', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['category']); ?>
                <?php echo $view['form']->widget($form['category']); ?>
            </div>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.explain_one_title_experience', array(), 'vlance') ?></div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_two_title_experience', array(), 'vlance') ?><span class="text-red">*</span> <span class="label-tag label-new">Mới</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['level']); ?>
                <?php echo $view['form']->widget($form['level']); ?>
            </div>
            <div class="subtitle">
                <?php echo $view['translator']->trans('profile.edit_profile.explain_two_title_experience', array(), 'vlance') ?>:<br/>
                <?php echo $view['translator']->trans('profile.edit_profile.explain_three_title_experience', array(), 'vlance') ?><br/>
                <?php echo $view['translator']->trans('profile.edit_profile.explain_four_title_experience', array(), 'vlance') ?><br/>
                <?php echo $view['translator']->trans('profile.edit_profile.explain_five_title_experience', array(), 'vlance') ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_three_title_experience', array(), 'vlance') ?></dt>
        <dd>
            <div class="form-add-skill">
                <div class="inner-skill">
                    <input type="text" data-trigger="hover" data-content="Thêm kỹ năng bạn sở hữu để gây ấn tượng với khách hàng" data-placement="right" data-toggle="popover" placeholder="Kỹ năng bạn có" data-provide="typeahead" name="hiddenTagListA" placeholder="Tags" class="tm-input"/>
                </div>
            </div>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.explain_six_title_experience', array(), 'vlance') ?></div>
            <div class="subtitle">Kỹ năng của bận không có trong danh sách trên? Hãy <a href="http://goo.gl/forms/PMDNrL5nAO" target="_blank">gửi gợi ý cho chúng tôi</a> để trở thành người đầu tiên trên vLance.vn có kỹ năng đó trong hồ sơ làm việc.</div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <!-- Service --> 
    <div><label class="personal-info" id="service-can-provide"><?php echo $view['translator']->trans('profile.edit_service.label_title_service', array(), 'vlance') ?></label></div>
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.edit_service.label_input_service', array(), 'vlance') ?> <span class="label-tag label-new">Mới</span></dt>
        <dd>
            <div class="form-add-service">
                <div class="inner-service">
                    <input type="text" data-trigger="hover" data-content="Liệt kê các dịch vụ bạn có thể cung cấp cho khách hàng." data-placement="right" data-toggle="popover" placeholder="Tên dịch vụ (VD: Thiết kế banner facebook,...)" data-provide="typeahead" name="hiddenTagListB" placeholder="Tags" class="tm-input-cat"/>
                </div>
            </div>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_service.description_service', array(), 'vlance') ?></div>
            <div class="subtitle">Dịch vụ bạn muốn cung cấp không có trong danh sách trên? Hãy <a href="http://goo.gl/forms/dGdgljyzdm" target="_blank">gửi gợi ý cho chúng tôi</a> để trở thành người đầu tiên được cung cấp dịch vụ này trên vLance.vn.</div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <!-- Availability -->
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_one_title_avaibility', array(), 'vlance') ?>?<span class="text-red">*</span> <span class="label-tag label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['workAvailability']); ?>
                <?php echo $view['form']->widget($form['workAvailability']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.guider_two_title_avaibility', array(), 'vlance') ?><span class="text-red">*</span> <span class="label-tag label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['workRhythm']); ?>
                <?php echo $view['form']->widget($form['workRhythm']); ?>
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <!-- Submit --> 
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt></dt>
        <dd>
            <div class="submit-form-edit-profile">
                <?php echo $view['form']->row($form['_token'])?>
                <input type="submit" class="btn btn-large btn-primary" value="<?php echo $view['translator']->trans('profile.edit_profile.button_save', array(), 'vlance') ?>"/>
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
</form>

<?php 
    $js_skill = array();
    $skills = $entity->getSkills();
    foreach($skills as $skill) {
        $js_skill[] = array($skill->getTitle(),$skill->getId());
    }
    
    $js_cat = array();
    $cats = $entity->getServices();
    foreach($cats as $c) {
        $js_cat[] = array($c->getTitle(),$c->getId());
    }
?>
<script type="text/javascript">
    var source = <?php echo $view['actions']->render($view['router']->generate('skill_list'))  ?>;
    var prefilledSkills = <?php echo json_encode($js_skill) ?>;
    var cat_source = <?php echo $view['actions']->render($view['router']->generate('service_list'))  ?>;
    var prefilledServices = <?php echo json_encode($js_cat) ?>;
</script>
