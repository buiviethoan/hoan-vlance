<?php use Vlance\BaseBundle\Utils\ResizeImage;?>
<?php $view->extend('VlanceBaseBundle::layout-2columns-right.html.php')?>

<?php $page_title = "profile.show.title.client"; ?>
<?php $view['slots']->set('title', $view['translator']->trans('profile.show.title.client', array('%name%' => $entity->getFullName(), '%category%' => $entity->getCategory() ? $entity->getCategory()->getTitle() : '' ), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans($page_title, array('%name%' => $entity->getFullName(), '%category%' => $entity->getCategory() ? $entity->getCategory()->getTitle() : ''), 'vlance'));?>
<?php $view['slots']->set('og_image_width', '300');?>
<?php $view['slots']->set('og_image_height', '300');?>

<?php // nofollow, noindex if profile is empty of less info ?>
<?php if ($entity->getVerifyInfoPercent() < 50) { $view['slots']->set('index_follow', 'noindex,nofollow'); }?>

<!-- Open graph content -->
    <?php $request = $this->container->get('request'); ?>

    <?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('account_show_client',array('hash' => $entity->getHash())));?>
    <?php $view['slots']->set('og_description', $meta_description);?>
    <?php $view['slots']->set('description', $meta_description);?>

    <?php $resize_face = ResizeImage::resize_image($entity->getFullPath(), '300x300', 300, 300, 1); ?>
    <?php if($resize_face) : ?>
        <?php $view['slots']->set('og_image', $resize_face);?>
    <?php else: ?>
        <?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
    <?php endif; ?>


<?php //End Rich snippets - People ?>
<!-- End Open graph content -->
<?php $view['slots']->start('content')?>
<?php //Breadcrum ?>
    <div style="display: none;">
        <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
          <a href="<?php echo $view['router']->generate('account_list') ?>" itemprop="url"> 
            <span itemprop="title">Khách hàng</span>
          </a> 
        </div>  
        <?php $filters = ''; ?>
        <?php if($entity->getCategory()) : ?>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <?php $chash = $entity->getCategory()->getHash(); ?>
            <?php $cpath = $entity->getCategory()->getParent()->getHash(); ?>
            <?php $filters = 'cpath_'.$cpath.'_chash_'.$chash ; ?>
            › <a href="<?php echo $view['router']->generate('account_list', array('filters' => $filters)) ?>" itemprop="url">
                <span itemprop="title"><?php echo $entity->getCategory()->getTitle() ?></span>
            </a>
            </div>
        <?php elseif($entity->getCity()): ?>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <?php $chash = $entity->getCity()->getHash(); ?>
            <?php $filters = 'city_'.$chash ; ?>
            › <a href="<?php echo $view['router']->generate('account_list', array('filters' => $filters)) ?>" itemprop="url">
                <span itemprop="title"><?php echo $entity->getCity()->getName() ?></span>
            </a>
            </div>
        <?php else: ?>
            <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="<?php echo $view['router']->generate('account_list') ?>" itemprop="url"> 
                  <span itemprop="title">Tất cả</span>
                </a> 
             </div> 
        <?php endif; ?>
    </div>
<?php // End Breadcrum ?>

    <?php /* <h1><?php echo $view['translator']->trans('%username%', array('%username%' => $entity->getFullNameHidden()))?></h1> */ ?>
    <?php echo $view->render('VlanceAccountBundle:Profile/client:content_new.html.php', array('entity' => $entity, 'abs_url' => $abs_url, 'hash_string' => $hash_string)) ?>
<?php $view['slots']->stop();?>
