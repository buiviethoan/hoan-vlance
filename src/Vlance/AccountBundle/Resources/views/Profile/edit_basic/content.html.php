<?php use Vlance\BaseBundle\Utils\ResizeImage;?>

<form id="edit-acc" class="edit-form" action="<?php echo $view['router']->generate('account_basic_update',array('id' => $entity->getId(), 'referer' => $referer)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.edit_profile.label_title_info', array(), 'vlance') ?></label></div>
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt>
            <?php echo $view['translator']->trans('profile.edit_portfolio.img_represent', array(), 'vlance') ?><span class="text-red">*</span>
            <div class="subtitle"><?php echo $view['translator']->trans('profile.edit_profile.label_title_info_image_explain', array(), 'vlance') ?></div>
        </dt>
        <dd>
            <div class="images">
                <!--<label><?php echo $view['translator']->trans('profile.edit_portfolio.img_represent', array(), 'vlance') ?></label>-->        
                <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '100x100', 100, 100, 1);?>
                <?php if($resize) : ?>
                <div class="img-avata"><img src="<?php echo $resize; ?>" alt="<?php echo $entity->getFullName(); ?>" title="<?php echo $entity->getFullName(); ?>" /></div>
                <?php else: ?>
                    <div class="img-avata">
                        <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $entity->getFullName(); ?>" title="<?php echo $entity->getFullName(); ?>" />
                    </div>
                <?php endif; ?>
                <div class="file_upload">
                <?php echo $view['form']->widget($form['file']) ?>
                </div>
                <div class="clear"></div>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.label_title_info_fullname', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['fullName']); ?>
                <?php echo $view['form']->widget($form['fullName']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt>Email<span class="text-red">*</span></dt>
        <dd>
            <div class="email-invalid">
                <?php echo $view['form']->row($form['email'])?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt>Skype</dt>
        <dd>
            <div class="skype-invalid">
                <?php echo $view['form']->row($form['skype'])?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.change_password',array(),'vlance')?></dt>
        <dd>
            <div class="change_password">
                <!--<label><?php echo $view['translator']->trans('profile.edit_profile.change_password',array(),'vlance')?></label>-->
                <?php echo $view['form']->errors($form['plainPassword']); ?>
                <?php echo $view['form']->widget($form['plainPassword'])?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.label_title_info_phone', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['telephone']); ?>
                <?php echo $view['form']->widget($form['telephone']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.label_title_info_city', array(), 'vlance') ?><span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['city']); ?>
                <?php echo $view['form']->widget($form['city']); ?>
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.edit_profile.label_title_other', array(), 'vlance') ?></label></div>
    <div class="clear"></div>
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('profile.edit_profile.label_title_other_step1', array(), 'vlance') ?></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['idCardNumber']); ?>
                <?php echo $view['form']->widget($form['idCardNumber']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt>Ngày sinh<span class="text-red">*</span></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['birthday']); ?>
                <?php echo $view['form']->widget($form['birthday']); ?>
            </div>
        </dd>
        <div class="clear"></div>
        
        <dt><?php echo $view['translator']->trans('profile.edit_profile.label_title_other_step3', array(), 'vlance') ?></dt>
        <dd>
            <div>
                <?php echo $view['form']->errors($form['address']); ?>
                <?php echo $view['form']->widget($form['address']); ?>
            </div>
        </dd>
        <div class="clear"></div>
    </dl>
    <div class="clear"></div>
    
    <dl class="dl-horizontal">
        <dt></dt>
        <dd>
            <div class="submit-form-edit-profile">
                <?php echo $view['form']->row($form['_token'])?>
                <input type="submit" class="btn btn-large btn-primary" value="<?php echo $view['translator']->trans('profile.edit_profile.button_save', array(), 'vlance') ?>"/>
            </div>
        </dd>
    </dl>
</form>