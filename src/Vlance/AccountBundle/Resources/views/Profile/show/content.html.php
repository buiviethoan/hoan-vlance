<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $is_owner = ($acc === $entity); // viewer in on his profile page ?>
<?php $jobs_create = $entity->getJobsPublish(); ?>
<?php $portfolios = $entity->getPortfolios();?>

<?php /* <?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminAccount_edit',array('pk' => $entity->getId())) . '" target="_blank" class="btn">'
        . '<i class="icon-pencil"></i> '
        . $view['translator']->trans('profile.center.admin_edit_profile_ds', array(), 'vlance')
    .'</a>'
);?> */ ?>

<div class="row-fluid" itemscope itemtype="http://data-vocabulary.org/Person">
    <div class="col3-left span4 left-profile">
        <?php /*<div class="gumshoe"><a href="#"><?php echo '+ '.$view['translator']->trans('profile.menu_left.watch_list', array(), 'vlance') ?></a></div>*/ ?>
        <div class="avata">
                <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                <?php $real_avatar = ResizeImage::resize_image($entity->getFullPath(), '170x170', 170, 170, 1); ?>
                <?php if($real_avatar) : ?>
                    <?php $profile_avatar = $real_avatar;?>
                <?php endif;?>
                <?php if($is_owner): // Display edit link on his profile page?>
                <div class="update-link tf300"><a class="tf300" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId(), 'referer' => 'redirect')) ?>"><?php echo $view['translator']->trans('profile.center.update_image', array(), 'vlance') ?></a></div>
                <?php endif;?>
                <img width="170" height="170" itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $entity->getFullName(); ?>" title="<?php echo $entity->getFullName(); ?>" />
        </div>
        <?php // Block tab left ?>
        <div class="dropdown clearfix">
            <ul class="nav nav-tabs nav-left" id="tabs-profile">
                <li class="active"><a href="#tong-quan"><?php echo $view['translator']->trans('profile.menu_left.qverview', array(), 'vlance') ?></a></li>
                <li class="job_profile_menu"> <a data-toggle="tab" href="#viec-dang-lam"><?php echo $view['translator']->trans('profile.menu_left.jobs_working', array(), 'vlance') ?></a></li>                
                <li class="job_profile_menu"> <a data-toggle="tab" href="#viec-da-lam"><?php echo $view['translator']->trans('profile.menu_left.work_done', array(), 'vlance') ?></a></li>                
                <li class="job_portfolio_menu"><a data-toggle="tab" href="#ho-so-viec-lam"><?php echo $view['translator']->trans('profile.menu_left.job_profile', array(), 'vlance') ?></a></li>
                <li></li>
            </ul>
        </div>
        <div class="id_profile">
            <?php echo $view['translator']->trans('profile.menu_left.id_profile', array(), 'vlance').'. '.$entity->getId(); ?>
        </div>
        <div class="time-online-last">
            <?php echo $view['translator']->trans('profile.menu_left.last_time_online', array(), 'vlance').': ' ?><?php echo $entity->getLastLogin() ? $entity->getLastLogin()->format('d/m/Y') : '' ?>
        </div>
        <div class="fb-like" style="margin-bottom: 5px;" data-href="<?php echo $abs_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
        <?php // Google Plus +1 button ?>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <div class="g-plusone" data-size="medium" style="margin-top:3px;"></div>
    </div>
    <div class="col3-center span8 right-profile">
        <div class="row-fluid header-profile">
            <?php if(is_object($acc) && !$is_owner): // is logged in?>
                <?php if(count($acc->getJobsOpenForBid()) > 0): // has jobs open for bidding?>
                    <div id="invite-button" class="btn-group invite-project">
                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                            <?php echo $view['translator']->trans('profile.center.invite_to_bid_full', array(), 'vlance') ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach($acc->getJobsOpenForBid() as $jo):?>
                                <?php $checkJobInvite = FALSE;?>
                                <?php foreach ($job_invites as $jobInvite):?>
                                    <?php if(($jobInvite->getAccount()==$entity)&&($jobInvite->getJob()==$jo)): ?>
                                    <?php $checkJobInvite = TRUE;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                                <?php if($checkJobInvite){ ?>
                                    <li><a class="invite-action tf200" style="cursor: context-menu;" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>"><div class="overflow-text da-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right">Đã mời</span></a></li>
                                    <?php $checkJobInvite = FALSE;?>
                                    <?php }else{?>
                                    <li><a class="invite-action tf200" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>"><div class="overflow-text chua-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right"></span></a></li>
                                <?php } ?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <?php // invite freelancer join job ?>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            jQuery('#invite-button a.invite-action').click(function(){
                                that = $(this).parent();
                                $.ajax({
                                    'url': $(this).attr('ajax-href')
                                })
                                .done(function(res){
                                    that.find('div').removeClass("chua-moi");
                                    that.find('div').addClass("da-moi");
                                    that.find('span.label').html("Đã Mời");
                                    that.find('span.label').show();
                                    $('.navbar .upper-section .container #messages').remove();
                                    var alertClass = 'alert-error';
                                    if (res.error == 0) {
                                        alertClass = 'alert-success';
                                    }
                                    alertItem = '<div class="alert fade in ' + alertClass + '"><a class="close" data-dismiss="alert">×</a><div>' + res.errorMsg + '</div></div>';
                                    $("#messages .body-messages").html("");
                                    $("#messages .body-messages").append(alertItem);
                                    $("#messages").show();                    
                                    $(document).ready(function(){
                                        setTimeout( "$('#messages').hide();", 5000);
                                    });
                                })
                                .fail(function(){
                                })
                                .always(function(){

                                });
                                return;
                            });
                        });
                    </script>
                <?php endif;?>
            <?php endif;?>
            <div class="span12">
                <h1 style="float: left">
                    <span itemprop="name"><?php echo htmlentities($entity->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>
                        <?php // Certification icon ?>
                        <?php if ($entity->getIsCertificated()) : ?>
                        <img style="margin-top:-6px; width: 25px; height: 25px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                             title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if (!is_null($entity->getTelephoneVerifiedAt())): ?>
                        <img style="margin-top:-6px;width:25px; height:25px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                        <?php endif; ?>
                        <?php if (!is_null($entity->getPersonId())): ?>
                            <?php if (!is_null($entity->getPersonId()->getVerifyAt())): ?>
                            <img style="margin-top:-6px;width:25px; height:25px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php /*
                        <img style="margin-top:-6px;width: 25px; height:25px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND"> 
                         */ ?>
                        <script type="text/javascript" language="javascript">
                            jQuery(document).ready(function(){
                                $(function () {
                                    $('[data-toggle="tooltip"]').tooltip()
                                })        
                            });
                        </script>
                        <?php /* // Google+ +1 button ?>
                        <script src="https://apis.google.com/js/platform.js" async defer></script>
                        <g:plusone></g:plusone>
                        <?php */ ?>

                        <?php if($entity->isLocked()): // Account is locked, show the notification to public?>
                            <span class="label label-important">Tài khoản đã bị khóa</span>
                        <?php endif;?>

                        <?php // User is in his profile page, display Edit links ?>
                        <?php if($is_owner): ?>
                            <span class="update-profile-link tf200">
                                <a class="btn btn-small" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId(), 'referer' => 'redirect')) ?>"><?php echo $view['translator']->trans('profile.center.update_info', array(), 'vlance') ?></a>
                            </span>
                        <?php endif;?>
                    </span>
                </h1>

                <?php if($entity->getTitle() == "" && $entity->getCity() ==""): '' ?>
                <?php else: ?>
                    <div class="clearfix"></div>
                    <?php if(!$is_owner): ?>
                        <?php if($entity->getTelephone() && $entity->getEmail() && !($entity->isBanned()) && $entity->isTypeFreelancer()):?>
                            <span class="contact-block" style="float:right;margin-left:10px;">
                                <a class="btn btn-primary hidejs" style="display:none" data-toggle="modal" href="#pay-sms" 
                                        data-request='{"fid":<?php echo $entity->getId();?>, "pid":<?php echo is_object($acc) ? $acc->getId() : -1;?>}'
                                        onclick="vtrack('Click contact', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>','location':'freelancer page', 'category':'<?php echo $entity->getCategory()?$entity->getCategory()->getTitle():''?>', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
                                    <i class="fa fa-phone fa-lg"></i> <?php echo $view['translator']->trans('list_freelancer.right_content.contact', array(), 'vlance') ?></a>
                            </span>
                        <?php endif;?>
                    <?php endif; ?>
                    <div class="career">
                    <?php if($entity->getTitle()):?>
                        <span itemprop="title editable tf300">
                            <?php echo htmlentities($entity->getTitle(), ENT_SUBSTITUTE, "UTF-8");?>
                        </span>
                    <?php endif;?>
                    </div>
                    <div class="regional-price">
                        <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                            <span class="location editable tf300" itemprop="region"><?php echo $entity->getCity() ? $entity->getCity()->getName() : $view['translator']->trans('common.country', array(), 'vlance')  ?></span>
                        </span>
                        <?php // if($entity->getInCome()): ?>
                        <!--<span class="separator">|</span>-->
                        <!--<span class="price"><?php // echo $view['translator']->trans('common.price', array(), 'vlance').':'.$entity->getInCome().' '.$view['translator']->trans('profile.center.price_h', array(), 'vlance') ?></span>-->
                        <?php // endif; ?>
                    </div>
                <?php endif; ?>
            </div>      
        </div>

        <div class="tab-content profile-content">
            <?php /* Tổng quan */ ?>
            <div class="active tab-pane" id="tong-quan">
                <?php if($entity->getDescription()): ?>
                <div class="row-fluid overview">
                    <h2>
                        <?php echo $view['translator']->trans('profile.center.qverview', array(), 'vlance') ?>
                    </h2>
                    <div class="body">
                        <?php echo $entity->getDescription() ? nl2br(htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")): '' ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:jobs_working.html.php', array('entity' => $entity, 'jobs_working' => $jobs_working, 'acc' => $acc)) ?>
                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:jobs_worked.html.php', array('entity' => $entity, 'jobs_worked' => $jobs_worked, 'acc' => $acc)) ?>

                <?php /* Hồ sơ việc làm */ ?>
                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:portfolios.html.php', array('entity' => $entity, 'acc' => $acc)) ?>

                <?php /* Kỹ năng */ ?>
                <?php $skills = $entity->getSkills() ?>
                <?php if(count($skills) > 0): ?>
                    <?php $counter = 0; ?>
                    <div class="row-fluid">
                        <h2>
                            <?php echo $view['translator']->trans('common.skill', array(), 'vlance') ?>
                        </h2>
                        <div class="skills">
                            <?php foreach ($skills as $skill): ?>
                            <?php $counter++; ?>
                            <a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                               title="<?php print $skill->getTitle(); ?>"> <div><?php print $skill->getTitle() ?></div></a>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <?php // các Tab ?>                
            <?php // Tab việc đang làm ?>
            <div id="viec-dang-lam" class="tab-pane">
                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:jobs_working_tab.html.php', array('entity' => $entity, 'jobs_working' => $jobs_working, 'abs_url' => $abs_url, 'hash_string' => $hash_string, 'acc' => $acc)) ?>
            </div>

            <?php // Tab việc đã làm ?>
            <div id="viec-da-lam" class="tab-pane">
                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:jobs_worked_tab.html.php', array('entity' => $entity, 'jobs_worked' => $jobs_worked, 'abs_url' => $abs_url, 'hash_string' => $hash_string, 'acc' => $acc)) ?>
            </div>

            <?php // tab ho so viec lam ?>
            <div class="tab-pane" id="ho-so-viec-lam">
                <?php echo $view->render('VlanceAccountBundle:Profile/show/content:portfolios_tab.html.php', array('entity' => $entity, 'acc' => $acc)) ?>
            </div>
        </div>
    </div>

<script type="text/javascript">
        function scrollWindow(select){
            window.scrollTo(0,100);
            $(select).click();
        }
    $(document).ready(function() {        
        $('#tabs-profile a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        
        $(".comment-three .more" ).click(function() {
            $('.comment-fd-' + jQuery(this).attr("data-id")).toggleClass( "hidebody" );
//            document.getElementById("#more-comment-fd-" + jQuery(this).attr("data-id")).innerText ="Rút gọn";
        });
        
        $(".comment-list .more" ).click(function() {
            $('.comment-listfd-' + jQuery(this).attr("data-id")).toggleClass( "hidebody" );
//            document.getElementById("#more-listfd-" + jQuery(this).attr("data-id")).innerText ="Rút gọn";
          });
        
    });
</script>
</div>

<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $acc, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>
