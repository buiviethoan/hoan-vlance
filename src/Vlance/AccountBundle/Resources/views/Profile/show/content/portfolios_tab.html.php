<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<div class="row-fluid job-list portfolios">
    <h2 class="span12"><?php echo $view['translator']->trans('profile.center.job_profile', array(), 'vlance') ?></h2>
</div>
<div class="row-fluid portfolios">
  <?php echo $view['actions']->render($view['router']->generate('portfolio_show',array('aid' => $entity->getId()))) ?>
</div>