<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 1): ?> last<?php endif; */ ?>">
    <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
        <?php if($job_item->getIsPrivate()):?>
            <?php if(is_object($acc)):?>
                <?php if($acc->isAdmin()):?>
                    <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                        <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                    </a> -
                <?php endif;?>
            <?php endif;?>
            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
            <i class="icon-lock"></i>
        <?php else:?>
            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>"><?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?></a>
        <?php endif;?>
    </h3>
        <div class="description">
            <?php if($job_item->getStartedAt()): ?>
            <span class="time"><?php echo $job_item->getStartedAt()->format('d/m/Y') ?></span>
            <span class="separator">|</span>
            <?php endif; ?>
            <span class="category"><?php echo $job_item->getCategory() ? $job_item->getCategory()->getTitle() : '' ?></span>
            <span class="separator">|</span>

            <span class="price"><?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></span>

            <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
            <?php if(count($feedbacks) > 0) :?>
                <?php foreach ($feedbacks as $feedback) :?>
                    <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
                        <span class="separator">|</span>
                        <span class="rating_pf">
                            <?php if($entity->getNumReviews() != 0): ?>
                                <div class="rating-box">
                                    <?php 
                                    $score = ($feedback->getDegree() + $feedback->getDuration() + $feedback->getPrice() + $feedback->getProfessional() + $feedback->getQuality())/5;
                                    $rating = $score * 20;
                                    ?>
                                    <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                                </div>
                            <?php endif; ?>
                        </span>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    <?php // else: ?>
        <?php if(count($feedbacks) > 0) :?>
            <?php foreach ($feedbacks as $feedback) :?>
                <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
                    <div class="comment comment-three comment-fd-<?php echo $feedback->getId(); ?>">
                        <div class="ds"><strong><?php echo $view['translator']->trans('profile.right.review1',array(),'vlance'); ?> </strong><?php echo htmlentities(JobCalculator::replacement($feedback->getComment()), ENT_SUBSTITUTE, "UTF-8") ?></div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?> 
    <?php // endif; ?>
</div>