<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 1): ?> last<?php endif; */ ?>">
    <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
        <?php if($job_item->getIsPrivate()):?>
            <?php if(is_object($acc)):?>
                <?php if($acc->isAdmin()):?>
                    <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                        <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                    </a> -
                <?php endif;?>
            <?php endif;?>
            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
            <i class="icon-lock"></i>
        <?php else:?>
            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>"><?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?></a>
        <?php endif;?>
    </h3>
    <div class="description">
        <?php if($job_item->getStartedAt()): ?>
        <span class="time"><?php echo $job_item->getStartedAt()->format('d/m/Y') ?></span>
        <span class="separator">|</span>
        <?php endif; ?>
        <span class="category"><?php echo $job_item->getCategory() ? $job_item->getCategory()->getTitle() : '' ?></span>
        <span class="separator">|</span>

        <span class="price"><?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></span>
    </div>
</div>