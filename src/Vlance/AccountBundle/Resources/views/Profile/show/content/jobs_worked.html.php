<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<?php /* Việc đã làm(hoan thanh) */ ?>
<div class="row-fluid job-list job-worked">
    <h2><?php echo $view['translator']->trans('profile.center.work_done', array(), 'vlance') ?></h2>
<?php if(count($jobs_worked) < 1) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else : ?>
    <?php $i = 0;?>
    <?php foreach($jobs_worked as $job_item) : ?>
        <?php if(is_object($job_item) && $i < 3) : ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/show/content/item:job_worked.html.php', array('entity' => $entity, 'job_item' => $job_item, 'acc' => $acc)) ?>
            <?php $i++;?>
        <?php endif; ?>
    <?php endforeach; ?>
    <?php if(count($jobs_worked) > 2) : ?>
        <div class="more">
            <a data-toggle="tab" onclick="scrollWindow('#tabs-profile li.job_profile_menu a')" href="#viec-da-lam">
                <?php echo $view['translator']->trans('profile.center.read_more', array(), 'vlance') ?>
            </a>
        </div>
    <?php endif; ?>
  <?php endif; ?>
</div>
