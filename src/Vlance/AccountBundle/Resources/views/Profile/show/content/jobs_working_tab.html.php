<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<h2><?php echo $view['translator']->trans('profile.center.jobs_working', array(), 'vlance') ?></h2>
<?php if(count($jobs_working) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else: ?>
    <?php foreach ($jobs_working as $job) : ?>
        <?php echo $view->render('VlanceAccountBundle:Profile/show/content/item:job_working.html.php', array('entity' => $entity, 'job_item' => $job, 'acc' => $acc)) ?>
    <?php endforeach; ?>
<?php endif; ?>
