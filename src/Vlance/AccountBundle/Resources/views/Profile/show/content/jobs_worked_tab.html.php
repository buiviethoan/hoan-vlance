<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<h2><?php echo $view['translator']->trans('profile.center.work_done', array(), 'vlance') ?></h2>
<?php if(count($jobs_worked) <= 0) : ?>
    <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
<?php else: ?>
    <?php foreach ($jobs_worked as $job) : ?>
        <?php echo $view->render('VlanceAccountBundle:Profile/show/content/item:job_worked.html.php', array('entity' => $entity, 'job_item' => $job, 'acc' => $acc)) ?>
    <?php endforeach; ?>
<?php endif; ?>
