<?php /* @var $entity Account */ ?>
<?php $jobs_create = $entity->getNumJob() ?>
<div class="summary-profile" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
    <h2><?php echo $view['translator']->trans('profile.right.summary', array(), 'vlance') ?></h2>
    <?php /* <!--Viec lam--> */ ?>
    <div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.work_label', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7">
            <dt>
            <?php if (count($jobs_worked) > 0): ?>
                <a title="<?php echo count($jobs_worked) ?>" itemprop="ratingCount">
                    <?php echo count($jobs_worked) ?>
                </a>
            <?php else: ?>
                <a title="0" itemprop="ratingCount">0</a>
            <?php endif; ?>
            </dt>
            <dd></dd>
        </dl>
    </div>
    
    <?php /* <!--Danh gia--> */ ?>
    <div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.review', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7">
            <dt>
            <?php if ($entity->getNumReviews() != 0): ?>
                <a title="<?php echo round($entity->getScore() / $entity->getNumReviews(), 2); ?>" itemprop="ratingValue">
                    <?php echo number_format($entity->getScore() / $entity->getNumReviews(), '2', ',', '.') ?>
                </a>
            <?php else: ?>
                <a title="0" itemprop="ratingValue">0</a>
            <?php endif; ?>
            <span style="display:none;">5</span>
            </dt>
            <dd>
                <?php if ($entity->getNumReviews() != 0): ?>
                    <div class="rating-box num_star_rating" data-toggle="popover" 
                         data-placement="right"  
                         data-content="<?php echo number_format($entity->getScore() / $entity->getNumReviews(), '2', ',', '.') ?>"  
                         data-trigger="hover">
                             <?php $rating = ($entity->getScore() / $entity->getNumReviews()) * 20; ?>
                        <div class="rating" style="width:<?php echo number_format($rating, '2', '.', '.') . '%'; ?>"></div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('.list-summary-profile div.num_star_rating').popover();
                        })
                    </script>
                <?php else: ?>
                    <div class="rating-box">
                        <div class="rating" style="width:0%"></div>
                    </div>
                <?php endif; ?>
            </dd>
            <dt>
            <?php //Tinh so khach hang khuyen dung ?>
            <?php /* @var $entity /Vlance/AccountBundle/Entity/Account */ ?>
            <?php $reviews = $entity->getNumReviews(); ?>
            <?php if ($reviews > 0) : ?>
                <?php
                $feedbacks = $entity->getFeedBacksReceiver();
                $recomment = 0;
                foreach ($feedbacks as $feedback) {
                    if ($feedback->getRecommend() == TRUE) {
                        $recomment += 1;
                    }
                }
                $per_recomment = ($recomment / $reviews) * 100;
                ?>
                <a>
                    <?php echo number_format($per_recomment, '0', ',', '.') . '%'; ?>
                </a>
            <?php else: ?>
                <a title="0">0</a>
            <?php endif; ?>
            </dt>
            <dd><?php echo $view['translator']->trans('profile.right.recommend', array(), 'vlance') ?></dd>
        </dl>
    </div>
    
    <?php /* <!--Dem so khach hang da lam viec --> */ ?>
    <div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.customers', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7">
            <?php
            //Tinh so khach hang da lam viec voi freelancer
            $number_client = array();
            foreach ($jobs_worked as $jobworked) {
                if (in_array($jobworked->getAccount()->getId(), $number_client) == FALSE) {
                    $number_client[] = $jobworked->getAccount()->getId();
                }
            }
            ?>
            <dt><a title="<?php echo count($number_client); ?>"><?php echo count($number_client); ?></a></dt>
            <dd></dd>
            <?php /* <dt><a href="#" title="0">0%</a></dt>
              <dd><?php echo $view['translator']->trans('profile.right.visitors_back', array(), 'vlance') ?></dd> */ ?>
        </dl>
    </div>
    <?php /* <!--Da kiem duoc--> */ ?>
    <div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.have_earned', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7">
            <dt><a title="0"><?php echo number_format($entity->getInCome(), '0', ',', '.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></a></dt>
        </dl>
    </div>
</div>

<div class="adv-right-profile">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.Profile.Right.300x250 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:300px;height:250px"
         data-ad-client="ca-pub-7001499363485748"
         data-ad-slot="8854260515"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<div class="adv-300-250-profile-freelancer-bottom">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.app profile freelancer 300x250 (bottom) -->
    <ins class="adsbygoogle"
    style="display:inline-block;width:300px;height:250px"
    data-ad-client="ca-pub-7001499363485748"
    accesskey=""data-ad-slot="2603256510"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>