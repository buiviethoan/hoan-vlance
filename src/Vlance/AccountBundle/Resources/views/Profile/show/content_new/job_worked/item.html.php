<?php use Vlance\BaseBundle\Utils\ResizeImage;?>
<?php use Vlance\BaseBundle\Utils\JobCalculator;?>
<?php use Vlance\JobBundle\Entity\Job as Job;?>

<div class="job-item <?php /* if($i == 0): ?> first<?php endif; ?><?php if($i == 1): ?> last<?php endif; */ ?>">
    <div class="row-fluid">
        <div class="span2">
            <div class="avata-client">
                <?php $client = $job_item->getAccount();?>
                <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                <?php $real_avatar = ResizeImage::resize_image($client->getFullPath(), '80x80', 80, 80, 1); ?>
                <?php if($real_avatar) : ?>
                    <?php $profile_avatar = $real_avatar;?>
                <?php endif;?>
                <?php if($job_item->getIsPrivate()):?>
                    <?php $profile_avatar = $view['assets']->getUrl('img/private.png')?>
                        <img itemprop="photo" src="<?php echo $profile_avatar ?>" />
                <?php else: ?>
                    <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $client->getHash())) ?>">
                        <img itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $client->getFullName(); ?>" title="<?php echo $client->getFullName(); ?>" />
                    </a>
                <?php endif;?>
            </div>
        </div> 
        <div class="job-comment span7">
            <h3 class="title<?php echo ($job_item->getIsPrivate())?" private":""?>">
                <?php if($job_item->getIsPrivate()):?>
                    <?php if(is_object($acc)):?>
                        <?php if($acc == $entity || $acc->isAdmin()):?>
                            <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>">
                                <?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                            </a>
                            <i class="fa fa-lock" title="<?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>" alt="<?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>"></i>
                        <?php else:?>
                            <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
                            <i class="fa fa-lock"></i>
                        <?php endif;?> 
                    <?php else:?> 
                        <?php echo $view['translator']->trans('profile.center.private_job', array(), 'vlance')?>
                        <i class="fa fa-lock"></i>    
                    <?php endif; ?>
                <?php else:?>
                    <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $job_item->getHash())) ?>"><?php echo htmlentities($job_item->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?></a>
                <?php endif;?>
            </h3>
            <?php $feedbacks = $job_item->getFeedBacksJob(); ?>
            <?php // else: ?>
                <?php if(count($feedbacks) > 0) :?>
                    <?php foreach ($feedbacks as $feedback) :?>
                        <?php if($feedback->getReceiver()->getId() == $entity->getId()): ?>
                            <div class="comment comment-three comment-fd-<?php echo $feedback->getId(); ?>">
                                <div class="ds"><?php echo htmlentities(JobCalculator::replacement($feedback->getComment()), ENT_SUBSTITUTE, "UTF-8") ?></div>
                            </div>
                            <?php if($job_item->getIsPrivate()):?>
                                <div class="client-name"></div>
                            <?php else:?>
                                <div class="client-name"><?php echo $client->getFullName(); ?></div>
                            <?php endif; ?>    
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php else:?>    
                    <span style="font-style: italic;">Chưa có nhận xét</span>
                <?php endif; ?> 
            <?php // endif; ?>
        </div>
        <div class="description span3">
            <?php // Status of job ?>
            <?php if ($job_item->getStatus() == Job::JOB_AWARDED): ?>
                <?php echo $view['translator']->trans('profile.center.job_awarded', array(), 'vlance'); ?>
            <?php elseif ($job_item->getStatus() == Job::JOB_WORKING
                    || $job_item->getStatus() == Job::JOB_FINISH_REQUEST 
                    || $job_item->getStatus() == Job::JOB_CANCEL_REQUEST): ?>
                <?php echo $view['translator']->trans('job.status.job_working', array(), 'vlance') ?>
            <?php elseif ($job_item->getStatus() == Job::JOB_CANCELED): ?>
                <?php echo $view['translator']->trans('job.status.job_canceled', array(), 'vlance') ?> 
            <?php endif; ?>
            
            <?php // Rating & budget?>
            <?php $feedback = null ; ?>
            <?php if(count($feedbacks) > 0) :?>    
                <?php foreach ($feedbacks as $fb) :?>
                    <?php if($fb->getReceiver()->getId() == $entity->getId()): ?>
                        <?php $feedback = $fb;?>
                        <?php break; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            
            <?php // At least 1 feedback ?>
            <?php if($feedback): ?>
                <span class="rating_pf">
                    <?php if($entity->getNumReviews() != 0): ?>
                        <?php 
                            $score = ($feedback->getDegree() + $feedback->getDuration() + $feedback->getPrice() + $feedback->getProfessional() + $feedback->getQuality())/5;
                            $rating = $score * 20;
                        ?>
                        <div class="rating-box num_star_rating" style="float:right" data-toggle="popover" 
                            data-placement="bottom"  
                            data-content="<?php echo number_format($score, '1', ',', '.') ?>"  
                            data-trigger="hover">
                            
                            <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                        </div>
                        <div class="price">
                            <?php if($job_item->getType() == Job::TYPE_BID): ?>
                                <?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                            <?php else: ?>
                                <?php echo number_format($job_item->getBudget(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </span>
                
            <?php // There is 0 feedback ?>
            <?php else: ?>
                <?php if ($job_item->getStatus() == Job::JOB_FINISHED): ?>
                    <span class="rating_pf">
                            <div class="rating-box" style="float:right" title="Chưa có đánh giá"></div>
                            <div class="price">
                                <?php if($job_item->getType() == Job::TYPE_BID): ?>
                                    <?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php else: ?>
                                    <?php echo number_format($job_item->getBudget(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php endif; ?>
                            </div>
                    </span>
                <?php elseif ($job_item->getStatus() == Job::JOB_CANCELED) : ?> 
                    <span class="rating_pf">
                            <div class="rating-box" style="float:right"></div>
                            <div class="price">
                                <?php if($job_item->getType() == Job::TYPE_BID): ?>
                                    <?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php else: ?>
                                    <?php echo number_format($job_item->getBudget(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php endif; ?>
                            </div>
                    </span>
                <?php elseif ($job_item->getStatus() == Job::JOB_AWARDED
                        || $job_item->getStatus() == Job::JOB_WORKING
                        || $job_item->getStatus() == Job::JOB_FINISH_REQUEST 
                        || $job_item->getStatus() ==Job::JOB_CANCEL_REQUEST) : ?> 
                    <span class="rating_pf">    
                        <div class="price">
                            <?php if($job_item->getType() == Job::TYPE_BID): ?>
                                <?php echo number_format($job_item->getbudgetBided(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                            <?php else: ?>
                                <?php echo number_format($job_item->getBudget(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                            <?php endif; ?>
                        </div>
                    </span>    
                <?php endif; ?>
            <?php endif; ?>
            
        </div>
    </div>    
</div>