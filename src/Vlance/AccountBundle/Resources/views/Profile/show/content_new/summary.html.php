<?php use \Vlance\AccountBundle\Entity\Account; ?>
<?php /* @var $entity Account */ ?>
<?php $jobs_create = $entity->getNumJob() ?>
<div class="summary-profile profile-rate" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
    <h2><?php echo $view['translator']->trans('profile.right.summary', array(), 'vlance') ?></h2>
    <?php /* <!--Danh gia--> */ ?>
    <div class="list-summary-profile row-fluid">
        <dl class="dl-horizontal span7">
            <dt>
            <?php if ($entity->getNumReviews() != 0): ?>
                <a title="<?php echo round($entity->getScore() / $entity->getNumReviews(), 2); ?>" itemprop="ratingValue">
                    <?php echo number_format($entity->getScore() / $entity->getNumReviews(), '2', ',', '.') ?>
                </a>
            <?php else: ?>
                <a title="0" itemprop="ratingValue">0</a>
            <?php endif; ?>
            </dt>
            <dd>
                <?php if ($entity->getNumReviews() != 0): ?>
                    <div class="rating-box num_star_rating" data-toggle="popover" 
                         data-placement="right"  
                         data-content="<?php echo number_format($entity->getScore() / $entity->getNumReviews(), '2', ',', '.') ?>"  
                         data-trigger="hover">
                             <?php $rating = ($entity->getScore() / $entity->getNumReviews()) * 20; ?>
                        <div class="rating" style="width:<?php echo number_format($rating, '2', '.', '.') . '%'; ?>"></div>
                    </div>
                <?php else: ?>
                    <div class="rating-box">
                        <div class="rating" style="width:0%"></div>
                    </div>
                <?php endif; ?>
            </dd>
        </dl>
        <label class="span5" style="font-weight:600;text-align: right;">
            <?php //Tinh so khach hang khuyen dung ?>
            <?php /* @var $entity /Vlance/AccountBundle/Entity/Account */ ?>
            <?php $reviews = $entity->getNumReviews(); ?>
            <?php if ($reviews > 0) : ?>
                <?php
                $feedbacks = $entity->getFeedBacksReceiver();
                $recomment = 0;
                foreach ($feedbacks as $feedback) {
                    if ($feedback->getRecommend() == TRUE) {
                        $recomment += 1;
                    }
                }
                $per_recomment = $reviews;
                ?>
                <a>
                    <?php echo number_format($per_recomment, '0', ',', '.'); ?>
                </a>
            <?php else: ?>
                <a title="0">0</a>
            <?php endif; ?>
                
            <?php echo $view['translator']->trans('profile.right.review', array(), 'vlance') ?>
        </label>
    </div>
    
    <?php /* <!--Da kiem duoc--> */ ?>
    <div class="list-summary-profile row-fluid">
        <label class="span5"><?php echo $view['translator']->trans('profile.right.have_earned', array(), 'vlance') ?></label>
        <dl class="dl-horizontal span7" style="text-align:right;margin-left:5px;">
            <a title="0"><?php echo number_format($entity->getInCome(), 0, ',', '.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></a>
        </dl>
    </div>
    <div class="progress-bar">
        <span>Hoàn thành việc</span>
        <div class="progress">
            <?php if ($success_rate < 0.09 ) : ?>
            <div class="bar" style="width: <?php echo number_format($success_rate * 100,0,',','.'); ?>%;color:#000000;"><?php echo number_format($success_rate*100,0,',','.'); ?>%</div>
            <?php else: ?>
            <div class="bar" style="width: <?php echo number_format($success_rate * 100,0,',','.'); ?>%;"><?php echo number_format($success_rate*100,0,',','.'); ?>%</div>
        <?php endif; ?>    
    </div>
    </div>
    <div class="progress-bar">
        <span>Được thuê lại</span>
        <div class="progress">
            <?php if ($rehire_rate < 0.09) : // Neu duoi 9% thi chuyen text sang mau den ?>
            <div class="bar" style="width: <?php echo number_format($rehire_rate * 100, 0, ',', '.'); ?>%;color:#000000;"><?php echo number_format($rehire_rate*100,0,',','.'); ?>%</div>
            <?php else: ?>
            <div class="bar" style="width: <?php echo number_format($rehire_rate * 100, 0, ',', '.'); ?>%;"><?php echo number_format($rehire_rate*100,0,',','.'); ?>%</div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php /* <!--Lam viec--> */ ?>
<?php if($entity->getWorkRhythm() != '' || $entity->getLevel() != '') : ?>
<div class="summary-profile profile-rate" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
    <h2><?php echo $view['translator']->trans('profile.right.worked', array(), 'vlance') ?></h2>
    <?php /* <!--Viec lam--> */ ?>
    <div class="list-summary-profile row-fluid">
        <?php if($entity->getWorkRhythm() != '') : ?>
            <?php if ($entity->getWorkRhythm() == Account::WORK_RHYTHM_PARTTIME
                    || $entity->getWorkRhythm() == Account::WORK_RHYTHM_FULLTIME) : ?>    
                <div class="time-work">
                    <?php echo $view['translator']->trans('profile.right.' . $entity->getWorkRhythm(), array(), 'vlance') ?> 
                </div>
            <?php endif; ?>
        <?php endif; ?>
        
        <?php if($entity->getLevel() != ''): ?>
            <?php if($entity->getLevel() == Account::LEVEL_BEGINNER
                    || $entity->getLevel() == Account::LEVEL_PROFESSIONAL
                    || $entity->getLevel() == Account::LEVEL_EXPERT): ?>
                <div class="experienced">
                    <?php echo $view['translator']->trans('profile.right.' . $entity->getLevel(), array(), 'vlance') ?>       
                </div>
            <?php endif; ?>
        <?php endif; ?>   
        
    </div>
</div>
<?php endif; ?>   

<div class="adv-right-profile">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.Profile.Right.300x250 -->
    <ins class="adsbygoogle"
         style="display:inline-block;width:300px;height:250px"
         data-ad-client="ca-pub-7001499363485748"
         data-ad-slot="8854260515"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>
<div class="adv-300-250-profile-client-bottom">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <!-- vLance.app profile client 300x250 (bottom) -->
    <ins class="adsbygoogle"
    style="display:inline-block;width:300px;height:250px"
    data-ad-client="ca-pub-7001499363485748"
    data-ad-slot="4219590517"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</div>