<?php use Vlance\BaseBundle\Utils\JobCalculator;?>

<?php /* Việc đã làm(hoan thanh) */ ?>
<div class="row-fluid job-list job-worked">
    <h2>
        <?php echo $view['translator']->trans('profile.menu_left.jobs_work', array(), 'vlance') ?>
    </h2>
    <div class="slide-job-worked">
    <?php if(count($jobs_worked) < 1) : ?>
        <?php echo $view['translator']->trans('profile.center.no_job', array(), 'vlance') ?>
    <?php else : ?>
        <?php // Preparing job list ?>
        <?php $jobs_worked = array_reverse($jobs_worked); ?>
        <?php $new_jobs_worked = array();?>
        <?php $k = 0; ?>
        <?php $new_jobs_worked[$k] = array(); ?>
        <?php foreach($jobs_worked as $key => $job_item) : ?>
            <?php if(count($new_jobs_worked[$k]) <3) : ?>
                <?php $new_jobs_worked[$k][] = $job_item; ?>
            <?php else: ?>
                <?php $k = $k + 1; ?>
                <?php $new_jobs_worked[$k] = array(); ?>
                <?php $new_jobs_worked[$k][] = $job_item; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php // Print out ?>
        <?php foreach($new_jobs_worked as $sublist_job_item) : ?>
            <div class="sublist-item">
                <?php foreach($sublist_job_item as $job_item) : ?>
                    <?php echo $view->render('VlanceAccountBundle:Profile/show/content_new/job_worked:item.html.php', array('entity' => $entity, 'job_item' => $job_item, 'acc' => $acc)) ?>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>


        <?php /* if(count($jobs_worked) > 2) : ?>
            <div class="more">
                <a data-toggle="tab" onclick="scrollWindow('#tabs-profile li.job_profile_menu a')" href="#viec-da-lam">
                    <?php echo $view['translator']->trans('profile.center.read_more', array(), 'vlance') ?>
                </a>
            </div>
        <?php endif; */ ?>
      <?php endif; ?>
    </div>        
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.slide-job-worked').slick({
            speed:0,
            infinite: false
        });
    });
</script>
