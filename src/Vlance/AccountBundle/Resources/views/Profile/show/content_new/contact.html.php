<?php /* @var $entity Account */ ?>
<?php if ($owner_view == true || (is_object($current_user) && $current_user->isAdmin())) : ?>
    <?php // When user view their own profile, show all information ?>
    <div class="summary-profile contact-direct" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
        <h2><?php echo $view['translator']->trans('profile.edit_profile.label_title_contact', array(), 'vlance') ?></h2>
        <?php /* <!--Lien lac dien thoai--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-phone" style="font-size:24px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo htmlentities($entity->getTelephone(), ENT_SUBSTITUTE, "UTF-8") ?></dd>
            </dl>
        </div>

        <?php /* <!--Dia chi Email--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-envelope" style="font-size:20px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo htmlentities($entity->getEmail(), ENT_SUBSTITUTE, "UTF-8") ?></dd>
            </dl>
        </div>

        <?php /* <!--Lien lac skype--> 
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-skype" style="font-size:23px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo $view['translator']->trans('profile.right.hidden', array(), 'vlance') ?></dd>
            </dl>
        </div> 
        */ ?>

    </div>

<?php else: ?>
    <?php // When user view other profile, hide contact information, show contact button ?>
    <div class="summary-profile contact-direct" itemprop="rating" itemscope itemtype="http://schema.org/AggregateRating">
        <h2><?php echo $view['translator']->trans('profile.edit_profile.label_title_contact', array(), 'vlance') ?></h2>
        <?php /* <!--Lien lac dien thoai--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-phone" style="font-size:24px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo $view['translator']->trans('profile.right.hidden', array(), 'vlance') ?></dd>
            </dl>
        </div>

        <?php /* <!--Dia chi Email--> */ ?>
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-envelope" style="font-size:20px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo $view['translator']->trans('profile.right.hidden', array(), 'vlance') ?></dd>
            </dl>
        </div>

        <?php /* <!--Lien lac skype--> 
        <div class="list-summary-profile row-fluid">
            <label class="span2"><i class="fa fa-skype" style="font-size:23px;color:#0fe19b;"></i></label>
            <dl class="dl-horizontal span7">
                <dd><?php echo $view['translator']->trans('profile.right.hidden', array(), 'vlance') ?></dd>
            </dl>
        </div>
        */ ?>

        <?php if($entity->getTelephone() && $entity->getEmail() && !($entity->isBanned()) && $entity->isTypeFreelancer()):?>
        <div class="contact-block">
            <a class="btn btn-large btn-contact" data-toggle="modal" href="#pay-sms"
               data-request='{"fid":<?php echo $entity->getId();?>, "pid":<?php echo is_object($current_user) ? $current_user->getId() : -1;?>}'
               onclick="vtrack('Click contact', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>','location':'freelancer page', 'category':'<?php echo $entity->getCategory()?$entity->getCategory()->getTitle():''?>', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
            <?php echo $view['translator']->trans('list_freelancer.right_content.contact', array(), 'vlance') ?></a>
            <div>Nhấn <span>Liên hệ trực tiếp</span> để làm việc với freelancer ngay lập tức</div>
        </div>
        <?php endif;?>    
    </div>
<?php endif;?>

<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>