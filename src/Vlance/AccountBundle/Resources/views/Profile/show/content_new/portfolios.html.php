<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $portfolios = $entity->getPortfolios();?>
<div class="row-fluid job-list portfolios">
    <div class="span12">
        <h2><?php echo $view['translator']->trans('profile.center.job_profile', array(), 'vlance') ?></h2>
    </div>
    <div class="clear"></div>

    <?php if(count($portfolios) < 1) : ?>
        <?php if(is_object($acc)): ?>                       
            <?php if($acc->getId() === $entity->getId()) : ?>
                <div> <?php echo $view['translator']->trans('profile.center.not_job_profile_acc', array(), 'vlance') ?></div>
            <?php else: ?>
                <div><?php echo $view['translator']->trans('profile.center.not_job_profile_noacc', array('%a%'=> htmlentities($entity->getFullNameHidden(), ENT_SUBSTITUTE, "UTF-8")), 'vlance') ?></div>
            <?php endif; ?>
        <?php else: ?>
            <div><?php echo $view['translator']->trans('profile.center.not_job_profile_noacc', array('%a%'=> htmlentities($entity->getFullNameHidden(), ENT_SUBSTITUTE, "UTF-8")), 'vlance') ?></div>
        <?php endif; ?>
    <?php else : ?>
        <div class="row-fluid three-img-profile slide-portfolios">
            <?php for($i = 0 ; $i < count($portfolios) ; $i++) :  ?>
                <?php if($portfolios[$i]->getMimeType() == 'application/msword' 
                        || $portfolios[$i]->getMimeType() == 'application/vnd.ms-works' 
                        || $portfolios[$i]->getMimeType()=='application/vnd.openxmlformats-officedocument.wordprocessingml.document') : ?>
                    <div class="img-profile">
                        <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $portfolios[$i]->getId()))?>" title="<?php echo $portfolios[$i]->getFilename() ?>">
                            <img src="<?php echo $view['assets']->getUrl('img/icon/icon_word.png');?>" 
                                 alt="<?php echo $portfolios[$i]->getFilename() ?>"
                                 title="<?php echo $portfolios[$i]->getFilename() ?>"/>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if($portfolios[$i]->getMimeType() == 'application/pdf' 
                        || $portfolios[$i]->getMimeType() == 'application/x-pdf') : ?>
                    <div class="img-profile">
                        <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $portfolios[$i]->getId()))?>" title="<?php echo $portfolios[$i]->getFilename() ?>">
                            <img src="<?php echo $view['assets']->getUrl('img/icon/icon_pdf.png');?>" 
                                 alt="<?php echo $portfolios[$i]->getFilename() ?>"
                                 title="<?php echo $portfolios[$i]->getFilename() ?>"/>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if($portfolios[$i]->getMimeType() == 'image/jpeg' 
                       || $portfolios[$i]->getMimeType() == 'image/png' 
                       || $portfolios[$i]->getMimeType() == 'image/gif'): ?>

                    <div class="img-profile">
                        <a rel="lightbox" href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $portfolios[$i]->getId()));//$portfolios[$i]->getUploadUrl().DS.$portfolios[$i]->getPath().DS.$portfolios[$i]->getStoredName() ?>" 
                           data-lightbox="lightbox-tq"
                           title="<?php echo $portfolios[$i]->getTitle(); ?> - <a target='_blank' href='<?php echo $portfolios[$i]->getUrl(); ?>'><?php echo $portfolios[$i]->getUrl(); ?></a>">
                            <?php $resize = ResizeImage::resize_image($portfolios[$i]->getFullStoredName(), '180x140', 180, 140, 1); ?>
                            <img src="<?php echo $resize ? $resize : ($view['assets']->getUrl('img/default02.png')) ; ?>" 
                                 alt="<?php echo $portfolios[$i]->getFilename() ?>" width="180px" height="140px;" 
                                 title="<?php echo $portfolios[$i]->getFilename() ?>"/>
                        </a>
                        <div><?php echo $portfolios[$i]->getTitle(); ?></div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.slide-portfolios').slick({
                    infinite: false,
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    responsive: [
                        {
                        breakpoint: 639,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            infinite: false
                        }
                    }
                    ]
                });
            });
        </script>
    <?php endif; ?>  
</div>