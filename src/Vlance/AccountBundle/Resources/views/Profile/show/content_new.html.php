<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\AccountBundle\Entity\Account; ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $is_owner = ($acc === $entity); // viewer in on his profile page ?>
<?php $portfolios = $entity->getPortfolios();?>

<?php /* <?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminAccount_edit',array('pk' => $entity->getId())) . '" target="_blank" class="btn">'
        . '<i class="icon-pencil"></i> '
        . $view['translator']->trans('profile.center.admin_edit_profile_ds', array(), 'vlance')
    .'</a>'
);?> */ ?>

<div class="row-fluid" itemscope itemtype="http://data-vocabulary.org/Person">
    <div class="col3-left span4 left-profile">
        <?php /*<div class="gumshoe"><a href="#"><?php echo '+ '.$view['translator']->trans('profile.menu_left.watch_list', array(), 'vlance') ?></a></div>*/ ?>
        <div class="avata">
                <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                <?php $real_avatar = ResizeImage::resize_image($entity->getFullPath(), '160x160', 160, 160, 1); ?>
                <?php if($real_avatar) : ?>
                    <?php $profile_avatar = $real_avatar;?>
                <?php endif;?>
                <?php if($is_owner): // Display edit link on his profile page?>
                    <div class="update-link tf300"><a class="tf300" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId(), 'referer' => 'redirect')) ?>"><?php echo $view['translator']->trans('profile.center.update_image', array(), 'vlance') ?></a></div>
                    <img itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $entity->getFullName(); ?>" title="<?php echo $entity->getFullName(); ?>" />
                <?php else:?>
                    <img itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $entity->getFullNameHidden(); ?>" title="<?php echo $entity->getFullNameHidden(); ?>" />
                <?php endif;?>
        </div>
        <div class="id_profile">
            <?php echo $view['translator']->trans('profile.menu_left.id_profile', array(), 'vlance').'. '.$entity->getId(); ?>
        </div>
        <div class="time-online-last">
            <?php echo $view['translator']->trans('profile.menu_left.last_time_online', array(), 'vlance').': ' ?><?php echo $entity->getLastLogin() ? $entity->getLastLogin()->format('d/m/Y') : '' ?>
        </div>
        <?php /* <div class="fb-like" style="margin-bottom: 5px;" data-href="<?php echo $abs_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
        <?php // Google Plus +1 button ?>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <div class="g-plusone" data-size="medium" style="margin-top:3px;"></div> */?>
    </div>
    <div class="col3-center span8 right-profile">
        <div class="row-fluid header-profile">
            <?php if(is_object($acc) && !$is_owner): // is logged in?>
                <?php if(count($acc->getJobsOpenForBid()) > 0): // has jobs open for bidding?>
                    <div id="invite-button" class="btn-group invite-project">
                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"
                           onclick="vtrack('Click job invite dropdown', {'location':'freelancer view', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})">
                            <?php echo $view['translator']->trans('profile.center.invite_to_bid_full', array(), 'vlance') ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach($acc->getJobsOpenForBid() as $jo):?>
                                <?php $checkJobInvite = FALSE;?>
                                <?php foreach ($job_invites as $jobInvite):?>
                                    <?php if(($jobInvite->getAccount()==$entity)&&($jobInvite->getJob()==$jo)): ?>
                                    <?php $checkJobInvite = TRUE;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                                <?php if($checkJobInvite){ ?>
                                    <li><a class="invite-action tf200" style="cursor: context-menu;" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>"><div class="overflow-text da-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right">Đã mời</span></a></li>
                                    <?php $checkJobInvite = FALSE;?>
                                    <?php }else{?>
                                    <li><a class="invite-action tf200" href="javascript:void(0)" ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($jo->getId(), $entity->getId(), true);?>" onclick="vtrack('Click job invite', {'location':'freelancer view', 'freelancer':'<?php echo $entity->getFullname()?>', 'freelancer_id':'<?php echo $entity->getId()?>'})"><div class="overflow-text chua-moi"><?php echo $jo->getTitle()?></div><span class="label label-info pull-right"></span></a></li>
                                <?php } ?>
                            <?php endforeach;?>
                        </ul>
                    </div>
                    <?php // invite freelancer join job ?>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            jQuery('#invite-button a.invite-action').click(function(){
                                that = $(this).parent();
                                $.ajax({
                                    'url': $(this).attr('ajax-href')
                                })
                                .done(function(res){
                                    that.find('div').removeClass("chua-moi");
                                    that.find('div').addClass("da-moi");
                                    that.find('span.label').html("Đã Mời");
                                    that.find('span.label').show();
                                    $('.navbar .upper-section .container #messages').remove();
                                    var alertClass = 'alert-error';
                                    if (res.error == 0) {
                                        alertClass = 'alert-success';
                                    }
                                    alertItem = '<div class="alert fade in ' + alertClass + '"><a class="close" data-dismiss="alert">×</a><div>' + res.errorMsg + '</div></div>';
                                    $("#messages .body-messages").html("");
                                    $("#messages .body-messages").append(alertItem);
                                    $("#messages").show();                    
                                    $(document).ready(function(){
                                        setTimeout( "$('#messages').hide();", 5000);
                                    });
                                })
                                .fail(function(){
                                })
                                .always(function(){

                                });
                                return;
                            });
                        });
                    </script>
                <?php endif;?>
            <?php endif;?>
            <div class="span12">
                <h1 style="float: left">
                    <span itemprop="name">
                        <?php if($is_owner):?>
                            <?php echo htmlentities($entity->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>
                        <?php else: ?>
                            <?php echo htmlentities($entity->getFullNameHidden(), ENT_SUBSTITUTE, "UTF-8") ?>
                        <?php endif; ?>
                        <?php // Certification icon ?>
                        <?php if ($entity->getIsCertificated()) : ?>
                        <img style="margin-top:-6px; width: 25px; height: 25px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                             title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if (!is_null($entity->getTelephoneVerifiedAt())): ?>
                        <img style="margin-top:-6px;width:25px; height:25px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                        <?php endif; ?>
                        <?php if (!is_null($entity->getPersonId())): ?>
                            <?php if (!is_null($entity->getPersonId()->getVerifyAt())): ?>
                            <img style="margin-top:-6px;width:25px; height:25px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php /*<img style="margin-top:-6px;width: 25px; height:25px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND"> 
                        <script type="text/javascript" language="javascript">
                            jQuery(document).ready(function(){
                                $(function () {
                                    $('[data-toggle="tooltip"]').tooltip()
                                })        
                            });
                        </script>*/ ?>

                        <?php if($entity->isLocked()): // Account is locked, show the notification to public?>
                            <span class="label label-important">Tài khoản đã bị khóa</span>
                        <?php endif;?>

                        <?php // User is in his profile page, display Edit links ?>
                        <?php if($is_owner): ?>
                            <span class="update-profile-link tf200">
                                <a class="btn btn-update info-fr" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId(), 'referer' => 'redirect')) ?>"><?php echo $view['translator']->trans('profile.center.update_info', array(), 'vlance') ?></a>
                            </span>
                        <?php endif;?>
                    </span>
                </h1>

                <?php if($entity->getTitle() == "" && $entity->getCity() ==""): '' ?>
                <?php else: ?>
                    <div class="clearfix"></div>
                    <div class="career">
                    <?php if($entity->getTitle()):?>
                        <span itemprop="title editable tf300">
                            <?php echo htmlentities($entity->getTitle(), ENT_SUBSTITUTE, "UTF-8");?>
                        </span>
                    <?php endif;?>
                    </div>
                    <div class="regional-price">
                        <i class="fa fa-map-marker"></i>
                        <span itemprop="address" itemscope itemtype="http://data-vocabulary.org/Address">
                            <span class="location editable tf300" itemprop="region"><?php echo $entity->getCity() ? $entity->getCity()->getName() : $view['translator']->trans('common.country', array(), 'vlance')  ?></span>
                        </span>
                        <?php // if($entity->getInCome()): ?>
                        <!--<span class="separator">|</span>-->
                        <!--<span class="price"><?php // echo $view['translator']->trans('common.price', array(), 'vlance').':'.$entity->getInCome().' '.$view['translator']->trans('profile.center.price_h', array(), 'vlance') ?></span>-->
                        <?php // endif; ?>
                    </div>
                    
                    <?php /* Kỹ năng */ ?>
                    <?php $skills = $entity->getSkills() ?>
                    <?php if(count($skills) > 0): ?>
                        <?php $counter = 0; ?>
                        <div class="row-fluid">
                            <div class="skills">
                                <?php foreach ($skills as $skill): ?>
                                <?php $counter++; ?>
                                <a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                    title="<?php print $skill->getTitle(); ?>"> <div><?php print $skill->getTitle() ?></div></a>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>      
        </div>
    </div>
    <div class="tab-content profile-content">
        <?php /* Tổng quan */ ?>
        <div class="active tab-pane" id="tong-quan">
            <?php if($entity->getDescription()): ?>
            <div class="row-fluid overview">
                <h2>
                    <?php echo $view['translator']->trans('profile.center.qverview', array(), 'vlance') ?>
                </h2>
                <div class="body">
                    <?php echo $entity->getDescription() ? nl2br(htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")): '' ?>
                </div>
            </div>
            <?php endif; ?>
            
            <div class="service-freelancer">
                <h2>Dịch vụ
                    <span class="label-tag label-new">Mới</span>
                </h2>
                <?php $services = $entity->getServices(); ?>
                <?php if ($is_owner): // viewer in on his profile page ?>
                    <?php if (count($services) > 0): ?>
                        <?php foreach($services as $s): ?>
                            <div class="services-provided">
                                <a><i class="fa fa-check"></i>
                                    <?php echo $s->getTitle(); ?>                
                                </a>
                            </div>
                        <?php endforeach; ?>
                        <div class="update-service">
                            <a class="btn btn-large btn-service" href="<?php echo $view['router']->generate('account_edit',array('id' => $entity->getId())) ?>">Thêm dịch vụ</a>
                        </div>    
                    <?php else: ?>
                        <div class="update-service">
                            <p>Bạn có thể cung cấp những dịch vụ gì</p>
                            <p>Hãy thêm vào hồ sơ để khách hàng có thể thuê bạn ngay</p>
                            <a class="btn btn-large btn-service" href="<?php echo $view['router']->generate('account_edit',array('id' => $entity->getId())) ?>">Thêm dịch vụ</a>
                        </div>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if (count($services) > 0): ?>    
                        <p class="">Danh sách các dịch vụ được freelancer cung cấp:</p>
                        <?php /* @var $entity Account */ ?>
                        <?php foreach($services as $s): ?>
                            <div class="services-provided">
                                <a><i class="fa fa-check"></i>
                                    <?php echo $s->getTitle(); ?>                
                                </a>
                            </div>
                        <?php endforeach; ?> 
                    <?php else: ?>
                        <p>Hiện tại freelancer chưa cung cấp dịch vụ nào.</p>
                    <?php endif; ?>    
                <?php endif; ?>    
            </div>

            <?php /* echo $view->render('VlanceAccountBundle:Profile/show/content:jobs_working.html.php', array('entity' => $entity, 'jobs_working' => $jobs_working, 'acc' => $acc)) */ ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/show/content_new:jobs_worked.html.php', array('entity' => $entity, 'jobs_worked' => $jobs_worked, 'acc' => $acc)) ?>

            <?php /* Hồ sơ việc làm */ ?>
            <?php echo $view->render('VlanceAccountBundle:Profile/show/content_new:portfolios.html.php', array('entity' => $entity, 'acc' => $acc)) ?>

        </div>
    </div>
<script type="text/javascript">
    function scrollWindow(select){
        window.scrollTo(0,100);
        $(select).click();
    }
    $(document).ready(function() {        
        $('.num_star_rating').popover();
        
        $('#tabs-profile a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        
        $(".comment-three .more" ).click(function() {
            $('.comment-fd-' + jQuery(this).attr("data-id")).toggleClass( "hidebody" );
//            document.getElementById("#more-comment-fd-" + jQuery(this).attr("data-id")).innerText ="Rút gọn";
        });
        
        $(".comment-list .more" ).click(function() {
            $('.comment-listfd-' + jQuery(this).attr("data-id")).toggleClass( "hidebody" );
//            document.getElementById("#more-listfd-" + jQuery(this).attr("data-id")).innerText ="Rút gọn";
          });
        
    });
</script>
</div>
