<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Xác thực số điện thoại của %username%', array('%username%' =>  $auth_user->getFullName())));?>
<?php $view['slots']->start('content')?>

<div class="profile-tab">
    <ul class="nav nav-tabs">
        <li><a href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $auth_user->getId())) ?>">Thông tin cá nhân</a></li>
        <li><a href="<?php echo $view['router']->generate('account_edit',array('id' => $auth_user->getId())) ?>"><?php echo $view['translator']->trans('profile.edit_profile.work_profile', array(), 'vlance') ?></a></li>
        <li><a href="<?php echo $view['router']->generate('account_portfolio_edit',array('id' => $auth_user->getId())) ?>"><?php echo $view['translator']->trans('profile.edit_profile.ability_profile', array(), 'vlance') ?></a></li>
        <li class="active"><a><?php echo $view['translator']->trans('profile.edit_verify_information.tab_title', array(), 'vlance') ?></a></li>
    </ul>
</div> 
<div>
    <div class="title-verify-account">
        <h3>Xác thực ID tài khoản</h3>
        <p>Xác thực đầy đủ thông tin cá nhân là cách nhanh nhất giúp bạn nhận được sự tin tưởng từ khách hàng, freelancer và làm việc an toàn, hiệu quả.</p>
    </div>
    <div class="verify-account">
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle <?php if(!is_null($auth_user->getTelephoneVerifiedAt())) echo "verified-icon"; ?>" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                      <i class="fa fa-check-circle-o" aria-hidden="true"></i> 1. Xác thực số điện thoại
                    </a>
                </div>
                <div id="collapseOne" class="accordion-body collapse <?php if(is_null($auth_user->getTelephoneVerifiedAt())) echo "in"; ?>">
                    <div class="accordion-inner">
                        <div>
                            <?php echo $view->render('VlanceAccountBundle:Profile/edit_verify_information:verify_telephone.html.php', array('auth_user' => $auth_user, 'referer' => $referer)) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle <?php if($auth_user->getPersonId()){if(!is_null($auth_user->getPersonId()->getVerifyAt())) echo "verified-icon";} ?>" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                        <i class="fa fa-check-circle-o" aria-hidden="true"></i> 2. Xác thực chứng minh nhân dân (<b>Không bắt buộc</b>)
                    </a>
                </div>
                <div id="collapseTwo" class="accordion-body collapse <?php if(!is_null($auth_user->getTelephoneVerifiedAt()) && $auth_user->getIsVerifiedPersonId() == false) echo "in";?>">
                    <div class="accordion-inner">
                        <?php echo $view['actions']->render($view['router']->generate('verify_id',array())); ?>
                    </div>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle <?php if($auth_user->getTaxId()){if($auth_user->getTaxId()->getVerifiedAt() != NULL) echo "verified-icon";} ?>" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                        <i class="fa fa-check-circle-o" aria-hidden="true"></i> 3. Xác thực mã số thuế
                    </a>
                </div>
                <div id="collapseThree" class="accordion-body collapse <?php if(!is_null($auth_user->getTelephoneVerifiedAt()) && $auth_user->getIsVerifiedPersonId() == true && $auth_user->getIsVerifiedTaxId() == false) echo "in";?>">
                    <div class="accordion-inner">
                        <?php echo $view['actions']->render($view['router']->generate('verify_tax_id',array())); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
<?php $view['slots']->stop();?>