<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Sửa hồ sơ công việc của %username%', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->start('content')?>

<div id="edit-acc" class="edit-form">
    <div class="profile-tab">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $entity->getId())) ?>">Thông tin cá nhân</a></li>
            <li><a href="<?php echo $view['router']->generate('account_edit',array('id' => $entity->getId())) ?>"></i> Hồ sơ làm việc</a></li>
            <li class="active"><a>Hồ sơ năng lực</a></li>
            <li><a href="<?php echo $view['router']->generate('account_verify_information',array()) ?>"><?php echo $view['translator']->trans('profile.edit_verify_information.tab_title', array(), 'vlance') ?></a></li>
        </ul>
    </div> 
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.center.job_profile', array(), 'vlance') ?></label></div>
    <div class="row-fluid portfolios">
        <?php echo $view['actions']->render($view['router']->generate('portfolio_list',array('aid' => $entity->getId()))) ?>
    </div>

    <?php if($acc === $entity) : ?>
        <div><label class="personal-info"><?php echo $view['translator']->trans('profile.center.new', array(), 'vlance'); ?></label></div>
        <?php // popup add ho so viec lam ?>
        <?php echo $view['actions']->render($view['router']->generate('portfolio_add',array())); ?>
    <?php endif; ?>
</div> 
<?php $view['slots']->stop();?>