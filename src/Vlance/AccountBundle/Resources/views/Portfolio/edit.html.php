<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Sửa hồ sơ năng lực'));?>
<?php $view['slots']->start('content')?>

<div id="edit-acc" class="edit-form">
    <div class="profile-tab">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $acc->getId())) ?>">Thông tin cá nhân</a></li>
            <li><a href="<?php echo $view['router']->generate('account_edit',array('id' => $acc->getId())) ?>">Hồ sơ làm việc</a></li>
            <li class="active"><a>Hồ sơ năng lực</a></li>
            <li><a href="<?php echo $view['router']->generate('account_verify_information',array()) ?>"><?php echo $view['translator']->trans('profile.edit_verify_information.tab_title', array(), 'vlance') ?></a></li>
        </ul>
    </div> 
    <div><label class="personal-info"><?php echo $view['translator']->trans('profile.center.job_profile', array(), 'vlance') ?></label></div>
    
    <h2><?php echo $view['translator']->trans('profile.edit_portfolio.edit_portfolio', array(), 'vlance') ?></h2>
    <form id="form-update-portfolio" action="<?php echo $view['router']->generate('portfolio_update',array('id' => $entity->getId())) ?>" method="post" <?php echo $view['form']->enctype($edit_form)?>>
        <dl class="dl-horizontal">
            <dt></dt>
            <dd>
                <?php 
                    if($entity->getMimeType() == 'application/msword' 
                            || $entity->getMimeType() == 'application/vnd.ms-works' 
                            || $entity->getMimeType()=='application/vnd.openxmlformats-officedocument.wordprocessingml.document') { ?>  
                            <div class="img-profile span4">
                                <img src ="<?php echo $view['assets']->getUrl('img/icon/icon_word.png');?> " alt="<?php echo $entity->getFilename() ?>" title="<?php echo $entity->getFilename() ?>" />
                            </div> 
                   <?php  }

                    if($entity->getMimeType() == 'application/pdf' || $entity->getMimeType() == 'application/x-pdf') { ?>
                        <div class="img-profile span4">
                            <img src ="<?php echo $view['assets']->getUrl('img/icon/icon_pdf.png'); ?>" alt="<?php echo $entity->getFilename();?>" title="<?php echo $entity->getFilename() ?>" />
                        </div>
                    <?php  }

                    if($entity->getMimeType() == 'image/jpeg' 
                            || $entity->getMimeType() == 'image/png' 
                            || $entity->getMimeType() == 'image/gif') { ?>
                        <div class="img-profile span4">                   
                            <a href="<?php echo $entity->getUploadUrl().DS.$entity->getPath().DS.$entity->getStoredName()?>"  
                               rel="lightbox">
                                <img src="<?php echo $entity->getUploadUrl().DS.$entity->getPath().DS.$entity->getStoredName()?>" alt="<?php echo $entity->getFilename() ?>" title="<?php echo $entity->getFilename() ?>" />
                            </a>

                        </div>
                    <?php } ?>
                 <div class="clear"></div>
            </dd>

            <dt></dt>
            <dd>
                <?php echo $view['form']->errors($edit_form['file']) ?>
                <?php echo $view['form']->widget($edit_form['file']) ?>
                <div class="subtitle"><?php echo $view['translator']->trans('profile.center.image_explain', array(), 'vlance') ?></div>
            </dd>
            <div class="clear"></div>
            
            <dt><?php echo $view['translator']->trans('profile.center.title_newfile', array(), 'vlance') ?></dt>
            <dd>
                <div>
                    <?php echo $view['form']->errors($edit_form['title']); ?>
                    <?php echo $view['form']->widget($edit_form['title']); ?>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.center.title_newfile_explain', array(), 'vlance') ?></div>
                </div>
            </dd>
            <div class="clear"></div>
            
            <dt>URL</dt>
            <dd>
                <div>
                    <?php echo $view['form']->errors($edit_form['url']); ?>
                    <?php echo $view['form']->widget($edit_form['url']); ?>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.center.link_explain', array(), 'vlance') ?></div>
                </div>
            </dd>
            <div class="clear"></div>

            <dt>
                <?php echo $view['translator']->trans('profile.center.describe_job', array(), 'vlance') ?>
            </dt>
            <dd>
                <div>
                    <?php echo $view['form']->errors($edit_form['description']); ?>
                    <?php echo $view['form']->widget($edit_form['description']); ?>
                    <div class="subtitle"><?php echo $view['translator']->trans('profile.center.describe_job_explain', array(), 'vlance') ?></div>
                </div>
            </dd>
            <div class="clear"></div>
            
            <dt><?php echo $view['translator']->trans('profile.edit_service.label_input_service_portfolio', array(), 'vlance') ?></dt>
            <dd>
                <div class="services-update">
                    <div class="form-add-service">
                        <div class="inner-service">
                            <input type="text" data-trigger="hover" data-content="Liệt kê các dịch vụ bạn có thể cung cấp cho khách hàng." data-placement="right" data-toggle="popover" placeholder="Tên dịch vụ (VD: Thiết kế banner facebook,...)" data-provide="typeahead" name="hiddenTagListB" placeholder="Tags" class="tm-input-cat"/>
                        </div>
                    </div>
                </div>
            </dd>
            <div class="clear"></div>
            
            <dt></dt>
            <dd>
                <div class="button">
                    <?php echo $view['form']->row($edit_form['_token'])?>
                    <input type="submit" class="btn btn-primary submit" value="<?php echo $view['translator']->trans('profile.edit_portfolio.update_portfolio', array(), 'vlance') ?>"/>
                    <input type="submit" onclick="window.history.back();" class="btn" value="<?php echo $view['translator']->trans('common.back', array(), 'vlance') ?>"/>
                    <div class="delete">
                        <a href="#popup-delete" data-toggle="modal" role="button">
                            <?php echo $view['translator']->trans('profile.edit_portfolio.delete', array(), 'vlance') ?>
                        </a>
                    </div>
                </div>
                <div class="modal hide fade" id="popup-delete">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                      <h3><?php echo $view['translator']->trans('profile.edit_portfolio.title_popup', array(), 'vlance') ?> <?php echo $entity->getTitle(); ?></h3>
                    </div>
                    <div class="modal-body">
                      <p><?php echo $view['translator']->trans('profile.edit_portfolio.text_delete', array(), 'vlance').'?' ?></p>
                    </div>
                    <div class="modal-footer">
                      <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">
                          <?php echo $view['translator']->trans('common.back', array(), 'vlance') ?>
                      </a>
                      <a href="<?php echo $view['router']->generate('portfolio_delete',array('id' => $entity->getId())) ?>" 
                         class="btn btn-primary">
                             <?php echo $view['translator']->trans('profile.edit_portfolio.delete', array(), 'vlance') ?>
                      </a>
                    </div>
                </div>
            </dd>
        </dl>
    </form>
    
</div> 
    <?php /*
    <script>
        function myFunction() { 
            $.get('<?php //echo $view['router']->generate('portfolio_delete_file',array('id' => $entity->getId())) ?>',{'id': <?php //echo $entity->getId() ?>}).done(function(data){alert('Xoa thanh cong')});
    }
            
           
//        $('.delete-file').post(
//          '<?php //echo $view['router']->generate('portfolio_delete_file',array('id' => $entity->getId())) ?>',
//           {'id': <?php //echo $entity->getId() ?>}
//        ).done(function(data){alert('Xoa thanh cong')});
    </script>
    */ ?>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#form-update-portfolio .popovers-input').popover();
            $("#form-update-portfolio").validate({
                        rules: {
//                            "vlance_accountbundle_portfoliotype[file]": "required",
                            "vlance_accountbundle_portfoliotype[title]": "required",
//                            "vlance_accountbundle_portfoliotype[url]": "required",
                            "vlance_accountbundle_portfoliotype[description]": "required"
                        }
                    });
            $("#form-update-portfolio .button .submit").click(function() {
                var ext = $('#vlance_accountbundle_portfoliotype_file').val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg','doc','docx','pdf']) == -1) {
                    if(ext !== ''){
                        $('#form-update-portfolio .file_upload_portfolio label.error').remove();
                        $('#form-update-portfolio .file_upload_portfolio').append('<label for="vlance_accountbundle_portfoliotype_file" class="error" style="display: block !important"><?php echo $view['translator']->trans('profile.create_portfolio.error_file', array(), 'vlance').'.' ?></label>');
                        $('#form-update-portfolio .file_upload_portfolio input').focus();
                        return false;
                    }
                    $('#form-update-portfolio .file_upload_portfolio label.error').remove();
                    if($("#form-update-portfolio").valid()){
                        $("#form-update-portfolio .button .submit").submit();
                    }
                }
             })

           });
    </script>
    
    <?php 
        $js_cat = array();
        $cats = $entity->getServices();
        foreach($cats as $c) {
            $js_cat[] = array($c->getTitle(),$c->getId());
        }
    ?>
    
    <script type="text/javascript">
        var cat_source = <?php echo $view['actions']->render($view['router']->generate('service_list'))  ?>;
        var prefilledServices = <?php echo json_encode($js_cat) ?>;
    </script>
<?php $view['slots']->stop();?> 	 	