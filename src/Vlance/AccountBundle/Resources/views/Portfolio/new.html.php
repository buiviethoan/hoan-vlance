
    <form id="form-create-portfolio" action="<?php echo $view['router']->generate('portfolio_create',array('aid' => $aid)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->widget($form) ?>
        <p>
            <input type="submit" class="btn btn-primary button" value="<?php echo $view['translator']->trans('profile.create_portfolio.save', array(), 'vlance') ?>">
        </p>
    </form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#form-create-portfolio .popovers-input').popover();
        
        jQuery.validator.addMethod("filesize", function (val, element) {
            var size = element.files[0].size;
            if (size > 6291456)// checks the file more than 6 MB
            {
                return false;
            } else {
                return true;
            }
        }, "Kích thước file phải nhỏ hơn 6MB");
      
        $("#form-create-portfolio").validate({
            rules: {
                "vlance_accountbundle_portfoliotype[file]": {
                    "required": true,
                    "filesize": true
                },
                "vlance_accountbundle_portfoliotype[title]": "required",
                "vlance_accountbundle_portfoliotype[description]": "required"
            }
        });
        $("#form-create-portfolio .button").click(function() {
            var ext = $('#vlance_accountbundle_portfoliotype_file').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['gif','png','jpg','jpeg','doc','docx','pdf']) == -1) {
                if(ext == ''){
                    $('#form-create-portfolio .file_upload label.error').remove();
                    $('#form-create-portfolio .file_upload').append('<label for="vlance_accountbundle_portfoliotype_file" class="error" style="display: block !important"><?php echo $view['translator']->trans('profile.create_portfolio.error_file_empty', array(), 'vlance').'.' ?></label>');
                    $('#form-create-portfolio .file_upload input').focus();  
                    return false;
                }
                $('#form-create-portfolio .file_upload label.error').remove();
                $('#form-create-portfolio .file_upload').append('<label for="vlance_accountbundle_portfoliotype_file" class="error" style="display: block !important"><?php echo $view['translator']->trans('profile.create_portfolio.error_file', array(), 'vlance').'.' ?></label>');
                $('#form-create-portfolio .file_upload input').focus();
                return false;
            }else{
                $('#form-create-portfolio .file_upload label.error').remove();
                if($("#form-create-portfolio").valid()){
                    $("#form-create-portfolio .button").submit();
                }else{
                    return false;
                }
            }
            
         })
         
       });
</script>

