<?php if(empty($entities)) : ?>
    <div><?php echo $view['translator']->trans('profile.center.not_job_profile', array(), 'vlance') ?></div>
<?php else : ?>
    <?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
    <?php foreach($entities as $entity) : ?>
        <?php if($entity->getMimeType() == 'application/msword' 
                || $entity->getMimeType() == 'application/vnd.ms-works' 
                || $entity->getMimeType()=='application/vnd.openxmlformats-officedocument.wordprocessingml.document') : ?>
            <div class="img-profile">
                <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $entity->getId()))?>" title="<?php echo $entity->getFilename() ?>">
                    <img src="<?php echo $view['assets']->getUrl('img/icon/icon_word.png');?>" 
                         alt="<?php echo $entity->getFilename() ?>"
                         title="<?php echo $entity->getFilename() ?>"/>
                </a>
            </div>
        <?php endif; ?>

        <?php if($entity->getMimeType() == 'application/pdf' || $entity->getMimeType() == 'application/x-pdf'):?>
            <div class="img-profile">
                <a href="<?php echo $view['router']->generate('download_portfolio',array('portfolio_id' => $entity->getId()))?>" title="<?php echo $entity->getFilename() ?>">
                    <img src="<?php echo $view['assets']->getUrl('img/icon/icon_pdf.png');?>" 
                         alt="<?php echo $entity->getFilename() ?>"
                         title="<?php echo $entity->getFilename() ?>"/>
                </a>
            </div>
        <?php endif; ?>

        <?php if($entity->getMimeType() == 'image/jpeg' 
                || $entity->getMimeType() == 'image/png' 
                || $entity->getMimeType() == 'image/gif') : ?>
            <div class="img-profile">
                <?php $path = $view['router']->generate('download_portfolio',array('portfolio_id'=>$entity->getId())); ?>
                <a href="<?php echo $path; ?>"  
                   data-lightbox="lightbox-tq"
                   title="<?php echo $entity->getTitle(); ?> - <a target='_blank' href='<?php echo $entity->getUrl(); ?>'><?php echo $entity->getUrl(); ?></a>">
                    <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($entity->getFullStoredName(), '140x140', 140, 140, 1); ?>

                    <img src="<?php echo $resize ? $resize : ($view['assets']->getUrl('img/default.jpg')); ?>" 
                         alt="<?php echo $entity->getFilename() ?>" width="140px" height="140px"
                         title="<?php echo $entity->getFilename() ?>"/>
                </a>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>