<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('reset_password.page_title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>
 <?php //Login page ?>
<div class="login-section container">
    <div class="span6 login-box">
        <h1><?php echo $view['translator']->trans('reset_password.header', array(), 'vlance')?></h1>
        <p>
            <?php echo $view['translator']->trans('reset_password.enter_email', array(), 'vlance')?>
        </p>
        <form action="<?php echo $view['router']->generate('fos_user_resetting_send_email') ?>" method="post">
            <?php if(isset($invalid_username)):?>
                <p class="alert"><?php echo $view['translator']->trans('reset_password.invalid_email', array('%username%' => $invalid_username), 'vlance') ?></p>
            <?php endif;?>
            <input type="email" id="username" name="username" class="span4" required="required" placeholder="<?php echo $view['translator']->trans('reset_password.email', array(), 'vlance')?>"/>
            <p>
                <button type="submit" class="btn btn-large btn-primary"><?php echo $view['translator']->trans('reset_password.submit', array(), 'vlance')?></button>
            </p>
        </form>
    </div>
</div>
<?php $view['slots']->stop();?>