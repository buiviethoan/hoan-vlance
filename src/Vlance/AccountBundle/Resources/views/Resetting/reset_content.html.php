<form action="<?php echo $view['router']->generate('fos_user_resetting_reset', array('token' => $token)); ?>"
      <?php echo $view['form']->enctype($form)?> method="POST" class="fos_user_resetting_reset">
    <?php echo $view['form']->widget($form)?>
    <div>
        <p>
            <input type="submit" class="btn btn-large btn-primary" value="<?php echo $view['translator']->trans('reset_password.reset.submit', array(), 'vlance'); ?>" />
        </p>
    </div>
</form>