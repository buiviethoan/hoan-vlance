<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('reset_password.page_title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>
<div class="confirm-section">
    <div class="login-section container">
        <div class="login-box">
            <div class="jumbotext">
                <h1 class="lead"><?php echo $view['translator']->trans('reset_password.success_title', array(), 'vlance')?></h1>
                <p><?php echo $view['translator']->trans('reset_password.check_email', array('%email%' => $email), 'vlance')?></p>
            </div>
            <div class="row-fluid">
                 <div class="confirm-image">
                </div>
            </div>
        </div>
    </div>
</div>
<?php $view['slots']->stop();?>