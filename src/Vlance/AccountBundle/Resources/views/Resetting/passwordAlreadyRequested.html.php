<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('reset_password.page_title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>
<div class="login-section container">
    <div class="login-box">
        <h1><?php echo $view['translator']->trans('reset_password.already_requested.title', array(), 'vlance')?></h1>
        <p>
            <?php echo $view['translator']->trans('reset_password.already_requested.text', array(), 'vlance')?>
        </p>
        <p>
            <?php echo $view['translator']->trans('reset_password.already_requested.return_login', array('%trang-login%' => $view['router']->generate('fos_user_security_login')), 'vlance')?>
        </p>
    </div>
</div>
<?php $view['slots']->stop();?>