<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('reset_password.reset.title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>
<div class="login-section container">
    <div class="span6 login-box">
        <h1><?php echo $view['translator']->trans('reset_password.reset.title', array(), 'vlance');?></h1>
        <p>
            <?php echo $view['translator']->trans('reset_password.reset.description', array(), 'vlance')?>
        </p>
        <?php echo $view->render('VlanceAccountBundle:Resetting:reset_content.html.php', array('form' => $form, 'token' => $token)) ?>
    </div>
</div>
<?php $view['slots']->stop();?>