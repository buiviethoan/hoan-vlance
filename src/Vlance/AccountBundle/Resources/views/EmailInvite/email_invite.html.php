<?php

use Vlance\AccountBundle\Entity\EmailInvite
?>
<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('forminvite.title_page', array(), 'vlance')); ?>
<?php
$view['slots']->start('content')
/* @var $account \Vlance\AccountBundle\Entity\Account */
?>
<h1><?php echo $view['translator']->trans('list_emailinvite.title', array(), 'vlance') ?></h1>
<div class="hero-unit text-warning note-email-invited">
    <div>
        <?php echo $view['translator']->trans('list_emailinvite.guide_p1', array(), 'vlance') ?>
    </div>
    <div>
        <?php echo $view['translator']->trans('list_emailinvite.guide_p2', array(), 'vlance') ?>
    </div>
    <div>
        <?php echo $view['translator']->trans('list_emailinvite.guide_p3', array(), 'vlance') ?>
    </div>
</div>
<?php if (count($listemails) > 0):?>
<div class="row-fluid mail-invite">
    <a href="javascript:void(0)" class="startus_icon"><?php echo $view['translator']->trans('profile.invite_email.status_icon',array(), 'vlance') ?></a>
    <div class="block-hidden">
        <div class="span12" style="position: relative;">
            <div class="startus_icon_show info-iconmail">
                <div class="left startus-job info-iconmail-invited">
                    <h3><?php echo $view['translator']->trans('profile.invite_email.info',array(), 'vlance') ?></h3>
                    <ul>
                        <li><i class="newmail"></i><?php echo $view['translator']->trans('profile.invite_email.new_email',array(), 'vlance') ?></li>
                        <li><i class="sentmail"></i><?php echo $view['translator']->trans('profile.invite_email.sent_email',array(), 'vlance') ?></li>
                        <li><i class="failedmail"></i><?php echo $view['translator']->trans('profile.invite_email.failed_email',array(), 'vlance') ?></li>
                    </ul>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".startus_icon").click(function(e) {
                        $(".startus_icon_show").toggle();
                        e.stopPropagation();
                    });
                    $('body').click(function() {
                        $('.startus_icon_show').hide();
                    });
                    $('.startus_icon_show').click(function(e) {
                        e.stopPropagation();
                    });
                });
            </script>       
        </div>
    </div>
</div>
<?php endif;?>
<?php foreach ($listemails as $email) : ?>
    <div class="span4 type-email-invited">
        <?php if ($email->getVerified() == EmailInvite::EMAIL_INVITE_NEW): ?>
            <div class="new-email-invite into"></div>
        <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_SENT): ?>
            <div class="sent-email-invite into"></div>    
        <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_ERROR): ?>
            <div class="error-email-invite into" ></div>   
        <?php endif; ?>
            <div class="emai-invited"><?php print_r($email->getEmail()); ?></div>   
    </div>
    <?php ?>
<?php endforeach; ?>
<div class="form-invite-email">        
    <?php echo $view['actions']->render($view['router']->generate('emailinvite_new', array('accid' => $account->getId()))); ?>
</div>
<?php if(count($emails) > 0): ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $("#popup_gamil_contact").modal('show');
        });
    </script>

<div id="popup_gamil_contact" class="modal hide fade popup_share_main" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <b id="myModalLabel">Mời bạn bè</b>
  </div>
  <div class="modal-body">
    <div class="text-des">Bạn có đồng ý gửi thư mời tới tất cả bạn bè trên gmail không ?</div>
    <div class="submit">
        <a class="btn btn-primary btn-ok" href="<?php echo $view['router']->generate('invite_google', array('emails' => implode(',', $emails)))?>">Đồng ý</a>
        <a class="btn btn-close" data-dismiss="modal" aria-hidden="true">Không đồng ý</a>
    </div>
    
  </div>
</div>
<?php endif; ?>
<?php $view['slots']->stop(); ?>