<form id="form-invite-email" 
    action="<?php echo $view['router']->generate('email_invite_create',array('accid' => $accid)) ?>" 
    method="post" <?php echo $view['form']->enctype($form)?>>
    <script type="text/javascript">
        function google_email() {
           window.location.href = "https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $view['vlance']->getParameter('vlance_system.google.client_id') ?>&redirect_uri=<?php echo $view['router']->generate('login_google', array(), true) ?>&scope=https://www.google.com/m8/feeds/&response_type=code";
       }
     </script>
    <div class="network-socail-gmail">
        <?php $session = $app->getSession(); ?>
        <?php $session->set('service', 'contact') ?>
        <a class="btn btn-primary btn-large button-gmail" 
           onclick="vtrack('Invite friends', {'type':'Gmail'})"
           href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $view['vlance']->getParameter('vlance_system.google.client_id') ?>&redirect_uri=<?php echo $view['router']->generate('login_google', array(), true) ?>&scope=https://www.google.com/m8/feeds/&response_type=code"><?php echo $view['translator']->trans('list_emailinvite.invite_email', array(), 'vlance') ?></a>
    </div>
     <div class="text-gmail"><?php echo $view['translator']->trans('list_emailinvite.send_email', array(), 'vlance') ?></div>
  <?php echo $view['form']->widget($form) ?>
  <p>
      <input type="submit" class="btn btn-primary button" 
             onclick="vtrack('Invite friends', {'type':'Email'})"
             value="<?php echo $view['translator']->trans('forminvite.button', array(), 'vlance') ?>">
  </p>
</form>