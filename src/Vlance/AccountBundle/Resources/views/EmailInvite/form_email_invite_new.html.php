<form id="form-invite-email" 
    action="<?php echo $view['router']->generate('email_invite_create',array('accid' => $accid)) ?>" 
    method="post" <?php echo $view['form']->enctype($form)?>>
    <script type="text/javascript">
        function google_email() {
           window.location.href = "https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $view['vlance']->getParameter('vlance_system.google.client_id') ?>&redirect_uri=<?php echo $view['router']->generate('login_google', array(), true) ?>&scope=https://www.google.com/m8/feeds/&response_type=code";
       }
    </script>
    <div class="form-input span6" style="margin-left:0px;">
        <p>Gửi email tới từng bạn bè</p>
        <?php echo $view['form']->widget($form) ?> 
        <div>
            <input type="submit" class="btn btn-primary btn-large btn-block button-invite" 
                 value="<?php echo $view['translator']->trans('forminvite.button', array(), 'vlance') ?>">
        </div>
    </div> 
    <div class="network-socail-gmail span6">
        <p class="text-choice">Hoặc</p>
        <?php $session = $app->getSession(); ?>
        <?php $session->set('service', 'contact') ?>
        <a class="btn btn-primary btn-large button-gmail" 
           href="https://accounts.google.com/o/oauth2/auth?client_id=<?php echo $view['vlance']->getParameter('vlance_system.google.client_id') ?>&redirect_uri=<?php echo $view['router']->generate('login_google', array(), true) ?>&scope=https://www.google.com/m8/feeds/&response_type=code"><?php echo $view['translator']->trans('list_emailinvite.invite_email', array(), 'vlance') ?></a>
        <p class="text-note">Mời tất cả bạn bè với chỉ một nút bấm</p>
    </div>
</form>