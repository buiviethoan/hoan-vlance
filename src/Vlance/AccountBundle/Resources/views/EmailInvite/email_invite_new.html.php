<?php

use Vlance\AccountBundle\Entity\EmailInvite
?>
<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('forminvite.title_page', array(), 'vlance')); ?>
<?php
$view['slots']->start('content')
/* @var $account \Vlance\AccountBundle\Entity\Account */
?>
<div class="invite-friend">
    <div class="container">
        <h2>Nhận tới<span> 30 Credit</span>* khi giới thiệu thành công một người bạn</h2>
        <img src="/img/invite_friend.png">
        <?php /* if (count($listemails) > 0):?>
        <div class="row-fluid mail-invite">
            <a href="javascript:void(0)" class="startus_icon"><?php echo $view['translator']->trans('profile.invite_email.status_icon',array(), 'vlance') ?></a>
            <div class="block-hidden">
                <div class="span12" style="position: relative;">
                    <div class="startus_icon_show info-iconmail">
                        <div class="left startus-job info-iconmail-invited">
                            <h3><?php echo $view['translator']->trans('profile.invite_email.info',array(), 'vlance') ?></h3>
                            <ul>
                                <li><i class="newmail"></i><?php echo $view['translator']->trans('profile.invite_email.new_email',array(), 'vlance') ?></li>
                                <li><i class="sentmail"></i><?php echo $view['translator']->trans('profile.invite_email.sent_email',array(), 'vlance') ?></li>
                                <li><i class="failedmail"></i><?php echo $view['translator']->trans('profile.invite_email.failed_email',array(), 'vlance') ?></li>
                            </ul>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $(".startus_icon").click(function(e) {
                                $(".startus_icon_show").toggle();
                                e.stopPropagation();
                            });
                            $('body').click(function() {
                                $('.startus_icon_show').hide();
                            });
                            $('.startus_icon_show').click(function(e) {
                                e.stopPropagation();
                            });
                        });
                    </script>       
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php foreach ($listemails as $email) : ?>
            <div class="span4 type-email-invited">
                <?php if ($email->getVerified() == EmailInvite::EMAIL_INVITE_NEW): ?>
                    <div class="new-email-invite into"></div>
                <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_SENT): ?>
                    <div class="sent-email-invite into"></div>    
                <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_ERROR): ?>
                    <div class="error-email-invite into" ></div>   
                <?php endif; ?>
                    <div class="emai-invited"><?php print_r($email->getEmail()); ?></div>   
            </div>
            <?php ?>
        <?php endforeach; */?>
        <div class="form-invite-email row-fluid">        
            <?php echo $view['actions']->render($view['router']->generate('emailinvite_new', array('accid' => $account->getId()))); ?>
        </div>
        <?php if(count($emails) > 0): ?>
            <div id="popup_gamil_contact" class="modal hide fade popup_share_main" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <b id="myModalLabel">Mời bạn bè</b>
                </div>
                <div class="modal-body">
                    <div class="text-des">Bạn có đồng ý gửi thư mời tới <?php echo count($emails); ?> bạn bè trên gmail không ?</div>
                    <div class="submit">
                        <a id="confirm-send-invites" class="btn btn-primary btn-ok">Đồng ý</a>
                        <a class="btn btn-close" data-dismiss="modal" aria-hidden="true">Không đồng ý</a>
                    </div>
                    <script type="text/javascript">
                        var url = "<?php echo $view['router']->generate('invite_google', array()); ?>";
                        var emails = '<?php echo json_encode($emails); ?>';
                        var quantity = '<?php echo count($emails); ?>';
                    </script>
                </div>
            </div>
            <div id="popup_notification" class="modal hide fade popup_share_main" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                    <b id="myModalLabel">Mời bạn bè</b>
                </div>
                <div class="modal-body">
                    <div class="text-des">Email bạn bè đang được gửi.</div>
                </div>
                <div class="button">
                    <a class="btn btn-close" data-dismiss="modal" aria-hidden="true">Đóng</a>
                </div>
            </div>
        <?php endif; ?>
        <div class="social-invite-mail">
            <p>Hoặc chia sẻ để mời được nhiều bạn bè hơn</p>   
            <div class="input-prepend">
                <button type="button" class="add-on" id="copyToClipboard"><i class="fa fa-link"></i> Sao chép</button>
                <input class="span2" id="prependedInput" type="text" placeholder="" value="<?php echo $url_invite;?>">
            </div>
            <a id="share-button" class="btn-facebook-share" style="cursor: pointer;"><img src="/img/icon-facebook.png">
                Chia sẻ lên Facebook
            </a>
            <p id="noti-ctc"></p>
            <script type="text/javascript">
                var href = '<?php echo $url_invite; ?>';
            </script>
        </div> 
        
        <?php if (count($listemails) > 0):?>
        <div class="row-fluid mail-invited">
            <p>Thư đã gửi</p>
            <?php foreach ($listemails as $email) : ?>
                <div class="span4 type-email-invited">
                    <?php if ($email->getVerified() == EmailInvite::EMAIL_INVITE_NEW): ?>
                        <div class="new-email-invite into"></div>
                    <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_SENT): ?>
                        <div class="sent-email-invite into"></div>    
                    <?php elseif ($email->getVerified() == EmailInvite::EMAIL_INVITE_ERROR): ?>
                        <div class="error-email-invite into" ></div>  
                    <?php else: ?>
                        <div class="success-email-invite into" ></div>
                    <?php endif; ?>
                        <div class="emai-invited"><?php print_r($email->getEmail()); ?></div>   
                </div>
                <?php ?>
            <?php endforeach; ?>  
        </div>    
        <?php endif; ?>
            
        <div class="text-email-invited">
            <p>*Bạn sẽ nhận được nhận <span>5 Credit</span> khi người được giới thiệu đăng ký tài khoản và hoàn thành công việc đầu tiên,
                nhận <span>30 Credit</span> khi người được giới thiệu thuê thành công việc đầu tiên (có giá trị ít nhất 5.000.000 VNĐ) trên vLance.vn.
                Trong cả 2 trường hợp trên, người được giới thiệu cũng được tặng số lượng credit tương tự.</p>    
        </div>    
    </div>        
</div>        
<?php $view['slots']->stop(); ?>