<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\JobBundle\Entity\Account; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $now = new DateTime('now'); ?>

<?php
    /* @var $category Vlance\AccountBundle\Entity\Category */
    /* @var $numJob int */
    /* @var $totalBudget float */
    /* get sub category: $category->getChilds() */
?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $category->getTitle(). " | vLance"); ?>
<?php $view['slots']->set('description', $category->getMetaDescription());?>
<?php $view['slots']->set('keyword', $category->getMetaKeyword());?>

<?php $view['slots']->set('og_title', $category->getMetaTitle());?>
<?php $view['slots']->set('og_description', $category->getMetaDescription());?>
<?php $view['slots']->set('og_image', $category->getMetaImage());?>
<?php $view['slots']->set('og_keyword', $category->getMetaKeyword());?>

<?php if($category->getSeoUrl() != 0): ?>
<?php $view['slots']->set('og_url', $view['router']->generate('category_show', array('seoUrl' => $category->getSeoUrl()), true));?>
<?php else: ?>
<?php $view['slots']->set('og_url', $view['router']->generate('category_show', array('seoUrl' => $category->getHash()), true));?>
<?php endif; ?>

<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminCategory_edit',array('pk' => $category->getId())) . '" target="_blank" class="btn">'
        . '<i class="icon-pencil"></i> Sửa category'
    .'</a>'
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminCategory_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách categories'
    .'</a>'
);?>

<?php $view['slots']->start('content') ?>
<div class="banner-single-post" style="background-image: url(../img/banner-category-lv2.jpg)">
    <div class="background_banner">
        <div class="container">
            <div class="row-fluid title_single_post">
                <h2 class="entry-title">Bạn đang <strong>Tìm kiếm Freelancer</strong> hay muốn <strong>Nhận việc</strong> <?php echo $category->getTitle(); ?></h2>
            </div>
            <div class="row-fluid entry-meta">
                <div class="button span6">
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>" class="new-btn new-btn-orange new-btn-xlarge ">Tìm Freelancer</a>
                </div>
                <div class="button span6">
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" class="new-btn new-btn-primary new-btn-xlarge ">Nhận việc làm</a>                    
                </div>
            </div>
        </div>
    </div>
</div>

<div class="category-view container">
    <div class="row category-content">
        <div class="span6 content-text">
            <h1><?php echo $category->getTitle();?></h1>
            <div class="category-desc"><?php echo $nl2pDesc;?></div>
            <div class="category-social">
                <div class="fb-like" data-href="<?php echo $absoluteUrl; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>
        </div>
        <div class="span5 offset1 category-cta">
            <h2>Bạn làm về <strong><?php echo $category->getTitle();?></strong>?</h2>
            <div class="cta-button">
                <a href="<?php echo $view['router']->generate('account_register')?>" class="new-btn new-btn-large new-btn-primary btn-weight">Đăng ký ngay</a>
            </div>
            <div class="cta-stats">
                <p>Hiện tại trên vLance.vn</p>
                <ul>
                    <li>Có <strong><?php echo $numJob; ?></strong> việc <strong><?php echo $category->getTitle();?></strong></li>
                    <li>Với tổng giá trị <strong><?php echo $totalBudget; ?> VNĐ</strong></li>
                </ul>
            </div>
        </div>  
    </div>
</div>

<div class="top-freelancer container">
    <div class="row-fluid">
        <div class="row top-freelancer-title">
            <h2 class="span12">Freelancer hàng đầu trong lĩnh vực <?php echo $category->getTitle() ?>:</h2>
        </div>
        <div class="big-block-top-freelancer row-fluid">
            <?php $i = 0; ?>
            <?php foreach($topFreelancer as $freelancer): ?>
                <?php $i++; ?>
                <?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
                <?php $is_owner = ($acc == $freelancer['entity']) // viewer in on his profile page ?>
                <?php if($i == 1) {
                        $bonus = "first"; 
                } else if($i == count($topFreelancer)-2) {
                        $bonus = "four"; 
                } else if($i == count($topFreelancer)-1) {
                        $bonus = "five"; 
                } else if($i == count($topFreelancer)) {
                        $bonus = "last";
                } else {
                        $bonus = "";
                }?>

                <div class="span2 block-top-freelancer <?php echo $bonus;?> ">
                    <div class="block-avatar">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>">
                            <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                            <?php $real_avatar = ResizeImage::resize_image($freelancer['entity']->getFullPath(), '180x180', 180, 180, 1); ?>
                            <?php if($real_avatar) : ?>
                                <?php $profile_avatar = $real_avatar;?>
                            <?php endif;?>
                            <?php if($is_owner): // Display edit link on his profile page?>
                            <div class="update-link tf300"><a class="tf300" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $freelancer['entity']->getId(), 'referer' => 'redirect')) ?>"><?php echo $view['translator']->trans('profile.center.update_image', array(), 'vlance') ?></a></div>
                            <?php endif;?>
                            <img width="180" height="180" itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $freelancer['entity']->getFullName(); ?>" title="<?php echo $freelancer['entity']->getFullName(); ?>" />
                        </a>
                    </div>
                    <div class="block-info">
                        <p class="fullname">
                            <span>
                                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>">
                                    <?php echo $freelancer['entity']->getFullName(); ?> 
                                </a>
                            </span>
                            <span>
                                <?php if ($freelancer['entity']->getIsCertificated()) : ?>
                                    <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>" title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                                <?php endif;?> 

                                <?php if (!is_null($freelancer['entity']->getTelephoneVerifiedAt())): ?>
                                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                                <?php endif; ?>
                                <?php if (!is_null($freelancer['entity']->getPersonId())): ?>
                                    <?php if (!is_null($freelancer['entity']->getPersonId()->getVerifyAt())): ?>
                                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php /* comment        
                                    <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                                */ ?>  
                                <script type="text/javascript" language="javascript">
                                    jQuery(document).ready(function(){
                                        $(function () {
                                            $('[data-toggle="tooltip"]').tooltip();
                                        });
                                    });
                                </script>
                            </span>
                        </p>
                        <p class="info-btn">
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>" class="new-btn new-btn-orange large-btn new-btn-info">xem freelancer</a>
                        </p>
                        <p class="money-earn">
                            Đã làm: <span style="font-size: 14px"><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>"><?php echo number_format($freelancer['entity']->getInCome(), '0', ',', '.'); ?></a></span> VNĐ
                        </p>
                    </div>
                    <div class="rating-block block-rating">
                        <div class="rating-box add-rating-box">
                            <?php $rating = ($freelancer['entity']->getNumReviews() == 0) ? 0 : (($freelancer['entity']->getScore() / $freelancer['entity']->getNumReviews()) * 20); ?>
                            <div class="rating" style="width:<?php echo number_format($rating, '2', '.', '.') . '%'; ?>"></div>
                        </div>
                        <div class="pass-projects">
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>"><?php echo $freelancer['jobworked']; ?> việc</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>        
        </div>
    </div>
</div>

<div class="job-projects container">
    <div class="block-job-projects">
        <div class="row-fluid">
            <h3 class="span12">Công việc <?php echo $category->getTitle() ?>:</h3>
        </div>
        <div class="row-fluid">
            <table class="table container">
                <thead class="table-name-column row-fluid">
                    <tr class="row-fluid">
                        <th class="span2">Tên dự án</th>
                        <th class="span2">Địa điểm</th>
                        <th class="span2">Ngân sách</th>
                        <th class="span2">Hạn chào giá</th>
                        <th class="span2">Lượt chào giá</th>
                    </tr>
                </thead>
                <tbody class="table-statistics row-fluid">
                    <?php if($totalJob < 10)
                            $qty = $totalJob;
                        else
                            $qty = 10;
                        ?>
                    <?php for($j=0;$j<$qty; $j++): ?>
                    <tr class="row-fluid <?php if($j>4) echo "cut-on-mobile"; ?>">
                        <td class="span4 job-Description">
                            <div class="text">
                                <p><a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $jobsSubCategory[$j]->getHash())) ?>"><?php echo $jobsSubCategory[$j]->getTitle(); ?></a></p>
                                <p>                                
                                    <?php if($jobsSubCategory[$j]->getDescription()): ?>
                                    <?php echo htmlentities(JobCalculator::cut(JobCalculator::replacement($jobsSubCategory[$j]->getDescription()),150), ENT_SUBSTITUTE, "UTF-8") ?>
                                        <a class="read_more" href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $jobsSubCategory[$j]->getHash())) ?>">
                                            <?php echo $view['translator']->trans('list_job.right_content.read_more', array(), 'vlance') ?>
                                        </a>
                                    <?php endif; ?>
                                </p>
                            </div>
                        </td>
                        <td class="span2 job-Location"><p><?php echo $jobsSubCategory[$j]->getCity(); ?></p></td>
                        <td class="span2 job-Budget"><p><?php echo number_format($jobsSubCategory[$j]->getBudget(), '0', ',', '.'); ?> VNĐ</p></td>
                        <td class="span2 job-Deadline">
                            <p>
                                <?php 
                                    $remain = JobCalculator::ago($jobsSubCategory[$j]->getCloseAt(), $now);
                                    if($jobsSubCategory[$j]->getStatus() == Job::JOB_FINISHED){
                                        echo '<div>'.$view['translator']->trans('list_job.right_content.has_finished', array(), 'vlance').'</div>';
                                    } elseif($jobsSubCategory[$j]->getStatus() == Job::JOB_WORKING){
                                        echo '<div>'.$view['translator']->trans('list_job.right_content.is_working', array(), 'vlance').'</div>';
                                    } elseif($jobsSubCategory[$j]->getStatus() == Job::JOB_AWARDED){
                                        echo '<div>'.$view['translator']->trans('list_job.right_content.has_awarded', array(), 'vlance').'</div>';
                                    } elseif($remain == false){
                                        echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</div>';
                                    }  else {
                                        echo $view['translator']->trans('list_job.right_content.bidding_dealine', array(), 'vlance').': '.$remain ;
                                    };
                                ?>
                            </p>
                        </td>
                        <td class="span2 job-Block-Bid">
                            <div>
                                <p class="numBid"><?php echo count($jobsSubCategory[$j]->getBids()); ?> chào giá</p>
                                <p class="bid-btn"><a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $jobsSubCategory[$j]->getHash())) ?>" class="new-btn btn-default">Chào giá</a></p>
                            </div>
                        </td>
                    </tr>
                    <?php endfor; ?>
                </tbody>
            </table>
        </div>
        <div class="table-footer">
            <p>
                <a href="<?php echo $view['router']->generate('freelance_job_list',array('filters' => $category->getJobListFilters())); ?>">Xem tất cả <strong><?php echo $numJob; ?></strong> công việc</a>
            </p>
        </div>
    </div>
</div>

<div class="post-job-category container">
    <div class="block-post-job">
        <div class="post-button">
            <a href="<?php echo $view['router']->generate('job_new'); ?>" class="new-btn new-btn-orange new-btn-large btn-weight" title="Đăng dự án" onclick="vtrack('Click post job', {'location':'Category page LV2'})">Đăng dự án ngay</a>
        </div>
        <div>
            <p class="text">(Đăng dự án dễ dàng để tìm được freelancer ưng ý)</p>
        </div>
    </div>
    <div class="block-statistics">
        <div class="row-fluid">
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>" title="Thành viên hoạt động"><?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?></a>
                </p>
                <p>
                    Thành viên hoạt động
                </p>
            </div>
            <div class="span6 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" title="Tổng giá trị các dự án đăng"><?php echo $totalBudget; ?><font size="3"> VNĐ</font></a>
                </p>
                <p>
                    Tổng giá trị các dự án đăng
                </p>
            </div>
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" title="Dự án đã đăng"><?php echo $numJob; ?></a>
                </p>
                <p>
                    Dự án đã đăng
                </p>
            </div>
        </div>
    </div>
</div>

<?php if(count($subCategories) > 0): ?>
<?php $pos = 0; ?>
<div class="other-category-footer container">
    <div class="block-other-category">
        <div class="row-fluid">
            <h2 class="span12">Các việc liên quan:</h2>
        </div>
        <div class="row-fluid">
            <p class="span12">
                <?php foreach ($subCategories as $subCat): ?>
                    <?php $entity = $subCat['entity']; ?>
                    <?php if($entity->getSeoUrl() != NULL): ?>
                        <?php $url = $view['router']->generate('category_show', array('seoUrl' => $entity->getSeoUrl())); ?>
                    <?php else: ?>
                        <?php $url = $view['router']->generate('category_show', array('seoUrl' => $entity->getHash())); ?>
                    <?php endif; ?>
                    <a title="<?php echo $entity; ?>" href="<?php echo $url; ?>"><?php echo $entity->getTitle(); ?></a><?php if($pos == count($subCategories) -1){
                            echo "";
                        } else {
                            echo ",";
                        }
                    ?>
                    <?php $pos++; ?>
                <?php endforeach; ?>
            </p>
        </div>
    </div>
</div>
<?php endif; ?>

<?php $view['slots']->stop(); ?>