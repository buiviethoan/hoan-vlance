<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\JobBundle\Entity\Account; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $now = new DateTime('now'); ?>
<?php
    /* @var $category Vlance\AccountBundle\Entity\Category */
    /* @var $numJob int */
    /* @var $totalBudget float */
    /* get sub category: $category->getChilds() */
?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $category->getTitle(). " | vLance"); ?>

<?php $view['slots']->set('description', $category->getMetaDescription());?>
<?php $view['slots']->set('keyword', $category->getMetaKeyword());?>

<?php $view['slots']->set('og_title', $category->getMetaTitle());?>
<?php $view['slots']->set('og_description', $category->getMetaDescription());?>
<?php $view['slots']->set('og_image', $category->getMetaImage());?>
<?php $view['slots']->set('og_keyword', $category->getMetaKeyword());?>

<?php if($category->getSeoUrl() != 0): ?>
<?php $view['slots']->set('og_url', $view['router']->generate('category_show', array('seoUrl' => $category->getSeoUrl()), true));?>
<?php else: ?>
<?php $view['slots']->set('og_url', $view['router']->generate('category_show', array('seoUrl' => $category->getHash()), true));?>
<?php endif; ?>

<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminCategory_edit',array('pk' => $category->getId())) . '" target="_blank" class="btn">'
        . '<i class="icon-pencil"></i> Sửa category'
    .'</a>'
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminCategory_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách categories'
    .'</a>'
);?>

<?php $view['slots']->start('content') ?>
<div class="category-view container">
    <div class="row category-content">
        <div class="span6 content-text">
            <h1><?php echo $category->getTitle();?></h1>
            <div class="category-desc"><?php echo $nl2pDesc;?></div>
            <div class="category-social">
                <div class="fb-like" data-href="<?php echo $absoluteUrl; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>
        </div>
        <div class="span5 offset1 category-cta">
            <h2>Bạn làm về <strong><?php echo $category->getTitle();?></strong>?</h2>
            <div class="cta-button">
                <a href="<?php echo $view['router']->generate('account_register')?>" class="new-btn new-btn-large new-btn-primary btn-weight" title="Đăng ký tài khoản">Đăng ký ngay</a>
            </div>
            <div class="cta-stats">
                <p>Hiện tại trên vLance.vn</p>
                <ul>
                    <li>Có <strong><?php echo $numJob; ?></strong> việc <strong><?php echo $category->getTitle();?></strong></li>
                    <li>Với tổng giá trị <strong><?php echo $totalBudget; ?> VNĐ</strong></li>
                </ul>
            </div>
        </div>  
    </div>
    
    <?php if(count($subCategories) > 0): ?>
    <?php $pos = 0; ?>
    <?php foreach ($subCategories as $subCat): ?>
        <?php $entity = $subCat['entity']; ?>
        <?php if($entity->getSeoUrl() != NULL): ?>
            <?php $url = $view['router']->generate('category_show', array('seoUrl' => $entity->getSeoUrl())); ?>
        <?php else: ?>
            <?php $url = $view['router']->generate('category_show', array('seoUrl' => $entity->getHash())); ?>
        <?php endif; ?>
        <?php if($pos % 3 == 0):?>
            <div class="row-fluid sub-category">
        <?php endif;?>
                <div class="span4 item">
                    <?php $resize = ResizeImage::resize_image($entity->getFullPathMetaImage(), '300x180', 300, 180, 1);?>
                    <?php if($resize) : ?>
                    <a class="cat-thumb" href="<?php echo $url; ?>" title="<?php echo $entity->getTitle(); ?>">   
                        <img class="img" width="300" height="180" src="<?php echo $resize; ?>" alt="<?php echo $entity->getTitle(); ?>" title="<?php echo $entity->getTitle(); ?>" />
                    </a>
                    <?php else: ?>
                    <a class="cat-thumb" href="<?php echo $url; ?>" title="<?php echo $entity->getTitle(); ?>">   
                        <img width="300" height="180" src = "<?php echo $view['assets']->getUrl('img/thumbnail/300x180.png') ?>" alt="<?php echo $entity->getTitle(); ?>" title="<?php echo $entity->getTitle(); ?>" />
                    </a>    
                    <?php endif; ?>
                    <a href="<?php echo $url; ?>" class="cat-link" title="<?php echo $entity->getTitle(); ?>"><?php echo $entity->getTitle(); ?> (<?php echo $subCat['count']; ?>)</a>
                </div>
        <?php if($pos % 3 == 2 || $pos == count($subCategories) - 1): ?>
            </div>
        <?php endif; ?> 
        <?php $pos++; ?>
    <?php endforeach; ?>
    <?php endif; ?>
</div>


<div class="post-job-category container">
    <div class="block-post-job">
        <div class="post-button">
            <a href="<?php echo $view['router']->generate('job_new'); ?>" class="new-btn new-btn-orange new-btn-large btn-weight" onclick="vtrack('Click post job', {'location':'Category page LV1'})">Đăng dự án ngay</a>
        </div>
        <div>
            <p class="text">(Đăng dự án dễ dàng để tìm được freelancer ưng ý)</p>
        </div>
    </div>
    <div class="block-statistics">
        <div class="row-fluid">
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>"><?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?></a>
                </p>
                <p>
                    Thành viên hoạt động
                </p>
            </div>
            <div class="span6 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>"><?php echo $totalBudget; ?><font size="3"> VNĐ</font></a>
                </p>
                <p>
                    Tổng giá trị các dự án đăng
                </p>
            </div>
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>"><?php echo $numJob ?></a>
                </p>
                <p>
                    Dự án đã đăng
                </p>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop(); ?>