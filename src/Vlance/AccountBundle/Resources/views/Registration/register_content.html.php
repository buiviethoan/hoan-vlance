<form action="<?php echo $view['router']->generate('fos_user_registration_register') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->widget($form)?>
    <div>
        <input type="submit" value="<?php echo $view['translator']->trans('registration.submit', array(), 'FOSUserBundle'); ?>"/>
    </div>
</form>