<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('registration.check_email.title', array('%email%' => $user->getEmail()), 'vlance'));?>
<?php $view['slots']->start('content')?>
<div class="confirm-section">
    <div class="container">
        <div class="jumbotext">
            <h1 class="lead"><?php echo $view['translator']->trans('registration.check_email.mail_sent', array(), 'vlance')?></h1>
            <p><?php echo $view['translator']->trans('registration.check_email.description', array('%email%' => $user->getEmail()), 'vlance')?>
                <br>
                <span style="text-align: center;font-style: italic;color: #999;">(Hãy kiểm tra trong thư mục Spam nếu bạn không nhận được thư của chúng tôi)</span>
            </p>            
        </div>

        <div class="row-fluid">
             <div class="confirm-image">
            </div>
        </div>
    </div>
</div>
<?php $view['slots']->stop();?>