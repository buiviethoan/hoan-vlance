<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Activate account successfully'));?>
<?php $view['slots']->start('content')?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'RegistrationConfirmEmail');
</script>
<?php endif;?>
<?php // End Tracking ?>

    <?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
    <div class="confirm-section">
    <div class="container">
        <div class="jumbotext">
            <h1 class="lead"><?php echo $view['translator']->trans('registration.confirmed', array(), 'FOSUserBundle')?></h1>
            <?php if( $acc->getType() == 'freelancer') :?>
                <script type="text/javascript">
                    $(document).ready(function(){
                        window.setTimeout("location=('<?php echo $view['router']->generate('jobs_bid_project') ?>');",5000);
                      });
                </script>
            <?php endif; ?>
            <?php if( $acc->getType() == 'client') :?>
                <script type="text/javascript">
                    $(document).ready(function(){
                        window.setTimeout("location=('<?php echo $view['router']->generate('jobs_bid_project') ?>');",5000);
                      });
                </script>
            <?php endif; ?>
            <p>Bạn sẽ được chuyển về trang <a href="<?php echo $view['router']->generate('jobs_bid_project') ?>">góc làm việc sau 5s</a></p>




            <?php if ($app->getSession()) : ?>
                <?php $url = $app->getSession()->get('_security.' . $app->getSecurity()->getToken()->getProviderKey() . '.target_path') ?>
                <?php if ($url):?>
                    <p><a href="<?php echo $url;?>"><?php echo $view['translator']->trans('registration.back', array(), 'FOSUserBundle')?></a></p>
                <?php endif;?>
            <?php endif;?>
        </div>

        <div class="row-fluid">
             <div class="confirm-image">
            </div>
        </div>
    </div>
</div>
<?php $view['slots']->stop();?>