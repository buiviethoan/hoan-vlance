<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Create an account'));?>
<?php $view['slots']->start('content')?>
    <h1><?php echo $view['translator']->trans('Account creation')?></h1>
    <?php echo $view->render('VlanceAccountBundle:Account:miniForm.html.php', array('form' => $form)) ?>
<?php $view['slots']->stop();?>