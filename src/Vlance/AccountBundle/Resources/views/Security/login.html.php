<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('page.title', array(), 'vlance'));?>
<?php $view['slots']->set('og_title', $view['translator']->trans('page.title', array(), 'vlance'));?>

<?php $view['slots']->set('og_description', "Đăng nhập vào vLance.vn. Đăng việc miễn phí. Cộng đồng freelancer chuyên nghiệp đầu tiên tại Việt Nam. Giúp freelancer có nhiều việc làm, tăng thu nhập.");?>
<?php $view['slots']->start('content')?>
<!-- Login page -->
<div class="login-section container">
    <div class="span5">
        <h1><?php echo $view['translator']->trans('page.title', array(), 'vlance')?></h1>
        <div class="login-box">
            <form id="login-form" action="<?php echo $view['router']->generate('fos_user_security_check') ?>" method="post">
                <div class="control-group<?php if (!empty($error)) : ?> error<?php endif;?>">
                <input type="hidden" name="_csrf_token" value="<?php echo $csrf_token ?>" />
                <input type="text" id="username" name="_username" value="<?php echo $last_username?>" required="required" class="span4" 
                       placeholder="<?php echo $view['translator']->trans('security.login.username', array(), 'FOSUserBundle'); ?>" 
                       title="<?php echo $view['translator']->trans('security.login.username', array(), 'FOSUserBundle'); ?>"/>
                <input type="password" id="password" name="_password" required="required" class="span4"
                       placeholder="<?php echo $view['translator']->trans('security.login.password', array(), 'FOSUserBundle'); ?>" 
                       title="<?php echo $view['translator']->trans('security.login.password', array(), 'FOSUserBundle'); ?>"/>
                <?php if (!empty($error)) : ?>
                    <div class="alert alert-error"><?php echo $error;?></div>
                <?php endif;?>
                </div>
                <label for="remember_me" class="checkbox">
                    <input type="checkbox" id="remember_me" name="_remember_me" value="on" />
                    <?php echo $view['translator']->trans('security.login.remember_me', array(), 'FOSUserBundle'); ?></label>
                <div class="row-fluid">
                    <button type="submit" id="_submit" name="_submit" class="btn btn-large btn-primary">
                        <?php echo $view['translator']->trans('security.login.submit', array(), 'FOSUserBundle'); ?>
                    </button>
                </div>
                <?php if($app->getRequest()->get('_route') != $app->getRequest()->headers->get('referer')) : ?>
                    <input type="hidden" name="_target_path" value="<?php echo $view['router']->generate('account_login_after', array('referer' => urlencode($app->getRequest()->headers->get('referer'))), true); ?>" /> 
                <?php else: ?>
                    <input type="hidden" name="_target_path" value="<?php echo $view['router']->generate('vlance_homepage'); ?>" /> 
                <?php endif; ?>
                <p class="lost-password"><?php echo $view['translator']->trans('page.forgotPassword', array('%url%' => $view['router']->generate('fos_user_resetting_request')), 'vlance') ?></p>
            </form>
        </div>
        <div class="signup-box">
            <h3><?php echo $view['translator']->trans('page.dontHaveAccountTitle', array(), 'vlance')?></h3>
            <?php echo $view->render('VlanceAccountBundle:Account/register:types.html.php');?>
            <p>
                <?php echo $view['translator']->trans('register.orSocialLogin', array(), 'vlance')?>
            </p>
            <!--<p class="login-social">-->
            <div class="login-socail-network">
                <a class="btn-facebook" onclick="fb_login()"><span><img src="/img/icon-facebook.png">Facebook</span></a>
                <a class="btn-google-plus" onclick="google_login()"><span><img src="/img/icon-google+.png">Google</span></a>
                <a class="btn-linkedin" onclick="linkedin_login()"><span><img src="/img/icon-linkedin.png">LinkedIn</span></a>
            </div>   
                <!--<input class="btn btn-large btn-facebook" onclick="fb_login()" type="button" value="<?php // echo $view['translator']->trans('registration.home.facebook', array(), 'vlance'); ?>"/>-->
                <?php // TODO tạm thời tắt vì chỉ hỗ trợ login facebook
                /*<a href="#" class="lg-facebook"><?php echo $view['translator']->trans('register.facebook', array(), 'vlance')?></a>
                <a href="#" class="lg-google"><?php echo $view['translator']->trans('register.google', array(), 'vlance')?></a>*/?>
            <!--</p>-->
        </div>
    </div>
    <div class="quick-guide span5 offset1">
        <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('page.whyChooseVlance', array(), 'vlance')?></h3>
        <p><?php echo $view['translator']->trans('page.whyChooseVlance_explain1', array(), 'vlance')?></p>
        <p><?php echo $view['translator']->trans('page.whyChooseVlance_explain2', array(), 'vlance')?></p>
        <div class="small-pic">
            <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_cs_4.jpg') ?>" alt="" /></span>
            <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_2.jpg') ?>" alt="" /></span>
            <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_3.jpg') ?>" alt="" /></span>
            <span><img src="<?php echo $view['assets']->getUrl('img/icon/icon_create_f_c_4.jpg') ?>" alt="" /></span>
        </div>
    </div>

</div>
<!-- End Login page -->










<?php /*<h1><?php echo $view['translator']->trans('page.title', array(), 'vlance')?></h1>
    <?php if (!empty($error)) : ?>
    <div class="alert alert-error"><?php echo $error;?></div>
    <?php endif;?>
    <form action="<?php echo $view['router']->generate('fos_user_security_check') ?>" method="post">
        <input type="hidden" name="_csrf_token" value="<?php echo $csrf_token ?>" />

        <label for="username"><?php echo $view['translator']->trans('security.login.username', array(), 'FOSUserBundle'); ?></label>
        <input type="text" id="username" name="_username" value="<?php echo $last_username?>" required="required" />

        <label for="password"><?php echo $view['translator']->trans('security.login.password', array(), 'FOSUserBundle'); ?></label>
        <input type="password" id="password" name="_password" required="required" />

        <input type="checkbox" id="remember_me" name="_remember_me" value="on" />
        <label for="remember_me"><?php echo $view['translator']->trans('security.login.remember_me', array(), 'FOSUserBundle'); ?></label>

        <input type="submit" id="_submit" name="_submit" value="<?php echo $view['translator']->trans('security.login.submit', array(), 'FOSUserBundle'); ?>" />
    </form>
    <?php echo $view->render('VlanceAccountBundle:Account/register:types.html.php');?> */?>
<?php $view['slots']->stop();?>