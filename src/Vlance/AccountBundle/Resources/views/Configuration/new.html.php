<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('config.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<h1><?php echo $view['translator']->trans('list_configuration.title', array(), 'vlance'); ?></h1>
<div class="page-configuration">
<div><?php echo $view['translator']->trans('list_configuration.guide_email', array(), 'vlance'); ?></div>
    <form id="form-update-config" action="<?php echo $view['router']->generate('update_user') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->widget($form) ?>
        <p class="btn-submit">
            <input type="submit" class="btn btn-primary button" value="<?php echo $view['translator']->trans('config.save', array(), 'vlance') ?>">
        </p>
    </form>
</div>

<!--<div class="update-facebookId">
    <?php // if($entity->getFacebookId() == '' || $entity->getLinkedinId() == '') :  ?>
        <p>Bạn có muốn kết nối với <b>vLance.vn</b> bằng tài khoản mạng xã hội ? Hãy click vào liên kết bên dưới. </p>
    <?php // endif; ?>
    <?php // if($entity->getFacebookId() == '') : ?>
        <p><a class="btn btn-facebook"  onclick="fb_login()">Facebook</a></p>
    <?php // endif; ?>
    <?php // if($entity->getLinkedinId() == '') : ?>
        <p><a class="btn btn-linkedin" onclick="linkedin_login()"><span></span></a></p>
    <?php // endif; ?>
</div>-->

 <?php // Facebook js SDK, just after opening <body> tag ?>
    <div id="fb-root"></div>
    <script>
              window.fbAsyncInit = function() {
                <?php // init the FB JS SDK ?>
                FB.init({
                    appId      : '197321233758025',    // App ID from the app dashboard
                    channelUrl : '/web/fbchannel.php', // Channel file for x-domain comms
                    status     : true,                 // Check Facebook Login status
                    xfbml      : true                  // Look for social plugins on the page
                });

                <?php // Additional initialization code such as adding Event Listeners goes here ?>
            };

            <?php // Load the SDK asynchronously?>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/vi_VN/all.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            function fb_login() {
                window.location.href = "https://www.facebook.com/dialog/oauth?%20client_id=197321233758025&scope=email&redirect_uri=<?php echo /* chỗ này phải thêm domain vào */ $view['router']->generate('update_face_user', array(), true) ?>";
            }

            function fb_logout() {
                FB.logout(function(response) {
                    console.log('Logout thành công');
                });
            }
        </script>
    <?php // End facebook js SDK ?>
        
    <?php // login by Linkedin ?>
     <script type="text/javascript">
       function linkedin_login() {
                window.location.href = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=7588i5efds8ydr&scope=r_basicprofile%20r_emailaddress&state=53cddbb67061f&redirect_uri=<?php echo $view['router']->generate('update_linkedin_user', array(), true) ?>";
            }
     </script>
      
    <?php // End linkedin js SDK ?>
<?php $view['slots']->stop(); ?>