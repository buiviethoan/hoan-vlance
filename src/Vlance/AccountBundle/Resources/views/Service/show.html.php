<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $now = new DateTime('now'); ?>

<?php /* @var $service Vlance\AccountBundle\Entity\Service */?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $service->getTitle(). " | vLance"); ?>
<?php $view['slots']->set('description', $service->getMetaDescription());?>
<?php $view['slots']->set('keyword', $service->getMetaKeyword());?>

<?php $view['slots']->set('og_title', $service->getMetaTitle());?>
<?php $view['slots']->set('og_description', $service->getMetaDescription());?>
<?php $view['slots']->set('og_image', $service->getMetaImage());?>
<?php $view['slots']->set('og_keyword', $service->getMetaKeyword());?>

<?php if($service->getSeoUrl() != 0): ?>
<?php $view['slots']->set('og_url', $view['router']->generate('service_landing_page', array('seoUrl' => $service->getSeoUrl()), true));?>
<?php else: ?>
<?php $view['slots']->set('og_url', $view['router']->generate('service_landing_page', array('seoUrl' => $service->getHash()), true));?>
<?php endif; ?>

<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminService_edit',array('pk' => $service->getId())) . '" target="_blank" class="btn">'
        . '<i class="icon-pencil"></i> Sửa service'
    .'</a>'
    .'<a href="' . $view['router']->generate('Vlance_AccountBundle_AdminService_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách service'
    .'</a>'
);?>

<?php $view['slots']->start('content') ?>
<div class="banner-single-post" style="background-image: url(../img/banner-category-lv2.jpg)">
    <div class="background_banner">
        <div class="container">
            <div class="row-fluid title_single_post">
                <h2 class="entry-title">Bạn đang tìm kiếm <strong>Freelancer</strong> cung cấp dịch vụ <strong><?php echo $service->getTitle(); ?></strong></h2>
            </div>
            <div class="row-fluid entry-meta">
                <div class="button span12">
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>" class="new-btn new-btn-orange new-btn-xlarge ">Tìm Freelancer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="category-view container">
    <div class="row category-content">
        <div class="span6 content-text">
            <h1><?php echo $service->getTitle();?></h1>
            <div class="category-desc"><?php echo $nl2pDesc;?></div>
            <div class="category-social">
                <div class="fb-like" data-href="<?php echo $absoluteUrl; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>
        </div>
        <div class="span5 offset1 category-cta">
            <h2>Bạn làm về <strong><?php echo $service->getTitle();?></strong>?</h2>
            <div class="cta-button">
                <a href="<?php echo $view['router']->generate('account_register')?>" class="new-btn new-btn-large new-btn-primary btn-weight">Đăng ký ngay</a>
            </div>
            <div class="cta-stats">
                <p>Hiện tại trên vLance.vn</p>
                <ul>
                    <li>Có <strong><?php echo $numFl; ?></strong> freelances cung cấp dịch vụ <strong><?php echo $service->getTitle();?></strong></li>
                </ul>
            </div>
        </div>  
    </div>
</div>

<div class="top-freelancer container">
    <div class="row-fluid">
        <div class="row top-freelancer-title">
            <h2 class="span12"><b>Các Freelancers hàng đầu cung cấp dịch vụ <?php echo $service->getTitle() ?>:</b></h2>
        </div>
        <div class="big-block-top-freelancer row-fluid">
            <?php $i = 0; ?>
            <?php foreach($topFreelancer as $freelancer): ?>
                <?php $i++; ?>
                <?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
                <?php $is_owner = ($acc == $freelancer['entity']) // viewer in on his profile page ?>
                <?php if($i == 1 || $i == 6) { 
                    $bonus = "first"; 
                } else if($i == count($topFreelancer)) {
                    $bonus = "last-service";
                } else if($i > 6) {
                    $bonus = "cut-on-mobile";
                } else {
                    $bonus = "";
                }?>
                <div class="span2 block-top-freelancer <?php echo $bonus;?> ">
                    <div class="block-avatar">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>">
                            <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                            <?php $real_avatar = ResizeImage::resize_image($freelancer['entity']->getFullPath(), '180x180', 180, 180, 1); ?>
                            <?php if($real_avatar) : ?>
                                <?php $profile_avatar = $real_avatar;?>
                            <?php endif;?>
                            <img width="180" height="180" itemprop="photo" src="<?php echo $profile_avatar ?>" alt="<?php echo $freelancer['entity']->getFullName(); ?>" title="<?php echo $freelancer['entity']->getFullName(); ?>" />
                        </a>
                    </div>
                    <div class="block-info">
                        <p class="fullname">
                            <span>
                                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>">
                                    <b><?php echo $freelancer['entity']->getFullName(); ?> </b>
                                </a>
                            </span>
                            <span>
                                <?php if ($freelancer['entity']->getIsCertificated()) : ?>
                                    <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>" title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                                <?php endif;?> 

                                <?php if (!is_null($freelancer['entity']->getTelephoneVerifiedAt())): ?>
                                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                                <?php endif; ?>
                                <?php if (!is_null($freelancer['entity']->getPersonId())): ?>
                                    <?php if (!is_null($freelancer['entity']->getPersonId()->getVerifyAt())): ?>
                                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php /* comment        
                                    <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                                */ ?>  
                                <script type="text/javascript" language="javascript">
                                    jQuery(document).ready(function(){
                                        $(function () {
                                            $('[data-toggle="tooltip"]').tooltip();
                                        });
                                    });
                                </script>
                            </span>
                        </p>
                        <p class="info-btn">
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>" class="new-btn new-btn-orange large-btn new-btn-info">hồ sơ freelancer</a>
                        </p>
                        <p class="money-earn">
                            Đã làm: <span style="font-size: 14px"><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>"><?php echo number_format($freelancer['entity']->getInCome(), '0', ',', '.'); ?></a></span> VNĐ
                        </p>
                    </div>
                    <div class="rating-block block-rating">
                        <div class="rating-box add-rating-box">
                            <?php $rating = ($freelancer['entity']->getNumReviews() == 0) ? 0 : (($freelancer['entity']->getScore() / $freelancer['entity']->getNumReviews()) * 20); ?>
                            <div class="rating" style="width:<?php echo number_format($rating, '2', '.', '.') . '%'; ?>"></div>
                        </div>
                        <div class="pass-projects">
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancer['entity']->getHash())) ?>"><?php echo $freelancer['jobworked']; ?> việc</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>  
        </div>
    </div>
</div>

<div class="post-job-category container">
    <div class="block-post-job">
        <div class="post-button">
            <a href="<?php echo $view['router']->generate('job_new'); ?>" class="new-btn new-btn-orange new-btn-large btn-weight" title="Đăng dự án" onclick="vtrack('Click post job', {'location':'Service page'})">Đăng dự án ngay</a>
        </div>
        <div>
            <p class="text">(Đăng dự án dễ dàng để tìm được freelancer ưng ý)</p>
        </div>
    </div>
    <div class="block-statistics">
        <div class="row-fluid">
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>" title="Thành viên hoạt động">
                        <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                    </a>
                </p>
                <p>Thành viên hoạt động</p>
            </div>
            <div class="span6 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" title="Tổng giá trị các dự án đăng">
                        <?php echo number_format($jobStatistic['budget'], '0', ',', '.') . ' ' ?><font size="3"> VNĐ</font>
                    </a>
                </p>
                <p>
                    Tổng giá trị các dự án đăng
                </p>
            </div>
            <div class="span3 counted">
                <p>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" title="Dự án đã đăng">
                        <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                    </a>
                </p>
                <p>
                    Dự án đã đăng
                </p>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop(); ?>