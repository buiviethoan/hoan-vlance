<form action="<?php echo $view['router']->generate('fos_user_change_password') ?>" method="POST"  class="fos_user_change_password" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->widget($form)?>
    <div>
        <input type="submit" value="<?php echo $view['translator']->trans('change_password.submit', array(), 'FOSUserBundle'); ?>"/>
    </div>
</form>