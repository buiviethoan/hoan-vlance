<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('Change password for %username%', array('%username%' => $user->getUsername())));?>
<?php $view['slots']->start('content')?>
    <h1><?php echo $view['translator']->trans('Change password for %username%', array('%username%' => $user->getUsername()))?></h1>
    <?php echo $view->render('VlanceAccountBundle:ChangePassword:changePassword_content.html.php', array('form' => $form)) ?>
<?php $view['slots']->stop();?>