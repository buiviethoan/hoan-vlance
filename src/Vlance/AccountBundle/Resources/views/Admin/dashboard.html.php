<?php $current_route = $app->getRequest()->attributes->get('_route'); ?>

<div class="admin-menu-wrapper">
    <ul class="nav nav-tabs" id="account-admin-dashboard">
        <li><a href="#profile" data-toggle="tab" class="active">Hồ sơ</a></li>
        <li><a href="#usercash" data-toggle="tab">Ví tiền</a></li>
        <li><a href="#credit" data-toggle="tab">Credit (<b><?php echo $acc->getCredit()->getBalance(); ?></b>)</a></li>
        <li><a href="#verification" data-toggle="tab">Xác thực tài khoản</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="profile">
            <div class="accordion" id="accordion-display">
                <?php echo $view->render('VlanceAccountBundle:Admin/dashboard/profile:profile.html.php', array('acc' => $acc, 'vips' => $vips)) ?>
            </div>
        </div>
        <div class="tab-pane" id="usercash">
            <div class="accordion" id="accordion-display">
                <?php echo $view['actions']->render($view['router']->generate('admin_topup_usercash', array('uid' => $acc->getId())));?>
                <?php echo $view['actions']->render($view['router']->generate('admin_list_topupusercashtransaction', array('uid' => $acc->getId())));?>
            </div>
        </div>
        <div class="tab-pane" id="credit">
            <div class="accordion" id="accordion-display">
                <?php echo $view['actions']->render($view['router']->generate('admin_buy_credit', array('uid' => $acc->getId())));?>
                <?php echo $view['actions']->render($view['router']->generate('admin_list_creditbuytransaction', array('uid' => $acc->getId())));?>
            </div>
        </div>
        <div class="tab-pane" id="verification">
            <div class="accordion" id="accordion-display">
                <?php echo $view->render('VlanceAccountBundle:Admin/dashboard/verification:verified.html.php', array('acc' => $acc)) ?>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#account-admin-dashboard a:first').tab('show');
        });
    </script>
</div>