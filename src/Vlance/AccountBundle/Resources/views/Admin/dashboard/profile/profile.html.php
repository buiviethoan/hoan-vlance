<?php /* @var $acc Vlance\AccountBundle\Entity\Account */ ?>
<?php use Vlance\AccountBundle\Entity\AccountVip; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#profile--profile">
            Các link thường dùng
        </a>
    </div>
    <div id="profile--profile" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid">
                <a class="btn" title="Edit Admin View" href="<?php echo $view['router']->generate('Vlance_AccountBundle_AdminAccount_edit',array('pk' => $acc->getId()))?>" target="_blank">
                    Chỉnh sửa <em>(Admin view)</em>
                 </a>
                 <a class="btn" title="Edit User View" href="<?php echo $view['router']->generate('account_basic_edit',array('id' => $acc->getId()))?>" >
                    Chỉnh sửa <em>(User view)</em>
                 </a>
            </div>
        </div>
    </div>
</div>
    
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#profile--upgradevip">
            Nâng cấp tài khoản
        </a>
    </div>
    <div id="profile--upgradevip" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#profile--upgradevip" href="#profile--upgrade">
                        Nâng cấp VIP
                    </a>
                </div>
                <div id="profile--upgrade" class="accordion-body collapse">
                    <div id="admin-upgrade-vip" class="container row-fluid" style="text-align: center; width: auto; padding: 10px;">
                        <div class="span3">
                            <select id="package-vip" style="height: 42px;">
                                <option value="1" selected>VIP3 (360k-$17.90)</option>
                                <option value="2">VIP6 (600k-$27.90)</option>
                                <option value="3">VIP12 (960k-$42.90)</option>
                                <option value="4">MiniVIP3 (180k-$8.95)</option>
                                <option value="5">MiniVIP6 (300k-$13.95)</option>
                                <option value="6">MiniVIP12 (480k-$21.45)</option>
                            </select>
                        </div>
                        <div class="payment-methods span2">
                            <select id="payment-method" style="height: 42px; width: initial;">
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_BANK_TRANSFER; ?>" selected>Chuyển khoản</option>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_PAYPAL; ?>" >Paypal</option>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_USERCASH; ?>" >Ví vLance</option>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_OTHER; ?>" >Khác</option>
                            </select>
                        </div>
                        <div class="span3 type-vip" style="padding: 10px;">
                            <b>
                                <?php if(!is_null($acc->getTypeVIP())): ?>
                                    <?php if($acc->getTypeVIP() == 0): ?>
                                        <?php echo "Mini Vip</b><br/> Lĩnh vực: <b>";?>
                                        <?php if(is_object($acc->getCategoryVIP())){
                                            echo $acc->getCategoryVIP()->getTitle();
                                        } else {
                                            echo "NULL";
                                        }?>
                                        <?php echo "</b>"; ?>
                                    <?php else: ?>
                                        <?php echo "Vip"; ?>
                                    <?php endif ?>
                                <?php else: ?>
                                    <?php echo "Thường"; ?>
                                <?php endif; ?>
                            </b>
                        </div>
                        <div class="expired-vip-update span2" style="padding: 10px;">
                            <?php if(!is_null($acc->getTypeVIP())): ?>
                                <?php echo date_format($acc->getExpiredDateVip(), 'd-m-Y H:i:s'); ?>
                            <?php else: ?>
                                <?php echo '<span style="color: red;">NULL</span>'; ?>
                            <?php endif; ?>
                        </div>
                        <div class="button-submit span2">
                            <button id="btn-vip-submit" class="btn btn-large">Nâng cấp</button>
                            <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                        </div>
                    </div>
                </div>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#profile--upgradevip" href="#profile--cancel">
                        Hủy VIP
                    </a>
                </div>
                <div id="profile--cancel" class="accordion-body collapse">
                    <div id="admin-upgrade-vip-cancel" class="container row-fluid" style="text-align: center; width: auto; padding: 10px;">
                        <div class="type-vip span3" style="padding: 10px;">
                            <b>
                                <?php if(!is_null($acc->getTypeVIP())): ?>
                                    <?php if($acc->getTypeVIP() == 0): ?>
                                        <?php echo "Mini Vip";?>
                                        <?php echo "</b>"; ?>
                                    <?php else: ?>
                                        <?php echo "Vip"; ?>
                                    <?php endif ?>
                                <?php else: ?>
                                    <?php echo "Thường"; ?>
                                <?php endif; ?>
                            </b>
                        </div>
                        <div class="category-mini-vip span3" style="padding: 10px;">
                            <?php if(!is_null($acc->getTypeVIP())): ?>
                                <?php if(!is_null($acc->getCategoryVIP())): ?>
                                    <?php echo $acc->getCategoryVIP()->getTitle(); ?>
                                <?php else: ?>
                                    <?php echo "NULL"; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php echo "Lĩnh vực"; ?>
                            <?php endif; ?>
                        </div>
                        <div class="expired-vip span2" style="padding: 10px;">
                            <?php $now = new \DateTime('now'); ?>
                            <?php if(!is_null($acc->getExpiredDateVip())): ?>
                                <?php if(JobCalculator::ago($acc->getExpiredDateVip(), $now) == false): ?>
                                    <?php echo '<span style="color: red;">' . date_format($acc->getExpiredDateVip(), 'd-m-Y H:i:s') . '</span>'; ?>
                                <?php else: ?>
                                    <?php echo date_format($acc->getExpiredDateVip(), 'd-m-Y H:i:s'); ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php echo "Thời hạn VIP"; ?>
                            <?php endif; ?>
                        </div>
                        <div class="refund-vip span2" style="padding: 10px;">
                            <label>
                                <input type="checkbox" id="refund" name="refund" value="1" style="margin-top: -2px;"> Hoàn tiền
                            </label>
                        </div>
                        <div class="button-cancel-submit span2">
                            <button id="btn-vip-cancel-submit" class="btn btn-large" style="padding-left: 35px; padding-right: 35px;" 
                                <?php if(!is_null($acc->getTypeVIP())): ?>
                                    <?php if(!is_null($acc->getExpiredDateVip())): ?>
                                        <?php if(JobCalculator::ago($acc->getExpiredDateVip(), $now) == false): ?>
                                            <?php echo "disabled"; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php echo "disabled"; ?>
                                <?php endif; ?>
                            >Hủy</button>
                            <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</div>

<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#profile--listvip">
            Lịch sử nâng cấp tài khoản
        </a>
    </div>
    <div id="profile--listvip" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Gói</th>
                                <th class="text-right">Số tiền</th>
                                <th>Hình thức TT</th>
                                <th>Ngày</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vips as $vip): ?>
                            <tr>
                                <td><?php echo $vip->getId();?></td>
                                <td><?php echo $vip->getPackageText(); ?></td>
                                <td class="text-right"><?php echo number_format($vip->getPrice(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance')?></td>
                                <td>[<?php echo $vip->getPaymentMethod(); ?>] <?php
                                    if(!is_null($vip->getPaymentMethod())){
                                        echo $vip->getPaymentMethodText();
                                    }
                                ?></td>
                                <td><?php echo date_format($vip->getCreatedAt(), "d/m/Y h:i:s")?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-vip-submit').on('click', function(){
            $('.button-submit').addClass('loading');
            $('#btn-vip-submit').css('display', 'none');
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_upgrade_code", array()); ?>",
                type: "POST",
                data: {
                    account: <?php echo $acc->getId(); ?>,
                    package: $('#package-vip').val(),
                    method: $('#payment-method').val(),
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label style="color: red;">Đã có lỗi xảy ra</label>').insertAfter('#id-account');
                        $('.button-submit.loading').removeClass('loading');
                        $('#btn-vip-submit').css('display', 'block');
                    } else {
                        $('.button-submit.loading').removeClass('loading');
                        $('#btn-vip-submit').css('display', 'block');
                        $('.expired-vip-update').html('<b>' + result.time + '</b>');
                        
                        if(result.type !== 'undefined'){
                            if(result.type == 0){
                                $('.type-vip').html('<b>Mini Vip</b>');
                            } else if(result.type == 1){
                                $('.type-vip').html('<b>Vip</b>');
                            }
                        }
                    }
                }
            });
        });
        
        $('#btn-vip-cancel-submit').on('click', function(){
            var isRefund = 'not';
            if(document.getElementById('refund').checked == true){
                isRefund = 'refund';
            }
        
            $('.button-cancel-submit').addClass('loading');
            $('#btn-vip-cancel-submit').css('display', 'none');
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_upgrade_cancel", array()); ?>",
                type: "POST",
                data: {
                    account: <?php echo $acc->getId(); ?>,
                    refund: isRefund
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label style="color: red;">Đã có lỗi xảy ra</label>').insertAfter('#id-account-cancel');
                        $('.button-cancel-submit.loading').removeClass('loading');
                        $('#btn-vip-cancel-submit').css('display', 'block');
                    } else {
                        $('.button-cancel-submit.loading').removeClass('loading');
                        $('#btn-vip-cancel-submit').css('display', 'block');
                        if(typeof result.type !== 'undefined'){
                            $('.type-vip').html('<span style="color:' + result.color + '">' + result.type + '</span>');
                        }

                        if(typeof result.category !== 'undefined'){
                            $('.category-mini-vip').html('<span style="color:' + result.color + '">' + result.category + '</span>');
                        }

                        if(typeof result.type !== 'undefined' && typeof result.color !== 'undefined'){
                            $('.expired-vip').html('<span style="color:' + result.color + '">' + result.time + '</span>');
                        }
                    }
                }
            });
        });
    });
</script>