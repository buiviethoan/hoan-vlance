<?php /* @var $acc Vlance\AccountBundle\Entity\Account */ ?>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#verification--verified">
            Các link thường dùng
        </a>
    </div>
    <div id="verification--verified" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid">
                <p>Thông tin liên quan đến xác thực tài khoản:</p>
                <ol>
                    <li><b>Xác thực số điện thoại:</b>
                        <ul style="list-style-type:disc">
                            <li>Số: <?php echo $acc->getTelephone(); ?></li>
                            <li>Tình trạng: <b>
                                <?php if(is_null($acc->getTelephoneVerifiedAt())): ?>
                                    Chưa xác thực
                                <?php else: ?>
                                    <font color="#44f46e"> Đã xác thực</font>
                                <?php endif; ?>
                            </b></li>
                        </ul>
                    </li>
                    <li><b>Xác thực chứng minh nhân dân:</b>
                        <ul style="list-style-type:disc">
                            <li>Số: 
                                <?php if($acc->getPersonId() instanceof Vlance\AccountBundle\Entity\PersonId): ?>
                                    <?php echo $acc->getPersonId()->getIdNumber(); ?>
                                <?php else: ?>
                                    <?php echo "Chưa có"; ?>
                                <?php endif; ?>
                            </li>
                            <li>Tình trạng: 
                                <?php if($acc->getPersonId() instanceof Vlance\AccountBundle\Entity\PersonId): ?>
                                <b>
                                    <?php if(!is_null($acc->getPersonId()->getVerifyAt())):?>
                                        <font color="#44f46e"><?php echo "Đã xác thực"; ?></font>
                                    <?php else: ?>
                                        <?php echo "Chưa xác thực"; ?>
                                    <?php endif; ?>
                                </b> - <a href="<?php echo $view['router']->generate('Vlance_AccountBundle_AdminPersonId_show',array('pk' => $acc->getPersonId()->getId()))?>" accesskey=""target="_blank" class="btn">Xem thông tin</a>
                                <?php else: ?>
                                    <?php echo "Chưa có"; ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </li>
                    <li><b>Xác thực mã số thuế:</b>
                        <ul style="list-style-type:disc">
                            <li>Số: 
                                <?php if($acc->getTaxId() instanceof Vlance\AccountBundle\Entity\TaxId): ?>
                                    <?php echo $acc->getTaxId()->getIdNumber(); ?>
                                <?php else: ?>
                                    <?php echo "Chưa có"; ?>
                                <?php endif; ?>
                            </li>
                            <li>Tình trạng:
                                <?php if($acc->getTaxId() instanceof Vlance\AccountBundle\Entity\TaxId): ?>
                                <b>
                                    <?php if(!is_null($acc->getTaxId()->getVerifiedAt())):?>
                                        <font color="#44f46e"><?php echo "Đã xác thực"; ?></font>
                                    <?php else: ?>
                                        <?php echo "Chưa xác thực"; ?>
                                    <?php endif; ?>
                                </b> - <a href="<?php echo $view['router']->generate('Vlance_AccountBundle_AdminTaxId_show',array('pk' => $acc->getTaxId()->getId()))?>" accesskey=""target="_blank" class="btn">Xem thông tin</a>
                                <?php else: ?>
                                    <?php echo "Chưa có"; ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>