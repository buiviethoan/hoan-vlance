<?php
namespace Vlance\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vlance\PaymentBundle\Entity\UserCash;

use Vlance\AccountBundle\Entity\Account;
use Aws\Ses\SesClient;

class AccountCommand extends ContainerAwareCommand {
    protected function configure() {
        $this
                ->setName('vlance:account')
                ->setDescription('Vlance command for account')
                ->addArgument('action', InputArgument::REQUIRED, 'Action that you want to execute')
                ->addOption('test', NULL, InputOption::VALUE_NONE, "Run test for this command")
                // php app\console vlance:account --day=7
                ->addOption('days', NULL, InputOption::VALUE_NONE, "Number of days.")
                ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'initializeAccount' :
                $output->writeln('================= Start creating system account');
                $this->initializeAccount($output);
                $output->writeln('================= Finished create system account');
                break;
            case 'cash' :
                $output->writeln('================= Create cash missing for user');
                $this->cash($output);
                $output->writeln('================= Finished');
                break;
            case 'reset' :
                $output->writeln('================= Reset password for all account to use in local');
                $this->reset($output);
                $output->writeln('================= Finished');
                break;
            /**
             * Status: Obsoleted on 09/05/2016
             */
            /*case 'certificate-newsletter' :
                $output->writeln('================= Send newsletter to user');
                $this->certificateNewsletter($input, $output);
                $output->writeln('================= Finished');
                break;*/
            case 'countdeposit' :
                $output->writeln('================= Start count number job deposit');
                $this->countJobDeposit($output);
                $output->writeln('================= Finished count number job deposit');
                break;
            default:
                break;
        }
    }
    
    /**
     * Status: Obsoleted on 09/05/2016
     */
    protected function certificateNewsletter(InputInterface $input, OutputInterface $output) {
        if ($input->getOption('test')) {
            $output->writeln("test for send newsletter");
            $template = "VlanceAccountBundle:Email:certificate_newsletter.html.twig";
            $this->getContainer()->get('router')->getContext()->setHost('www.vlance.vn');
            $toAsddresses = array('Tuân Trần Ngọc <tuan.tran@vlance.vn>');
            $result = $this->getContainer()->get('vlance.mailer')->sendBatchMessage($template, array(), $toAsddresses);
            var_dump($result);
            return;
        }
        //read sql file
        $path = __DIR__ . "/../../../../sql/certificate.sql";
        if (!file_exists($path)) {
            $output->writeln("Check file certificate.sql in sql folder");
            return;
        }
        $sql = file_get_contents($path);
        $output->writeln("Excute sql in certificate.sql");
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->getConnection()->exec($sql);
        $output->writeln("Sql excuted");
        $this->getContainer()->get('router')->getContext()->setHost('www.vlance.vn');
//        $accounts = $em->getRepository('VlanceAccountBundle:Account')->findByNotify(Account::CAN_NOTIFY);
        $fields = array('a.id', 'a.email', 'a.fullName');
        $conditions = array(
            'enabled'   => array('eq', 1),
            'notify'    => array('eq', Account::CAN_NOTIFY),
            'newFeature'    => array('eq', Account::WILL_NOTIFY_NEW_FEATURE),
        );
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->getFields($fields, $conditions);
        $template = "VlanceAccountBundle:Email:certificate_newsletter.html.twig";
        /**
         * @todo This code should be moved to mailer service
         */
        $size = count($accounts);
        $pages = ceil($size / 45);
        $sentIds = array();
        $total = 0;
        $output->writeln("Found {$size} accounts need to send email");
        for ($i = 0; $i < $pages; $i++) {
            $start = $i * 45;
            $end = min($start + 45, $size);
            $toAsddresses = array();
            for ($j = $start; $j < $end; $j++) {
                $sentIds[] = $accounts[$j]['id'];
                if (strstr($accounts[$j]['email'], '@facebook.com') == false &&
                        filter_var($accounts[$j]['email'], FILTER_VALIDATE_EMAIL)) {
                    $toAsddresses[] = $accounts[$j]['email'];
                } else {
                    $output->writeln($accounts[$j]['email'] . " is not valid");
                }
            }
            $end--;
            $number = count($toAsddresses);
            $total += $number;
            $output->writeln("send email {$number} from {$start} to {$end}");
            /**
             * @todo Uncomment this code to send email
             */
            $result = $this->getContainer()->get('vlance.mailer')->sendBatchMessage($template, array(), $toAsddresses);
            if ($result instanceof Aws\Ses\Exception\SesException) {
                print_r($result);
            }
            $output->writeln("sent email, sleep 6s to avoid limited sending rate (10emails/s");
            sleep(6);
        }
        $sentIdsStr = implode(",", $sentIds);
        $output->writeln("Sent total {$total} emails");
        $output->writeln("Update new_feature to 0");
        $sql = "UPDATE `account` SET `account`.`new_feature` = 0 WHERE `account`.`id` IN ({$sentIdsStr})";
        $em->getConnection()->exec($sql);
        $output->writeln("Sql excuted");
    }
    
    protected function reset(OutputInterface $output) {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->findAll();
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        foreach ($accounts as $account) {
            $output->writeln('Account: ' . $account->getFullName());
            //$account->setPlainPassword('abc123'); // Comment to avoid dangerous
            $em->persist($account);
        }
        $em->flush();
    }


    protected function cash(OutputInterface $output) {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->findByMissingCash();
        $output->writeln("Found " . count($accounts) . " users who don't have cash");
        foreach ($accounts as $account) {
            $userCash = new UserCash();
            $userCash->setAccount($account);
            $userCash->setBalance(0);
            $em->persist($userCash);
        }
        $em->flush();
    }
    
    protected function initializeAccount(OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $output->writeln('+ Create system account');
        $sysAccount = new Account();
        $sysAccount->setFullName('Vlance system');
        $sysAccount->setUsername('vlance_system_user');
        $sysAccount->setUsernameCanonical('vlance_system_user');
        $sysAccount->setEmail('system_user@vlance.vn');
        $sysAccount->setEmailCanonical('system_user@vlance.vn');
        $sysAccount->setType(Account::TYPE_VLANCE_SYSTEM);
        $sysAccount->addRole(Account::ROLE_VLANCE_SYSTEM);
        $sysAccount->setPlainPassword('system.vlance.vn');
        $em->persist($sysAccount);
        $output->writeln('- Create system account successfully!!!!');
        
        $output->writeln('+ Create admin account');
        $adminAccount = new Account();
        $adminAccount->setFullName('Vlance admin');
        $adminAccount->setUsername('vlance_admin_user');
        $adminAccount->setUsernameCanonical('vlance_admin_user');
        $adminAccount->setEmail('admin_user@vlance.vn');
        $adminAccount->setEmailCanonical('admin_user@vlance.vn');
        $adminAccount->setType(Account::TYPE_VLANCE_SYSTEM);
        $adminAccount->addRole(Account::ROLE_SUPER_ADMIN);
        $adminAccount->addRole(Account::ROLE_VLANCE_SUPER_ADMIN);
        $adminAccount->setPlainPassword('admin.vlance.vn');
        $adminAccount->setEnabled(1);
        $em->persist($adminAccount);
        $output->writeln('- Create admin account successfully!!!!');
        
        $em->flush();
        
        $output->writeln('+ Create user account');
        $sql = "INSERT INTO `account` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `type`, `roles`, `enabled`) VALUES (1000, 'vlance_user', 'vlance_user', 'user@vlance.vn', 'user@vlance.vn', 'vlance_system', 'a:1:{i:0;s:16:\"ROLE_VLANCE_USER\"}', 0);";
        $em->getConnection()->exec($sql);
        $output->writeln('- Create user account successfully!!!!');
        
        $output->writeln('+ remove user account');
        $sql = "DELETE FROM `account` WHERE `id`=1000;";
        $em->getConnection()->exec($sql);
        $output->writeln('- Remove user account successfully!!!!');
    }
    
    protected function countJobDeposit(OutputInterface $output) {
       /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /* @var $queryBuilder Doctrine\ORM\QueryBuilder */
//        $queryBuilder = $this->createQueryBuilder('a');
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('acc')->from('VlanceAccountBundle:Account','acc');
        $queryBuilder->where('acc.numJob > 0');
                
//      echo $queryBuilder->getQuery()->getDQL();die;
        $accounts = $queryBuilder->getQuery()->getResult();
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        foreach($accounts as $account) {
            $jobs = $account->getJobs();
            /* @var $job \Vlance\JobBundle\Entity\Job */
            $count = 0;
            foreach($jobs as $job) {
                if($job->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::ESCROWED || $job->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::PAIED) {
                    $count = $count + 1;
                }
            }
            $account->setNumJobDeposit($count);
            $em->persist($account);
        }
        $em->flush();
    }
}
