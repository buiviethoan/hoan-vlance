<?php
namespace Vlance\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFeaturedFreelancersCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
            ->setName('vlance:job:featuredfreelancers')
            ->setDescription('Vlance command for update featured freelancers');
    }
    
    /* Remove job's featuredUntil if the date is passed */
    protected function execute(InputInterface $input, OutputInterface $output) {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $currentDay = date('Y-m-d h:i:s', time());
        $accountConditions['featuredUntil'] = array('lt', "'$currentDay'");
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->getFields("a", $accountConditions);
        
        foreach($accounts as $a){
            $a->setFeaturedUntil(null);
            $em->persist($a);
            $em->flush();
        }
    }
}
