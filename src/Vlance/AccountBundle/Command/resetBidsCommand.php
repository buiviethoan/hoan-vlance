<?php
namespace Vlance\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Vlance\AccountBundle\Entity\Account;

class ResetBidsCommand extends ContainerAwareCommand {
     protected function configure() {
        $this
                ->setName('vlance:account:resetbids')
                ->setDescription('Vlance command for reset number bid on mounth')
                ->addArgument('action', InputArgument::REQUIRED, 'Action that you want to execute')
                ;
    }
    protected function execute(InputInterface $input, OutputInterface $output) {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'resetBids' :
                $output->writeln('================= Start reset number bids account');
                $this->resetBids($output);
                $output->writeln('================= Finished reset number bids account');
                break;
            default:
                break;
        }
    }
    
    protected function resetBids(OutputInterface $output) {
       
        $curr_month = date('m', time());
        $curr_day = date('d', time());
        $curr_year = date('Y', time());
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        //Lay ra account
        $queryBuilder = $em->createQueryBuilder()
                ->select('acc')
                ->from('Vlance\AccountBundle\Entity\Account', 'acc')
                ->where('DAY(acc.createdAt) = :curr_day')
                ;
        $queryBuilder->setParameter('curr_day', $curr_day);
//      echo $queryBuilder->getQuery()->getDQL();die;
        $accounts = $queryBuilder->getQuery()->getResult();

        //TH: nam nhuan
        $currentDay = date('d/m', time());
        $queryBuilder_2 = $em->createQueryBuilder()
                ->select('a')
                ->from('Vlance\AccountBundle\Entity\Account', 'a')
                ->where("Mod(YEAR(a.createdAt),4) = 0")
                ->andWhere("DAY(a.createdAt) = 29")
                ->andWhere("MONTH(a.createdAt) = 02")
                ->andWhere("{$currentDay} = 01/03")
                ->andwhere("Mod({$curr_year},4) != 0")
                ;
        $acc = $queryBuilder_2->getQuery()->getResult();   
        
        if(count($accounts) > 0 || count($acc) > 0) {
            foreach ($accounts as $account) {
                $account->setNumCurrBids($account->getMonthlyBids());
                $em->persist($account);
            }
            foreach ($acc as $user) {
                $user->setNumCurrBids($user->getMonthlyBids());
                $em->persist($user);
            }  
            $em->flush();
        }
    }

}
