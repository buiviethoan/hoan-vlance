<?php
namespace Vlance\AccountBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Vlance\BaseBundle\Command\BaseCommand as BaseCommand;
use Vlance\BaseBundle\Entity\MassEmail;
use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\AccountRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * CRON FREQUENCY:  5 mins
 */
class SuggestionInvitationEmailCommand extends BaseCommand {
    protected function config() 
    {
        $this->setName('vlance:account:suggestioninvitationemail')
             ->setDescription("Send email invite.");
    }
    
    protected function exec(InputInterface $input, OutputInterface $output) 
    {
        $this->sendSuggestEmail($input, $output);
    }
        
    protected function sendSuggestEmail()
    {
        $em = $this->em;
        $list_new_user = $em->getRepository('VlanceAccountBundle:Account')->getNewAccount();
        
        if(count($list_new_user) == 0){
            $this->log("Nothing happen.");
            return true;
        }
        
        $this->log(count($list_new_user), "Number of enabled new user");
        
        /* @var $user Account */
        foreach($list_new_user as $user){
            // Initialising email template
            $template = 'VlanceAccountBundle:Email/AccountInvite:suggest_invite_email.html.twig';
            
            $tracking_properties = array(
                                // for mixpanel
                                'user_id'   => $user->getId(),
                                // for ga
                                'ec'            => 'suggestmail',     // event category
                                'ea'            => 'open',          // event action
                                'el'            => 'invitoropensuggestemail',    // event label
                                'cs'            => '',     // Campaign source
                                'cm'            => '',         // Campaign medium
                                'cn'            => '',     // Campaign name
                            );
            $mixpanel_tracking  = $this->buildTrackingCodeMixpanel("Invitor Open Suggest Email", $tracking_properties, $user->getId());
                $this->log($mixpanel_tracking);
            $ga_tracking = $this->buildTrackingCodeGa($tracking_properties, $user->getId());
                            $this->log($ga_tracking);
                            
            $context = array(
                'account'       => $user,
                'iurl'          => $this->container->get('router')->generate('email_invite', array('id' => $user->getId()), true),
                'pixel_tracking'=> array(
                                    'mixpanel'  => $mixpanel_tracking,
                                    'ga'        => $ga_tracking
                                    ),
                );
            
            $from = $this->container->getParameter('from_email');
            
            try{
                // Send confirmation mail to payer
                print_r($user->getEmail());
                $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $user->getEmail());
                
                // Send tracking event to Mixpanel
                $this->mixpanel('Invitor Receive Suggest Email', array(), $user->getId());
                // End Mixpanel
            } catch (Exception $ex) {
                return new JsonResponse(array('error' => 101));
            }
        }
        $this->log("Sending completed.");
    }
}