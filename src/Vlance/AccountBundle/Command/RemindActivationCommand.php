<?php
namespace Vlance\AccountBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Vlance\BaseBundle\Command\BaseCommand;
use Vlance\AccountBundle\Entity\Account;

/**
 * CRON FREQUENCY:  1 once a month
 */
class RemindActivationCommand extends BaseCommand {
    protected function config() 
    {
        $this->setName('vlance:account:remind_activate_account')
            ->addOption('action', NULL, InputOption::VALUE_NONE, "Action that you want to execute")
            ->setDescription("Remind user to activate his account after registration.");
    }
    
    protected function exec(InputInterface $input, OutputInterface $output) 
    {
        $action = $input->getOption('action');
        if($action){
            switch ($action) {
                case "firstRemind":
                    $this->firstRemindEmail();
                    break;
            }
        }
        else{
            $this->firstRemindEmail();
        }
        
    }
        
    protected function firstRemindEmail()
    {
        $this->log("Start firstRemindEmail");
        $accounts = $this->em->getRepository('VlanceAccountBundle:Account')->findBy(array('enabled' => 0, 'locked' => 0));
        if(count($accounts) == 0){
            $this->log("There is 0 account.");
            return true;
        }
        
        $this->log(count($accounts), "Number of unactivated accounts");
        
        $tracking_properties = array(
            // for mixpanel
            'campaign_id'   => 'remind-email-1',
            // for ga
            'ec'            => 'remind_activate_account',   // event category
            'ea'            => 'open email',                // event action
            'el'            => 'email 1',                   // event label
            'cs'            => 'remind_activate_account',   // Campaign source
            'cm'            => 'email',                     // Campaign medium
            'cn'            => 'remind-email-1'             // Campaign name
        );
        $template = 'VlanceAccountBundle:Email/Command/RemindActivation:email_active.html.twig';
        $from = $this->container->getParameter('from_email');
        
        foreach($accounts as $account){
            $this->log("#" . $account->getId());
            if(!is_null($account->getConfirmationToken())){
                $mixpanel_tracking  = $this->buildTrackingCodeMixpanel("Reminder Activate Account Open", $tracking_properties, $account->getId());
                $this->log($mixpanel_tracking);
                $ga_tracking = $this->buildTrackingCodeGa($tracking_properties, $account->getId());
                $this->log($ga_tracking);

                $context = array(
                    'account'           => $account,
                    'pixel_tracking'    => array(
                        'mixpanel'  => $mixpanel_tracking,
                        'ga'        => $ga_tracking
                    )
                );

                // Set receipt email then send email
                if($this->isTestMode()){
                    $to = "tuan.tran@vlance.vn";
                } else {
                    $to = $account->getEmail();
                }

                try {
                    $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
                    if($this->isTestMode()){
                        break;
                    }
                } catch (Exception $ex) {
                    $this->log($ex);
                }
            }
        }
        $this->log("Sending firstRemindEmail completed.");
    }
}