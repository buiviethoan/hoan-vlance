<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobType extends AbstractType {
    protected $_container;
            
    public function __construct(ContainerInterface $container) {
        $this->_container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $service_list = $this->_container->get('vlance_account.helper')->getServiceList();
        $builder
            ->add('category','entity',array(
                'label' => 'form.job.category.label',
                'class' => 'VlanceAccountBundle:Category',
                'choices' => $this->_container->get('vlance_account.helper')->getCategoryList(),
                'translation_domain' => 'vlance',
                'empty_value' => 'form.job.category.empty_value',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job.category.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.category.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('service','entity',array(
                'label' => 'form.job.services.label',
                'class' => 'VlanceAccountBundle:Service',
                'choices' => $service_list,
                'translation_domain' => 'vlance',
                'empty_value' => 'form.job.services.empty_value',
                'wrapper_class' => 'service_wrapper hidejs',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.job.services.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.services.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('title', null, array(
                'label' => 'form.job.title.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job.title.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.popover.title',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('description', null, array(
                'label' => 'form.job.description.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job.description.placeholder_2',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.description.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12',
                    'rows' => '14')))
            ->add('files', 'multiupload', array(
                'type' => new JobFileType(),
                'label' => 'form.job.attachment',
                'translation_domain' => 'vlance',
                'wrapper_class' => 'fileupload_wrapper',
                'required' => false,
                'by_reference' => false,
                'options' => array(
                    'label' => false
                )
                ))
            ->add('city', 'entity', array(
                'label' => 'form.job.city.label',
                'translation_domain' => 'vlance',
                'class' => 'VlanceBaseBundle:City',
                'choices' => $this->_container->get('vlance_base.helper')->getCityList(),
                'empty_value' => 'form.job.city.empty_value',
                'required' => true,
                // 'wrapper_class' => 'wrapper',
                'attr' => array(
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.popover.city',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'                        
                    )))
            ->add('budget', 'number', array(
                'label' => 'form.job.budget.label',
                'translation_domain' => 'vlance',
                'append' => 'site.append.currency',
                'wrapper_class' => 'wrapper append-white combo-row-1st span6 budget',
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'placeholder' => 'form.job.budget.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.budget.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )))
            ->add('closeAt', 'datepicker', array(
                'label' => 'form.job.closed_at.label',
                'format' => 'd/M/y',
                'translation_domain' => 'vlance',
                'append' => 'site.append.date_icon',
                'wrapper_class' => 'closeat wrapper append-white',
                'start_date' => date('d/m/Y', time() + 3600*24*1),
                'end_date' => date('d/m/Y', time() + 3600*24*30),
                'attr' => array(
                    'complex_attr' => array(
                        'name' => 'placeholder',
                        'value' => 'form.job.closed_at.placeholder',
                        '%var%' => date("d/m/Y", (time() + 3600*24*30)) // 30 days later
                    ),
                    'title' => false,
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.closed_at.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12')))
            ->add('isPrivate', 'checkbox', array(
                'label'                 => 'form.job.is_private.explication_2',
                'translation_domain'    => 'vlance',
                'required'              => false))
            ->add('requireEscrow', 'checkbox', array(
                'label'                 => 'form.job.require_escrow',
                'translation_domain'    => 'vlance',
                'required'              => false)
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Job',
            'cascade_validation' => true,
        ));
        

    }

    public function getName() {
        return 'vlance_jobbundle_jobtype';
    }

}
