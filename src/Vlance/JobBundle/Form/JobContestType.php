<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobContestType extends AbstractType {
    protected $_container;
            
    public function __construct(ContainerInterface $container) {
        $this->_container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $service_list = $this->_container->get('vlance_account.helper')->getServiceContest();
        $builder
            ->add('category','entity',array(
                'label' => 'form.job_contest.category.label',
                'class' => 'VlanceAccountBundle:Category',
                'choices' => $this->_container->get('vlance_account.helper')->getCategoryContest(),
                'translation_domain' => 'vlance',
                'empty_value' => 'form.job.category.empty_value',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job.category.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.category.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('service','entity',array(
                'label' => 'form.job.services.label',
                'class' => 'VlanceAccountBundle:Service',
                'choices' => $service_list,
                'translation_domain' => 'vlance',
                'empty_value' => 'form.job.services.empty_value',
                'wrapper_class' => 'service_wrapper hidejs',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.job.services.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.services.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('title', null, array(
                'label' => 'form.job_contest.type.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job_contest.title.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'VD: Thiết kế logo',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('description', null, array(
                'label' => 'form.job.description.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job_contest.description.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12',
                    'rows' => '14')))
            ->add('files', 'multiupload', array(
                'type' => new JobFileType(),
                'label' => 'form.job.attachment',
                'translation_domain' => 'vlance',
                'wrapper_class' => 'fileupload_wrapper',
                'required' => false,
                'by_reference' => false,
                'options' => array(
                    'label' => false
                )
            ))
            ->add('budget', 'number', array(
                'label' => 'form.job.budget.label',
                'translation_domain' => 'vlance',
                'append' => 'site.append.currency',
                'wrapper_class' => 'wrapper append-white combo-row-1st span6 budget',
                'required' => false,
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'placeholder' => 'form.job.budget.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.budget.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )))
            ->add('isPrivateContest', 'checkbox', array(
            'label'                 => 'form.job.is_private.explication_2',
            'translation_domain'    => 'vlance',
            'required'              => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Job',
            'cascade_validation' => true,
        ));
    }

    public function getName() {
        return 'vlance_jobbundle_jobcontesttype';
    }

}
