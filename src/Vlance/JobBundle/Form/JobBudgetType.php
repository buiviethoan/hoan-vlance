<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class JobBudgetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('budget', null, array(
                'required' => true,
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'placeholder' => 'job.update.budget.placeholder',
                )))
            ->add('save', 'submit', array(
                'label' => 'job.update.budget.save',
                'translation_domain' => 'vlance',
                'attr' => array(
                    'class' => 'btn btn-large btn-primary fb-budget'
                )
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => 'vlance_job_budget',
        ));
    }

    public function getName()
    {
        return 'vlance_jobbundle_jobfeedbacktype';
    }
}
