<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BidType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'form.bid.description_placeholder',
                    'class' => 'form-textarea',
                    'data-placement' => 'right',
                    'data-trigger' => 'focus',
                    'data-toggle' => 'popover',
                    'data-content' => 'form.bid.description_placeholder',
                    'data-title' => 'form.bid.description'
                )))
            ->add('introDescription', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'form.bid.intro_description_placeholder',
                    'class' => 'form-textarea',
                    'data-placement' => 'right',
                    'data-trigger' => 'focus',
                    'data-toggle' => 'popover',
                    'data-content' => 'form.bid.intro_description_placeholder',
                    'data-title' => 'form.bid.intro_description'
                )))
            ->add('amount','number', array(
                'label' => 'form.bid.remuneration', 
                'translation_domain' => 'vlance',
                'append' => 'site.append.currency',
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'class' => 'span9 popovers-input',
                    'data-toggle'=> "popover",
                    'data-placement'=>"top",
                    'data-trigger'=>"focus",
                    'autocomplete' => 'off'
                )))
            ->add('duration', 'choice', array(
                'choices' => array('1' => '1 Ngày', '2' => '2 Ngày', '3' => '3 Ngày', '5' => '5 Ngày', '7' => '7 Ngày', '10' => '10 Ngày', '14' => '2 Tuần', '21' => '3 Tuần', '28' => '4 Tuần', '42' => '6 Tuần'),
                'data' => 3,
                'label' =>  'form.bid.duration',
                'translation_domain' => 'vlance',
                'append' => 'site.append.days',
                'empty_value' => null,
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'form.bid.duration_placeholder',
                    'class' => 'row-fluid span12')))
            ->add('files', 'multiupload', array(
                'type' => new BidFileType(),
                'label' => 'form.job.attachment',
                'translation_domain' => 'vlance',
                'wrapper_class' => 'fileupload_wrapper',
                'required' => false,
                'by_reference' => false,
                'options' => array(
                    'label' => false
                )))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Bid'
        ));
    }

    public function getName()
    {
        return 'vlance_jobbundle_bidtype';
    }
}
