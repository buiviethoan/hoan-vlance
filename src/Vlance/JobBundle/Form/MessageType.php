<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content')
            ->add('content', 'textarea', array(
                    'translation_domain'=> 'vlance',
                    'label'             => 'form.job.messenger',
                    'wrapper_class'     => 'row-fluid',
                    'attr'              => array(
                        "class" => "span12",
                    )
                ))
            ->add('files', 'multiupload', array(
                    'type'              => new MessageFileType(),
                    'label'             => 'form.job.attachment',
                    'translation_domain'=> 'vlance',
                    'wrapper_class'     => 'fileupload_wrapper',
                    'required'          => false,
                    'by_reference'      => false,
                    'options'           => array(
                        'label'     => false,
                        'required'  => false
                    )
               ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Message'
        ));
    }

    public function getName()
    {
        return 'vlance_jobbundle_messagetype';
    }
}
