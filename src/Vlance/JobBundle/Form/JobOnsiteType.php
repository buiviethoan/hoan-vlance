<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobOnsiteType extends AbstractType {
    protected $_container;
            
    public function __construct(ContainerInterface $container) {
        $this->_container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('category','entity',array(
                'label' => 'Lĩnh vực',
                'class' => 'VlanceAccountBundle:Category',
                'choices' => $this->_container->get('vlance_account.helper')->getCategoryList(),
                'translation_domain' => 'vlance',
                'empty_value' => 'form.job.category.empty_value',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'form.job.category.placeholder',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.category.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('title', null, array(
                'label' => 'Tên vị trí cần tuyển dụng',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'VD: Lập trình Web',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.popover.title',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('description', null, array(
                'label' => 'Thông tin chi tiết',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => '',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.description.placeholder',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12',
                    'rows' => '14')))
            ->add('files', 'multiupload', array(
                'type' => new JobFileType(),
                'label' => 'form.job.attachment',
                'translation_domain' => 'vlance',
                'wrapper_class' => 'fileupload_wrapper',
                'required' => false,
                'by_reference' => false,
                'options' => array(
                    'label' => false
                )
                ))
            ->add('city', 'entity', array(
                'label' => 'Làm việc tại',
                'translation_domain' => 'vlance',
                'class' => 'VlanceBaseBundle:City',
                'choices' => $this->_container->get('vlance_base.helper')->getCityOnsiteList(),
                'empty_value' => '- Nơi cần tuyển -',
                'required' => true,
                // 'wrapper_class' => 'wrapper',
                'attr' => array(
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.popover.city',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'                        
                    )))
            ->add('budget', 'number', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'append' => 'site.append.currency_onsite',
                'required' => true,
                'wrapper_class' => 'wrapper append-white combo-row-1st span6 budget',
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'placeholder' => 'Tối thiểu: 3.000.000',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.budget.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )))
            ->add('budgetMax', 'number', array(
                'label' => false,
                'translation_domain' => 'vlance',
                'required' => true,
                'append' => 'site.append.currency_onsite',
                'wrapper_class' => 'wrapper append-white combo-row-1st span6 budget',
                'attr' => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'placeholder' => 'Tối đa: 100.000.000',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.budget.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )))
            ->add('startedAt', 'datepicker', array(
                'label' => 'Ngày bắt đầu làm việc',
                'format' => 'd/M/y',
                'required' => true,
                'translation_domain' => 'vlance',
                'append' => 'site.append.date_icon',
                'wrapper_class' => 'closeat wrapper append-white',
                'start_date' => date('d/m/Y', time() + 3600*24*1),
                'end_date' => date('d/m/Y', time() + 3600*24*30*12),
                'attr' => array(
                    'complex_attr' => array(
                        'name' => 'placeholder',
                        'value' => 'form.job.closed_at.placeholder',
                        '%var%' => date("d/m/Y", (time() + 3600*24*30*12)) // 30 days later
                    ),
                    'title' => false,
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.closed_at.popover',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12')))
//            ->add('closeAt', 'datepicker', array(
//                'label' => 'Hạn nhận CV',
//                'format' => 'd/M/y',
//                'translation_domain' => 'vlance',
//                'append' => 'site.append.date_icon',
//                'wrapper_class' => 'closeat wrapper append-white',
//                'start_date' => date('d/m/Y', time() + 3600*24*1),
//                'end_date' => date('d/m/Y', time() + 3600*24*30),
//                'attr' => array(
//                    'complex_attr' => array(
//                        'name' => 'placeholder',
//                        'value' => 'form.job.closed_at.placeholder',
//                        '%var%' => date("d/m/Y", (time() + 3600*24*30)) // 30 days later
//                    ),
//                    'title' => false,
//                    'data-toggle' => 'popover',
//                    'data-placement' => 'top',
//                    'data-content' => 'form.job.closed_at.popover',
//                    'data-trigger' => 'hover',
//                    'class' => 'popovers-input span12')))
            ->add('duration', 'choice', array(
                'choices' => array('1' => '1 Tháng', '2' => '2 Tháng', '3' => '3 Tháng', '4' => '4 Tháng', '5' => '5 Tháng', '6' => '6 Tháng', '7' => '7 Tháng', '8' => '8 Tháng', '9' => '9 Tháng', '10' => '10 Tháng', '11' => '11 Tháng', '12' => '12 Tháng'),
                'data' => 3,
                'label' =>  'Công việc kéo dài trong',
                'translation_domain' => 'vlance',
                'append' => 'site.append.days',
                'empty_value' => null,
                'required' => true,
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'form.bid.duration_placeholder',
                    'class' => 'row-fluid span12')))
            ->add('onsiteRequiredExp', 'choice', array(
                'choices' => array('0' => '< 2 năm', '1' => '2 - 5 năm', '2' => '5 - 10 năm', '3' => '> 10 năm'),
                'data' => 1,
                'label' =>  'Số năm kinh nghiệm',
                'translation_domain' => 'vlance',
                'append' => 'site.append.days',
                'empty_value' => null,
                'required' => true,
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'form.bid.duration_placeholder',
                    'class' => 'row-fluid span12')))
            ->add('onsiteLocation', 'choice', array(
                'choices' => array('0' => 'Làm việc tại văn phòng', '1' => 'Làm online và đến văn phòng khi cần', '2' => 'Làm hoàn toàn online'),
                'data' => 0,
                'label' =>  'Hình thức làm việc',
                'translation_domain' => 'vlance',
                'append' => 'site.append.days',
                'empty_value' => null,
                'required' => true,
                'attr' => array(
                    'pattern' => '[0-9]*',
                    'placeholder' => 'form.bid.duration_placeholder',
                    'class' => 'row-fluid span12')))
            ->add('onsiteCompanyName', null, array(
                'label' => 'Tên công ty',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder' => 'VD: Công ty vLance.vn',
                    'data-toggle' => 'popover',
                    'data-placement' => 'top',
                    'data-content' => 'form.job.popover.title',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input span12'
                    )
                ))
            ->add('file', 'file', array(
                'label' => 'Logo công ty',
                'required' => false,
                'translation_domain' => 'vlance',
                'error_bubbling' => true,
                'attr' => array(
                    'error' => 'profile.edit_profile.error_file'
                )));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Job',
            'cascade_validation' => true,
        ));
    }

    public function getName() {
        return 'vlance_jobbundle_jobonsitetype';
    }

}
