<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BidOnsiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', null, array(
                'label'                 => 'Bạn có muốn gửi tin nhắn tới khách hàng',
                'required'              => true,
                'translation_domain'    => 'vlance',
                'attr' => array(
                    'placeholder'       => 'Gửi tin nhắn ở đây',
                    'class'             => 'form-textarea span12',
                    'data-placement'    => 'right',
                    'data-trigger'      => 'focus',
                    'data-toggle'       => 'popover',
                    'data-content'      => 'form.bid.description_placeholder',
                    'data-title'        => 'form.bid.description'
                )))
            ->add('files', 'multiupload', array(
                'type'                  => new BidFileType(),
                'label'                 => 'form.job.attachment',
                'translation_domain'    => 'vlance',
                'wrapper_class'         => 'fileupload_wrapper',
                'required'              => true,
                'by_reference'          => false,
                'attr' => array(
                    'required_one'      => true,
                ),
                'options' => array(
                    'label'             => false,
                )))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Bid'
        ));
    }

    public function getName()
    {
        return 'vlance_jobbundle_bidonsitetype';
    }
}
