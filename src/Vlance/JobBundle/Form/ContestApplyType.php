<?php

namespace Vlance\JobBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContestApplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('files', 'multiupload', array(
                'type' => new BidFileType(),
                'label' => 'form.job.attachment',
                'translation_domain' => 'vlance',
                'wrapper_class' => 'fileupload_wrapper',
                'required' => false,
                'by_reference' => false,
                'options' => array(
                    'label'     => false
                ),
                'attr'=> array(
                    'singlefile'=> true
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\JobBundle\Entity\Bid'
        ));
    }

    public function getName()
    {
        return 'vlance_jobbundle_contestapplytype';
    }
}
