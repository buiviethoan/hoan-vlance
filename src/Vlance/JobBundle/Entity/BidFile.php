<?php

namespace Vlance\JobBundle\Entity;

use Vlance\BaseBundle\Entity\File as BaseFile;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BidFile
 *
 * @ORM\Table(name="bid_file")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\BidFileRepository")
 */
class BidFile extends BaseFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Bid", inversedBy="files")
     * @ORM\JoinColumn(name="bid_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $bid;
    
    /**
     * Which type this file is uploaded (job, message, porfolio)
     */
    protected $type = 'bid';
    
    public function __toString()
    {
       return $this->path;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bid
     *
     * @param \Vlance\JobBundle\Entity\Bid $bid
     * @return BidFile
     */
    public function setBid(\Vlance\JobBundle\Entity\Bid $bid = null)
    {
        $this->bid = $bid;
    
        return $this;
    }

    /**
     * Get bid
     *
     * @return \Vlance\JobBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }
   
}