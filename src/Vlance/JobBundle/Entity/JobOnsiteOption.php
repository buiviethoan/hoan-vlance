<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobOnsiteOnption
 *
 * @ORM\Table(name="job_onsite_option")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobOnsiteOptionRepository")
 */
class JobOnsiteOption
{
    const ONSITE_OPTION_FEATURE_3DAYS   = 10;
    const ONSITE_OPTION_EMAIL           = 20;
    const ONSITE_OPTION_BANNER          = 30;
    const ONSITE_OPTION_EXTEND_10       = 40;
    const ONSITE_OPTION_EXTEND_30       = 50;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="bids")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $job;

    /**
     * @var integer
     * @ORM\Column(name="onsite_option", type="smallint")
     */
    protected $onsiteOption = self::ONSITE_OPTION_FEATURE_3DAYS;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return JobOnsiteOption
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set onsiteOption
     *
     * @param integer $onsiteOption
     * @return JobOnsiteOption
     */
    public function setOnsiteOption($onsiteOption)
    {
        $this->onsiteOption = $onsiteOption;
    
        return $this;
    }

    /**
     * Get onsiteOption
     *
     * @return integer 
     */
    public function getOnsiteOption()
    {
        return $this->onsiteOption;
    }
}