<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Bid
 *
 * @ORM\Table(name="bid")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\BidRepository")
 * @Assert\Callback(methods={
 *     { "Vlance\JobBundle\Entity\ValidatorFormBid", "isNumBidsValid"}
 * })
 */
class Bid implements \Serializable
{
    const PUBLISH = 1;
    const UNPUBLISH = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="bids")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $job;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="bids")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $account;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @Assert\NotBlank()
     * @var string
     * @ORM\Column(name="intro_description", type="text")
     */
    private $introDescription;
    
    /**
     * @var float
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric", message="The value {{ value }} is not a valid {{ type }}.")
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @Assert\NotBlank()
     * @var integer
     * @Assert\Type(type="numeric", message="The value {{ value }} is not a valid {{ type }}.")
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var \DateTime
     * 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * 
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="awarded_at", type="datetime", nullable=true)
     */
    private $awardedAt = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_declined", type="boolean", options={"default":"0"})
     */
    private $isDeclined = false;
    
    /**
     * Update 20160525: Add for Contest
     * @var type boolean
     *  @ORM\Column(name="is_qualified", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isQualified = false;
    
    /**
     *
     * @var type boolean
     *  @ORM\Column(name="is_violated", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isViolated = false;
    
    /**
     *
     * @var type boolean
     *  @ORM\Column(name="is_featured", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isFeatured = false;
    
    /**
     *
     * @var type boolean
     *  @ORM\Column(name="is_notified", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isNotified = false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="publish_option", type="boolean", options={"default":"1"})
     * @Assert\Type(type="boolean")
     */
    private $publish = true;
    
    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="bid")
     */
    protected $messages;
    
    /**
     * @ORM\OneToOne(targetEntity="Vlance\WorkshopBundle\Entity\Workshop", mappedBy="bid")
     */
    protected $workshop;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\BidFile", mappedBy="bid")
     */
    protected $files;
    
    /**
     * Update 20160610
     * @var \DateTime
     * @ORM\Column(name="won_at", type="datetime", nullable=true)
     */
    protected $wonAt = null;
    
    /**
     * @var string
     * @ORM\Column(name="reason", type="text", nullable=true)
     */
    protected $reason = null;
    
     /**
     * @ORM\OneToOne(targetEntity="Vlance\JobBundle\Entity\BidRate", mappedBy="bid")
     */
    protected $bidRate;

    /**
     * Set bidRate
     *
     * @param \Vlance\JobBundle\Entity\BidRate $bidRate
     * @return Bid
     */
    public function setBidRate(\Vlance\JobBundle\Entity\BidRate $bidRate = null)
    {
        $this->bidRate = $bidRate;
    
        return $this;
    }

    /**
     * Get bidRate
     *
     * @return \Vlance\JobBundle\Entity\BidRate 
     */
    public function getBidRate()
    {
        return $this->bidRate;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bid
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set isViolated
     *
     * @param boolean $isViolated
     * @return Bid
     */
    public function setIsViolated($isViolated)
    {
        $this->isViolated = $isViolated;
    
        return $this;
    }

    /**
     * Get isViolated
     *
     * @return boolean 
     */
    public function getIsViolated()
    {
        return $this->isViolated;
    }
    
    /**
     * Set isFeatured
     *
     * @param boolean $isFeatured
     * @return Bid
     */
    public function setIsFeatured($isFeatured)
    {
        $this->isFeatured = $isFeatured;
    
        return $this;
    }

    /**
     * Get isFeatured
     *
     * @return boolean 
     */
    public function getIsFeatured()
    {
        return $this->isFeatured;
    }
    
    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     * @return Bid
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;
    
        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean 
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Bid
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }
    
    /**
     * Set publish
     *
     * @param integer $publish
     * @return Bid
     */
    public function setPublish($publish) {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer 
     */
    public function getPublish() {
        return $this->publish;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Bid
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Bid
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Bid
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set awardedAt
     *
     * @param \DateTime $awardedAt
     * @return Bid
     */
    public function setAwardedAt($awardedAt)
    {
        $this->awardedAt = $awardedAt;
    
        return $this;
    }

    /**
     * Get awardedAt
     *
     * @return \DateTime 
     */
    public function getAwardedAt()
    {
        return $this->awardedAt;
    }

    /**
     * Set isDeclined
     *
     * @param boolean $isDeclined
     * @return Bid
     */
    public function setIsDeclined($isDeclined)
    {
        $this->isDeclined = $isDeclined;
    
        return $this;
    }

    /**
     * Get isDeclined
     *
     * @return boolean 
     */
    public function getIsDeclined()
    {
        return $this->isDeclined;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return Bid
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return Bid
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
       
    }
    
    /**
     * Add messages
     *
     * @param \Vlance\JobBundle\Entity\Message $messages
     * @return Bid
     */
    public function addMessage(\Vlance\JobBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;
    
        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Vlance\JobBundle\Entity\Message $messages
     */
    public function removeMessage(\Vlance\JobBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Vlance\JobBundle\Entity\Message
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * Get messages by freelancer / bid owner
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessagesByFreelancer()
    {
        $account = $this->getAccount();
        return $this->getMessages()->filter(
            function($entry) use ($account){
                if($entry->getSender() == $account) {
                    return true;
                }
                    return false;
            }
        );
    }
    

    /**
     * Set workshop
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshop
     * @return Bid
     */
    public function setWorkshop(\Vlance\WorkshopBundle\Entity\Workshop $workshop = null)
    {
        $this->workshop = $workshop;
    
        return $this;
    }

    /**
     * Get workshop
     *
     * @return \Vlance\WorkshopBundle\Entity\Workshop 
     */
    public function getWorkshop()
    {
        return $this->workshop;
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }
    
    /**
     * Add files
     *
     * @param \Vlance\JobBundle\Entity\BidFile $files
     * @return Bid
     */
    public function addFile(\Vlance\JobBundle\Entity\BidFile $files)
    {
        $this->files[] = $files;
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \Vlance\JobBundle\Entity\BidFile $files
     */
    public function removeFile(\Vlance\JobBundle\Entity\BidFile $files)
    {
        $this->files->removeElement($files);
    }
    
    public function serialize() {
        $files = $this->getFiles();
        $files_serialize = array();
        if (!empty($files)) {
            foreach($files as $file) {
                $files_serialize[] = $file->serialize();
            }
        }
        return serialize(array(
            'description' => $this->description,
            'amount' => $this->amount,
            'duration' => $this->duration,
            'files' => count($files_serialize) ? serialize($files_serialize) : '',
        ));
    }
    public function unserialize($serialized) {
        $data = unserialize($serialized);
        $this->description = $data['description'];
        $this->amount = $data['amount'];
        $this->duration = $data['duration'];
        $files = unserialize($data['files']);
        if (!empty($files)) {
            foreach ($files as $file_text) {
                $file = new BidFile();
                $this->addFile($file->unserialize($file_text));
            }
        }
        return $this;
    }
    
    public function __toString() {
        return "Bid of {$this->getAccount()->getFullName()} for job {$this->getJob()->getTitle()} ({$this->getJob()->getId()})";
    }

    /**
     * Set introDescription
     *
     * @param string $introDescription
     * @return Bid
     */
    public function setIntroDescription($introDescription)
    {
        $this->introDescription = $introDescription;
    
        return $this;
    }

    /**
     * Get introDescription
     *
     * @return string 
     */
    public function getIntroDescription()
    {
        return $this->introDescription;
    }
    
    /**
     * Get introDescription
     *
     * @return string 
     */
    public function getFullDescription()
    {
        /*$des_arr = array($this->introDescription, $this->description);
        $fullDescription = implode("\n\n", $des_arr);
        return $fullDescription;*/
        if(!$this->introDescription){
            return $this->description;
        } else {
            return $this->introDescription . "\n\n" . $this->description;
        }
    }

    /**
     * Set isQualified
     *
     * @param boolean $isQualified
     * @return Bid
     */
    public function setIsQualified($isQualified)
    {
        $this->isQualified = $isQualified;
    
        return $this;
    }

    /**
     * Get isQualified
     *
     * @return boolean 
     */
    public function getIsQualified()
    {
        return $this->isQualified;
    }

    /**
     * Set wonAt
     *
     * @param \DateTime $wonAt
     * @return Bid
     */
    public function setWonAt($wonAt)
    {
        $this->wonAt = $wonAt;
    
        return $this;
    }

    /**
     * Get wonAt
     *
     * @return \DateTime 
     */
    public function getWonAt()
    {
        return $this->wonAt;
    }

    /**
     * Set reason
     *
     * @param string $reason
     * @return Bid
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
    
        return $this;
    }

    /**
     * Get reason
     *
     * @return string 
     */
    public function getReason()
    {
        return $this->reason;
    }
}