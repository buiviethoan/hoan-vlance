<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\MessageRepository")
 */
class Message
{
    const NEW_MESSAGE = 0;
    const READ_MESSAGE = 1;

        /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bid", inversedBy="messages")
     * @ORM\JoinColumn(name="bid_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $bid;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $sender;
    
     /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="messagesReceiver")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $receiver;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     *
     * @var type boolean
     *  @ORM\Column(name="is_notified", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    private $isNotified = false;
    
     /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\MessageFile", mappedBy="message")
     */
    protected $files;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set bid
     *
     * @param \Vlance\JobBundle\Entity\Bid $bid
     * @return Message
     */
    public function setBid(\Vlance\JobBundle\Entity\Bid $bid = null)
    {
        $this->bid = $bid;
    
        return $this;
    }

    /**
     * Get bid
     *
     * @return \Vlance\JobBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set sender
     *
     * @param \Vlance\AccountBundle\Entity\Account $sender
     * @return Message
     */
    public function setSender(\Vlance\AccountBundle\Entity\Account $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return VlanceAccountBundle\Entity\Account
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \Vlance\AccountBundle\Entity\Account $receiver
     * @return Message
     */
    public function setReceiver(\Vlance\AccountBundle\Entity\Account $receiver = null)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
    
    /**
     * Set status
     *
     * @param integer $status
     * @return Job
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add files
     *
     * @param \Vlance\JobBundle\Entity\MessageFile $files
     * @return Message
     */
    public function addFile(\Vlance\JobBundle\Entity\MessageFile $files)
    {
        $this->files[] = $files;
    
        return $this;
    }

    /**
     * Remove files
     *
     * @param \Vlance\JobBundle\Entity\MessageFile $files
     */
    public function removeFile(\Vlance\JobBundle\Entity\MessageFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }
    /**
     * Set isNotified
     *
     * @param boolean $isNotified
     * @return Message
     */
    public function setIsNotified($isNotified)
    {
        $this->isNotified = $isNotified;
    
        return $this;
    }

    /**
     * Get isNotified
     *
     * @return boolean 
     */
    public function getIsNotified()
    {
        return $this->isNotified;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Message
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}