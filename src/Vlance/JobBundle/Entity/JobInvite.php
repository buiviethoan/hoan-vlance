<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\AccountBundle\Entity\Account;

/**
 * JobInvite
 * 
 * @ORM\Table(name="job_invite")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobInviteRepository")
 */
class JobInvite {

    const INVITE_NEW = 1;
    const INVITE_NOTIFIED = 2;
    const INVITE_BIDDED = 3;
    const INVITE_REFUSED = 4;
    
    const NO_ERROR = 0;
    const ERROR_JOB_NOT_EXIST = 1;
    const ERROR_ACCOUNT_NOT_EXIST = 2;
    const ERROR_INVITE_EXIST = 3;
    const ERROR_NOT_OWNER = 4;
    const ERROR_JOB_EXPRISED = 5;
    const ERROR_MAX_INVITES = 6;
    
    const NO_ERROR_TEXT = 'controller.job.invite.success';
    const ERROR_TEXT_JOB_NOT_EXIST = 'controller.job.invite.notexist.job';
    const ERROR_TEXT_ACCOUNT_NOT_EXIST = 'controller.job.invite.notexist.account';
    const ERROR_TEXT_INVITE_EXIST = 'controller.job.invite.exist';
    const ERROR_TEXT_NOT_OWNER = 'controller.job.invite.notowner';
    const ERROR_TEXT_JOB_EXPRISED = 'controller.job.invite.job.exprised';
    const ERROR_TEXT_MAX_INVITES = 'controller.job.invite.job.maxinvite';
    

    /**
     * @var integer
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", options={"default":"1"})

     */
    protected $status = self::INVITE_NEW;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="invites")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="invites")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $job;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return JobInvite
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return JobInvite
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return JobInvite
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return JobInvite
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return JobInvite
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }
}