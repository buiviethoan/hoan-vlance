<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Persistence\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\Id;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vlance\AccountBundle\Entity\CategoryRepository;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\JobBundle\Entity\Bid;
use Vlance\JobBundle\Entity\Message;
use Vlance\PaymentBundle\Entity\Transaction;

/**
 * Job
 * @ORM\Table(name="job")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Job {

    const JOB_OPEN              = 1;
    const JOB_AWARDED           = 2;
    const JOB_WORKING           = 3;
    const JOB_CLOSED            = 4;
    const JOB_FINISH_REQUEST    = 5;
    const JOB_FINISHED          = 6;
    const JOB_CANCEL_REQUEST    = 7;
    const JOB_CANCELED          = 8;
    const JOB_CONTEST_QUALIFIED = 10; //only Contest
    
    const STATUS_CANCEL_NONE = 0;
    const STATUS_CANCEL_REQUEST = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_CANCELLED_REWORK = 3;
    const STATUS_CANCELLED_DONE_SUCCESS = 4;
    
    const TYPE_BID      = 0;
    const TYPE_CONTEST  = 1;
    const TYPE_ONSITE   = 2;
    
    const ONSITE_LOCATION_OFFICE_FULL       = 0;
    const ONSITE_LOCATION_OFFICE_AND_ONLINE = 1;
    const ONSITE_LOCATION_ONLINE            = 2;
    
    const WAITING_ESCROW    = 0;
    const ESCROWING         = 1;
    const ESCROWED          = 2;
    const PAIED             = 3;
    const REFUNED           = 4;
    
    const PUBLISH   = 1;
    const UNPUBLISH = 0;
    
    const VIOLATED  = 1;
    const VALID     = 1;
    
    const TEST_TYPE_UPFRONT_ESCROW = 1;
    
    const VALID_CONTEST_NO  = 0;
    const VALID_CONTEST_YES = 1;
    
    const EXTEND_CONTEST = 20;

    /********** BASIC FIELDS **********/
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    
    private $description;

    /**
     * @var integer
     * @ORM\Column(name="duration", type="integer", nullable = true)
     */
    private $duration;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Range(
     *                  min = 200,
     *                  max = 2000000000,
     *                  minMessage = "Lớn hơn hoặc bằng 200 VND",
     *                  maxMessage = "Nhỏ hơn hoặc bằng 2000000000 VND"
     *              )
     * @ORM\Column(name="budget", type="integer")
     */
    private $budget;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\BaseBundle\Entity\City", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $city;

    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @Assert\NotBlank()
     * @ORM\Column(name="close_at", type="datetime")
     */
    private $closeAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="consolidated_at", type="datetime", nullable=true)
     */
    private $consolidatedAt;
    
    /**
     * Update 20160525: Added to database
     * @var integer
     * @ORM\Column(name="type", type="smallint")
     */
    private $type = self::TYPE_BID;
    
    /**
     * @var integer
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = self::JOB_OPEN;
    
    /**
     * @var integer
     * @ORM\Column(name="cancel_status", type="smallint")
     */
    private $cancelStatus = self::STATUS_CANCEL_NONE;
    
    /**
     * @var integer
     * @ORM\Column(name="payment_status", type="smallint", options={"default":"0"})
     */
    protected $paymentStatus = self::WAITING_ESCROW;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="jobs")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Category", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\BidRate", mappedBy="job")
     */
    protected $bidRates;

    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Bid", mappedBy="job")
     */
    protected $bids;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\WorkshopBundle\Entity\Workshop", mappedBy="job")
     */
    protected $workshopsJob;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\WorkshopBundle\Entity\FeedBack", mappedBy="job")
     */
    protected $feedBacksJob;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\JobFile", mappedBy="job")
     */
    protected $files;

    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\JobInvite", mappedBy="job")
     */
    protected $invites;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Skill", inversedBy="jobs")
     * @ORM\JoinTable(name="skill_job",
     * joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    private $skills;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Service", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $service;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\JobActivity", mappedBy="job")
     */
    protected $jobActivities;
    
    /********** END BASIC FIELDS **********/

    /********** THESE FIELDS WILL ONLY BE ASSIGNED WHEN THE JOB IS AWARDED **********/
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="jobsWorked")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $worker;

    /**
     * @var \DateTime
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    protected $startedAt = null;
    
    /**
     * Update 20160610
     * @var \DateTime
     * @ORM\Column(name="qualified_at", type="datetime", nullable=true)
     */
    protected $qualifiedAt = null;

    /**
     * @var \DateTime
     * @ORM\Column(name="finished_at", type="datetime", nullable=true)
     */
    protected $finishedAt = null;

    /**
     * @var integer
     * @ORM\Column(name="budget_bided", type="integer", options={"default":"0"})
     */
    protected $budgetBided = 0;

    /**
     * @ORM\OneToOne(targetEntity="Vlance\PaymentBundle\Entity\JobCash", mappedBy="job")
     */
    protected $cash;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\PaymentBundle\Entity\Transaction", mappedBy="job")
     */
    protected $transactions;
    /********** END THESE FIELDS WILL ONLY BE ASSIGNED WHEN THE JOB IS AWARDED **********/
    
    /********** ADDITIONAL OPTIONS FOR JOB, SET BY USER **********/
    /**
     * Require Freelancer to escrow before starting work
     * @var type boolean
     * @ORM\Column(name="require_escrow", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $requireEscrow = false;
    
    /**
     * Job is private or not. Default job is not private
     * @var type boolean
     * @ORM\Column(name="is_private", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isPrivate = false;
    
    /**
     * Contest is private or not. Default contest is not private
     * @var type boolean
     * @ORM\Column(name="contest_private", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isPrivateContest = false;
    
    /**
     * Client want to escrow via vLance
     * @var type boolean
     * @ORM\Column(name="is_will_escrow", type="boolean", options={"default":"1"})
     * @Assert\Type(type="boolean")
     */
    protected $isWillEscrow = true;
    
    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(name="featured_until", type="datetime", nullable=true)
     */
    private $featuredUntil;


    /**
     * @var integer
     * @ORM\Column(name="test_type", type="smallint", nullable=true)
     */
    private $testType;
    
    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(name="upfront_at", type="datetime", nullable=true)
     */
    protected $upfrontAt;
    
    /**
     * @var boolean
     * @ORM\Column(name="featured_fb" , type = "boolean", options={"default":false})
     * @Assert\Type(type="boolean")
     */
    protected $featuredFb = false;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="available_conversation", type="integer", options={"default":"4"})
     */
    private $availableConversation = 4;
    
    /********** END ADDITIONAL OPTIONS FOR JOB, SET BY USER **********/
    
    /********** ADMIN OPTIONS **********/
        
    protected $sendEmail;
    
    protected $resonCancel;
    
    /**
     * @var integer
     * @ORM\Column(name="publish_option", type="smallint")
     */
    private $publish = self::PUBLISH;
    
    /**
     * Update 20160528: Added to database
     * @var integer
     * @ORM\Column(name="valid_contest", type="smallint")
     */
    private $valid = self::VALID_CONTEST_NO;
    
    /**
     * @var type boolean
     * @ORM\Column(name="is_violated", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isViolated = false;
    
    /**
     * Job is reviewed by vLance or not.
     * @var type boolean
     * @ORM\Column(name="is_reviewed", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $isReviewed = false;
    
    /**
     * @var string
     * @ORM\Column(name="review_comment", type="text", nullable=true)
     */
    private $reviewComment;
    /********** END ADMIN OPTIONS **********/
    
    /********** STATISTICS **********/
    /**
     * @var integer
     * 
     * @ORM\Column(name="min_amount", type="integer", options={"default":"0"})
     */
    private $minAmount = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_amount", type="integer", options={"default":"0"})
     */
    private $maxAmount = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="sum_amount", type="integer", options={"default":"0"})
     */
    private $sumAmount = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="sum_duration", type="integer", options={"default":"0"})
     */
    private $sumDuration = 0;
    /********** END STATISTICS **********/
    
    /********** OBSOLETE **********/
    /**
     * @var \DateTime
     *
     * @Assert\Date()
     * @ORM\Column(name="start_date", type="date", nullable = true)
     */
    private $startDate;
    
    /**
     * Number bids be deducted over a job
     * @var integer
     * @ORM\Column(name="num_bids_job", type="integer", options={"default" : "1"})
     */
    protected $numBids = 1;
    
    /********** END OBSOLETE **********/

    /********** ONSITE **********/
    /**
     * @var integer
     * @ORM\Column(name="budget_max", type="integer", nullable=true)
     */
    protected $budgetMax;
    
    /**
     * @var integer
     * @ORM\Column(name="onsite_required_exp", type="integer", nullable=true)
     */
    protected $onsiteRequiredExp = 0;
    
    /**
     * @var string
     * @ORM\Column(name="onsite_company_name", type="string", nullable=true)
     */
    protected $onsiteCompanyName;
    
    /**
     * @var string
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path;
    
    /**
     * @Assert\Image(maxSize ="5120k")
     */
    protected $file = null;
    
    /**
     * @var integer
     * @ORM\Column(name="onsite_location", type="smallint", nullable=true)
     */
    protected $onsiteLocation = self::ONSITE_LOCATION_OFFICE_FULL;
    
    /********** END ONSITE **********/
    
    /**
     * @var integer
     * @ORM\Column(name="max_bid", type="integer", options={"default" : "24"})
     */
    protected $maxBid;

    
    public function setMaxBid($maxBid){
	$this->maxBid = $maxBid;
	
	return $this;
    }
    
    public function getMaxBid(){
	return $this->maxBid;
    }

    /**
     * Get status allowed for job
     * Used when change status for a job - to verify the status is allowed or not
     */
    public static function statusAllowed($payment_status = false) {
        $status = array();
        if ($payment_status) {
            $status[self::WAITING_ESCROW] = 'job.status.payment_waiting_escrow';
            $status[self::ESCROWING] = 'job.status.payment_escrowing';
            $status[self::ESCROWED] = 'job.status.payment_escrowed';
            $status[self::PAIED] = 'job.status.payment_paied';
            $status[self::REFUNED] = 'job.status.payment_refunded';
        } else {
            $status[self::JOB_OPEN] = 'job.status.job_open';
            $status[self::JOB_AWARDED] = 'job.status.job_awarded';
            $status[self::JOB_CLOSED] = 'job.status.job_closed';
            $status[self::JOB_WORKING] = 'job.status.job_working';
            $status[self::JOB_FINISH_REQUEST] = 'job.status.job_finish_request';
            $status[self::JOB_FINISHED] = 'job.status.job_finished';
            $status[self::JOB_CANCEL_REQUEST] = 'job.status.job_cancel_request';
            $status[self::JOB_CANCELED] = 'job.status.job_canceled';
            $status[self::JOB_CONTEST_QUALIFIED] = 'job.status.job_contest_qualified';
        }
        return $status;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Job
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return Job
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Job
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }
    
    /**
     * Set isViolated
     *
     * @param boolean $isViolated
     * @return Bid
     */
    public function setIsViolated($isViolated)
    {
        $this->isViolated = $isViolated;
    
        return $this;
    }

    /**
     * Get isViolated
     *
     * @return boolean 
     */
    public function getIsViolated()
    {
        return $this->isViolated;
    }
    
    /**
     * Set isPrivate
     *
     * @param boolean $isPrivate
     * @return Job
     */
    public function setIsPrivate($isPrivate)
    {
        $this->isPrivate = $isPrivate;
    
        return $this;
    }

    /**
     * Get isPrivate
     *
     * @return boolean 
     */
    public function getIsPrivate()
    {
        return $this->isPrivate;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Job
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Job
     */
    public function setDuration($duration) {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration() {
        return $this->duration;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     * @return Job
     */
    public function setBudget($budget) {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return integer 
     */
    public function getBudget() {
        return $this->budget;
    }

    /**
     * Set closeAt
     *
     * @param \DateTime $closeAt
     * @return Job
     */
    public function setCloseAt($closeAt) {
        $this->closeAt = $closeAt;

        return $this;
    }

    /**
     * Get closeAt
     *
     * @return \DateTime 
     */
    public function getCloseAt() {
        return $this->closeAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Job
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Job
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Job
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus() {
        return $this->status;
    }
    
    /**
     * Set publish
     *
     * @param integer $publish
     * @return Job
     */
    public function setPublish($publish) {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return integer 
     */
    public function getPublish() {
        return $this->publish;
    }
    
    /**
     * Get status in text
     *
     * @return string
     */
    public function getStatusText() {
        $statusText = self::statusAllowed();
        return $statusText[$this->status];
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return Job
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null) {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount() {
        return $this->account;
    }

    /**
     * Set category
     *
     * @param \Vlance\AccountBundle\Entity\Category $category
     * @return Job
     */
    public function setCategory(\Vlance\AccountBundle\Entity\Category $category = null) {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Vlance\AccountBundle\Entity\Category 
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Constructor
     */
    public function __construct() {
        $this->bids = new \Doctrine\Common\Collections\ArrayCollection();
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set city
     *
     * @param \Vlance\BaseBundle\Entity\City $city
     * @return Job
     */
    public function setCity(\Vlance\BaseBundle\Entity\City $city = null) {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \Vlance\BaseBundle\Entity\City 
     */
    public function getCity() {
        return $this->city;
    }

    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Get bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBids() {
        return $this->bids;
    }

    /**
     * Get valid bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBidsValid() {
        $valid_bids = array();
        foreach($this->bids as $b){ /* @var $b Bid */
            if(!($b->getIsViolated())){
                $valid_bids[] = $b;
            }
        }
        return $valid_bids;
    }

    /**
     * Get valid bids
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBidsSuitable() {
        $suitable_bids = array();
        foreach($this->bids as $b){ /* @var $b Bid */
            if($b->getPublish() == true){
                $suitable_bids[] = $b;
            }
        }
        return $suitable_bids;
    }

    /**
     * Add files
     *
     * @param \Vlance\JobBundle\Entity\JobFile $files
     * @return Job
     */
    public function addFile(\Vlance\JobBundle\Entity\JobFile $files) {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Vlance\JobBundle\Entity\JobFile $files
     */
    public function removeFile(\Vlance\JobBundle\Entity\JobFile $files) {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles() {
        return $this->files;
    }


    /**
     * Set minAmount
     *
     * @param integer $minAmount
     * @return Job
     */
    public function setMinAmount($minAmount) {
        $this->minAmount = $minAmount;

        return $this;
    }

    /**
     * Get minAmount
     *
     * @return integer 
     */
    public function getMinAmount() {
        return $this->minAmount;
    }

    /**
     * Set maxAmount
     *
     * @param integer $maxAmount
     * @return Job
     */
    public function setMaxAmount($maxAmount) {
        $this->maxAmount = $maxAmount;

        return $this;
    }

    /**
     * Get maxAmount
     *
     * @return integer 
     */
    public function getMaxAmount() {
        return $this->maxAmount;
    }

    /**
     * Set sumAmount
     *
     * @param integer $sumAmount
     * @return Job
     */
    public function setSumAmount($sumAmount) {
        $this->sumAmount = $sumAmount;

        return $this;
    }

    /**
     * Get sumAmount
     *
     * @return integer 
     */
    public function getSumAmount() {
        return $this->sumAmount;
    }

    /**
     * Set sumDuration
     *
     * @param integer $sumDuration
     * @return Job
     */
    public function setSumDuration($sumDuration) {
        $this->sumDuration = $sumDuration;

        return $this;
    }

    /**
     * Get sumDuration
     *
     * @return integer 
     */
    public function getSumDuration() {
        return $this->sumDuration;
    }

    /**
     * Set startedAt
     *
     * @param \DateTime $startedAt
     * @return Job
     */
    public function setStartedAt($startedAt) {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * Get startedAt
     *
     * @return \DateTime 
     */
    public function getStartedAt() {
        return $this->startedAt;
    }

    /**
     * Set finishedAt
     *
     * @param \DateTime $finishedAt
     * @return Job
     */
    public function setFinishedAt($finishedAt) {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * Get finishedAt
     *
     * @return \DateTime 
     */
    public function getFinishedAt() {
        return $this->finishedAt;
    }

    /**
     * Set budgetBided
     *
     * @param integer $budgetBided
     * @return Job
     */
    public function setBudgetBided($budgetBided) {
        $this->budgetBided = $budgetBided;

        return $this;
    }

    /**
     * Get budgetBided
     *
     * @return integer 
     */
    public function getBudgetBided() {
        return $this->budgetBided;
    }

    /**
     * Set paymentStatus
     *
     * @param integer $paymentStatus
     * @return Job
     */
    public function setPaymentStatus($paymentStatus) {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return integer 
     */
    public function getPaymentStatus() {
        return $this->paymentStatus;
    }
    
    /**
     * Get paymentStatus in text
     *
     * @return string
     */
    public function getPaymentStatusText() {
        $statusText = self::statusAllowed(true);
        return $statusText[$this->paymentStatus];
    }

    /**
     * Set worker
     *
     * @param \Vlance\AccountBundle\Entity\Account $worker
     * @return Job
     */
    public function setWorker(\Vlance\AccountBundle\Entity\Account $worker = null) {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getWorker() {
        return $this->worker;
    }

    public function isOwner(\Vlance\AccountBundle\Entity\Account $acc) {
        return ($this->getAccount() === $acc);
    }
    
    public function isBiddedBy($acc){
        if(count($this->getBids()) == 0)return false;
        foreach($this->getBids() as $bid){
            if($bid->getAccount() == $acc){
                return true;
            }
        }
        return false;
    }

    /**
     * Set cash
     *
     * @param \Vlance\PaymentBundle\Entity\JobCash $cash
     * @return Job
     */
    public function setCash(\Vlance\PaymentBundle\Entity\JobCash $cash = null) {
        $this->cash = $cash;

        return $this;
    }

    /**
     * Get cash
     *
     * @return \Vlance\PaymentBundle\Entity\JobCash 
     */
    public function getCash() {
        return $this->cash;
    }
    
    /**
     * @return boolean $new_messages
     */
    public function getNewMessages($acc) {
       $new_messages = false;
       $bids = $this->getBids();
       foreach($bids as $bid) {
           $messages = $bid->getMessages();
           if(count($messages) > 0) {
               foreach($messages as $message) {
                   if($message->getStatus() == Message::NEW_MESSAGE && $message->getReceiver() == $acc){
                       $new_messages = true;
                       break;
                   }
               }
               
           }
       }
       return $new_messages;
    }

    /**
     * Add workshopsJob
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsJob
     * @return Job
     */
    public function addWorkshopsJob(\Vlance\WorkshopBundle\Entity\Workshop $workshopsJob)
    {
        $this->workshopsJob[] = $workshopsJob;
    
        return $this;
    }

    /**
     * Remove workshopsJob
     *
     * @param \Vlance\WorkshopBundle\Entity\Workshop $workshopsJob
     */
    public function removeWorkshopsJob(\Vlance\WorkshopBundle\Entity\Workshop $workshopsJob)
    {
        $this->workshopsJob->removeElement($workshopsJob);
    }

    /**
     * Get workshopsJob
     *
     * @return \Vlance\WorkshopBundle\Entity\Workshop
     */
    public function getWorkshopsJob()
    {
        return $this->workshopsJob;
    }
    
    /**
     * Add feedBacksJob
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksJob
     * @return Job
     */
    public function addFeedBacksJob(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksJob)
    {
        $this->feedBacksJob[] = $feedBacksJob;
    
        return $this;
    }

    /**
     * Remove feedbacksJob
     *
     * @param \Vlance\WorkshopBundle\Entity\FeedBack $feedBacksJob
     */
    public function removeFeedBacksJob(\Vlance\WorkshopBundle\Entity\FeedBack $feedBacksJob)
    {
        $this->feedBacksJob->removeElement($feedBacksJob);
    }
    
    /**
     * Get feedBacksJob
     *
     * @return \Vlance\WorkshopBundle\Entity\FeedBack
     */
    public function getFeedBacksJob()
    {
        return $this->feedBacksJob;
    }
   
    /**
     * Add skills
     *
     * @param \Vlance\AccountBundle\Entity\Skill $skills
     * @return Job
     */
    public function addSkill(\Vlance\AccountBundle\Entity\Skill $skills)
    {
        $this->skills[] = $skills;
    
        return $this;
    }

    /**
     * Remove skills
     *
     * @param \Vlance\AccountBundle\Entity\Skill $skills
     */
    public function removeSkill(\Vlance\AccountBundle\Entity\Skill $skills)
    {
        $this->skills->removeElement($skills);
    }

    /**
     * Get skills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkills()
    {
        return $this->skills;
    }
     /**
     * Get num bid not deline
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getfilterBids () {
        $bid = new \Vlance\JobBundle\Entity\Bid;
        $num = $this->getBids()->filter(
                    function($entry) {
                        if($entry->getIsDeclined() == 1) {
                            return true;
                        }
                        return false;
                    }
                );
        return count($num);
    }

    /**
     * Add transactions
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactions
     * @return Job
     */
    public function addTransaction(\Vlance\PaymentBundle\Entity\Transaction $transactions)
    {
        $this->transactions[] = $transactions;
    
        return $this;
    }

    /**
     * Remove transactions
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactions
     */
    public function removeTransaction(\Vlance\PaymentBundle\Entity\Transaction $transactions)
    {
        $this->transactions->removeElement($transactions);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Get deposits transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransactionsTypeDeposit()
    {
        $deposit_transactions = array();
        foreach($this->transactions as $t){
            /* @var $t \Vlance\PaymentBundle\Entity\Transaction */
            if($t->getType() == Transaction::TRANS_TYPE_DEPOSIT || $t->getType() == Transaction::TRANS_TYPE_DEPOSIT_UPFRONT){
                $deposit_transactions[] = $t;
            }
        }
        return $deposit_transactions;
    }

    /**
     * Get deposits transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContestTransactions()
    {
        foreach($this->transactions as $t){
            /* @var $t \Vlance\PaymentBundle\Entity\Transaction */
            if($t->getType() == Transaction::TRANS_TYPE_DEPOSIT_UPFRONT){
                $contest_transactions = $t;
            }
        }
        return $contest_transactions;
    }

    /**
     * Get amount funded of DC (upfront) transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFundedAmountDc()
    {
        $total_amount = 0;
        if(count($this->transactions) > 0){
            foreach($this->transactions as $tr){ /* @var $tr Transaction */
                if($tr->getType() == Transaction::TRANS_TYPE_DEPOSIT_UPFRONT && $tr->getStatus() == Transaction::TRANS_TERMINATED){
                    $total_amount += $tr->getAmount();
                }
            }
        }
        return $total_amount;
    }

    /**
     * Get amount funded of VC (normal) transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFundedAmountVc()
    {
        $total_amount = 0;
        if(count($this->transactions) > 0){
            foreach($this->transactions as $tr){ /* @var $tr Transaction */
                if($tr->getType() == Transaction::TRANS_TYPE_DEPOSIT && $tr->getStatus() == Transaction::TRANS_TERMINATED){
                    $total_amount += $tr->getAmount();
                }
            }
        }
        return $total_amount;
    }

    /**
     * Get amount of funded transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFundedAmount()
    {
        return $this->getFundedAmountDc() + $this->getFundedAmountVc();
    }

    /**
     * Add bids
     *
     * @param \Vlance\JobBundle\Entity\Bid $bids
     * @return Job
     */
    public function addBid(\Vlance\JobBundle\Entity\Bid $bids)
    {
        $this->bids[] = $bids;
    
        return $this;
    }

    /**
     * Remove bids
     *
     * @param \Vlance\JobBundle\Entity\Bid $bids
     */
    public function removeBid(\Vlance\JobBundle\Entity\Bid $bids)
    {
        $this->bids->removeElement($bids);
    }
    public function getSendEmail() {
        return $this->sendEmail;
    }

    public function getResonCancel() {
        return $this->resonCancel;
    }

    public function setSendEmail($sendEmail) {
        $this->sendEmail = $sendEmail;
    }

    public function setResonCancel($resonCancel) {
        $this->resonCancel = $resonCancel;
    }
    
    /**
     * Get numBids
     *
     * @return int
     */
    public function getNumBids() {
        return $this->numBids;
    }
    
    /**
     * Set numBids
     *
     * @param int $numBids
     * @return Job
     */
    public function setNumBids($numBids) {
        $this->numBids = $numBids;
    }
    
    /**
     * Set featuredUntil
     *
     * @param \DateTime $featuredUntil
     * @return Job
     */
    public function setFeaturedUntil($featuredUntil) {
        $this->featuredUntil = $featuredUntil;

        return $this;
    }

    /**
     * Get featuredUntil
     *
     * @return \DateTime 
     */
    public function getFeaturedUntil() {
        return $this->featuredUntil;
    }

    /**
     * Get budget in different case
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobBudget($authen = false, $currency = 'đ') {
        $budget = ($this->getBudgetBided() > 0) ? $this->getBudgetBided() : $this->getBudget();
        if($authen === true){
            return number_format($budget,'0',',','.') . " " . $currency;
        } else{
            if($budget < 5000000){
                return "0" . " " . $currency . " - ". number_format(5000000,'0',',','.') . " " . $currency;
            } elseif($budget < 10000000){
                return number_format(5000000,'0',',','.') . " " . $currency . " - " . number_format(10000000,'0',',','.') . " " . $currency;
            } elseif($budget < 20000000){
                return number_format(10000000,'0',',','.') . " " . $currency . " - " . number_format(20000000,'0',',','.') . " " . $currency;
            } elseif($budget < 50000000){
                return number_format(20000000,'0',',','.') . " " . $currency . " - " . number_format(50000000,'0',',','.') . " " . $currency;
            } else {
                return "Trên " . number_format(50000000,'0',',','.') . " " . $currency;
            }
        }
        
    }
    
    /**
     * Get awarded bid
     */
    public function getAwardedBid()
    {
        foreach($this->getBids() as $bid){
            if(!is_null($bid->getAwardedAt())){
                return $bid;
            }
        }
        return null;
    }
    
    /**
     * Get is Open to bid status
     * 
     * @return boolean 
     */
    public function isOpen()
    {
        
        return ($this->getStatus() == self::JOB_OPEN && $this->getCloseAt() > new \DateTime())?true:false;
    }
    
    /**
     * Get is Awarded
     * 
     * @return boolean 
     */
    public function isAwarded()
    {
        
        return ($this->getStatus() == self::JOB_AWARDED) ? true : false;
    }
    
    /**
     * Get is Escrowed status
     * 
     * @return boolean 
     */
    public function isEscrowed()
    {
        return ($this->getPaymentStatus() == self::ESCROWED)?true:false;
    }
    
    /**
     * Get current step status
     */
    public function getCurrentStepStatus()
    {
        if($this->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_FINISHED){
            $feedbacks = $this->getFeedBacksJob();
            if(count($feedbacks) > 0){
                return 'feedback';
            } else{
                return 'finished';
            }
        } elseif($this->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::ESCROWED){
            return 'escrowed';
        } elseif($this->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_AWARDED){
            return 'awarded';
        } else{
            return 'open';
        }
    }
    
    /**
     * Get current step status for Contest
     */
    public function getCurrentStepStatusContest()
    {
        if  ($this->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_FINISHED){
            return 'finished';
        } elseif ($this->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_CONTEST_QUALIFIED){
            return 'qualified';
        } elseif ($this->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::ESCROWED) {
            return 'escrowed';
        } else {
            return 'open';
        }
    }
    
    /**
     * Check if project is successfully completed
     * 
     * @return boolean 
     */
    public function getIsCompleted()
    {
        if($this->getStatus() == self::JOB_FINISHED){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set requireEscrow
     *
     * @param boolean $requireEscrow
     * @return Job
     */
    public function setRequireEscrow($requireEscrow)
    {
        $this->requireEscrow = $requireEscrow;
    
        return $this;
    }

    /**
     * Get requireEscrow
     *
     * @return boolean 
     */
    public function getRequireEscrow()
    {
        return $this->requireEscrow;
    }

    /**
     * Set isReviewed
     *
     * @param boolean $isReviewed
     * @return Job
     */
    public function setIsReviewed($isReviewed)
    {
        $this->isReviewed = $isReviewed;
    
        return $this;
    }

    /**
     * Get isReviewed
     *
     * @return boolean 
     */
    public function getIsReviewed()
    {
        return $this->isReviewed;
    }

    /**
     * Set isWillEscrow
     *
     * @param boolean $isWillEscrow
     * @return Job
     */
    public function setIsWillEscrow($isWillEscrow)
    {
        $this->isWillEscrow = $isWillEscrow;
    
        return $this;
    }

    /**
     * Get isWillEscrow
     *
     * @return boolean 
     */
    public function getIsWillEscrow()
    {
        return $this->isWillEscrow;
    }

    /**
     * Set reviewComment
     *
     * @param string $reviewComment
     * @return Job
     */
    public function setReviewComment($reviewComment)
    {
        $this->reviewComment = $reviewComment;
    
        return $this;
    }

    /**
     * Get reviewComment
     *
     * @return string 
     */
    public function getReviewComment()
    {
        return $this->reviewComment;
    }

    /**
     * Set upfrontAt
     *
     * @param \DateTime $upfrontAt
     * @return Job
     */
    public function setUpfrontAt($upfrontAt)
    {
        $this->upfrontAt = $upfrontAt;
    
        return $this;
    }

    /**
     * Get upfrontAt
     *
     * @return \DateTime 
     */
    public function getUpfrontAt()
    {
        return $this->upfrontAt;
    }

    /**
     * Set testType
     *
     * @param integer $testType
     * @return Job
     */
    public function setTestType($testType)
    {
        $this->testType = $testType;
    
        return $this;
    }

    /**
     * Get testType
     *
     * @return integer 
     */
    public function getTestType()
    {
        return $this->testType;
    }

    /**
     * Set service
     *
     * @param \Vlance\AccountBundle\Entity\Service $service
     * @return Job
     */
    public function setService(\Vlance\AccountBundle\Entity\Service $service = null)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return \Vlance\AccountBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set featuredFb
     *
     * @param boolean $featuredFb
     * @return Job
     */
    public function setFeaturedFb($featuredFb)
    {
        $this->featuredFb = $featuredFb;
    
        return $this;
    }

    /**
     * Get featuredFb
     *
     * @return boolean 
     */
    public function getFeaturedFb()
    {
        return $this->featuredFb;
    }

    /**
     * Add invites
     *
     * @param \Vlance\JobBundle\Entity\JobInvite $invites
     * @return Job
     */
    public function addInvite(\Vlance\JobBundle\Entity\JobInvite $invites)
    {
        $this->invites[] = $invites;
    
        return $this;
    }

    /**
     * Remove invites
     *
     * @param \Vlance\JobBundle\Entity\JobInvite $invites
     */
    public function removeInvite(\Vlance\JobBundle\Entity\JobInvite $invites)
    {
        $this->invites->removeElement($invites);
    }

    /**
     * Get invites
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvites()
    {
        return $this->invites;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Job
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set valid
     *
     * @param integer $valid
     * @return Job
     */
    public function setValid($valid)
    {
        $this->valid = $valid;
    
        return $this;
    }

    /**
     * Get valid
     *
     * @return integer 
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set qualifiedAt
     *
     * @param \DateTime $qualifiedAt
     * @return Job
     */
    public function setQualifiedAt($qualifiedAt)
    {
        $this->qualifiedAt = $qualifiedAt;
    
        return $this;
    }

    /**
     * Get qualifiedAt
     *
     * @return \DateTime 
     */
    public function getQualifiedAt()
    {
        return $this->qualifiedAt;
    }

    /**
     * Add jobActivities
     *
     * @param \Vlance\JobBundle\Entity\JobActivity $jobActivities
     * @return Job
     */
    public function addJobActivitie(\Vlance\JobBundle\Entity\JobActivity $jobActivities)
    {
        $this->jobActivities[] = $jobActivities;
    
        return $this;
    }

    /**
     * Remove jobActivities
     *
     * @param \Vlance\JobBundle\Entity\JobActivity $jobActivities
     */
    public function removeJobActivitie(\Vlance\JobBundle\Entity\JobActivity $jobActivities)
    {
        $this->jobActivities->removeElement($jobActivities);
    }

    /**
     * Get jobActivities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobActivities()
    {
        return $this->jobActivities;
    }

    /**
     * Set consolidatedAt
     *
     * @param \DateTime $consolidatedAt
     * @return Job
     */
    public function setConsolidatedAt($consolidatedAt)
    {
        $this->consolidatedAt = $consolidatedAt;
    
        return $this;
    }

    /**
     * Get consolidatedAt
     *
     * @return \DateTime 
     */
    public function getConsolidatedAt()
    {
        return $this->consolidatedAt;
    }

    /**
     * Set isPrivateContest
     *
     * @param boolean $isPrivateContest
     * @return Job
     */
    public function setIsPrivateContest($isPrivateContest)
    {
        $this->isPrivateContest = $isPrivateContest;
    
        return $this;
    }

    /**
     * Get isPrivateContest
     *
     * @return boolean 
     */
    public function getIsPrivateContest()
    {
        return $this->isPrivateContest;
    }

    /**
     * Set budgetMax
     *
     * @param integer $budgetMax
     * @return Job
     */
    public function setBudgetMax($budgetMax)
    {
        $this->budgetMax = $budgetMax;
    
        return $this;
    }

    /**
     * Get budgetMax
     *
     * @return integer 
     */
    public function getBudgetMax()
    {
        return $this->budgetMax;
    }

    /**
     * Set onsiteRequiredExp
     *
     * @param integer $onsiteRequiredExp
     * @return Job
     */
    public function setOnsiteRequiredExp($onsiteRequiredExp)
    {
        $this->onsiteRequiredExp = $onsiteRequiredExp;
    
        return $this;
    }

    /**
     * Get onsiteRequiredExp
     *
     * @return integer 
     */
    public function getOnsiteRequiredExp()
    {
        return $this->onsiteRequiredExp;
    }

    /**
     * Set onsiteCompanyName
     *
     * @param string $onsiteCompanyName
     * @return Job
     */
    public function setOnsiteCompanyName($onsiteCompanyName)
    {
        $this->onsiteCompanyName = $onsiteCompanyName;
    
        return $this;
    }

    /**
     * Get onsiteCompanyName
     *
     * @return string 
     */
    public function getOnsiteCompanyName()
    {
        return $this->onsiteCompanyName;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     * @return Job
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * Set path
     *
     * @param string $path
     * @return Job
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Get full path
     *
     * @return string 
     */
    public function getFullPath()
    {
        return $this->getUploadDir() . DS . $this->path;
    }

    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR . "/job_onsite";
    }
    
   /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL. "/job_onsite";
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        
        if($this->getFile() !== null) {
            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->getClientOriginalExtension();
        }
    }
 
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        if (!file_exists($this->getUploadDir())) {
            mkdir($this->getUploadDir(), 0777, true);
        }
        $this->getFile()->move($this->getUploadDir(), $this->path);
        $this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if($this->path === null) {
            return;
        }
        $file = $this->path;
        if(file_exists($file)) {
            unlink($file);
        }
    }
    
    /**
     * Set onsiteLocation
     *
     * @param integer $onsiteLocation
     * @return Job
     */
    public function setOnsiteLocation($onsiteLocation)
    {
        $this->onsiteLocation = $onsiteLocation;
    
        return $this;
    }

    /**
     * Get onsiteLocation
     *
     * @return integer 
     */
    public function getOnsiteLocation()
    {
        return $this->onsiteLocation;
    }

    public function getTextOnsiteLocation()
    {
        if($this->getOnsiteLocation() === 0){
            return "Làm việc tại văn phòng";
        } elseif($this->getOnsiteRequiredExp() === 1){
            return "Làm online và đến văn phòng khi cần";
        } else {
            return "Làm hoàn toàn online";
        }
    }
    
    public function getTextOnsiteRequiredExp()
    {
        if($this->getOnsiteRequiredExp() === 0){
            return "<2 năm";
        } elseif($this->getOnsiteRequiredExp() === 1){
            return "2 - 5 năm";
        } elseif($this->getOnsiteRequiredExp() === 2){
            return "5 - 10 năm";
        } else {
            return ">10 năm";
        }
    }

    /**
     * Set availableConversation
     *
     * @param integer $availableConversation
     * @return Job
     */
    public function setAvailableConversation($availableConversation)
    {
        $this->availableConversation = $availableConversation;
    
        return $this;
    }

    /**
     * Get availableConversation
     *
     * @return integer 
     */
    public function getAvailableConversation()
    {
        return $this->availableConversation;
    }

    /**
     * Add bidRates
     *
     * @param \Vlance\JobBundle\Entity\BidRate $bidRates
     * @return Job
     */
    public function addBidRate(\Vlance\JobBundle\Entity\BidRate $bidRates)
    {
        $this->bidRates[] = $bidRates;
    
        return $this;
    }

    /**
     * Remove bidRates
     *
     * @param \Vlance\JobBundle\Entity\BidRate $bidRates
     */
    public function removeBidRate(\Vlance\JobBundle\Entity\BidRate $bidRates)
    {
        $this->bidRates->removeElement($bidRates);
    }

    /**
     * Get bidRates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBidRates()
    {
        return $this->bidRates;
    }

    /**
     * Set cancelStatus
     *
     * @param integer $cancelStatus
     * @return Job
     */
    public function setCancelStatus($cancelStatus)
    {
        $this->cancelStatus = $cancelStatus;
    
        return $this;
    }

    /**
     * Get cancelStatus
     *
     * @return integer 
     */
    public function getCancelStatus()
    {
        return $this->cancelStatus;
    }
}