<?php

namespace Vlance\JobBundle\Entity;

use Vlance\BaseBundle\Entity\File as BaseFile;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobFile
 *
 * @ORM\Table(name="job_file")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobFileRepository")
 */
class JobFile extends BaseFile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="files", cascade={"persist"})
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    

    
    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return JobFile
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null) {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob() {
        return $this->job;
    }
 
}