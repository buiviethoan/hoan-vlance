<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobFeedback
 *
 * @ORM\Table(name="job_feedback")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobFeedbackRepository")
 */
class JobFeedback
{
    const TYPE_DESCRIPTION = 0;
    const TYPE_BUDGET = 1;
    
    const FIELD_SQL_ADDITION = 'additional_data';
    const FIELD_ADDITION = 'additionalData';
    const FIELD_NOTIFICATION = 'notification';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $sender;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $job;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", options={"default":"0"})
     */
    protected $type = 0;
    
    /**
     * @var float
     *
     * @ORM\Column(name="budget", type="float", options={"default":"0"})
     */
    protected $budget = 0;
    
    /**
     * @var string
     * @ORM\Column(name="additional_data", type="text", nullable=true)
     */
    protected $additionalData;
    
    protected $addtionalDataArray;
        
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return JobFeedback
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set sender
     *
     * @param \Vlance\AccountBundle\Entity\Account $sender
     * @return JobFeedback
     */
    public function setSender(\Vlance\AccountBundle\Entity\Account $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return VlanceAccountBundle\Entity\Account
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addtionalDataArray = array();
    }
    
    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return Bid
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return JobFeedback
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set budget
     *
     * @param integer $budget
     * @return JobFeedback
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    
        return $this;
    }

    /**
     * Get budget
     *
     * @return float 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set additionalData
     *
     * @param string $additionalData
     * @return JobFeedback
     */
    public function setAdditionalData($additionalData)
    {
        if (is_array($additionalData)) {
            $this->addtionalDataArray = $additionalData;
            $this->additionalData = json_encode($additionalData);
        } else {
            $this->additionalData = $additionalData;
            $this->addtionalDataArray = json_decode($additionalData);
        }
    
        return $this;
    }

    /**
     * Get additionalData
     *
     * @return string 
     */
    public function getAdditionalData($field = "")
    {
        if ($field) {
            if ($field !== "*") {
                return $this->addtionalDataArray[$field];
            } else {
                return $this->addtionalDataArray;
            }
        } else {
            return $this->additionalData;
        }
    }
    
    /**
     * Add a field to additional data
     * 
     * @param string $field
     * @param type $value
     * @return \Vlance\JobBundle\Entity\JobFeedback
     */
    public function addAdditionalData($field, $value) {
        $this->addtionalDataArray[$field] = $value;
        $this->additionalData = json_encode($this->addtionalDataArray);
        return $this;
    }
    
    public function removeAdditionalData($fields) {
        if (is_array($fields)) {
            foreach ($fields as $field) {
                if (is_string($field) && isset($this->addtionalDataArray[$field])) {
                    unset($this->addtionalDataArray[$field]);
                }
            }
        } else if (is_string($field) && isset($this->addtionalDataArray[$field])) {
            unset($this->addtionalDataArray[$field]);
        }
        return $this;
    }
    
    public function getTypeText() {
        switch ($this->type) {
            case self::TYPE_BUDGET:
                return 'budget';
            case self::TYPE_DESCRIPTION:
                return 'description';
        }
    }
}
