<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * JobFeedbackRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class JobFeedbackRepository extends EntityRepository
{
    public function statistic ($job_id = 0, array $conditions = array()) {
        // calculate for JobFeedback::TYPE_DESCRIPTION
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('IDENTITY(jf.job) job_id, count(jf.id) numFeedback')->from('VlanceJobBundle:JobFeedback', 'jf');
        $where = $queryBuilder->expr()->andX();
        $where->add($queryBuilder->expr()->eq('jf.type', JobFeedback::TYPE_DESCRIPTION));
        
        foreach ($conditions as $field => $condition) {
            if (in_array($condition[0], array('null', 'notnull'))) {
                $where->add($queryBuilder->expr()->{$condition[0]}("jf.{$field}"));
            } else {
                $where->add($queryBuilder->expr()->{$condition[0]}("jf.{$field}", $condition[1]));
            }
        }
        if ($job_id) {
            $where->add($queryBuilder->expr()->eq('jf.job', $job_id));
        } else {
            $queryBuilder->groupBy('jf.job');
        }
        $queryBuilder->where($where);
        $result = array();
        $tmp = $queryBuilder->getQuery()->execute();
        foreach ($tmp as $value) {
            if ($value['job_id']) {
                $result[$value['job_id']][JobFeedback::TYPE_DESCRIPTION] = $value;
            }
        }
        
        // Calculate for JobFeedback::TYPE_BUDGET
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('IDENTITY(jf.job) job_id, count(jf.id) numFeedback, avg(jf.budget) budget')->from('VlanceJobBundle:JobFeedback', 'jf');
        $where = $queryBuilder->expr()->andX();
        $where->add($queryBuilder->expr()->eq('jf.type', JobFeedback::TYPE_BUDGET));
        foreach ($conditions as $field => $condition) {
            if (in_array($condition[0], array('null', 'notnull'))) {
                $where->add($queryBuilder->expr()->{$condition[0]}("jf.{$field}"));
            } else {
                $where->add($queryBuilder->expr()->{$condition[0]}("jf.{$field}", $condition[1]));
            }
        }
        if ($job_id) {
            $where->add($queryBuilder->expr()->eq('jf.job', $job_id));
        } else {
            $queryBuilder->groupBy('jf.job');
        }
        $queryBuilder->where($where);
        $tmp = $queryBuilder->getQuery()->execute();
        foreach ($tmp as $value) {
            if ($value['job_id']) {
                $result[$value['job_id']][JobFeedback::TYPE_BUDGET] = $value;
            }
        }
        return $result;
    }
}
