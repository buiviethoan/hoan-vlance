<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * JobActivity
 *
 * @ORM\Table(name="job_activity")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\JobActivityRepository")
 */
class JobActivity
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="jobActivities")
     * @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     */
    protected $editor;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="jobActivities")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $job;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="content", type="text")
     */
    protected $content;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="hidden_content", type="text", nullable=true)
     */
    protected $hiddenContent;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="target_url", type="text", nullable=true)
     */
    protected $targetUrl;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return JobActivity
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return JobActivity
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return JobActivity
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set editor
     *
     * @param \Vlance\AccountBundle\Entity\Account $editor
     * @return JobActivity
     */
    public function setEditor(\Vlance\AccountBundle\Entity\Account $editor = null)
    {
        $this->editor = $editor;
    
        return $this;
    }

    /**
     * Get editor
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return JobActivity
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set hiddenContent
     *
     * @param string $hiddenContent
     * @return JobActivity
     */
    public function setHiddenContent($hiddenContent)
    {
        $this->hiddenContent = $hiddenContent;
    
        return $this;
    }

    /**
     * Get hiddenContent
     *
     * @return string 
     */
    public function getHiddenContent()
    {
        return $this->hiddenContent;
    }

    /**
     * Set targetUrl
     *
     * @param string $targetUrl
     * @return JobActivity
     */
    public function setTargetUrl($targetUrl)
    {
        $this->targetUrl = $targetUrl;
    
        return $this;
    }

    /**
     * Get targetUrl
     *
     * @return string 
     */
    public function getTargetUrl()
    {
        return $this->targetUrl;
    }
}