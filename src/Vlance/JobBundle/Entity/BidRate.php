<?php

namespace Vlance\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
/**
 * Bid
 *
 * @ORM\Table(name="bid_rate")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\BidRateRepository")
 */
class BidRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="bidRates")
     * @ORM\JoinColumn(name="sender", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $sender;

    /**
    * @ORM\OneToOne(targetEntity="Vlance\JobBundle\Entity\Bid", inversedBy="bidRate")
    */
    private $bid;
    
    /**
     * @ORM\ManyToOne(targetEntity="Job", inversedBy="bidRates")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $job;
    
    /**
     * @var integer
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var \DateTime
     * @ORM\Column(name="rating_at", type="datetime", nullable=true)
     */
    private $ratingAt;
    
    /**
     * @var \DateTime
     * 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * 
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return BidRate
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    
        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set ratingAt
     *
     * @param \DateTime $ratingAt
     * @return BidRate
     */
    public function setRatingAt($ratingAt)
    {
        $this->ratingAt = $ratingAt;
    
        return $this;
    }

    /**
     * Get ratingAt
     *
     * @return \DateTime 
     */
    public function getRatingAt()
    {
        return $this->ratingAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return BidRate
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return BidRate
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set bid
     *
     * @param \Vlance\JobBundle\Entity\Bid $bid
     * @return BidRate
     */
    public function setBid(\Vlance\JobBundle\Entity\Bid $bid = null)
    {
        $this->bid = $bid;
    
        return $this;
    }

    /**
     * Get bid
     *
     * @return \Vlance\JobBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * Set sender
     *
     * @param \Vlance\AccountBundle\Entity\Account $sender
     * @return BidRate
     */
    public function setSender(\Vlance\AccountBundle\Entity\Account $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return BidRate
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }
}