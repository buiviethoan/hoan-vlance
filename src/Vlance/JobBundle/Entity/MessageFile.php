<?php

namespace Vlance\JobBundle\Entity;

use Vlance\BaseBundle\Entity\File as BaseFile;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MessageFile
 *
 * @ORM\Table(name="message_file")
 * @ORM\Entity(repositoryClass="Vlance\JobBundle\Entity\MessageFileRepository")
 */
class MessageFile extends BaseFile
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Message", inversedBy="files")
     * @ORM\JoinColumn(name="message_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $message;
    
    /**
     * Which type this file is uploaded (job, message, porfolio)
     */
    protected $type = 'message';

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param \Vlance\JobBundle\Entity\Message $message
     * @return MessageFile
     */
    public function setMessage(\Vlance\JobBundle\Entity\Message $message = null) {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return \Vlance\JobBundle\Entity\Message 
     */
    public function getMessage() {
        return $this->message;
    }
    
}