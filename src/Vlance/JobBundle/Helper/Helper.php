<?php
namespace Vlance\JobBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper as BaseHelper;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\Job;

class Helper extends BaseHelper {
    protected $_em;
    protected $_container;
    
    public function __construct(ContainerInterface $container, EntityManager $em) {
        $this->_em = $em;
        $this->_container = $container;
    }
    
    public function getName() {
        return 'vlance_job';
    }
    
    public function generateInviteUrl($jobId, $accountId, $referenceType = false) {
        $hashString = $this->_container->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        $jobHash = md5("$jobId@$hashString");
        $accountHash = md5("$accountId@$hashString");
        return $this->_container->get('router')->generate('jobinvite_create', array('jobId' => $jobHash, 'accountId' => $accountHash), $referenceType);
    }
    
    public function generateRefuseInviteUrl($jobId, $accountId, $referenceType = false) {
        $hashString = $this->_container->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        $jobHash = md5("$jobId@$hashString");
        $accountHash = md5("$accountId@$hashString");
        return $this->_container->get('router')->generate('refuse_jobinvite_acc', array('jobId' => $jobHash, 'accountId' => $accountHash), $referenceType);
    }
    
    public function countCvJobOnsite($job, $onsiteOption){
        $count = 0;
        $options = $this->_em->getRepository('VlanceJobBundle:JobOnsiteOption')->findBy(array('job' => $job, 'onsiteOption' => $onsiteOption));
        if(count($options) > 0){
            $count = count($options);
        }
        return $count;
    }
    
    public function checkBidRate($job){
        $helper = $this->_container->get('vlance_payment.helper');
        $count_rate = count($job->getBidRates());
        if($count_rate === 5){
            if($job->getStatus() === Job::JOB_WORKING || $job->getStatus() === Job::JOB_FINISH_REQUEST || $job->getStatus() === Job::JOB_FINISHED){
                if(!($helper->addPromotionCredit($job->getAccount(), 0, 1, array(
                        'description'   => 'Client rated 5 bids',
                        'job_id'        => $job->getId(),
                        'qty'           => '1',
                    )))){
                    $helper->addPromotionCredit($job->getAccount(), 0, 1, array(
                            'description'   => 'Client rated 5 bids',
                            'job_id'        => $job->getId(),
                            'qty'           => '1',
                        ));
                }
            }
        }
    }
}