<?php

namespace Vlance\JobBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JobTest extends WebTestCase
{
    /**
     * @var EntityManager
     */
    private $_em;

    protected function setUp()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->_em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->_em->beginTransaction();
    }
    
    /**
     * Rollback changes.
     */
    public function tearDown()
    {
        $this->_em->rollback();
    }

    public function testSomething()
    {
        $user = $this->_em->getRepository('VlanceJobBundle:Job')->find(1);
    }
}