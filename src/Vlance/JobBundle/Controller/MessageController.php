<?php
namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Vlance\JobBundle\Entity\Message;
use Vlance\JobBundle\Entity\Bid;
use Vlance\JobBundle\Entity\Job;
use Vlance\JobBundle\Event\BidEvent;
use Vlance\JobBundle\Event\JobEvent;
use Vlance\JobBundle\Form\MessageType;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\PaymentBundle\Entity\CreditExpenseTransaction as CreditExpenseTransaction;

/**
 * Message controller.
 *
 * @Route("/message")
 */
class MessageController extends Controller
{
    /**
     * Submit a new Message
     *
     * @Route("/new/{bid}", name="message_create")
     * @Method("POST")
     * @Template("VlanceJobBundle:Message:new.html.php", engine ="php")
     */
    public function createAction(Request $request, $bid)
    {
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        
        $entity  = new Message();
        $em = $this->getDoctrine()->getManager();
        /* @var $entity_bid Bid */
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->find($bid);
        /* @var $job Job */
        $job = $entity_bid->getJob();
        $workshop = $entity_bid->getWorkshop();

        /* This is first message -> need to check role if user is client (owner of job) */
        if($job->getType() == Job::TYPE_BID){ // Case Bidding project
            if (is_null($workshop) && $acc !== $job->getAccount() && $job->getStatus() == Job::JOB_OPEN ) {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('controller.message.form.not_permission', array(), 'vlance'));
                return $this->redirect($this->generateUrl("404_page"), 302);
            }
        } else { // Job is "Contest"
            if($job->getType() === Job::TYPE_CONTEST){
                if($job->getStatus() == Job::JOB_OPEN ) {
                    $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('controller.message.form.not_permission', array(), 'vlance'));
                    return $this->redirect($this->generateUrl("404_page"), 302);
                }
            }
        }
        
        
        $acc_vail = array();
        foreach($job->getBids() as $bid) {
            $acc_vail[] = $bid->getAccount()->getId();
            // Allow admin send message to freelancer & client
            if ($acc->hasRole(\Vlance\AccountBundle\Entity\Account::ROLE_VLANCE_SUPER_ADMIN)) {
                $acc_vail[] = $acc->getId();
            }
        }
        $acc_vail[] = $job->getAccount()->getId();
        
        /* Check account send message must client (owner of job) or freelancer (owner of bid) */
        if(in_array($acc->getId(), $acc_vail) == false) {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('controller.message.form.not_permission', array(), 'vlance'));
            return $this->redirect($this->generateUrl("404_page"), 302);
        }
        
//        // When sender is Freelancer (id owner)
//        $credit_required_for_interview = false;
//        $having_enough_credit = false;
//        $require_credit = array(
//            'cost' => 0
//        );
//        if($job->getType() == Job::TYPE_BID){
//            if($entity_bid->getAccount() == $acc){
//                if(count($entity_bid->getMessagesByFreelancer()) == 0){
//                    // Freelancer has not send back any message
//                    $parameters = $this->container->getParameter('vlance_system');
//                    $require_credit = $parameters['testing_config']['credit_for_interview'];
//                    if(isset($require_credit['from_job_id']) && isset($require_credit['from_account_id']) && isset($require_credit['cost'])){
//                        if((int)($entity_bid->getAccount()->getId()) >= (int)($require_credit['from_account_id']) && (int)($job->getId()) >= (int)($require_credit['from_job_id'])){
//                            $credit_required_for_interview = true;
//                        }
//                        if((int)($entity_bid->getAccount()->getCredit()->getBalance()) >= (int)($require_credit['cost'])){
//                            $having_enough_credit = true;
//                        }
//                    }
//                }
//            }
//            if($credit_required_for_interview && !$having_enough_credit) {
//                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.message.form.not_enough_credit', array(), 'vlance'));
//                return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $entity_bid->getId())), 302);
//            }
//        }
        
        $form = $this->createForm(new MessageType(), $entity);
        $form->bind($request); 

        if ($form->isValid()) {
            $entity->setBid($entity_bid);
            $files = $entity->getFiles();
            foreach ($files as $file) {
                $file->setMessage($entity);
                $em->persist($file);
            }
            if($acc == $entity_bid->getAccount()) {
                $entity->setReceiver($entity_bid->getJob()->getAccount());
            }else {
                $entity->setReceiver($entity_bid->getAccount());
            }
            $entity->setStatus(Message::NEW_MESSAGE);
            $entity->setSender($acc);
            $vaild = JobCalculator::report_abuse($entity->getContent());
            if($vaild == false) {
                $entity->setIsNotified(true);
            }
            $em->persist($entity);

            if(is_null($workshop)) {
                if($job->getType() == Job::TYPE_BID){
                    if($job->getAvailableConversation() === 0) {
                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Bạn không có quyền gửi tin nhắn.', array(), 'vlance'));
                        return $this->redirect($this->generateUrl("job_show_freelance_job", array('hash' => $job->getHash())));
                    }
                    
                    $job_avai_chat = (int)$job->getAvailableConversation() - 1;
                    $job->setAvailableConversation($job_avai_chat);
                    $em->persist($job);
                }
                
                /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->dispatch(JobEvent::JOB_FIRST_MESSAGE, new BidEvent($entity_bid, $request));
            }
            
//            if($job->getType() == Job::TYPE_BID){
//                if($credit_required_for_interview && $having_enough_credit) {
//                    if((int)($require_credit['cost']) > 0){
//
//
//                        // Store CreditExpenseTransaction in database
//                        $credit_trans = new CreditExpenseTransaction();
//                        $credit_account = $entity_bid->getAccount()->getCredit();
//
//                        $credit_trans->setCreditAccount($credit_account);
//                        $credit_trans->setPreBalance($credit_account->getBalance());
//                        $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
//                        $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
//                        $credit_trans->setAmount((int)($require_credit['cost']));
//                        $credit_trans->setService(CreditExpenseTransaction::SERVICE_RESPOND_INTERVIEW);
//                        $credit_trans->setDetail(json_encode(array('job_id' => $job->getId(), 'bid_id' => $entity_bid->getId(), 'require_credit' => (int)($require_credit['cost']))));
//
//                        $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());
//                        $credit_trans->setPostBalance($credit_account->getBalance());
//
//                        $em->persist($credit_trans);
//                        $em->persist($credit_account);
//
//                        /** Tracking Event "Bid" **/
//                        // Mixpanel
//                        $this->get('vlance_base.helper')->trackAll($acc, "Spend Credit Respond Interview", $tracking_properties = array(
//                            'job_category'  => $job->getCategory()->getTitle(),
//                            'job_budget'    => $job->getBudget(),
//                            'credit_spent'  => $require_credit['cost']
//                        ));
//                    }
//                }
//            }
            
            $em->flush();
            
            // Gui mail thong bao.
            /* @var $job Job  */
            $job = $entity_bid->getJob();
            $from_email = $this->container->getParameter('from_email');
            //Send email to receiver
            $template = 'VlanceJobBundle:Email:new_message.html.twig';
            $context = array(
                'message' => $entity,
                'summary' => JobCalculator::cut($entity->getContent(), 150),
                'job' => $job,
                'bid' => $entity_bid,
                'sender' => $acc
            );
            $to = $entity->getReceiver()->getEmail();
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to);
            
            // Send email to admin
            $template_admin = 'VlanceJobBundle:Email:admin_new_message.html.twig';
            $context_admin = array(
                'message' => $entity,
                'job' => $job,
                'bid' => $entity_bid,
            );
            $to_admin = $this->container->getParameter('to_email');
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from_email, $to_admin);

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('job.message.send_success', array(), 'vlance'));
            return $this->redirect($this->generateUrl('workshop_bid', array('job_id' => $entity_bid->getJob()->getId(),'bid' => $entity_bid->getId())));
        }
        return array(
            'form'          => $form->createView(),
            'bid'           => $entity_bid,
            'current_user'  => $acc
        );
       
    }
    
   /**
     * Displays a form to create a new Message entity.
     *
     * @Route("/new/{bid}", name="message_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction($bid)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $current_user \Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        $message = new Message();
        $form   = $this->createForm(new MessageType(), $message);
        
        $em = $this->getDoctrine()->getManager();
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->find($bid);
        
        return array(
            'form'          => $form->createView(),
            'bid'           => $entity_bid,
            'current_user'  => $current_user
        );
    }
    
    /**
     * Finds and displays a Message entity.
     *
     * @Route("/s/{id}", name="message_show")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($id)
    {
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
       
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceJobBundle:Message')->find($id);
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        // Kiem tra quyen xem tin nhan.
        $current_use = $this->get('security.context')->getToken()->getUser();
        if($current_use != $entity->getSender() || $current_use != $entity->getReceiver())  {
             return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        //Update trang thai cua tin nhan
        if($entity->getStatus() == Message::NEW_MESSAGE) {
            $entity->setStatus(Message::READ_MESSAGE);
            $em->persist($entity);
            $em->flush();
        }
        
        return array(
            'entity'      => $entity,
        );
    }
    
     /**
     * Finds and displays a Message entity in bid.
     *
     * @Route("/bid/{bid}", name="message_show_bid")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showBidAction($bid)
    {
        $em = $this->getDoctrine()->getManager();
        $acc = $this->get('security.context')->getToken()->getUser();
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->find($bid);
        $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('bid' => $entity_bid),array('createdAt' => 'ASC'));

        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        if ($acc != $entity_bid->getAccount() || $acc != $entity_bid->getJob()->getAccount() ) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        return array(
            'entities'      => $entities,
            'bid'           => $bid,
        );
    }

    /**
     * Deletes a Message entity.
     *
     * @Route("/{id}", name="message_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VlanceJobBundle:Message')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl("404_page"), 301);
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('message'));
    }

    /**
     * Creates a form to delete a Message entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * Lists all Messages file entities by bid.
     *
     * @Route("/file/{bid}", name="message_file")
     * @Method("GET")
     * @Template(engine = "php")
     */
    public function showFileAction($bid)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('bid' => $bid),array('createdAt' => 'DESC'));
        
        return array(
            'entities'      => $entities,
        );
    }
    
    /**
     * List all Messages is New_Message of current user
     * 
     * @Route("/new-message", name="new_message")
     * @Method("GET")
     * @Template(engine ="php")
     */
    public function listNewAction() 
    {
         //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $current_user \Vlance\AccountBundle\Entity\Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceJobBundle:Message')->groupProject($current_user);
//        $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('receiver' => $current_user, 'status' => Message::NEW_MESSAGE), array('createdAt' => 'DESC'), 15);
        return array(
            'entities' => $entities,
        );
    }

        /**
     * Lists all Message entities.
     *
     * @Route("/{filters}", name="message")
     * @Method("GET")
     * @Template(engine ="php")
     */
    public function indexAction(Request $request , $filters = "")
    {
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $this->get('session')->set('filter', $params);
        $em = $this->getDoctrine()->getManager();
       
        $entities = $em->getRepository('VlanceJobBundle:Message')->findBy($params);

        return array(
            'entities' => $entities,
        );
    }
}
