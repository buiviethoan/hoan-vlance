<?php

namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Vlance\JobBundle\Entity\Bid;
use Vlance\JobBundle\Entity\BidRate;
use Vlance\JobBundle\Entity\BidFile;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Entity\City;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Event\TransactionEvent;
use Vlance\PaymentBundle\Entity\PaymentMethod;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Form\BidType;
use Vlance\JobBundle\Form\BidOnsiteType;
use Vlance\JobBundle\Form\BidFileType;
use Vlance\JobBundle\Form\ContestApplyType;
use Vlance\BaseBundle\Event\VlanceEvents;
use Vlance\JobBundle\Event\BidEvent;
use Vlance\JobBundle\Event\JobEvent;
use Symfony\Component\HttpFoundation\Response;
use Vlance\JobBundle\Entity\Message;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\JobBundle\Helper\Helper as JobHelper;
use Vlance\PaymentBundle\Entity\JobCash;
use Vlance\PaymentBundle\Entity\CreditExpenseTransaction as CreditExpenseTransaction;
use Vlance\PaymentBundle\Entity\CreditAccount;

/**
 * Bid controller.
 *
 * @Route("/bid")
 */
class BidController extends Controller
{
    
    /**
     * Submit a new Bid entity.
     *
     * @Route("/create/{id}", name="bid_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request,$id)
    {
        
        $em = $this->getDoctrine()->getManager();
        /* @var $acc Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        /*@var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        // If current user is banned
        if ($acc->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.bid', array('%%bannedTo%%' => $acc->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
        } 
        
        /*if($acc->getNumCurrBids() < $job->getNumBids()) {
            $day_acc = $acc->getCreatedAt()->format('d');
            $today = date("Y-m-d");
            $next_month = date("m/Y", strtotime("$today +1 month"));
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.bid.not_bid', array('%day_reset%' => $day_acc.'/'.$next_month ,'%number_bid%' => $acc->getMonthlyBids()), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
        }*/
        
        $tracking_properties = array(
            'owner_category'=> $acc->getCategory()->getTitle(),
            'job_category'  => $job->getCategory()->getTitle(),
            'job_budget'    => $job->getBudget()
        );
        
        $parameters = $this->get('vlance_payment.helper')->getParameter("credit");
        $now = new \DateTime('now');
        if($job->getType() == Job::TYPE_BID){
            if(JobCalculator::ago($acc->getExpiredDateVip(), $now) == false){
                if($acc->getCredit()->getBalance() < 1){
                    //Lưu thông tin bid_form khi người dùng không đủ credit
                    $json = json_encode($_POST['vlance_jobbundle_bidtype']);
                    setcookie('Bid_Data', $json, time() + 3600, '/viec-freelance/'.$job->getHash().'');

                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.bid.require_credit.controller.error', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
                } else{
                    // Store CreditExpenseTransaction in database
                    $credit_trans = new CreditExpenseTransaction();
                    $credit_trans->setCreditAccount($acc->getCredit());
                    $credit_trans->setPreBalance($acc->getCredit()->getBalance());
                    $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
                    $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
                    $credit_trans->setAmount(1);
                    $credit_trans->setService(CreditExpenseTransaction::SERVICE_MAKE_BID);
                    $credit_trans->setDetail(json_encode(array('job_id' => $job->getId(), 'require_credit' => 1)));

                    $credit_account = $acc->getCredit();
                    $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());

                    // Set addtional tracking event properties
                    $tracking_properties["credit_spent"] = 1;

                    $credit_trans->setPostBalance($credit_account->getBalance());
                    $em->persist($credit_trans);
                    $em->persist($credit_account);
                }
            } else {
                if($acc->getTypeVip() == Account::TYPE_VIP_MINI){
                    if($acc->getCategoryVIP()->getId() !== $job->getCategory()->getId()){
                        // Check if user has enough credit to spend
                        if($acc->getCredit()->getBalance() < 1){
                            //Lưu thông tin bid_form khi người dùng không đủ credit
                            $json = json_encode($_POST['vlance_jobbundle_bidtype']);
                            setcookie('Bid_Data', $json, time() + 3600, '/viec-freelance/'.$job->getHash().'');

                            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('form.bid.require_credit.controller.error', array(), 'vlance'));
                            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
                        } else{
                            // Store CreditExpenseTransaction in database
                            $credit_trans = new CreditExpenseTransaction();
                            $credit_trans->setCreditAccount($acc->getCredit());
                            $credit_trans->setPreBalance($acc->getCredit()->getBalance());
                            $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
                            $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
                            $credit_trans->setAmount(1);
                            $credit_trans->setService(CreditExpenseTransaction::SERVICE_MAKE_BID);
                            $credit_trans->setDetail(json_encode(array('job_id' => $job->getId(), 'require_credit' => 1)));

                            $credit_account = $acc->getCredit();
                            $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());

                            // Set addtional tracking event properties
                            $tracking_properties["credit_spent"] = 1;

                            $credit_trans->setPostBalance($credit_account->getBalance());
                            $em->persist($credit_trans);
                            $em->persist($credit_account);
                        }
                    }
                }
            }
        }
        
        /* @var $entity Bid */
        $entity  = new Bid();
        $entity->setAccount($acc);
        $entity->setJob($job);
        $form = $this->createForm(new BidType(), $entity);
        $form->bind($request);
        
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc,'job' => $job));
        
        // Already made bid, not allow bidding more
        if(count($bid) > 0) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.notentity',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage')); // TODO redirect to that job page
        }
        
        // Verify if files size are less than 10MB
        $file_size_error = false;
        if(($fs = $entity->getFiles())){
            foreach ($fs as $f) {
                if(($file = $f->getFile())){
                    if($file->getSize() > 10485760){
                        $file_size_error = true;
                        break;
                    }
                }
            }
        } 
        
        if($file_size_error){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('profile.edit_profile.error_file', array(), 'vlance'));
        } else {
            if ($form->isValid()) { 
                $files = $entity->getFiles();
                foreach ($files as $file) {
                    $file->setBid($entity);
                    $em->persist($file);
                }

                // Check if violated, then marked the bid as "Violated"
                $violation = JobCalculator::report_abuse($entity->getDescription());
                if($violation) {
                    $entity->setIsViolated(true);
                } else {
                    $entity->setIsNotified(true);
                }
                // Save into database
                $em->persist($entity);
                $tracking_properties["owner_category"]  = $acc->getCategory()->getTitle();
                $tracking_properties["bid_violated"]  = $entity->getIsViolated();

                // update field minAmount, maxAmount, sumAmount, sumDuration in table job
                if($job->getMinAmount() == 0) {
                    $job->setMinAmount($entity->getAmount());
                } else {
                        if($job->getMinAmount() > $entity->getAmount()) {
                        $job->setMinAmount($entity->getAmount());
                    } 
                }

                if($job->getMaxAmount() < $entity->getAmount()) {
                    $job->setMaxAmount($entity->getAmount());
                }

                $job->setSumAmount($job->getSumAmount() + $entity->getAmount());
                $job->setSumDuration($job->getSumDuration() + $entity->getDuration());
                $em->persist($job);

                /* update field numCurrBids */
                /*if($acc->getNumCurrBids() >= $job->getNumBids()) {
                    $acc->setNumCurrBids($acc->getNumCurrBids() - $job->getNumBids());
                    $em->persist($acc);
                }*/

                /**
                 * update invite status
                 */
                /* @var $jobInvite \Vlance\JobBundle\Entity\JobInvite */
                $jobInvite = $em->getRepository('VlanceJobBundle:JobInvite')->findOneBy(array('job' => $job, 'account' => $acc));
                if ($jobInvite) {
                    $jobInvite->setStatus(\Vlance\JobBundle\Entity\JobInvite::INVITE_BIDDED);
                    $em->persist($jobInvite);
                }

                $telephone = $request->request->get('input_telephone');
                $skype = $request->request->get('input_skype');
                
                if(!$acc->getTelephoneVerifiedAt()){
                    if($acc->getTelephone() != $telephone){
                        $acc->setTelephone($telephone);
                    }
                }
                
                $acc->setSkype($skype);
                $em->persist($acc);

                $em->flush();

                /** Tracking Event "Bid" **/
                // Mixpanel
                $this->get('vlance_base.helper')->trackAll($acc, "Bid", $tracking_properties);
                // Facebook
                $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                        '<!-- Facebook Conversion Code for Made bid -->
                        <script>(function() {
                          var _fbq = window._fbq || (window._fbq = []);
                          if (!_fbq.loaded) {
                            var fbds = document.createElement("script");
                            fbds.async = true;
                            fbds.src = "//connect.facebook.net/en_US/fbds.js";
                            var s = document.getElementsByTagName("script")[0];
                            s.parentNode.insertBefore(fbds, s);
                            _fbq.loaded = true;
                          }
                        })();
                        window._fbq = window._fbq || [];
                        window._fbq.push(["track", "6023542039885", {"value":"0.00","currency":"VND"}]);
                        </script>
                        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023542039885&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                    )
                ));
                /** END Tracking Event "Bid" **/

                // Gui email
                $from_email = $this->container->getParameter('from_email');

                //Send email client
                $to_client = $job->getAccount()->getEmail();
                $template_client = 'VlanceJobBundle:Email:bid_job.html.twig';
                $context_client = array(
                    'job' => $job,
                );
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from_email, $to_client);

                //Send email admin
                $to_admin = $this->container->getParameter('to_email');
                $template_admin = 'VlanceJobBundle:Email:admin_bid_job.html.twig';
                if($violation) {
                    $template_admin = 'VlanceJobBundle:Email:admin_bid_violation.html.twig';
                }
                $context_admin = array(
                    'job' => $job,
                    'bid' => $entity,
                );
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from_email, $to_admin);

                $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.bid.create.message',array('%here%' => '<a href="'.$this->generateUrl('freelance_job_list').'">tại đây</a>'),'vlance'));
                return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
            }
        }
        
        // From no validation
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
    }
    
    /**
     * Submit a new Contest Apply entity.
     *
     * @Route("/create-contest-apply/{id}", name="contest_apply_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createContestApplyAction(Request $request,$id)
    {
        
        $em = $this->getDoctrine()->getManager();
        /* @var $acc Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        /*@var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        // If current user is banned
        if ($acc->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.bid', array('%%bannedTo%%' => $acc->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
        } 
        
        $tracking_properties = array(
            'contest_category'  => $job->getCategory()->getTitle(),
            'contest_budget'    => $job->getBudget()
        );
        
        /* @var $entity Bid */
        $entity  = new Bid();
        $entity->setAccount($acc);
        $entity->setJob($job);
        $entity->setIsNotified(true);
        $entity->setDescription("NULL");
        $entity->setIntroDescription("NULL");
        $entity->setAmount($job->getBudget());
        $entity->setDuration(5);
        $form = $this->createForm(new ContestApplyType(), $entity);
        $form->bind($request);
        
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc,'job' => $job));

	//Gửi tối đa 4 bài dự thi trong 1 contest
	if(count($bid) > 3 && $acc->getExtraBid() <= 0){
	    $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('Quá số bài mặc định cho phép. Thanh toán credit để gửi thêm bài.',array(),'vlance'));
	    return $this->redirect($this->generateUrl('job_contest_view',array('hash'=>$job->getHash())));
	}
	
	$edit_extra_bid = 0;
	if(count($bid) > 3){
	    $edit_extra_bid = 1;
	}
	
        $bid2 = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job ));
	
	//Tối cuộc thi đã có 24 bài thì thông báo không nhận bài nữa, nhưng vẫn lưu db :p
	$maximum_bids = "0";
	if(count($bid2) >= $job->getMaxBid()){
	    $maximum_bids = "1";
	    $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('Gửi bài thành công. Bài vừa gửi sẽ KHÔNG hiển thị do số lượng bài của cuộc thi này đã vượt quá giới hạn.',array(),'vlance'));
//	    return $this->redirect($this->generateUrl('job_contest_view',array('hash'=>$job->getHash()))); 
	}
	if($maximum_bids === "1"){
	    $entity->setPublish(Bid::UNPUBLISH);
	}
	
        // Already made bid, not allow bidding more
//        if(count($bid) > 0) {
//            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.notentity',array(),'vlance'));
//            return $this->redirect($this->generateUrl('vlance_homepage')); // TODO redirect to that job page
//        }
        // Verify if files size are less than 10MB
        $file_size_error = false;
        if(($fs = $entity->getFiles())){
            foreach ($fs as $f) {
                if(($file = $f->getFile())){
                    if($file->getSize() > 10485760){
                        $file_size_error = true;
                        break;
                    }
                }
            }
        } 
        if($file_size_error){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('profile.edit_profile.error_file', array(), 'vlance'));
        } else {
            if ($form->isValid()) { 
                $files = $entity->getFiles();
                foreach ($files as $file) {
                    $file->setBid($entity);
                    $em->persist($file);
                }
		
		//Trừ dần số lượng bài dự được phép gửi thêm (do freelancer mua thêm)
		if($edit_extra_bid == 1){
		    $acc->setExtraBid($acc->getExtraBid()-1);
		    $em->persist($acc);
		}
		
                // Save into database
                $em->persist($entity);

                // update field minAmount, maxAmount, sumAmount, sumDuration in table job
                if($job->getMinAmount() == 0) {
                    $job->setMinAmount($entity->getAmount());
                } else {
                        if($job->getMinAmount() > $entity->getAmount()) {
                        $job->setMinAmount($entity->getAmount());
                    } 
                }

                if($job->getMaxAmount() < $entity->getAmount()) {
                    $job->setMaxAmount($entity->getAmount());
                }

                $job->setSumAmount($job->getSumAmount() + $entity->getAmount());
                $job->setSumDuration($job->getSumDuration() + $entity->getDuration());
                $em->persist($job);
                $em->flush();

                /** Tracking Event "Bid" **/
                // Mixpanel
                $this->get('vlance_base.helper')->trackAll($acc, "Contest apply", $tracking_properties);
                // Facebook
                $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                        '<!-- Facebook Conversion Code for Made bid -->
                        <script>(function() {
                          var _fbq = window._fbq || (window._fbq = []);
                          if (!_fbq.loaded) {
                            var fbds = document.createElement("script");
                            fbds.async = true;
                            fbds.src = "//connect.facebook.net/en_US/fbds.js";
                            var s = document.getElementsByTagName("script")[0];
                            s.parentNode.insertBefore(fbds, s);
                            _fbq.loaded = true;
                          }
                        })();
                        window._fbq = window._fbq || [];
                        window._fbq.push(["track", "6023542039885", {"value":"0.00","currency":"VND"}]);
                        </script>
                        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023542039885&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                    )
                ));
                /** END Tracking Event "Bid" **/

                // Gui email
                $from_email = $this->container->getParameter('from_email');

                //Send email client
                $to_client = $job->getAccount()->getEmail();
                $template_client = 'VlanceJobBundle:Email:bid_job.html.twig';
                $context_client = array(
                    'job' => $job,
                );
//                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from_email, $to_client);

                //Send email admin
                $to_admin = $this->container->getParameter('to_email');
                $template_admin = 'VlanceJobBundle:Email:admin_bid_job.html.twig';
//                if($violation) {
//                    $template_admin = 'VlanceJobBundle:Email:admin_bid_violation.html.twig';
//                }
                $context_admin = array(
                    'job' => $job,
                    'bid' => $entity,
                );
//                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from_email, $to_admin);
		
		if($maximum_bids === "0"){
                    $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.bid.create.message',array('%here%' => '<a href="'.$this->generateUrl('freelance_job_list').'">tại đây</a>'),'vlance'));
		}
                return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $job->getHash()), true));
            }
        }
        
        // From no validation
        return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $job->getHash()), true));
    }

    /**
     * Displays a form to make a bid.
     *
     * @Route("/new/{id}", name="bid_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction($id, $bid_form = null)
    {
        /* @var $acc Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc,'job' => $job));
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($job->getCloseAt(), $now);
        
        $bid_able = true;
        // Chua dang nhap
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response('<div class="login-bid">'.$this->get('translator')->trans('view_job_page.job_detail.login_send_bid', array(), 'vlance').' <a href="'.$this->generateUrl('fos_user_security_login').'">'.$this->get('translator')->trans('view_job_page.job_detail.login', array(), 'vlance').'</a></div>');
        } 
        
        /*if($acc->getNumCurrBids() < $job->getNumBids()) {
            $bid_able = false;
            $day_acc = $acc->getCreatedAt()->format('d');
            $today = date("Y-m-d");
            $next_month = date("m/Y", strtotime("$today +1 month"));
            return new Response('<div class="bid-notice"><i>'.$this->get('translator')->trans('controller.bid.defect_bid', array('%day_reset%' => $day_acc.'/'.$next_month ,'%number_bid%' => $acc->getMonthlyBids()), 'vlance').'</i></div>');
        }*/
        
        // Check if there are missing information, good if count($missing_info) == 0
        $missing_info = $acc->getMissingInfo();
        
        if(is_object($acc)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($acc->getEmail())){
                if($helper->emailNotFacebook($acc->getEmail()) == false){
                    if(in_array('email', $missing_info) == false){
                        array_push($missing_info, 'email');
                    }
                }
            }
        }
        
        if(count($missing_info) > 0){
            foreach($missing_info as $key => $info){
                $missing_info[$key] = $this->get('translator')->trans('controller.job.showFreelanceJob.' . $info, array(), 'vlance');
            }
        }
        
        // Nguoi dung da bao gia
        if($bid) {
            return new Response($this->get('translator')->trans('controller.bided',array(),'vlance'));
        } 
        // Het han bao gia
        if($remain == false) {
            return new Response($this->get('translator')->trans('controller.expire',array(),'vlance'));
        }
        //Job da duoc giao cho freelancer
        if($job->getStatus() != Job::JOB_OPEN) {
            return new Response($this->get('translator')->trans('controller.awarded',array(),'vlance'));
        }
        
        // Neu form chua co du lieu
        if($bid_form == null) {
            $entity = new Bid();
            $form = $this->createForm(new BidType(), $entity);
            
            //Lấy giá trị đã lưu trong form khi gặp lỗi
            if(isset($_COOKIE['Bid_Data'])){
                foreach(json_decode($_COOKIE['Bid_Data']) as $d){
                    $data[] = $d;
                }
                $form->get('amount')->setData($data[0]);
                $form->get('duration')->setData($data[1]);
                $form->get('introDescription')->setData($data[2]);
                $form->get('description')->setData($data[3]);
            }
        } else {
            $form = $bid_form;
            
            //Lấy giá trị đã lưu trong form khi gặp lỗi
            if(isset($_COOKIE['Bid_Data'])){
                foreach(json_decode($_COOKIE['Bid_Data']) as $d){
                    $data[] = $d;
                }
                $form->get('amount')->setData($data[0]);
                $form->get('duration')->setData($data[1]);
                $form->get('introDescription')->setData($data[2]);
                $form->get('description')->setData($data[3]);
            }
        }
        
        
        
        $response = array(
            'job'           => $job,
            'bid_able'      => $bid_able,
            'form'          => $form->createView(),
            'id'            => $id,
            'budget'        => $job->getBudget(),
            'missing_info'  => $missing_info
        );
        
        // If current user is banned
        if ($acc->isBanned()) {
            $response['bannedTo'] = $acc->getBannedTo()->format('H:i:s d/m/Y');
            $response['bid_able'] = false;
            //return new Response('<div class="banned-bid">'.$this->get('translator')->trans('view_job_page.job_detail.banned_send_bid', array('%bannedTo%' => $acc->getBannedTo()->format('H:i:s d/m/Y')), 'vlance').'</div>');
        }
        
        // Current user phải cung cấp dịch vụ tương tự job UPDATE 19/06/2017
//        $job_service = $job->getService();
//        if($job_service){
//            $acc_service = $acc->getServices();
//            $id_ser_arr = array();
//            foreach($acc_service as $s){
//                $id_ser_arr[] = $s->getId();
//            }
//            
//            if(!in_array($job_service->getId(), $id_ser_arr)){
//                $response['service'] = false;
//                $response['bid_able'] = false;
//            }
//        }
        
        // Check if freelancer belongs to the testing case "Require Credit to respond Interview"
        $vlance_system = $this->container->getParameter('vlance_system');
//        $require_credit_param = $vlance_system['testing_config']['credit_for_interview'];
//        $credit_required_for_interview = false;
//        if(isset($require_credit_param['from_job_id']) && isset($require_credit_param['from_account_id']) && isset($require_credit_param['cost'])){
//            if((int)($acc->getId()) >= (int)($require_credit_param['from_account_id']) && (int)($job->getId()) >= (int)($require_credit_param['from_job_id'])){
//                $credit_required_for_interview = true;
//            }
//        }
        
        // If freelancer doesn't belongs to the testing case "Require Credit to respond Interview"
        // AND this job require Credit to Bid
        $parameters = $this->get('vlance_payment.helper')->getParameter("credit");
//        if(isset($parameters['pay_to_bid']) && $credit_required_for_interview == false){
//            $pay_to_bid = $parameters['pay_to_bid'];
//            
//            // Check if this category require credit in certain conditions
//            if(isset($pay_to_bid[$job->getCategory()->getId()])){
//                $require_credit = $pay_to_bid[$job->getCategory()->getId()];
//                $require_credit_for_bid = false;
//                
//                // If config require Credit for bidding is set
//                if(isset($require_credit['cost']) && isset($require_credit['limit'])){
//                    $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job, 'publish' => 1, 'isViolated' => 0));
//
//                    // Check if number of free bid reachs the limit
//                    if(count($bids) >= (int)($require_credit['limit']) && (int)($require_credit['cost']) > 0){
//
//                        // Freelancer has to use credit to bid if:
//                        // - freelancer got Rating < 4.5
//                        // - or had no Feedback
//                        // - or work on different category
//                        // - or his profile has not set category
//                        if($acc->getNumReviews() > 0){
//                            if(($acc->getCategory())){
//                                if(($acc->getScore() / $acc->getNumReviews() < 4.5) || ($acc->getCategory()->getId() != $job->getCategory()->getId())){
//                                    $require_credit_for_bid = true;
//                                }
//                            } else{
//                                $require_credit_for_bid = true;
//                            }
//                        } else {
//                            $require_credit_for_bid = true;
//                        }
//                    }
//                }
//                
//                if($require_credit_for_bid){
//                    $response['require_credit'] = $require_credit;
//                    $response['credit_balance'] = $acc->getCredit()->getBalance();
//                }
//            }
//        }

        return $this->container->get('templating')->renderResponse("VlanceJobBundle:Job:show/bid_form.html.php", $response);
        
    }

    /**
     * Displays a form to make a bid.
     *
     * @Route("/new-contest-apply/{id}", name="contest_apply_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newContestApplyAction($id, $bid_form = null)
    {
        /* @var $acc Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc,'job' => $job));
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($job->getCloseAt(), $now);
	
        $current_num_bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job));
	$max_bid = $job->getMaxBid();
	$display_pay_credit = 0; // không cho mua thêm lượt gửi bài
	//Cho freelancer mua thêm lượt khi: đã gửi 4 bài, không còn dư số lần mua thêm lượt
	// bid_form.html.php
	if( count($bid) >= 4 && $acc->getExtraBid() <= 0){
	    //kiểm tra credit
	    if($acc->getCredit()->getBalance() >= 1){
		$display_pay_credit = 1; //thông báo mua thêm lượt gửi bài
	    }else{
		$display_pay_credit = 2; //thông báo không đủ credit
	    }
	}else {
	    $display_pay_credit = 0; //cho gửi bài
	}
	
        $bid_able = true;
        // Chua dang nhap
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response('<div class="login-bid">'.$this->get('translator')->trans('view_job_page.job_detail.login_send_bid', array(), 'vlance').' <a href="'.$this->generateUrl('fos_user_security_login').'">'.$this->get('translator')->trans('view_job_page.job_detail.login', array(), 'vlance').'</a></div>');
        } 
        
        /*if($acc->getNumCurrBids() < $job->getNumBids()) {
            $bid_able = false;
            $day_acc = $acc->getCreatedAt()->format('d');
            $today = date("Y-m-d");
            $next_month = date("m/Y", strtotime("$today +1 month"));
            return new Response('<div class="bid-notice"><i>'.$this->get('translator')->trans('controller.bid.defect_bid', array('%day_reset%' => $day_acc.'/'.$next_month ,'%number_bid%' => $acc->getMonthlyBids()), 'vlance').'</i></div>');
        }*/
        
        // Check if there are missing information, good if count($missing_info) == 0
        $missing_info = $acc->getMissingInfo();
        if(count($missing_info) > 0){
            foreach($missing_info as $key => $info){
                $missing_info[$key] = $this->get('translator')->trans('controller.job.showFreelanceJob.' . $info, array(), 'vlance');
            }
        }
        
        // Nguoi dung da bao gia
//        if($bid) {
//            return new Response($this->get('translator')->trans('controller.bided',array(),'vlance'));
//        }
        // Het han bao gia
        if($remain == false) {
            return new Response($this->get('translator')->trans('controller.expire',array(),'vlance'));
        }
        //Job da duoc giao cho freelancer
        if($job->getStatus() != Job::JOB_OPEN) {
            return new Response($this->get('translator')->trans('controller.awarded',array(),'vlance'));
        }
        // Neu form chua co du lieu
        if($bid_form == null) {
            $entity = new Bid();
            $form   = $this->createForm(new ContestApplyType(), $entity);
        } else {
            $form = $bid_form;
        }
        $response = array(
            'job'           => $job,
            'bid_able'      => $bid_able,
            'form'          => $form->createView(),
            'id'            => $id,
            'budget'        => $job->getBudget(),
            'missing_info'  => $missing_info,
	    'display_pay_credit' => $display_pay_credit
        );
        
        // If current user is banned
        if ($acc->isBanned()) {
            $response['bannedTo'] = $acc->getBannedTo()->format('H:i:s d/m/Y');
            $response['bid_able'] = false;
            //return new Response('<div class="banned-bid">'.$this->get('translator')->trans('view_job_page.job_detail.banned_send_bid', array('%bannedTo%' => $acc->getBannedTo()->format('H:i:s d/m/Y')), 'vlance').'</div>');
        }
        
        // Check if freelancer belongs to the testing case "Require Credit to respond Interview"
        $vlance_system = $this->container->getParameter('vlance_system');
//        $require_credit_param = $vlance_system['testing_config']['credit_for_interview'];
//        $credit_required_for_interview = false;
//        if(isset($require_credit_param['from_job_id']) && isset($require_credit_param['from_account_id']) && isset($require_credit_param['cost'])){
//            if((int)($acc->getId()) >= (int)($require_credit_param['from_account_id']) && (int)($job->getId()) >= (int)($require_credit_param['from_job_id'])){
//                $credit_required_for_interview = true;
//            }
//        }
        
        return $this->container->get('templating')->renderResponse("VlanceJobBundle:Job:contest/view/bid_form.html.php", $response);
        
    }
    
    /**
     * Edits an existing Bid entity.
     *
     * @Route("/{id}/isdeclined/update", name="bid_update_isdeclined")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function isDeclinedAction(Request $request, $id)
    {
        $referer = $request->headers->get('referer');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceJobBundle:Bid')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.notfound',array(),'vlance'));
        } 
        
        // Neu la nguoi da duoc chon, thi thong bao loi
        if (!is_null($entity->getAwardedAt())) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // Neu khong phai nguoi tao job thi redirect ve home page
        $current_user = $this->get('security.context')->getToken()->getUser();
        if (!$entity->getJob()->isOwner($current_user)) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        $entity->setIsDeclined(1);
        $em->persist($entity);
        $em->flush();
        
        //update field sumAmount, sumDuration , maxAmount, minAmount of job entity
        $job = $entity->getJob();
        $job->setSumAmount($job->getSumAmount() - $entity->getAmount());
        $job->setSumDuration($job->getSumDuration() - $entity->getDuration());
        // bid tu choi la nguoi dua gia thap nhat
        if($entity->getAmount() == $job->getMinAmount()) {
            $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job, 'isDeclined' => 0));
            if(count($bids) < 1) {
                $job->setMinAmount(0);
            } else {
                $min = $bids[0]->getAmount();
                foreach($bids as $bid) {
                    if ($bid->getAmount() < $min ) {
                        $min = $bid->getAmount();
                    }
                }
                $job->setMinAmount($min);
            }
            
        }
        // bid tu choi la nguoi dua ra gia cao nhat
        if($entity->getAmount() == $job->getMaxAmount()) {
            $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job,'isDeclined' => 0));
            if(count($bids) < 1) {
                $job->setMaxAmount(0);
            } else {
                $max = $bids[0]->getAmount();
                foreach($bids as $bid) {
                    if ($bid->getAmount() > $max ) {
                        $max = $bid->getAmount();
                    }
                }
                $job->setMaxAmount($max);
            }
        }
        $em->persist($job);
        
        $em->flush();
        if ($referer == '') {
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $entity->getJob()->getHash())));       
        } else{
            return $this->redirect($referer);
        }
     }
     
     /**
     * @Route("/violated/{id}",name="bid_violate")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function isViolatedAction($id) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $bid = $em->getRepository('VlanceJobBundle:Bid')->find($id);
        if($bid->getIsViolated() == true) {
            return $this->redirect($this->generateUrl("500_page"), 301);
        } 
        $bid->setIsViolated(true) ;
        $em->persist($bid);
        $em->flush();
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $bid->getJob()->getHash())));
    }
        
    /**
     * Edits an existing Bid entity.
     *
     * @Route("/{id}/awardeat/update", name="bid_update_awardeat")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function awardeAtAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $base_helper = $this->get('vlance_base.helper');    /* @var $base_helper BaseHelper */
        $awardedBid = $em->getRepository('VlanceJobBundle:Bid')->find($id); /* @var $awardedBid Bid */
        if (!$awardedBid) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $job            = $awardedBid->getJob();        /* @var $job Job */
        $client         = $job->getAccount();           /* @var $client Account */
        $freelancer     = $awardedBid->getAccount();    /* @var $freelancer Account */
        $email_hotro    = $base_helper->getParameter('vlance_email.from.hotro');
        $bcc[]          = $base_helper->getParameter('vlance_email.to.antispam');
        /* Paypal */
        $currencyRate   = (double)($base_helper->getParameter('vlance_system.currency_rate')    ? $base_helper->getParameter('vlance_system.currency_rate') : 0);
        $paypalFee      = (double)($base_helper->getParameter('vlance_system.paypal_fee')       ? $base_helper->getParameter('vlance_system.paypal_fee')    : 0);
        
        /* Validation */
        // Check if user is authenticated
        /* @var $current_user Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(!is_object($current_user)){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.bid.award.error.required_login', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        // Check if current user is the job owner or Admin
        if (!$job->isOwner($current_user) && false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.bid.award.error.must_be_job_owner', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        // Check if job is still Open to be awarded
        if ($job->getStatus() != Job::JOB_OPEN) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.bid.award.error.job_not_open', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        // Create jobcash for the job if neccessary
        if(is_null($job->getCash())){
            $jobcash = new JobCash();
            $jobcash->setJob($job);
            $jobcash->setBalance(0);
            $em->persist($jobcash);
            $em->flush();
        }
        /* END Validation */
        
        /**
         * STEP 1: Award job to the freelancer
         */
        // Update bid
        $awardedBid->setAwardedAt(new \DateTime('now'));
        $em->persist($awardedBid);
        
        /* Update job's working status */
        $job->setStatus(Job::JOB_AWARDED);
        $job->setWorker($freelancer);
        $job->setBudgetBided($awardedBid->getAmount());
        $em->persist($job);
                    
        // Update freelancer (worker) profile
        $freelancer->setNumBid($freelancer->getNumBid() + 1);
        $em->persist($freelancer);
        $em->flush();
        
        /* Call dispatch event when job is awarded */
        /* Create workshop */
        /* @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');
        $workshop = $awardedBid->getWorkshop();
        if(is_null($workshop)){
            $dispatcher->dispatch(JobEvent::JOB_AWARDED_SUCCESS, new BidEvent($awardedBid, $request));
        }
        // Mixpanel
        $base_helper->trackAll(NULL, "Awarded");
        
        /* Sending vLance message */
        $assign_msg_client      = $base_helper->createSystemMessage($awardedBid, $client, "Bạn đã giao việc thành công cho " . $freelancer->getFullName() . ".");
        $assign_msg_freelancer  = $base_helper->createSystemMessage($awardedBid, $freelancer, $client->getFullName() . ' đã giao việc cho bạn.');
        /** END STEP 1: Award job to the freelancer */
        
        /** 
         * STEP 2: Create/Update payment transaction 
         **/
        /* In case DC Job */
        if($job->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($job->getUpfrontAt())){
            /**
             * Về việc xử lý tiền sau khi chọn được FL:
             * - (DONE) Nếu con số awarded và jobcash bằng nhau thì chỉ cần chuyển trạng thái payment sang escrowed
             * - Nếu con số awarded và jobcash khác nhau thì:
             *      + (DONE) Nếu chọn ít hơn, sẽ giảm jobcash xuống bằng cách thêm giao dịch refund chuyển tiền từ jobcash về usercash khách hàng
             *      + (DONE) Nếu chọn nhiều hơn thì vẫn để jobcash như vậy, tạo thêm giao dịch deposit (type 1) chờ nạp tiền thiếu vào jobcash.
             *        khi nào CL nạp tiền thì admin chuyển status của trans deposit này sang 4, hệ thống tự động thêm trans escrow nữa, và tăng jobcash lên con số tổng mới
             */

            /* If job is funded with the exact amount, then update job status to working */
            if($job->getCash()->getBalance() == $awardedBid->getAmount()){
                // Update payment status
                $job->setPaymentStatus(Job::ESCROWED);
                $job->setStatus(Job::JOB_WORKING);
                $em->persist($job);
                $em->flush();
                
                // Send in-system notification
                $exact_msg_client       = $base_helper->createSystemMessage($awardedBid, $client, $this->get('translator')->trans('controller.job.escrow.message.escrow_success_client',     array("%%amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance(), "VNĐ")), 'vlance'));
                if($job->getCategory()->getId() === 48){
                    $warning            = $base_helper->createSystemMessage($awardedBid, $client, "Lưu ý: Trong trường hợp bạn hủy dự án khi dự án đang thực hiện, vLance sẽ thu của bạn 10% phí quản lý.");
                }
                $exact_msg_freelancer   = $base_helper->createSystemMessage($awardedBid, $freelancer, $this->get('translator')->trans('controller.job.escrow.message.escrow_success_freelancer', array("%%amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance(), "VNĐ")), 'vlance'));
                
                // Send email confirm job AWARDED and was funded COMPLETELY
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_and_funded_complete__client.html.twig", 
                        array(
                            'job'           => $job, 
                            'bid'           => $awardedBid,
                            'freelancer'    => $freelancer,
                            'amount'        => $job->getCash()->getBalance(),
                            'pixel_tracking'=> $base_helper->mixpanelTrackMail("Mail Open - bid_awarded_and_funded_complete__client", $client->getId())
                        ), 
                        $email_hotro, 
                        $client->getEmail()
                );
                // to freelancer (win)
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_and_funded_complete__freelancer.html.twig", 
                        array(
                            'job'       => $job, 
                            'bid'       => $awardedBid,
                            'amount'    => $job->getCash()->getBalance()
                        ), 
                        $email_hotro, 
                        $freelancer->getEmail(), 
                        $bcc
                );
            }
            /* If job is funded more than awarded amount, send the rest to client's usercash, then update job status to working */
            elseif($job->getCash()->getBalance() > $awardedBid->getAmount()){
                // Update payment status
                $job->setPaymentStatus(Job::ESCROWED);
                $job->setStatus(Job::JOB_WORKING);
                $em->persist($job);
                
                // Send in-system notification
                $escrow_success_msg_client      = $base_helper->createSystemMessage($awardedBid, $client, $this->get('translator')->trans('controller.job.escrow.message.escrow_success_client', array("%%amount%%" => $base_helper->formatCurrency($awardedBid->getAmount(), "VNĐ")), 'vlance'));
                $refund_different_msg_client    = $base_helper->createSystemMessage($awardedBid, $client, $this->get('translator')->trans('controller.job.escrow.message.escrow_refund_different_client', array("%%refund_diff_amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance() - $awardedBid->getAmount(), "VNĐ")), 'vlance'));
                $escrow_success_msg_freelancer  = $base_helper->createSystemMessage($awardedBid, $freelancer, $this->get('translator')->trans('controller.job.escrow.message.escrow_success_freelancer', array("%%amount%%" => $base_helper->formatCurrency($awardedBid->getAmount(), "VNĐ")), 'vlance'));
                if($job->getCategory()->getId() === 48){
                    $warning                    = $base_helper->createSystemMessage($awardedBid, $client, "Lưu ý: Trong trường hợp bạn hủy dự án khi dự án đang thực hiện, vLance sẽ thu của bạn 10% phí quản lý.");
                }
                
                // Transfert partial upfront money from project back to client's usercash
                $refund_diff_trans = new Transaction();
                $refund_diff_trans->setType(Transaction::TRANS_TYPE_REFUND);
                $refund_diff_trans->setAmount($job->getCash()->getBalance() - $awardedBid->getAmount());
                $refund_diff_trans->setReceiver($client);
                $refund_diff_trans->setJob($job);
                $refund_diff_trans->setComment($this->get('translator')->trans('controller.job.escrow.transaction.comment_refund_different_job_cash', array(), 'vlance'));
                $refund_diff_trans->setStatus(Transaction::TRANS_TERMINATED);
                $em->persist($refund_diff_trans);
                
                $job_cash = $job->getCash();
                $job_cash->setBalance($awardedBid->getAmount());
                $em->persist($job_cash);
                
                $user_cash = $client->getCash();
                $user_cash->setBalance($user_cash->getBalance() + $refund_diff_trans->getAmount());
                $em->persist($user_cash);
                
                $em->flush();
                
                // Send email confirm job AWARDED and was funded COMPLETELY
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_and_funded_complete_with_partial_refund__client.html.twig", 
                        array(
                            'job'           => $job, 
                            'bid'           => $awardedBid,
                            'freelancer'    => $freelancer,
                            'amount'        => $job->getCash()->getBalance(),
                            'refund'        => $refund_diff_trans->getAmount(),
                            'pixel_tracking'=> $base_helper->mixpanelTrackMail("Mail Open - bid_awarded_and_funded_complete_with_partial_refund__client", $client->getId())
                        ), 
                        $email_hotro, 
                        $client->getEmail()
                );
                // to freelancer (win)
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_and_funded_complete__freelancer.html.twig", 
                        array(
                            'job'       => $job, 
                            'bid'       => $awardedBid,
                            'amount'    => $job->getCash()->getBalance(),
                        ), 
                        $email_hotro, 
                        $freelancer->getEmail(), 
                        $bcc
                );
            }
            /* If job is partially funded, notify client to fund more */
            elseif($job->getCash()->getBalance() < $awardedBid->getAmount()){
                // Add additional deposit transaction
                $add_trans = new Transaction();
                $add_trans->setType(Transaction::TRANS_TYPE_DEPOSIT);
                $add_trans->setAmount($awardedBid->getAmount() - $job->getCash()->getBalance());
                $add_trans->setReceiver($client);
                $add_trans->setJob($job);
                $add_trans->setComment($this->get('translator')->trans('controller.job.escrow.transaction.comment_add_escrow_partial_client', array(), 'vlance'));
                $add_trans->setStatus(Transaction::TRANS_WAITING_PAYMENT);
                $em->persist($add_trans);
                $em->flush();
                
                // Send in-system notification
                $escrow_partial_msg_client = $base_helper->createSystemMessage($awardedBid, $client, 
                        $this->get('translator')->trans('controller.job.escrow.message.escrow_partial_client', array(
                            "%%total_amount%%"  => $base_helper->formatCurrency($awardedBid->getAmount(), "VNĐ"),
                            "%%funded_amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance(), "VNĐ"),
                            "%%add_amount%%"    => $base_helper->formatCurrency($awardedBid->getAmount() - $job->getCash()->getBalance(), "VNĐ")
                        ), 'vlance') . "<br/>"
                        . "<br/>"
                        . $this->get('translator')->trans('controller.job.escrow.message.make_payment_guide', array(
                            "%%jobId%%"         => $job->getId(),
                            "%%currencyRate%%"  => number_format($currencyRate,0,',','.'),
                            "%%paypalFee%%"     => number_format($paypalFee, 1,',','.'),
                            "%%usdAmount%%"     => number_format($add_trans->getAmount() / $currencyRate / (1 - $paypalFee/100), 2,',','.')
                        ), 'vlance')
                );
                if($job->getCategory()->getId() === 48){
                    $warning                    = $base_helper->createSystemMessage($awardedBid, $client, "Lưu ý: Trong trường hợp bạn hủy dự án khi dự án đang thực hiện, vLance sẽ thu của bạn 10% phí quản lý.");
                }
                $escrow_partial_msg_freelancer = $base_helper->createSystemMessage($awardedBid, $freelancer, $this->get('translator')->trans('controller.job.escrow.message.escrow_partial_freelancer', array(
                    "%%total_amount%%"  => $base_helper->formatCurrency($awardedBid->getAmount(), "VNĐ"),
                    "%%funded_amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance(), "VNĐ"),
                    "%%add_amount%%"    => $base_helper->formatCurrency($awardedBid->getAmount() - $job->getCash()->getBalance(), "VNĐ")
                ), 'vlance'));
                
                
                // Send email confirm job AWARDED but need to add more fund
                // To client
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_need_add_fund__client.html.twig",
                        array(
                            'job'               => $job,
                            'current_job_cash'  => $job->getCash()->getBalance(),
                            'add_fund'          => $add_trans->getAmount(),
                            'paypal'            => array(
                                'currencyRate'      => $currencyRate,
                                'paypalFee'         => $paypalFee
                            ),
                            'pixel_tracking'    => $base_helper->mixpanelTrackMail("Mail Open - bid_awarded_need_add_fund__client", $client->getId())
                        ),
                        $email_hotro,
                        $client->getEmail()
                );
                // to freelancer (win)
                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/job_awarded:bid_awarded_need_add_fund__freelancer.html.twig", array(
                            'job'               => $job, 
                            'bid'               => $awardedBid,
                            'current_job_cash'  => $job->getCash()->getBalance(),
                            'add_fund'          => $add_trans->getAmount()
                        ), 
                        $email_hotro, 
                        $freelancer->getEmail(), 
                        $bcc
                );
            }

            $em->flush();
        }
        /* In case VC Job */
        else{
            // Send email confirm job AWARDED but has not been FUNDED
            // To client
            $this->get('vlance.mailer')->sendTWIGMessageBySES(
                    "VlanceJobBundle:Email/job_awarded:bid_awarded_but_not_funded__client.html.twig",
                    array(
                        'job'       => $job,
                        'bid'       => $awardedBid,
                        'paypal'    => array(
                            'currencyRate'  => $currencyRate,
                            'paypalFee'     => $paypalFee
                        ),
                        'pixel_tracking' => $base_helper->mixpanelTrackMail("Mail Job-funding-request Open", $client->getId())
                    ),
                    $email_hotro,
                    $client->getEmail()
            );
            // to freelancer (win)
            $this->get('vlance.mailer')->sendTWIGMessageBySES(
                    "VlanceJobBundle:Email/job_awarded:bid_awarded_but_not_funded__freelancer.html.twig", array(
                        'job' => $job, 
                        'bid' => $awardedBid
                    ), 
                    $email_hotro, 
                    $freelancer->getEmail(), 
                    $bcc
            );
        
            // Add new Escrow Request Transaction
            $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('id' => $job->getId()));
            $transaction = new Transaction();
            $transaction->setJob($job);
            $transaction->setReceiver($job->getAccount());
            $transaction->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $transaction->setAmount($job->getBudgetBided());
            $transaction->setStatus(Transaction::TRANS_WAITING_PAYMENT);
            $transaction->setType(Transaction::TRANS_TYPE_DEPOSIT);
            $transaction->setComment("VC".$job->getId());
            $transaction->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 1)));

            // Dispatch After TRANS_DEPOSIT_BEFORE_CREATE event
            $event_before = new TransactionEvent($transaction, $request);
            $dispatcher->dispatch(VlanceEvents::TRANS_DEPOSIT_BEFORE_CREATE, $event_before);

            // If there is a response in one of the event listener -> return to cancel create transaction
            if (($response = $event_before->getResponse())) {
                return $response;
            }
            $em->persist($transaction);

            $job->setPaymentStatus(\Vlance\JobBundle\Entity\Job::ESCROWING);
            $em->persist($job);
            
            $em->flush();
            
            // Dispatch After TRANS_DEPOSIT_AFTER_CREATE event
            $event_after = new TransactionEvent($transaction, $request);
            $dispatcher->dispatch(VlanceEvents::TRANS_DEPOSIT_AFTER_CREATE, $event_after);

            // Send deposit guide to client, in the workroom with selected Freelancer.
            // Note: Workshop is already open, so no need to create one.
            foreach($job->getWorkshopsJob() as $room){
                if($job->getWorker()->getId() == $room->getWorker()->getId()){
                    /* For client  */
                    $deposit_msg_client = $base_helper->createSystemMessage(
                        $job->getAwardedBid(), 
                        $job->getAccount(), 
                        "Số tiền bạn cần nạp cho dự án là: <b>" . $base_helper->formatCurrency($job->getAwardedBid()->getAmount(), "VNĐ") . "</b>.
                        vLance hiện đang hỗ trợ 3 hình thức nạp tiền dự án như sau:
                        <br/><b>Cách 1: Nạp tiền trực tiếp</b>
                        Tại địa chỉ văn phòng của vLance.vn:
                        <i>Tầng 4, số 2, ngõ 68, đường Nam Đồng, phường Nam Đồng, quận Đống Đa, Hà Nội.</i>
                        <br/><b>Cách 2: Chuyển khoản ngân hàng</b>
                        Vui lòng ghi rõ mã số nạp tiền <b>VC".$job->getId()."</b> trong nội dung chuyển khoản.
                        Chủ tài khoản: Trần Ngọc Tuân
                        <br/>1. Vietcombank:
                        <i>Số tài khoản: 0011004141791
                        Chi nhánh Sở giao dịch Hà Nội - Ngân Hàng TMCP Ngoại Thương Vietcombank</i>
                        <br/>2. ACB:
                        <i>Số tài khoản: 228085579
                        Chi nhánh Hà Nội - Ngân hàng TMCP Á Châu ACB</i>
                        <br/>3. Techcombank:
                        <i>Số tài khoản: 19030881539011
                        Chi nhánh Hà Nội - Ngân hàng TMCP Kỹ Thương Việt Nam Techcombank</i>
                        <br/><b>Cách 3: Qua Paypal</b>
                        Bạn có thể thực hiện giao dịch qua tài khoản Paypal của vLance.vn: <i>invoice@vlance.vn</i>, số tiền được tính theo công thức sau:
                        <i>Số tiền USD cần nạp = Giá trị dự án theo VND / " . number_format($currencyRate,0,',','.') . " / (1 - " . number_format($paypalFee, 1,',','.') . "%</i>
                        Cụ thể nếu thanh toán qua Paypal, số tiền bạn phải nạp là: <b>".number_format($job->getAwardedBid()->getAmount() / $currencyRate / (1 - $paypalFee/100), 2,',','.')." USD</b>.
                        <br/><b><i>Lưu ý: <br/> - Freelancer chỉ bắt đầu làm việc sau khi dự án được nạp tiền. Vì vậy hãy nạp tiền ngay để dự án của bạn có thể bắt đầu trong thời gian sớm nhất.<br/> - Trong trường hợp bạn hủy dự án khi dự án đang thực hiện, vLance sẽ thu 5% phí quản lý.</i></b>
                        <br/>Thân mến,"
                    );
                }
            }
            // END Send deposit guide to client
            
            // Gui mail cho admin
            $template = 'VlancePaymentBundle:Email/job_escrow:admin_escrow_job.html.twig';
            $context = array(
                'transaction' => $transaction
            );
            $from = $this->container->getParameter('from_email');
            $to = $this->container->getParameter('to_email');            
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
            // END Gui mail cho admin
            
            // If we need to redirect to somewhere
            if (($response = $event_after->getResponse())) {
                return $response;
            }
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('message.award.success', array(), 'vlance'));
        
        /* Sending notification email to freelancers who have not been AWARDED */
        $bids_notselect =  $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job ,'awardedAt' => null));
        foreach($bids_notselect as $bid) {  /* @var $bid Bid */
            $this->get('vlance.mailer')->sendTWIGMessageBySES(
                    "VlanceJobBundle:Email/job_awarded:bid_not_awarded__freelancer.html.twig", 
                    array(
                        'job' => $job, 
                        'bid' => $bid, 
                        'freelancer' => $awardedBid->getAccount()->getFullName()
                    ),
                    $email_hotro, 
                    $bid->getAccount()->getEmail()
            );
        }
        return $this->redirect($this->generateUrl('workshop_bid', array('job_id' => $job->getId(),'bid' => $job->getAwardedBid()->getId())));
    }
    
    /**
     * 
     * @Route("/deactive/{id}", name="bid_deactive_admin")
     * @Method("GET")
     * @Template("VlanceJobBundle:Bid:admin/bid_deactive.html.php",engine="php")
     */
    public function deactiveAction($id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $bid = $em->getRepository("VlanceJobBundle:Bid")->find($id);
        if (!$bid) {
            return new Response("");
        }
        return array(
            'bid' => $bid,
            '_csrf_token' => $this->get('form.csrf_provider')->generateCsrfToken($this->generateUrl("Vlance_JobBundle_AdminBid_object",array("pk" => $bid->getId(), "action" => "deactive"))),
            "confirm_url" => $this->generateUrl("Vlance_JobBundle_AdminBid_object",array("pk" => $bid->getId(), "action" => "deactive")),
        );
    }

    /************* BID ONSITE *****************/
    /**
     * Displays a form to make a bid onsite.
     *
     * @Route("/onsite/new/{id}", name="bid_onsite_new")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/onsite:bid_form.html.php", engine="php")
     */
    public function newBidOnsiteAction($id, $bid_form = null)
    {
        /* @var $acc Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc,'job' => $job));
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($job->getCloseAt(), $now);
        
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response('<div class="login-bid">'.$this->get('translator')->trans('view_job_page.job_detail.login_send_bid', array(), 'vlance').' <a href="'.$this->generateUrl('fos_user_security_login').'">'.$this->get('translator')->trans('view_job_page.job_detail.login', array(), 'vlance').'</a></div>');
        } 
        
        if($bid) {
            return new Response($this->get('translator')->trans('controller.bided',array(),'vlance'));
        } 
        
        if($remain == false) {
            return new Response($this->get('translator')->trans('controller.expire',array(),'vlance'));
        }
                
        // Neu form chua co du lieu
        if($bid_form == null) {
            $entity = new Bid();
            $form = $this->createForm(new BidOnsiteType(), $entity);
            
//            //Lấy giá trị đã lưu trong form khi gặp lỗi
//            if(isset($_COOKIE['Bid_Onsite_Data'])){
//                foreach(json_decode($_COOKIE['Bid_Onsite_Data']) as $d){
//                    $data[] = $d;
//                }
//                $form->get('description')->setData($data[3]);
//            }
        } else {
            $form = $bid_form;
//            
//            //Lấy giá trị đã lưu trong form khi gặp lỗi
//            if(isset($_COOKIE['Bid_Data'])){
//                foreach(json_decode($_COOKIE['Bid_Data']) as $d){
//                    $data[] = $d;
//                }
//                $form->get('description')->setData($data[3]);
//            }
        }
        
        $response = array(
            'job'           => $job,
            'form'          => $form->createView(),
            'id'            => $id,
            'budget'        => $job->getBudget(),
        );
        
        return $this->container->get('templating')->renderResponse("VlanceJobBundle:Job/onsite:bid_form.html.php", $response);
    }
    
    /**
     * Submit a new Bid entity.
     *
     * @Route("/onsite/submit/{id}", name="bid_onsite_submit")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createBidOnsiteAction(Request $request,$id)
    {
        
        $em = $this->getDoctrine()->getManager();
        /* @var $acc_login Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        /*@var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneById($id);
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $entity Bid */
        $entity  = new Bid();
        $entity->setAccount($acc_login);
        $entity->setJob($job);
        $entity->setDuration(7);
        $entity->setAmount($job->getBudget());
        $entity->setIntroDescription('Null - Job Onsite');
        $form = $this->createForm(new BidOnsiteType(), $entity);
        $form->bind($request);
        
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $acc_login,'job' => $job));
        
        // Already made bid, not allow bidding more
        if(count($bid) > 0) {
            $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.notentity',array(),'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage')); // TODO redirect to that job page
        }
        
        // Verify if files size are less than 10MB
        $file_size_error = false;
        if(($fs = $entity->getFiles())){
            foreach ($fs as $f) {
                if(($file = $f->getFile())){
                    if($file->getSize() > 10485760){
                        $file_size_error = true;
                        break;
                    }
                }
            }
        } 
        
        if($file_size_error){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('profile.edit_profile.error_file', array(), 'vlance'));
        } else {
            if ($form->isValid()) { 
                $files = $entity->getFiles();
                foreach ($files as $file) {
                    $file->setBid($entity);
                    $em->persist($file);
                }
                $em->persist($entity);

                $telephone = $request->request->get('input_telephone');
                $skype = $request->request->get('input_skype');
                
                if(!$acc_login->getTelephoneVerifiedAt()){
                    if($acc_login->getTelephone() != $telephone){
                        $acc_login->setTelephone($telephone);
                    }
                }
                
                $acc_login->setSkype($skype);
                $em->persist($acc_login);

                $em->flush();

                /** Tracking Event "Applied CV" **/
                // Mixpanel
                $this->get('vlance_base.helper')->trackAll($acc_login, "Applied CV");
                // Facebook
                $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                        '<!-- Facebook Conversion Code for Made bid -->
                        <script>(function() {
                          var _fbq = window._fbq || (window._fbq = []);
                          if (!_fbq.loaded) {
                            var fbds = document.createElement("script");
                            fbds.async = true;
                            fbds.src = "//connect.facebook.net/en_US/fbds.js";
                            var s = document.getElementsByTagName("script")[0];
                            s.parentNode.insertBefore(fbds, s);
                            _fbq.loaded = true;
                          }
                        })();
                        window._fbq = window._fbq || [];
                        window._fbq.push(["track", "6023542039885", {"value":"0.00","currency":"VND"}]);
                        </script>
                        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023542039885&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                    )
                ));
                /** END Tracking Event "Applied CV" **/

                // Gui email
                $from_email = $this->container->getParameter('from_email');

                //Send email client
                $to_client = $job->getAccount()->getEmail();
                $template_client = 'VlanceJobBundle:Email/onsite:bid_job.html.twig';
                $context_client = array(
                    'job'   => $job,
                    'bid'   => $entity,
                    'url'   => $this->container->get('router')->generate('job_onsite_view', array('hash' => $job->getHash()), true),
                );
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from_email, $to_client);

                $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.bid.create.message',array('%here%' => '<a href="'.$this->generateUrl('freelance_job_list').'">tại đây</a>'),'vlance'));
                return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $job->getHash()), true));
            }
        }
        
        // From no validation
        return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $job->getHash()), true));
    }
    
    /************* END BID ONSITE *****************/
    
    /************* BID RATE *****************/
    /**
     * @Route("/bid/rating", name="rating_bid")
     * @Method("POST")
     */
    public function ratingBid(Request $request)
    {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();

        $b_id = $request->request->get('bid');
        $rating = $request->request->get('rating');

        if($b_id === ""){
            return new JsonResponse(array('error' => 101));
        }
        if($rating === ""){
            return new JsonResponse(array('error' => 102));
        }

        if(!is_numeric($rating)){
            return new JsonResponse(array('error' => 103));
        }

        /* @var $bid Bid */
        $bid = $em->getRepository('VlanceJobBundle:Bid')->findOneById($b_id);
        if(!is_object($bid)){
            return new JsonResponse(array('error' => 104));
        }

        $job = $bid->getJob();
        if($job->getAccount() != $acc_login){
            if(\Vlance\BaseBundle\Utils\HasPermission::hasAdminPermission($acc_login) === FALSE){
                return new JsonResponse(array('error' => 105));
            }
        }

        /* @var $rated BidRate */
        $rated = $em->getRepository('VlanceJobBundle:BidRate')->findOneBy(array('bid' => $bid->getId()));
        if(is_object($rated)){
            $rated->setRating($rating);
            $em->persist($rated);
            $em->flush();
            return new JsonResponse(array('error' => 0, 'status' => 'created'));
        }

        $now = new \DateTime('now');
        $rate = new BidRate();
        $rate->setBid($bid);
        $rate->setSender($job->getAccount());
        $rate->setRating($rating);
        $rate->setRatingAt($now);
        $rate->setJob($job);
        $em->persist($rate);
        $em->flush();

        //Check bid rate
        $this->get('vlance_job.helper')->checkBidRate($job);
        
        // Gui email
        $from_email = $this->container->getParameter('from_email');
        $to = $bid->getAccount()->getEmail();
        $template = 'VlanceJobBundle:Email:bid_rate.html.twig';
        $context = array(
            'job'   => $job,
            'bid'   => $bid,
            'rate'  => $rate,
            'url'   => $this->container->get('router')->generate('job_show_freelance_job',array('hash' => $job->getHash()), true),
        );
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to);
        
        return new JsonResponse(array('error' => 0, 'newBalance' => $job->getAccount()->getCredit()->getBalance(), 'numRated' => count($job->getBidRates())));
    }

    /************* END BID RATE *****************/
    
    /**
     * Lists all Bid entities.
     *
     * @Route("/{filters}", name="bid_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request, $filters = "")
    {
        $param = $this->get('vlance_base.url')->pasteParameter($filters);
        $this->get('session')->set('filter',$param);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceJobBundle:Bid')->findBy($param);
        return array(
            'entities' => $entities,
        );
    }
   
}
