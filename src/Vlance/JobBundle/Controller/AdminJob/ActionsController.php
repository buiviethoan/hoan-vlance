<?php

namespace Vlance\JobBundle\Controller\AdminJob;

use Admingenerated\VlanceJobBundle\BaseAdminJobController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Vlance\JobBundle\Entity\Bid;
use Vlance\BaseBundle\Utils\HasPermission;
use Vlance\PaymentBundle\Entity\Transaction;

class ActionsController extends BaseActionsController
{
    /*
     * Hide job from public
     */
    public function attemptObjectDeactive($id) {
        try {
            /* @var $job \Vlance\JobBundle\Entity\Job */
            $job = $this->getObject($id);

            if ('POST' == $this->get('request')->getMethod()) {
                // Check CSRF token for action
                $intention = $this->getRequest()->getRequestUri();
                $this->isCsrfTokenValid($intention);
                //Deactive job
                $job->setPublish(Job::UNPUBLISH);
                $em = $this->getDoctrine()->getManager();
                $em->persist($job);
                
                //Update account statistic
                $account = $job->getAccount();
                $account->setNumJob($account->getNumJob() - 1);
                
                /* Ban user */
                $isBanned = $this->get('request')->request->get('banned');
                if ($isBanned) {
                    $bannedTo = new \DateTime();
                    $bannedTo->add(date_interval_create_from_date_string('7 days'));
                    $account->setBannedTo($bannedTo);
                }
                $em->persist($account);
                
                $em->flush();
                
                // send email to user
                $reason = $this->get('request')->request->get('reason');
                $context = array(
                    'job'       => $job,
                    'account'   => $job->getAccount(),
                    'reason'    => "admin.job.deactive.reason.$reason",
                    'isBanned'  => $isBanned
                );
                $from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
                
                /*Gui email thong bao cho client*/
                $result = $this->get('vlance.mailer')->sendTWIGMessageBySES("VlanceJobBundle:Email/notify_hide_job:to_job_owner.html.twig", $context, $from, $job->getAccount()->getEmail());
                
                /*Gui email thong bao cho freelancer*/
                $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'isDeclined' => false, 'publish' => Bid::PUBLISH));
                $context_freelancer = array(
                  'job' => $job,
                  'reason' => "admin.job.deactive.reason.$reason",
                );
                foreach($bids as $bid) {
                    $context_freelancer['account'] = $bid->getAccount();
                    $result_free = $this->get('vlance.mailer')->sendTWIGMessageBySES("VlanceJobBundle:Email/notify_hide_job:to_freelancers.html.twig", $context_freelancer, $from, $bid->getAccount()->getEmail());
                }
                
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("admin.job.deactive.success", array('%title%' => $job->getTitle()), 'vlance'));
                return new RedirectResponse($this->generateUrl("freelance_job_list"));
            }

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("admin.job.deactive.error", array('%id%' => $id), 'vlance'));
            return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminJob_list"));
        }

        return $this->render(
            'VlanceJobBundle:AdminJobActions:deactive.html.twig',
            $this->getAdditionalRenderParameters($job, 'deactive') + array(
                "job"           => $job,
                "title"         => $this->get('translator')->trans("admin.job.deactive.title", array('%title%' => $job->getTitle()), 'vlance'),
                "actionRoute"   => "Vlance_JobBundle_AdminJob_object",
                "actionParams"  => array("pk" => $id, "action" => "deactive")
            )
        );
    }
    
    /**
     * This function handles common object actions behaviour like
     * checking CSRF protection token or credentials.
     *
     * To customize your action look into:
     * executeObjectDelete() - holds action logic
     * successObjectDelete() - called if action was successfull
     * errorObjectDelete()   - called if action errored
     */
    protected function attemptObjectDelete($pk)
    {
        try {
            /* @var $Job Job */
            $Job = $this->getObject($pk);

                if ('POST' == $this->get('request')->getMethod()) {
                // Check CSRF token for action
                $intention = $this->getRequest()->getRequestUri();
                $this->isCsrfTokenValid($intention);
                // Update lai so cong viec da tao
                $account = $Job->getAccount();
                $account->setNumJob($account->getNumJob()-1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($account);
                $em->flush();
                
                $this->executeObjectDelete($Job);
                
                return $this->successObjectDelete($Job);
            }

        } catch (\Exception $e) {
            return $this->errorObjectDelete($e, $Job);
        }

        return $this->render(
            'VlanceJobBundle:AdminJobActions:index.html.twig',
            $this->getAdditionalRenderParameters($Job, 'delete') + array(
                "Job" => $Job,
                "title" => $this->get('translator')->trans(
                    "action.object.delete.confirm",
                    array('%name%' => 'delete'),
                    'Admingenerator'
                ),
                "actionRoute" => "Vlance_JobBundle_AdminJob_object",
                "actionParams" => array("pk" => $pk, "action" => "delete")
            )
        );
    }
    
    /**
     * This is called when action is successfull
     * Default behavior is redirecting to list with success message
     *
     * @param \Vlance\JobBundle\Entity\Job $Job Your \Vlance\JobBundle\Entity\Job object
     * @return Response Must return a response!
     */
    protected function successObjectDelete(\Vlance\JobBundle\Entity\Job $Job)
    {
        $this->get('session')->getFlashBag()->add(
            'success',
            $this->get('translator')->trans(
                "action.object.delete.success",
                array('%name%' => 'delete'),
                'Admingenerator'
            )
        );

        return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminJob_list"));
    }
    
    /**
     * Set a job from Awarded status to Open status.
     * 
     * @param type $id
     * @return RedirectResponse
     */
    public function attemptObjectReopen($id) {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return new RedirectResponse($this->generateUrl('fos_user_security_login'));
        }
         /* @var $current_user Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        if(!isset($id)){
            $this->get('session')->getFlashBag()->add('error',"#100 - Lỗi thiếu id job");
            return new RedirectResponse($this->generateUrl("500_page"));
        }
        if(!is_numeric($id)){
            $this->get('session')->getFlashBag()->add('error',"#101 - Lỗi id job");
            return new RedirectResponse($this->generateUrl("500_page"));
        }
        
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneById($id);
        if(!$job instanceof Job){
            $this->get('session')->getFlashBag()->add('error',"#102 - Lỗi không có Job");
            return new RedirectResponse($this->generateUrl("500_page"));
        }
        
        if($job->getAccount() != $current_user && HasPermission::hasAdminPermission($current_user) == FALSE) {
            $this->get('session')->getFlashBag()->add('error',"#103 - Lỗi không có quyền với Job này.");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getStatus() != Job::JOB_AWARDED){
            $this->get('session')->getFlashBag()->add('error',"#110 - Lỗi trạng thái việc không phải là Awarded");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getPaymentStatus() != Job::ESCROWING){
            $this->get('session')->getFlashBag()->add('error',"#120 - Lỗi trạng thái thanh toán không phải là Escrowing");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getWorker() == null){
            $this->get('session')->getFlashBag()->add('error',"#130 - Lỗi chưa có Worker");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getAwardedBid() == null){
            $this->get('session')->getFlashBag()->add('error',"#140 - Lỗi chưa có Bid được Awarded");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW){
            $this->get('session')->getFlashBag()->add('error',"#150 - Lỗi vì đây là dự án DC (upfront)");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getType() != Job::TYPE_BID){
            $this->get('session')->getFlashBag()->add('error',"#160 - Lỗi không phải là dự án Bidding");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        if($job->getCash()->getBalance() != 0){
            $this->get('session')->getFlashBag()->add('error',"#170 - Lỗi vì số dư của Jobcash khác 0 VNĐ");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        // END Validators
            
        $deposit_trans = $job->getTransactionsTypeDeposit();
        foreach($deposit_trans as $t){ /* @var $t Transaction */
            if($t->getStatus() != Transaction::TRANS_WAITING_PAYMENT && $t->getStatus() != Transaction::TRANS_STATUS_CANCEL){
                $this->get('session')->getFlashBag()->add('error',"#180 - Lỗi với các giao dịch. Có nhiều giao dịch nạp tiền phức tạp, nên bảo kỹ thuật chuyển trạng thái theo cách thủ công.");
            return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
            }
        }
        
        // Update statistic for worker
        /* @var $worker Account */
        $worker = $job->getWorker();
        $worker->setNumJob($worker->getNumJob() - 1);
        $em->persist($worker);
        
        // Set this job is open
        $job->setStatus(Job::JOB_OPEN);
        $job->setPaymentStatus(Job::WAITING_ESCROW);
        $job->setWorker(null);
        $job->setBudgetBided(0);
        $em->persist($job);

        // Remove Award label on the Bid
        $awarded_bid = $job->getAwardedBid();
        $awarded_bid->setAwardedAt(null);
        $em->persist($awarded_bid);
        
        foreach($deposit_trans as $tr){ /* @var $tr Transaction */
            $tr->setStatus(Transaction::TRANS_STATUS_CANCEL);
            $em->persist($tr);
        }
        
        $em->flush();
        
        /* Add message */
        $base_helper = $this->get('vlance_base.helper');
        $base_helper->createSystemMessage($awarded_bid, $job->getAccount(), $this->get('translator')->trans('controller.job.admin.status.cancel_award.message_client_and_worker', array("%%user%%" => $current_user->getFullName()), 'vlance'));
        $base_helper->createSystemMessage($awarded_bid, $worker,            $this->get('translator')->trans('controller.job.admin.status.cancel_award.message_client_and_worker', array("%%user%%" => $current_user->getFullName()), 'vlance'));
        
        /* Send email notifications */
        $from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
        // to Job owner
        $email_cl = $this->get('vlance.mailer')->sendTWIGMessageBySES(
                "VlanceJobBundle:Email/notify_cancel_award:to_job_owner.html.twig",
                array(
                    'account'   => $job->getAccount(),
                    'job'       => $job
                ),
                $from,
                $job->getAccount()->getEmail()
        );
        // to awarded freelancer
        $awd_fl = $this->get('vlance.mailer')->sendTWIGMessageBySES(
                "VlanceJobBundle:Email/notify_cancel_award:to_awarded_freelancer.html.twig",
                array(
                    'account'   => $worker,
                    'job'       => $job
                ),
                $from,
                $worker->getEmail()
        );
        // to other freelancers
        foreach ($job->getBidsValid() as $bid){ /* @var $bid Bid */
            if($bid->getAccount()->getId() != $worker->getId()){
                $oth_fl = $this->get('vlance.mailer')->sendTWIGMessageBySES(
                        "VlanceJobBundle:Email/notify_cancel_award:to_other_freelancers.html.twig",
                        array(
                            'account'   => $bid->getAccount(),
                            'job'       => $job
                        ),
                        $from,
                        $bid->getAccount()->getEmail()
                );
            }
        }
        
        $this->get('session')->getFlashBag()->add('success',"Việc '{$job->getTitle()}' đã được hủy chọn Freelancer");
        return new RedirectResponse($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
    
    public function attemptObjectLink($id) {
        $job = $this->getObject($id);
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
    
    public function attemptObjectReopenrefun ($id) {
        
        if ("POST" == $this->getRequest()->getMethod()) {
            $intention = $this->getRequest()->getRequestUri();
            $this->isCsrfTokenValid($intention);
        }
        $job = $this->getObject($id);
        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('Vlance_JobBundle_AdminJob_object', array('pk' => $id, 'action' => 'reopenrefun')))
                ->add('refun', 'text', array(
                    'label' => 'Refun to client',
                ))
                ->add('pay', 'text', array(
                    'label' => 'Pay to freelancer',
                ))
                ->add('pay', 'text', array(
                    'label' => 'Pay to freelancer',
                ));
        
        return $this->render('VlanceJobBundle:AdminJobEdit:refun.html.twig', $this->getAdditionalRenderParameters() + array(
            'job'                       => $job,
            'form'                      => $form->createView(),
        ));
    }
}
