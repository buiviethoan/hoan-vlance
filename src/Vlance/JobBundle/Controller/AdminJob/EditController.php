<?php

namespace Vlance\JobBundle\Controller\AdminJob;

use Admingenerated\VlanceJobBundle\BaseAdminJobController\EditController as BaseEditController;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\JobBundle\Entity\Message;

class EditController extends BaseEditController
{
    public function updateAction($pk)
    {
        $Job = $this->getObject($pk);

        if (!$Job) {
            throw new NotFoundHttpException("The Vlance\JobBundle\Entity\Job with id $pk can't be found");
        }
        $publish_old = $Job->getPublish();
        $this->preBindRequest($Job);
        $form = $this->createForm($this->getEditType(), $Job);
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            try {
                
                $this->preSave($form, $Job);
                $this->saveObject($Job);
                $this->postSave($form, $Job);
                //update lai truong numberjob cua freelancer khi truong publish_option thay doi trang thai
                if($publish_old != $Job->getPublish()) {
                    $em = $this->getDoctrine()->getManager();
                    /* @var $account Account */
                    $account = $Job->getAccount();
                    $jobs = $em->getRepository('VlanceJobBundle:Job')->findBy(array('account' => $account, 'publish' => Job::PUBLISH));
                    $account->setNumJob(count($jobs));
                    $em->persist($account);
                    $em->flush();
                }
                
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminJob_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminJob_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminJob_edit", array('pk' => $pk) ));

                        } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $Job);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('VlanceJobBundle:AdminJobEdit:index.html.twig', $this->getAdditionalRenderParameters($Job) + array(
            "Job" => $Job,
            "form" => $form->createView(),
        ));
    }
    
    public function postSave(\Symfony\Component\Form\Form $form, Job $Job) {
        // If we accept job is canceled & refun for client
        if ($Job->getStatus() === Job::JOB_FINISHED && $Job->getCancelStatus() === Job::STATUS_CANCELLED) {
            $em = $this->getDoctrine()->getManager();
            $vlanceSystem = $em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS);
            $jobCash = $Job->getCash();
            
            if ($Job->getPaymentStatus() !== Job::REFUNED) {
                $Job->setPaymentStatus(Job::REFUNED);
                $em->persist($Job);
            }
            /**
             * - Create refun transaction
             */
            $refunTrans = new Transaction();
            $refunTrans->setType(Transaction::TRANS_TYPE_REFUND);
            $refunTrans->setAmount($jobCash->getBalance());
            $refunTrans->setReceiver($Job->getAccount());
            $refunTrans->setJob($Job);
            $refunTrans->setSender($vlanceSystem);
            $refunTrans->setComment($this->get('translator')->trans('controller.transaction.refun.comment', array("%%job_title%%" => $Job->getTitle()), 'vlance'));
            $refunTrans->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($refunTrans);
            
            /**
             * - Decrease job cash
             */
            $jobCash->setBalance(0);
            $em->persist($jobCash);
            
            /**
             * Increase user cash
             */
            $userCash = $Job->getAccount()->getCash();
            $userCash->setBalance($userCash->getBalance() + $refunTrans->getAmount());
            $em->persist($userCash);
            
            /**
             * Send message to client & freelancer
             */
            $awardedBid = $em->getRepository('VlanceJobBundle:Job')->getAwardedBid($Job);
            $messageClient = new Message();
            $messageClient->setBid($awardedBid);
            $messageClient->setSender($vlanceSystem);
            $messageClient->setReceiver($Job->getAccount());
            $messageClient->setStatus(Message::NEW_MESSAGE);
            $messageClient->setContent($this->get('translator')->trans('controller.workshop.cancel.success.client', array("%%job_title%%" => $Job->getTitle()), 'vlance'));
            $em->persist($messageClient);
            
            $messageFreelancer = new Message();
            $messageFreelancer->setBid($awardedBid);
            $messageFreelancer->setSender($vlanceSystem);
            $messageFreelancer->setReceiver($Job->getWorker());
            $messageFreelancer->setStatus(Message::NEW_MESSAGE);
            $messageFreelancer->setContent($this->get('translator')->trans('controller.workshop.cancel.success.freelancer', array("%%job_title%%" => $Job->getTitle()), 'vlance'));
            $em->persist($messageFreelancer);
            
            $em->flush();
            
            //Send mail to client : refund seccess
            $from_email = $this->container->getParameter('from_email');
            $to_admin = $this->container->getParameter('to_email');
            $template = 'VlanceJobBundle:Email:admin_refund_client.html.twig';
            $context = array(
                'refunTrans' => $refunTrans,
                'job' => $Job,
            );
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to_admin);
            /**
             * @todo Send email to cilent & freelancer
             */
            if($form->get('sendEmail')->getData() && $Job->getStatus() == Job::JOB_FINISHED && $Job->getCancelStatus() === Job::STATUS_CANCELLED) {
                $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
                $email_context = array('job' => $Job, 'reson' => $form->get('resonCancel')->getData());

                //Send email to freelancer
                $free = $Job->getWorker();
                $template = 'VlanceJobBundle:Email:freelancer_confirm_job_cancel.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $free->getEmail());
                //Send email to client
                $client = $Job->getAccount();
                $email_context = array('job' => $Job);
                $template = 'VlanceJobBundle:Email:client_confirm_job_cancel.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $client->getEmail());
            }
        }
    }
}
