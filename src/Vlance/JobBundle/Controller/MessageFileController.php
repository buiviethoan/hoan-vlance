<?php

namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\JobBundle\Entity\MessageFile;
use Vlance\JobBundle\Form\MessageFileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * MessageFile controller.
 *
 * @Route("/messagefile")
 */
class MessageFileController extends Controller
{

    /**
     * Creates a new MessageFile entity.
     *
     * @Route("/new", name="messagefile_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request)
    {
        $entity  = new MessageFile();
        $form = $this->createForm(new MessageFileType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $uploadAction = $form->get('upload')->isClicked()? 'messagefile_upload': 'workshop_bid';
            $uploadAction = $form->get('cancel')->isClicked()? 'messagefile_cancel': 'workshop_bid';
            
            return $this->redirect($this->generateUrl('workshop_bid'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Upload a new MessageFile entity.
     *
     * @Route("/upload", name="messagefile_upload")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function uploadAction(Request $request)
    {
        $entity  = new MessageFile();
        $form = $this->createForm(new MessageFileType(), $entity);
        $form->bind($request);
       
        $entity->setFile(new UploadedFile($_FILES['vlance_jobbundle_messagetype']['tmp_name']['file'],$_FILES['vlance_jobbundle_messagetype']['name']['file']));
        $entity->setMimeType($_FILES['vlance_jobbundle_messagetype']['type']['file']);
        $entity->preUpload();
        $entity->upload();
        $entity->setFilename($_FILES['vlance_jobbundle_messagetype']['name']['file']);
        $entity->setSize($_FILES['vlance_jobbundle_messagetype']['size']['file']);
        $entity->setPath($entity->getWebPath());

        $em->persist($entity);
        $em->flush();
        
        $response = new JsonResponse();
        $response->setData(array(
                'data' => 'Save file success.',
        ));
        $response->send();
        
        return $response;
        
    }
    
     /**
     * Cancel a new MessageFile entity.
     *
     * @Route("/cancel/{id}", name="messagefile_cancel")
     * @Method("Delete")
     * @Template(engine="php")
     */
    public function cancelAction($id)
    {
        $entity  = new MessageFile();
        $form = $this->createForm(new MessageFileType(), $entity);
        $form->bind($request);
        
        $entity->setFile(new UploadedFile($_FILES['vlance_jobbundle_messagefiletype']['tmp_name']['file'],$_FILES['vlance_jobbundle_messagefiletype']['name']['file']));
        $entity->setMimeType($_FILES['vlance_jobbundle_messagefiletype']['type']['file']);
        $entity->preUpload();
        $entity->upload();
        $entity->setFilename($_FILES['vlance_jobbundle_messagefiletype']['name']['file']);
        $entity->setSize($_FILES['vlance_jobbundle_messagefiletype']['size']['file']);
        $entity->setPath($entity->getWebPath());

        $em->persist($entity);
        $em->flush();
        
    }

    /**
     * Displays a form to create a new MessageFile entity.
     *
     * @Route("/new", name="messagefile_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction()
    {
        $entity = new MessageFile();
        $form   = $this->createForm(new MessageFileType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Deletes a MessageFile entity.
     *
     * @Route("/{id}", name="messagefile_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VlanceJobBundle:MessageFile')->find($id);

            if (!$entity) {
                return $this->redirect($this->generateUrl("404_page"), 301);
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('messagefile'));
    }

    /**
     * Creates a form to delete a MessageFile entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * Lists all MessageFile entities.
     *
     * @Route("/{filters}", name="messagefile")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request,$filters)
    {
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $this->get('session')->set('filter',$params);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceJobBundle:MessageFile')->findBy($params);

        return array(
            'entities' => $entities,
        );
    }
}
