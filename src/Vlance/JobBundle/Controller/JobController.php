<?php
namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\UnitOfWork;
use Doctrine\Common\Persistence\Event\PreUpdateEventArgs;
use Pagerfanta\View\DefaultView;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\JobBundle\Entity\Job;
use Vlance\JobBundle\Entity\JobOnsiteOption;
use Vlance\JobBundle\Entity\JobInvite;
use Vlance\JobBundle\Entity\Bid;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\JobFile;
use Vlance\JobBundle\Form\JobType;
use Vlance\JobBundle\Form\FilterStatusType;
use Vlance\JobBundle\Entity\MessageFile;
use Vlance\AccountBundle\Entity\Category;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\BaseBundle\Event\VlanceEvents;
use Vlance\JobBundle\Event\JobEvent;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;
use Vlance\JobBundle\Entity\Message;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\BaseBundle\Utils\HasPermission;
use Vlance\JobBundle\Form\JobBudgetType;
use Vlance\JobBundle\Form\JobContestType;

use Vlance\PaymentBundle\Entity\CreditExpenseTransaction as CreditExpenseTransaction;
use Vlance\PaymentBundle\Entity\CreditAccount;

/**
 * Job controller.
 *
 * @Route("/")
 */
class JobController extends Controller {

    /**
     * Creates a new Job entity.
     *
     * @Route("/j/new", name="job_new_submit")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job:new.html.php",engine="php")
     */
    public function createAction(Request $request) {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        // If current user is banned
        if ($account->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }

        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobtype', $entity);

        $form->bind($request);
        if ($form->isValid()) {
            /* Xử lý chào giá đăng kí hạn quá 30 ngày */
            //lấy thời gian đăng việc trong form
            $now = new \DateTime('now');
            $closeAt = $form['closeAt']->getData();
            $closeAt = $closeAt->setTime($now->format('H'), $now->format('i'), $now->format('s'));
            //Chuyển sang kiểu int
            $now_int = strtotime(date_format($now, 'Y/m/d H:i:s'));
            $closeAt_int = strtotime(date_format($closeAt, 'Y/m/d H:i:s'));
            //Nếu thời gian đăng kí trong form lớn hơn 30 ngày thì báo lỗi
            if(($closeAt_int - $now_int) > 2592000){
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.job.post.date_error', array(), 'vlance'));
                return array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                    'acc'    => $account
                );
            }
            /* END */
            
            $em = $this->getDoctrine()->getManager();

            $files = $entity->getFiles();
            foreach ($files as $idx => $file) {
                /* @var $file \Vlance\JobBundle\Entity\JobFile */
                if ($file instanceof \Vlance\JobBundle\Entity\JobFile) {
                    $file->setJob($entity);
                    $em->persist($file);
                } else {
                    $entity->getFiles()->remove($idx);
                }
            }
            
            $entity->setAccount($account);
            $account->addJob($entity);
            $account->setNumJob($account->getNumJob() + 1);
            
            // update numberjob in account entity
            $em->persist($account);
            
            //Insert skill
            $skills = explode(',', $_POST['hiddenTagListA']);
            
            // Insert skill khi tai khoan dang nhap them moi skill
            foreach($skills as $skill_new) {
                if(empty($skill_new)) {
                    break;
                }
                $acc_skill = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('title' => $skill_new));
                $entity->addSkill($acc_skill);
            }
            $now = new \DateTime('now');
            $close = $entity->getCloseAt();
            $entity->setCloseAt($close->setTime($now->format('H'), $now->format('i'), $now->format('s')));
            
            if ($request->get('is_private', false)) {
                $entity->setIsPrivate(true);
            }
            if ($request->get('require_escrow', false)) {
                $entity->setRequireEscrow(true);
            }
            if ($request->get('refuse_use_vlance_payment', false)) {
                $entity->setIsWillEscrow(false);
            } else {
                $entity->setIsWillEscrow(true);
            }
            
            /** Test deposit upfront escrow in order to send message to freelancer **/
            $is_test_deposit_client = false;
            if($this->get('vlance_base.helper')->getIsTestDepositClient()){
                // - Increase from 2M to 3M from 19/6/2016
                // - Decrease to 2.5M, and not include "Lập trình web" & "Lập trình phần mềm" from 5/7/2016
                if($entity->getBudget() <= 2500000 && $entity->getCategory()->getId() != 2 && $entity->getCategory()->getId() != 9){ // Not "Lập trình web" & "Lập trình phần mềm"
                    $upfront_deposit_trans = new Transaction();
                    $upfront_deposit_trans->setAmount($entity->getBudget());
                    $upfront_deposit_trans->setReceiver($account);
                    $upfront_deposit_trans->setJob($entity);
                    $upfront_deposit_trans->setType(Transaction::TRANS_TYPE_DEPOSIT_UPFRONT);
                    $upfront_deposit_trans->setStatus(Transaction::TRANS_WAITING_PAYMENT);
                    $upfront_deposit_trans->setComment("Nạp trước 100% dự án");
                    $em->persist($upfront_deposit_trans);
                    
                    $entity->setTestType(Job::TEST_TYPE_UPFRONT_ESCROW);
                    $is_test_deposit_client = true;
                }
            }
            /** END Test deposit upfront escrow in order to send message to freelancer **/
            
            $em->persist($entity);            
            $em->flush();
                    
            /** Tracking event "Create job" **/
            // Mixpanel
            $tracking_array = array(
                'category'                  => ($entity->getCategory())?$entity->getCategory()->getTitle():NULL,
                'budget'                    => $entity->getBudget(),
                'city'                      => ($entity->getCity())?$entity->getCity()->getName():NULL,
                'skills_count'              => count($entity->getSkills()),
                'require_escrow'            => ($entity->getRequireEscrow()) ? 'true' : 'false',
                'private'                   => ($entity->getIsPrivate()) ? 'true' : 'false',
                'use_vlance_payment'        => ($entity->getIsWillEscrow()) ? 'true' : 'false',
                'is_test_deposit_client'    => $is_test_deposit_client ? "true" : "false"
            );
            
            $this->get('vlance_base.helper')->trackAll(NULL, "Create job", $tracking_array);
            
            // Google Adwords
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Google Code for Create job Conversion Page -->
                    <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 925105271;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "6Ml0CMnjg1oQ9_iPuQM";
                    var google_conversion_value = '.$entity->getBudget().'.00;
                    var google_conversion_currency = "VND";
                    var google_remarketing_only = false;
                    /* ]]> */
                    </script>
                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                    </script>
                    <noscript>
                    <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$entity->getBudget().'.00&amp;currency_code=VND&amp;label=6Ml0CMnjg1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                    </div>
                    </noscript>'
                )
            ));
            
            // Facebook
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Facebook Conversion Code for Create job -->
                    <script>(function() {
                      var _fbq = window._fbq || (window._fbq = []);
                      if (!_fbq.loaded) {
                        var fbds = document.createElement("script");
                        fbds.async = true;
                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(fbds, s);
                        _fbq.loaded = true;
                      }
                    })();
                    window._fbq = window._fbq || [];
                    window._fbq.push(["track", "6023661433485", {"value":"'.$entity->getBudget().'.00","currency":"VND"}]);
                    </script>
                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023661433485&amp;cd[value]='.$entity->getBudget().'.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                )
            ));
            /** END Tracking event "Create job" **/        
            
            $template = 'VlanceJobBundle:Email:admin_create_job.html.twig';
            $context = array(
                'job' => $entity,
            );
            $from = $this->container->getParameter('from_email');
            $to = $this->container->getParameter('to_email');
            
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);

            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(JobEvent::JOB_CREATE_COMPLETED, new JobEvent($entity));
            
            $this->get('session')->set('createjob', 'new');
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $entity->getHash())));
        }
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'acc'    => $account
        );
    }
    
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/j/new", name="job_new_obsolet")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newJobObsoletAction($entity = null) {
        return $this->redirect($this->generateUrl('job_new', array()), 301);
    }
    
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/dang-du-an", name="job_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction($entity = null) {
        // Not permission
//        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
//            return $this->redirect($this->generateUrl('fos_user_security_login'));
//        }
        
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        if(is_object($account)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($account->getEmail())){
                if($helper->emailNotFacebook($account->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $account->getId())));
                }
            }
        }
        
//        // If current user is banned
//        if ($account->isBanned()) {
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
//            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
//        }
        
//        $this->get('vlance_base.helper')->trackAll($account, "View Create job page", array(
//           'version'   => '1.20160309'
//        ));
        
        $this->get('vlance_base.helper')->trackAll($account, "View Create job page", array(
            'authenticated' => (is_object($account)) ? 'true' : 'false',
            'version'       => "1.20160606",
            'new_design'    => "true"
         ));
        
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobtype', $entity);
        
        return array(
            'entity'=> $entity,
            'form'  => $form->createView(),
//            'acc'   => $account
        );
    }
    
    /**
     * Update 20160526
     * Creates a new Job Contest entity.
     *
     * @Route("/dang-cuoc-thi/new", name="contest_new_submit")
     * @Method("POST")
     */
    public function createContestAction(Request $request) {
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        // If current user is banned
        if ($account->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }
        
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('vlance_payment.helper');
        
        $number = $request->request->get('num_package');
        if($number === ""){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Đã có lỗi xảy ra. Vui lòng thử lại. (1)', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_contest_new', array(), true));
        }
        
        if(!is_numeric($number)){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Đã có lỗi xảy ra. Vui lòng thử lại. (2)', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_contest_new', array(), true));
        }
        
        if((int)$number < 1 || (int)$number > 3){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Đã có lỗi xảy ra. Vui lòng thử lại. (4)', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_contest_new', array(), true));
        }
        
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobcontesttype', $entity);
        
        //Set day
        $now = new \DateTime('now');
        $days = $helper->getParameter('contest.days.waiting');
        $close = ($now->modify('+'.$days.' days'));
        
        $entity->setAccount($account);
        $entity->setCloseAt($close->setTime($now->format('H'), $now->format('i'), $now->format('s')));
        $entity->setCity($em->getRepository('VlanceBaseBundle:City')->findOneById($helper->getParameter('contest.city')));
        
        $form->bind($request);
        if ($form->isValid()) {
            $files = $entity->getFiles();
            foreach ($files as $idx => $file) {
                /* @var $file \Vlance\JobBundle\Entity\JobFile */
                if ($file instanceof \Vlance\JobBundle\Entity\JobFile) {
                    $file->setJob($entity);
                    $em->persist($file);
                } else {
                    $entity->getFiles()->remove($idx);
                }
            }
            
            $account->addJob($entity);
            $account->setNumJob($account->getNumJob() + 1);
            $em->persist($account);
            
            $arr = array();
            if($entity->getService()->getContestPricing() != NULL){
                foreach(json_decode($entity->getService()->getContestPricing()) as $p){
                    $arr[] = $p->price;
                }
            } else {
                $arr = array(1000000, 2000000, 4000000);
            }
            
            if((int)$number === 1){
                $entity->setBudget($arr[0]);
            } elseif((int)$number === 2){
                $entity->setBudget($arr[1]);
            } else {
                $entity->setBudget($arr[2]);
            }
            $em->persist($entity);
            
            //Add
//            if(isset($_POST['promote-contest-fb'])){
//                $entity->setFeaturedFb(true);
//            }
//            if(isset($_POST['promote-contest-hp'])){
//                
//            }
            
            $upfront_deposit_trans = new Transaction();
            $upfront_deposit_trans->setAmount($entity->getBudget());
            $upfront_deposit_trans->setSender($account);
            $upfront_deposit_trans->setJob($entity);
            $upfront_deposit_trans->setType(Transaction::TRANS_TYPE_DEPOSIT_UPFRONT);
            $upfront_deposit_trans->setStatus(Transaction::TRANS_WAITING_PAYMENT);
            $upfront_deposit_trans->setComment('Contest - '.$entity->getBudget());
            $em->persist($upfront_deposit_trans);
            
            $entity->setType(Job::TYPE_CONTEST);
            $entity->setPublish(Job::UNPUBLISH);
            $entity->setFeaturedUntil($close->setTime($now->format('H'), $now->format('i'), $now->format('s')));
            $em->persist($entity);            
            $em->flush();
                    
            /** Tracking event "Created contest" **/
            // Mixpanel
            $tracking_array = array(
                'id'                        => $entity->getId(),
                'category'                  => ($entity->getCategory())?$entity->getCategory()->getTitle():NULL,
                'budget'                    => $entity->getBudget(),
            );
            
            $this->get('vlance_base.helper')->trackAll(NULL, "Created contest", $tracking_array);
            
            // Google Adwords
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Google Code for Create Contest Conversion Page -->
                    <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 925105271;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "IPMFCM3W9GcQ9_iPuQM";
                    var google_conversion_value = '.$entity->getBudget().'.00;
                    var google_conversion_currency = "VND";
                    var google_remarketing_only = false;
                    /* ]]> */
                    </script>
                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                    </script>
                    <noscript>
                    <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$entity->getBudget().'.00&amp;currency_code=VND&amp;label=IPMFCM3W9GcQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                    </div>
                    </noscript>'
                )
            ));
            
            // Facebook
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Facebook Conversion Code for Create job -->
                    <script>(function() {
                      var _fbq = window._fbq || (window._fbq = []);
                      if (!_fbq.loaded) {
                        var fbds = document.createElement("script");
                        fbds.async = true;
                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(fbds, s);
                        _fbq.loaded = true;
                      }
                    })();
                    window._fbq = window._fbq || [];
                    window._fbq.push(["track", "6023661433485", {"value":"'.$entity->getBudget().'.00","currency":"VND"}]);
                    </script>
                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023661433485&amp;cd[value]='.$entity->getBudget().'.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                )
            ));
            /** END Tracking event "Create job" **/        
            
            $template = 'VlanceJobBundle:Email:admin_create_job.html.twig';
            $context = array(
                'job' => $entity,
            );
            $from = $this->container->getParameter('from_email');
            $to = $this->container->getParameter('to_email');
            
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);

            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(JobEvent::JOB_CREATE_COMPLETED, new JobEvent($entity));
            
            $this->get('session')->set('createjob', 'new');
            return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $entity->getHash())));
        }
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'acc'    => $account
        );
    }
    
    /**
     * Update 20160526
     * Displays a form to create a new Job Contest entity.
     *
     * @Route("/dang-cuoc-thi", name="job_contest_new")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/contest:new.html.php",engine="php")
     */
    public function newContestAction($entity = null) {
        // Not permission
//        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
//            return $this->redirect($this->generateUrl('fos_user_security_login'));
//        }
      
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        //Không vào được trang đăng cuộc thi nếu không phải KH
//        if($account->getType() != Account::TYPE_CLIENT){
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.edit.notpermission', array(), 'vlance'));
//            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
//        }
        
        // If current user is banned
        if(is_object($account)){
            if ($account->isBanned()) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
                return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
            }
            
            $helper_acc = $this->get('vlance_account.helper');
            if(!is_null($account->getEmail())){
                if($helper_acc->emailNotFacebook($account->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $account->getId())));
                }
            }
        }
        
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobcontesttype', $entity);
        
        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }
    
    /**
     * Tăng thêm 20 bài dự thi cho cuộc thi 
     *
     * @Route("/contest/extend/pay-credit/{hash}", name="job_contest_extend_pay_credit")
     * @Method("GET")
     */
    public function payCreditExtendContest(Request $request, $hash)
    {
	// Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
	$em = $this->getDoctrine()->getManager();
	/* @var $acc Account */
	$acc = $this->get('security.context')->getToken()->getUser();
	/* @var $job Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
        if (!$job) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
	
	// Nếu tài khoản không đủ credit 
	if($acc->getCredit()->getBalance() == 0 ){
	    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Tài khoản không đủ credit. Hãy nạp thêm.', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_contest_view',array('hash' => $hash)));
	}
	
	// Lưu lịch sử giao dịch
	/* @var $cet CreditExpenseTransaction */
	$cet = new CreditExpenseTransaction();
	$cet->setCreditAccount($acc->getCredit());
	$cet->setType(CreditExpenseTransaction::TYPE_EXPENSE);
	$cet->setService(CreditExpenseTransaction::SERVICE_BUY_DISPLAY_BID);
	$cet->setPreBalance($acc->getCredit()->getBalance());
	$cet->setAmount(1);
	$cet->setPostBalance($acc->getCredit()->getBalance()-1);
	$cet->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
	$cet->setDetail('{"job_id":'.$job->getId().'}');
	$em->persist($cet);
	    
	// Trừ credit
	$acc->getCredit()->setBalance($acc->getCredit()->getBalance()-1);
	$em->persist($acc);
	
	//Set publish cho những bài trong tình trạng unpublish
	//lấy max_bid - số publish = số được mở thêm
	$publish_bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job->getId(), 'publish' => Bid::PUBLISH));
	$set_publish_num = $job->getMaxBid()+ Job::EXTEND_CONTEST - count($publish_bid);
	/* @var $get_unpublish_bid Bid*/
	//lấy số unpublish limit(0, số được mở thêm) set publish
	$get_unpublish_bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(
		array('job'=> $job->getId(), 'publish'=> Bid::UNPUBLISH),
		array('createdAt'=> 'asc'));
	if(count($get_unpublish_bid) <= $set_publish_num){
	    $unpublish_approve = count($get_unpublish_bid);
	}else{
	    $unpublish_approve = $set_publish_num;
	}
	for($i=0; $i<$unpublish_approve; $i++){
	    $get_unpublish_bid[$i]->setPublish(Bid::PUBLISH);
	    $em->persist($get_unpublish_bid[$i]);
	}
	
	// tăng thêm 20 bài dự thi
	$job->setMaxBid($job->getMaxBid()+ Job::EXTEND_CONTEST);
	$em->persist($job);
	
	$em->flush();
	
	// redirect về cuộc thi
	$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Tăng số lượng bài dự thi thành công.', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_contest_view',array('hash' => $hash)));
    }
    
    /**
     * Tăng thêm 2 lượt đăng bài cho freelancer khi trả 1 credit
     *
     * @Route("/contest/pay-credit/{hash}", name="job_contest_pay_credit")
     * @Method("GET")
     */
    public function payCreditApplyContest(Request $request, $hash)
    {
	// Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
	
	$em = $this->getDoctrine()->getManager();
	/* @var $acc Account */
	$acc = $this->get('security.context')->getToken()->getUser();
	/* @var $job Job */
	$job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
	if (!$job) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
	
	//Nếu tài khoản không đủ credit 
	if($acc->getCredit()->getBalance() == 0 ){
	    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Tài khoản không đủ credit. Hãy nạp thêm.', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_contest_view',array('hash' => $hash)));
	}
	// Lưu lịch sử giao dịch
	/* @var $cet CreditExpenseTransaction */
	$cet = new CreditExpenseTransaction();
	$cet->setCreditAccount($acc->getCredit());
	$cet->setType(CreditExpenseTransaction::TYPE_EXPENSE);
	$cet->setService(CreditExpenseTransaction::SERVICE_BUY_APPLY_BID);
	$cet->setPreBalance($acc->getCredit()->getBalance());
	$cet->setAmount(1);
	$cet->setPostBalance($acc->getCredit()->getBalance()-1); // -amount
	$cet->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
	$cet->setDetail('{"job_id":'.$job->getId().'}');
	$em->persist($cet);
	
	$acc->getCredit()->setBalance($acc->getCredit()->getBalance()-1);
	$acc->setExtraBid($acc->getExtraBid()+2); // tăng thêm 2 lượt gửi bài
	$em->persist($acc);
	
	$em->flush();
	$this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Thanh toán thành công. Bạn có thể gửi thêm bài dự thi.', array(), 'vlance'));
	return $this->redirect($this->generateUrl('job_contest_view',array('hash' => $hash)));
    }
    
    /**
     * Update 20160526
     * Finds and displays a Job Contest entity.
     *
     * @Route("/cuoc-thi/{hash}", name="job_contest_view")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/contest:view.html.php",engine="php")
     */
    public function showContestJobAction(Request $request, $hash, $bid_form = null)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Check is job exists
        /* @var $entity Job */
        $entity = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        /* @var $current_user Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        if(is_object($current_user)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($current_user->getEmail())){
                if($helper->emailNotFacebook($current_user->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $current_user->getId())));
                }
            }
        }
        
        // Redirect to correct view of job
        switch($entity->getType()){
            case Job::TYPE_BID:
                return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $entity->getHash()), true), 301);
            case Job::TYPE_ONSITE:
                return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $entity->getHash()), true), 301);
        }
        
        // Check if user's profile are missing some basic info
        if(is_object($current_user)){
            if(!$current_user->getCategory() || !$current_user->getCity() || !$current_user->getType()){
                return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->generateUrl('job_show_freelance_job', array('hash' => $hash)))), true));
            }
        }
        
        if($entity->getValid() === Job::VALID_CONTEST_NO){
            if(is_object($current_user)){
                if($entity->getAccount() !== $current_user){
                    if(HasPermission::hasAdminPermission($current_user) == FALSE){
                        return $this->redirect($this->generateUrl("404_page"), 302);
                    }
                }
            } else {
                return $this->redirect($this->generateUrl("404_page"), 302);
            }
        } 
        
        // Check if current user didn't make any bid to the job
        $bids = $entity->getBids();
        $is_bidded = false;
        $bid_current = array();
        
        if(is_object($current_user)){
            foreach ($bids as $b){
                if($b->getAccount()->getId() == $current_user->getId()){
                    $is_bidded = true;
                    break; // quit the for loop
                }
            }
            $bid_current = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $entity->getId(), 'account' => $current_user->getId(), 'isDeclined' => false, 'publish' => Bid::PUBLISH));
        }
      
        // Build absolute URL.
        // If in dev environnement, return production domain as default.
        $request = $this->getRequest();
        $abs_url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();
        if(in_array($this->container->getParameter('kernel.environment'), array('dev'))){
            $abs_url = "http://www.vlance.vn";
        }
        
        // Build time remain
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($entity->getCloseAt(), $now);
        
        /** Step **/
        $step = 1;
        if($remain == false){
            if($entity->getStatus() === Job::JOB_OPEN){$step = 2;}
        }
        if($entity->getStatus() === Job::JOB_CONTEST_QUALIFIED){
            $step = 3;
            //Khi thời gian 
            if($now->diff($entity->getQualifiedAt())->format('%a') >= 4){$step = 4;}
        }
        if($entity->getStatus() === Job::JOB_FINISHED){$step = 5;}
        /** END **/
        
        // Build meta description
        $meta_description = 'Ngân sách dự kiến: '.  JobCalculator::vlance_number_format($entity->getBudget()).' VNĐ. '.$entity->getDescription();
        
        // Transaction
        $tran = false;
        $trans = $entity->getTransactions();
        foreach ($trans as $t){
            if($t->getStatus() === Transaction::TRANS_TERMINATED){
                $tran = true;
            }
        }
        
        $view_array = array(
            'abs_url'           => $abs_url,
            'entity'            => $entity,
            'remain'            => $remain,
            'tran'              => $tran,
            'meta_title'        => ucfirst(JobCalculator::replacement($entity->getTitle()).' | vLance.vn'),
            'meta_description'  => str_replace('>','*',addslashes(JobCalculator::cut(JobCalculator::replacement(strip_tags($meta_description)),150))),
            'bid_form'          => $bid_form,
            'bid_current'       => $bid_current,
            'level'             => $this->get('vlance_payment.helper')->getLevelPayingContactClient($entity),
            'step'              => $step,
        );
        
        // Set current job, so the job prgress bar will know this job Id.
        $this->get('session')->set('current_job', '');
        $job_id = $entity->getId();
        if($job_id){
            $this->get('session')->set('current_job', $job_id);
        }
        
        // Tracking with mixpanel
        if($is_bidded){
            $this->get('vlance_base.helper')->trackMixpanel(NULL, "Show feature bid block", array(
                'category'  => $entity->getCategory()->getTitle(),
                'bidded'    => $is_bidded ? 'YES' : 'NO'
            ));
        }
        
        // Use client view for jow owner and admin
        if($entity->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE) {
            $content = $this->renderView('VlanceJobBundle:Job/contest:view_as_client.html.php', $view_array);
            return new Response($content);
        }
        
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED') && $current_user->isBanned()) {
            $view_array['isBanned'] = true;
        }

        // Use freelancer view for freelancer
        return $view_array;
    }
    
        /**
     * Displays a form to edit an existing Contest entity.
     *
     * @Route("/cuoc-thi/{id}/edit", name="contest_edit")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/contest:edit.html.php",engine="php")
     */
    public function editContestAction($id) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        /*@var $entity Job*/
        $entity = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $job_files = $entity->getFiles();

        /**
         * Entity must exist
         */
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /**
         * Current user must be owner to show edit form
         */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE){
            if (!$entity->isOwner($current_user) || $entity->getWorker()) {
                return $this->redirect($this->generateUrl("500_page"), 302);
            }
        }
        
        $editForm = $this->createForm('vlance_jobbundle_jobcontesttype', $entity);
        $package = $entity->getBudget();
        
        /** Tracking event "Edit job" **/
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Edit contest");
            
        return array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'job_files' => $job_files,
            'acc'       => $current_user,
            'cpackage'  => $package,
        );
    }
    
    /**
     * Edits an existing Job entity.
     *
     * @Route("/cuoc-thi/{id}/update", name="contest_edit_submit")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job/contest:edit.html.php", engine="php")
     */
    public function updateContestAction(Request $request, $id) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $entity Job */
        $entity = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        /**
         * Entity must exist
         */
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /**
         * Current user must be owner to update entity info
         */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE && !$entity->isOwner($current_user)) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // This is used to check which field gets update on save
        $before_update = array(
            'title'         => $entity->getTitle(),
            'description'   => $entity->getDescription(),
            'service'       => $entity->getService(),
            'category'      => $entity->getCategory()->getTitle(),
            'budget'        => $entity->getBudget(),
            'city'          => $entity->getCity()->getName(),
            'close_at'      => $entity->getCloseAt(),
        );
        
        $editForm = $this->createForm('vlance_jobbundle_jobcontesttype', $entity);
        
        /**
         * Must catch exception when submit data from request to handle some error
         * In this case, when client try to submit empty file input
         */
        try {
            $editForm->submit($request);
        } catch (\Symfony\Component\Debug\Exception\ContextErrorException $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError('controller.job.create.unknowerror'));
        } catch (\ErrorException $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError("controller.job.create.unknowerror"));
        } catch (\Exception $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError("controller.job.create.unknowerror"));
        }
        if ($editForm->isValid()) {
            foreach ($entity->getFiles() as $idx => $file) {
                // New file added
                if ($file instanceof JobFile && $em->getUnitOfWork()->getEntityState($file) === UnitOfWork::STATE_NEW) {
                    $file->setJob($entity);
                    $em->persist($file);
                } else {
                    $entity->getFiles()->remove($idx);
                }
            }
            
            $em->persist($entity);
            $em->flush();
            
            /** Tracking event "Updated job" **/
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Updated contest", array(
                'title'         => $entity->getTitle()==$before_update['title']?NULL:$entity->getTitle(),
                'description'   => $entity->getDescription()==$before_update['description']?NULL:$entity->getDescription(),
                'service'       => $entity->getService() ? ($entity->getService()->getTitle()==$before_update['service'] ? NULL : $entity->getService()->getTitle()) : NULL,
                'category'      => $entity->getCategory()->getTitle()==$before_update['category']?NULL:$entity->getCategory()->getTitle(),
                'budget'        => $entity->getBudget()==$before_update['budget']?NULL:$entity->getBudget(),
                'city'          => $entity->getCity()->getName()==$before_update['city']?NULL:$entity->getCity()->getName(),
                'close_at'      => $entity->getCloseAt()==$before_update['close_at']?NULL:$entity->getCloseAt(),
            ));
            
            return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $entity->getHash())));
        }

        return array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'acc'       => $current_user
        );
    }
    
    /**
     * Displays a form to create a new Job entity.
     *
     * @Route("/workdii/new-service", name="job_new_service")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:new_service_job.html.php", engine="php")
     */
    public function newServiceAction($entity = null) {
        /** BETA VERSION, ACCESSIBLE BY VLANCE ADMIN ONLY **/
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE) {
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }
        /** BETA VERSION, ACCESSIBLE BY VLANCE ADMIN ONLY **/
        

        // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        // If current user is banned
        if ($account->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobtype', $entity);
        return array(
            'entity'=> $entity,
            'form'  => $form->createView(),
            'acc'   => $account
        );
    }
    
    /**
     * Display all job the fond
     *
     * @Route("/j/search/{filters}", name="job_search", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:formSearch.html.php", engine="php")
     */
//    public function searchAction(Request $request, $filters = '') {
//        $params = $this->get('vlance_base.url')->pasteParameter($filters);
//        $find = $request->get('search');
//        $em = $this->getDoctrine()->getManager();
//        $pager = null;
//        try {
//            $pager = $em->getRepository('VlanceJobBundle:Job')->searchJobs($find, $params);
//        }
//        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
//            
//        }
//        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
//            
//        }
//        catch (\Exception $e) {
//            return $this->redirect($this->generateUrl('vlance_homepage'));
//        }
//        
//        $pagerView = new DefaultView(new PagerTemplate($params, 'job_search'));
//        $options = array('proximity' => 3);
//        
//        return array(
//            'pager' => $pager,  
//            'pagerView' => $pagerView,
//            'entities' => $pager->getCurrentPageResults(),
//        );
//    }
    
    /**
     * Finds and displays a Job entity.
     *
     * @Route("/viec-freelance/{hash}", name="job_show_freelance_job")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:show.html.php",engine="php")
     */
    public function showFreelanceJobAction(Request $request, $hash, $bid_form = null)
    {
        $em = $this->getDoctrine()->getManager();
        $modulo = -1;
        
        // Check is job exists
        /* @var $entity Job */
        $entity = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        /* @var $current_user Account */
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        if(is_object($current_user)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($current_user->getEmail())){
                if($helper->emailNotFacebook($current_user->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $current_user->getId())));
                }
            }
        }
        
        // Redirect to correct view of job
        switch($entity->getType()){
            case Job::TYPE_ONSITE:
                return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $entity->getHash()), true), 301);
            case Job::TYPE_CONTEST:
                return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $entity->getHash()), true), 301);
        }
        
        // Check if job is published.
        if($entity->getPublish() == Job::UNPUBLISH && HasPermission::hasAdminPermission($current_user) == FALSE) {
            return new Response($this->renderView('VlanceJobBundle:Job/show:job_is_unpublished.html.php'));
        }
        
        if(is_null($request->cookies->get('vluid')) && !is_object($current_user)){
            $is_anonym = true;
            if(isset($request->query)){
                $query = $request->query;
                if(!is_null($query->get('ref'))){
                    if(in_array($query->get('ref'), array('newjob', 'newjobwk'))){
                        $is_anonym = false;
                    }
                }
            }
            if($is_anonym){
                if(isset($_COOKIE["vlanon_id"])) {
                    $modulo = abs($_COOKIE["vlanon_id"] % 12); 
                }
                //$case = $modulo < 9 ? "Test1-NEW" : "Test1-OLD";
                $this->get('vlance_base.helper')->trackAll(NULL, "View Job as Anonymous", array(
                    'category'  => $entity->getCategory()->getTitle(),
                    'city'      => $entity->getCity()->getName(),
                    'budget'    => $entity->getBudget(),
                    'bids'      => $entity->getNumBids(),
                    'title'     => $entity->getTitle(),
                    //'modulo'    => $modulo,
                    //'divide'    => 12
                    //'case'      => $case
                ));
            }
        }
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "job_show_freelance_job",
                    'authenticated' => is_object($current_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        // Check if user's profile are missing some basic info
        if(is_object($current_user)){
            if(!$current_user->getCategory() || !$current_user->getCity() || !$current_user->getType()){
                return $this->redirect($this->generateUrl('account_login_after', array('referer' => urlencode($this->generateUrl('job_show_freelance_job', array('hash' => $hash)))), true));
            }
        }
        
        // Check if current user didn't make any bid to the job
        $bids = $entity->getBids();
        $is_bidded = false;
        $bid_current = array();
        
        
        //Thống kê
        //statistic
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        if(is_object($current_user)){
            foreach ($bids as $b){
                if($b->getAccount()->getId() == $current_user->getId()){
                    $is_bidded = true;
                    break; // quit the for loop
                }
            }
            $bid_current = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $entity->getId(), 'account' => $current_user->getId(), 'isDeclined' => false, 'publish' => Bid::PUBLISH));
        }
        
        // Check if job is private
        if($entity->getIsPrivate()) {
            // Check if user is anonymous
            if(!is_object($current_user)){
                return new Response($this->renderView('VlanceJobBundle:Job/show:job_is_private.html.php'));
            }            
            
            // Check if current user is not Admin neither Job owner
            if(HasPermission::hasAdminPermission($current_user) == FALSE && $current_user->getId() != $entity->getAccount()->getId()){
                // Check if current user doesn't get any invite to the job
                $invitations = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('job' => $entity));
                $is_invited = false;
                foreach ($invitations as $i){
                    /* @var $i JobInvite */
                    if($i->getAccount()->getId() == $current_user->getId()){
                        $is_invited = true;
                        break; // quit the for loop
                    }
                }
                
                if($is_invited == false && $is_bidded == false){
                    return new Response($this->renderView('VlanceJobBundle:Job/show:job_is_private.html.php'));
                }
            }
        }
      
        // Build absolute URL.
        // If in dev environnement, return production domain as default.
        $request = $this->getRequest();
        $abs_url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();
        if(in_array($this->container->getParameter('kernel.environment'), array('dev'))){
            $abs_url = "http://www.vlance.vn";
        }
        
        // Build time remain
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($entity->getCloseAt(), $now);
        
        // Build meta title
        $city = $entity->getCity() ? $entity->getCity()->getName() : 'Toàn Quốc';
        if($city == 'Toàn Quốc' || $city == '') {
            $tp_title = '';
        } else {
            $tp_title = ' - '.$city;
        }
        
        // Build meta description
        $meta_description = 'Việc freelance tại '.$city.'. Ngân sách dự kiến: '.  JobCalculator::vlance_number_format($entity->getBudget()).' VNĐ. '.$entity->getDescription();
        
        $view_array = array(
            'abs_url'           => $abs_url,
            'entity'            => $entity, 
            'remain'            => $remain,
            'meta_title'        => ucfirst(JobCalculator::replacement($entity->getTitle()).$tp_title.' | vLance.vn'),
            'meta_description'  => str_replace('>','*',addslashes(JobCalculator::cut(JobCalculator::replacement(strip_tags($meta_description)),150))),
            'bid_form'          => $bid_form,
            'bid_current'       => $bid_current,
            'jobStatistic'      => $jobStatistic,
            'accountStatistic'  => $accountStatistic,
            'level'             => $this->get('vlance_payment.helper')->getLevelPayingContactClient($entity),
            'modulo'            => $modulo
        );
        
        // Set current job, so the job prgress bar will know this job Id.
        $this->get('session')->set('current_job', '');
        $job_id = $entity->getId();
        if($job_id){
            $this->get('session')->set('current_job', $job_id);
        }
        
        // Tracking with mixpanel
        if($is_bidded){
            $this->get('vlance_base.helper')->trackMixpanel(NULL, "Show feature bid block", array(
                'category'  => $entity->getCategory()->getTitle(),
                'city'      => $entity->getCity()->getName(),
                'bidded'    => $is_bidded ? 'YES' : 'NO'
            ));
        }

        // Use client view for jow owner and admin
        if($entity->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE) {
            $view_array['is_test_deposit_client'] = ($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW) ? true : false;
            $content = $this->renderView('VlanceJobBundle:Job:show_as_client.html.php', $view_array);
            return new Response($content);
        }
        
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED') && $current_user->isBanned()) {
            $view_array['isBanned'] = true;
        }
        
        /*if(!is_object($current_user)){
            //Bid thoa man dkien
            $bids_enabled = $em->getRepository('VlanceJobBundle:Bid')->getBestBid($entity, 4);
            $view_array['bids'] = $bids_enabled;
            return new Response($this->renderView('VlanceJobBundle:Job:show_not_login.html.php', $view_array));
        }*/

        // Use freelancer view for freelancer
        return $view_array;
    }
    
    /**
     * Finds and displays a Job entity.
     *
     * @Route("/j/cong-viec/{hash}", name="job_show_alias")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAliasAction($hash, $bid_form = null)
    {
        return $this->redirect($this->generateUrl('job_show_freelance_job',array('hash' => $hash, 'bid_form' => $bid_form)),301);
    }
    
    /**
     * Finds and displays a Job entity.
     *
     * @Route("/j/s/{id}", name="job_show")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceJobBundle:Job')->find($id);
        //Khong ton tai job
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        return $this->redirect($this->generateUrl('job_show_freelance_job',array('hash' => $entity->getHash())),301);
    }
    
    /**
     * Displays a form to edit an existing Job entity.
     *
     * @Route("/j/{id}/edit", name="job_edit")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function editAction($id) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        /*@var $entity Job*/
        $entity = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $job_files = $entity->getFiles();

        /**
         * Entity must exist
         */
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /**
         * Current user must be owner to show edit form
         */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE){
            if (!$entity->isOwner($current_user) || $entity->getWorker()) {
                return $this->redirect($this->generateUrl("500_page"), 302);
            }
        }
        
        $editForm = $this->createForm('vlance_jobbundle_jobtype', $entity);
        
        /** Tracking event "Edit job" **/
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Edit job");
            
        return array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'job_files' => $job_files,
            'acc'       => $current_user
        );
    }

    /**
     * Edits an existing Job entity.
     *
     * @Route("/j/{id}/update", name="job_edit_submit")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job:edit.html.php", engine="php")
     */
    public function updateAction(Request $request, $id) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $entity Job */
        $entity = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        /**
         * Entity must exist
         */
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /**
         * Current user must be owner to update entity info
         */
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE && !$entity->isOwner($current_user)) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        // This is used to check which field gets update on save
        $before_update = array(
            'title'         => $entity->getTitle(),
            'description'   => $entity->getDescription(),
            'service'       => $entity->getService(),
            'category'      => $entity->getCategory()->getTitle(),
            'budget'        => $entity->getBudget(),
            'city'          => $entity->getCity()->getName(),
            'close_at'      => $entity->getCloseAt(),
            'is_private'    => $entity->getIsPrivate(),
            'skills_count'  => count($entity->getSkills())
        );
        
        $files_old = array();
        foreach ($entity->getFiles() as $file) {
            $files_old[] = $file->getId();
        }
        
        $editForm = $this->createForm('vlance_jobbundle_jobtype', $entity);
        
        /**
         * Must catch exception when submit data from request to handle some error
         * In this case, when client try to submit empty file input
         */
        try {
            $editForm->submit($request);
        } catch (\Symfony\Component\Debug\Exception\ContextErrorException $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError('controller.job.create.unknowerror'));
        } catch (\ErrorException $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError("controller.job.create.unknowerror"));
        } catch (\Exception $e) {
            $editForm->addError(new \Symfony\Component\Form\FormError("controller.job.create.unknowerror"));
        }
        if ($editForm->isValid()) {
//            foreach ($entity->getFiles() as $idx => $file) {
//                // New file added
//                if ($file instanceof JobFile && $em->getUnitOfWork()->getEntityState($file) === UnitOfWork::STATE_NEW) {
//                    $file->setJob($entity);
//                    $em->persist($file);
//                } else {
//                    $entity->getFiles()->remove($idx);
//                }
//            }
            //File
            $new_arr = array();
            foreach ($entity->getFiles() as $file) {
                $file->setJob($entity);
                $em->persist($file);
                $new_arr[] = $file->getId();
            }
            
            foreach($files_old as $fo){
                if(!in_array($fo, $new_arr)){
                    $file = $em->getRepository('VlanceJobBundle:JobFile')->findOneById($fo);
                    $em->remove($file);
                }
            }
            $em->persist($entity);
            
            $created = $entity->getCreatedAt();
            $close = $entity->getCloseAt();
            $entity->setCloseAt($close->setTime($created->format('H'), $created->format('i'), $created->format('s')));
            
            /*if ($request->get('is_private', false)) {
                $entity->setIsPrivate(true);
            } else{
                $entity->setIsPrivate(false);
            }*/
            
            $em->persist($entity);
            
            //Update skill 
            $skilled = $entity->getSkills();
            $skill_old = array();
            $skills = explode(',', $request->get('hiddenTagListA', false));
            //Remove skill
            foreach($skilled as $skill) {
                if(!in_array($skill->getTitle(), $skills)){
                    $entity->removeSkill($skill);
                }
                $skill_old[] = $skill->getTitle();
            }
            // Insert skill
            foreach($skills as $skill_new) {
                if(empty($skill_new)) {
                    break;
                }
                elseif(!in_array($skill_new, $skill_old)) {
                    $acc_skill = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('title' => $skill_new));
                    $entity->addSkill($acc_skill);
                }
            }
            
            $em->flush();
            
            /** Tracking event "Updated job" **/
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Updated job", array(
                'title'         => $entity->getTitle()==$before_update['title']?NULL:$entity->getTitle(),
                'description'   => $entity->getDescription()==$before_update['description']?NULL:$entity->getDescription(),
                'service'       => $entity->getService() ? ($entity->getService()->getTitle()==$before_update['service'] ? NULL : $entity->getService()->getTitle()) : NULL,
                'category'      => $entity->getCategory()->getTitle()==$before_update['category']?NULL:$entity->getCategory()->getTitle(),
                'budget'        => $entity->getBudget()==$before_update['budget']?NULL:$entity->getBudget(),
                'city'          => $entity->getCity()->getName()==$before_update['city']?NULL:$entity->getCity()->getName(),
                'close_at'      => $entity->getCloseAt()==$before_update['close_at']?NULL:$entity->getCloseAt(),
                'is_private'    => $entity->getIsPrivate()==$before_update['is_private'] ? NULL:$entity->getIsPrivate(),
                'skills_count'  => count($entity->getSkills())==$before_update['skills_count']?NULL:count($entity->getSkills())
            ));
            
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $entity->getHash())));
        }

        return array(
            'entity'    => $entity,
            'edit_form' => $editForm->createView(),
            'acc'       => $current_user
        );
    }

    /**
     * Update status job when freelancer or client request
     * @Route("/j/update/{bid}/{status}", name="job_update_status")
     * @Method({"GET"})
     * @Template(engine="php")
     */
    public function updateStatusAction($bid, $status) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        /* @var $entity_bid \Vlance\JobBundle\Entity\Bid */
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->find($bid);
        
        /**
         * If wrong bid id so can not get bid entity
         */
        if (!$entity_bid) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        /**
         * Verify status to set is allowed or not
         */
        $jobStatus = Job::statusAllowed();
        if (!isset($jobStatus[$status])) {
            return $this->redirect($this->generateUrl("404_page"), 302);
        }
        
        $workshop = $entity_bid->getWorkshop();

        if (!$workshop) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $entity_bid->getJob();
        if ($status == Job::JOB_FINISH_REQUEST && $acc_login != $job->getWorker()) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        // Only client and Admin can click to finish job
        if ($status == Job::JOB_FINISHED && $acc_login != $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        $job->setStatus($status);
        $em->persist($job);
        
        /**
         * @todo write content for system message
         */
        $message = new Message();
        $message->setBid($entity_bid);
        $message->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
        $message->setStatus(Message::NEW_MESSAGE);
        $message->setIsNotified(true);
        
        //khach hang
        $client = $job->getAccount();
        // freelancer
        $freelancer = $entity_bid->getAccount();
        // Gui thong bao bang message he thong va mail cho client va freelancer.
        $email = array();
        /**
         * Check if job update to one of these status: JOB_FINISH_REQUEST, JOB_FINISHED, JOB_WORKING
         */
        if (!in_array($status, array(Job::JOB_WORKING, Job::JOB_FINISH_REQUEST, Job::JOB_FINISHED))) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.job.updatestatus.wrong', array(), 'vlance'));
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        // Client don't accept finish job
        if ($status == Job::JOB_WORKING) {
            // message system
            $message->setReceiver($freelancer);
            $message->setContent(''.$client->getFullName().' không chấp nhận kết thúc dự án.');
            
            // Send mail to freelancer
            $to = $freelancer->getEmail();
            $context = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            $template = 'VlanceJobBundle:Email:redo_job.html.twig';
            
            // send email to admin.
            $template_admin = 'VlanceJobBundle:Email:admin_redo_job.html.twig';
            $context_admin = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            
        }
        
        if ($status == Job::JOB_FINISH_REQUEST) {
            // message system
            $message->setReceiver($client);
            $message->setContent(''.$freelancer->getFullName().' yêu cầu kết thúc công việc.');
            
            // send email to client
            $to = $client->getEmail();
            $context = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            $template = 'VlanceJobBundle:Email:job_required_finish.html.twig';
            
            // send email to admin.
            $template_admin = 'VlanceJobBundle:Email:admin_finish_request.html.twig';
            $context_admin = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            
            
            
        }
        if ($status == Job::JOB_FINISHED) {
            /* update status workshop when job finish */
            $workshop->setStatus(\Vlance\WorkshopBundle\Entity\Workshop::WORKSHOP_CLOSE);
            $em->persist($workshop);
            
            /**
             * Payment part
             * - Update job cash
             * - Update job payment status -> paied
             * - Create paied transaction
             * - Update freelancer cash
             * - Create vlance fee transaction
             * - Update vlance cash
             */
            $vlance_parameter = $this->container->getParameter('vlance_system');
            $commission = $vlance_parameter['commission'];
            $jobCash = $job->getCash();
            $jobAmmount = $jobCash->getBalance();
            $jobCash->setBalance(0);
            $em->persist($jobCash);
            
            $job->setPaymentStatus(Job::PAIED);
            $em->persist($job);
            
            $paied_trans = new Transaction();
            $paied_trans->setAmount($jobAmmount * (100 - $commission) / 100);
            $paied_trans->setSender($client);
            $paied_trans->setReceiver($freelancer);
            $paied_trans->setJob($job);
            $paied_trans->setType(Transaction::TRANS_TYPE_PAYMENT);
            $paied_trans->setStatus(Transaction::TRANS_TERMINATED);
            $paied_trans->setComment($this->get('translator')->trans('transaction.pay.default_comment',array(),'vlance'));
            $em->persist($paied_trans);
            
            $freelancerCash = $freelancer->getCash();
            $freelancerCash->setBalance($freelancerCash->getBalance() + $paied_trans->getAmount());
            $em->persist($freelancerCash);
            
            $fee_trans = new Transaction();
            $fee_trans->setAmount($jobAmmount * $commission / 100);
            $fee_trans->setSender($client);
            $fee_trans->setReceiver($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $fee_trans->setJob($job);
            $fee_trans->setType(Transaction::TRANS_TYPE_FEE);
            $fee_trans->setStatus(Transaction::TRANS_TERMINATED);
            $fee_trans->setComment($this->get('translator')->trans('transaction.fee.default_comment',array(),'vlance'));
            $em->persist($fee_trans);
            
            $vlanceCash = new \Vlance\PaymentBundle\Entity\VlanceCash();
            $vlanceCash->setBalance($fee_trans->getAmount());
            $vlanceCash->setTransaction($fee_trans);
            $em->persist($vlanceCash);
            
            // Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Job done");
            // Tracking Revenue
            $this->get('vlance_base.helper')->mixpanelTrackRevenue($client, $fee_trans->getAmount(), "Job done");
            // End Mixpanel
            // Google Adwords
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Google Code for Job paid Conversion Page -->
                    <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 925105271;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "eJJVCK3Kx1oQ9_iPuQM";
                    var google_conversion_value = '.$fee_trans->getAmount().';
                    var google_conversion_currency = "VND";
                    var google_remarketing_only = false;
                    /* ]]> */
                    </script>
                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                    </script>
                    <noscript>
                    <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$fee_trans->getAmount().'&amp;currency_code=VND&amp;label=eJJVCK3Kx1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                    </div>
                    </noscript>'
                )
            ));
            // Facebook
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Facebook Conversion Code for Job paid -->
                    <script>(function() {
                      var _fbq = window._fbq || (window._fbq = []);
                      if (!_fbq.loaded) {
                        var fbds = document.createElement("script");
                        fbds.async = true;
                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(fbds, s);
                        _fbq.loaded = true;
                      }
                    })();
                    window._fbq = window._fbq || [];
                    window._fbq.push(["track", "6024219005085", {"value":"'.$fee_trans->getAmount().'","currency":"VND"}]);
                    </script>
                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6024219005085&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                )
            ));
            
            /**
             * Statistic part
             */
            /* update outcome of client  */
            $client->setOutCome($client->getOutCome() + $entity_bid->getAmount());
            $em->persist($client);
            
            /* update income, numjobfinsh of freelancer */
            $freelancer->setInCome($freelancer->getInCome() + $paied_trans->getAmount());
            $freelancer->setNumJobFinish($freelancer->getNumJobFinish() + 1);
            $em->persist($freelancer);
            
            //Send message (from system) to freelancer
            $message->setReceiver($freelancer);
            $message->setContent(''.$client->getFullName().' đã xác nhận kết thúc công việc. Bạn hãy vào trang <strong>Quản lý</strong> để kiểm tra tài khoản và thực hiện lệnh rút tiền.');
            
            //Send message (from system) to client
            $message_to_client = new Message();
            $message_to_client->setBid($entity_bid);
            $message_to_client->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
            $message_to_client->setStatus(Message::NEW_MESSAGE);
            $message_to_client->setIsNotified(true);
            $message_to_client->setReceiver($client);
            $message_to_client->setContent('Bạn đã xác nhận kết thúc công việc.');
            $em->persist($message_to_client);
            
            // Send email freelancer
            $to = $freelancer->getEmail();
            $template = 'VlanceJobBundle:Email:freelancer_job_finished.html.twig';
            $context = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            //Send Email to admin
            $template_admin = 'VlanceJobBundle:Email:admin_job_finished.html.twig';
            $context_admin = array(
                'job' => $job,
                'bid' => $entity_bid,
                'freelancer' => $freelancer,
            );
            //Guide client feedback for freelancer
            $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            $context_client = array(
                'job' => $job,
                'freelancer' => $freelancer,
            );
            $template_client = 'VlanceJobBundle:Email:client_job_finished.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $email_hotro, $client->getEmail());
        }
        
        $em->persist($message);
        $em->flush();
        
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Payment paid");
        
        $from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
        $to_admin = $this->container->getParameter('to_email');
        
        /* send email to client/freelancer by sendTWIGMessageBySES  */
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
        
        /* send email to admin by sendTWIGMessageBySES  */
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from, $to_admin);
        
        
        /**
         * Offer Credit for invitor when referring vLance to new people
         * - Case 1: Invitor & Invitee both get 5 Credit if Invitee get first job done (budget >= 5.000.000)
         * - Case 2: Invitor & Invitee both get 30 Credit if Invitee hire successfully first job done (budget >= 5.000.000)
        **/
        $helper = $this->get('vlance_payment.helper');
        $from_vlance = $this->container->getParameter('from_email');
        
        /* Offer Credit - Case 1 - Get 5 Credit  */
        // Invitee is freelancer
        $acc_fl = $em->getRepository('VlanceAccountBundle:EmailInvite')->findOneByEmail($freelancer->getEmail());
        if(is_object($acc_fl)){
            //get job worked
            $job_worked_array = $em->getRepository('VlanceJobBundle:Job')->findBy(array('worker' => $freelancer, 'status' => Job::JOB_FINISHED, 'publish' => Job::PUBLISH));
            $job_worked = array();
            $count = 0;
            foreach($job_worked_array as $jw){
                if($jw->getBudgetBided() >= 5000000){
                    $count++;
                }
                $job_worked = $jw;
            }
            
            //check first job budget >= 5000000 post
            if($count == 1){
                /* @var $job_worked \Vlance\JobBundle\Entity\Job */
                if($job_worked->getId() == $job->getId()){
                    /* Add Credit for Invitor */
                    if(!($helper->addPromotionCredit($acc_fl->getAccount(), 0, 5, array(
                        'role'          => 'invitor',
                        'invitor_id'    => $acc_fl->getAccount()->getId(),
                        'case'          => 'Freelancer get 1st job paid',
                        'job_id'        => $job->getId(),
                        'qty'           => '5',
                    )))){
                        return new JsonResponse(array('error' => 302));
                    }
                    
                    // Send Email
                    $context_invitor = array(
                        'invitor'   => $acc_fl->getAccount(),
                        'invitee'   => $freelancer,
                        'job'       => $job,
                    );
                    $tpl_invitor = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitor_first_job_freelancer.html.twig';
                    try{
                        $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitor, $context_invitor, $from_vlance, $acc_fl->getAccount()->getEmail());
                    } catch (Exception $ex) {
                        return new JsonResponse(array('error' => 303));
                    }
                    
                    
                    /* Add Credit for Invitee */
                    if(!($helper->addPromotionCredit($freelancer, 0, 5, array(
                        'role'          => 'invitee',
                        'invitee_id'    => $freelancer->getId(),
                        'case'          => 'Freelancer get 1st job paid',
                        'job_id'        => $job->getId(),
                        'qty'           => '5',
                    )))){
                        return new JsonResponse(array('error' => 304));
                    }
                    
                    // Send Email
                    $context_invitee = array(
                        'invitee'   => $freelancer,
                        'job'       => $job,
                    );
                    $tpl_invitee = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitee_first_job_freelancer.html.twig';
                    try{
                        $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitee, $context_invitee, $from_vlance, $freelancer->getEmail());
                    } catch (Exception $ex) {
                        return new JsonResponse(array('error' => 305));
                    }
                    
                    // Send tracking event to Mixpanel
                    $this->container->get('vlance_base.helper')->trackAll(NULL, "Job done by referral", array(
                        'job_id'        => $job->getId(),
                        'job_category'  => $job->getCategory()->getTitle(),
                        'job_budget'    => $job->getBudget(),
                        'job_city'      => $job->getCity()->getName()
                    ));
                }
            }            
        }
        /* END Offer Credit - Case 1 - Get 5 Credit */
        
        /* Offer Credit - Case 2 - Get 30 Credit */
        // Invitee is client
        $acc_cl = $em->getRepository('VlanceAccountBundle:EmailInvite')->findOneByEmail($client->getEmail());
        if(is_object($acc_cl)){
            $job_finished_array = $em->getRepository('VlanceJobBundle:Job')->findBy(array('account' => $client->getId(), 'status' => Job::JOB_FINISHED, 'publish' => Job::PUBLISH));
            $job_finished = array();
            $count = 0;
            foreach($job_finished_array as $jn){
                if($jn->getBudgetBided() >= 5000000){
                    $count++;
                }
                $job_finished = $jn;
            }
            //check first job budget >= 5000000 post
            if($count == 1){
                /* @var $job_finished \Vlance\JobBundle\Entity\Job */
                if($job_finished->getId() == $job->getId()){
                    /* Add Credit for Invitor */
                    if(!($helper->addPromotionCredit($acc_cl->getAccount(), 0, 30, array(
                        'role'          => 'invitor',
                        'invitor_id'    => $acc_cl->getAccount()->getId(),
                        'case'          => 'Client hired 1st job',
                        'job_id'        => $job->getId(),
                        'qty'           => '30',
                    )))){
                        return new JsonResponse(array('error' => 306));
                    }
                    
                    // Send Email
                    $context_invitor = array(
                        'invitor'   => $acc_cl->getAccount(),
                        'invitee'   => $client,
                    );
                    $tpl_invitor = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitor_first_job_client.html.twig';
                    try{
                        $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitor, $context_invitor, $from_vlance, $acc_cl->getAccount()->getEmail());
                    } catch (Exception $ex) {
                        return new JsonResponse(array('error' => 307));
                    }
                    
                    
                    /* Add Credit for Invitee */
                    if(!($helper->addPromotionCredit($client, 0, 30, array(
                        'role'          => 'invitee',
                        'invitee_id'    => $client->getId(),
                        'case'          => 'Client hired 1st job',
                        'job_id'        => $job->getId(),
                        'qty'           => '30',
                    )))){
                        return new JsonResponse(array('error' => 308));
                    }

                    // Send Email
                    $context_invitee = array(
                        'invitee'   => $client,
                    );
                    $tpl_invitee = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitee_first_job_client.html.twig';

                    try{
                        $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitee, $context_invitee, $from_vlance, $client->getEmail());
                    } catch (Exception $ex) {
                        return new JsonResponse(array('error' => 309));
                    }
                    
                    // Send tracking event to Mixpanel
                    $this->container->get('vlance_base.helper')->trackAll(NULL, "Job hired by referral", array(
                        'job_id'        => $job->getId(),
                        'job_category'  => $job->getCategory()->getTitle(),
                        'job_budget'    => $job->getBudget(),
                        'job_city'      => $job->getCity()->getName()
                    ));
                }
            }
        } 
        /* END Offer Credit - Case 2 - Get 30 Credit */
        /**
         * END Offer Credit
         */
        
        return $this->redirect($this->generateUrl('workshop_bid', array('job_id' => $job->getId(),'bid' => $bid)));
    }
    
    /**
     * Edits an existing Job entity.
     *
     * @Route("/j/update/budget/{job_id}", name="job_update_budget")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job:edit.html.php", engine="php")
     */
    public function updateBudgetAction(Request $request, $job_id) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $acc_login Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        /* @var $job Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        if ($job->getAccount() !== $acc_login) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.wrong', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $form = $this->createForm(new JobBudgetType(), null);
        $form->bind($request);
        
        if ($form->isValid()) {
            $newBudget = $form->get('budget')->getData();
            $job->setBudget($newBudget);
            $em->persist($job);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.job.update.budget.success', array(), 'vlance'));
        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.job.update.budget.error', array(), 'vlance'));
        }
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
    
    /**
     * Bao cao vi pham mot job
     * @Route("/j/violated/{id}",name="job_violate")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function isViolatedAction($id) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        if($job->getIsViolated() == true) {
            return $this->redirect($this->generateUrl("500_page"), 301);
        } 
        $job->setIsViolated(true) ;
        $em->persist($job);
        $em->flush();
        // Gui mail thong bao cho admin.
        $from_email = $this->container->getParameter('from_email');
        $template = 'VlanceJobBundle:Email:admin_job_violated.html.twig';
        $context = array(
            'job' => $job,
        );
        $to = $this->container->getParameter('to_email');
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to);
        
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }

    /**
     * Display all bid in a job (type BID and type ONSITE)
     * @Route("/j/{id}/bid", name="job_show_bid")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:show_list_bid.html.php", engine="php")
     */
    public function listBidAction($id) {
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        $bid_current = array();
        $bids = array();
        
        if(is_object($acc_login)) {
            $acc_login = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $acc_login->getId()));
            /*Get bid account current*/
            $bid_current = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'account' => $acc_login->getId(), 'isDeclined' => false, 'publish' => Bid::PUBLISH));
            /*Get all bid (condition : publish, not decline, no account current)*/
            $bids = $em->getRepository('VlanceJobBundle:Bid')->findByNot(
                    array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH), 
                    array('account' => $acc_login->getId()),
                    array('isFeatured' => 'DESC'));
        } else {
            $bids = $em->getRepository('VlanceJobBundle:Bid')
                    ->findBy(
                            array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH),
                            array('isFeatured' => 'DESC')
                            );
        }
        /*Get bid declined*/
        $bids_declined = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'isDeclined' => true, 'publish' => Bid::PUBLISH));
        
        /* Counting number of featured bids */
        $nb_featured_bids = 0;
        foreach ($bids as $b){
            if($b->getIsFeatured()){
                $nb_featured_bids++;
            }
        }
        
        $jobs_worked = array();
        if(count($bid_current) > 0){
            $job_by_freelancer = $em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($bid_current[0]->getAccount());
            $jobs_worked[$bid_current[0]->getAccount()->getId()] = count($job_by_freelancer);
        }
        foreach($bids as $b){
            $job_by_freelancer = $em->getRepository('VlanceJobBundle:Job')->findJobsWorkedBy($b->getAccount());
            if(count($job_by_freelancer) > 0){
                $jobs_worked[$b->getAccount()->getId()] = count($job_by_freelancer);
            } else{
                $jobs_worked[$b->getAccount()->getId()] = 0;
            }
        }
        
        return array(
            'bids'              => $bids,
            'nb_featured_bids'  => $nb_featured_bids,
            'bid_current'       => $bid_current,
            'number_declined'   => count($bids_declined),
            'job'               => $job,
            'acc_login'         => $acc_login,
            'jobs_worked'       => $jobs_worked
        );
    }
    
    /**
     * Display all bid in a job
     * @Route("/cuoc-thi/{id}/bid", name="contest_show_bid")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/contest/view:list_contest_apply.html.php", engine="php")
     */
    public function listContestApplyAction($id) {
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
	
	
        $bid_current = array(); // bài dự thi của tài khoản đang đăng nhập nếu có
        $bids = array(); // Tất cả các bài dự thi
        //bid qualified
        $bid_qualified = array(); // Các bài dự thi được chọn
	$bid_not_qualified = array(); // Các bài dự thi không được chọn
        $display_pay_credit = 0;
	
        if(is_object($acc_login)) {
            $acc_login = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $acc_login->getId()));
            /*Get bid account current*/
            $bid_current = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'account' => $acc_login->getId(), 'isDeclined' => false, 'publish' => Bid::PUBLISH),array('wonAt' => 'DESC'));
            /*Get all bid (condition : publish, not decline, no account current)*/
            $bids = $em->getRepository('VlanceJobBundle:Bid')->findByNot(
                    array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH), 
                    array('account' => $acc_login->getId()),
                    array('wonAt' => 'DESC'));
            $bid_qualified = $em->getRepository('VlanceJobBundle:Bid')->findByNot(
                    array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH, 'isQualified' => true), 
                    array('account' => $acc_login->getId()),
		    array('wonAt' => 'DESC'));
//                    array('createdAt' => 'ASC'));
	    $bid_not_qualified = $em->getRepository('VlanceJobBundle:Bid')->findByNot(
		    array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH, 'isQualified' => 0),
		    array('account' => $acc_login->getId()),
		    array('createdAt' => 'ASC'));
	    
	    $current_num_bid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job));
	    $max_bid = $job->getMaxBid();
	    if($max_bid - count($current_num_bid) <= 0){
		$display_pay_credit = 1;
	    }
	    
        } else {
            $bids = $em->getRepository('VlanceJobBundle:Bid')
                    ->findBy(
                            array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH),
                            array('wonAt' => 'DESC')
                            );
            $bid_qualified = $em->getRepository('VlanceJobBundle:Bid')->findBy(
                    array('job' => $id, 'isDeclined' => 0, 'publish' => Bid::PUBLISH, 'isQualified' => true),
                    array('createdAt' => 'ASC'));
        }
        /*Get bid declined*/
        $bids_declined = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'isDeclined' => true, 'publish' => Bid::PUBLISH));
        
        // Build time remain
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($job->getCloseAt(), $now);
        
        /** XỬ LÝ BƯỚC **/
        $step = 1;
        /** Step 2: Chuyển sang bước KH chọn 4 sản phẩm. Ngày = closeAt() + 4 days */
        if($job->getStatus() === Job::JOB_OPEN){
	    //Nếu hết thời gian nhận bài dự thi
            if(JobCalculator::ago($job->getCloseAt(), $now) === false){
                {$step = 2;}
            }
        }
        /** END Step 2 */
        /** Step 3,4: Khi khách hàng có 4 ngày để trao đổi vs FL đã chọn*/
        if($job->getStatus() === Job::JOB_CONTEST_QUALIFIED){
            $step = 3;
            if($now->diff($job->getQualifiedAt())->format('%a') >= 4){$step = 4;}
        }
        /** END Step 3,4 */
        if($job->getStatus() === Job::JOB_FINISHED){
	    $step = 5;
	}
        /** END **/
        
        /** XỬ LÝ THỜI GIAN **/
        /** Step 2: Chuyển sang bước KH chọn 4 sản phẩm. Ngày = closeAt() + 4 days */
        if($step == 2){
            $remain = JobCalculator::ago($job->getCloseAt()->modify('+4 days'), $now);
        }
        /** END Step 2 */
        if($job->getStatus() === Job::JOB_CONTEST_QUALIFIED){
            if($step == 3){
                $remain = JobCalculator::ago($job->getQualifiedAt()->modify('+4 days'), $now);
            } else if($step == 4){
                $remain = JobCalculator::ago($job->getQualifiedAt()->modify('+7 days'), $now);
            }
        }
        /** END **/
        
        //Lấy số bài dự thi
        $bidQualified = array();
        $totalBid = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job->getId() , 'isDeclined' => 0, 'publish' => Bid::PUBLISH));
	$totalBid_real = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job->getId()));
        if($job->getStatus() != Job::JOB_OPEN){   
            $bidQualified = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $id, 'isDeclined' => 0, 'isQualified' => true, 'publish' => Bid::PUBLISH), array('createdAt' => 'ASC'));
        }

        return array(
            'bids'              => $bids,
            'bid_current'       => $bid_current,
	    'bid_not_qualified' => $bid_not_qualified,
            'remain'            => $remain,
            'number_declined'   => count($bids_declined),
            'job'               => $job,
            'acc_login'         => $acc_login,
            'number_files'      => count($totalBid), //Tổng số bài dự thi được hiển thị
	    'all_files'		=> count($totalBid_real), // Tổng số bài dự thi ( kể cả những bài không được hiển thị)
            'number_qualified'  => count($bidQualified), //Tổng số bài đc chọn
            'bid_qualified'     => $bid_qualified,
            'step'              => $step,
	    'selected'          => "selected",
	    'display_pay_credit'=> $display_pay_credit,
	    
        );
    }

    
    /**
     * Display all jobs create by account
     * @Route("/j/client/{filters}", name="jobs_create_project", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/workspace:posted_jobs.html.php", engine="php")
     */
    public function jobsCreateAction(Request $request, $filters = "") {
         
        $status = $request->get('_route_params');
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $current_user = $this->get('security.context')->getToken()->getUser();
//        $filters .= 'account-'.$current_user->getId();
//        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $params = array();        
        $params = $this->get('vlance_base.url')->pasteParameterOldFormat($filters);
        foreach($params as $key => $value) {
            $params[$key] = $value;
        }
        $params['j.account'] = $current_user->getId(); 
        $params['includePrivateJob'] = true;
        
        $param2 = $this->get('vlance_base.url')->pasteParameter($filters);
        foreach($param2 as $key => $value) {
            $params[$key] = $value;
        }        
        $em = $this->getDoctrine()->getManager();
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceJobBundle:Job')->getPagerWorkspace($params);
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        }
        catch (\Exception $e) {
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'jobs_create_project'));
        $options = array('proximity' => 3);
        return array(
            'pager' => $pager,
            'pagerView' => $pagerView,
            'jobs' => $pager->getCurrentPageResults(),
            'filters' => $filters,
            'status' => $status,
        );
    }
    
     /**
      * Display all jobs bided by account
     * @Route("/j/freelancer/{filters}", name="jobs_bid_project", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/workspace:bidded_jobs.html.php", engine="php")
     */
    public function jobsBidAction(Request $request, $filters = "") {
         
        $status = $request->get('_route_params'); 
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
             $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $current_user = $this->get('security.context')->getToken()->getUser();
        $params = array();
        
//        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $params = $this->get('vlance_base.url')->pasteParameterOldFormat($filters);
        
        foreach($params as $key => $value) {
            $params[$key] = $value;
        }
//        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $params['bid.account'] = $current_user->getId(); 
        $params['includePrivateJob'] = true;
        
        $param2 = $this->get('vlance_base.url')->pasteParameter($filters);
        foreach($param2 as $key => $value) {
            $params[$key] = $value;
        }
        $em = $this->getDoctrine()->getManager();
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceJobBundle:Job')->getPagerWorkspace($params, array('j.status' => 'ASC'));
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        }
        catch (\Exception $e) {
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'jobs_bid_project'));
        $options = array('proximity' => 3);
        
        return array(
            'pager' => $pager,
            'pagerView' => $pagerView,
            'jobs' => $pager->getCurrentPageResults(),
//            'filters' => $filters,
            'status' => $status,
        );
    }

    /**
     * List status project
     * 
     * @Route("/j/status/{type}/{filters}", name="project_list_status")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function listStatusAction($type, $filters = '') {
        $em = $this->getDoctrine()->getManager();
        $status = $em->getRepository('VlanceJobBundle:Job')->groupByField($field = 'status');
        
        return array(
            'entities' => $status,
            'params' => $filters,
            'type'  => $type,
            
        );
    }
    
    /**
     * 
     * @Route("/viec-lam-freelance/{filters}", name="freelance_job_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:list_job.html.php",engine="php")
     */
    public function listFreelanceJobAction(Request $request, $filters = "") {
        $em = $this->getDoctrine()->getManager();
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $current_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "freelance_job_list",
                    'authenticated' => is_object($current_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        // Processing old format filters
        $params = $this->get('vlance_base.url')->pasteParameterOldFormat($filters);
        if (isset($params["cat.path"]) || isset($params['cat.id']) || isset($params['city.id'])) {
            if (isset($params["cat.path"])) {
                // @todo Will remove this code because we will remove the shit character % in url
                $path = substr($params['cat.path'], 0, strpos($params['cat.path'], "%"));
                $hash = $em->getRepository('VlanceAccountBundle:Category')->getHashFromField(array('path' => $path));
                $params['cpath'] = $hash["hash"];
                unset($params['cat.path']);
            }
            
            if (isset($params["cat.id"])) {
                $ids = is_array($params['cat.id']) ? $params['cat.id'] : array($params['cat.id']);
                $chash = array();
                foreach ($ids as $id) {
                    $hash = $em->getRepository('VlanceAccountBundle:Category')->getHashFromField(array('id' => $id));
                    $chash[] = $hash["hash"];
                }
                $params['chash'] = $chash;
                unset($params['cat.id']);
            }
            
            if (isset($params["city.id"])) {
                $ids = is_array($params['city.id']) ? $params['city.id'] : array($params['city.id']);
                $cities = array();
                foreach ($ids as $id) {
                    $city = $em->getRepository('VlanceBaseBundle:City')->getHashFromField(array('id' => $id));
                    $cities[] = $city['hash'];
                }
                $params['city'] = $cities;
                unset($params['city.id']);
            }
            return $this->redirect($this->generateUrl('freelance_job_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params))), 301);
        }
        // END Processing old format filters
        
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        if (!empty($params) && !isset($params["cpath"]) && !isset($params['chash']) && !isset($params['city']) && !isset($params['page']) && !isset($params['status']) && !isset($params['kynang']) && !isset($params['closeAt']) ) {
            return $this->redirect($this->generateUrl('404_page'), 301);
        }
        
        if(isset($params['page']) && $params['page'] == 1){
            unset($params['page']);
            return $this->redirect($this->generateUrl('freelance_job_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params))), 301);
        }

        $this->get('session')->set('filter', $params);
        $this->get('session')->set('route', $request->attributes->get('_route'));
        
        // set session for city filter: call function to get number of jobs
        $this->get('session')->set(\Vlance\BaseBundle\Entity\CityRepository::SESS_FUNC, \Vlance\BaseBundle\Entity\CityRepository::JOB_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\SkillRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\SkillRepository::JOB_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\CategoryRepository::JOB_FUNC);
        
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceJobBundle:Job')->getPager($params);
            
            // Build data meta description
            $cities = array();
            $breakcrumbs = array();
            $breakcrumbs[] = array('title' => 'Việc freelance', 'params' => '');
            $implode_cat = array();
            $implode_city = array();
            $implode_skill = array();
            
            if (isset($params['cpath']) || isset($params['chash'])) {
                $des_cats = $em->getRepository('VlanceAccountBundle:Category')->getFieldFromParam($params);
                foreach ($des_cats as $des_cat) {
                    $implode_cat[] = $des_cat['title'];
                }
                $breadcrumbs[] = array('title' => $des_cats[count($des_cats) - 1]['title'], 'params' => $des_cats[count($des_cats) - 1]['hash']);
            }
            if (isset($params['city'])){
                $cities = $em->getRepository('VlanceBaseBundle:City')->getFieldFromParam($params);
                foreach($cities as $c){
                    $implode_city[] = $c['name'];
                }
            } 
            if (isset($params['kynang'])){
                $skills = $em->getRepository('VlanceAccountBundle:Skill')->getFieldFromParam($params);
                foreach($skills as $s){
                    $implode_skill[] = $s['title'];
                }
            }
                       
            $job_domain = implode(', ', array_merge($implode_cat,$implode_skill));
            if(strlen($job_domain) == 0){
                $job_domain = implode(', ', $implode_city);
            } else{
                if(strlen(implode(', ', $implode_city)) >0){
                    $job_domain .= " tại " . implode(', ', $implode_city);
                } else{
                    $job_domain .= implode(', ', $implode_city);
                }
            }
            $title = 'Việc freelance' . (strlen($job_domain)>2?" " . $job_domain:"");
            $descriptions = $this->get('translator')->trans('list_job.og_description_custom', array('%number%' => $pager->getNbResults(), '%linhvuc%' => $job_domain), 'vlance');
            
            $filteraccpage = count($pager->getCurrentPageResults());
            $_SESSION['filteraccpage'] = $filteraccpage;
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {}
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {}
        catch (\Exception $e) {/* return $this->redirect($this->generateUrl('vlance_homepage'));*/}
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'freelance_job_list'));
        
        // Thêm thẻ Noindex, Nofollow vào các trang có pagination,
        // trừ những trang ko có gì ngoài tham số page (để đảm bảo vẫn index tất cả các việc)
        $index_follow = 'noindex,nofollow';
        if(count($params) <= 1){
            if(!isset($params['page']) || $params['page'] != 1){
                // page n, without other filters OR page 1, with one filter
                $index_follow = 'index,follow';
            }
        } elseif(count($params) > 1){
            if(!isset($params['page'])){
                // first page, with multiple filters
                $index_follow = 'index,follow';
            }
        }
        // END Thêm thẻ Noindex, Nofollow
        
        // Switching between "All jobs" list and "Funded jobs" list
        $header_switch_status = array('nhan-chao-gia', 'nap-tien', 'tat-ca');   // Order is sensitive
        $header_switch = array();
        $is_there_active_el = false;                    // Flag to check if there is active element
        foreach($header_switch_status as $status){
            $switch_params = $params;
            unset($switch_params['page']);              // Remove paging when switch to other status
            if($status == 'nhan-chao-gia'){
                unset($switch_params['status']);        // Remove "status" parameter for the default option "nhan-chao-gia"
            } else{
                $switch_params['status'] = $status;
            }
            
            $header_switch_el = array(
                'name'  => $status,
                'count' => $em->getRepository('VlanceJobBundle:Job')->getPager($switch_params)->getNbResults(),
                'url'   => $this->generateUrl('freelance_job_list', array('filters' => $this->get('vlance_base.url')->buildUrl($switch_params))),
                'active'=> false
            );
            
            if(isset($params['status'])){
                if($params['status'] == $status){
                    $header_switch_el['active'] = true;
                    $is_there_active_el = true;
                }
            }
            
            $header_switch[$status] = $header_switch_el;
        }
        
        if($is_there_active_el == false || !isset($params['status'])){
            $header_switch['nhan-chao-gia']['active'] = true;
        }
        // END Switching
        
        $contest = $em->getRepository('VlanceJobBundle:Job')->findBy(array('type' => Job::TYPE_CONTEST, 'valid' => Job::VALID_CONTEST_YES), array('createdAt' => 'DESC'));
        $onsite = $em->getRepository('VlanceJobBundle:Job')->findBy(array('type' => Job::TYPE_ONSITE), array('createdAt' => 'DESC'));
        
        return array(
            'breadcrumbs'       => $breakcrumbs,
            'title'             => $title,
            'title_meta'        => ($filters=="")?"Việc freelance - Nhận tiền ngay khi hoàn thành":str_replace('"', '', $title),
            'description'       => JobCalculator::cut($descriptions, 150),
            'description_meta'  => ($filters=="")?">100 việc freelance mới mỗi tuần, thu nhập từ 500k - 30tr VNĐ tùy theo khả năng freelancer và loại hình công việc từng dự án. Nhận tiền đầy đủ khi xong việc. Làm Online từ xa các việc làm web, thiết kế web, marketing, viết bài, dịch thuật, quản lý fanpage,v.v. Khách hàng liên tục, Việt Nam & quốc tế. Đăng ký để nhận công việc đầu tiên.":JobCalculator::cut(str_replace('"', '', $descriptions), 150),
            'no_param'          => (count($params) == 0)?true:false,
            'pager'             => $pager,
            'pagerView'         => $pagerView,
            'entities'          => $pager->getCurrentPageResults(),
            'index_follow'      => $index_follow,
            'listing_switch'    => $header_switch,
            'count_jobs'        => array(
                'jobs'      => $em->getRepository('VlanceJobBundle:Job')->getPager(array('status' => 'tat-ca'))->getNbResults(),
                'contests'  => $em->getRepository('VlanceJobBundle:Job')->getContestList()->getNbResults(),
            ),
            'onsite'            => $onsite,
            'contest'           => $contest,
            'first_page'        => isset($params['page']) ? false : true,
        );
    }
    
    /**
     * 
     * @Route("/cuoc-thi-thiet-ke/{filters}", name="contest_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:list_contest.html.php",engine="php")
     */
    public function listContestAction(Request $request, $filters = "") {
        $em = $this->getDoctrine()->getManager();
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $current_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "contest_list",
                    'authenticated' => is_object($current_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        if (!empty($params) && !isset($params["cpath"]) && !isset($params['chash']) && !isset($params['page']) && !isset($params['closeAt']) ) {
            return $this->redirect($this->generateUrl('404_page'), 301);
        }
        
        if(isset($params['page']) && $params['page'] == 1){
            unset($params['page']);
            return $this->redirect($this->generateUrl('contest_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params))), 301);
        }

        $this->get('session')->set('filter', $params);
        $this->get('session')->set('route', $request->attributes->get('_route'));
        
        // set session for city filter: call function to get number of jobs
        $this->get('session')->set(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\CategoryRepository::JOB_CONTEST_FUNC);
        
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceJobBundle:Job')->getContestList($params);
            
            // Build data meta description
            $breakcrumbs = array();
            $breakcrumbs[] = array('title' => 'Cuộc thi thiết kế đồ họa', 'params' => '');
            $implode_cat = array();
            $implode_city = array();
            $implode_skill = array();
            
            if (isset($params['cpath']) || isset($params['chash'])) {
                $des_cats = $em->getRepository('VlanceAccountBundle:Category')->getFieldFromParam($params);
                foreach ($des_cats as $des_cat) {
                    $implode_cat[] = $des_cat['title'];
                }
                $breadcrumbs[] = array('title' => $des_cats[count($des_cats) - 1]['title'], 'params' => $des_cats[count($des_cats) - 1]['hash']);
            }
                       
            $job_domain = implode(', ', array_merge($implode_cat,$implode_skill));
            if(strlen($job_domain) == 0){
                $job_domain = implode(', ', $implode_city);
            } else{
                if(strlen(implode(', ', $implode_city)) >0){
                    $job_domain .= " tại " . implode(', ', $implode_city);
                } else{
                    $job_domain .= implode(', ', $implode_city);
                }
            }
            $title = 'Việc freelance' . (strlen($job_domain)>2?" " . $job_domain:"");
            $descriptions = $this->get('translator')->trans('list_job.og_description_custom', array('%number%' => $pager->getNbResults(), '%linhvuc%' => $job_domain), 'vlance');
            
            $filteraccpage = count($pager->getCurrentPageResults());
            $_SESSION['filteraccpage'] = $filteraccpage;
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {}
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {}
        catch (\Exception $e) {/* return $this->redirect($this->generateUrl('vlance_homepage'));*/}
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'contest_list'));
        
        // Thêm thẻ Noindex, Nofollow vào các trang có pagination,
        // trừ những trang ko có gì ngoài tham số page (để đảm bảo vẫn index tất cả các việc)
        $index_follow = 'noindex,nofollow';
        if(count($params) <= 1){
            if(!isset($params['page']) || $params['page'] != 1){
                // page n, without other filters OR page 1, with one filter
                $index_follow = 'index,follow';
            }
        } elseif(count($params) > 1){
            if(!isset($params['page'])){
                // first page, with multiple filters
                $index_follow = 'index,follow';
            }
        }
        // END Thêm thẻ Noindex, Nofollow
        
        return array(
            'breadcrumbs'       => $breakcrumbs,
            'title'             => $title,
            'title_meta'        => ($filters=="")?"Cuộc thi thiết kế - Nhận tiền ngay khi hoàn thành":str_replace('"', '', $title),
            'description'       => JobCalculator::cut($descriptions, 150),
            'description_meta'  => ($filters=="")?">Hàng chục cuộc thi thiết kế mới mỗi tuần. Nhận toàn bộ giải thưởng ngay khi thiết kế của bạn được chọn. Đăng ký và bắt đầu gửi bài tham gia cuộc thi.":JobCalculator::cut(str_replace('"', '', $descriptions), 150),
            'no_param'          => (count($params) == 0)?true:false,
            'pager'             => $pager,
            'pagerView'         => $pagerView,
            'entities'          => $pager->getCurrentPageResults(),
            'index_follow'      => $index_follow,
            'count_jobs'        => array(
                'jobs'      => $em->getRepository('VlanceJobBundle:Job')->getPager(array('status' => 'tat-ca'))->getNbResults(),
                'contests'  => $em->getRepository('VlanceJobBundle:Job')->getContestList()->getNbResults()
            )
        );
    }
    
    /**
     * return job list in json format
     * @Route("/json/job_typeahead/", name="job_json_typeahead_nostring")
     * @Route("/json/job_typeahead/{string}", name="job_json_typeahead")
     * @Method("GET")
     */
    public function jsonTypeaheadAction(Request $request, $string)
    {
        /* @var $auth_user /Vlance/AccountBundle/Entity/Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Not exist
        if (!$auth_user) {
            return new JsonResponse(array(
                'error' => 10
            ));
        }

        // Account is locked, redirect to 404 page. Only admin can view.
        if(!($auth_user->isAdmin())){
            return new JsonResponse(array(
                'error' => 20
            ));
        }
        
        $em = $this->getDoctrine()->getManager();
        $jobs = $em->getRepository('VlanceJobBundle:Job')->getTypeahead($string);
        return new JsonResponse($jobs);
    }
    
    /**
     * return job list in json format
     * @Route("/json/get_job/", name="job_json_get_job_noid")
     * @Route("/json/get_job/{id}", name="job_json_get_job")
     * @Method("GET")
     */
    public function jsonGetJobAction(Request $request, $id)
    {
        /* @var $auth_user /Vlance/AccountBundle/Entity/Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Not exist
        if (!$auth_user) {
            return new JsonResponse(array(
                'error' => 10
            ));
        }

        // Account is locked, redirect to 404 page. Only admin can view.
        if(!($auth_user->isAdmin())){
            return new JsonResponse(array(
                'error' => 20
            ));
        }
        
        // $id is invalid
        if(!is_numeric($id)){
            return new JsonResponse(array(
                'error' => 30
            ));
        }
     
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('id' => (int)($id)));
        if(!($job->getId())){
            return new JsonResponse(array(
                'error' => 40
            ));
        }
        
        return new JsonResponse(array(
            'error' => 0,
            'data'  => array(
                'id'    => $job->getId(),
                'title' => $job->getTitle()
            )
        ));
    }
    
    /**
     * Update 20160606: Xác nhận bid của contest
     * @Route("/cuoc-thi/confirm_bid", name="confirm_bid_contest")
     * @Method("POST")
     */
    public function confirmContestAction(Request $request){
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $em = $this->getDoctrine()->getManager();
        $now = new \DateTime('now');
        
        $bids = $request->request->get('bid');
        $hash = $request->request->get('job');
        /*@var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
        
        /** Gửi thư cho freelancer được chọn và không được chọn */
        $all_bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job));
        $bids_notselected = array();
        $bids_selected = array();
        
        //Mảng để sử dụng lấy id của bids đc chọn và không được chọn.
        $arr_id_all = array();
        $arr_id_notselected = array();
        $arr_id_selected = array();
        foreach($all_bids as $a){$arr_id_all[] = $a->getId();}
        
        if($bids){
            foreach($bids as $b){
                $bid = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $b));
                $bids_selected[] = $bid;
                $arr_id_selected[] = $bid->getId(); //array chứa Id bids đã đc chọn
                $bid->setIsQualified(true);
                $em->persist($bid);
            }
            $job->setStatus(Job::JOB_CONTEST_QUALIFIED);
            $job->setQualifiedAt($now);
            $em->persist($job);
            $em->flush();
            
            //Lấy bids k đc chọn
            $arr_id_notselected = array_diff($arr_id_all, $arr_id_selected);
            foreach($arr_id_notselected as $d){
                $decline = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $d));
                $bids_notselected[] = $decline;
            }
            
            $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            //Gui email cho freelancer có bài được chọn
            $template_fl_selected = "VlanceJobBundle:Email\contest:freelancer_selected_contest.html.twig";
            foreach($bids_selected as $ba){
                $email_context_fl_selected = array('contest' => $job, 'bid' => $ba);
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_fl_selected, $email_context_fl_selected, $email_hotro, $ba->getAccount()->getEmail());
            }
            
            //Send email to client
            $email_context_client = array('contest' => $job);
            $template_client = "VlanceJobBundle:Email/contest:client_contest_qualified.html.twig";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $email_context_client, $email_hotro, $job->getAccount()->getEmail());

            //Gui email cho freelancer không được chọn
            $template_fl_notselected = "VlanceJobBundle:Email\contest:freelancer_notselected_contest.html.twig";
            foreach($bids_notselected as $bd){
                $email_context_fl_notselected = array('contest' => $job, 'bid' => $bd);
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_fl_notselected, $email_context_fl_notselected, $email_hotro, $bd->getAccount()->getEmail());
            }
            /** End gửi email */
            
            //Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Bid qualified", array(
                'count'    => count($bids_selected),
                'bid_id'    => $arr_id_selected,
            ));
            
            return new JsonResponse(array('error' => 0));
        } else {
            return new JsonResponse(array('error' => 102));
        }
    }
    
    /**
     * Update 20160606: trao thưởng cuộc thi
     * @Route("/cuoc-thi/award", name="contest_award")
     * @Method("POST")
     */
    public function contestAwardAction(Request $request){
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $entity = $request->request->get('bid');
        $hash = $request->request->get('job');
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $hash));
        
        if (!$job->isOwner($acc_login)) {
            return new JsonResponse(array('error' => 102));
        }
        
        if ($job->getStatus() == Job::JOB_FINISHED && $acc_login != $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
            return new JsonResponse(array('error' => 103));
        }
     
        if($entity){
            /*@var $bid Bid*/
            $bid = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $entity));
            $client = $job->getAccount();
            $freelancer = $bid->getAccount();
            
            //Chuyển trang thái của job
            $job->setStatus(Job::JOB_FINISHED);
            $job->setWorker($freelancer);
            $em->persist($job);
            
            $now = new \DateTime('now');
            $bid->setWonAt($now);
            $em->persist($bid);
            
            //Get bids not win
            $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job, 'isQualified' => true));
            $bids_not_win = array();
            foreach($bids as $b){
                if(is_null($b->getWonAt())){
                    $bids_not_win[] = $b;
                }
            }
            
            /**
             * Payment part
             * - Update job cash
             * - Update job payment status -> paied
             * - Create paied transaction
             * - Update freelancer cash
             * - Create vlance fee transaction
             * - Update vlance cash
             */
            $vlance_parameter = $this->container->getParameter('vlance_system');
            $commission = $vlance_parameter['commission_contest'];
            $jobCash = $job->getCash();
            $jobAmmount = $jobCash->getBalance();
            $jobCash->setBalance(0);
            $em->persist($jobCash);

            $job->setPaymentStatus(Job::PAIED);
            $em->persist($job);
            
            $paied_trans = new Transaction();
            $paied_trans->setAmount($jobAmmount * (100 - $commission) / 100);
            $paied_trans->setSender($client);
            $paied_trans->setReceiver($freelancer);
            $paied_trans->setJob($job);
            $paied_trans->setType(Transaction::TRANS_TYPE_PAYMENT);
            $paied_trans->setStatus(Transaction::TRANS_TERMINATED);
            $paied_trans->setComment($this->get('translator')->trans('transaction.pay.default_comment',array(),'vlance'));
            $em->persist($paied_trans);
            
            $freelancerCash = $freelancer->getCash();
            $freelancerCash->setBalance($freelancerCash->getBalance() + $paied_trans->getAmount());
            $em->persist($freelancerCash);
            
            $fee_trans = new Transaction();
            $fee_trans->setAmount($jobAmmount * $commission / 100);
            $fee_trans->setSender($client);
            $fee_trans->setReceiver($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $fee_trans->setJob($job);
            $fee_trans->setType(Transaction::TRANS_TYPE_FEE);
            $fee_trans->setStatus(Transaction::TRANS_TERMINATED);
            $fee_trans->setComment($this->get('translator')->trans('transaction.fee.default_comment',array(),'vlance'));
            $em->persist($fee_trans);
            
            $vlanceCash = new \Vlance\PaymentBundle\Entity\VlanceCash();
            $vlanceCash->setBalance($fee_trans->getAmount());
            $vlanceCash->setTransaction($fee_trans);
            $em->persist($vlanceCash);
            
            /**
             * Statistic part
             */
            /* update outcome of client  */
            $client->setOutCome($client->getOutCome() + $bid->getAmount());
            $em->persist($client);

            /* update income, numjobfinsh of freelancer */
            $freelancer->setInCome($freelancer->getInCome() + $paied_trans->getAmount());
            $freelancer->setNumJobFinish($freelancer->getNumJobFinish() + 1);
            $em->persist($freelancer);
            
            /**
             * @todo write content for system message
             */
            $message = new Message();
            $message->setBid($bid);
            $message->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
            $message->setStatus(Message::NEW_MESSAGE);
            $message->setIsNotified(true);
            
            //Send message (from system) to freelancer
            $message->setReceiver($freelancer);
            $message->setContent(''.$client->getFullName().' đã xác nhận kết thúc công việc. Bạn hãy vào trang <strong>Quản lý</strong> để kiểm tra tài khoản và thực hiện lệnh rút tiền.');
            
            //Send message (from system) to client
            $message_to_client = new Message();
            $message_to_client->setBid($bid);
            $message_to_client->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
            $message_to_client->setStatus(Message::NEW_MESSAGE);
            $message_to_client->setIsNotified(true);
            $message_to_client->setReceiver($client);
            $message_to_client->setContent('Bạn đã xác nhận kết thúc cuộc thi.');
            $em->persist($message_to_client);
            
            $to = $freelancer->getEmail();
            $template = 'VlanceJobBundle:Email\contest:freelancer_contest_finished.html.twig';
            $context = array(
                    'job' => $job,
                    'bid' => $bid,
                    'freelancer' => $freelancer,
            );
            //Send Email to admin
            $template_admin = 'VlanceJobBundle:Email\contest:admin_contest_finished.html.twig';
            $context_admin = array(
                    'job' => $job,
                    'bid' => $bid,
                    'freelancer' => $freelancer,
            );
            //Guide client feedback for freelancer
            $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            $context_client = array(
                    'job' => $job,
                    'freelancer' => $freelancer,
            );
            $template_client = 'VlanceJobBundle:Email\contest:client_contest_finished.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $email_hotro, $client->getEmail());
            
            $em->persist($message);
            $em->flush();
            
            //Mixpanel
            $this->get('vlance_base.helper')->trackAll(NULL, "Bid won", array(
                'id'    => $freelancer->getId(),
            ));
            
            $from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            $to_admin = $this->container->getParameter('to_email');

            /* send email to client/freelancer by sendTWIGMessageBySES  */
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);

            /* send email to admin by sendTWIGMessageBySES  */
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from, $to_admin);
            
            // Send email freelancer not win
            $template_not_won = 'VlanceJobBundle:Email\contest:freelancer_notwon_contest.html.twig';
            foreach($bids_not_win as $bd){
                $email_context_fl_notwon = array('contest' => $job, 'bid' => $bd);
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_not_won, $email_context_fl_notwon, $email_hotro, $bd->getAccount()->getEmail());
            }
            
            return new JsonResponse(array('error' => 0));
        } else {
            return new JsonResponse(array('error' => 104));
        }
    }
    
    /**
     * Update 20160629
     * @Method("GET")
     * @Route("/j/new/catser", name="job_cat_ser")
     * @Template(engine="php")
     */
    public function catserAction(Request $request)
    {
        $cat_id = $request->query->get('cat_id');
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('VlanceAccountBundle:Category');
        $categories = $repo->findBy(array('parent' => null));
        
        $ser_id = array();
        $ser_tit = array();
        foreach($categories as $category){
            $sub_cats = $category->getChilds();
            foreach($sub_cats as $sub_cat) {
                if($sub_cat->getId() == $cat_id){
                    foreach($sub_cat->getServicesSorted() as $s){
                        $ser_id[] = $s->getId();
                        $ser_tit[] = $s->getTitle();
                    }
                }
            }
        }
        
        return new JsonResponse(array(
            'error' => 0,
            'id'    => $ser_id,
            'title' => $ser_tit,
        ));
    }
    
    /**
     * Update 20161012
     * @Method("GET")
     * @Route("/contest/new/price", name="contest_service_price")
     * @Template(engine="php")
     */
    public function contestPricingAction(Request $request)
    {
        $service_id = $request->query->get('ser_id');
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('VlanceAccountBundle:Service')->findOneById($service_id);
        
        if(!$service){
            return new JsonResponse(array('error' => 101));
        }
        
        $arr = array();
        if($service->getContestPricing() != NULL){
            foreach(json_decode($service->getContestPricing()) as $p){
                $arr[] = $p->price;
            }
        } else {
            $arr = array('1000000', '2000000', '4000000');
        }
        
        
        return new JsonResponse(array(
            'error'     => 0,
            'ser_id'    => $service->getId(),
            'price1'    => $arr[0],
            'price2'    => $arr[1],
            'price3'    => $arr[2],
        ));
    }
    
    /************* Job Onsite *************/
    /**
     * Displays a form to create a new Job onsite entity.
     *
     * @Route("/dang-viec-lam-onsite", name="job_onsite_new")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job\onsite:new.html.php",engine="php")
     */
    public function newJobOnsiteAction($entity = null) {
        /* @var $acc_login \Vlance\AccountBundle\Entity\Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $this->get('vlance_base.helper')->trackAll($acc_login, "View create onsite job page", array(
            'authenticated' => (is_object($acc_login)) ? 'true' : 'false',
            'version'       => "1",
            'new_design'    => "true"
         ));
        
        if(is_object($acc_login)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($acc_login->getEmail())){
                if($helper->emailNotFacebook($acc_login->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $acc_login->getId())));
                }
            }
        }
        
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobonsitetype', $entity);
        
        //Trả lại giá trị form lưu trong cookie khi submit bị lỗi
        if(isset($_COOKIE['JO_Data'])){
            foreach(json_decode($_COOKIE['JO_Data']) as $d){
                $data[] = $d;
            }
//            var_dump($data);die;
            $form->get('title')->setData($data[0]);
            $form->get('category')->setData($data[1]);
            $form->get('description')->setData($data[2]);
            $form->get('budget')->setData($data[3]);
            $form->get('budgetMax')->setData($data[4]);
//            $form->get('startedAt')->setData($data[5]->format('d/m/Y'));
            $form->get('duration')->setData($data[6]);
            $form->get('onsiteRequiredExp')->setData($data[7]);
            $form->get('city')->setData($data[8]);
            $form->get('onsiteLocation')->setData($data[9]);
            $form->get('onsiteCompanyName')->setData($data[10]);
        }

        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }
    
    /**
     * Creates a new Job onsite entity.
     *
     * @Route("/dang-viec-lam-onsite/submit", name="job_onsite_submit")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job\onsite:new.html.php",engine="php")
     */
    public function createJobOnsiteAction(Request $request) {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $acc_login \Vlance\AccountBundle\Entity\Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        // If current user is banned
        if ($acc_login->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.job', array('%%bannedTo%%' => $account->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }
        
        $parameters = $this->get('vlance_payment.helper')->getParameter("credit");
        $helper = $this->get('vlance_payment.helper');
        
        $entity = new Job();
        $form = $this->createForm('vlance_jobbundle_jobonsitetype', $entity);
        
        $now = new \DateTime('now');
        $close = $now->modify('+1 weeks');
        $entity->setCloseAt($close->setTime($now->format('H'), $now->format('i'), $now->format('s')));
        
        $form->bind($request);
        if ($form->isValid()) {
            //File
            $files = $entity->getFiles();
            foreach ($files as $idx => $file) {
                /* @var $file \Vlance\JobBundle\Entity\JobFile */
                if ($file instanceof \Vlance\JobBundle\Entity\JobFile) {
                    $file->setJob($entity);
                    $em->persist($file);
                } else {
                    $entity->getFiles()->remove($idx);
                }
            }
            
            $entity->setType(Job::TYPE_ONSITE);
            $entity->setAccount($acc_login);
            $acc_login->addJob($entity);
            $acc_login->setNumJob($acc_login->getNumJob() + 1);
            $em->persist($acc_login);
            
            //Insert skill
            $skills = explode(',', $_POST['hiddenTagListA']);
            
            // Insert skill khi tai khoan dang nhap them moi skill
            foreach($skills as $skill_new) {
                if(empty($skill_new)) {
                    break;
                }
                $acc_skill = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('title' => $skill_new));
                $entity->addSkill($acc_skill);
            }
            
            $start = $entity->getStartedAt();
            $entity->setStartedAt($start->setTime($now->format('H'), $now->format('i'), $now->format('s'))); 
            
            $em->persist($entity);            
            $em->flush();
            
//            $template = 'VlanceJobBundle:Email:admin_create_job.html.twig';
//            $context = array(
//                'job' => $entity,
//            );
//            $from = $this->container->getParameter('from_email');
//            $to = $this->container->getParameter('to_email');
//            
//            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);

            if(isset($_POST['onsite_option'])){
                $options = $_POST['onsite_option'];
                
                if(isset($parameters['pay_onsite'])){
                    $pay_onsite = $parameters['pay_onsite'];
                    $credit_pay = 0;
                    
                    if(isset($pay_onsite['cost'])){
                        $credit_pay += $pay_onsite['cost'];
                    }
                    
                    if(isset($pay_onsite['option'])){
                        foreach($options as $o){
                            switch ($o) {
                                case JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS:
                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS])){
                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS];
                                    }
                                    break;
//                                case JobOnsiteOption::ONSITE_OPTION_EMAIL:
//                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL])){
//                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL];
//                                    }
//                                    break;
//                                case JobOnsiteOption::ONSITE_OPTION_BANNER:
//                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER])){
//                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER];
//                                    }
//                                    break;
                                case JobOnsiteOption::ONSITE_OPTION_EXTEND_10:
                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10])){
                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10];
                                    }
                                    break;
                                case JobOnsiteOption::ONSITE_OPTION_EXTEND_30:
                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30])){
                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30];
                                    }
                                    break;
                                default :
                            }
                        }
                    }
                    
                    if($acc_login->getCredit()->getBalance() < $credit_pay){
                        //Lưu thông tin bid_form khi người dùng không đủ credit
                        $json = json_encode($_POST['vlance_jobbundle_jobonsitetype']);
                        setcookie('JO_Data', $json, time() + 3600, '/');

                        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Bạn không đủ credit để đăng việc Onsite.', array(), 'vlance'));
                        return $this->redirect($this->generateUrl('job_onsite_new', array(), true));
                    }
                    
                    if($pay_onsite['cost'] > 0){
                        $helper->createCreditExpenseTransaction(
                            $acc_login, $pay_onsite['cost'], 
                            CreditExpenseTransaction::SERVICE_JOB_ONSITE_POST, 
                            json_encode(array(
                                'job'               => $entity->getId(),
                                'job_type'          => 'onsite', 
                                'require_credit'    => $pay_onsite['cost']))
                        );
                    }
                    
                    foreach($options as $o){
                        $option = new JobOnsiteOption();
                        switch ($o) {
                            case JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_FEATURE_3DAYS, 
                                    json_encode(array(
                                        'job'               => $entity->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS])
                                    )
                                );
                                
                                $option->setJob($entity);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS);
                                $em->persist($option);

                                $now = new \DateTime('now');
                                $feature = $now->modify('+3 days');
                                $entity->setFeaturedUntil($feature->setTime($now->format('H'), $now->format('i'), $now->format('s')));
                                $em->persist($entity);
                                break;
//                            case JobOnsiteOption::ONSITE_OPTION_EMAIL:
//                                $helper->createCreditExpenseTransaction(
//                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL], 
//                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EMAIL, 
//                                    json_encode(array(
//                                        'job'             => $entity->getId(),
//                                        'job_type'        => 'onsite', 
//                                        'require_credit'  => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL])
//                                    )
//                                );
//                                
//                                $option->setJob($entity);
//                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EMAIL);
//                                $em->persist($option);
//                                break;
//                            case JobOnsiteOption::ONSITE_OPTION_BANNER:
//                                $helper->createCreditExpenseTransaction(
//                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER], 
//                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_BANNER, 
//                                    json_encode(array(
//                                        'job'                 => $entity->getId(),
//                                        'job_type'            => 'onsite', 
//                                        'require_credit'      => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER])
//                                    )
//                                );
//                                
//                                $option->setJob($entity);
//                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_BANNER);
//                                $em->persist($option);
//                                break;
                            case JobOnsiteOption::ONSITE_OPTION_EXTEND_10:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EXTEND_10, 
                                    json_encode(array(
                                        'job'               => $entity->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10])
                                    )
                                );
                                
                                $option->setJob($entity);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EXTEND_10);
                                $em->persist($option);

                                $close_add_week = $entity->getCloseAt()->modify('+1 weeks');
                                $entity->setCloseAt($close_add_week->setTime($now->format('H'), $now->format('i'), $now->format('s')));
                                $em->persist($entity);
                                break;
                            case JobOnsiteOption::ONSITE_OPTION_EXTEND_30:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EXTEND_30, 
                                    json_encode(array(
                                        'job'               => $entity->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30])
                                    )
                                );
                                
                                $option->setJob($entity);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EXTEND_30);
                                $em->persist($option);

                                $close_add_month = $entity->getCloseAt()->modify('+1 months');
                                $entity->setCloseAt($close_add_month->setTime($now->format('H'), $now->format('i'), $now->format('s')));
                                $em->persist($entity);
                                break;
                            default :
                        }
                    }
                }
            } else {
                if(isset($parameters['pay_onsite'])){
                    $pay_onsite = $parameters['pay_onsite'];
                    if(isset($pay_onsite['cost'])){
                        if((int)($pay_onsite['cost']) > $acc_login->getCredit()->getBalance()){
                            //Lưu thông tin bid_form khi người dùng không đủ credit
                            $json = json_encode($_POST['vlance_jobbundle_jobonsitetype']);
                            setcookie('JO_Data', $json, time() + 3600, '/');

                            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Bạn không đủ credit để đăng việc Onsite.', array(), 'vlance'));
                            return $this->redirect($this->generateUrl('job_onsite_new', array(), true));
                        } else {
                            if($pay_onsite['cost'] > 0){
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['cost'], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_POST, 
                                    json_encode(array(
                                        'job'               => $entity->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['cost']))
                                );
                            }
                        }
                    }
                } 
            }
            
            $em->flush();
            
            /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(JobEvent::JOB_CREATE_COMPLETED, new JobEvent($entity));
            
            $this->get('session')->set('createjob', 'new');
            
            /** Tracking event "Create job onsite" **/
            // Mixpanel
            $tracking_array = array(
                'category'      => ($entity->getCategory())?$entity->getCategory()->getTitle():NULL,
                'budget'        => $entity->getBudget(),
                'city'          => ($entity->getCity())?$entity->getCity()->getName():NULL,
                'skills_count'  => count($entity->getSkills()),
            );
            
            $this->get('vlance_base.helper')->trackAll(NULL, "Onsite job created", $tracking_array);
            
            // Google Adwords
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Google Code for Create job onsite Conversion Page -->
                    <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 925105271;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "6Ml0CMnjg1oQ9_iPuQM";
                    var google_conversion_value = '.$entity->getBudget().'.00;
                    var google_conversion_currency = "VND";
                    var google_remarketing_only = false;
                    /* ]]> */
                    </script>
                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                    </script>
                    <noscript>
                    <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$entity->getBudget().'.00&amp;currency_code=VND&amp;label=6Ml0CMnjg1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                    </div>
                    </noscript>'
                )
            ));
            
            // Facebook
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Facebook Conversion Code for Create job onsite-->
                    <script>(function() {
                      var _fbq = window._fbq || (window._fbq = []);
                      if (!_fbq.loaded) {
                        var fbds = document.createElement("script");
                        fbds.async = true;
                        fbds.src = "//connect.facebook.net/en_US/fbds.js";
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(fbds, s);
                        _fbq.loaded = true;
                      }
                    })();
                    window._fbq = window._fbq || [];
                    window._fbq.push(["track", "6023661433485", {"value":"'.$entity->getBudget().'.00","currency":"VND"}]);
                    </script>
                    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6023661433485&amp;cd[value]='.$entity->getBudget().'.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>'
                )
            ));
            /** END Tracking event "Create job" **/  
            
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Đăng dự án thành công', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $entity->getHash())));
        }
        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
            'acc'       => $acc_login,
            'form_able' => true,
        );
    }
    
    /**
     * Finds and displays a Job Onsite entity.
     *
     * @Route("/viec-onsite/{hash}", name="job_onsite_view")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/onsite:show.html.php",engine="php")
     */
    public function showJobOnsiteAction(Request $request, $hash)
    {
        /* @var $current_user Account */
//        $current_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        /* @var $entity Job */
        $entity = $em->getRepository('VlanceJobBundle:Job')->findOneByHash($hash);
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 302);
        }
        
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        if(is_object($current_user)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($current_user->getEmail())){
                if($helper->emailNotFacebook($current_user->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $current_user->getId())));
                }
            }
        }
        
        // Redirect to correct view of job
        switch($entity->getType()){
            case Job::TYPE_BID:
                return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $entity->getHash()), true), 301);
            case Job::TYPE_CONTEST:
                return $this->redirect($this->generateUrl('job_contest_view', array('hash' => $entity->getHash()), true), 301);
        }

        $abs_url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getRequestUri();
        if(in_array($this->container->getParameter('kernel.environment'), array('dev'))){
            $abs_url = "http://www.vlance.vn";
        }
        
        // Build time remain
        $now = new \DateTime('now');
        $remain = JobCalculator::ago($entity->getCloseAt(), $now);
        
        $view_as_job_owner  = false;
        $view_as_freelancer = false;
        $view_as_bid_owner  = false;
        if($current_user === $entity->getAccount()){
            $view_as_job_owner = true;
        } else{
            $view_as_freelancer = true;
        }
        foreach($entity->getBids() as $b){
            if($b->getAccount() === $current_user){
                $view_as_bid_owner = true;
                break;
            }
        }
        
        
        return array(
            'abs_url'           => $abs_url,
            'remain'            => $remain,
            'meta_title'        => strip_tags($entity->getTitle()),
            'meta_description'  => strip_tags($entity->getDescription()),
            'entity'            => $entity,
            'current_user'      => $current_user,
            'view_as_job_owner' => $view_as_job_owner,
            'view_as_freelancer'=> $view_as_freelancer,
            'view_as_bid_owner' => $view_as_bid_owner
        );
    }
    
    /**
     * Edit Job onsite entity.
     *
     * @Route("/viec-onsite/{id}/edit", name="job_onsite_edit")
     * @Method("GET")
     * @Template("VlanceJobBundle:Job/onsite:edit.html.php",engine="php")
     */
    public function editJobOnsiteAction($id) {
        /* @var $acc_login \Vlance\AccountBundle\Entity\Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('VlanceJobBundle:Job')->findOneById($id);
        if(!is_object($entity)){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Đã có lỗi xảy ra. Vui lòng thử lại.', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        if(is_object($acc_login)){
            if($acc_login !== $entity->getAccount()){
                if(HasPermission::hasAdminPermission($acc_login) === FALSE){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Bạn không có quyền thực hiện việc này.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('vlance_homepage'));//Sửa lại về trang show job
                }
            }
        } else {
            return $this->redirect($this->generateUrl("404_page"), 302);
        }
        
        $form = $this->createForm('vlance_jobbundle_jobonsitetype', $entity);
        
        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
        );
    }
    
    /**
     * Update Job onsite entity.
     *
     * @Route("/viec-onsite/{id}/update", name="job_onsite_update")
     * @Method("POST")
     * @Template("VlanceJobBundle:Job\onsite:new.html.php",engine="php")
     */
    public function updateJobOnsiteAction(Request $request, $id) {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $acc_login \Vlance\AccountBundle\Entity\Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        
        $entity = $em->getRepository('VlanceJobBundle:Job')->findOneById($id);
        if(!is_object($entity)){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Đã có lỗi xảy ra. Vui lòng thử lại.', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        if(HasPermission::hasAdminPermission($acc_login) == FALSE && !$entity->isOwner($acc_login)) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        $files_old = array();
        foreach ($entity->getFiles() as $file) {
            $files_old[] = $file->getId();
        }
        
        $form = $this->createForm('vlance_jobbundle_jobonsitetype', $entity);
        
        $form->bind($request);
        if ($form->isValid()) {
            //File
            $new_arr = array();
            foreach ($entity->getFiles() as $file) {
                $file->setJob($entity);
                $em->persist($file);
                $new_arr[] = $file->getId();
            }
            
            foreach($files_old as $fo){
                if(!in_array($fo, $new_arr)){
                    $file = $em->getRepository('VlanceJobBundle:JobFile')->findOneById($fo);
                    $em->remove($file);
                }
            }
            $em->persist($entity);
            
            //Update skill 
            $skilled = $entity->getSkills();
            $skill_old = array();
            $skills = explode(',', $request->get('hiddenTagListA', false));
            //Remove skill
            foreach($skilled as $skill) {
                if(!in_array($skill->getTitle(), $skills)){
                    $entity->removeSkill($skill);
                }
                $skill_old[] = $skill->getTitle();
            }
            // Insert skill
            foreach($skills as $skill_new) {
                if(empty($skill_new)) {
                    break;
                }
                elseif(!in_array($skill_new, $skill_old)) {
                    $acc_skill = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('title' => $skill_new));
                    $entity->addSkill($acc_skill);
                }
            }
            
            $em->persist($entity);            
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Cập nhật dự án thành công', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_onsite_view', array('hash' => $entity->getHash())));
        }
        return array(
            'entity'    => $entity,
            'form'      => $form->createView(),
            'acc'       => $acc_login,
            'form_able' => true,
        );
    }
    
    /**
     * @Route("/viec-onsite/option", name="onsite_option_view")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function optionOnViewOnsiteAction(Request $request)
    {
        $job_id = $request->request->get('job_id');
        $option_array = $request->request->get('option');
        
        $parameters = $this->get('vlance_payment.helper')->getParameter("credit");
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        
        if(is_null($option_array)){
            return new JsonResponse(array('error' => 101));
        }
        
        if(is_null($job_id)){
            return new JsonResponse(array('error' => 102));
        }
        
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneById($job_id);
        if(!is_object($job)){
            return new JsonResponse(array('error' => 103));
        }
        
        $acc_login = $this->get('security.context')->getToken()->getUser();
        if(!is_object($acc_login)){
            return new JsonResponse(array('error' => 104));
        }
        
        $credit_pay = 0;
        if(isset($parameters['pay_onsite'])){
            $pay_onsite = $parameters['pay_onsite'];
            if(isset($pay_onsite['option'])){
                foreach($option_array as $o){
                    switch ($o) {
                        case JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS:
                            if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS])){
                                $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS];
                            }
                            break;
//                                case JobOnsiteOption::ONSITE_OPTION_EMAIL:
//                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL])){
//                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL];
//                                    }
//                                    break;
//                                case JobOnsiteOption::ONSITE_OPTION_BANNER:
//                                    if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER])){
//                                        $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER];
//                                    }
//                                    break;
                        case JobOnsiteOption::ONSITE_OPTION_EXTEND_10:
                            if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10])){
                                $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10];
                            }
                            break;
                        case JobOnsiteOption::ONSITE_OPTION_EXTEND_30:
                            if(isset($pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30])){
                                $credit_pay += $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30];
                            }
                            break;
                        default :
                    }
                }

                if($acc_login->getCredit()->getBalance() < $credit_pay){
                    return new JsonResponse(array('error' => 105));
                } else {
                    foreach($option_array as $o){
                        $option = new JobOnsiteOption();
                        switch ($o) {
                            case JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_FEATURE_3DAYS, 
                                    json_encode(array(
                                        'job'               => $job->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS])
                                    )
                                );
                                
                                $option->setJob($job);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS);
                                $em->persist($option);
                                
                                $now = new \DateTime('now');
                                $remain = JobCalculator::ago($job->getCloseAt(), $now);
                                if($remain === false){
                                    $close_add_week = $now->modify('+1 weeks');
                                    $job->setCloseAt($close_add_week->setTime($close_add_week->format('H'), $close_add_week->format('i'), $close_add_week->format('s')));
                                    
                                    if(is_null($job->getFeaturedUntil())){
                                        $feature = $now->modify('+3 days');
                                        $job->setFeaturedUntil($feature->setTime($feature->format('H'), $feature->format('i'), $feature->format('s')));
                                    } else {
                                        if(JobCalculator::ago($job->getFeaturedUntil(), $now) === false){
                                            $feature = $now->modify('+3 days');
                                        } else {
                                            $feature = $job->getFeaturedUntil()->modify('+3 days');
                                        }
                                        $job->setFeaturedUntil($feature->setTime($feature->format('H'), $feature->format('i'), $feature->format('s')));
                                    }
                                } else {
                                    if(is_null($job->getFeaturedUntil())){
                                        $feature = $now->modify('+3 days');
                                        $job->setFeaturedUntil($feature->setTime($feature->format('H'), $feature->format('i'), $feature->format('s')));
                                    } else {
                                        if(JobCalculator::ago($job->getFeaturedUntil(), $now) === false){
                                            $feature = $now->modify('+3 days');
                                        } else {
                                            $feature = $job->getFeaturedUntil()->modify('+3 days');
                                        }
                                        $job->setFeaturedUntil($feature->setTime($feature->format('H'), $feature->format('i'), $feature->format('s')));
                                    }
                                }
                                $em->persist($job);
                                break;
//                            case JobOnsiteOption::ONSITE_OPTION_EMAIL:
//                                $helper->createCreditExpenseTransaction(
//                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL], 
//                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EMAIL, 
//                                    json_encode(array(
//                                        'job'             => $job->getId(),
//                                        'job_type'        => 'onsite', 
//                                        'require_credit'  => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EMAIL])
//                                    )
//                                );
//                                
//                                $option->setJob($job);
//                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EMAIL);
//                                $em->persist($option);
//                                break;
//                            case JobOnsiteOption::ONSITE_OPTION_BANNER:
//                                $helper->createCreditExpenseTransaction(
//                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER], 
//                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_BANNER, 
//                                    json_encode(array(
//                                        'job'                 => $job->getId(),
//                                        'job_type'            => 'onsite', 
//                                        'require_credit'      => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_BANNER])
//                                    )
//                                );
//                                
//                                $option->setJob($job);
//                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_BANNER);
//                                $em->persist($option);
//                                break;
                            case JobOnsiteOption::ONSITE_OPTION_EXTEND_10:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EXTEND_10, 
                                    json_encode(array(
                                        'job'               => $job->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_10])
                                    )
                                );
                                
                                $option->setJob($job);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EXTEND_10);
                                $em->persist($option);

                                $remain = JobCalculator::ago($job->getCloseAt(), $now);
                                if($remain === false){
                                    $now = new \DateTime('now');
                                    $close_add_week = $now->modify('+1 weeks');
                                } else {
                                    $close_add_week = $job->getCloseAt()->modify('+1 weeks');
                                }
                                $job->setCloseAt($close_add_week->setTime($close_add_week->format('H'), $close_add_week->format('i'), $close_add_week->format('s')));
                                $em->persist($job);
                                break;
                            case JobOnsiteOption::ONSITE_OPTION_EXTEND_30:
                                $helper->createCreditExpenseTransaction(
                                    $acc_login, $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30], 
                                    CreditExpenseTransaction::SERVICE_JOB_ONSITE_OPTION_EXTEND_30, 
                                    json_encode(array(
                                        'job'               => $job->getId(),
                                        'job_type'          => 'onsite', 
                                        'require_credit'    => $pay_onsite['option'][JobOnsiteOption::ONSITE_OPTION_EXTEND_30])
                                    )
                                );
                                
                                $option->setJob($job);
                                $option->setOnsiteOption(JobOnsiteOption::ONSITE_OPTION_EXTEND_30);
                                $em->persist($option);

                                $remain = JobCalculator::ago($job->getCloseAt(), $now);
                                if($remain === false){
                                    $now = new \DateTime('now');
                                    $close_add_week = $now->modify('+1 months');
                                } else {
                                    $close_add_week = $job->getCloseAt()->modify('+1 months');
                                }
                                $job->setCloseAt($close_add_week->setTime($close_add_week->format('H'), $close_add_week->format('i'), $close_add_week->format('s')));
                                $em->persist($job);
                                break;
                            default :
                        }
                    }
                    $em->flush();
                }
                
                return new JsonResponse(array('error' => 0));
            } else {
                return new JsonResponse(array('error' => 106));
            }
        } else {
            return new JsonResponse(array('error' => 107));
        }
    }
    
    /**
     * 
     * @Route("/viec-lam-onsite/{filters}", name="onsite_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlanceJobBundle:Job:list_onsite.html.php",engine="php")
     */
    public function listOnsiteAction(Request $request, $filters = "") {
        $em = $this->getDoctrine()->getManager();
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $acc_login = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "onsite_list",
                    'authenticated' => is_object($acc_login) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        if (!empty($params) && !isset($params["cpath"]) && !isset($params['chash']) && !isset($params['kynang']) && !isset($params['city']) && !isset($params['page']) && !isset($params['closeAt']) ) {
            return $this->redirect($this->generateUrl('404_page'), 301);
        }
        
        if(isset($params['page']) && $params['page'] == 1){
            unset($params['page']);
            return $this->redirect($this->generateUrl('onsite_list', array('filters' => $this->get('vlance_base.url')->buildUrl($params))), 301);
        }

        $this->get('session')->set('filter', $params);
        $this->get('session')->set('route', $request->attributes->get('_route'));
        
        // set session for city filter: call function to get number of jobs
        $this->get('session')->set(\Vlance\BaseBundle\Entity\CityRepository::SESS_FUNC, \Vlance\BaseBundle\Entity\CityRepository::JOB_ONSITE_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\SkillRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\SkillRepository::JOB_ONSITE_FUNC);
        $this->get('session')->set(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC, \Vlance\AccountBundle\Entity\CategoryRepository::JOB_ONSITE_FUNC);
        
        $pager = null;
        try {
            $pager = $em->getRepository('VlanceJobBundle:Job')->getOnsiteList($params);
            
            // Build data meta description
            $implode_cat = array();
            $implode_city = array();
            $implode_skill = array();
            
            if (isset($params['cpath']) || isset($params['chash'])) {
                $des_cats = $em->getRepository('VlanceAccountBundle:Category')->getFieldFromParam($params);
                foreach ($des_cats as $des_cat) {
                    $implode_cat[] = $des_cat['title'];
                }
            }
                       
            $job_domain = implode(', ', array_merge($implode_cat,$implode_skill));
            if(strlen($job_domain) == 0){
                $job_domain = implode(', ', $implode_city);
            } else{
                if(strlen(implode(', ', $implode_city)) >0){
                    $job_domain .= " tại " . implode(', ', $implode_city);
                } else{
                    $job_domain .= implode(', ', $implode_city);
                }
            }
            $title = 'Việc freelance' . (strlen($job_domain)>2?" " . $job_domain:"");
            $descriptions = $this->get('translator')->trans('list_job.og_description_custom', array('%number%' => $pager->getNbResults(), '%linhvuc%' => $job_domain), 'vlance');
            
            $filteraccpage = count($pager->getCurrentPageResults());
            $_SESSION['filteraccpage'] = $filteraccpage;
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {}
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {}
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {}
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {}
        catch (\Exception $e) {/* return $this->redirect($this->generateUrl('vlance_homepage'));*/}
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'onsite_list'));
        
        // Thêm thẻ Noindex, Nofollow vào các trang có pagination,
        // trừ những trang ko có gì ngoài tham số page (để đảm bảo vẫn index tất cả các việc)
        $index_follow = 'noindex,nofollow';
        if(count($params) <= 1){
            if(!isset($params['page']) || $params['page'] != 1){
                // page n, without other filters OR page 1, with one filter
                $index_follow = 'index,follow';
            }
        } elseif(count($params) > 1){
            if(!isset($params['page'])){
                // first page, with multiple filters
                $index_follow = 'index,follow';
            }
        }
        // END Thêm thẻ Noindex, Nofollow
        
        return array(
            'title'             => $title,
            'description'       => JobCalculator::cut($descriptions, 150),
            'no_param'          => (count($params) == 0)?true:false,
            'pager'             => $pager,
            'pagerView'         => $pagerView,
            'entities'          => $pager->getCurrentPageResults(),
            'index_follow'      => $index_follow,
            'count_jobs'        => array(
                'jobs'          => $em->getRepository('VlanceJobBundle:Job')->getPager(array('status' => 'tat-ca'))->getNbResults(),
                'onsite'        => $em->getRepository('VlanceJobBundle:Job')->getOnsiteList()->getNbResults()
            )
        );
    }
    
    /************* END Job Onsite *************/
    
    /**
     * @Route("/viec-freelance/credit-workroom", name="credit_workroom")
     * @Method("POST")
     */
    public function creditWorkroomAction(Request $request){
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('vlance_payment.helper');
        $jid = $request->request->get('jid');
        
        if(is_null($jid)){
            return new JsonResponse(array('error' => 102));
        }
        
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('Vlance\JobBundle\Entity\Job')->findOneBy(array('id' => $jid));
        
        if(!is_object($job)){
            return new JsonResponse(array('error' => 103));
        }
        
        /* @var $acc_login \Vlance\AccountBundle\Entity\Account */
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $credit = $acc_login->getCredit();
        $parameters = $this->get('vlance_payment.helper')->getParameter("credit");
        
        if(isset($parameters['pay_to_chat'])){
            $pay_chat = $parameters['pay_to_chat'];
            if($credit->getBalance() < $pay_chat['cost']){
                return new JsonResponse(array('error' => 104));
            } else {
                $helper->createCreditExpenseTransaction(
                    $acc_login, $pay_chat['cost'], 
                    CreditExpenseTransaction::SERVICE_BID_INTERVIEW, 
                    json_encode(array(
                        'job'               => $job->getId(),
                        'job_type'          => 'bid',
                        'type'              => 'interview',
                        'require_credit'    => $pay_chat['cost']))
                );
                
                $job->setAvailableConversation($job->getAvailableConversation() + 1);
                $em->persist($job);
                
                $em->flush();
                
                
                
                return new JsonResponse(array('error' => 0));
            }
        } else {
            return new JsonResponse(array('error' => 105));
        }
        
//        if($credit <)
    }
    
    /**
     * 
     * @Route("/j/{filters}", name="job_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request, $filters = "") {
        return $this->redirect($this->generateUrl('freelance_job_list',array('request' => $request, 'filters' => $filters)),301);
    }
}
