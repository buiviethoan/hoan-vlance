<?php
namespace Vlance\JobBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Message;
use Vlance\JobBundle\Entity\Job;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\BaseBundle\Utils\JobCalculator;

/**
 * Admin Job controller.
 *
 * @Route("/")
 */
class AdminController extends Controller {
    /**
     * @Route("/admin-job/block/dashboard/{_route_params}", name="admin_job_block_dashboard")
     * @Method("GET")
     * @Template("VlanceJobBundle:Admin:dashboard.html.php",engine="php")
     */
    public function dashboardAction($_route_params) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        $_params = unserialize($_route_params['_route_params']);
        
        if(isset($_params["hash"])){
            /* @var $job \Vlance\JobBundle\Entity\Job */
            $job = $em->getRepository("VlanceJobBundle:Job")->findOneByHash($_params["hash"]);
            if (!$job) {
                return new Response("");
            }

            $trans = $em->getRepository('VlancePaymentBundle:Transaction')->findBy(array('job' => $job));
            
            return array(
                'job' => $job,
                'transaction' => $trans,
                '_route_params' => $_route_params
            );
        }
        return new Response("");
    }
    
    /**
     * @Route("/j/deactive/{job_id}", name="job_deactive_admin")
     * @Method("GET")
     * @Template("VlanceJobBundle:Admin:dashboard/display/hide_job.html.php",engine="php")
     */
    public function deactiveAction($job_id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }

        if(!$job_id){
            return new Response("");
        }
        
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository("VlanceJobBundle:Job")->findOneById($job_id);
        if (!$job){
            return new Response("");
        } 
        
        return array(
            'job' => $job,
            '_csrf_token' => $this->get('form.csrf_provider')->generateCsrfToken($this->generateUrl("Vlance_JobBundle_AdminJob_object",array("pk" => $job->getId(), "action" => "deactive"))),
            "confirm_url" => $this->generateUrl("Vlance_JobBundle_AdminJob_object",array("pk" => $job->getId(), "action" => "deactive")),
        );
    }
    
    /**
     * @Route("/j/admin/cancel_award_not_fund/{job_id}", name="admin_job__cancel_award_not_fund")
     * @Method("GET")
     * @Template("VlanceJobBundle:Admin:dashboard/status/cancel_award_not_fund.html.php",engine="php")
     */
    public function cancelAwardNotFundAction($job_id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }

        if(!$job_id){
            return new Response("");
        }
        
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository("VlanceJobBundle:Job")->findOneById($job_id);
        if (!$job){
            return new Response("");
        } 
        
        return array(
            'job' => $job,
            '_csrf_token' => $this->get('form.csrf_provider')->generateCsrfToken($this->generateUrl("Vlance_JobBundle_AdminJob_object",array("pk" => $job->getId(), "action" => "deactive"))),
            "confirm_url" => $this->generateUrl("Vlance_JobBundle_AdminJob_object",array("pk" => $job->getId(), "action" => "deactive")),
        );
    }
    
    /**
     * @Route("/j/admin/close_job/{job_id}", name="admin_job_close")
     * @Method("GET")
     * @Template("VlanceJobBundle:Admin:dashboard/status/admin_action.html.php",engine="php")
     */
    public function closeJobAction(Request $request, $job_id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }

        if(!$job_id){
            return new Response("");
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository("VlanceJobBundle:Job")->findOneById($job_id);
        if (!$job){
            return new Response("");
        } 
        
        if($job->getTestType() === 1){
            $tran_es = $em->getRepository('VlancePaymentBundle:Transaction')->findBy(array(
                                                    'type'      => Transaction::TRANS_TYPE_DEPOSIT_UPFRONT, 
                                                    'status'    => Transaction::TRANS_TERMINATED,
                                                    'job'       => $job
                                                ));
            
            $amount = 0;
            if(!$tran_es){
                return new Response("");
            } else {
                foreach($tran_es as $t){
                    $amount += $t->getAmount();
                }
            }
            
            $refunTrans = new Transaction();
            $refunTrans->setType(Transaction::TRANS_TYPE_REFUND);
            $refunTrans->setAmount($amount);
            $refunTrans->setReceiver($job->getAccount());
            $refunTrans->setJob($job);
            $refunTrans->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $refunTrans->setComment($this->get('translator')->trans('controller.transaction.refun.comment', array("%%job_title%%" => $job->getTitle()), 'vlance'));
            $refunTrans->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($refunTrans);
            
            $jobCash = $job->getCash();
            $userCash = $job->getAccount()->getCash();
            
            $usercash_balance = $userCash->getBalance() + $jobCash->getBalance();
            $userCash->setBalance($usercash_balance);
            $em->persist($userCash);
            
            $jobCash->setBalance(0);
            $em->persist($jobCash);
        }
        
        $job->setStatus(Job::JOB_CLOSED);
        $em->persist($job);
        
        $em->flush();
        
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Đóng dự án thành công', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
    
    /**
     * @Route("/j/admin/return_open_job/{job_id}", name="admin_job_return_open")
     * @Method("GET")
     * @Template("VlanceJobBundle:Admin:dashboard/status/admin_action.html.php",engine="php")
     */
    public function returnOpenJobAction(Request $request, $job_id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }

        if(!$job_id){
            return new Response("");
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository("VlanceJobBundle:Job")->findOneById($job_id);
        if (!$job){
            return new Response("");
        } 
        
        if($job->getStatus() === Job::JOB_AWARDED){
            $now = new \DateTime('now');            
            $job->setStatus(Job::JOB_OPEN);
            $job->setCloseAt($now->modify('+7 days'));
            $job->setWorker(NULL);
            $job->setStartedAt(NULL);
            $job->setPaymentStatus(Job::WAITING_ESCROW);
            $em->persist($job);
            
            foreach ($job->getBids() as $b){
                /* @var $b \Vlance\JobBundle\Entity\Bid */
                if(!is_null($b->getAwardedAt())){
                    $b->setAwardedAt(NULL);
                    $em->persist($b);
                }
            }
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Dự án đã quay trở lại bước chào giá', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
    
    /**
     * @Route("/j/admin/cancel_job_refund/{job_id}", name="admin_job_cancel")
     * @Method("POST")
     * @Template("VlanceJobBundle:Admin:dashboard/status/admin_action.html.php",engine="php")
     */
    public function cancelJobAction(Request $request, $job_id) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("");
        }

        if(!$job_id){
            return new Response("");
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository("VlanceJobBundle:Job")->findOneById($job_id);
        if (!$job){
            return new Response("");
        } 
        
        if(!$_POST['type_cancel']){
            return new Response("");
        }

        $jobCash = $job->getCash();
        $userCash = $job->getAccount()->getCash();
        
        if ($job->getPaymentStatus() !== Job::REFUNED) {
            $job->setPaymentStatus(Job::REFUNED);
            $em->persist($job);
        }
        
        $vlance_parameter = $this->container->getParameter('vlance_system');
        $commission = $vlance_parameter['commission'];
        $from_email = $this->container->getParameter('from_email');
        $to_admin = $this->container->getParameter('to_email');
        
        if($_POST['type_cancel'] === 'refund'){
            $job->setStatus(Job::JOB_FINISHED);
            $job->setCancelStatus(Job::STATUS_CANCELLED);
            $em->persist($job);
            
            $fee_amount = $jobCash->getBalance() * 5 / 100;
            $refund_amount = $jobCash->getBalance() - $fee_amount;

            $refunTrans = new Transaction();
            $refunTrans->setType(Transaction::TRANS_TYPE_REFUND);
            $refunTrans->setAmount($refund_amount);
            $refunTrans->setReceiver($job->getAccount());
            $refunTrans->setJob($job);
            $refunTrans->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $refunTrans->setComment($this->get('translator')->trans('controller.transaction.refun.comment', array("%%job_title%%" => $job->getTitle()), 'vlance'));
            $refunTrans->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($refunTrans);

            $jobCash->setBalance(0);
            $em->persist($jobCash);

            $userCash->setBalance($userCash->getBalance() + $refunTrans->getAmount());
            $em->persist($userCash);

            $fee_trans = new Transaction();
            $fee_trans->setAmount($fee_amount);
            $fee_trans->setSender($job->getAccount());
            $fee_trans->setReceiver($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $fee_trans->setJob($job);
            $fee_trans->setType(Transaction::TRANS_TYPE_FEE);
            $fee_trans->setStatus(Transaction::TRANS_TERMINATED);
            $fee_trans->setComment($this->get('translator')->trans('transaction.fee.default_comment',array(),'vlance'));
            $em->persist($fee_trans);

            $vlanceCash = new \Vlance\PaymentBundle\Entity\VlanceCash();
            $vlanceCash->setBalance($fee_trans->getAmount());
            $vlanceCash->setTransaction($fee_trans);
            $em->persist($vlanceCash);

            $awardedBid = $em->getRepository('VlanceJobBundle:Job')->getAwardedBid($job);
            $messageClient = new Message();
            $messageClient->setBid($awardedBid);
            $messageClient->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $messageClient->setReceiver($job->getAccount());
            $messageClient->setStatus(Message::NEW_MESSAGE);
            $messageClient->setContent($this->get('translator')->trans('controller.workshop.cancel.success.client', array("%%job_title%%" => $job->getTitle()), 'vlance'));
            $em->persist($messageClient);

            $messageFreelancer = new Message();
            $messageFreelancer->setBid($awardedBid);
            $messageFreelancer->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $messageFreelancer->setReceiver($job->getWorker());
            $messageFreelancer->setStatus(Message::NEW_MESSAGE);
            $messageFreelancer->setContent($this->get('translator')->trans('controller.workshop.cancel.success.freelancer', array("%%job_title%%" => $job->getTitle()), 'vlance'));
            $em->persist($messageFreelancer);    
            
            $em->flush();
            
            $template = 'VlanceJobBundle:Email:admin_refund_client.html.twig';
            $context = array(
                'refunTrans'    => $refunTrans,
                'job'           => $job,
            );
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to_admin);
            
            if((isset($_POST['sendEmail']) && $_POST['sendEmail']) && (is_object($job->getWorker()))) {
                $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
                $email_context = array('job' => $job, 'reson' => $_POST['reasonCancel']);

                //Send email to freelancer
                $free = $job->getWorker();
                $template_fl = 'VlanceJobBundle:Email:freelancer_confirm_job_cancel.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_fl, $email_context, $email_hotro, $free->getEmail());
                //Send email to client
                $client = $job->getAccount();
                $template_cl = 'VlanceJobBundle:Email:client_confirm_job_cancel.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_cl, $email_context, $email_hotro, $client->getEmail());
            }
        } elseif(($_POST['type_cancel'] === 'pay_apart') || ($_POST['type_cancel'] === 'pay_apart_bid')) {
            $freelancer = $job->getWorker();
            if($_POST['type_cancel'] === 'pay_apart'){
                $job->setStatus(Job::JOB_FINISHED);
                $job->setCancelStatus(Job::STATUS_CANCELLED);
                $em->persist($job);
            } elseif($_POST['type_cancel'] === 'pay_apart_bid'){
                $now = new \DateTime('now');
                $job->setStatus(Job::JOB_OPEN);
                $job->setCancelStatus(Job::STATUS_CANCELLED_REWORK);
                $job->setCloseAt($now->modify('+7 days'));
                $job->setWorker(NULL);
                $job->setStartedAt(NULL);
                $job->setPaymentStatus(Job::WAITING_ESCROW);
                $em->persist($job);

                /* @var $bid_awarded \Vlance\JobBundle\Entity\Bid */
                $bid_awarded = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('account' => $job->getWorker()));
                $bid_awarded->setAwardedAt(NULL);
                $em->persist($bid_awarded);
                                
                if(!is_null($job->getTestType())){
                    $job->setTestType(NULL);
                    $job->setUpfrontAt(NULL);
                    $em->persist($job);
                }
            }
            
            $money_pay = (int)$_POST['money_pay'];
            
            if($money_pay > (int)$job->getBudget()){
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Hủy dự án thất bại', array(), 'vlance'));
                return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
            }
            
            $money_refund = ((int)$jobCash->getBalance() - $money_pay) * 95 / 100;      //Thu phí 5% quản lý dự án khi KH hủy
            $fee_vl = (((int)$jobCash->getBalance() - $money_pay) * 5 / 100) + ($money_pay * $commission / 100);
            
            $refunTrans = new Transaction();
            $refunTrans->setType(Transaction::TRANS_TYPE_REFUND);
            $refunTrans->setAmount($money_refund);
            $refunTrans->setReceiver($job->getAccount());
            $refunTrans->setJob($job);
            $refunTrans->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $refunTrans->setComment($this->get('translator')->trans('controller.transaction.refun.comment', array("%%job_title%%" => $job->getTitle()), 'vlance'));
            $refunTrans->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($refunTrans);
                
            $paied_trans = new Transaction();
            $paied_trans->setAmount($money_pay * (100 - $commission) / 100);
            $paied_trans->setSender($job->getAccount());
            $paied_trans->setReceiver($freelancer);
            $paied_trans->setJob($job);
            $paied_trans->setType(Transaction::TRANS_TYPE_PAYMENT);
            $paied_trans->setStatus(Transaction::TRANS_TERMINATED);
            $paied_trans->setComment($this->get('translator')->trans('transaction.pay.default_comment',array(),'vlance'));
            $em->persist($paied_trans);
            
            $jobCash->setBalance(0);
            $em->persist($jobCash);

            $userCash->setBalance($userCash->getBalance() + $refunTrans->getAmount());
            $em->persist($userCash);
            
            $flCash = $freelancer->getCash();
            $flCash->setBalance($flCash->getBalance() + $paied_trans->getAmount());
            $em->persist($flCash);
            
            $awardedBid = $em->getRepository('VlanceJobBundle:Job')->getAwardedBid($job);
            $messageClient = new Message();
            $messageClient->setBid($awardedBid);
            $messageClient->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $messageClient->setReceiver($job->getAccount());
            $messageClient->setStatus(Message::NEW_MESSAGE);
            $messageClient->setContent($this->get('translator')->trans('controller.workshop.cancel.success_apart.client', array("%%job_title%%" => $job->getTitle(), "%%money_pay%%" => $money_pay, "%%money_refund%%" => $money_refund), 'vlance'));
            $em->persist($messageClient);

            $messageFreelancer = new Message();
            $messageFreelancer->setBid($awardedBid);
            $messageFreelancer->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $messageFreelancer->setReceiver($freelancer);
            $messageFreelancer->setStatus(Message::NEW_MESSAGE);
            $messageFreelancer->setContent($this->get('translator')->trans('controller.workshop.cancel.success_apart.freelancer', array("%%job_title%%" => $job->getTitle(), "%%money_pay%%" => ($money_pay * (100 - $commission) / 100)), 'vlance'));
            $em->persist($messageFreelancer);
            
            $fee_trans = new Transaction();
            $fee_trans->setAmount($fee_vl);
            $fee_trans->setSender($job->getAccount());
            $fee_trans->setReceiver($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
            $fee_trans->setJob($job);
            $fee_trans->setType(Transaction::TRANS_TYPE_FEE);
            $fee_trans->setStatus(Transaction::TRANS_TERMINATED);
            $fee_trans->setComment($this->get('translator')->trans('transaction.fee.default_comment',array(),'vlance'));
            $em->persist($fee_trans);
            
            $vlanceCash = new \Vlance\PaymentBundle\Entity\VlanceCash();
            $vlanceCash->setBalance($fee_trans->getAmount());
            $vlanceCash->setTransaction($fee_trans);
            $em->persist($vlanceCash);
            
            $em->flush();
            
            $template = 'VlanceJobBundle:Email:admin_refund_apart_client.html.twig';
            $context = array(
                'paiedTrans'    => $paied_trans,
                'refunTrans'    => $refunTrans,
                'job'           => $job,
            );
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_email, $to_admin);
            
            if((isset($_POST['sendEmail']) && $_POST['sendEmail']) && (is_object($freelancer))) {
                $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
                $email_context = array(
                    'money_pay'     => $money_pay,
                    'paiedTrans'    => $paied_trans,
                    'refunTrans'    => $refunTrans,
                    'job'           => $job, 
                    'reson'         => $_POST['reasonCancel'],
                    'freelancer'    => $freelancer
                );

                //Send email to freelancer
                $template_fl = 'VlanceJobBundle:Email:freelancer_confirm_job_cancel_apart.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_fl, $email_context, $email_hotro, $freelancer->getEmail());
                //Send email to client
                $client = $job->getAccount();
                $template_cl = 'VlanceJobBundle:Email:client_confirm_job_cancel_apart.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_cl, $email_context, $email_hotro, $client->getEmail());
            }
        }
        
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('Hủy dự án thành công', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
    }
}