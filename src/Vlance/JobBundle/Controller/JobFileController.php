<?php

namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\JobBundle\Entity\JobFile;
use Vlance\JobBundle\Form\JobFileType;

/**
 * JobFile controller.
 *
 * @Route("/jobfile")
 */
class JobFileController extends Controller
{

    /**
     * Creates a new JobFile entity.
     *
     * @Route("/new", name="jobfile_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request)
    {
        $entity  = new JobFile();
        $form = $this->createForm(new JobFileType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('jobfile_create'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new JobFile entity.
     *
     * @Route("/new", name="jobfile_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new JobFile();
        $form   = $this->createForm(new JobFileType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a JobFile entity.
     *
     * @Route("/{id}", name="jobfile_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VlanceJobBundle:JobFile')->find($id);

        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing JobFile entity.
     *
     * @Route("/{id}/edit", name="jobfile_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VlanceJobBundle:JobFile')->find($id);

        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }

        $editForm = $this->createForm(new MessageFileType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing JobFile entity.
     *
     * @Route("/{id}", name="jobfile_update")
     * @Method("PUT")
     * @Template("VlanceJobBundle:JobFile:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('VlanceJobBundle:JobFile')->find($id);

        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new JobFileType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('jobfile_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a JobFile entity.
     *
     * @Route("/{id}/delete", name="jobfile_delete")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function deleteAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('VlanceJobBundle:JobFile')->find($id);
            
            if (!$entity) {
                return $this->redirect($this->generateUrl("404_page"), 301);
            }
            
            $job = $entity->getJob();
            $acc = $this->get('security.context')->getToken()->getUser();
            if ($job->getAccount() == $acc) {
                $entity->removeUpload();    
                $em->remove($entity);
                $em->flush();
            }
        
        return $this->redirect($this->generateUrl('job_edit',array('id' => $job->getId())));
    }

    /**
     * Creates a form to delete a JobFile entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * Lists all JobFile entities.
     *
     * @Route("/{filters}", name="jobfile")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request,$filters)
    {
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $this->get('session')->set('filter',$params);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceJobBundle:MessageFile')->findBy($params);

        return array(
            'entities' => $entities,
        );
    }

}
