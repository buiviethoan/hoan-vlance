<?php

namespace Vlance\JobBundle\Controller\AdminBid;

use Admingenerated\VlanceJobBundle\BaseAdminBidController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vlance\JobBundle\Form\Type\AdminBid\EditType;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Bid;


class EditController extends BaseEditController
{
    public function updateAction($pk)
    {
        $Bid = $this->getObject($pk);

        
        if (!$Bid) {
            throw new NotFoundHttpException("The Vlance\JobBundle\Entity\Bid with id $pk can't be found");
        }
        $bid_old = $Bid->getPublish();
        $isNotified = $Bid->getIsNotified();
        $this->preBindRequest($Bid);
        $form = $this->createForm($this->getEditType(), $Bid);
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            try {
                $this->preSave($form, $Bid);
                $this->saveObject($Bid);
                // gui mail cho khach hang neu chao gia khong vi pham loi
                $notified = $Bid->getIsNotified();
                $isViolated = $Bid->getIsViolated();
                $job = $Bid->getJob();
                $to = $job->getAccount()->getEmail();
                $bcc = 'vlanceantispam@gmail.com';
                if($isNotified == false && $notified == true && $isViolated == false) {
                    $message = \Swift_Message::newInstance()
                    ->setSubject('Có người chào giá mới cho dự án '.$job->getTitle().' của bạn trên vLance.vn!')
                    ->setFrom('noreply@vlance.vn')
                    ->setTo($to)
                    ->setBcc($bcc)
                    ->setBody(
                            $this->renderView(
                                'VlanceAccountBundle:Registration:email_sys.html.twig',
                                array('name' => $job->getAccount()->getFullName(), 'hash' => $job->getHash() ,'type' => 'bided')
                            ),'text/html'
                        )
                    ;
                    $this->get('mailer')->send($message);
                }
                $this->postSave($form, $Bid);
                //Update lai so cong viec da chao gia cua freelancer
                /* @var $account Account  */
                if($bid_old != $Bid->getPublish()) {
                    $account = $Bid->getAccount();
                    $em = $this->getDoctrine()->getManager();
                    $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $account, 'publish' => Bid::PUBLISH));
                    $account->setNumBid(count($bids));
                    $em->persist($account);
                    $em->flush();
                }
                
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminBid_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminBid_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminBid_edit", array('pk' => $pk) ));

                        } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $Bid);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('VlanceJobBundle:AdminBidEdit:index.html.twig', $this->getAdditionalRenderParameters($Bid) + array(
            "Bid" => $Bid,
            "form" => $form->createView(),
        ));
    }
}
