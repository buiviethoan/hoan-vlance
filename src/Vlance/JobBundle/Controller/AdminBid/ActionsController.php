<?php

namespace Vlance\JobBundle\Controller\AdminBid;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Vlance\JobBundle\Entity\Bid;
use Admingenerated\VlanceJobBundle\BaseAdminBidController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController {

    public function attemptObjectDeactive($id) {
        try {
            /* @var $bid \Vlance\JobBundle\Entity\Bid */
            $bid = $this->getObject($id);
            
            if ('POST' == $this->get('request')->getMethod()) {
                // Check CSRF token for action
                $intention = $this->getRequest()->getRequestUri();
                $this->isCsrfTokenValid($intention);
                //Deactive job
                $bid->setPublish(Bid::UNPUBLISH);
                $em = $this->getDoctrine()->getManager();
                $em->persist($bid);
                $em->flush();
                
                /**
                 * Ban use if admin set this option
                 */
                $isBanned = $this->get('request')->request->get('banned');
                $reason = $this->get('request')->request->get('reason');
                if($reason != ""){
                    $bid->setReason($reason);
                    $em->persist($bid);
                    $em->flush();
                }
                
                $template = "VlanceJobBundle:Email:deactive_bid_banned.html.twig";
                if ($isBanned) {
                    $account = $bid->getAccount();
                    $bannedTo = new \DateTime();
                    $bannedTo->add(date_interval_create_from_date_string('7 days'));
                    $account->setBannedTo($bannedTo);
                    $em->persist($account);
                    $em->flush();
                }
                
                $bids_hired = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('account' => $bid->getAccount(), 'publish' => Bid::UNPUBLISH));
                $now = new \DateTime('now');
                $count_bid_hired = 0;
                foreach($bids_hired as $b){
                    if(!is_null($b->getReason())){
                        $time = strtotime(date_format($b->getCreatedAt(), 'Y/m/d H:i:s'));
                        $time_update = strtotime(date_format($now, '2017/01/19 00:00:00'));
                        if(((int)$time - (int)$time_update) >= 0){ //Lấy bid bắt đầu từ ngày 19/01/2017 00:00:00
                            $count_bid_hired++;
                        }
                    }
                }
                
                if($count_bid_hired > 1 && $count_bid_hired < 4){
                    $account = $bid->getAccount();
                    $bannedTo = new \DateTime();
                    $bannedTo->add(date_interval_create_from_date_string('2 days'));
                    $account->setBannedTo($bannedTo);
                    $em->persist($account);
                    $em->flush();
                } elseif($count_bid_hired > 3 && $count_bid_hired < 6){
                    $account = $bid->getAccount();
                    $bannedTo = new \DateTime();
                    $bannedTo->add(date_interval_create_from_date_string('7 days'));
                    $account->setBannedTo($bannedTo);
                    $em->persist($account);
                    $em->flush();
                } elseif($count_bid_hired > 5) {
                    $account = $bid->getAccount();
                    $bannedTo = new \DateTime();
                    $bannedTo->add(date_interval_create_from_date_string('30 days'));
                    $account->setBannedTo($bannedTo);
                    $em->persist($account);
                    $em->flush();
                }
                
                //send email to user - now we don't send to facebook account - because of email problem
//                if (!$bid->getAccount()->getFacebookId()) {
                    $context = array(
                        'bid' => $bid,
                    );
                    $from = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
                    $to = $bid->getAccount()->getEmail();
//                    die('before send mail');
                    $result = $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
//                    $result = $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, "hiep.hoang@vlance.vn");//test
//                }
                $this->get('session')->getFlashBag()->add(
                        'success', $this->get('translator')->trans(
                                "admin.bid.deactive.success", 
                                array('%name%' => $bid->getAccount()->getFullName(),'%title%' => $bid->getJob()->getTitle()),
                                'vlance'
                        )
                );

                return new RedirectResponse($this->generateUrl("job_show_freelance_job", array('hash' => $bid->getJob()->getHash())));
            }
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                    'error', $this->get('translator')->trans(
                            "admin.bid.deactive.error", array('%id%' => $id), 'vlance'
                    )
            );

            return new RedirectResponse($this->generateUrl("Vlance_JobBundle_AdminBid_list"));
        }
    }

}
