<?php

namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\JobBundle\Entity\JobInvite;
use Vlance\BaseBundle\Utils\HasPermission;
use Vlance\JobBundle\Form\JobInviteType;

use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\AccountRepository;
use Doctrine\ORM\Query\ResultSetMapping;
/**
 * JobInvite controller.
 *
 * @Route("/ji")
 */
class JobInviteController extends Controller
{
    /**
     * Creates a new JobInvite entity.
     *
     * @Route("/new/{jobId}/{accountId}", name="jobinvite_create")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function createAction(Request $request, $jobId, $accountId)
    {
        /** Tracking event "Invite to bid" **/
        // Mixpanel
        $this->get('vlance_base.helper')->trackAll(NULL, "Job invite initiated");
        
        $em = $this->getDoctrine()->getManager();
        $hashString = $this->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->getJobByIdHash($jobId, "@$hashString");
        // If job is not exist
        if (!$job) {
            $data = array(
                'error' => JobInvite::ERROR_JOB_NOT_EXIST,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_JOB_NOT_EXIST, array('%%id%%' => $job->getId()), 'vlance')
            );
            return new JsonResponse($data);
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $em->getRepository('VlanceAccountBundle:Account')->getAccountByIdHash($accountId, "@$hashString");
        if (!$account) {
            $data = array(
                'error' => JobInvite::ERROR_ACCOUNT_NOT_EXIST,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_ACCOUNT_NOT_EXIST, array('%%id%%' => $account->getId()), 'vlance')
            );
            return new JsonResponse($data);
        }
        $jobInvite = $em->getRepository('VlanceJobBundle:JobInvite')->findOneBy(array('job' => $job, 'account' => $account));
        //This user already invited for this job
        if ($jobInvite) {
            $data = array(
                'error' => JobInvite::ERROR_INVITE_EXIST,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_INVITE_EXIST, array('%%fullName%%' => $account->getFullName()), 'vlance')
            );
            return new JsonResponse($data);
        }
        
        // The inviter must be job owner or Admin
        /* @var $loggedAccount \Vlance\AccountBundle\Entity\Account */
        $loggedAccount = $this->get('security.context')->getToken()->getUser();
        if ($job->getAccount() != $loggedAccount && HasPermission::hasAdminPermission($loggedAccount) == FALSE) {
            $data = array(
                'error' => JobInvite::ERROR_NOT_OWNER,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_NOT_OWNER, array(), 'vlance')
            );
            return new JsonResponse($data);
        }
        
        //If this job is exprised
        if ($job->getCloseAt()->getTimestamp() < time()) {
            $data = array(
                'error' => JobInvite::ERROR_JOB_EXPRISED,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_JOB_EXPRISED, array(), 'vlance')
            );
            return new JsonResponse($data);
        }
        
        $jobInvites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('job' => $job));
        $maxInvites = $this->get('vlance_base.helper')->getParameter("vlance_system.max_invite");
        if (count($jobInvites) >= $maxInvites) {
            $data = array(
                'error' => JobInvite::ERROR_MAX_INVITES,
                'errorMsg' => $this->get('translator')->trans(JobInvite::ERROR_TEXT_MAX_INVITES, array('%%number%%' => $maxInvites), 'vlance')
            );
            return new JsonResponse($data);
        }
        
        $jobInvite = new JobInvite();
        $jobInvite->setAccount($account);
        $jobInvite->setJob($job);
        
        $em->persist($jobInvite);
        $em->flush();
        
        /* Send inviation email to freelancer */
        $template       = 'VlanceJobBundle:Email/job_invite:send_to_freelancer.html.twig';
        $template_v2    = 'VlanceJobBundle:Email/job_invite:send_to_freelancer_v2.html.twig';
        $case = (time() % 4 == 1) ? 1 : 2; // 25% for the 1st template, 75% for the 2nd
        $parameters = $this->container->getParameter('vlance_system');
        $context = array(
            'job' => $job,
            'freelancer' => $account,
            'pixel_tracking' => base64_encode(json_encode(
                array(
                    'event' => "Job invite open",
                    'properties' => array(
                        'token'             => $parameters['mixpanel_token']['prod'],
                        'distinct_id'       => $account->getId(), // Freelancer id
                        'case'              => $case,
                        'job_category'      => $job->getCategory()->getTitle(),
                        'job_budget'        => $job->getBudget(),
                        'relevant_category' => ($job->getCategory()->getId() == $account->getCategory()->getId()) ? "TRUE" : "FALSE"
                    )
                )
            ))
        );
        $from = $this->container->getParameter('from_email');
        if($case == 2){
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_v2, $context, $from, $account->getEmail());
        } else{
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $account->getEmail());
        }
        
        /** Tracking event "Invite to bid sent" **/
        // Track for Client
        $this->get('vlance_base.helper')->trackAll(NULL, "Job invite sent", array(
            'case'              => $case,
            'job_category'      => $job->getCategory()->getTitle(),
            'job_budget'        => $job->getBudget(),
            'relevant_category' => ($job->getCategory()->getId() == $account->getCategory()->getId()) ? "TRUE" : "FALSE"
        ));
        // Track for Freelancer
        $this->get('vlance_base.helper')->trackAll($account, "Job invite received", array(
            'case'              => $case,
            'job_category'      => $job->getCategory()->getTitle(),
            'job_budget'        => $job->getBudget(),
            'relevant_category' => ($job->getCategory()->getId() == $account->getCategory()->getId()) ? "TRUE" : "FALSE"
        ));
        
        $data = array(
            'error' => JobInvite::NO_ERROR,
            'errorMsg' => $this->get('translator')->trans(JobInvite::NO_ERROR_TEXT, array('%%fullName%%' => $account->getFullName()), 'vlance')
        );
        return new JsonResponse($data);
    }
    
    /**
     * Creates a new JobInvite entity.
     *
     * @Route("/new_invite/{jobId}/{accountId}", name="jobinvite_createinvite")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function createInviteAction(Request $request, $jobId, $accountId)
    {
        $res = json_decode($this->createAction($request, $jobId, $accountId)->getContent());
//        print_r($res);die;
        if($res->error == 0){
            $this->get('session')->getFlashBag()->add('success', $res->errorMsg);
        } else{
            $this->get('session')->getFlashBag()->add('error', $res->errorMsg);
        }
        $hashString = $this->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('VlanceJobBundle:Job')->getJobByIdHash($jobId, "@$hashString");
        return $this->redirect($this->generateUrl('job_show', array('id' => $job->getId())));        
    }

    /**
     * Display list invited freelancer in job view
     *
     * @Route("/list/{jobId}", name="jobinvite_list")
     * @Method("GET")
     * @Template("VlanceJobBundle:JobInvite:list.html.php",engine="php")
     */
    public function listAction($jobId)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->find($jobId);
        if (!$job) {
            return new Response("");
        }
        /* @var $loggedAccount \Vlance\AccountBundle\Entity\Account */
        $loggedAccount = $this->get('security.context')->getToken()->getUser();
        if ($job->getAccount() != $loggedAccount && HasPermission::hasAdminPermission($loggedAccount) == FALSE) {
            return new Response("");
        }
        $jobInvites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('job' => $job));
        
        $maxInvites = $this->get('vlance_base.helper')->getParameter("vlance_system.max_invite");
        if(count($jobInvites) < $maxInvites){
            /* suggseted freelancers */
            $suggested_freelancers = $em->getRepository('VlanceAccountBundle:Account')->getSuggestedFreelancersForJob($job, 20);
        } else{
            $suggested_freelancers = array();
        }
        
        return array('jobInvites' => $jobInvites, 'job' => $job, 'suggested_freelancers' => $suggested_freelancers);  
    }
    
    
    /**
     * Display popup list job invited in acc
     *
     * @Route("/popup_list_jobinvite_acc", name="popup_list_jobinvite_acc")
     * @Method("GET")
     * @Template("VlanceJobBundle:JobInvite:popup_list_jobinvite_acc.html.php",engine="php")
     */
    public function popuplistinviteAction()
    {
         //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
             return new Response("");
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $jobInvites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('account' => $account, 'status' => JobInvite::INVITE_NEW),array('createdAt' => 'DESC'));
        
        if (!$jobInvites || count($jobInvites) == 0) {
            return new Response("");
        }
        foreach ($jobInvites as $jobInvite){
            $jobInvite->setStatus(JobInvite::INVITE_NOTIFIED);
            $em->persist($jobInvite);
        }
        $em->flush();
        
        return array(
            'jobInvites' => $jobInvites
        );
    }
    
    /**
     * Display list job invited in acc view
     *
     * @Route("/list_jobinvite_acc", name="list_jobinvite_acc")
     * @Method("GET")
     * @Template("VlanceJobBundle:JobInvite:list_jobinvite_acc.html.php",engine="php")
     */
    public function listinviteAction()
    {
         //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
             return new Response("");
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $jobInvites = $em->getRepository('VlanceJobBundle:JobInvite')->findBy(array('account' => $account, 'status' => JobInvite::INVITE_NOTIFIED),array('createdAt' => 'DESC'));
        
        if (!$jobInvites || count($jobInvites) == 0) {
            return new Response("<div class='no-message'>Bạn không có lời mời chào giá nào</div>");
        }
        
        return array(
            'jobInvites' => $jobInvites
        );
    }
    
    /**
     * Display list job invited in acc view
     *
     * @Route("/refuse_jobinvite_acc/{jobId}/{accountId}", name="refuse_jobinvite_acc")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function refuseinviteAction(Request $request, $jobId , $accountId)
    {
        $em = $this->getDoctrine()->getManager();
        $hashString = $this->get('vlance_base.helper')->getParameter("vlance_system.hash_string");
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->getJobByIdHash($jobId, "@$hashString");
        // If job is not exist
        if (!$job) {
            return new Response("");
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $em->getRepository('VlanceAccountBundle:Account')->getAccountByIdHash($accountId, "@$hashString");
        if (!$account) {
            return new Response("");
        }
        
        $jobInvite = $em->getRepository('VlanceJobBundle:JobInvite')->findOneBy(array('job' => $job->getId(), 'account' => $account->getId(), 'status' => JobInvite::INVITE_NOTIFIED));
        //This user already invited for this job
//        print_r(count($jobInvite));die;
        if ($jobInvite) {
            $jobInvite->setStatus(JobInvite::INVITE_REFUSED);
            $em->persist($jobInvite);
            $em->flush();
        }
//        print_r($request->headers);die;
        $referer = $request->headers->get('referer');
        if($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
        return $this->redirect($referer);
    }
    
}
