<?php

namespace Vlance\JobBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Vlance\JobBundle\Entity\Job;
use Vlance\JobBundle\Entity\JobFeedback;
use Vlance\JobBundle\Form\JobFeedbackType;
use Vlance\JobBundle\Form\JobBudgetType;

use Symfony\Component\HttpFoundation\Response;

/**
 * Job feedback controller.
 *
 * @Route("/jfb")
 */
class JobFeedbackController extends Controller
{
    
    /**
     * Creates a new feed back for job
     *
     * @Route("/create/{id}", name="jfb_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        /*@var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        // If current user is banned
        if ($acc->isBanned()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.banned.bid', array('%%bannedTo%%' => $acc->getBannedTo()->format('H:s:i d/m/Y')), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
        }
        
        if ($job->getAccount() == $acc) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.job.feedback.owner', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        $feedback = $em->getRepository('VlanceJobBundle:JobFeedback')->findOneBy(array('sender' => $acc, 'job' => $job));
        if ($feedback && $feedback->getId()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.job.feedback.exist', array(), 'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        }
        
        /* @var $feedback JobFeedback */
        $feedback  = new JobFeedback();
        $feedback->setSender($acc);
        $feedback->setJob($job);
        $form = $this->createForm(new JobFeedbackType(), null);
        $form->bind($request);
        
        if ($form->isValid()) {
            
            //Freelancer request higher budget
            if ($form->get('saveWithBudget')->isClicked()) {
                $feedback->setBudget($form->get('budget')->getData());
                $feedback->setType(JobFeedback::TYPE_BUDGET);
            } else {
                $feedback->setType(JobFeedback::TYPE_DESCRIPTION);
            }
            
            $feedback->addAdditionalData(JobFeedback::FIELD_NOTIFICATION, '0');
            
            $em->persist($feedback);
            $em->flush();
            
            // Send email to client to notify -> move to command line to send by cron job
            /* @var $helper \Vlance\BaseBundle\Helper\Helper */
//            $helper = $this->get('vlance_base.helper');
//            $from_email = $helper->getParameter('vlance_email.from.hotro');
//            
//            //Send email client
//            $to_client = $job->getAccount()->getEmail();
//            $template_client = "VlanceJobBundle:Email:feedback_{$feedback->getTypeText()}.html.twig";
//            $context_client = array(
//                'feedback' => $feedback,
//            );
//            $this->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from_email, $to_client);
            
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.job.feedback.success',array(),'vlance'));
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
        }
        // From no validation
        $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.job.feedback.error',array(),'vlance'));
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), true));
    }

    /**
     * Displays a form to create a new Bid entity.
     *
     * @Route("/new/{id}", name="jfb_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction($id)
    {
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($id);
        
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        // If current user is banned
        if ($acc->isBanned()) {
            return new Response("");
        }
        
        // Feedback đã được gửi đi
        if ($acc == $job->getAccount()) {
            return new Response();
        }
        
        $feedback = $em->getRepository('VlanceJobBundle:JobFeedback')->findOneBy(array('sender' => $acc, 'job' => $job));
        if ($feedback && $feedback->getId()) {
            return new Response($this->renderView('VlanceJobBundle:JobFeedback:submitted.html.php', array('feedback' => $feedback)));
        }
        
        //Job da duoc giao cho freelancer
        if($job->getStatus() != Job::JOB_OPEN) {
            return new Response("");
        }
        
        $feedback = new JobFeedback();
        $form   = $this->createForm(new JobFeedbackType(), null, array(
            'action' => $this->generateUrl('jfb_create', array('id' => $id)),
        ));
        
        return array(
            'form' => $form->createView(),
        );
        
    }
    
    /**
     * Displays a form to create a new Bid entity.
     *
     * @Route("/show/{job_id}", name="jfb_show")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function showAction($job_id) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        /* @var $job Job*/
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        // if current user is not job owner -> display empty
        if ($job->getAccount() != $acc) {
            return new Response("");
        }
        
        $feedbacks = $em->getRepository('VlanceJobBundle:JobFeedback')->statistic($job_id);
        $form = $this->createForm(new JobBudgetType(), null, array(
                    'action' => $this->generateUrl('job_update_budget', array('job_id' => $job_id)),
                    ));
        return array(
            'form' => $form->createView(),
            'feedback' => count($feedbacks)>0?$feedbacks[$job_id]:null,
            'job' => $job
        );
    }
}
