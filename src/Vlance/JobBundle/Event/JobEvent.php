<?php

namespace Vlance\JobBundle\Event;

use Vlance\JobBundle\Entity\Job;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class JobEvent extends Event
{
    const JOB_FIRST_MESSAGE     = 'vlance.job.first_message';
    const JOB_CREATE_COMPLETED  = 'vlance.job.created';
    const JOB_AWARDED_SUCCESS   = 'vlance.job.awarded';
    
    private $request;
    private $job;

    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    /**
     * @return \Vlance\JobBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }
    
    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
