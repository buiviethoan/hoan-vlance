<?php

namespace Vlance\JobBundle\Event;

use Vlance\JobBundle\Entity\Bid;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class BidEvent extends Event
{
    private $request;
    private $bid;

    public function __construct(Bid $bid, Request $request)
    {
        $this->bid = $bid;
        $this->request = $request;
    }

    /**
     * @return \Vlance\JobBundle\Entity\Bid
     */
    public function getBid()
    {
        return $this->bid;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
