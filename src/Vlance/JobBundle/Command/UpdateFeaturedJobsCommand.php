<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFeaturedJobsCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
                ->setName('vlance:job:featuredjob')
                ->setDescription('Vlance command for update featured jobs');
    }
    
    /* Remove job's featuredUntil if the date is passed */
    protected function execute(InputInterface $input, OutputInterface $output) {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $currentDay = date('Y-m-d h:i:s', time());
        $jobConditions['featuredUntil'] = array('lt', "'$currentDay'");
        $jobs = $em->getRepository('VlanceJobBundle:Job')->getFields("j", $jobConditions);
        
        foreach($jobs as $j){
            $j->setFeaturedUntil(null);
            $j->setFeaturedFb(false);
            $em->persist($j);
            $em->flush();
        }
    }
}
