<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class NewsletterCommand extends ContainerAwareCommand {
    
    const DEFAULT_LIMIT = 5;
    const DEFAULT_DAYS = 1;
    protected $output = NULL;
    
    public function configure() {
        $this->setName('vlance:job:newsletter')
//                ->addOption('test', NULL, InputOption::VALUE_NONE, "Run test for this command")
                // Exemple command (windows), xx is category id:
                // php app\console vlance:job:newsletter --cat=xx
                ->addOption('cat', NULL, InputOption::VALUE_NONE, "Send jobs in selected category to freelancer.")
                // Exemple command (windows): 
                // php app\console vlance:job:newsletter --atleast
                ->addOption('select', NULL, InputOption::VALUE_NONE, "Mode to select jobs: exact / atleast / all")
                // Exemple command (windows), account ids are separated by comma (,): 
                // php app\console vlance:job:newsletter --exlude=abc@mail.com,def@mail.com
                ->addOption('exclude', NULL, InputOption::VALUE_NONE, "Excluded accounts from sending mail.")
                ->addOption('limit', NULL, InputOption::VALUE_NONE, "Number of jobs.")
                ->addOption('days', NULL, InputOption::VALUE_NONE, "Number of days.")
                ->addOption('newweek', NULL, InputOption::VALUE_NONE, "New jobs new week.")
                ->addOption('facebook', NULL, InputOption::VALUE_NONE, "Include facebook users.")
                // This --debug option can be used with any options above. No mail will be sent.
                // Exemple command (windows): 
                // php app\console vlance:job:newsletter --debug
                // php app\console vlance:job:newsletter --mode=test
                ->addOption('mode', NULL, InputOption::VALUE_NONE, "Mode to send mail: debug / test / production")
                ->addOption('modulo', NULL, InputOption::VALUE_NONE, "Mode to send mail: zero / one")
                ->addOption('nosendemail', NULL, InputOption::VALUE_NONE, "Don't send email")
                ->setDescription('Send newsletter to freelancer with new job');
    }
    
    /**
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * 
     */
    public function execute(InputInterface $input, OutputInterface $output) {
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger = $this->getContainer()->get("monolog.logger.job_news");
        $logger->info("Checkin #0: " . microtime(true));
        
        $this->output = $output;
        $cat= NULL;
        $include_facebook = false;
        $days = self::DEFAULT_DAYS;
        $limit = self::DEFAULT_LIMIT;
        $excluded_accounts = array();
        $template = "VlanceJobBundle:Email/newsletter:new_job.html.twig";
        $test_accounts = array(
            array('id' => 1, 'email' => "tuan.tran@vlance.vn", 'fullName' => "Tuân Trần Ngọc", 'hash' => "tuan-tran-ngoc"),
        );
        $mode = 'production';
        
        // Set number of days
        // --days=xx
        if($input->getOption('days')){
            $days = (int)($input->getOption('days'));
        }

        // Set limit to number of jobs
        // --limit=xx
        if($input->getOption('limit')){
            $limit = (int)($input->getOption('limit'));
        }
        
        // Category is specified
        // --cat=xx
        if($input->getOption('cat')){
            $em = $this->getContainer()->get('doctrine')->getManager();
            if($em->getRepository('VlanceAccountBundle:Category')->find($input->getOption('cat'))){
                $cat = $em->getRepository('VlanceAccountBundle:Category')->find($input->getOption('cat'));
            }
        }
        
        // Exclude accounts are set
        // --exclude=xx,xx,xx
        if($input->getOption('exclude')){
            $excluded_accounts = explode(',', $input->getOption('exclude')); // Explode then convert all values to integer
        }
        
        // Include all email which are from "@facebook.com"
        // --facebook
        if($input->getOption('facebook')){
            $include_facebook = true;
        }
        
        if($input->getOption('newweek')){
            $template = "VlanceJobBundle:Email/newsletter:new_job_new_week.html.twig";
        }
        
        if ($input->getOption('mode')) { 
            switch($input->getOption('mode')){
                case 'debug':
                    $mode = 'debug';
                    break;
                case 'test':
                    $mode = 'test';
                    break;
            }
            $accounts = $test_accounts;
        }
        
        $modulo = "all";
        if ($input->getOption('modulo')) { 
            switch($input->getOption('modulo')){
                case 'zero':
                    $modulo = 'zero';
                    break;
                case 'one':
                    $modulo = 'one';
                    break;
                case 'two':
                    $modulo = 'two';
                    break;
                case 'three':
                    $modulo = 'three';
                    break;
            }
        }
        
        $sendemail = true;
        if ($input->getOption('nosendemail')) { 
            $sendemail = false;
        }
        if($input->getOption('select') == 'exact' && $input->getOption('days')){
            $jobs = $this->currentDayJob($cat, 1000, $days);  // Get all jobs (actually max 1000) within specific nb of days
        } else{
            $jobs = $this->currentDayJob($cat, $limit, $days);        
        }
        if (!count($jobs)) {
           $logger->info("No new job yesterday");
           return;
        }
        $categories = array();
        foreach($jobs as $j){
            $categories[] = $j->getCategory()->getId();
        }
        $categories = array_unique($categories);
        
        // Send jobs to freelancers who are in exact categories
        // --exact        
        $logger->info("----------------------------------------");
        $logger->info("CASE: " . $input->getOption('select'));
        
        $success_counter = 0;
        
        $logger->info("Checkin #1: " . microtime(true));
        if($input->getOption('select')){
            switch($input->getOption('select')){
                case 'exact':
                    $stat_nb_jobs = 0;
                    $stat_nb_receiver = 0;
                    foreach($categories as $c){
                        $accounts = $this->getAccounts($c, $excluded_accounts, $include_facebook, $modulo);
                        $exact_jobs = $this->currentDayJob($c, $limit, $days);
                        if(count($exact_jobs)>0){
                            $success_counter += $this->sendMail($exact_jobs, $accounts, $template, $mode, $sendemail);
                        }
                        $stat_nb_jobs+= count($exact_jobs);
                        $stat_nb_receiver+= count($accounts);
                    }
                    
                    // Print out stats
                    $logger->info("-----------");
                    $logger->info("TOTAL");
                    $logger->info("New jobs: " . $stat_nb_jobs);
                    $logger->info("Number of accounts: " . $stat_nb_receiver);
                    break;
                    
                case 'atleast': // Send all jobs, which having at least 1 job fit FL's category
                    $catIds = array();
                    foreach($jobs as $job){
                        $catIds[] = $job->getCategory()->getId();
                    }
                    $catIds = array_unique($catIds);
                    $accounts = $this->getAccounts($catIds, $excluded_accounts, $include_facebook, $modulo);
                    $success_counter = $this->sendMail($jobs, $accounts, $template, $mode, $sendemail);
                    break;
                
                case 'noncat': // Account without category in profile
                    $noncat_accounts = $this->getAccounts(-1, $excluded_accounts, $include_facebook, $modulo);
                    $logger->info("Nb of noncat accounts: " . count($noncat_accounts));
                    $success_counter = $this->sendMail($jobs, $noncat_accounts, $template, $mode, $sendemail);
                    break;
                
                case 'notfit': // Account have set category, but there no work for them.
                    // All accounts
                    $all_accounts = $this->getAccounts(NULL, $excluded_accounts, $include_facebook, $modulo);
                    
                    // Get accounts which fit at least 1 new job.
                    $catIds = array();
                    foreach($jobs as $job){
                        $catIds[] = $job->getCategory()->getId();
                    }
                    $catIds = array_unique($catIds);
                    $fit_accounts = $this->getAccounts($catIds, $excluded_accounts, $include_facebook, $modulo);
                    
                    // Non category accounts
                    $noncat_accounts = $this->getAccounts(-1, $excluded_accounts, $include_facebook, $modulo);
                    
                    // Not fit account (but category is set)
                    $notfit_accounts = array_udiff($all_accounts, $fit_accounts, array($this,'compare_account_list'));
                    $notfit_accounts = array_udiff($notfit_accounts, $noncat_accounts, array($this,'compare_account_list'));
                    $logger->info("Nb of notfit accounts: " . count($notfit_accounts));
                    $success_counter = $this->sendMail($jobs, $notfit_accounts, $template, $mode, $sendemail);
                    break;
                
                case 'all':
                    $accounts = $this->getAccounts(NULL, $excluded_accounts, $include_facebook, $modulo);
                    $success_counter = $this->sendMail($jobs, $accounts, $template, $mode, $sendemail);
                    break;
                
                default:
                    break;
            }
        }
        $logger->info("NUMBER OF SUCCESS SENT EMAIL: " . $success_counter);
    }
    
    // Get all job created 
    protected function currentDayJob($category = null, $limit = self::DEFAULT_LIMIT, $days = self::DEFAULT_DAYS) {
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $yesterday = date('Y-m-d h:i:s', time() - 24 * 60 * 60 * $days);
        $currentDay = date('Y-m-d h:i:s', time());
        if ($category) {
            $jobConditions = array('category' => array('eq', "'$category'"));
        }
        $jobConditions['publish'] = array('eq', '1');
        $jobConditions['status'] = array('eq', '1');
        $jobConditions['createdAt'] = array('gte', "'$yesterday'");
        $jobConditions['closeAt'] = array('gte', "'$currentDay'");
        $jobConditions['isPrivate'] = array('eq', '0'); 
        $orders = array('createdAt' => 'DESC');
        return $em->getRepository('VlanceJobBundle:Job')->getFields("j", $jobConditions, $orders, $limit);
    }
    
    protected function getAccounts($category = null, $excluded_accounts = null, $include_facebook = false, $modulo = 'all') {
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger = $this->getContainer()->get("monolog.logger.job_news");
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $accountConditions = array();
        if ($category) {
            if(is_array($category)){
                $accountConditions['category'] = array('in', $category);
            } elseif ($category === -1) {
                $accountConditions['category'] = array('isNull');
            } else {
                $accountConditions['category'] = array('eq', $category);
            }
        }
        if(count($excluded_accounts)>0){
            $accountConditions['email'] = array('notIn', $excluded_accounts);
        }

        $accountConditions['notificationJob'] = array('eq', '1');
        $accountConditions['type'] = array('eq', "'freelancer'");
        $accountConditions['locked'] = array('eq', '0');
        $accountConditions['enabled'] = array('eq', '1');
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.id', 'a.email', 'a.fullName', 'a.hash'), $accountConditions);
            
        if(!$include_facebook){
            $logger->info("Before FB filter: " . count($accounts));
            foreach($accounts as $key => $a){
                if(strpos($a['email'], '@facebook.com')){
                    unset($accounts[$key]);
                }
            }
            $logger->info("After FB filter: " . count($accounts));
        }
        
        if($modulo != 'all'){
            $logger->info("Before modulo " .$modulo. ": " . count($accounts));
            foreach($accounts as $k => $acc){
                if($modulo == 'zero' && (int)($acc['id']) % 4 != 0){        // Keep account having id % == 0
                    unset($accounts[$k]);
                } elseif($modulo == 'one' && (int)($acc['id']) % 4 != 1){   // Keep account having id % == 1
                    unset($accounts[$k]);
                } elseif($modulo == 'two' && (int)($acc['id']) % 4 != 2){   // Keep account having id % == 2
                    unset($accounts[$k]);
                } elseif($modulo == 'three' && (int)($acc['id']) % 4 != 3){   // Keep account having id % == 3
                    unset($accounts[$k]);
                }
            }
            $logger->info("After modulo " .$modulo. ": " . count($accounts));
        }
        
        return $accounts;
    }
    
    protected function sendMail($jobs, $accounts, $template, $mode, $sendemail){    
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger = $this->getContainer()->get("monolog.logger.job_news");
        $logger->info("Checkin #2: " . microtime(true));
        $success_counter = 0;
        
        if($mode == 'test'){
            $test_accounts = array(
                array('id' => 1, 'email' => "tuan.tran@vlance.vn", 'fullName' => "Tuân Trần Ngọc", 'hash' => "tuan-tran-ngoc"),
            );
            $accounts = $test_accounts;
        }
        $logger->info("Sending time: " . date('Y-m-d h:i:s', time()));
        $logger->info("Number of accounts: " . count($accounts));
        $logger->info("New jobs: " . count($jobs));
        $job_list_id = "";
        foreach($jobs as $job){
            $job_list_id .= "#" . $job->getId() . " " . $job->getHash() . ", ";
        }
        $logger->info("Jobs: " . $job_list_id);
        $logger->info("Sent mail to accounts below:");
        
        $this->getContainer()->get('router')->getContext()->setHost('vlance.vn');
        $from = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
        // $parameters = $this->getContainer()->getParameter("vlance_system");
        $count = 1;
        foreach ($accounts as $account) {
            $context = array(
                'signature'         => 'signature',
                'account'           => $account,
                'jobs'              => $jobs,
                'encryption'        => md5($account['email'].'@vlance.vn')
                /*'mixpanel_tracking' => base64_encode(json_encode(
                    array(
                        'event'     => "Job Newsletter Open",
                        'properties'=> array(
                            'token'             => $parameters['mixpanel_token']['prod'],
                            'distinct_id'       => $account['id'],
                            'campaign'          => 'Job Newsletter',
                            'nb_email_in_batch' => count($accounts),
                            'nb_jobs'           => count($jobs),
                            'template'          => $template,
                            'time_weekday'      => date('D', time()),
                            'time_date'         => date('Y-m-d', time()),
                            'time_hour'         => date('H', time())
                        )
                    )
                ))*/
            );
            try {
                if($sendemail){
                    $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $account['email'], array(), false);
                    $success_counter++;
                }
                $logger->info($count++ . "/" . count($accounts) . ": #" . $account['id'] . " SUCCESS" . (!$sendemail?" - EMAIL NOT SENT":""));
            } catch (Exception $ex) {
                $logger->info($count++ . "/" . count($accounts) . ": #" . $account['id'] . " FAIL");
                $logger->info($ex);
            }
        }
        return $success_counter;
    }
    
    protected function compare_account_list($a, $b)
    {
        if((int)($a['id']) > (int)($b['id'])){
            return -1;
        } elseif ((int)($a['id']) < (int)($b['id'])){
            return 1;
        } else {
            return 0;
        }
    }
}
