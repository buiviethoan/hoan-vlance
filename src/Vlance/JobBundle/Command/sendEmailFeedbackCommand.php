<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\JobFeedback;

class sendEmailFeedbackCommand extends ContainerAwareCommand {
   const DEFAULT_TIMES = 1;
   const DEFAULT_LOWER_DAYS = 3;
   const DEFAULT_UPPER_DAYS = 4;

   protected function configure() {
        $this
                ->setName('vlance:job:sendEmail')
                ->setDescription('Vlance send email for admin')
                ->addArgument('action',  InputArgument::REQUIRED,'Action that you want to execute');
    }
    
    /**
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * 
     */
    public function execute(InputInterface $input, OutputInterface $output) {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'jobFeedback' :
                $output->writeln('================= Start send email to client');
                $this->sendFeedbackNotification($output);
                $output->writeln('================= Finished send email to client');
                break;
            default:
                break;
        }
    }
    protected function sendFeedbackNotification(OutputInterface $output) {
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /* @var $helper \Vlance\BaseBundle\Helper\Helper */
        $helper = $this->getContainer()->get('vlance_base.helper');
        $feedbackLimit = $helper->getParameter('vlance_system.feedback_limit');
        $from = $helper->getParameter('vlance_email.from.hotro');
        $template = "VlanceJobBundle:Email:job_feedback.html.twig";
        $search = '"' . JobFeedback::FIELD_NOTIFICATION . '":"0"';
        $replace = '"' . JobFeedback::FIELD_NOTIFICATION . '":"1"';
        $sql = "UPDATE job_feedback SET `" . JobFeedback::FIELD_SQL_ADDITION . "` = REPLACE(`" . JobFeedback::FIELD_SQL_ADDITION . "`, '$search', '$replace') WHERE job_id = '%s'";
        
        $feedbacks = $em->getRepository('VlanceJobBundle:JobFeedback')->statistic(0, array(JobFeedback::FIELD_ADDITION => array('like', '\'%"' . JobFeedback::FIELD_NOTIFICATION . '":"0"%\'')));
        foreach ($feedbacks as $job_id => $feedback) {
            $total = 0;
            if (isset($feedback[0])) {$total += $feedback[0]['numFeedback'];}
            if (isset($feedback[1])) {$total += $feedback[1]['numFeedback'];}
            if ($total >= $feedbackLimit) {
                $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
                $to = $job->getAccount()->getEmail();
                $context = array(
                    'feedback' => $feedback,
                    'total' => $total,
                    'job' => $job
                );
                $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
                $update_sql = sprintf($sql, $job_id);
                $em->getConnection()->exec($update_sql);
                $output->writeln("Sent email notify for job.{$job_id} to email:{$to}");
            }
        }
    }
}
