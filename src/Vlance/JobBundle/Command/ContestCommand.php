<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Input\InputArgument;
use Vlance\BaseBundle\Command\BaseCommand as BaseCommand;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Vlance\JobBundle\Entity\Job;

/**
 * update 20160615: send email to admin
 * CRON FREQUENCY:  liên tục
 */
class ContestCommand extends BaseCommand {
    protected function config() 
    {
        $this->setName('vlance:contest:contestemail')
             ->setDescription("Send email admin.");
    }
    
    protected function exec(InputInterface $input, OutputInterface $output) 
    {
        $this->sendContestEmail($input, $output);
    }
        
    protected function sendContestEmail()
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $now = new \DateTime('now');
        
        $list_contest = $em->getRepository('VlanceJobBundle:Job')->findBy(array('type' => Job::TYPE_CONTEST));

        if(count($list_contest) == 0){
            $this->log("Nothing happen.");
            return true;
        }

        $this->log(count($list_contest), "Number of enabled contest");
        
        $from = $this->container->getParameter('from_email');
        $template = 'VlanceJobBundle:Email/contest:admin_contest_expired.html.twig';
        $email_admin = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
        
        /* @var $contest Job */
        foreach($list_contest as $contest){
            /** 1. Thông báo cho admin về bước 2 đã quá thời gian mà chưa chọn bài dự thi (Hết bước 2)*/
            $now_int = strtotime(date_format($now, 'Y/m/d H:i:s'));
            $close_int = strtotime(date_format($contest->getCloseAt(), 'Y/m/d H:i:s'));
            $qualified_int = strtotime(date_format($contest->getQualifiedAt(), 'Y/m/d H:i:s'));
            if($contest->getStatus() === Job::JOB_OPEN){
                if(($now_int - $close_int) == 0){
                    if($contest->getStatus() != Job::JOB_CONTEST_QUALIFIED){
                        $context_s2 = array(
                            'cid'       => $contest->getId(),
                            'ctitle'    => $contest->getTitle(),
                            'curl'      => $this->container->get('router')->generate('job_show_freelance_job',array('hash' => $contest->getHash()), true),
                            'cmess'     => 'chưa chọn bài dự thi nào dù đã hết thời gian.(Bước 2)'
                        );

                        try{
                            $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context_s2, $from, $email_admin);
                        } catch (Exception $ex) {
                            return new JsonResponse(array('error' => 101));
                        }
                    }
                }
            }
            /** END 1 */
            
            /** 2. Thông báo cho admin về bước 4 đã quá thời gian nhưng chưa trao thưởng cho bài thi nào (Hết bước 4)*/
            if($contest->getStatus() === Job::JOB_CONTEST_QUALIFIED){
                if($contest->getQualifiedAt()){
                    if(($now_int - $qualified_int) == 604800){
                        if($contest->getStatus() != Job::JOB_FINISHED){
                            $context_s4 = array(
                                'cid'       => $contest->getId(),
                                'ctitle'    => $contest->getTitle(),
                                'curl'      => $this->container->get('router')->generate('job_show_freelance_job',array('hash' => $contest->getHash()), true),
                                'cmess'     => 'chưa chọn bài dự thi nào dù đã hết thời gian.(Bước 4)'
                            );

                            try{
                                $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context_s4, $from, $email_admin);
                            } catch (Exception $ex) {
                                return new JsonResponse(array('error' => 102));
                            }
                        }
                    }
                }
            }
            /** END 2 */
        }
        
        $this->log("Sending completed.");
    }
}