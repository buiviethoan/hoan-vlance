<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Utils\JobCalculator;

class GenerateURLCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
                ->setName('vlance:url')
                ->setDescription('Generate url friendly for account')
                ->addArgument('action',  InputArgument::REQUIRED,'Action that you want to execute');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'urlAliasJob' :
                $output->writeln('================= Start generate url friendly');
                $this->generateUrl('VlanceJobBundle:Job');
                $output->writeln('================= Finished generate url friendly');
                break;
            case 'urlAliasAccount':
                $output->writeln('================= Start generate url friendly');
                $this->generateUrl('VlanceAccountBundle:Account');
                $output->writeln('================= Finished generate url friendly');
                break;
            case 'updateCloseAt':
                $output->writeln('================= Update closeAt job');
                $this->updateCloseAt();
                $output->writeln('================= Finished update closeAt job');
                break;
            case 'updateAliasCategory':
                $output->writeln('================= Start generate url friendly');
                $this->generateUrl('VlanceAccountBundle:Category');
                $output->writeln('================= Finished generate url friendly');
                break;
            case 'updateAliasCity':
                $output->writeln('================= Start generate url friendly');
                $this->generateUrl('VlanceBaseBundle:City');
                $output->writeln('================= Finished generate url friendly');
                break;
            case 'updateAliasSkill':
                $output->writeln('================= Start generate url friendly');
                $this->generateUrl('VlanceAccountBundle:Skill');
                $output->writeln('================= Finished generate url friendly');
                break;
            default:
                break;
        }
        
    }
    
    protected function updateCloseAt() {
        /* @var $em EntityManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        /* @var $uow Doctrine\ORM\UnitOfWork */
        $uow = $em->getUnitOfWork();
        $jobs = $em->getRepository('VlanceJobBundle:Job')->findAll();
        /*@var $job Job*/
        foreach ($jobs as $job) {
            $closeAt = $job->getCloseAt();
            $uow->propertyChanged($job, 'closeAt', $job->getCloseAt(), $closeAt->setTime(23,59,59));
            $uow->scheduleForUpdate($job);
        }
        $em->flush();
        
    }

    protected function generateUrl($string) {
        /* @var $em EntityManager*/
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        //Neu la Account slug = getFullName
        if($string == 'VlanceAccountBundle:Account') {
            $slugs = $em->getRepository($string)->findAll();
            foreach ($slugs as $slug) {
                $hash = JobCalculator::url_slug($slug->getFullName().' '.$slug->getId());
                $slug->setHash($hash);
                $em->persist($slug);
            }
            $em->flush();

        } 
        //Lay truong slug = getTitle
        elseif($string == 'VlanceJobBundle:Job') {
            $slugs = $em->getRepository($string)->findAll();
            foreach ($slugs as $slug) {
                $hash = JobCalculator::url_slug($slug->getTitle().' '.$slug->getId());
                $slug->setHash($hash);
                $em->persist($slug);
            }
            $em->flush();
        }
        //slug = title
        elseif($string == 'VlanceAccountBundle:Category') {
            /* @var $uow Doctrine\ORM\UnitOfWork */
            $uow = $em->getUnitOfWork();
            $slugs = $em->getRepository($string)->findAll();
            foreach ($slugs as $slug) {
                $hash = JobCalculator::url_slug($slug->getTitle());
                $slug->setHash($hash);
//                $uow->propertyChanged($slug, 'hash', $slug->getHash(), $slug->setHash($hash));
//                $uow->scheduleForUpdate($slug);
//                $slug->setHash($hash);
                $em->persist($slug);
            }
            $em->flush();
        }
        elseif($string == 'VlanceBaseBundle:City') {
            $slugs = $em->getRepository($string)->findAll();
            foreach ($slugs as $slug) {
                $hash = JobCalculator::url_slug($slug->getName());
                $slug->setHash($hash);
                $em->persist($slug);
            }
            $em->flush();
        }
        elseif($string == 'VlanceAccountBundle:Skill') {
            $slugs = $em->getRepository($string)->findAll();
            foreach ($slugs as $slug) {
                $hash = JobCalculator::url_slug($slug->getTitle());
                $slug->setHash($hash);
                $em->persist($slug);
            }
            $em->flush();
        }
    }
    
}