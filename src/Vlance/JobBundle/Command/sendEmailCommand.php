<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;

class sendEmailCommand extends ContainerAwareCommand {
   const DEFAULT_TIMES = 1;
   const DEFAULT_LOWER_DAYS = 3;
   const DEFAULT_UPPER_DAYS = 4;

   protected function configure() {
        $this
                ->setName('vlance:job:sendEmailAdmin')
                ->setDescription('Vlance send email for admin')
                ->addArgument('action',  InputArgument::REQUIRED,'Action that you want to execute');
                ;
    }
    
    /**
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * 
     */
    public function execute(InputInterface $input, OutputInterface $output) {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'expired_jobs' :
                $output->writeln('================= Start send email to client');
                $this->ExpiredJobs($output);
                $output->writeln('================= Finished send email to client');
                break;
            case 'expired_jobs_account':
                $output->writeln('================= Start send email to account');
                $this->ExpiredJobsAccount($output, $times = self::DEFAULT_TIMES);
                $output->writeln('================= Finished send email to account');
                break;
            case 'request_deposit':
                $output->writeln('================= Start send email request deposit to client');
                $this->RequestDeposit($output, $lower = self::DEFAULT_LOWER_DAYS, $upper_day = self::DEFAULT_UPPER_DAYS);
                $output->writeln('================= Finished send email request deposit to client');
                break;
            default:
                break;
        }
    }
    protected function ExpiredJobs(OutputInterface $output) {
        $beforHour = date('Y-m-d H:i:s', time() - 60 * 60);
        $currentDay = date('Y-m-d H:i:s', time());
        $sql = "select j.id j_id,j.title j_title, j.hash j_hash, acc.id acc_id, acc.full_name acc_name, acc.email acc_email from job j, account acc where j.close_at >= '".$beforHour."' and j.close_at <= '".$currentDay."' and j.sum_duration = 0 and j.account_id = acc.id and j.publish_option = '".\Vlance\JobBundle\Entity\Job::PUBLISH."'" ;
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $jobs = $em->getConnection()->fetchAll($sql);
        if(count($jobs) > 0) {
            $template = "VlanceJobBundle:Email:client_expired_job_nobid.html.twig";
            $this->getContainer()->get('router')->getContext()->setHost('vlance.vn');
            $from = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
            $bcc = array();
            $bcc[] = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
            
            foreach ($jobs as $job) {
                $context = array(
                    'entity' => $job,
                );
                $to = $job['acc_email'];
                try {
                    $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to, $bcc);
                } catch (Exception $ex) {
                    /**
                     * @todo Write log for email exeption
                     */

                }
            }
        
        } 
    }
    
    protected function ExpiredJobsAccount(OutputInterface $output, $times) {
        $beforHour = date('Y-m-d H:i:s', time() - $times * 60 * 60);
        $currentDay = date('Y-m-d H:i:s', time());
        $sql = "select j.id j_id,j.title j_title, j.hash j_hash, (minute('".$currentDay."') - minute(j.close_at)) j_exprise, acc.id acc_id, acc.email acc_email, acc.full_name acc_name, acc.hash acc_hash,count(b.id) num_bid from job j, account acc, bid b where j.close_at >= '".$beforHour."' and j.close_at < '".$currentDay."' and j.sum_duration > 0 and j.account_id = acc.id and b.job_id = j.id and j.publish_option = '".\Vlance\JobBundle\Entity\Job::PUBLISH."'" ;
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $jobs = $em->getConnection()->fetchAll($sql);
        if(count($jobs) > 0) {
            $template_client = "VlanceJobBundle:Email:client_expired_job.html.twig";
            $this->getContainer()->get('router')->getContext()->setHost('vlance.vn');
            $from = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
            $bcc = array();
            $bcc[] = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
            foreach ($jobs as $job) {
                $context_client = array(
                    'entity' => $job,
                );
                $to_client = $job['acc_email'];
                try {
                    $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from, $to_client, $bcc);
                } catch (Exception $ex) {
                    /**
                     * @todo Write log for email exeption
                     */

                }
                
            }
        } 
        
    }
    
    protected function RequestDeposit(OutputInterface $output, $lower_day, $upper_day) {
        $beforDays = date('Y-m-d H:i:s', time() - $lower_day * 24 * 60 * 60);
        $afterDays = date('Y-m-d H:i:s', time() - $upper_day * 24 * 60 * 60);
        $currentDays = date('Y-m-d H:i:s', time());
        $sql = "select j.id j_id,j.title j_title, j.hash j_hash,j.budget_bided j_budgetbided, acc.id acc_id, acc.email acc_email, acc.full_name acc_name, acc.hash acc_hash, b.awarded_at b_awarded, (day('".$currentDays."')- day(b.awarded_at)) b_time from job j, account acc, bid b where b.awarded_at is not null and b.awarded_at >= '".$afterDays."' and b.awarded_at <= '".$beforDays."' and j.budget_bided > 0 and j.account_id = acc.id and j.status = '".\Vlance\JobBundle\Entity\Job::JOB_AWARDED."'" ;
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $jobs = $em->getConnection()->fetchAll($sql);
        if(count($jobs) > 0) {
            $template_client = "VlanceJobBundle:Email:client_request_deposit.html.twig";
            $this->getContainer()->get('router')->getContext()->setHost('vlance.vn');
            $from = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
            foreach ($jobs as $job) {
                $context_client = array(
                    'entity' => $job,
                );
                $to_client = $job['acc_email'];
                try {
                    $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template_client, $context_client, $from, $to_client);
                } catch (Exception $ex) {
                    /**
                     * @todo Write log for email exeption
                     */

                }
            }
        } 
    }
 
}
