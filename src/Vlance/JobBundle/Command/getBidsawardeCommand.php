<?php
namespace Vlance\JobBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\Job;
use Proxies\__CG__\Vlance\JobBundle\Entity\Bid;
use Vlance\BaseBundle\Utils\JobCalculator;

class getBidsawardeCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
                ->setName('vlance:getbids')
                ->setDescription('Notification bid awarde success')
                ->addArgument('action',  InputArgument::REQUIRED,'Action that you want to execute');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'bidsawarde' :
                $output->writeln('================= Start send email');
                $this->sendEmailAdmin();
                $output->writeln('================= Finished send email');
                break;
            default:
                break;
        }
        
    }
    
    public function sendEmailAdmin() {
        $to_day = '2014-07-26 00:00:00';
        $from_day = '2014-07-29 00:00:00';
         $sql = "select * from bid where awarded_at >= '".$to_day."' and awarded_at <= '".$from_day."'";
        /* @var $em \Doctrine\Common\Persistence\ObjectManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $queryBuilder = $em->createQueryBuilder()
                ->select('bid')
                ->from('Vlance\JobBundle\Entity\Bid', 'bid')
                ;
        $andCondition = $queryBuilder->expr()->andX();
        $andCondition->add($queryBuilder->expr()->gte('bid.awardedAt', "'" . $to_day . "'"));
        $andCondition->add($queryBuilder->expr()->lte('bid.awardedAt', "'" . $from_day . "'"));
        $queryBuilder->andWhere($andCondition);
        $bids = $queryBuilder->getQuery()->getResult();
        
        
        $email_hotro = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
        $email_admin = $this->getContainer()->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
        
        foreach($bids as $bid) {
            $email_context = array('job' => $bid->getJob(), 'bid' => $bid);
            $template = "VlanceJobBundle:Email/job_awarded:bid_awarded_but_not_funded__freelancer.html.twig";
            $this->getContainer()->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $email_admin);
        }
        
    }
    
}