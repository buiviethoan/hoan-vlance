<?php
    /* @var $feedback array(
     *  0 => array('job_id' => $job_id, 'numFeedback' => $numFeedback), //JobFeedback::TYPE_DESCRIPTION
     *  1 => array('job_id' => $job_id, 'numFeedback' => $numFeedback, 'budget' => $budget), //JobFeedback::TYPE_BUDGET
     * )
     */
//     print_r($feedback);
?>

<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>

<?php echo $view['form']->start($form); ?>
    <div class="row-fluid">
        <div class="span4 feedback-option client-view offset1 left-col">
            <h4>Cần mô tả chi tiết hơn về dự án</h4>
            <p><?php echo (isset($feedback[0]))?$feedback[0]['numFeedback']:0;?> người gợi ý</p>
            <a class="btn btn-large btn-primary" href="<?php echo $view['router']->generate('job_edit',array('id' => $job->getId())) ?>">Bổ sung thông tin</a>
        </div>
        <div class="span5 feedback-option client-view offset1">
            <h4>Cần nâng ngân sách lên mức phù hợp hơn</h4>
            <p><?php echo (isset($feedback[1]))?$feedback[1]['numFeedback']:0;?> người gợi ý</p>
            <?php echo $view['form']->widget($form['save'])?>
            <?php echo $view['form']->row($form['budget'])?>
        </div>
    </div>
<?php echo $view['form']->end($form);