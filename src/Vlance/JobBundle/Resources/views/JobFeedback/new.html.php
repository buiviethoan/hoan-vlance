<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<?php echo $view['form']->start($form); ?>
    <div class="row-fluid">
        <div class="span4 feedback-option offset1 left-col">
            <h4>Thông tin mô tả về dự án chưa đầy đủ?</h4>
            <?php echo $view['form']->widget($form['save'])?>
        </div>
        <div class="span5 feedback-option offset1">
            <h4>Ngân sách khách hàng đưa ra chưa hợp lý?</h4>
            <?php echo $view['form']->widget($form['saveWithBudget'])?>
            <?php echo $view['form']->row($form['budget'])?>            
        </div>
    </div>
<?php echo $view['form']->end($form); ?>
