<h3>Tài liệu đã tải lên</h3>
<?php if($entities) {?>
<?php foreach ($entities as $entity) :?>
    <?php $files = $entity->getFiles(); ?>
    <?php if($files) : ?>
        <?php foreach($files as $file) : ?>
            <div><a href="/web/<?php echo $file->getPath() ?>"><?php echo $file->getFilename() ?></a></div>
        <?php endforeach; ?>
    <?php endif; ?>
<?php endforeach;?>
<?php } else { ?>
    <div class="message-file">Chưa có tài liệu</div>
<?php } ?>
