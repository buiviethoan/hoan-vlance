<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', $entity->getSender()->getFullName()); ?>
<?php $view['slots']->start('content') ?>
<h1>Create new message</h1>
<div class ="form-message">
    <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => $entity->getBid()->getId())));?>
</div>
<div>       
    <div><lable>Người gửi : </label> <?php echo $entity->getSender()->getFullName() ?></div>
    <div><label>Ngày gửi: </lable><?php echo $entity->getCreatedAt()->format('d-m-Y H.i') ?></div>
    <div><label>Nội dung :</label><?php echo substr($entity->getContent(),0 , 500) ?></div>
    
</div>
<?php $view['slots']->stop(); ?>
