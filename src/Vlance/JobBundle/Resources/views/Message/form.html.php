<?php  $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<form action="<?php echo $view['router']->generate('message_create_first',array('bid' => $bid)) ?>" method="post" <?php echo $view['form']->enctype($form) ?> id="sendmessage">
    <?php echo $view['form']->widget($form)?>
    <div>
        <input type="file" name="file" multiple="multiple" />
        <input type="submit" class="btn btn-primary" onclick="sendMessage(); return false;" value="<?php echo $view['translator']->trans('Gửi tin nhắn', array(), 'vlance'); ?>"/>
    </div>
</form>
<script type="text/javascript">
     
        function sendMessage() {
           jQuery.post("<?php echo $view['router']->generate('message_create_first',array('bid' => $bid)) ?>",$("#sendmessage").serialize(), function(data) {
              jQuery('.send-message').html(data);
            },"json")
            .fail(function() { alert("Send message error"); });
        }
</script>