<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', 'Hộp thư đến'); ?>
<?php $view['slots']->start('content') ?>
<h1>Create new message</h1>
<div class ="form-message">
    <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => $bid)));?>
</div>
<table class="records_list">
    <thead>
        <tr>
            <th>Người gửi</th>
            <th>Lời nhắn</th>
            <th>Thời gian</th>
        </tr>
    </thead>
    <tbody>
        
        <?php foreach ($entities as $entity) :?>
        <tr>
           
            <td><a href="<?php echo $view['router']->generate('message_show',array('id' => $entity->getId())) ?>"><?php echo $entity->getSender()->getFullName() ?></a></td>
            <td><?php echo substr($entity->getContent(),0 , 500) ?></td>
            <td><?php echo $entity->getCreatedAt()->format('d-m-Y H.i') ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php $view['slots']->stop(); ?>
