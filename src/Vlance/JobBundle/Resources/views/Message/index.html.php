<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', 'Hộp thư đến'); ?>
<?php $view['slots']->start('left') ?>
<?php $view['slots']->stop(); ?>
<?php $view['slots']->start('content') ?>
<h1>Hộp thư đến</h1>
<table class="records_list">
    <thead>
        <tr>
            <th>Người gửi</th>
            <th>Lời nhắn</th>
            <th>Thời gian</th>
        </tr>
    </thead>
    <tbody>
        
        <?php foreach ($entities as $entity) :?>
        <tr>
           
            <td><?php echo $entity->getSender()->getFullName() ?></td>
            <td><?php echo $entity->getContent() ?></td>
            <td><?php echo $entity->getCreatedAt()->format('d-m-Y H.i') ?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php $view['slots']->stop(); ?>
