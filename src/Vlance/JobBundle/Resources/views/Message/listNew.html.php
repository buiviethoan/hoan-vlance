<?php 
    use Vlance\BaseBundle\Utils\JobCalculator; 
?>
<div class="popover-mail">
   <?php if(count($entities) < 0): ?>
        <?php // echo $view['translator']->trans('site.menu.popover_messager', array(), 'vlance') ?>
    <?php endif; ?>

    
    <div class="i32 i32-mail">
        <?php if(count($entities) > 0): ?>
            <div class="mail-counter">
                <?php  echo count($entities); ?>
            </div>
        <?php endif; ?>
        <div class="mail-counter mail-counter-invite"></div>
    </div>    
    <div class="block-new-message">
        <ul class="nav nav-tabs" id="tab_messages_listinvite">
            <li class="active"><a data-toggle="tab" href="#tab_messages"><?php echo $view['translator']->trans('job.workroom.workroom',array(),'vlance'); ?></a></li>
            <li><a data-toggle="tab" href="#tab_list_invite"><?php echo $view['translator']->trans('invite.popup.title1',array(),'vlance') ;?></a></li>
        </ul>
        <div class="tab-content">
            <?php if(count($entities) == 0): // There is no message ?>
                <div class="tab-pane active" id="tab_messages">
                    <div class="no-message"><?php echo $view['translator']->trans('menu.message.nomessage', array(), 'vlance');?></div>
                </div>
            <?php else:?>
                <div  class="tab-pane active" id="tab_messages">
                    <ul><a style="position: absolute;"></a>
                        <?php foreach ($entities as $entity): ?>
                        <?php /* @var $entity \Vlance\JobBundle\Message */ ?>
                            <?php $bid = $entity->getBid() ?>
                            <?php $job = $bid->getJob() ?>
                        <li>
                            <div class="acc-image"><a href="<?php echo $view['router']->generate('workshop_bid', array('job_id' => $job->getId(), 'bid' => $bid->getId())) ?>">  
                                <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($entity->getSender()->getUploadDir().DS.$entity->getSender()->getPath(), '62x62', 62, 62, 1); ?>
                                <?php if($resize) : ?>
                                <img src="<?php echo $resize; ?>" alt="<?php echo $entity->getSender()->getFullName(); ?>" title="<?php echo $entity->getSender()->getFullName(); ?>" />
                                <?php else: ?>
                                <img width="62" height="62" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $entity->getSender()->getFullName(); ?>" title="<?php echo $entity->getSender()->getFullName(); ?>" />
                                <?php endif; ?>
                                </a></div>
                            <div class="message-content">
                                <a href="<?php echo $view['router']->generate('workshop_bid', array('job_id' => $job->getId(), 'bid' => $bid->getId())) ?>">
                                    <?php echo $entity->getSender()->getFullName() ?> <?php echo $view['translator']->trans('menu.message.content1',array(),'vlance'); ?> 
                                    <strong>
                                        <?php echo htmlentities(JobCalculator::cut(JobCalculator::replacement($job->getTitle()),100), ENT_SUBSTITUTE, "UTF-8") ?>
                                    </strong> : 
                                    "<?php echo JobCalculator::cut(JobCalculator::replacement($entity->getContent()),100) ?>" 
                                </a>
                            <?php 
                                $time = $entity->getCreatedAt()->format('d/m/Y H.i A');
                                $today = new DateTime('now');
                                $yesterday = new DateTime('yesterday');
                                $createat = $entity->getCreatedAt();
                                if($today->format('d/m/Y') == $createat->format('d/m/Y')) {
                                    $time = $createat->format('H.i A');
                                }elseif($createat->format('d/m/Y') == $yesterday->format('d/m/Y')) {
                                    $time = $view['translator']->trans('workshop.time.yesterday', array(), 'vlance') .', ' .$createat->format('d/m/Y H.i A');
                                } 
                            ?>
                                 <div class="time"><?php echo $time ?></div>
                            </div>

                        </li>
                        <?php endforeach; ?>       
                    </ul>
                </div>
            <?php endif;?>

            <div class="tab-pane" id="tab_list_invite">
                <?php echo $view['actions']->render($view['router']->generate('list_jobinvite_acc'));?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">  
    $(document).ready(function() {        
        $('#tab_messages_listinvite a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        $('.mail-counter-invite').append($( "#tab_list_invite ul" ).attr( "att-count" ));
    });
</script>

