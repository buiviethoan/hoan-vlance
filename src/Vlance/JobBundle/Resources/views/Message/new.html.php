<?php use Vlance\JobBundle\Entity\Job; ?>
<?php  $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>

<?php /* @var $job \Vlance\JobBundle\Entity\Job */ ?>
<?php /* @var $bid \Vlance\JobBundle\Entity\Bid */ ?>
<?php $job = $bid->getJob(); ?>
<?php $credit_required_for_interview = false; ?>
<?php $having_enough_credit = false; ?>
<?php $require_credit = array('cost' => 0);?>
<?php $now = new \DateTime('now'); ?>

<?php if($current_user->getId() == $bid->getAccount()->getId()): ?>
    <?php if(is_null($current_user->getExpiredDateVip())): ?>
        <?php if($job->getCategory()->getParent()->getId() !== 1 && $job->getCategory()->getParent()->getId() !== 4): ?>
            <?php if(count($bid->getMessagesByFreelancer()) == 0): ?>
                <?php // Job is open or never funded; ?>
                <?php if($job->getStatus() == Job::JOB_AWARDED || $job->getStatus() == Job::JOB_OPEN): ?>
                    <?php // Freelancer has not send back any message ?>
                    <?php $require_credit = $view['vlance']->getParameter('vlance_system.testing_config.credit_for_interview'); ?>
                    <?php if(isset($require_credit['from_job_id']) && isset($require_credit['from_account_id'])):?>
                        <?php if((int)($bid->getAccount()->getId()) >= (int)($require_credit['from_account_id']) && (int)($job->getId()) >= (int)($require_credit['from_job_id'])): ?>
                            <?php /* @TODO Kiểm tra xem bid này có mất credit ko. Nếu có thì ko phải mất credit nữa, còn nếu chưa thì mới hiện ra yêu cầu dùng credit. Cách làm: thêm helper kiểm tra xem FL có phải dùng credit cho bid ko? */ ?>
                            <?php $credit_required_for_interview = true; ?>
                            <?php if(isset($require_credit['cost'])): ?>
                                <?php if($bid->getAccount()->getCredit()->getBalance() >= (int)($require_credit['cost'])): ?>
                                    <?php $having_enough_credit = true; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php else: ?>
            <?php $credit_required_for_interview = true; ?>
            <?php $having_enough_credit = true; ?>
        <?php endif; ?>
    <?php elseif(!is_null($current_user->getExpiredDateVip())): ?>
        <?php if(\Vlance\BaseBundle\Utils\JobCalculator::ago($current_user->getExpiredDateVip(), $now) === false): ?>
            <?php if($job->getCategory()->getParent()->getId() !== 1 && $job->getCategory()->getParent()->getId() !== 4): ?>
                <?php if(count($bid->getMessagesByFreelancer()) == 0): ?>
                    <?php // Job is open or never funded; ?>
                    <?php if($job->getStatus() == Job::JOB_AWARDED || $job->getStatus() == Job::JOB_OPEN): ?>
                        <?php // Freelancer has not send back any message ?>
                        <?php $require_credit = $view['vlance']->getParameter('vlance_system.testing_config.credit_for_interview'); ?>
                        <?php if(isset($require_credit['from_job_id']) && isset($require_credit['from_account_id'])):?>
                            <?php if((int)($bid->getAccount()->getId()) >= (int)($require_credit['from_account_id']) && (int)($job->getId()) >= (int)($require_credit['from_job_id'])): ?>
                                <?php /* @TODO Kiểm tra xem bid này có mất credit ko. Nếu có thì ko phải mất credit nữa, còn nếu chưa thì mới hiện ra yêu cầu dùng credit. Cách làm: thêm helper kiểm tra xem FL có phải dùng credit cho bid ko? */ ?>
                                <?php $credit_required_for_interview = true; ?>
                                <?php if(isset($require_credit['cost'])): ?>
                                    <?php if($bid->getAccount()->getCredit()->getBalance() >= (int)($require_credit['cost'])): ?>
                                        <?php $having_enough_credit = true; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php else: ?>
                <?php $credit_required_for_interview = true; ?>
                <?php $having_enough_credit = true; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <?php $credit_required_for_interview = true; ?>
        <?php $having_enough_credit = true; ?>
    <?php endif; ?>
<?php endif; ?>

<?php if($credit_required_for_interview && $job->getType() === Job::TYPE_BID): ?>
    <?php // Require Credit to respond ?>
    <div class="require-credit-popup">
        <div class="overlay-bidform compact-style"></div>  
        <div class="out-of-bid compact-style">
            <h2><?php echo $view['translator']->trans('workshop.message_form.heading', array(), 'vlance')?></h2>
            <p><?php echo $view['translator']->trans('workshop.message_form.reason', array("%%cost%%" => $require_credit['cost']), 'vlance')?></p>
            <p class="credit-balance"><?php echo $view['translator']->trans('workshop.message_form.balance', array("%%balance%%" => $current_user->getCredit()->getBalance()), 'vlance')?></p>
            <p class="action-buttons">
                <?php if(!$having_enough_credit): ?>
                    <a href="<?php echo $view['router']->generate('credit_balance', array()); ?>?ref=require-credit-respond-client" class="btn btn-large btn-primary" data-display-track="show-require-credit-respond-client-not-enough"
                       onclick="vtrack('Click Go to buy credit', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'Workroom respond client', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>'})">
                        <strong><?php echo $view['translator']->trans('workshop.message_form.action_buy', array(), 'vlance')?></strong>
                    </a>
                <?php else: ?>
                    <a class="btn btn-large btn-primary close-popup" data-display-track="show-require-credit-respond-client-enough"
                       onclick="vtrack('Click Close require credit popup', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'Workroom respond client', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>'})">
                        <strong><?php echo $view['translator']->trans('workshop.message_form.action_ok', array(), 'vlance')?></strong>
                    </a>
                <?php endif; ?>
            </p>
        </div>
    </div>
    <?php // END Require Credit to respond ?>
<?php endif; ?>

<form <?php if(!$credit_required_for_interview):?>
    action="<?php echo $view['router']->generate('message_create', array('bid' => $bid->getId()))?>" 
    method="post" <?php echo $view['form']->enctype($form) ?>
    <?php elseif($credit_required_for_interview): ?>
        <?php if($job->getType() === Job::TYPE_BID && $having_enough_credit): ?>
            action="<?php echo $view['router']->generate('message_create', array('bid' => $bid->getId()))?>" 
            method="post" <?php echo $view['form']->enctype($form) ?>
        <?php else: ?>
            action="<?php echo $view['router']->generate('message_create', array('bid' => $bid->getId()))?>" 
            method="post" <?php echo $view['form']->enctype($form) ?>
        <?php endif; ?> 
    <?php endif; ?> 
        id="send-message">
    <?php echo $view['form']->widget($form)?>
    
    <?php if(!$credit_required_for_interview):?>
        <div class="button">
            <input type="submit" class="btn btn-large btn-primary input-send-message"
                   <?php // If the job is funded, and on WORKING status, Do not check the contact leak, so: data-validate = 0 ?>
                   data-validate="<?php echo $bid->getJob()->getStatus() == Job::JOB_WORKING ? 0 : 1 ?>" 
                   value="<?php echo $view['translator']->trans('message.button', array(), 'vlance') ?>"/>
        </div>
    <?php elseif($credit_required_for_interview): ?>
        <?php if($job->getCategory()->getParent()->getId() !== 1 && $job->getCategory()->getParent()->getId() !== 4): ?>
            <div class="button">
                <input type="submit" class="btn btn-large btn-primary input-send-message"
                       <?php // If the job is funded, and on WORKING status, Do not check the contact leak, so: data-validate = 0 ?>
                       data-validate="<?php echo $bid->getJob()->getStatus() == Job::JOB_WORKING ? 0 : 1 ?>" 
                       value="<?php echo $view['translator']->trans('message.button', array(), 'vlance') ?>"/>
            </div>
        <?php else: ?>
            <?php if($job->getType() === Job::TYPE_BID && $having_enough_credit): ?>
                <div class="button">
                    <input type="submit" class="btn btn-large btn-primary input-send-message"
                            <?php // If the job is funded, and on WORKING status, Do not check the contact leak, so: data-validate = 0 ?>
                            data-validate="<?php echo $bid->getJob()->getStatus() == Job::JOB_WORKING ? 0 : 1 ?>" 
                            value="<?php echo $view['translator']->trans('message.button', array(), 'vlance') ?>"/>
                </div>
            <?php else: ?>
                <div class="button">
                    <input type="submit" class="btn btn-large btn-primary input-send-message"
                            <?php // If the job is funded, and on WORKING status, Do not check the contact leak, so: data-validate = 0 ?>
                            data-validate="<?php echo $bid->getJob()->getStatus() == Job::JOB_WORKING ? 0 : 1 ?>" 
                            value="<?php echo $view['translator']->trans('message.button', array(), 'vlance') ?>"/>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
</form>
