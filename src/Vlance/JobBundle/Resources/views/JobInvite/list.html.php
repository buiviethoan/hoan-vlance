<?php // $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('freelancer_list'));?>
<?php    use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php    use Vlance\JobBundle\Entity\Job; ?>
<?php /* @var $job Vlance\JobBundle\Entity\Job */ ?>
<?php  $i = 0;  ?>
<?php foreach ($jobInvites as $invite) : ?>
    <?php $i++;?>
    <?php $accountInvite = $invite->getAccount(); ?>
    <div class="span3 freelan-invite <?php if($i == 5 || $i == 9 || $i == 13 || $i == 17 || $i == 21 || $i == 25 || $i == 29){ echo 'span-freelan-invite'; } ?> <?php if($i == count($jobInvites)){echo 'last';}?>">
        <span class="dropdown-toggle invite-avatar" >
            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $accountInvite->getHash())) ?>" style="color:#333333">
            <?php $resize = ResizeImage::resize_image($accountInvite->getFullPath(), '80x80', 80, 80, 1);?>
            <?php if($resize) : ?>
            <img src="<?php echo $resize; ?>" alt="<?php echo $accountInvite->getFullName(); ?>" title="<?php echo $accountInvite->getFullName(); ?>" />
            <?php else: ?>
            <img class="blur" width="80" height="80" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $accountInvite->getFullName(); ?>"  title="<?php echo $accountInvite->getFullName(); ?>" />
            <?php endif; ?>
            <?php $bids = $job->getBids();$bidded = false;?>
            <?php foreach ($bids as $bid) {
                /* @var $bid Vlance\JobBundle\Entity\Bid */
                if ($bid->getAccount() == $accountInvite) {
                    $bidded = true;
                    break;
                }
            }?>
            <?php if ($bidded==1):?>
                <span class="background-text-invited"><?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job4', array(), 'vlance') ?></span>
            <?php endif; ?>
            </a>
            <div class="invite-account">
                <dt class="title-account" ><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $accountInvite->getHash())) ?>"><?php echo $accountInvite->getFullName(); ?></a></dt>
                <dt class="rating-box">
                    <?php if($accountInvite->getNumReviews() != 0): ?>
                    <?php $rating= ($accountInvite->getScore() / $accountInvite->getNumReviews()) * 20;?>
                        <div class="rating" style="width:<?php echo round($rating, 2).'%'; ?>"></div>
                    <?php else: ?>
                        <div class="rating" style="width:0%"></div>
                    <?php endif; ?>
                </dt>
                <dt class="count-job-done"><?php echo $accountInvite->getNumBid();?> <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job3', array(), 'vlance') ?></dt>
            </div>
        </span>
        <div class="dropdown-menu-body">
            <div class="popover bottom dropdown-menu-invite" role="menu" aria-labelledby="dLabel">
                <div class="arrow"></div>
                <div class="popover-content info-acc-invited">
                    <div class="title-acc-invited">
                        <a style="width:80%;color: #333333;" href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $accountInvite->getHash())) ?>"><?php echo $accountInvite->getFullName(); ?></a>
                        <?php if ($accountInvite->getIsCertificated()) : ?>
                            <img style="height: 24px; margin-top: 1px; width: 24px;" src="/img/logo_certificated.png" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                                title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                    </div>
                    <div class="skill-acc-invited">
                        <span style="color: #a0a0a0;margin-left: 6px;"><?php echo $accountInvite->getTitle();?></span>
                    </div>
                    <div class="acc-num-bid" style="color:#666666;">
                        <a class="dt1"><?php echo $accountInvite->getNumBid();?> <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job2', array(), 'vlance') ?> </a> 
                        <a class="dt2" style="color:#666666;padding-left: 25px;"><?php echo number_format($accountInvite->getInCome(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></a>
                    </div>
                    <div class="skill">
                        <?php $skills = $accountInvite->getSkills() ?>
                        <?php if(count($skills) > 0): ?>
                            <?php foreach ($skills as $skill): ?>
                            <span class="skill-list-acc">
                                <?php echo $skill->getTitle(); ?>  
                            </span>
                            <?php endforeach;?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php  endforeach; ?>

<!-- Add freelance -->
<?php if($job->getStatus() === Job::JOB_OPEN): ?>
    <?php if(count($jobInvites) < 24):?>
    <div class="span3 open-popup-suggest <?php if($i + 1 == 5 || $i + 1 == 9 || $i + 1 == 13 || $i + 1 == 17 || $i + 1 == 21 || $i + 1 == 25 || $i + 1 == 29){ echo 'first'; } ?>">
        <div class="block-button">
            <a class="btn btn-primary btn-popup-invite-fl" href="#invite-job-popup" data-toggle="modal"
               onclick="vtrack('Click job invite popup', {'location' : 'Job view', 
                                                          'job_category' : '<?php echo $job->getCategory()?>', 
                                                          'job_budget' : '<?php echo $job->getBudget() ?>', 
                                                          'job_city' : '<?php echo $job->getCity() ?>'})">
                + Mời freelancer
            </a>
        </div>
    </div>
    <?php endif; ?> 
<?php endif; ?> 

<!-- modal -->
<div id="invite-job-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Freelancer được đề xuất với bạn</h3>
    </div>
    <div class="modal-body">
        <?php $j = 0; ?>
        <?php foreach ($suggested_freelancers as $freelancers): ?>
            <?php $j++; ?>
            <div class="suggested-freelancer <?php if($j==count($suggested_freelancers)){echo 'last';}?>">
                <div class="span9 profile-suggested-fl">
                    <div class="avata-fl span3">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancers->getHash())) ?>">
                            <?php $resize = ResizeImage::resize_image($freelancers->getFullPath(), '80x80', 80, 80, 1);?>
                            <?php if($resize) : ?>
                                <img src="<?php echo $resize; ?>" alt="<?php echo $freelancers->getFullName(); ?>" title="<?php echo $freelancers->getFullName(); ?>" />
                            <?php else: ?>
                                <img class="blur" width="80" height="80" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $freelancers->getFullName(); ?>"  title="<?php echo $freelancers->getFullName(); ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="info-fl span9">
                        <div class="row-fluid">
                            <div class="name-fl">
                                <span>
                                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $freelancers->getHash())) ?>">
                                        <span class="name-text"><strong><?php echo $freelancers->getFullName(); ?></strong></span>
                                    </a>
                                </span>
                                <span class="checked">
                                    <?php if ($freelancers->getIsCertificated()) : ?>
                                        <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>" title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                                    <?php endif;?> 

                                    <?php if (!is_null($freelancers->getTelephoneVerifiedAt())): ?>
                                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                                    <?php endif; ?>
                                    <?php if (!is_null($freelancers->getPersonId())): ?>
                                        <?php if (!is_null($freelancers->getPersonId()->getVerifyAt())): ?>
                                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php /* comment        
                                        <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                                    */ ?>  
                                    <script type="text/javascript" language="javascript">
                                        jQuery(document).ready(function(){
                                            $(function () {
                                                $('[data-toggle="tooltip"]').tooltip();
                                            });
                                        });
                                    </script>
                                </span>
                            </div>
                            <div class="title-fl">
                                <?php echo $freelancers->getTitle(); ?>
                            </div>
                            <div class="rating-box add-rating-box">
                                <?php $rating = ($freelancers->getNumReviews() == 0) ? 0 : (($freelancers->getScore() / $freelancers->getNumReviews()) * 20); ?>
                                <div class="rating" style="width:<?php echo number_format($rating, '2', '.', '.') . '%'; ?>"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3 button-invite-suggested-fl" id="invite-button">
                    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                    <a class="invite-action btn btn-primary btn-invite-suggested" href="javascript:void(0)" 
                       ajax-href="<?php echo $view['vlance_job']->generateInviteUrl($job->getId(), $freelancers->getId(), true);?>" 
                       title="<?php echo $job->getTitle()?>"
                       onclick="vtrack('Click job invite popup item', {'location' : 'Job view', 
                                            'job_category' : '<?php echo $job->getCategory()?>', 
                                            'job_budget' : '<?php echo $job->getBudget()?>', 
                                            'job_city' : '<?php echo $job->getCity()?>',
                                            'freelancer_id' : '<?php echo $freelancers->getID() ?>',
                                            'freelancer_category' : '<?php echo $freelancers->getCategory()? $freelancers->getCategory() : '' ?>',
                                            'freelancer_city' : '<?php echo $freelancers->getCity() ? $freelancers->getCity() : '' ?>'})">
                        Mời chào giá
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php //invite freelancer join job ?>
<script type="text/javascript">
    $(document).ready(function(){
        jQuery('#invite-button a.invite-action').click(function(){
            var parent = $(this).closest('.suggested-freelancer');
            parent.addClass("loading");
            $.ajax({
                'url': $(this).attr('ajax-href')
            })
            .done(function(res){
                $('.navbar .upper-section .container #messages').remove();
                var alertClass = 'alert-error';
                if (res.error == 0) {
                    alertClass = 'alert-success';
                    
                    avatar_url          = parent.find('.avata-fl img').attr('src');
                    profile_url         = parent.find('.name-fl a').attr('href');
                    rating_score        = parent.find('.rating-box .rating').css('width');
                    profile_fullname    = parent.find('.name-fl .name-text strong').text();
                    
                    if($('.freelan-invite').size() == 0){
                        //Chua co freelancer nao duoc moi
                        alertItem1 = '<div class="span3 freelan-invite last"><span class="dropdown-toggle invite-avatar"><a href="' + profile_url + '" style="color:#333333"><img class="blur" width="80" height="80" src="'+avatar_url+'" alt="' + profile_fullname + '" title="' + profile_fullname + '"></a><div class="invite-account"><dt class="title-account"><a href="' + profile_url + '">'+ profile_fullname +'</a></dt><dt class="rating-box"><div class="rating" style="width:'+rating_score+'"></div></dt></div></span></div>'; 
                        $(alertItem1).insertBefore(".open-popup-suggest");
                    } else {
                        //Da co freelancer nao duoc moi
                        /* set freelance first */
                        $('.freelan-invite.last').addClass("before-last");
                        alertItem1 = '<div class="span3 freelan-invite last"><span class="dropdown-toggle invite-avatar"><a href="' + profile_url + '" style="color:#333333"><img class="blur" width="80" height="80" src="'+avatar_url+'" alt="' + profile_fullname + '" title="' + profile_fullname + '"></a><div class="invite-account"><dt class="title-account"><a href="' + profile_url + '">'+ profile_fullname +'</a></dt><dt class="rating-box"><div class="rating" style="width:'+rating_score+'"></div></dt></div></span></div>'; 
                        $(alertItem1).insertAfter(".freelan-invite.last");
                        $('.freelan-invite.before-last').removeClass("last");

                        if($('.freelan-invite').size() % 4 == 1){
                            $('.freelan-invite.last').addClass("span-freelan-invite");
                        }

                        /* set button invite first */
                        if($('.freelan-invite').size() % 4 == 0){
                            $('.open-popup-suggest').addClass("first");
                        } else {
                            $('.open-popup-suggest.first').removeClass("first");
                        }
                        
                        //Xoa nut moi freelancer khi moi du 24 nguoi
                        if($('.freelan-invite').size() == 24){
                            $('.open-popup-suggest').remove();
                        }
                    }
                    parent.hide(300, function(){parent.remove()});
                }
                alertItem = '<div class="alert fade in ' + alertClass + '"><a class="close" data-dismiss="alert">×</a><div>' + res.errorMsg + '</div></div>';
                $("#messages .body-messages").html("");
                $("#messages .body-messages").append(alertItem);
                $("#messages").show();
                $(document).ready(function(){
                    setTimeout( "$('#messages').hide();", 5000);
                })
            })
            .fail(function(){
            })
            .always(function(){
                parent.removeClass("loading");
            });
            return;
        });
    });
</script>
<?php // END invite freelancer join job ?>
