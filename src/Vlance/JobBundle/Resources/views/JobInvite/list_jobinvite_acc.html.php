<?php

use Vlance\BaseBundle\Utils\ResizeImage;
?>
    <?php /* @var $jobinvite Vlance\JobBundle\Entity\JobInvite */ ?>
    <?php /* @var $jobinvite Vlance\JobBundle\Entity\Job */ ?>
<ul att-count="<?php echo count($jobInvites); ?>">
<?php foreach ($jobInvites as $jobinvite) : ?>
    <?php $accountInvite = $jobinvite->getJob()->getAccount(); /*     * acc ngtao job* */ ?>
        <li>
            <div class="acc-image">
                <a href="<?php echo $view['router']->generate('account_show_freelancer', array('hash' => $accountInvite->getHash())) ?>">
                    <?php $resize = ResizeImage::resize_image($accountInvite->getFullPath(), '62x62', 62, 62, 1); ?>
                    <?php if ($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $accountInvite->getFullName(); ?>" title="<?php echo $accountInvite->getFullName(); ?>" />
                    <?php else: ?>
                        <img class="blur" width="62" height="62" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $accountInvite->getFullName(); ?>"  title="<?php echo $accountInvite->getFullName(); ?>" />
                    <?php endif; ?>
                </a>
            </div>
            <div class="message-content">
                <a href="<?php echo $view['router']->generate('job_show_freelance_job', array('hash' => $jobinvite->getJob()->getHash())) ?>">
                    <?php echo $accountInvite->getFullName(); ?> <?php echo $view['translator']->trans('invite.popup.freelance_invite', array(), 'vlance') ?>
                    <strong><?php echo $jobinvite->getJob()->getTitle(); ?></strong>
                    <?php if($jobinvite->getJob()->getCloseAt()->getTimestamp() < time()): ?>
                    <i>(<?php echo $view['translator']->trans('job_invite.status.job_closed', array(), 'vlance') ?>)</i>
                    <?php endif; ?>
                    <?php if($jobinvite->getJob()->getStatus() == Vlance\JobBundle\Entity\Job::JOB_AWARDED): ?>
                    <i>(<?php echo $view['translator']->trans('job_invite.status.job_awarded', array(), 'vlance') ?>)</i>
                    <?php endif; ?>
                </a>
                <div class="time"><?php echo $jobinvite->getCreatedAt()->format('d/m/Y, H:i'); ?></div>
                <div class="refuse_invite tf200">
                    <a class="btn btn-primary" 
                       href="<?php echo $view['vlance_job']->generateRefuseInviteUrl($jobinvite->getJob()->getId(), $jobinvite->getAccount()->getId(), true); ?>">
                    <?php echo $view['translator']->trans('invite.popup.button', array(), 'vlance') ?>
                    </a>
                </div>

            </div>

        </li>
<?php endforeach; ?>
</ul>
