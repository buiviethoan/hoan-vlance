<!-- Modal -->
<?php    use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php /* @var $jobinvite Vlance\JobBundle\Entity\JobInvite */ ?>
<div id="block_notify" class="modal hide fade popinvite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3><?php echo $view['translator']->trans('invite.popup.title', array(), 'vlance') ?></h3>
    </div>
    <div class="modal-body" style="min-height: 0px">
    <ul>
        <?php foreach ($jobInvites as $jobinvite) : ?>
    <?php  $accountInvite = $jobinvite->getJob()->getAccount(); ?>
        <li>
            <div class="acc-image">
                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $accountInvite->getHash())) ?>">
                    <?php $resize = ResizeImage::resize_image($accountInvite->getFullPath(), '62x62', 62, 62, 1);?>
                    <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $accountInvite->getFullName(); ?>" title="<?php echo $accountInvite->getFullName(); ?>" />
                    <?php else: ?>
                        <img class="blur" width="62" height="62" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $accountInvite->getFullName(); ?>"  title="<?php echo $accountInvite->getFullName(); ?>" />
                    <?php endif; ?>
                </a>
            </div>
            <div class="message-content">
                <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $jobinvite->getJob()->getHash())) ?>">
                    <?php echo $accountInvite->getFullName(); ?> <?php echo $view['translator']->trans('invite.popup.freelance_invite', array(), 'vlance') ?>
                    <strong><?php echo $jobinvite->getJob()->getTitle(); ?></strong>
                </a>
                <div class="time"><?php echo $jobinvite->getCreatedAt()->format('d/m/Y, H:i'); ?></div>
            </div>

        </li>
        <?php endforeach; ?>
    </ul>

    
    </div>
</div>
<script type="text/javascript">
    jQuery(function($) {
        $('#block_notify').modal('show');
    });
</script>