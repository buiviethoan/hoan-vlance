<?php $current_route = $app->getRequest()->attributes->get('_route'); ?>

<div class="admin-menu-wrapper">
    <ul class="nav nav-tabs" id="job-admin-dashboard">
        <li><a href="#display" data-toggle="tab" class="active">Hiển thị</a></li>
        <li><a href="#status" data-toggle="tab">Trạng thái làm việc</a></li>
        <li><a href="#bid-freelancer" data-toggle="tab">Chào giá & freelancer</a></li>
        <li><a href="#message" data-toggle="tab">Tin nhắn</a></li>
        <li><a href="#payment" data-toggle="tab">Giao dịch tiền</a></li>
        <li><a href="#feedback" data-toggle="tab">Đánh giá</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="display">
            <div class="accordion" id="accordion-display">
                <?php echo $view->render('VlanceJobBundle:Admin/dashboard/display:common_action.html.php', array('job' => $job)) ?>
                <?php echo $view['actions']->render($view['router']->generate('job_deactive_admin', array('job_id' => $job->getId())));?>
            </div>
        </div>
        <div class="tab-pane" id="status">
            <div class="accordion" id="accordion-display">
                <?php echo $view->render('VlanceJobBundle:Admin/dashboard/status:cancel_award.html.php', array('job' => $job)) ?>
            </div>
            <div class="accordion" id="accordion-display">
                <?php echo $view->render('VlanceJobBundle:Admin/dashboard/status:admin_action.html.php', array('job' => $job)) ?>
            </div>
        </div>
        <div class="tab-pane" id="bid-freelancer"></div>
        <div class="tab-pane" id="message"></div>
        <div class="tab-pane" id="payment">
            <div class="accordion" id="accordion-payment">
                <?php echo $view->render('VlanceJobBundle:Admin/dashboard/payment:list_transaction.html.php', array('job' => $job, 'transaction' => $transaction)) ?>
            </div>
        </div>
        <div class="tab-pane" id="feedback"></div>
    </div>

    <script>
        $(function () {
            $('#job-admin-dashboard a:first').tab('show');
        })
    </script>
</div>