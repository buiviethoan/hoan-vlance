<?php use Vlance\JobBundle\Entity\Job; ?>
<?php /* @var $job Vlance\JobBundle\Entity\Job */ ?>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#status--admin-action">
            Thao tác cơ bản
        </a>
    </div>
    <div id="status--admin-action" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span6">
                    <div class="row-fluid">
                        Trạng thái dự án: <b><?php 
                                        if($job->getStatus() === Job::JOB_OPEN){
                                            if($job->getCancelStatus() === Job::STATUS_CANCEL_NONE){
                                                echo "Đang chờ chào giá";
                                            }elseif($job->getCancelStatus() === Job::STATUS_CANCELLED_REWORK){
                                                echo "Đang chờ chào giá (REWORK)";
                                            }
                                        }elseif($job->getStatus() === Job::JOB_AWARDED){
                                            echo "Đã chọn được FL";
                                        }elseif($job->getStatus() === Job::JOB_WORKING){
                                            echo "Đang thực hiện";
                                        }elseif($job->getStatus() === Job::JOB_CLOSED){
                                            echo "Đã đóng";
                                        }elseif($job->getStatus() === Job::JOB_FINISH_REQUEST){
                                            echo "Yêu cầu kết thúc";
                                        }elseif($job->getStatus() === Job::JOB_FINISHED){
                                            if($job->getCancelStatus() === Job::STATUS_CANCEL_NONE){
                                                echo "Đã hoàn thành";
                                            }elseif($job->getCancelStatus() === Job::STATUS_CANCELLED){
                                                echo "Đã hủy";
                                            }elseif($job->getCancelStatus() === Job::STATUS_CANCELLED_DONE_SUCCESS){
                                                echo "Đã hoàn thành.(Từng hủy)";
                                            }
                                        }
                                    ?></b>
                    </div>
                    <div class="row-fluid">
                        <a href="<?php echo $view['router']->generate('admin_job_return_open', array('job_id' => $job->getId())) ?>" class="btn" <?php if($job->getStatus() !== Job::JOB_AWARDED){echo 'disabled';}?>>
                            Mở chào giá dự án
                        </a>
                        <a href="<?php echo $view['router']->generate('admin_job_close', array('job_id' => $job->getId())) ?>" class="btn" <?php if($job->getStatus() !== Job::JOB_OPEN){echo 'disabled';}?>>
                            Đóng dự án
                        </a>
                        <a href="#cancel-refund-popup" data-toggle="modal" class="btn" <?php if($job->getStatus() !== Job::JOB_WORKING && $job->getStatus() !== Job::JOB_FINISH_REQUEST){echo 'disabled';}?>>
                            Hủy dự án
                        </a>
<!--                        <a href="#finish-popup" data-toggle="modal" class="btn" <?php if($job->getStatus() !== Job::JOB_WORKING 
                                                                                        || ($job->getStatus() == Job::JOB_WORKING && $job->getCancelStatus() == Job::STATUS_CANCEL_REQUEST)
                                                                                        || $job->getStatus() == Job::JOB_FINISHED){echo 'disabled';}?>>
                            Kết thúc dự án
                        </a>-->
                    </div>
                </div>
                <div class="span6">
                    <div class="row-fluid"><h1>Hướng dẫn Admin nút <span style="color: #28ec1d;">Active</span> khi:</h1></div>
                    <div class="row-fluid">
                        <div class="span4">Mở chào giá dự án</div>
                        <div class="span8">Trường hợp dự án <b>đã chọn được Freelancer</b> (JOB_AWARDED)</div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Đóng dự án</div>
                        <div class="span8">Trường hợp dự án đang <b>nhận chào giá</b> (JOB_OPEN)</div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">Hủy dự án</div>
                        <div class="span8">Trường hợp dự án đang <b>thực hiện</b> (JOB_OPEN) hoặc đang <b>yêu cầu hoàn thành</b> (JOB_REQUEST_FINISHED)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="cancel-refund-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Hủy dự án <b>[<?php echo $job->getId(); ?>]</b></h3>
    </div>
    <div class="modal-body">
        <form id="cancel-refund-form" action="<?php echo $view['router']->generate('admin_job_cancel', array('job_id' => $job->getId())) ?>" method="POST">
            <div class="row-fluid">
                <label for="reasonCancel" style="font-size: 16px;">Lý do:</label>
                <textarea style="width: 96%;" id="reasonCancel" name="reasonCancel" placeholder="Lý do hủy dự án" required autofocus></textarea>
            </div>
            <div class="row-fluid" style="margin-top: 15px; margin-bottom: 5px;">
                <div class="span7">
                    <div class="row-fluid" style="font-size: 16px; margin-bottom: 5px;">Hình thức hủy:</div>
                    <div class="row-fluid">
                        <label>
                            <input style="margin-top: -2px; margin-right: 5px;" type="radio" name="type_cancel" value="refund" checked>Hoàn toàn bộ tiền cho KH
                        </label>
                    </div>
                    <div class="row-fluid">
                        <label>
                            <input style="margin-top: -2px; margin-right: 5px;" type="radio" name="type_cancel" value="pay_apart">Trả 1 phần tiền cho FL
                        </label>
                    </div>
                    <div class="row-fluid">
                        <label>
                            <input style="margin-top: -2px; margin-right: 5px;" type="radio" name="type_cancel" value="pay_apart_bid">Trả 1 phần tiền FL + nhận chào giá
                        </label>
                    </div>
                </div>
                <div id="block-refund" class="span5" style="display: none">
                    <div class="row-fluid">
                        <label for="money_pay">Số tiền trả FL:</label>
                        <div class="input-append">
                            <input style="width: 65%;" type="text" id="money_pay" name="money_pay" value="0"/>
                            <span style="padding: 10px;" class="add-on">VNĐ</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <label style="width: 100px;">
                    <input style="margin-top: -2px; margin-right: 5px;" type="checkbox" name="sendEmail" /> Gửi Email
                </label>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
        <button id="btn-confirm" class="btn btn-primary">Xác nhận</button>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#cancel-refund-form').on('change', function(){
            if($('input[name="type_cancel"]:checked').val() !== 'refund'){
                $('#block-refund').slideDown();
            } else {
                $('#block-refund').slideUp();
            }
        });
        $('#btn-confirm').on('click', function(){
            $('#cancel-refund-form').submit();
        });
        $('#money_pay').on('keypress input', function(event){
            return event.charCode >= 48 && event.charCode <= 57;
        });
    });
</script>


<!--<div id="finish-popup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Kết thúc dự án <b>[<?php // echo $job->getId(); ?>]</b></h3>
    </div>
    <div class="modal-body">
        <form id="finish-refund-form" action="<?php // echo $view['router']->generate('admin_job_cancel', array('job_id' => $job->getId())) ?>" method="POST">
            <div class="row-fluid" style="margin-top: 15px; margin-bottom: 5px;">
                <div class="span7">
                    <div class="row-fluid" style="font-size: 16px; margin-bottom: 5px;">Hình thức kết thúc:</div>
                    <div class="row-fluid">
                        <label>
                            <input style="margin-top: -2px; margin-right: 5px;" type="radio" name="type_finish" value="refund" checked>Trả toàn bộ tiền cho FL
                        </label>
                    </div>
                    <div class="row-fluid">
                        <label>
                            <input style="margin-top: -2px; margin-right: 5px;" type="radio" name="type_finish" value="pay_apart">Trả 1 phần tiền cho FL
                        </label>
                    </div>
                </div>
                <div id="block-finish-refund" class="span5" style="display: none">
                    <div class="row-fluid">
                        <label for="finish_money_pay">Số tiền trả FL:</label>
                        <div class="input-append">
                            <input style="width: 65%;" type="text" id="finish_money_pay" name="finish_money_pay" value="0"/>
                            <span style="padding: 10px;" class="add-on">VNĐ</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <label style="width: 100px;">
                    <input style="margin-top: -2px; margin-right: 5px;" type="checkbox" name="sendEmail" /> Gửi Email
                </label>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
        <button id="btn-confirm-finish" class="btn btn-primary">Xác nhận</button>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#finish-refund-form').on('change', function(){
            if($('input[name="type_finish"]:checked').val() !== 'refund'){
                $('#block-finish-refund').slideDown();
            } else {
                $('#block-finish-refund').slideUp();
            }
        });
        $('#btn-confirm-finish').on('click', function(){
            $('#finish-refund-form').submit();
        });
        $('#finish_money_pay').on('keypress input', function(event){
            return event.charCode >= 48 && event.charCode <= 57;
        });
    });
</script>-->