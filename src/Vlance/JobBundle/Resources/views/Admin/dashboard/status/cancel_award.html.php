<?php /* @var $job Vlance\JobBundle\Entity\Job */ ?>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#status--cancel-award">
            Các link thường dùng
        </a>
    </div>
    <div id="status--cancel-award" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid">
                <p>Thông tin giao dịch tiền liên quan đến dự án:</p>
                <ul>
                    <li>Tổng số tiền đã nạp vào dự án:
                        <b class="<?php echo $job->getFundedAmount() != 0 ? 'text-error' : ''?>">
                            <?php echo number_format($job->getFundedAmount(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                        </b>
                        <?php if($job->getFundedAmount() != 0): ?>
                            DC <?php echo number_format($job->getFundedAmountDc(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                            + VC <?php echo number_format($job->getFundedAmountVc(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                        <?php endif; ?>
                    </li>
                    <li>Số dư Jobcash hiện tại: 
                        <b class="<?php echo $job->getCash()->getBalance() <> 0 ? 'text-error' : ''?>">
                            <?php echo number_format($job->getCash()->getBalance(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                        </b>
                    </li>
                    <li>Số dư Usercash của chủ dự án: 
                        <b class="<?php echo $job->getAccount()->getCash()->getBalance() <> 0 ? 'text-error' : ''?>">
                            <?php echo number_format($job->getAccount()->getCash()->getBalance(),'0',',','.')?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>
                        </b>
                    </li>
                </ul>
                <!-- Hủy chọn freelancer khi chưa nạp tiền -->
                <a
                    <?php if($job->isAwarded()): ?>
                        title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>"
                        href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_object',array('pk' => $job->getId(), "action" => "reopen")) ?>" onclick="return confirm('Bạn chắn chắn muốn bỏ chọn Freelancer chứ?')"
                    <?php else: ?>
                        disabled title="Không sử dụng được trong trường hợp này"
                    <?php endif; ?>
                    class="btn">
                        Hủy chọn freelancer <em>(chưa nạp tiền)</em>
                </a>
            </div>
        </div>
    </div>
</div>