<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#display--hide-job">
            Ẩn việc không phù hợp <?php if($job->getPublish() == false): ?> <span class="label-tag label-danger">Việc đã bị ẩn</span> <?php endif; ?>
        </a>
    </div>
    <div id="display--hide-job" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <?php if($job->getPublish() == true): ?>
                    <form action="<?php echo $confirm_url?>" method="post" class="admin_form">
                        <input type="hidden" name="_csrf_token" value="<?php echo $_csrf_token;?>"/>
                        <div class="control-group">
                            <div class="controls">
                                <label for="reason" class="control-label">Lý do ẩn job</label>
                                <select name="reason" required="required">
                                    <option value="contact_info"><?php echo $view['translator']->trans('admin.job.deactive.reason.contact_info', array(), 'vlance');?></option>
                                    <option value="freelancer"><?php echo $view['translator']->trans('admin.job.deactive.reason.freelancer', array(), 'vlance');?></option>
                                    <option value="duplication"><?php echo $view['translator']->trans('admin.job.deactive.reason.duplication', array(), 'vlance');?></option>
                                    <option value="publication"><?php echo $view['translator']->trans('admin.job.deactive.reason.publication', array(), 'vlance');?></option>
                                    <option value="spam"><?php echo $view['translator']->trans('admin.job.deactive.reason.spam', array(), 'vlance');?></option>
                                    <option value="unclear"><?php echo $view['translator']->trans('admin.job.deactive.reason.unclear', array(), 'vlance');?></option>
                                    <option disabled>-----</option>
                                    <option value="content"><?php echo $view['translator']->trans('admin.job.deactive.reason.content', array(), 'vlance');?></option>
                                </select>
                            </div>
                            <div class="controls">
                                <label class="checkbox"><input type="checkbox" name="banned" value="1"/> <?php echo $view['translator']->trans('admin.job.deactive.label.banned', array(), 'vlance');?></label>
                            </div>
                            <div class="controls">
                                <button type="submit" class="btn">
                                    <i class="icon-remove"></i> <?php echo $view['translator']->trans('admin.job.deactive.button.confirm', array(), 'vlance');?>
                                </button>
                            </div>
                        </div>
                    </form>
                <?php else: ?>
                    <em>Việc đã ẩn rồi mà, không ẩn thêm lần nữa được đâu.</em>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>