<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#display--common-actions">
            Các link thường dùng
        </a>
    </div>
    <div id="display--common-actions" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid">
                <a class="btn" title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>"
                   href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $job->getId())) ?>" target="_blank">
                        <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?> <em>(Admin view)</em>
                </a>
                <a class="btn" title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>"
                   href="<?php echo $view['router']->generate('job_edit',array('id' => $job->getId())) ?>" >
                        <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?> <em>(User view)</em>
                </a>
            </div>
        </div>
    </div>
</div>