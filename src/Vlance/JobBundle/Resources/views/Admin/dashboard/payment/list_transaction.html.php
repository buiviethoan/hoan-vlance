<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-payment" href="#payment--transaction">
            Danh sách giao dịch dự án
        </a>
    </div>
    <div id="payment--transaction" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Loại</th>
                                <th>Trạng thái</th>
                                <th class="text-right">Số tiền</th>
                                <th>Hình thức TT</th>
                                <th>Ngày</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transaction as $t): ?>
                            <tr>
                                <td><?php echo $t->getId();?></td>
                                <td><?php echo $t->getTypeText(); ?></td>
                                <td><?php echo $t->getStatusText(); ?></td>
                                <td class="text-right"><?php echo number_format($t->getAmount(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance')?></td>
                                <td><?php
                                    if(!is_null($t->getPaymentMethod())){
                                        echo $t->getPaymentMethod()->getName();
                                    } else {
                                        echo "NULL";
                                    }
                                ?></td>
                                <td><?php echo date_format($t->getCreatedAt(), "d/m/Y h:i:s")?></td>
                                <td>
                                    <a class="btn" href="<?php echo $view['router']->generate('Vlance_PaymentBundle_AdminTransaction_edit',array('pk' => $t->getId())) ?>" target="_blank">
                                        Chỉnh sửa
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>