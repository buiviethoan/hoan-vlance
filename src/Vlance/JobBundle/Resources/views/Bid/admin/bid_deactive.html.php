<!--<form action="<?php echo $confirm_url?>" method="post" class="admin_form">
    <input type="hidden" name="_csrf_token" value="<?php echo $_csrf_token;?>"/>
    <div class="form-actions" style="float: right; margin: 0px; width: 265px; padding: 10px">
        <label for="banned" style="float: left; margin: 0px 5px"><?php echo $view['translator']->trans('admin.bid.deactive.label.banned', array(), 'vlance');?></label>
        <input type="checkbox" name="banned" value="1" style="margin: 4px 5px; float: left"/>
        <button type="submit" class="btn" style="float: left; margin: 0px 5px">
            <i class="icon-remove"></i><?php echo $view['translator']->trans('admin.bid.deactive.button.confirm', array(), 'vlance');?>
        </button>
    </div>
</form>-->

<div class="admin_form">
    <div class="form-actions" style="float: right; margin: 0px; width: inherit; padding: 10px; text-align: center">
        <a class="btn form-hire-bid" style="float: left; margin: 0px 5px" 
           data-action="<?php echo $confirm_url?>" data-token="<?php echo $_csrf_token;?>"
           href="#form_hire_bid" role="button" data-toggle="modal">
            <i class="icon-remove"></i><?php echo $view['translator']->trans('admin.bid.deactive.button.confirm', array(), 'vlance');?>
        </a>
    </div>
</div>
