<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $view['translator']->trans('new_job_page.title', array(), 'vlance'));?>
<?php $view['slots']->start('content')?>

<?php /* create-service-job page */?>
<div class="workdii-create-job">
    <div class="content-section container create-service-job">
        <div class="container">
            <div class="row-fluid">
                <div class="whitebase">
                    <div class="row-fluid service-request-top">
                        <div class="span12">
                            <h3>Dọn dẹp nhà cửa</h3>
                        </div>
                    </div> 
                    <div class="row-fluid">
                        <div class="span5">
                            <div class="service-request-sidebar">
                                <ul class="list-unstyled request-steps">
                                    <li>
                                        <h4>
                                            <i class="fa fa-pencil-square-o"></i>Bước 1
                                        </h4>
                                        <p>Bạn sẽ trả lời mẫu câu hỏi đơn giản</p>
                                    </li>
                                    <li>
                                        <h4>
                                            <i class="fa fa-list-ul"></i>Bước 2
                                        </h4>
                                        <p>5 báo giá sẽ được gửi đến bạn. Hãy so sánh để đưa ra quyết định.</p>
                                    </li>
                                    <li>
                                        <h4>
                                            <i class="fa fa-check-square-o"></i>Bước 3
                                        </h4>
                                        <p>Chọn người bạn thích nhất</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="service-request-form">
                                <div class="form-group">
                                    <label>Bao lâu bạn dọn dẹp nhà cửa?</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                            Dọn dẹp đơn giản mỗi ngày
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
                                            Ngày trong tuần
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                            Ngày trong tháng
                                        </label>
                                    </div>    
                                    <div class="input-prepend input-service-requests-small">
                                        <span class="add-on">
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                        </span>
                                        <input class="form-control" id="prependedInput" type="text" placeholder="Khác">
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label>Loại nhà bạn muốn được dọn dẹp?</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                            Nhà cấp 4
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
                                            Chung cư
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                            Văn phòng
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                            Biệt thự
                                        </label> 
                                    </div>    
                                    <div class="input-prepend">
                                        <span class="add-on">
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                        </span>
                                        <input class="form-control" id="prependedInput" type="text" placeholder="Khác">
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label>Kích thước ngôi nhà của bạn? (tính theo mét vuông)</label>
                                    <input type="form-control" id="service_request_house_info" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Dọn dẹp nhà kỹ càng hơn sẽ cần chuẩn bị thêm đồ nghề, bạn có muốn?</label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">Có
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">Không
                                        </label>
                                    </div>
                                </div>    
                                <div class="form-group">
                                    <label>Bạn có nuôi động vật?</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="inlineCheckbox1" value="option1">Không
                                        </label>
                                    </div>    
                                    <div class="checkbox">    
                                        <label>
                                            <input type="checkbox" id="inlineCheckbox2" value="option2">Chó
                                        </label>
                                    </div> 
                                    <div class="checkbox"> 
                                        <label>
                                            <input type="checkbox" id="inlineCheckbox3" value="option3">Mèo
                                        </label>
                                    </div>    
                                    <div class="input-prepend">
                                        <span class="add-on">
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option3">
                                        </span>
                                        <input class="form-control" id="service_request_pets" type="text" placeholder="Khác">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Viết mô tả cho dịch vụ (tùy chọn)</label>   
                                    <p>Điều này sẽ giúp cho báo giá được chính xác hơn</p>
                                    <textarea class="form-control" id="service_request_other_info"></textarea>
                                </div>  
                                <div class="form-group">
                                    <label>Bạn khi nào muốn bắt đầu?</label> 
                                    <div class="datepicker-widget input-group">
                                        <span class="input-group-addon">
                                        <input type="radio" name="optionsRadios" id="specific_requested_time_radio" value="option4">
                                        </span>
                                        <input class="form-control form-datetime" id="service_request_specific_requested_time" type="text" placeholder="Bấm để chọn ngày">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                    <script type="text/javascript">$(document).ready(function(){$('#service_request_specific_requested_time').datepicker({format:'dd/mm/yyyy',language:'vn',autoclose:true,weekStart:1,startDate:'',endDate:''});});</script>
                                    <br>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="radio" name="optionsRadios" id="service_request_other_requested_time" value="option5">
                                        </span>
                                        <input class="form-control" id="service_request_other_requested_time" type="text" placeholder="Bạn chưa được rõ để lại thời gian gần nhất">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Nơi bạn cần dịch vụ?</label> 
                                    <select class="form-control big"></select>
                                    <p>VD: Hà Nội, Tp Hồ Chí Minh...</p>
                                </div>  
                                <div class="form-group">
                                    <label>Đính kèm</label> 
                                    <p>Thêm hình ảnh
                                       <br>
                                       Hỗ trợ các định dạng (jpg, png...) 
                                    </p>
                                    <button class="btn btn-blue add-file">Chọn</button>
                                </div>
                                <div class="form-group">
                                    <label>Thông tin liên lạc của bạn</label> 
                                    <div class="contact-group name-group">
                                        <label>Tên</label>
                                        <input class="form-control" id="service_request_name" type="text">
                                    </div>
                                    <div class="contact-group email-group">
                                        <label>Địa chỉ email</label>
                                        <input class="form-control" id="service_request_email" type="text">
                                    </div>
                                    <div class="contact-group phone-group">
                                        <label>Số điện thoại</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">+84</span>
                                            <input class="form-control" id="service_request_phone" type="text">
                                        </div>
                                    </div>
                                    <p>Khi bạn đang dịch vụ trên vLance, thông tin liên lạc của bạn sẽ không được tiết lộ cho người làm dịch vụ hay bên thứ 3 bất kỳ.</p>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-large btn-block btn-primary btn-submit">Nhận báo giá</button>
                                    <br>
                                    <p>Khi bấm tạo tài khoản đồng nghĩa rằng bạn đồng ý với <a href="#" target="_blank">điều khoản của vLance</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php /* end create-job page */?>
<?php $view['slots']->stop();?>
