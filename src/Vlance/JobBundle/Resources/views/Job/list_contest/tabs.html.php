<div class="header-list-job">
    <div class="container">
        <div class="tabbable menu-desktop">
            <ul class="nav nav-tabs">
                <li class="<?php echo $current_route == "freelance_job_list" ? "active" : ""?>">
                    <a class="nav-item-job project-short" href="<?php echo $view['router']->generate('freelance_job_list', array()) ?>" title="Việc làm freelance">
                        <i class="icon"></i>
                        Việc freelance ngắn hạn
                    </a>
                </li>
                <li class="<?php echo $current_route == "contest_list" ? "active" : ""?>">
                    <a class="nav-item-job item-contest" href="<?php echo $view['router']->generate('contest_list', array()) ?>" title="Cuộc thi thiết kế">
                        <i class="icon"></i>
                        Cuộc thi thiết kế
                    </a>
                </li>
                <li class="<?php echo $current_route == "onsite_list" ? "active" : ""?>">
                    <a class="nav-item-job item-onsite" href="<?php echo $view['router']->generate('onsite_list', array()) ?>" title="Việc on-site"><i class="icon"></i>Việc on-site</a>
                </li>
                <li>
                    <a class="nav-item-job item-tn" href="https://www.thuengay.vn?ref=vlance-tab" title="Thuê dịch vụ nhanh - Thuengay.vn">
                        <i class="icon"></i>
                        Dịch vụ trọn gói<br/><b>thuengay.vn</b>
                    </a>
                </li>
                <li>
                    <a class="nav-item-job item-sign" href="http://thuengay.vn/cong-cu/tao-chu-ky-email-chuyen-nghiep?ref=vlance_list_job" title="Tạo chữ ký email" onclick="vtrack('Page Signature', {'location':'vllistjob'})">
                        <i class="icon"></i>
                        Tạo chữ ký email
                    </a>
                </li>
            </ul>
        </div>
        <div class="menu-hamburger">
            <ul class="nav nav-pills nav-tabs">
                <li class="dropdown active">
                    <?php if($current_route == "contest_list"): ?>
                        <a class="dropdown-toggle nav-item-job item-contest tf200" data-toggle="dropdown" href="#">
                            <i class="icon"></i>
                            Cuộc thi thiết kế
                            <i class="icon-hamburger fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="nav-item-job project-short" href="<?php echo $view['router']->generate('freelance_job_list', array()) ?>" title="Việc làm freelance">
                                    <i class="icon"></i>
                                    Việc freelance ngắn hạn
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-onsite" href="<?php echo $view['router']->generate('onsite_list', array()) ?>" title="Việc onsite">
                                    <i class="icon"></i>
                                    Việc onsite
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-tn" href="https://www.thuengay.vn?ref=vlance-tab" title="Thuê dịch vụ nhanh - Thuengay.vn">
                                    <div class="item-tn-wrapper"><i class="icon"></i>Dịch vụ trọn gói<br/><b>thuengay.vn</b></div>
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-sign" href="http://thuengay.vn/cong-cu/tao-chu-ky-email-chuyen-nghiep?ref=vlance_list_job" title="Tạo chữ ký email" onclick="vtrack('Page Signature', {'location':'vllistjob'})">
                                    <i class="icon"></i>
                                    Tạo chữ ký email
                                </a>
                            </li>
                        </ul>
                    <?php elseif($current_route == "onsite_list"): ?>
                        <a class="dropdown-toggle nav-item-job item-onsite tf200" data-toggle="dropdown" href="#">
                            <i class="icon"></i>
                            Việc onsite
                            <i class="icon-hamburger fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="nav-item-job project-short" href="<?php echo $view['router']->generate('freelance_job_list', array()) ?>" title="Việc làm freelance">
                                    <i class="icon"></i>
                                    Việc freelance ngắn hạn
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-contest" href="<?php echo $view['router']->generate('contest_list', array()) ?>" title="Cuộc thi thiết kế">
                                    <i class="icon"></i>
                                    Cuộc thi thiết kế
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-tn" href="https://www.thuengay.vn?ref=vlance-tab" title="Thuê dịch vụ nhanh - Thuengay.vn">
                                    <div class="item-tn-wrapper"><i class="icon"></i>Dịch vụ trọn gói<br/><b>thuengay.vn</b></div>
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-sign" href="http://thuengay.vn/cong-cu/tao-chu-ky-email-chuyen-nghiep?ref=vlance_list_job" title="Tạo chữ ký email" onclick="vtrack('Page Signature', {'location':'vllistjob'})">
                                    <i class="icon"></i>
                                    Tạo chữ ký email
                                </a>
                            </li>
                        </ul>
                    <?php else: ?>
                        <a class="dropdown-toggle nav-item-job project-short tf200" data-toggle="dropdown" href="#">
                            <i class="icon"></i>
                            Việc freelance ngắn hạn
                            <i class="icon-hamburger fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="nav-item-job item-onsite" href="<?php echo $view['router']->generate('onsite_list', array()) ?>" title="Việc onsite">
                                    <i class="icon"></i>
                                    Việc onsite
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-contest" href="<?php echo $view['router']->generate('contest_list', array()) ?>" title="Cuộc thi thiết kế">
                                    <i class="icon"></i>
                                    Cuộc thi thiết kế
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-tn" href="https://www.thuengay.vn?ref=vlance-tab-hambg" title="Thuê dịch vụ nhanh - Thuengay.vn">
                                    <div class="item-tn-wrapper"><i class="icon"></i>Dịch vụ trọn gói<br/><b>thuengay.vn</b></div>
                                </a>
                            </li>
                            <li>
                                <a class="nav-item-job item-sign" href="http://thuengay.vn/cong-cu/tao-chu-ky-email-chuyen-nghiep?ref=vlance_list_job" title="Tạo chữ ký email" onclick="vtrack('Page Signature', {'location':'vllistjob'})">
                                    <i class="icon"></i>
                                    Tạo chữ ký email
                                </a>
                            </li>
                        </ul>
                    <?php endif; ?>
                  </li>
            </ul>
        </div>
    </div>
</div>
<div class="head-title-list-job">
    <div class="container">
        <?php if($current_route == "contest_list"): ?>
            <h1><b>Cuộc thi thiết kế</b> - Giải thưởng cao cho người sáng tạo</h1>
            <p><?php if(isset($count_jobs)): ?><?php echo isset($count_jobs['contests']) ? "Hơn <b>".$count_jobs['contests'] . "</b>" : "Hàng trăm" ?><?php endif;?> cuộc thi thiết kế đã được triển khai thành công.<br/><b>100%</b> cuộc thi đã được nạp sẵn tiền thưởng. Người chiến thắng được toàn bộ giá trị giải thưởng.</p>
        <?php elseif($current_route == "onsite_list"): ?>
            <h1><b>Việc onsite</b> - Lương cao, hấp dẫn</h1>
            <p>Hàng trăm việc làm onsite, lương lên đến <b>50 triệu đồng / tháng</b>.<br/>Ký hợp đồng và nhận thanh toán trực tiếp với công ty đăng tuyển. Dành riêng cho freelancer có từ 3 năm kinh nghiệm trở lên.</p>
        <?php else: ?>
            <h1><b>Việc freelance</b> - Làm online, nhận tiền nhanh chóng</h1>
            <p><?php if(isset($count_jobs)): ?><?php echo isset($count_jobs['jobs']) ? "Hơn <b>".$count_jobs['jobs'] . "</b>" : "Hàng nghìn" ?><?php endif;?></b> dự án đã đăng và thuê được freelancer thành công.<br/>Được bảo đảm thanh toán với những <b>dự án được nạp tiền</b> qua vLance.vn.</p>
        <?php endif; ?>
    </div>
</div>