
<?php if($type == 'client') : ?>
    <option value="<?php echo $view['router']->generate('jobs_create_project',array(),true) ?>"><?php echo $view['translator']->trans('common.all', array(), 'vlance') ?></option>
    <?php 
        foreach($entities as $status) { ?>
            <option  <?php if($params == 'j.status-'.$status['status']){ echo "selected='selected'"; } ?>
                value="<?php echo $view['router']->generate('jobs_create_project',array('filters' => 'j.status-'.$status['status']), true)?> ">
                <?php echo Vlance\BaseBundle\Utils\GetStatus::getStatusJob($status['status']) ?>
            </option>
        <?php }
    ?>
<?php elseif($type == 'freelancer') : ?>
    <option value="<?php echo $view['router']->generate('jobs_bid_project',array(),true) ?>"><?php echo $view['translator']->trans('common.all', array(), 'vlance') ?></option>
    <?php 
        foreach($entities as $status) { ?>
            <option  <?php if($params == 'j.status-'.$status['status']){ echo "selected='selected'"; } ?>
                value="<?php echo $view['router']->generate('jobs_bid_project',array('filters' => 'j.status-'.$status['status']), true)?> ">
                <?php echo Vlance\BaseBundle\Utils\GetStatus::getStatusJob($status['status']) ?>
            </option>
        <?php
        }
    ?>
<?php endif; ?>
