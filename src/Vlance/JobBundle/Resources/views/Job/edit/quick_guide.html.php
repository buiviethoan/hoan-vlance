<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs " style="display: none;">
        <li class="active"><a href="#tab_default" class="tab_default" data-toggle="tab">Section 1</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_category" class="tab_vlance_jobbundle_jobtype_category" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_title" class="tab_vlance_jobbundle_jobtype_title" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_description" class="tab_vlance_jobbundle_jobtype_description" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_city" class="tab_vlance_jobbundle_jobtype_city" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_budget" class="tab_vlance_jobbundle_jobtype_budget" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_closeAt" class="tab_vlance_jobbundle_jobtype_closeAt" data-toggle="tab">Section 2</a></li>
        <li><a href="#tab_vlance_jobbundle_jobtype_skill" class="tab_vlance_jobbundle_jobtype_skill" data-toggle="tab">Section 2</a></li>
    </ul>
    <div class="tab-content">
        <!--mặc định-->
        <div class="tab-pane active" id="tab_default">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.default.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.default.guide_p1', array(), 'vlance') ?> 
                    <br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.default.guide_p2', array(), 'vlance') ?> <a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'cau-hoi-thuong-gap'));?>"><?php echo $view['translator']->trans('new_job_page.content_right.default.guide_p2_link', array(), 'vlance') ?></a>.
                </p>
                <div class="small-pic">
                    <span><img width="81" height="81" src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_1.jpg') ?>" alt="" /></span>
                    <span><img width="81" height="81" src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_cs_2.jpg') ?>" alt="" /></span>
                    <span><img width="81" height="81" src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_cs_3.jpg') ?>" alt="" /></span>
                    <span><img width="81" height="81" src="<?php echo $view['assets']->getUrl('img/icon/icon_dv_cs_4.jpg') ?>" alt="" /></span>
                </div>
            </div>
        </div>

        <!--category-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_category">
          <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.category_guide.heading', array(), 'vlance') ?></h3>
          <div class="text-content">
              <p>
                  <?php echo $view['translator']->trans('new_job_page.content_right.category_guide.guide_info', array(), 'vlance') ?>
              </p>
          </div>
        </div>

        <!--Tên công việc-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_title">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.title_guide.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p1', array(), 'vlance') ?>
                </p>
                <i>
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p2', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p2_info1', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p2_info2', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p2_info3', array(), 'vlance') ?><br>

                </p>
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p3', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p3_info1', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p3_info2', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.title_guide.guide_p3_info3', array(), 'vlance') ?><br>

                </p>
                </i>

            </div>
        </div>

        <!--Mô tả công việc-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_description">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p1', array(), 'vlance') ?>
                </p>

                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p2', array(), 'vlance') ?>:<br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p2_info1', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p2_info2', array(), 'vlance') ?><br>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p2_info3', array(), 'vlance') ?><br>
                </p>
                <i>
                    <p>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p3', array(), 'vlance') ?><br>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_type_description.guide_p3_info', array(), 'vlance') ?>
                    </p>
                </i>
            </div>
        </div>


        <!--vị trí freelance-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_city">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.job_city.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_city.guide_p', array(), 'vlance') ?>
                </p>
            </div>
        </div>

        <!--Thù lao-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_budget">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.job_budget.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_budget.guide_p', array(), 'vlance') ?>
                </p>
            </div>
        </div>


        <!--hạn nhận cào giá-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_closeAt">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.job_close_at.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_close_at.guide_p1', array(), 'vlance') ?>
                </p>
                <i>
                    <p>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_close_at.guide_p2', array(), 'vlance') ?><br>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_close_at.guide_p2_info', array(), 'vlance') ?>
                    </p>
                </i>
            </div>
        </div>

        <!--thêm kĩ năng-->
        <div class="tab-pane" id="tab_vlance_jobbundle_jobtype_skill">
            <h3 class="bottom-line-grey"><?php echo $view['translator']->trans('new_job_page.content_right.job_skill.heading', array(), 'vlance') ?></h3>
            <div class="text-content">
                <p>
                    <?php echo $view['translator']->trans('new_job_page.content_right.job_skill.guide_p1', array(), 'vlance') ?>
                </p>
                <i>
                    <p>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_skill.guide_p2', array(), 'vlance') ?><br>
                        <?php echo $view['translator']->trans('new_job_page.content_right.job_skill.guide_p2_info', array(), 'vlance') ?>
                    </p>
                </i>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $( "#edit-job-form input, #edit-job-form select, #edit-job-form textarea" ).focus(function() {
            var id = $(this).attr('id');
            $(".quick-guide .nav-tabs a.tab_"+id).click();
        });
    });
</script>