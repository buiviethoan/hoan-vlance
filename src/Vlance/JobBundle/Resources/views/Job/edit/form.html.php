<div id="vlance_jobbundle_jobtype">
    <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <div class="sprites-job-posting sprites-1"></div>
            </div>
            <div class="span10">
                <h4>Công việc bạn cần làm?</h4>
                <?php echo $view['form']->row($form['category']); ?>
            </div>
        </div>
    </div>
    
    <?php // Chon loai hinh cong viec ?>
    <?php echo $view['form']->row($form['service']); ?>
    <script type="text/javascript">
        var catser = "<?php echo $view['router']->generate('job_cat_ser', array()) ?>";
    </script>
    <div class="control-group">
        <div class="row-fluid">
            <div class="span10 offset2">
                <?php echo $view['form']->row($form['title']); ?>
            </div>
        </div>
    </div>
    <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <div class="sprites-job-posting sprites-2"></div>
            </div>
            <div class="span10">
                <h4>Nói với freelancer về công việc</h4>
                <?php echo $view['form']->row($form['description']); ?>
            </div>
        </div>
    </div>
    <div class="control-group">
        <div class="row-fluid">
            <div class="span10 offset2">
                <?php echo $view['form']->row($form['files']); ?>
            </div>
        </div>
    </div>

    <?php // Skills ?>
    <div class="control-group">
        <div class="row-fluid">
            <div class="span10 offset2">
                <label><?php echo $view['translator']->trans('form.job.skills.label', array(), 'vlance') ?></label>
                <div class="form-add-skill">    
                    <div class="inner-skill">
                        <input type="text" id="vlance_jobbundle_jobtype_skill" data-provide="typeahead" name="hiddenTagListA" placeholder="<?php echo $view['translator']->trans('form.job.skills.placeholder', array(), 'vlance') ?>" class="tm-input"/>
                        <script type="text/javascript">
                            <?php // Mảng skill ?>
                            var source = <?php echo $view['actions']->render($view['router']->generate('skill_list')) ?>;
                        </script>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <?php // Skills ?>
    
    <div class="control-group">
        <div class="row-fluid">
            <div class="span5 offset2 job-closed-at">
                <?php echo $view['form']->row($form['closeAt']); ?>
            </div> 
        </div>
    </div>
    <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <div class="sprites-job-posting sprites-3"></div>
            </div>
            <div class="span10">
                <h4>Yêu cầu freelancer</h4>
                <div class="row-fluid">
                    <div class="span6 job-fl-city">
                        <?php echo $view['form']->row($form['city']); ?>
                    </div>
                    <?php /* 
                    <?php // kinh nghiem lam viec ?>
                    <div class="span6 job-fl-experi">
                        <label>Kinh nghiệm làm việc</label>
                        <select></select>
                    </div> */?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <div class="sprites-job-posting sprites-4"></div>
            </div>
            <div class="span10">
                <h4>Ngân sách dự kiến</h4>
                <?php echo $view['form']->row($form['budget']); ?>
            </div>
        </div>
    </div>
    
    <?php /* <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <img src="/img/job/icon-contest-4.png" alt="Icon" title="Icon">
            </div>
            <div class="span10">
                <h4>Ngân sách dự kiến</h4>
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">Trả trọn gói</a></li>
                        <li><a href="#tab2" data-toggle="tab">Trả theo sản phẩm</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <p>Bạn có thể trả bao nhiêu cho ngân sách dự án?</p>
                            <div class="row-fluid">
                                <div class="span2">
                                    <p>Ngân sách</p>
                                </div>
                                <div class="span8">
                                    <select></select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    <p>Từ</p>
                                </div>
                                <div class="span8">
                                    <input type="text" placeholder="200.000">
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">
                                    <p>Đến</p>
                                </div>
                                <div class="span8 input-append">
                                    <input type="text" placeholder="450.000">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <p>Công việc yêu cầu bao nhiêu sản phẩm, thời gian?</p>
                            <div class="row-fluid">
                                <div class="span2">
                                    Số lượng
                                </div>
                                <div class="span2">
                                    <input>
                                </div>
                                <div class="span4">
                                    <select></select>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span2">Đơn giá</div>
                                <div class="span8">
                                    <input>
                                </div>
                            </div>
                            <div clas="separator">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> */ ?>
    
    <?php echo $view['form']->row($form['_token'])?>
</div>

<?php // Add more options ?>
<div class="more-options">
    <div class="control-group primary-type">
        <div class="row-fluid">
            <div class="span2">
                <div class="sprites-job-posting sprites-5"></div>
            </div>
            <div class="span10">
                <h4><?php echo $view['translator']->trans('form.job.other_option', array(), 'vlance'); ?> <i>(không bắt buộc)</i></h4>

                <?php // Private job checkbox ?>
                <?php echo $view['form']->errors($form['isPrivate']); ?>
                <label class="is-private checkbox">
                    <?php echo $view['form']->widget($form['isPrivate']); ?>
                    <p>
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        <?php echo $view['form']->label($form['isPrivate']); ?>
                    </p>
                </label>

                <?php // Require Escrow checkbox ?>
                <?php /*if($acc->getId() % 2 == 0): ?>
                <?php echo $view['form']->errors($form['requireEscrow']); ?>
                <label class="is-private checkbox" data-display-track="create-job-require-escrow">
                    <?php echo $view['form']->widget($form['requireEscrow']); ?>
                    <p>
                        <i class="fa fa-shield require-escrow"></i>
                        <?php echo $view['form']->label($form['requireEscrow']); ?>
                        <span class="label-tag label-new"><?php echo $view['translator']->trans('common.label_tag.beta_long', array(), 'vlance') ?></span>
                    </p>
                </label>
                <?php endif;*/ ?>
                <?php // END Require Escrow checkbox ?>
                <?php //<div class="pay-credit-contest">110 credit</div> ?>
            </div>
        </div>
    </div>
    
    <?php /* <div class="control-group ">
        <label><?php echo $view['translator']->trans('form.job.other_option', array(), 'vlance'); ?> <i>(không bắt buộc)</i></label>

        <?php // Private job checkbox ?>
        <?php echo $view['form']->errors($form['isPrivate']); ?>
        <label class="is-private checkbox">
            <?php echo $view['form']->widget($form['isPrivate']); ?>
            <p>
                <span class="label-tag label-large label-new"><i class='fa fa-lock'></i> <?php echo $view['translator']->trans('form.job.is_private.label', array(), 'vlance') ?></span>
                <?php echo $view['form']->label($form['isPrivate']); ?>
            </p>
        </label>

        <?php // Require Escrow checkbox ?>
        <?php /*if($acc->getId() % 2 == 0): ?>
        <?php echo $view['form']->errors($form['requireEscrow']); ?>
        <label class="is-private checkbox" data-display-track="create-job-require-escrow">
            <?php echo $view['form']->widget($form['requireEscrow']); ?>
            <p>
                <i class="fa fa-shield require-escrow"></i>
                <?php echo $view['form']->label($form['requireEscrow']); ?>
                <span class="label-tag label-new"><?php echo $view['translator']->trans('common.label_tag.beta_long', array(), 'vlance') ?></span>
            </p>
        </label>
        <?php endif;*/ ?>
        <?php // END Require Escrow checkbox ?>
    </div>
</div>
<div class="clear"></div>
<?php // END Add more options ?>