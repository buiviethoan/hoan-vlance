<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('edit_job_page.title_page', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>

<?php /* create-job page */ ?>
<div class="content-section container create-job">
    <div class="row-fluid">
        <div class="span12 left-side">
            <h1 class="edit_job_title"><?php echo $view['translator']->trans('edit_job_page.title', array(), 'vlance') ?></h1>
            <div class="post-job">
                <?php $view['form']->setTheme($edit_form, array('VlanceBaseBundle:Form')) ?>
                <form id="edit-job-form" action="<?php echo $view['router']->generate('job_edit_submit', array('id' => $entity->getId())) ?>" method="post" <?php echo $view['form']->enctype($edit_form) ?>>
                    <?php echo $view->render('VlanceJobBundle:Job/edit:form.html.php', array('form' => $edit_form, 'acc' => $acc)) ?>
                    
                    <?php $js_skill = array(); ?>
                    <?php $skills = $entity->getSkills(); ?>
                    <?php foreach($skills as $skill): ?>
                        <?php $js_skill[] = array($skill->getTitle(),$skill->getId()); ?>
                    <?php endforeach; ?>
                    <script type="text/javascript">
                        var prefilled_skills = <?php echo json_encode($js_skill) ?>;
                    </script>
                    
                    <?php // Preview Job before submit ?>
                    <div class="row-fluid">
                        <div class="span10 offset2">
                            <div class="form-cread-job-btn">
                                <div class="row-fluid">
                                    <div class="accept-agreement span12">
                                        <p><?php echo $view['translator']->trans('form.job.agree_info', array(
                                            '%agreement_link%' => $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung-danh-cho-khach-hang'))), 'vlance'); ?></p>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">    
                                        <input type="submit" class="btn btn-primary btn-large popup-validate span12" value="<?php echo $view['translator']->trans('edit_job_page.submit', array(), 'vlance') ?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<?php /* end create-job page */ ?>
<?php $view['slots']->stop(); ?>
