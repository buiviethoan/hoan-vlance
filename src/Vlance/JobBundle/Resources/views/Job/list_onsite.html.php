<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php // $view['slots']->set('title', $title_meta . " | vLance.vn"); ?>
<?php // $view['slots']->set('og_title', $title_meta . " | vLance.vn");?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('img/og/danh-sach-viec-freelance.jpg'));?>
<?php // $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('contest_list'));?>
<?php // $view['slots']->set('og_description', $description_meta);?>
<?php // $view['slots']->set('description', $description_meta);?>

<?php // Set page index,follow or noindex,nofollow ?>
<?php $view['slots']->set('index_follow', $index_follow) ?>
<?php $view['slots']->set('top_block_listing_page', $view->render('VlanceJobBundle:Job/list_contest:tabs.html.php', array('current_route' => "onsite_list", 'count_jobs' => $count_jobs)));?>

<?php $user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>
<?php $view['slots']->start('content') ?>

<?php // echo $view->render('VlanceJobBundle:Job/list_contest:content.html.php', array('entities' => $entities, 'pager' => $pager, 'pagerView' => $pagerView, 'current_user' => $user)); ?>

<div class="search-content row-fluid">
    <div class="layered-block search-results span12">
    <?php foreach($entities as $entity): ?>
        <div class="row-fluid row-result job-onsite">
            <div class="fr-info span12">
                <div class="row-fluid">
                    <div class="span10 onsite-item-detail">
                        <div class="row-fluid">
                            <div class="span2">
                                <a href="<?php echo $view['router']->generate('job_onsite_view',array('hash' => $entity->getHash())) ?>">
                                    <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '96x96', 96, 96, 1);?>
                                    <img src="<?php echo $resize ? $resize : "/media/img01-job-onsite.jpg"; ?>" alt="<?php echo $entity->getTitle(); ?>" title="<?php echo $entity->getTitle(); ?>" />
                                </a>
                            </div>
                            <div class="span10">
                                <h3 class="fr-name">
                                    <a href="<?php echo $view['router']->generate('job_onsite_view', array('hash' => $entity->getHash())); ?>" title="<?php echo $entity->getTitle(); ?>"><?php echo $entity->getTitle(); ?></a>
                                    <span class="label-tag label-purple">Onsite</span>
                                    <?php if ($entity->getFeaturedUntil() >= $now): ?><span class="label-tag label-blue">Nổi bật</span><?php endif; ?>
                                </h3>
                                <div class="info-client row-fluid">
                                    <div class="company-name-cl" title="Tên công ty"><?php echo $entity->getOnsiteCompanyName(); ?></div>
                                    <div class="salary-fl" title="Mức lương"><i></i><?php echo number_format($entity->getBudget(),'0',',','.'); ?> ₫ - <?php echo number_format($entity->getBudgetMax(),'0',',','.'); ?> ₫</div>
                                </div>
                                <div class="clear"></div>
                                <div class="info-client row-fluid">
                                    <div class="time-send-cv span4">
                                        Số lượng CV: <?php echo count($entity->getBids()); ?>
                                    </div>
                                    <div class="time-send-cv span8" title="Hạn nộp CV">
                                        <?php 
                                            $remain = JobCalculator::ago($entity->getCloseAt(), $now);
                                            if($remain == false){
                                                echo '<div class="has_expired_quote">Đã hết hạn nộp CV</div>';
                                            } else {
                                                echo 'Hạn nộp CV: '.$remain ;
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php $skills = $entity->getSkills(); ?>
                                <?php if(count($skills) > 0 || $entity->getService()): ?>
                                    <div class="skill-list span12 row-fluid">
                                        <span>
                                            <?php foreach ($skills as $skill): ?>
                                                <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                                   title="Việc freelance <?php print $skill->getTitle(); ?>"><?php print $skill->getTitle() ?></a>
                                            <?php endforeach;?>
                                        </span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="block-right span2">
                        <ul>
                            <li class="info-job place-work" title="Nơi làm việc"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $entity->getCity()->getName(); ?></li>
                            <li class="info-job date-begin-work" title="Ngày bắt đầu làm việc"><i></i><?php echo $entity->getStartedAt()->format('d/m/Y'); ?></li>
                            <li class="info-job time-work" title="Thời gian làm việc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $entity->getDuration(); ?> tháng</li>
                        </ul>
                    </div>
                </div>
            </div> 
        </div>
    <?php endforeach; ?>
    </div> 
    <?php if($pager->getNbPages() != 1) : ?>
    <div>
        <p class="layered-footer results-paging">
            <?php echo $pagerView->render($pager, $view['router']); ?>   
        </p>
    </div>
    <?php endif; ?>
</div>    
<?php $view['slots']->stop(); ?>