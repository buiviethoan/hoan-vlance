<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php // $view['slots']->set('title', $meta_title);?>
<?php // $view['slots']->set('og_title', $meta_title);?>
<?php // $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_onsite_view',array('hash' => $entity->getHash())));?>
<?php // $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl($image));?>

<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $cost = $view['vlance_payment']->getParameter('credit.pay_admin_post_job.cost'); ?>

<?php $view['slots']->start('content'); ?>
<div class="container block-admin-post-job">
    <div class="header">
        <h1 class="title">Trợ giúp đăng dự án hấp dẫn</h1>
    </div>
    <div class="content">
        <h2>Bạn được gì khi sử dụng dịch vụ của vLance.vn?</h2>
        <ul>
            <li> Dự án được vLance tư vấn về việc lựa chọn freelancer, cách đặt mức ngân sách phù hợp cho dự án. </li>
            <li> Dự án sẽ được vLance  mô tả chi tiết, giúp khách hàng nhận được chào giá sát với dự án nhất. </li>
            <li> Thông tin dự án đầy đủ, rõ ràng freelancer dễ dàng chào giá. Vì vậy, khách hàng sẽ nhận được chào giá hơn. </li>
        </ul>
        <h2>Chi phí và sử dụng dịch vụ?</h2>
        <ul>
            <li>Hãy gửi số điện thoại của bạn cho chúng tôi. vLance sẽ liên lạc với bạn trong thời gian ngắn nhất.</li>
            <li>Chi phí sử dụng dịch vụ: <b>5 Credit</b></li>
        </ul>
    </div>
    <div id="form-admin-post-job" class="row-fluid form">
        <div class="row-fluid">
            <input type="text" id="input-telephone" name="input-telephone" placeholder="Số điện thoại" minlength="10" maxlength="11"
                    <?php if(is_object($current_user)): ?>
                         value="<?php echo ($current_user->getTelephone()) ? $current_user->getTelephone() : ""; ?>"
                    <?php endif; ?>
                required>
        </div>
        <div class="row-fluid">
            <button id="submit-form" class="btn btn-primary btn-large">Gửi yêu cầu</button>
        </div>
    </div>
</div>

<?php if(is_object($current_user)): ?>
<div id="popup-credit-admin-post" class="modal hide fade popup-credit-bid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4>Trợ giúp đăng dự án</h4>
    </div>
    <div class="popup-body-credit-bid">
        <p class="message-credit-bid">Bạn cần trả <b><?php echo $cost; ?> Credit</b> để admin vLance hỗ trợ bạn đăng dự án. <br/> Bạn có muốn gửi yêu cầu trợ giúp?</p>
        <p class="show-credit-user">
            <span style="color: #999999">Bạn hiện có: <?php echo $current_user->getCredit()->getBalance();?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
        </p>
        <p><?php if($current_user->getCredit()->getBalance() < (int)$cost): ?><i><span style="color: red;">Bạn không đủ Credit</span></i><?php endif; ?></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
        <?php if($current_user->getCredit()->getBalance() < (int)$cost): ?>
            <a class="btn btn-primary" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
        <?php else: ?>
            <button id="confirm-request" class="btn btn-primary">Xác nhận</button>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>

<script>
    var url = "<?php echo $view['router']->generate('admin_post_job_submit') ?>";
</script>
<?php $view['slots']->stop(); ?>