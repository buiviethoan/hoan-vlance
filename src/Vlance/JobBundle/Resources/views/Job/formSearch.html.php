<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('list_job.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<?php /* Trang list job */?>
   <h1><?php echo $view['translator']->trans('list_job.title', array(), 'vlance') ?></h1>
    <div class="search-block row-fluid">
        <div class="input-append span8">
            <?php // echo $view['actions']->render($view['router']->generate('job_search_new', array())) ?>
            
            <form action="<?php echo $view['router']->generate('job_search'); ?>" id="search-freelancer-form">
                <input class="txt-large span9" id="search-freelancer-txt" 
                       type="<?php echo $view['translator']->trans('search', array(), 'vlance') ?>"
                       name="search"
                       placeholder="<?php echo $view['translator']->trans('list_job.right_content.text_in_search_job', array(), 'vlance') ?>">
                <button class="btn btn-large" id="search-freelancer-btn" type="submit">
                    <?php echo $view['translator']->trans('list_job.right_content.search', array(), 'vlance') ?> 
                    <span class="i16 i16-search inline"></span>
                </button>
            </form>
        </div>
        <div class="inner-content span4">
            <a href="<?php echo $view['router']->generate('job_new'); ?>" class="btn btn-large btn-primary" onclick="vtrack('Click post job', {'location':'Form search'})">
                <?php echo $view['translator']->trans('list_job.right_content.free-post-job', array(), 'vlance') ?>
            </a>
        </div>
    </div>

    <div class="search-content row-fluid">
        <div class="layered-block search-results span12">
            <div>
                <?php $countResults = $pager->getNbResults(); ?>
                <?php if($countResults != 0) : ?>
                    <p class="layered-heading results-counter">
                        <?php echo $view['translator']->trans('list_job.right_content.num_job_search', array('%num%'=> '<strong>' .  $countResults . '</strong>'), 'vlance') ?> 
                    </p>
                    <?php else: ?>
                    <p class="results-counter">
                        <?php echo $view['translator']->trans('list_job.right_content.no_num_job_search', array(), 'vlance') ?>
                    </p>
                <?php endif; ?>
            </div>
            <div class="block-content">
                <?php $now = new DateTime('now'); ?>
                <?php foreach ($entities as $entity) :?>
                <div class="row-fluid row-result">
                    <div class="fr-info span12">
                        <h3 class="fr-name">
                        <?php if($entity->getTitle()): ?>    
                            <a href="<?php echo $view['router']->generate('job_show',array('id' => $entity->getId())) ?>">
                                <?php echo ucfirst (htmlentities($entity->getTitle(), ENT_SUBSTITUTE, "UTF-8")); ?>
                            </a>
                        <?php endif; ?>
                        </h3>
                        <div class="fr-title">
                            <?php if($entity->getAccount()): ?>  
                            <a href="<?php echo $view['router']->generate('account_show',array('id' => $entity->getAccount()->getId())) ?>" 
                               title="<?php echo $entity->getAccount()->getFullName() ?>">
                                <span><?php echo $entity->getAccount()->getFullName() ?> </span>
                            </a>
                            <?php endif; ?>

                            <span class="location">
                                <?php echo $entity->getCity() ? ' - '.$entity->getCity()->getName() : ' - '.$view['translator']->trans('common.country', array(), 'vlance') ?>
                            </span>

                        </div>
                        <div class="fr-summary row-fluid">
                            <div class="history span8">
                                <?php $category = $entity->getCategory(); ?>
                                <?php if($category): ?> 
                                    <span class="category"><?php echo $category->getTitle() ?></span>
                                <?php endif; ?>
                                <span class="separator">|</span>
                                <?php /*
                                 * An thoi luong cua job
                                <?php if($entity->getDuration()): ?> 
                                    <?php echo $entity->getDuration().' '.$view['translator']->trans('list_job.right_content.date', array(), 'vlance') ?>
                                    <span class="separator">|</span>
                                <?php endif; ?>
                                 * */ ?>
                                
                                <?php if($entity->getBudget()): ?> 
                                    <span class="history-job"><?php echo number_format($entity->getBudget(),'0',',','.').' ' ?>
                                        <?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?>  
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="span4 remain-job">
                                <div class="remain">  
                                    <?php 
                                        $date_finish = $entity->getCloseAt()->format('d-m-Y H:i');
                                        $remain = \Vlance\BaseBundle\Utils\JobCalculator::ago($date_finish, $now->format('d-m-Y H:i'));
                                        
                                         if($remain < 0){
                                            echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</div>';
                                        }  else {
                                        echo $view['translator']->trans('list_job.right_content.remain', array(), 'vlance').': '.$remain ;
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="fr-service">
                            <?php if($entity->getDescription()): ?> 
                                <?php echo htmlentities(\Vlance\BaseBundle\Utils\JobCalculator::cut(Vlance\BaseBundle\Utils\JobCalculator::replacement($entity->getDescription()),300), ENT_SUBSTITUTE, "UTF-8") ?>
                                <a class="read_more" href="<?php echo $view['router']->generate('job_show',array('id' => $entity->getId())) ?>">
                                    <?php echo $view['translator']->trans('list_job.right_content.read_more', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="fr-profile row-fluid">
                            <?php $skills = $entity->getSkills() ?>
                                <?php if(count($skills) > 0): ?>
                                    <div class="skill-list span8 row-fluid">
                                        <span><?php echo $view['translator']->trans('common.skill', array(), 'vlance').':' ?></span>
                                        <span>
                                            <?php foreach ($skills as $skill): ?>
                                               <a title="<?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle().',' ?></a>
                                            <?php endforeach;?>
                                        </span>
                                    </div>
                                <?php endif; ?>
                            <div class="span3 offset1">
                                <div class="number_bid">
                                    <?php if(count($entity->getBids()) == 0):?>
                                        <span><?php echo $view['translator']->trans('list_job.right_content.no_bid', array(), 'vlance') ?> </span>
                                    <?php else:?>
                                        <span>
                                            <?php echo $view['translator']->trans('list_job.right_content.num_bid_job', array('%num%'=> '<strong>' . count($entity->getBids()) . '</strong>'), 'vlance') ?> 
                                        </span>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <?php endforeach; ?>          
            </div>
            <?php if($pager->getNbPages() != 1) : ?>
            <div>
                <p class="layered-footer results-paging">
                    <?php echo $pagerView->render($pager, $view['router']); ?>   
                </p>
            </div>
            <?php endif; ?>
        </div>
    </div>

<?php $view['slots']->stop(); ?>
