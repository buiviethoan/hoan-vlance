<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('edit_contest_page.title_page', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>

<?php /* create-job page */ ?>
<div class="content-section container create-contest">
    <div class="row-fluid">
        <div class="span12 left-side">
            <h1 class="edit_job_title"><?php echo $view['translator']->trans('edit_contest_page.title', array(), 'vlance') ?></h1>
            <div class="post-contest-job">
                <?php $view['form']->setTheme($edit_form, array('VlanceBaseBundle:Form')) ?>
                <form id="edit-contest-form" action="<?php echo $view['router']->generate('contest_edit_submit', array('id' => $entity->getId())) ?>" method="post" <?php echo $view['form']->enctype($edit_form) ?>>
                    <?php echo $view->render('VlanceJobBundle:Job/contest/edit:edit_form.html.php', array('form' => $edit_form, 'acc' => $acc, 'cpackage' => $cpackage)) ?>
                    <?php // Preview Job before submit ?>
                    <div class="row-fluid">
                        <div class="form-cread-job-btn">
                            <div class="row-fluid">
                                <div class="span12">    
                                    <input type="submit" class="btn btn-primary btn-large popup-validate span12" value="<?php echo $view['translator']->trans('edit_contest_page.submit', array(), 'vlance') ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>
<?php /* end create-job page */ ?>
<?php $view['slots']->stop(); ?>
