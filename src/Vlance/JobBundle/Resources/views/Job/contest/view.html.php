<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $view['slots']->start('content'); ?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'ViewJob');
</script>
<?php endif; ?>
<?php // End Tracking ?>

<?php /* detail page */?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<div class="content-section container detail fl-view">
    <div class="row-fluid">
        <div class="span12">
            <?php echo $view->render('VlanceJobBundle:Job/show:breadcrumb.html.php', array('entity' => $entity)) ?>
            <?php  
                /* @var $entity Job */
                $workrooms = $entity->getWorkshopsJob();
                if(is_object($current_user)) :
                    foreach ($workrooms as $workroom) {
                        if($current_user === $workroom->getWorker()) {
                            $workroom_current = $workroom;
                            break;
                        }
                    }
                    if(count($workrooms) > 0 && isset($workroom_current)) : ?>
                        <div class="row-fluid">
                            <h1 class="title">
                                <?php echo ucfirst(htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")); ?>
                                <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                    <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                <?php endif;?>
                                <?php if($entity->getType() != 1): ?>
                                    <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                        <?php if($entity->getFeaturedFb() == false): ?>
                                            <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                        <?php else: ?>
                                        <?php //Feature facebook job-view?> 
                                            <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if($entity->getRequireEscrow() == true): ?>
                                    <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                                <?php endif; ?>
                                <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                                    <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                <?php endif; ?>
                                <?php if($entity->getType() == Job::TYPE_CONTEST): ?>
                                    <?php if($entity->getValid() == true): ?>
                                        <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </h1>
                        </div>
                        <div class="row-fluid">
                            <div class="row-fluid page-tabs">
                                <ul>
                                    <li class="active">
                                        <a href="#">
                                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                    <li>
                                        <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $entity->getId(), 'bid' => $workroom_current->getBid()->getId())) ?>">
                                            <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                </ul>
                                <div class="border"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            <div class="row-fluid">
                <div class="span7 news-detail">
                   <?php if(!is_object($current_user) || !isset($workroom_current)) : ?>
                        <div class="row-fluid ">
                            <h1 class="title">
                                <?php echo ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                                <?php if($entity->getIsPrivate()):?>
                                    <i class="fa fa-lock" title="<?php echo $view['translator']->trans('list_working.content.private_as_freelancer', array(), 'vlance')?>"></i>
                                <?php endif;?>
                                <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                    <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                <?php endif;?>
                                <?php if($entity->getIsReviewed() == true): ?>
                                    <span class="label-tag label-large label-green"
                                          title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
                                <?php endif; ?>
                                <?php if($entity->getType() != 1): ?>
                                    <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                        <?php if($entity->getFeaturedFb() == false): ?>
                                            <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                        <?php else: ?>
                                        <?php //Feature facebook job-view ?>
                                            <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                    
                                <?php if($entity->getIsWillEscrow() == false): ?>
                                    <span class="label-tag label-large label-blue" 
                                          title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
                                <?php endif; ?>
                                <?php if($entity->getRequireEscrow() == true): ?>
                                    <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                                <?php endif; ?>
                                <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                                    <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                <?php endif; ?>
                                <?php if($entity->getType() == Job::TYPE_CONTEST): ?>
                                    <?php if($entity->getValid() == true): ?>
                                        <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </h1>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                    <div class="row-fluid body-view">
                        <?php if($entity->getService()): ?>
                            <div class="row-fluid">
                                <div class="service-need-hire">
                                    <div class="service-title">
                                        <?php echo $view['translator']->trans('view_job_page.job_detail.service.label', array(), 'vlance') ?>: <b><?php echo $entity->getService()->getTitle(); ?></b>
                                    </div>
                                    <?php if(is_object($current_user)): ?>
                                        <div class="service-suggest">
                                            Bạn có thể cung cấp dịch vụ này? <a href="<?php echo $view['router']->generate("account_edit", array('id' => $current_user->getId())); ?>#service-can-provide">Thêm vào hồ sơ làm việc</a>.
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="span10 description <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text-bao<?php endif; ?>">
                            <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>
                                <div class="login_view_job">
                                    <div class="span3 btn-connect">
                                        <p class="first">
                                            <?php // echo $view['translator']->trans('site.menu.got_account', array(), 'vlance') ?> 
                                            <strong>Để đọc thông tin, vui lòng đăng nhập bằng một trong hai cách sau:</strong>
                                        </p>
                                        <div class="center">
                                            <div class="job-login-socail">
                                                <a class="btn-facebook-login" onclick="fb_login()">
                                                    <span><img src="/img/icon-facebook.png"></span>
                                                </a>
                                                <a class="btn-google-plus-login" onclick="google_login()">
                                                    <span><img src="/img/icon-google+.png"></span>
                                                </a>
                                                <a class="btn-linkedin-login" onclick="linkedin_login()">
                                                    <span><img src="/img/icon-linkedin.png"></span>
                                                </a>
                                            </div>
<!--                                                    <a class="btn btn-facebook" onclick="fb_login()">
                                                <?php // echo $view['translator']->trans('site.menu.facebook_job', array(), 'vlance') ?>
                                            </a>-->
                                            <div class="job-login-vlance">
                                                Hoặc
                                                <a class="btn btn-primary" href="<?php echo $view['router']->generate('fos_user_security_login')?>">
                                                <?php echo $view['translator']->trans('site.menu.login_job', array(), 'vlance') ?>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="last"> 
                                            Bạn chưa có tài khoản vLance? 
                                            <a class="" href="<?php echo $view['router']->generate('account_register') ?>">
                                                Đăng ký ngay
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="<?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text<?php endif; ?>">
                                <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                                    <?php if (trim($line)): ?>
                                        <p><?php echo $line; ?></p>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <?php // Attachment files ?>
                        <?php $files = $entity->getFiles(); ?>
                        <?php if(count($files)>0): ?>
                            <div class="attach">
                                <label><?php echo $view['translator']->trans('view_job_page.job_detail.attachment', array(), 'vlance').':' ?></label>
                                <ul>
                                    <?php foreach($files as $file) : ?>
                                        <li class="row-fluid">
                                            <div class="i32 i32-attach"></div>
                                            <?php if(!is_object($current_user)) :?>
                                            <a href="#popup-login" data-toggle="modal" 
                                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                            </a>
                                            <?php echo $view->render('VlanceJobBundle:Job:popup_login.html.php', array()) ?>
                                            <?php else : ?>
                                            <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_job',array('job_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                            </a>

                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>

                <?php // Job information & client description ?>
                <div class="span4 job-information offset1">
                    <div class="job-information-inner">
                        <div class="row-fluid">
                            <?php echo $view->render('VlanceJobBundle:Job/show:job_information.html.php', array('entity' => $entity, 'remain' => $remain)) ?>
                        </div>
                        <div class="row-fluid">
                            <?php echo $view->render('VlanceJobBundle:Job/show:client_information.html.php', array('entity' => $entity)) ?>
                        </div>
                    </div>
                    
                    <div class="job-view-fb-page-box">
                        <h3>Like page để nhận được việc sớm nhất</h3>
                        <?php echo $view->render('VlanceJobBundle:Job/show:fanpage.html.php', array('entity' => $entity)) ?>
                    </div>
                </div>

                <?php // Bid form ?>
                <?php if($entity->getStatus() == Job::JOB_OPEN) : ?>
                    <?php if($entity->getIsWillEscrow() == true || ($entity->getIsWillEscrow() == false && count($entity->getBids()) < 10)): ?>
                        <?php //if(!$entity->isBiddedBy($current_user)):?>
                        <?php if($remain == true): ?>
                        <div class="row-fluid form-contest-apply">
                            <div class="span12 container"> 
                                <?php // Template: VlanceJobBundle:Job:contest/view/bid_form.html.php?>
                                <?php echo $view['actions']->render($view['router']->generate('contest_apply_new',array('id' => $entity->getId())));?>
                            </div>
                        </div>
                    <?php endif;?>
                    <?php endif;?>
                <?php endif; ?>
                <?php // END Bid form ?>

                <?php // list_bid.html.php ?>
                <div class="detail-bottom span12">
                    <?php echo $view['actions']->render($view['router']->generate('contest_show_bid',array('id' => $entity->getId())));?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php // popup gửi tin nhắn cho freelancer ?>
<div id="formmessage" class="formmessage modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">
            <?php echo $view['translator']->trans('view_bid__page.bid_job.messager_to', array(), 'vlance')?>
            <span><?php echo $entity->getAccount()->getFullname(); ?></span>
        </h3>
     </div>
    <div class="modal-body">
        <?php if(is_object($current_user)) : ?>
            <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => '25')));?>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    $('#formmessage').on('shown', function () {
        $('#vlance_jobbundle_messagetype_content').focus();
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        jQuery("a.formmessage-contest").click(function(){
            jQuery("#formmessage form").attr("action", jQuery(this).attr("data-action")); 
            jQuery("#formmessage h3 span").html(jQuery(this).attr("data-uname")); 
        });
        
        $("#formmessage form").submit(function() {
            // Validate attachment files because jQuery Validator can not validate
            // new_file_input
            if(validate_fileinput('#file_upload_container .new_file_input') == false){
                validate_fileinput('#file_upload_container .new_file_input');
                return false;
            }  
            $('#file_upload_container .new_file_input label.error').remove();
            if($("#formmessage form").valid() == false){
                return false;
            }
        });
    })
</script>

<?php /* End detail page */?>
<?php $view['slots']->stop(); ?>