<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $pathImg = ""; ?>
<?php if(!is_null($path)):?>
    <?php $pathImg = ROOT_DIR . "/uploads/bid/" . $path;?>
<?php endif; ?>
<?php $now = new \DateTime('now'); ?>
<div class="span3 profile-contest
        <?php if($pos % 4 == 1) echo 'first_row'?>
        <?php if($pos % 2 == 0) echo 'mode2' ?> 
        <?php if($pos % 3 == 0) echo 'mode3' ?> 
        <?php if($pos % 4 == 0) echo 'mode4' ?>">
    <div class="bid-contest <?php if($entity->getWonAt()) echo "winning-contest"?>">
        <div class="contest-apply">
        <?php if($pathImg != ""): ?>
            <?php //Resize image ?>
            <?php $resize = ResizeImage::resize_image($pathImg, '223x165', 223, 165, 1);?>
            <?php $bigSize = ResizeImage::resize_image($pathImg, '1500x1500', 1500, 1500, 0);?>

            <?php if($resize): ?>
            <div>
                <a href="<?php echo $bigSize ?>" data-lightbox="image-contest" title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                    <img id='img<?php echo $pos?>' src="<?php echo $resize ?>" 
                         alt="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>" 
                         title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                </a>
            </div>    
            <?php else: ?>
            <div>
                <img width="200" height="160" id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                     title="Bài thi của <?php echo $entity->getAccount()->getFullName()?>" 
                     alt="Bài thi của <?php echo $entity->getAccount()->getFullName()?>">
            </div>
            <?php endif; ?>
        <?php else: ?>
        <div>
            <img width="200" height="160" id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                 title="Bài thi của <?php echo $entity->getAccount()->getFullName()?>" 
                 alt="Bài thi của <?php echo $entity->getAccount()->getFullName()?>">
        </div>
        <?php endif; ?>

            <div class="name-fl">
                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getAccount()->getHash()))?>" 
                   title=""><?php echo $entity->getAccount()->getFullName()?></a>
                <?php if($job->getStatus() == Job::JOB_FINISHED): ?>
                    <?php if($entity->getWonAt()): ?>
                        <div class="fl-win">Thắng cuộc</div>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if($job->getStatus() === 1): ?>
                    <?php // if($step == 2): ?>
                    <label>
			    <input id="checkbox<?php echo $pos;?>" type="checkbox" onclick="onCheck(this)" value="<?php echo $entity->getId() ?>">
                    </label>
                    <?php // endif; ?>
                <?php endif; ?>
            </div>
            
            <?php if($job->getStatus() == Job::JOB_CONTEST_QUALIFIED || $job->getStatus() == Job::JOB_FINISHED): ?>
            <?php /*    <?php if($step == 3): ?>
                <div class="contact-fl-contest">
                    <?php $chatted = false;?>
                    <?php foreach($job->getWorkshopsJob() as $wk):?>
                        <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                            <?php $chatted = true; break;?>
                        <?php endif;?>
                    <?php endforeach;?>
                    <?php if($chatted):?>
                        <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $entity->getId())) ?>" 
                                role="button" class="btn btn-contest-apply formmessage-contest"
                                title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>"
                                style="padding:10px 50px;">
                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>
                        </a>
                    <?php else: ?>
                        <a href="#formmessage" role="button" class="btn btn-contest-apply formmessage-contest" data-toggle="modal" 
                            data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                            data-id="<?php echo $entity->getId() ?>" 
                            data-uname="<?php echo $entity->getAccount()->getFullname();?>"
                            title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                        </a>
                    <?php endif; ?>
                </div> */ ?>
                <?php // elseif($step == 4): ?>
                    <div class="contact-fl-contest">
                        <p>
                            <?php $chatted = false;?>
                            <?php foreach($job->getWorkshopsJob() as $wk):?>
                                <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                                    <?php $chatted = true; break;?>
                                <?php endif;?>
                            <?php endforeach;?>
                            <?php if($chatted):?>
                                <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $entity->getId())) ?>" 
                                        role="button" class="formmessage-contest"
                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>">
                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                </a>
                            <?php else: ?>
                                <a href="#formmessage" role="button" class="formmessage-contest" data-toggle="modal" 
                                    data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                                    data-id="<?php echo $entity->getId() ?>" 
                                    data-uname="<?php echo $entity->getAccount()->getFullname();?>"
                                    title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                        </p>
                        <?php if($job->getStatus() != Job::JOB_FINISHED and $entity->getIsQualified()===true): ?>
                            <div>
                                <input style="width: 63%" type="button" id="btn-awarded-<?php echo $pos ?>" onclick="clickAwarded(this, <?php echo $entity->getId()?>), vtrack('Contest award', {'winner':'<?php echo $entity->getAccount()->getId(); ?>'})" class="btn btn-awarded" value="Trao thưởng">
                            </div>
                        <?php endif; ?>
                    </div>
                <?php // endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
