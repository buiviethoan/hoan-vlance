<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php $now = new \DateTime('now'); ?>

<?php switch($current_step):?>
<?php case 'escrowed':?>
    <?php if($step == 1): ?>
    <div class="span12 guider-block">
        <div class="guider-block-step1">
            <div class="row-fluid">
                <div class="span6">
                    <img src="/img/job-contest/buoc1-kh-nhan-bai.jpg" title="Khách hàng nhận bài dự thi" alt="Khách hàng nhận bài dự thi">
                </div>
                <div class="span6 guider-client-right">
                    <h4>Khởi động cuộc thi của bạn</h4>
                    <ul>
                        <li>Freelancer có <b>5 ngày</b> để gửi đến bạn mẫu thiết kế.</li>
                        <li>Hãy xem xét các thiết kế và đưa ra lựa chọn</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if($step == 2): ?>
    <div class="span12 guider-block">
        <div class="guider-block-step2">
            <div class="row-fluid">
                <div class="span6">
                    <img src="/img/job-contest/buoc2-khach-hang-chon-bai.jpg" title="Khách hàng chọn bài dự thi" alt="Khách hàng chọn bài dự thi">
                </div>
                <div class="span6 guider-client-right">
                    <h4>Chọn <b>4 thiết kế</b> ưng ý</h4>
                    <ul>
                        <li>Hãy chọn thiết kế mà bạn thấy ưng ý nhất.</li>
                        <li>Ở bước sau, bạn sẽ có <b>4 ngày</b> làm việc với freelancer thiết kế đấy</li>
                    </ul>
                </div>
            </div>    
        </div>
    </div>
    <?php endif; ?>
<?php break;?>

<?php case 'qualified':?>
    <?php if($step == 3): ?>
    <div class="span12 guider-block">
        <div class="guider-block-step3">
            <div class="row-fluid">
                <div class="span6">
                    <img src="/img/job-contest/buoc3-nhan-tin-trao-doi.jpg" title="Khách hàng nhắn tin trao đổi công việc" alt="Khách hàng nhắn tin trao đổi công việc">
                </div>
                <div class="span6 guider-client-right">
                    <h4>Hoàn thiện thiết kế ngay trong <b>4 ngày</b> làm việc</h4>
                    <ul>
                        <li>Bạn có thể nhắn tin cho freelancer ngay lúc này ở trong trang <b>tin nhắn</b>.</li>
                        <li>Trao đổi với freelancer cho đến khi thiết kế được hoàn thiện.</li>
                        <li>Ở bước sau, bạn sẽ chọn ra một thiết kế đẹp nhất để nhận lấy thiết kế.</li>
                    </ul>
                </div>
            </div>    
        </div>
    </div>
    <?php endif; ?>
    <?php if($step == 4): ?>
    <div class="span12 guider-block">
        <div class="guider-block-step4">
            <div class="row-fluid">
                <div class="span6">
                    <img src="/img/job-contest/buoc4-chon-fl-thang-cuoc.jpg" title="Khách hàng chọn freelancer thắng cuộc" alt="Khách hàng chọn freelancer thắng cuộc">
                </div>
                <div class="span6 guider-client-right">
                    <h4>Bạn thích thiết kế nào nhất?</h4>
                    <ul>
                        <li>Hãy chọn ngay thiết kế phù hợp với yêu cầu thiết kế của bạn nhất.</li>
                        <li>Chỉ khi bạn nhận được thiết kết gốc, tiền thưởng mới được chuyển đến tay freelancer.</li>
                    </ul>
                </div>
            </div>    
        </div>
    </div>
    <?php endif; ?>
<?php break;?>

<?php case 'finished':?>
<div class="span12 guider-block">
    <div class="guider-block-step-end">
        <div class="row-fluid">
            <div class="span6">
                <img src="/img/job-contest/buoc4-fl-thang-cuoc-yeu-cau-fl-gui-ban-goc.jpg" title="Freelancer thắng cuộc và yêu cầu freelancer gửi bản gốc" alt="Freelancer thắng cuộc và yêu cầu freelancer gửi bản gốc">
            </div>
            <div class="span6 guider-client-right">
                <h4>Chúc mừng bạn đã chọn được thiết kế ưng ý</h4>
                <ul>
                    <li>Hãy yêu cầu freelancer gửi bản thiết kế gốc cho bạn.</li>
                    <li>Nếu có vấn đề gì hãy thông báo ngay cho freelancer biết.</li>
                    <li>Sau đó, bạn xác nhận "kết thúc cuộc thi" và tiền thưởng sẽ được chuyển đến tay freelancer.</li>
                </ul>
            </div>
        </div>    
    </div>
</div>
<?php break;?>

<?php default:?>
<?php break;?>

<?php endswitch;?>
