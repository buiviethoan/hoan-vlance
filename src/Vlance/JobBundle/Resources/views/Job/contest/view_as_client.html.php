<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $view['slots']->start('content')?>
<?php /* clientview page */?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<?php /* @var $entity Vlance\JobBundle\Entity\Job */ ?>
<?php echo $view->render('VlanceJobBundle:Job/show_as_client:guider_block_after_post_job.html.php', array('current_step' => $entity->getCurrentStepStatus(), 'entity' => $entity, 'current_user' => $current_user)); ?>


<div class="content-section container client-view">    
    <div class="row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <h1 class="title">
                    <?php echo  ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                    <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                        <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                    <?php endif;?>
                    <?php if($entity->getRequireEscrow() == true): ?>
                        <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                    <?php endif; ?>
                    <?php if($entity->getType() != 1): ?>
                        <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                            <?php if($entity->getFeaturedFb() == false): ?>
                                <span class="label-tag label-large label-blue">NỔI BẬT</span>
                            <?php else: ?>
                            <?php //Feature facebook job-view ?>
                                <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && is_null($entity->getUpfrontAt()) && is_object($current_user)): ?>
                        <?php if($entity->getAccount()->getId() == $current_user->getId()): ?>
                            <span class="label-tag label-large label-blue" title="Công việc chờ được kích hoạt">Chờ kích hoạt</span>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                        <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                    <?php endif; ?>
                    <?php if($entity->getType() == Job::TYPE_CONTEST): ?>
                        <?php if($entity->getValid() == true): ?>
                            <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                        <?php endif; ?>
                    <?php endif; ?>
                </h1>
                <div class="edit-view span2">
                    <?php if(HasPermission::hasAdminPermission($current_user) == FALSE ) : ?>
                        <?php if($entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_OPEN) : ?>
                            <?php if($entity->getValid() == false): ?>
                                <a href="<?php echo $view['router']->generate('contest_edit',array('id' => $entity->getId())) ?>" class="edit-link"
                                   title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php else : ?>
                        <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link"
                           title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                            <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid page-tabs">
                <ul>
                    <li class="active">
                        <a href="#">
                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                        </a>
                    </li>
                    <?php $workrooms = $entity->getWorkshopsJob(); ?>
                    <?php if(count($workrooms) > 0) : ?>
                        <li>
                            <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $entity->getId())) ?>">
                                <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="border"></div>
            </div>
            <div class="row-fluid">
                <div class="span7 news-client-view news-detail">
                    <?php echo $view->render('VlanceJobBundle:Job/show_as_client:job_description.html.php', array('entity' => $entity, 'abs_url' => $abs_url, 'current_user' => $current_user)); ?>
                </div>

                <div class="span4 job-information offset1">
                    <div class="job-information-inner">
                        <?php echo $view->render('VlanceJobBundle:Job/show_as_client:job_information.html.php', array('entity' => $entity, 'abs_url' => $abs_url, 'current_user' => $current_user, 'remain' => $remain)); ?>    
                    </div>
                    
                    <div class="job-view-fb-page-box">
                        <h3>Like page để nhận được nhiều ưu đãi</h3>
                        <?php echo $view->render('VlanceJobBundle:Job/show:fanpage.html.php', array('entity' => $entity)) ?>
                    </div>
                </div>
                
                <?php if($tran == false || $entity->getValid() == false || $entity->getPaymentStatus() == 0): ?>
                <div class="guider-contest-block">
                    <div class="guider-contest-title">
                        <h3>Các bước cần làm để khởi động cuộc thi</h3>
                    </div>
                    <div class="content step-contest">
                        <p>Xin chúc mừng, cuộc thi của bạn vừa được đăng. Để kích hoạt cuộc thi, vui lòng thực hiện làm theo hướng dẫn bên dưới đây.</p>
                        <div>
                            <div class="row-fluid">
                                <div class="span1">
                                    <div class="icon-tick <?php if($tran == true) echo "complete"?>">
                                    </div>
                                </div>
                                <div class="span11">
                                    <h4>Bước 1: Nạp tiền cho cuộc thi</h4>
                                    <p class="note-step-contest">Để nạp tiền cho cuộc thi, bạn vui lòng chuyển khoản số tiền bạn đã chọn là <b class="text-error contest-code">
                                        <?php if($entity->getIsPrivateContest() == true): ?>
                                            <?php echo number_format($entity->getBudget() + ($entity->getBudget() * 15 / 100), 0, '', '.'); ?> VNĐ</b>
                                        <?php else: ?>    
                                            <?php echo number_format($entity->getBudget(), 0, '', '.'); ?> VNĐ</b>
                                        <?php endif; ?> 
                                        vào tài khoản vLance theo thông tin bên dưới đây. Lưu ý, ghi rõ mã số cuộc thi của bạn : <b class="text-error contest-code">CT<?php echo $entity->getId(); ?></b> trong nội dung chuyển khoản.</p>
                                    <div class="account-information">
                                        <div class="row-fluid">
                                            <div class="span3">
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?></p>
                                            </div>
                                            <div class="span9 side-right">
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2_info', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1_info', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3_info', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4_info', array(), 'vlance'); ?></p>
                                            </div>
                                        </div>    
                                        <div class="row-fluid">
                                            <div class="span3">
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?></p>
                                            </div>
                                            <div class="span9 side-right">
                                                <p>Ngân hàng TMCP Á Châu ACB</p>
                                                <p>228085579</p>
                                                <p>Hà Nội</p>
                                            </div>
                                        </div>    
                                        <div class="row-fluid">
                                            <div class="span3">
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?></p>
                                                <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?></p>
                                            </div>
                                            <div class="span9 side-right">
                                                <p>Ngân hàng TMCP Kỹ Thương Techcombank</p>
                                                <p>19030881539011</p>
                                                <p>Hà Nội</p>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="account-information-mobile">
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2_info', array(), 'vlance'); ?></span></p>
                                        <br/>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1_info', array(), 'vlance'); ?></span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3_info', array(), 'vlance'); ?></span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4_info', array(), 'vlance'); ?></span></p>
                                        <br/>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Á Châu ACB</span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">228085579</span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                                        <br/>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Kỹ Thương Techcombank</span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">19030881539011</span></p>
                                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span1">
                                    <div class="icon-tick <?php if($entity->getPaymentStatus() == 2) echo "complete"; ?>">
                                    </div>
                                </div>
                                <div class="span11">
                                    <h4>Bước 2: vLance xác nhận thông tin</h4>
                                    <p class="note-step-contest">vLance sẽ duyệt lại thông tin, để cuộc thi của bạn có thể thu hút được hiệu quả nhất.</p>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span1">
                                    <div class="icon-tick <?php if($entity->getValid() == true) echo "complete"; ?>">
                                    </div>
                                </div>
                                <div class="span11">
                                    <h4>Bước 3: Cuộc thi được hiển thị</h4>
                                    <p class="note-step-contest">Chúc mừng bạn, cuộc thi đã được hiển thị cho tất cả freelancer</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="row-fluid">
                    <?php //Guider_block ?>
                    <?php echo $view->render('VlanceJobBundle:Job/contest/view_as_client:guider_block.html.php', array('current_step' => $entity->getCurrentStepStatusContest(), 'entity' => $entity, 'remain' => $remain, 'step' => $step)); ?>
            </div>
            <div class="row-fluid">
                <?php /*List bid*/ ?>
                <div class="client-view-bottom span12">
                    <?php echo $view['actions']->render($view['router']->generate('contest_show_bid',array('id' => $entity->getId())));?>
                </div>
            </div>
        </div>
    </div>
    <?php //popup share network social ?>
    <?php /* @var $session Symfony\Component\HttpFoundation\Session\Session */ ?>
    <?php $session = $app->getSession(); ?>
    <?php if($session->has('createjob')): ?>
        <?php echo $view->render('VlanceJobBundle:Job/show:share.html.php', array('url' => $view['router']->generate('job_show_freelance_job', array('hash' => $entity->getHash()), \Symfony\Component\Routing\Generator\UrlGenerator::ABSOLUTE_URL), 'title'=> $entity->getCategory()->getTitle() )) ?>
        <?php $session->remove('createjob'); ?>
    <?php endif; ?>
</div>
<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>

<?php /* End client page */?>
<?php $view['slots']->stop(); ?>
