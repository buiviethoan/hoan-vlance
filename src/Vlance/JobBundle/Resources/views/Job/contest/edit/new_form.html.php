<?php $packages = $view['vlance']->getParameter('contest.package'); ?>
<?php $promote = $view['vlance']->getParameter('contest.promote'); ?>
<div id="vlance_jobbundle_jobcontesttype" data-display-track="show-contest-form">
    <?php echo $view['form']->row($form['category']); ?>
    <?php echo $view['form']->row($form['service']); ?>
    <div class="description"><?php // echo $view['translator']->trans('form.job_contest.subtitle', array(), 'vlance')?></div>
    <div class="prize-choice">
        <h3>Giải thưởng</h3>
        <input type="hidden" id="num_package" name="num_package" value="3">
        <div id="block-info-price" class="row-fluid">
            <div class="span4 block-contest-choice">
                <label id="choice-one" class="radio">
                    <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget">
                    <div class="budget-choice">
                        <h4 id="price_one"><?php echo number_format($packages[1]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                        <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[1]['qty'] ?> mẫu thiết kế*.
                        </p>
    <!--                    <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Freelancer tốt.
                        </p>-->
                    </div>
                </label>
            </div>
            <div class="span4 block-contest-choice">
                <label id="choice-two" class="radio">
                    <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget">
                    <div class="budget-choice">
                        <h4 id="price_two"><?php echo number_format($packages[2]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                        <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[2]['qty'] ?> mẫu thiết kế*.
                        </p>
                        <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Designer kinh nghiệm.
                        </p>
                    </div>
                </label>
            </div>
            <div class="span4 block-contest-choice checked">
                <label id="choice-three" class="radio">
                    <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget" checked>
                    <div class="budget-choice">
                        <h4 id="price_three"><?php echo number_format($packages[3]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                        <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[3]['qty'] ?> mẫu thiết kế*.
                        </p>
                        <p>
                            <i class="fa fa-check" aria-hidden="true"></i>Designer chuyên gia.
                        </p>
                        <!--<p>
                            <i class="fa fa-check" aria-hidden="true"></i>Freelancer sẽ được chọn bởi vLance.
                        </p>-->
                    </div>
                </label>
            </div>
            <div class="span12 note-new-contest"><em>* Đây chỉ là con số tham khảo dựa trên ngân sách bạn đặt ra. Thực tế số lượng bài dự thi bạn có thể nhận được là <b>không giới hạn</b>.</em></div>
        </div>
    </div>
    <script type="text/javascript">
        var serPrice = "<?php echo $view['router']->generate('contest_service_price', array()) ?>";
    </script>
    <div><?php echo $view['form']->row($form['title']); ?></div>
    <div><?php echo $view['form']->widget($form['description']); ?></div>
    <div><?php echo $view['form']->row($form['files']); ?></div>
    <div class="block-private">
        <div class="row-fluid">
            <span class="contest_private_block1 span12">
                <label class="checkbox"><?php echo $view['form']->widget($form['isPrivateContest']); ?>Giữ bí mật các bài dự thi (cần thanh toán thêm 15% giá trị giải thưởng)</label>
            </span>
            
        </div>
        <div class="private_explanation row-fluid">
            <p>
                <i>Chỉ mình bạn có quyền được xem tất cả các bài dự thi. Các thí sinh không xem được bài dự thi của nhau. Hạn chế việc sao chép ý tưởng của nhau. Đồng thời đảm bảo được rằng ngoài bạn ra, không một ai khác xem được các mẫu thiết kế tham gia dự thi.</i>
            </p>
        </div>
    </div>
    
    <?php echo $view['form']->row($form['_token'])?>
</div>

<?php // Add more options ?>
<!--<div class="more-options">
    <div class="control-group ">
        <label><?php //echo $view['translator']->trans('form.job_contest.promote.title', array(), 'vlance'); ?></label>

        <?php // accelerating competition ?>
        <div class="promote-choice">
            <label class="fanpage checkbox">
                <div class="row-fluid">
                    <p class="span9"><input name="promote-contest-fb" type="checkbox" value="">
                        <?php //echo $view['translator']->trans('form.job_contest.promote.choice_1', array(), 'vlance') ?>
                    </p>
                    <span class="span3"><b><?php //echo number_format($promote, 0, '', '.') ?></b> VNĐ</span>    
                </div>
            </label>
        </div>  
        <div class="promote-choice">
            <label class="checkbox">
                <div class="row-fluid">
                    <p class="span9"><input name="promote-contest-hp" type="checkbox" value="">
                        <?php //echo $view['translator']->trans('form.job_contest.promote.choice_2', array(), 'vlance') ?>
                    </p>
                    <span class="span3"><b><?php //echo number_format($promote, 0, '', '.') ?></b> VNĐ</span>
                </div>    
            </label>
        </div>
    </div>
</div>-->
<div class="clear"></div>
<?php // END Add more options ?>