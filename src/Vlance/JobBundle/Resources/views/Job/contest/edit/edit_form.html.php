<?php $packages = $view['vlance']->getParameter('contest.package'); ?>
<?php $promote = $view['vlance']->getParameter('contest.promote'); ?>
<div id="vlance_jobbundle_jobcontesttype" data-display-track="show-contest-form">
    <?php echo $view['form']->row($form['category']); ?>
    <?php echo $view['form']->row($form['service']); ?>
    <div class="description"><?php // echo $view['translator']->trans('form.job_contest.subtitle', array(), 'vlance')?></div>
    <div class="row-fluid">
        <?php if($cpackage == $packages[1]['amount']): ?>
        <div class="offset4 span4 block-contest-choice checked">
            <label id="choice-one" class="radio">
                <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget" value="<?php echo $packages[1]['amount'] ?>" checked>
                <div class="budget-choice">
                    <h4><?php echo number_format($packages[1]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                    <p>
                        <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[1]['qty'] ?> mẫu thiết kế*.
                    </p>
                </div>
            </label>
        </div>
        <?php elseif($cpackage == $packages[2]['amount']): ?>
        <div class="offset4 span4 block-contest-choice checked">
            <label id="choice-two" class="radio">
                <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget" value="<?php echo $packages[2]['amount'] ?>" checked>
                <div class="budget-choice">
                    <h4><?php echo number_format($packages[2]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                    <p>
                        <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[2]['qty'] ?> mẫu thiết kế*.
                    </p>
                    <p>
                        <i class="fa fa-check" aria-hidden="true"></i>Designer kinh nghiệm.
                    </p>
                </div>
            </label>
        </div>
        <?php else: ?>
        <div class="offset4 span4 block-contest-choice checked">
            <label id="choice-three" class="radio">
                <input type="radio" name="vlance_jobbundle_jobcontesttype[budget]" id="vlance_jobbundle_jobcontesttype_budget" value="<?php echo $packages[3]['amount'] ?>" checked>
                <div class="budget-choice">
                    <h4><?php echo number_format($packages[3]['amount'], 0, '', '.') ?> <span>VNĐ</span></h4>
                    <p>
                        <i class="fa fa-check" aria-hidden="true"></i>Hơn <?php echo $packages[3]['qty'] ?> mẫu thiết kế*.
                    </p>
                    <p>
                        <i class="fa fa-check" aria-hidden="true"></i>Designer chuyên gia.
                    </p>
                </div>
            </label>
        </div>
        <?php endif; ?>
        <div class="span12 note-new-contest"><em>* Đây chỉ là con số tham khảo dựa trên ngân sách bạn đặt ra. Thực tế số lượng bài dự thi bạn có thể nhận được là <b>không giới hạn</b>.</em></div>
    </div>
    
    <?php echo $view['form']->row($form['title']); ?>
    <?php echo $view['form']->widget($form['description']); ?>
    <?php echo $view['form']->row($form['files']); ?>

    <?php echo $view['form']->row($form['_token'])?>
</div>

<div class="clear"></div>
<?php // END Add more options ?>