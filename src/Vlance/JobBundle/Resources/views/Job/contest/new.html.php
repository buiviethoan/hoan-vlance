<?php use Vlance\BaseBundle\Utils\ResizeImage;?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('og_image', 'http://'.$app->getRequest()->getHost() . $view['assets']->getUrl('img/og/cuoc-thi-thiet-ke.jpg'));?>
<?php $view['slots']->set('title', "Đăng cuộc thi thiết kế");?>
<?php $view['slots']->set('og_title', "Đăng cuộc thi thiết kế");?>
<?php $view['slots']->set('og_url', 'http://'.$app->getRequest()->getHost() . $view['router']->generate('job_contest_new'));?>
<?php $view['slots']->set('description', "Cuộc thi thiết kế giúp bạn chỉ thanh toán một lần, nhưng nhận hàng chục mẫu thiết kế chất lượng, độc nhất và đầy sáng tạo.");?>
<?php $view['slots']->start('content')?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'ViewCreateJobPage');
</script>
<?php endif; ?>
<?php // End Tracking ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<?php /* create-contest page */?>
<div class="content-section create-contest">
    <div class="header">
        <div class="container">
            <div class="row-fluid">
                <div class="span6 side-left">
                    <h1 class="title">Cuộc thi thiết kế</h1>
                    <div class="content">
                        <p>Chỉ thanh toán một lần, nhưng nhận hàng chục mẫu thiết kế chất lượng, độc nhất và đầy sáng tạo.</p>
                        <ul>
                            <li>Nhận ngay vài chục mẫu thiết kế chỉ trong 5 ngày.</li>
                            <li>Dễ dàng chọn ra được mẫu xuất sắc nhất.</li>
                            <li>Không giới hạn số lần chỉnh sửa.</li>
                            <li>Thanh toán khi hài lòng 100%.</li>
                        </ul>
                    </div>
                    <div class="button">
                        <a href="#contest-focus" class="btn btn-vl btn-vl-medium btn-vl-orange">Tạo cuộc thi ngay</a>
                    </div>
                </div>
                <div class="span6 side-right">
                    <img src="../img/job-contest/cuoc-thi-thiet-ke.png" title="Cuộc thi thiết kế" alt="Cuộc thi thiết kế">
                </div>
            </div>
        </div>
    </div>
    <div class="progress-contest">
        <div class="container">
            <li class="step first">
                <div class="step-number">Bước đầu tiên</div>
                <div class="tally">Tạo cuộc thi</div>
            </li>
            <li class="step">
                <div class="step-number">Vòng 1</div>
                <div class="tally">Nhận mẫu thiết kế</div>
            </li>
            <li class="step">
                <div class="step-number">Vòng 2</div>
                <div class="tally">Chọn 4 thiết kế xuất sắc</div>
            </li>
            <li class="step last">
                <div class="step-number">Vòng cuối</div>
                <div class="tally">Chọn thiết kế đẹp nhất & Trao thưởng</div>
            </li>
        </div>    
    </div>
    <div class="container">
        <div class="row-fluid">
            <div class="center-side">            
                <div class="contest_title" id="contest-focus">
                    <p>Bước đầu tiên:</p>
                    <h1><?php echo $view['translator']->trans('form.job_contest.title_contest', array(), 'vlance')?></h1>
                </div>
                <div class="row-fluid">   
                <?php /*    <div class="span12">
                        <p>
                            <b>Cuộc thi thiết kế</b> giúp bạn chỉ thanh toán một lần, nhưng nhận hàng chục mẫu thiết kế chất lượng, độc nhất và đầy sáng tạo.<br/>
                            <br/>
                            <b>Cuộc thi thiết kế hoạt động như nào?</b><br/>
                            <br/>
                            <b>Bước 1:</b> Bạn đăng cuộc thi, mô tả rõ yêu cầu thiết kế của bạn.<br/>
                            <b>Bước 2:</b> Bạn thanh toán cho vLance theo mức ngân sách bạn lựa chọn. Cuộc thi của bạn sẽ lập tức được khởi động ngay sau đó, và các designer sẽ gửi các mẫu thiết kế cho bạn trong 5 ngày liên tục.<br/>
                            <b>Bước 3:</b> Bạn sẽ chọn ra 4 mẫu có thiết kế xuất sắc nhất và tiếp tục làm việc với các designers, cho đến khi bạn thực sự ưng ý.<br/>
                            <b>Bước 4:</b> Cuối cùng bạn sẽ lựa chọn ra 1 mẫu xuất sắc nhất để trao thưởng cho người chiến thắng và nhận đầy đủ các files thiết kế gốc từ designer. Bạn xác nhận thanh toán, cuộc thi kết thúc với kết quả khiến bạn hài lòng 100%.<br/>
                            <br/>
                            <em>Chúng tôi cam kết bạn sẽ nhận được những mẫu thiết kế xuất sắc nhất từ các nhà thiết kế hàng đầu. Đồng thời cam kết bạn chỉ phải thanh toán với sản phẩm mà bạn <b>hài lòng 100%</b>.</em>
                        </p>
                    </div> */ ?>

                </div>
                <script type="text/javascript">
                    var logged = '<?php if(is_object($acc)){echo "true";} else {echo "false";}?>';
                </script>
                <?php if(!is_object($acc)): ?>
                <?php //START form dang ki ?>
                <div class="block-lr" style="margin-bottom: 20px;">
                    <div class="row-fluid">   
                        <div class="span12">
                            <h4>Thông tin người đăng cuộc thi</h4>
                            <div class="row-fluid block-type-user">
                                <div class="span6">
                                    <label class="radio checked"><input id="new_user" type="radio" name="typeofuser" value="new" checked> Tôi là người mới</label>
                                </div>
                                <div class="span6">
                                    <label class="radio"><input id="old_user" type="radio" name="typeofuser" value="old"> Tôi đã có tài khoản</label>
                                </div>
                            </div> 
                        </div>    
                    </div>
                    <div class="row-fluid">
                        <div class="block-form-lr span12">
                            <div class="block-form-reg">
                                <form id="reg-form" method="post">
                                    <div class="row-fluid field">
                                        <div class="span6">
                                            <label>Họ và tên</label>
                                            <input id="reg_name" type="text" name="reg_name" placeholder="Ví dụ: Trần Văn A">
                                        </div>
                                        <div class="span6 input-email">
                                            <label>Email</label>
                                            <input id="reg_email" type="email" name="reg_email" placeholder="Ví dụ: name@domain.com">
                                        </div>
                                    </div>
                                    <div class="row-fluid field">
                                    <div class="span6">
                                            <label>Mật khẩu</label>
                                            <input id="reg_pass" type="password" name="reg_pass" placeholder="********">
                                        </div>
                                        <div class="span6 re-pass">
                                            <label>Nhập lại mật khẩu</label>
                                            <input id="reg_re_pass" type="password" name="reg_re_pass" placeholder="********">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="block-form-log deactive">
                                <form id="log-form" method="post">
                                    <div class="row-fluid field">
                                        <div class="span6 block-username-log">
                                            <label>Email</label>
                                            <input type="text" id="login_email" name="login_email" placeholder="Email">
                                        </div>
                                        <div class="span6 block-password-log">
                                            <label>Mật khẩu</label>
                                            <input type="password" id="login_pass" name="login_pass" placeholder="Mật khẩu đăng nhập">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <script type="text/javascript">
                                var reg_form_in_job = '#reg-form';
                                var reg_form_in_job_params = {"url":"<?php echo $view['router']->generate('register_job') ?>"};
                                var login_form_in_job = '#log-form';
                                var login_form_in_job_params = {"url":"<?php echo $view['router']->generate('login_job') ?>"};
                            </script>
                            <div class="row-fluid">
                                <p class="login-socail-network-label">Hoặc dùng luôn tài khoản sau</p>
                                <div class="login-socail-network">
                                    <a class="btn-facebook btn-social-fb-post-job" onclick="fb_login()"><span><img src="/img/icon-facebook.png">Facebook</span></a>
                                    <a class="btn-google-plus btn-social-nfb-post-job" onclick="google_login()"><span><img src="/img/icon-google+.png">Google</span></a>
                                    <a class="btn-linkedin btn-social-nfb-post-job" onclick="linkedin_login()"><span><img src="/img/icon-linkedin.png">LinkedIn</span></a>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
                <?php //END form dang ki ?>
                <?php endif; ?>

                <div class="post-contest-job">
                    <?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
                    <form id="contest-form" action="<?php echo $view['router']->generate('contest_new_submit') ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
                        <?php echo $view->render('VlanceJobBundle:Job/contest/edit:new_form.html.php', array('form' => $form)) ?>

                        <?php // Preview Job before submit ?>
                        <div class="form-cread-job-btn">
                            <div class="row-fluid">
                                <div class="span12">    
                                    <input id="btn-submit-contest" type="button" class="btn btn-primary btn-large popup-validate span12" value="<?php echo $view['translator']->trans('form.job_contest.submit', array(), 'vlance') ?>"/>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="accept-agreement span12">
                                    <p>
                                        <?php echo $view['translator']->trans('form.job_contest.job.agree_info', array('%agreement_link%' => $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung-danh-cho-khach-hang'))), 'vlance'); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>    
</div>

<?php /* end create-job page */?>
<?php $view['slots']->stop();?>
