<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>

<?php $path_img = ""; ?>
<?php if(!is_null($path)):?>
    <?php $path_img = ROOT_DIR . "/uploads/bid/" . $path;?>
<?php endif; ?>
<div class="span3 profile-contest
        <?php if($pos % 4 == 1) echo 'first_row'?>
        <?php if($pos % 2 == 0) echo 'mode2' ?>
        <?php if($pos % 3 == 0) echo 'mode3' ?> 
        <?php if($pos % 4 == 0) echo 'mode4' ?> ">
    <div class="bid-contest <?php if($entity->getWonAt()) echo "winning-contest"?>">
	<?php //Nếu cuộc thi được public cho mọi user ?>
        <?php if($job->getIsPrivateContest() === false): ?>
	    <!-- Nếu có ảnh-->
            <?php if($path_img != ""): ?>
                <?php //Resize image ?>
                <?php $resize       = ResizeImage::resize_image($path_img, '223x165', 223, 165, 1);?>
                <?php $medium_size  = ResizeImage::resize_image($path_img, '1000x1000', 1000, 1000, 0);?>
                <?php if($resize && $medium_size): ?>
                <div>
                    <a href="<?php echo $medium_size ?>" data-lightbox="image-contest" title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                        <img src="<?php echo $resize ?>" 
                             alt="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>" 
                             title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                    </a>
                </div>
                <?php else: ?>
                <div>
                    <img id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                         title="Bài thi của <?php echo $entity->getAccount()->getFullName()?>" 
                         alt="Bài thi của <?php echo $entity->getAccount()->getFullName()?>">
                </div>
                <?php endif; ?>
	    <?php //Nếu không có ảnh?>
            <?php else: ?>
                <div>
                    <img id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                         title="Bài thi của <?php echo $entity->getAccount()->getFullName()?>" 
                         alt="Bài thi của <?php echo $entity->getAccount()->getFullName()?>">
                </div>
            <?php endif; ?>
	<?php //Nếu cuộc thi không được public ?>
        <?php else: ?>
            <div>
                <img id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                     title="Bài thi của <?php echo $entity->getAccount()->getFullName()?>" 
                     alt="Bài thi của <?php echo $entity->getAccount()->getFullName()?>">
            </div>
        <?php endif; ?>
        
        <div class="name-fl">
            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getAccount()->getHash()))?>" 
               title=""><?php echo $entity->getAccount()->getFullName()?></a>
        <?php if($entity->getWonAt()): ?>
            <div class="fl-win span6">Thắng cuộc</div>
        <?php endif; ?>
        </div>    
    </div>
</div>
