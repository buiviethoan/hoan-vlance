<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php 
    $path_img = "";
    if(count($entity->getFiles()) > 0){
        foreach($entity->getFiles() as $f){
            $path_img = ROOT_DIR . "/uploads/bid/".$f->getPath().'/'.$f->getstoredName();
        }
    }
?>
<div class="span3 profile-contest
        <?php if($pos % 4 == 1) echo 'first_row'?>
        <?php if($pos % 2 == 0) echo 'mode2' ?> 
        <?php if($pos % 3 == 0) echo 'mode3' ?> 
        <?php if($pos % 4 == 0) echo 'mode4' ?>">
    <div class="bid-contest acc_current <?php if($entity->getWonAt()) echo "winning-contest" ?>">
        <?php if($path_img != ""): ?>
            <?php //Resize image ?>
            <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($path_img, '223x165', 223, 165, 1);?>
            <?php $medium_size  = ResizeImage::resize_image($path_img, '1000x1000', 1500, 1500, 0);?>
            <?php if($resize && $medium_size): ?>
            <div>
                <a href="<?php echo $medium_size ?>" data-lightbox="image-contest" title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                    <img src="<?php echo $resize ?>" alt="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>" 
                         title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
                </a>
            </div>   
            <?php else: ?>
            <div>
                <img id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                    title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>" 
                    alt="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
            </div>
            <?php endif; ?>
        <?php else: ?>
            <img id='img<?php echo $pos?>' src="/img/job-contest/no-image-1.jpg" 
                title="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>" 
                alt="Bài dự thi của <?php echo $entity->getAccount()->getFullName()?>">
        <?php endif; ?>
        
        <div class="name-fl">
            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getAccount()->getHash()))?>" 
               title=""><?php echo $entity->getAccount()->getFullName()?></a>
            <?php if($entity->getWonAt()): ?>
                <div class="fl-win">Thắng cuộc</div>
            <?php endif; ?>
        </div>
        <div class="workroom">
            <?php $chatted = false;?>
            <?php foreach($job->getWorkshopsJob() as $wk):?>
                <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                    <?php $chatted = true; break;?>
                <?php endif;?>
            <?php endforeach;?>
            <?php if($chatted):?>
                <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $entity->getId())) ?>" 
                        role="button" class="formmessage-contest btn btn-large"
                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>">
                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                </a>
            <?php else: ?>
                <a href="#formmessage" role="button" class="formmessage-contest btn btn-large" data-toggle="modal" 
                    data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                    data-id="<?php echo $entity->getId() ?>" 
                    data-uname="<?php echo $job->getAccount()->getFullname();?>"
                    title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
