<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php //if(count($bid_current) > 0) : ?>
    <?php //echo $view->render('VlanceJobBundle:Job/show/list_bid:feature_bid.html.php', array('job' => $job, 'acc' => $acc_login, 'bid' => $bid_current[0])); ?>
<?php //endif; ?>

<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php if($current_user == $job->getAccount()):?>
    <?php echo $view->render('VlanceJobBundle:Job/contest/view/list_contest_apply:statistics.html.php', array( 'job' => $job, 'number_files' => /*$number_qualified*/ $all_files, 'remain' => $remain)) ?>
<?php else: ?>
    <?php echo $view->render('VlanceJobBundle:Job/contest/view/list_contest_apply:statistics.html.php', array( 'job' => $job, 'number_files' => $number_files, 'remain' => $remain)) ?>
<?php endif; ?>

<?php /* Dưới số đấu thầu */?>
<?php /* Phần xác nhận số bài đc chọn */?>
<div id="block-confirm-apply" class="confirm-contest-apply hide">
    <div class="row-fluid">
        <div class="span8">
            <p class="num-apply confirm-apply"><span id="num-confirm"></span> bài thi được chọn</p>
        </div>
        <div class="span4">
            <input id="btn-confirm-contest-apply" class="btn btn-confirm-contest-apply" onclick="vtrack('Contest qualify')" value="Xác nhận">
        </div>
    </div>    
</div>
<script type="text/javascript">
    var url     = "<?php echo $view['router']->generate('confirm_bid_contest') ?>";
    var job     = "<?php echo $job->getHash(); ?>"
    var award   = "<?php echo $view['router']->generate('contest_award') ?>";
</script>
<?php /* END */ ?>

    <?php // There is no bid ?>
    <?php if(count($bids) == 0 && count($bid_current) == 0) : ?>
        <?php if($number_declined > 0): ?>
            <div class="no-profile-job">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php else: ?> 
            <div class="no-profile-job">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.no_tender_bid', array(), 'vlance') ?>
            </div>
        <?php endif; ?> 

    <?php // There are some bids ?>
    <?php else : ?>
        <script type="text/javascript">
            var LBox = true; //Biến để xét lightbox
        </script>
	<?php $i = 0; ?>
	<?php $pos = 0; ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : ?>
<?php //Nếu tài khoản là admin hoặc người tuyển dụng ?>
            <?php $temp = 'VlanceJobBundle:Job/contest/view_as_client:as_client.html.php'; ?>
        <?php else: ?>
<?php //Nếu tài khoản là freelancer ?>
            <?php $temp = 'VlanceJobBundle:Job/contest/view/contest_apply:as_freelancer.html.php'; ?>
        <?php endif; ?>
    
        
        
<?php // View all bid ?>
<?php /* job not qualified */?>
	
<?php //Nếu tài khoản là nhà tuyển dụng hoặc là admin ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : ?>
<?php //Nếu cuộc thi ở step = 3 ?>
	    <?php if($display_pay_credit == 1):?>
	
		<div style="background: #fbc6c6;;margin: 0px 0px 10px 0px;padding: 13px 0px 11px 24px; width: initial;">
		    <div style="float: left; width: 80%">
			<b>Cuộc thi của bạn đã nhận tối đa <mark><?php echo $number_files."/".$all_files?></mark> số bài dự thi cho phép. Bạn có muốn mở rộng cuộc thi chỉ với 1 credit?</b>
		    </div>
		    <?php if($acc_login->getCredit()->getBalance() >= 1):?>
			<div style="float: left; width: 20%;">
			    <input type="button" value="Mở rộng" class="btn btn-primary" style="font-size: 14px; margin-left: 16%;" data-toggle="modal" data-target="#myModal">
			</div>
		    <?php else:?>
			<div style="float: left; width: 20%;">
			    <input type="button" value="Mở rộng" class="btn btn-primary" style="font-size: 14px; margin-left: 16%;" data-toggle="modal" data-target="#myModal2">
			</div>
		    <?php endif;?>
		    <div style="clear: both"></div>
		</div>
		
<!--	Modal xác nhận thanh toán-->
		<div class="container">
		    <!-- Modal -->
		    <div class="modal fade hide" id="myModal" role="dialog">
		      <div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">&times;</button>
			    <h4 class="modal-title">Xác nhận thanh toán</h4>
			  </div>
			  <div class="modal-body" style="min-height: 30px;">
			      <p>Nhận thêm <b>20 bài dự</b> thi chỉ với <b>1 credit</b></p>
			  </div>
			  <div class="modal-footer">
			    <button type="button" class="btn btn-default" id="btn-contest-extend-pay-credit" style="font-size: 14px">Thanh toán</button>
			    <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: 14px">Close</button>
			  </div>
			</div>
		      </div>
		    </div>
		  </div>
<!--Modal thông báo tài khoản không đủ credit-->
		    <div class="container">
		    <!-- Modal -->
		    <div class="modal fade hide" id="myModal2" role="dialog">
		      <div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal">&times;</button>
			    <h4 class="modal-title">Xác nhận thanh toán</h4>
			  </div>
			  <div class="modal-body" style="min-height: 30px;">
			      <p>Nhận thêm <b>20 bài dự</b> thi chỉ với <b>1 credit</b></p>
			      <span style="color: red;">*Tài khoản của bạn không đủ credit</span>
			  </div>
			  <div class="modal-footer">
			    <button type="button" class="btn btn-default" id="btn-buy-more-credit" style="font-size: 14px">Mua credit</button>
			    <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: 14px">Close</button>
			  </div>
			</div>
		      </div>
		    </div>
		  </div>

	    <?php endif;?>
	    <?php if(count($bid_qualified) > 0): ?>
	    <div style="-webkit-clip-path: polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);clip-path:polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);background: #f5f5f5;margin: 0px 0px 10px 0px;padding: 13px 0px 11px 24px;border-top-right-radius: 2em;width: 50%;">
		<b>Danh sách các bài dự thi đã được duyệt bởi bạn</b>
	    </div>   
	    <div class="row-fluid">
		<div class="bid-qualified">
		    <?php foreach ($bid_qualified as $entity) :?>
			<?php $i++; ?>
			<?php if(count($entity->getFiles()) > 0): ?>
			    <?php foreach ($entity->getFiles() as $f): ?>
				<?php $pos++; ?>
				<?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login, 'path' => $f->getPath().'/'.$f->getstoredName(), 'pos' => $pos, 'step' => $step )) ?>
			    <?php endforeach; ?>
			<?php else: ?>
			    <?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login, 'path' => null, 'pos' => 0, 'step' => $step, 'dmm'=>'dmm')) ?>
			<?php endif; ?>
		    <?php endforeach; ?>
		</div>
	    </div>
	    <?php endif; ?>
	    
	    <div style="-webkit-clip-path: polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);clip-path:polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);background: #f5f5f5;margin: 0px 0px 10px 0px;padding: 13px 0px 11px 24px;border-top-right-radius: 2em;width: 50%;">
		<b>Danh sách các bài dự thi</b>
	    </div>
	    <?php $pos = 0;?>
	    <div class="row-fluid">
		<div class="bid-not-qualified">
		    <?php foreach ($bid_not_qualified as $entity) :?>
			<?php $i++; ?>
			<?php if(count($entity->getFiles()) > 0): ?>
			    <?php foreach ($entity->getFiles() as $f): ?>
				<?php $pos++; ?>
				<?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login,'path' => $f->getPath().'/'.$f->getstoredName(), 'pos' => $pos, 'step' => $step)) ?>
			    <?php endforeach; ?>
			<?php else: ?>
			    <?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login, 'path' => null, 'pos' => 0, 'step' => $step)) ?>
			<?php endif; ?>
		    <?php endforeach;?>
		</div>
	    </div>

        <?php else: ?>
	
<?php //Nếu tài khoản là freelancer hoặc ẩn danh ?>
	<?php if(count($bid_current) > 0) : ?>
	    <div style="-webkit-clip-path: polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);clip-path:polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%); background: #f5f5f5;margin: 0px 0px 10px 0px;padding: 13px 0px 11px 24px;border-top-right-radius: 2em;width: 50%;">
		<b>Danh sách các bài dự thi của bạn</b>
	    </div>
	    <div class="row-fluid">
		<?php // View bid account current ?>
		    <?php foreach($bid_current as $bc): ?>
			<?php $pos++; ?>
			<?php echo $view->render('VlanceJobBundle:Job/contest/view/contest_apply:as_bid_owner.html.php',array('entity' => $bc, 'job' => $job,'current_user' => $acc_login, 'pos' => $pos, 'step' => $step)) ?>
		    <?php endforeach; ?>
		<?php //Calculating distance for intersect ad in the list bids ?>
		<?php $distance = ceil(count($bids) / ((round(count($bids)/10)==0)?1:round(count($bids)/10))); ?>
	    </div>
	<?php endif; ?>
	
	<div style="-webkit-clip-path: polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);clip-path:polygon(75% 0%, 80% 50%, 75% 100%, 0% 100%, 0% 50%, 0% 0%);background: #f5f5f5;margin: 0px 0px 10px 0px;padding: 13px 0px 11px 24px;border-top-right-radius: 2em;width: 50%;">
	    <b>Danh sách các bài dự thi</b>
	</div>
	<div class="row-fluid">
	    <?php $pos = 0; ?>
            <?php foreach ($bids as $entity) :?>
                <?php $i++; ?>
                <?php if(count($entity->getFiles()) > 0): ?>
                    <?php foreach ($entity->getFiles() as $f): ?>
                        <?php $pos++; ?>
                        <?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login,'path' => $f->getPath().'/'.$f->getstoredName(), 'pos' => $pos, 'step' => $step)) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <?php echo $view->render($temp,array('entity' => $entity, 'job'=> $job, 'current_user' => $acc_login, 'path' => null, 'pos' => 0, 'step' => $step)) ?>
                <?php endif; ?>
                <?php // Show intersect Ads ?>
                <?php /*if($i%$distance==0 && $i!=count($bids)):?>
                    <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:ad_intersect.html.php',array()) ?>
                <?php endif;*/?>
            <?php endforeach;?>
	</div>
        <?php endif;?>
        <?php //echo $view->render('VlanceJobBundle:Job/show/list_bid:ad_bottom.html.php',array()) ?>
    
        <?php // số bid bị từ chối ?>
        <?php  if($number_declined > 0): ?>
            <div class="count_bid_declined span12">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php endif; ?>

        <?php // Popups ?>
        <?php if($job->getAccount() === $acc_login): ?>
            <?php // popup gửi tin nhắn cho freelancer ?>
            <div id="formmessage" class="formmessage modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">
                        <?php echo $view['translator']->trans('view_bid__page.bid_job.messager_to', array(), 'vlance')?>
                        <span><?php echo $entity->getAccount()->getFullname(); ?></span>
                    </h3>
                 </div>
                <div class="modal-body">
                    <?php if(is_object($acc_login)) : ?>
                        <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => '25')));?>
                    <?php endif; ?>
                </div>
            </div>
            <script type="text/javascript">
                $('#formmessage').on('shown', function () {
                    $('#vlance_jobbundle_messagetype_content').focus();
                });
            </script>
            <?php if($acc_login->getIsMissingClientInfo()):?>
                <?php $infos = $acc_login->getMissingClientInfo();?>
                <?php if($infos[0] == "avatar" || $infos[0] == "city" || $infos[0] == "telephone"): ?>
                    <?php $edit_form = "account_basic_edit"; ?>
                <?php else: ?>
                    <?php $edit_form = "account_edit"; ?>
                <?php endif; ?>
                <?php foreach($infos as $key => $inf):?>
                    <?php $infos[$key] = $view['translator']->trans('controller.job.showFreelanceJob.' . $infos[$key], array(), 'vlance'); ?>
                <?php endforeach;?>
                <?php /*popup báo khách hàng thiếu thông tin nên không gửi được tin nhắn cho freelancer */ ?>
                <div id="popup-missing-info" class="popup-refusal modal hide fade" 
                     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title', array(), 'vlance') ?></h3>
                     </div>
                    <div class="modal-body">
                        <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_send_message', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
                        <a href="<?php echo $view['router']->generate($edit_form, array('id' => $acc_login->getId(), 'referer' => 'redirect')) ?>" 
                           class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                               <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
                        </a>
                    </div>
                </div>

                <?php /*popup báo khách hàng thiếu thông tin nên không xem được nội dung chào giá */ ?>
                <div id="popup-missing-info-viewbid" class="popup-refusal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title_view_bid', array(), 'vlance') ?></h3>
                    </div>
                    <div class="modal-body">
                        <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_view_bid', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
                        <a href="<?php echo $view['router']->generate($edit_form, array('id' => $acc_login->getId(), 'referer' => 'redirect')) ?>" 
                           class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                               <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
                        </a>
                    </div>
                </div>
            <?php endif;?>
        <?php endif; ?>
    <?php endif; ?>       


<script type="text/javascript">
    $(document).ready(function() {
        jQuery("a.formmessage-contest").click(function(){
            jQuery("#formmessage form").attr("action", jQuery(this).attr("data-action")); 
            jQuery("#formmessage h3 span").html(jQuery(this).attr("data-uname")); 
        });
        
        $("#formmessage form").submit(function() {
            // Validate attachment files because jQuery Validator can not validate
            // new_file_input
            if(validate_fileinput('#file_upload_container .new_file_input') == false){
                validate_fileinput('#file_upload_container .new_file_input');
                return false;
            }  
            $('#file_upload_container .new_file_input label.error').remove();
            if($("#formmessage form").valid() == false){
                return false;
            }
        });
    })
</script>

<!-- show big image -->
<div id="openBigSize" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <p id='titleModal'></p>
    </div>
    <div class="modal-body">
        <div>
            <img id='imgModal'>
        </div>
        <div>
            <p id="dimensions_image"></p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
    </div>
</div>

<!-- Popup confirm award -->
<div id="popup-message" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Xác nhận trao thưởng</h3>
    </div>
    <div class="modal-body">
        <p id="message-body">
            Bạn đã chắc chắn nhận được toàn bộ sản phẩm của Freelancer?
        </p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Đóng</a>
        <input id="btn-confirm-award" type="button" class="btn btn-primary" value="Xác nhận" onclick="clickConfirm()">
    </div>
</div>

<!-- Popup confirm award error-->
<div id="popup-message-error" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <h3 id="myModalLabel">Xác nhận trao thưởng</h3>
    </div>
    <div class="modal-body">
        <p id="message-body">
            Đã có lỗi xảy ra. Vui lòng thử lại sau.
        </p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Đóng</a>
    </div>
</div>

<!-- Popup confirm award error-->
<div id="popup-message-success" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">    
    <div class="modal-header">
        <h3 id="myModalLabel">Xác nhận trao thưởng</h3>
    </div>
    <div class="modal-body">
        <p id="message-body">
            Bạn đã trao thưởng thành công
        </p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Đóng</a>
    </div>
</div>

<script>
    $("#btn-contest-extend-pay-credit").click(function(){
	location.replace("<?php echo $view['router']->generate('job_contest_extend_pay_credit',array('hash' => $job->getHash()))?>");
    });
    $("#btn-buy-more-credit").click(function(){
//	console.log("link to buy more credit");
	alert("link to buy more credit");
    });
</script>