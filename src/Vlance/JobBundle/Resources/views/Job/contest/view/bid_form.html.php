<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<!--Popup xác nhận thanh toán-->
<div class="container">
  <!-- Modal -->
  <div class="modal fade hide" id="modal-popup-pay-credit" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body" style="min-height: 30px;">
	  <span>Bạn đã sử dụng hết lượt gửi bài của mình trong cuộc thi này!</span>
          <p>Bạn có muốn mua thêm 2 lượt gửi bài với chỉ 1 credit? </p>
        </div>
        <div class="modal-footer">
	  <button type="button" class="btn btn-default" id="btn-contest-confirm-pay-credit" style="font-size: 14px" >Thanh toán</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: 14px">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--Popup thông báo không đủ credit-->
<div class="container">
  <!-- Modal -->
  <div class="modal fade hide" id="modal-popup-error-credit" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body" style="min-height: 30px;">
	  <span>Bạn đã sử dụng hết lượt gửi bài của mình trong cuộc thi này!</span>
          <p>Bạn có chắc muốn mua thêm 2 lượt gửi bài bằng 1 credit? </p>
	  <span style="color: red"> *Tài khoản của bạn không đủ credit</span>
        </div>
        <div class="modal-footer">
	  <button type="button" class="btn btn-default" id="btn-buy-more-credit" style="font-size: 14px" >Mua credit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" style="font-size: 14px">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $ready = true; ?>
<?php if(isset($bannedTo)): ?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-banned-account-when-bid">
        <h2><?php echo $view['translator']->trans('form.bid.banned.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.banned.reason', array('%bannedTo%' => $bannedTo), 'vlance')?></p>
        <p><?php echo $view['translator']->trans('form.bid.banned.support', array(), 'vlance')?></p>
    </div>
<?php elseif (count($missing_info) > 0) :?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-missing-info-when-bid">
        <h2><?php echo $view['translator']->trans('form.bid.missing_info.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.missing_info.reason', array(), 'vlance')?>:</p>
        <p><b><i><?php echo implode(' - ', $missing_info); ?></i></b></p>
        <p class="action-buttons">
            <a target="_blank" href="<?php echo $view['router']->generate('account_edit', array('id' => $current_user->getId())); ?>?bidform" class="btn btn-large btn-primary">
                <strong><?php echo $view['translator']->trans('form.bid.missing_info.action', array(), 'vlance')?></strong>
            </a>
        </p>
    </div>
<?php endif;?>
<form <?php if($bid_able):?>
        <?php if($ready): ?>
            id="contest-apply-form" action="<?php echo $view['router']->generate('contest_apply_create',array('id' => $id)) ?>" 
            method="post" <?php echo $view['form']->enctype($form)?>
            data-display-track="contest-view-apply-form"
        <?php endif; ?>
      <?php else:?>
            class="blurry-content"
            data-display-track="contest-view-apply-form"
      <?php endif;?>>
    <div class="row-fluid">
        <div class="span7">
            <div>
                <p>
                    Đính kèm file thiết kế ở đây.<br/>
                    <br/>
                    <em>Lưu ý bạn chỉ phải gửi cho khách hàng file thiết kế gốc khi mẫu thiết kế của bạn được khách hàng trao thưởng</em>.
                </p>
                <div class="file_upload">
                    <?php echo $view['form']->row($form['files']) ?>
                </div>
            </div>
            <div class="note-image">
                <ul>
                    <li>JPEG, PNG hay GIF</li>
                    <li>Dung lượng tối đa 10 MB.</li>
                    <li>Kích thước ảnh nhỏ nhất là 500 x 400px</li>
                </ul>
            </div>
            <div class="row-fluid block-submit">
                <p>
                    <b>Lưu ý:</b><br/>
                    Gửi bài dự thi hoàn toàn miễn phí. vLance chỉ thu phí khi khách hàng trao giải cho bạn. Mức phí là <b>20%</b> mức ngân sách của cuộc thi.<br/>
                    <br/>
                    Khi gửi bài dự thi, bạn đồng ý với điều khoản sử dụng của vLance.vn.</p>
                <div class="span5 block-submit-btn">
                    <?php echo $view['form']->row($form['_token']);?>
<!--                    <input type="button" id="btn-submit" value="<?php //echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?>" class="btn btn-primary btn-contest-apply span12"
                             onclick="vtrack('Click Submit bid', {'authenticated':'<?php //echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php //echo $job->getBudget(); ?>', 'version':'v2'})"/>-->
                    <input type="button" id="btn-contest-apply" value="<?php echo $view['translator']->trans('form.bid.contest_bid', array(), 'vlance'); ?>" class="btn btn-primary btn-contest-apply span12" />
		    <div style="min-height: 10px;"></div>
<!--		    Cho freelancer mua thêm lần gửi bài-->
		    <?php //if($display_pay_credit == 1){?>
<!--			<input type="button" id="btn-contest-pay-credit" value="Mua thêm lần gửi bài" class="btn btn-primary span12" style="font-size: 14px;" data-toggle="modal" data-target="#myModal"/>-->
		    <?php //} ?>
		    <?php if($display_pay_credit == 0): ?>
			
<!--			Được gửi bài-->
			<input type="hidden" value="0" id="popup-condition">
		    <?php elseif($display_pay_credit == 1): ?>
			
<!--			được mua thêm lượt-->
			<input type="hidden" value="1" id="popup-condition">
		    <?php elseif($display_pay_credit == 2): ?>
			
<!--			không đủ credit-->
			<input type="hidden" value="2" id="popup-condition">
		    <?php endif; ?>

                </div>
            </div>
        </div>
        <div class="span5">
            <p>Những điều cần phải làm theo</p>
            <div class="note-text">
                <p><i class="fa fa-check" aria-hidden="true"></i>Đọc và nắm rõ toàn bộ thông tin cuộc thi</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Đọc các đánh giá của khách trong trang tin nhắn.</p>
                <p><i class="fa fa-check" aria-hidden="true"></i>Không dùng hình ảnh có sẵn trên mạng hay sao chép</p>
            </div>
        </div>
    </div>
</form>
<script>
    $('#btn-contest-confirm-pay-credit').click(function(){
	location.replace("<?php echo $view['router']->generate('job_contest_pay_credit',array('hash' => $job->getHash()))?>");
    });
    
</script>
