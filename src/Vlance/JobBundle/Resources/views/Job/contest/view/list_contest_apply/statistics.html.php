<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php $now = new DateTime('now'); ?>

<div class="row-fluid">
    <div class="information-contest row-fluid">
        <div class="span4 client-bidding-left">
            <span class="bid-samples">
                Mẫu thiết kế: <b><?php echo $number_files; ?></b>
            </span>
        </div>

        <?php // Statistic of all bids ?>
        <div class="span4 client-bidding-between">
            <span>
                Giải thưởng: <b><?php echo number_format($job->getBudget(), 0, '', '.'); ?> VNĐ</b>
            </span>
        </div>
        <div class="span4 client-bidding-right">
            <span class="duration-average">
                <?php if($job->getStatus() != Job::JOB_FINISHED): ?>
                    <?php if($remain == false): ?>
                        <?php echo '<div class="has_expired_quote"><b>'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</b></div>'; ?>
                    <?php else: ?>
                        Chỉ còn: <b><?php echo $remain; ?></b>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo '<div class="has_expired_quote"><b>'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</b></div>'; ?>
                <?php endif; ?>
            </span>
        </div>
    </div>
</div>
