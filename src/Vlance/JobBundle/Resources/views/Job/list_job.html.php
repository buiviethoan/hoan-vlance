<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', $title_meta . " | vLance.vn"); ?>
<?php $view['slots']->set('og_title', $title_meta . " | vLance.vn");?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('img/og/danh-sach-viec-freelance.jpg'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('freelance_job_list'));?>
<?php $view['slots']->set('og_description', $description_meta);?>
<?php $view['slots']->set('description', $description_meta);?>

<?php // Set page index,follow or noindex,nofollow ?>
<?php $view['slots']->set('index_follow', $index_follow) ?>
<?php $view['slots']->set('top_block_listing_page', $view->render('VlanceJobBundle:Job/list_contest:tabs.html.php', array("current_route" => "freelance_job_list", 'count_jobs' => $count_jobs)));?>

<?php  $user = $app->getSecurity()->getToken()->getUser(); ?>

<?php $view['slots']->start('content') ?>

<?php //Breadcrum ?>
<div style="display: none;">
  <?php for($i = 0; $i < count($breadcrumbs); $i++) : ?>
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => $breadcrumbs[$i]['params'])) ?>" itemprop="url"> 
        <span itemprop="title"><?php echo $breadcrumbs[$i]['title'] ?></span>
      </a>
      <?php if($i < count($breadcrumbs)- 1) : ?>
        ›
      <?php endif; ?>
    </div>  
    <?php endfor; ?>
</div>
<?php // End Breadcrum ?>

<?php /* Trang list job */?>
<div class="search-block row-fluid">
    <?php echo $view->render('VlanceJobBundle:Job/list:top.html.php', array('user' => $user)) ?>
</div>

<?php echo $view->render('VlanceJobBundle:Job/list:content.html.php', array('entities' => $entities, 'pager' => $pager, 'pagerView' => $pagerView, 'listing_switch' => $listing_switch, 'current_user' => $user, 'onsite' => $onsite, 'contest' => $contest, 'first_page' => $first_page)); ?>

<?php if(!$no_param):?> 
    <?php echo $view->render('VlanceJobBundle:Job/list:bottom.html.php', array()) ?>
<?php endif;?>

<?php $view['slots']->stop(); ?>