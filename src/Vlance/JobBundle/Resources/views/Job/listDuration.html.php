<?php 
$params = $app->getSession()->get('filter');
if(!empty($params) && array_key_exists('duration',$params)) {
    unset($params['duration']);
}
$url = Vlance\BaseBundle\Utils\Url::buildUrl($params);
?>
<option selected="selected" value="<?php echo $view['router']->generate('job_list_project',array('filters' => $url),true) ?>">All</option>
<?php
    foreach($entities as $duration) {
         if(!empty($params)) {
             if(array_key_exists('duration',$params)) {
                 $params = array_replace($params,array('duration' => $duration['duration'])); 
                 $url = Vlance\BaseBundle\Utils\Url::buildUrl($params);
                echo '<option value="'.$view['router']->generate('job_list_project',array('filters' => $url)).'">'.$duration['duration'].' ngay</option>'.'<br />';
             }else {
                 $url = Vlance\BaseBundle\Utils\Url::buildUrl($params);
                echo '<option value="'.$view['router']->generate('job_list_project',array('filters' => $url.'-duration-'.$duration['duration'])).'">'.$duration['duration'].' ngay</option>'.'<br />';
             }

        }else {
            echo '<option value="'.$view['router']->generate('job_list_project',array('filters' => 'duration-'.$duration['duration'])).'">'.$duration['duration'].' ngay</option>'.'<br />';
        }
        }
?>