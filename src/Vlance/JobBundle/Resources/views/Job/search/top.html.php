<div class="inner-content new-job-block span12">
    <a class="btn btn-large btn-primary p-job" data-toggle="modal" 
       onclick="vtrack('Click post job', {'location':'List job page', 'authenticated':'<?php echo !is_object($user)?"false":"true"?>'})"
       href="<?php echo !is_object($user)?"#login-to-post-job":$view['router']->generate('job_new')?>">
        <?php echo $view['translator']->trans('list_job.right_content.free-post-job', array(), 'vlance') ?>
    </a>
    <div class="sub-text">
        <p><?php echo $view['translator']->trans('list_job.right_content.free-job', array(), 'vlance') ?><img src="/img/icon-v.png"></p> 
        <span><?php echo $view['translator']->trans('list_job.right_content.description', array(), 'vlance') ?></span>
    </div>
</div>

<?php if(!is_object($user)):?>
<div class="modal fade popup-login" id="login-to-post-job">
    <div class="modal-body">
        <div class="login_view_job">
            <div class="span3 btn-connect">
                <p class="first">Để <strong>Đăng việc</strong>, bạn phải <strong>có tài khoản</strong> tại vLance.vn</p>
                <div class="center">
                    <div class="btn-create-account">
                        <a class="btn btn-primary btn-large" href="<?php echo $view['router']->generate('account_register', array('type' => 'client')) ?>"
                           onclick="vtrack('Post job registration', {'type':'normal'})">Tạo tài khoản mới</a>
                    </div>
                    <p>hoặc đăng ký bằng</p>
                    <div class="job-login-socail">
                        <a class="btn-flat-new btn-facebook-flat" onclick="vtrack('Post job registration', {'type':'facebook'});fb_login();">Facebook</a>
                        <a class="btn-flat-new btn-google-flat" onclick="vtrack('Post job registration', {'type':'google'});google_login();">Google</a>
                        <a class="btn-flat-new btn-linkedin-flat" onclick="vtrack('Post job registration', {'type':'linkedin'});linkedin_login();">Linkedin</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>