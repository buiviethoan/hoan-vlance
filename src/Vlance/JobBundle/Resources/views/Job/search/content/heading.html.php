<div class="layered-heading results-counter">
    <ul class="nav nav-pills">
        <?php foreach($listing_switch as $status): ?>
            <li <?php if($status['active'] == true):?>class="active"<?php endif; ?>>
                <a <?php if($status['active'] == false):?>href="<?php echo $status['url']?>"<?php endif; ?>>
                    <?php echo $view['translator']->trans('list_job.filters.status.' . $status['name'], array(), 'vlance') ?> 
                    <b>(<?php echo $status['count']; ?>)</b>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
