<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $view['slots']->start('content'); ?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'ViewJob');
</script>
<?php endif; ?>
<?php // End Tracking ?>

<?php /* detail page */?>
<?php $now = new DateTime('now'); ?>

<div class="job-show-not-login">
    <div class="subhead-job">
        <div class="container">
            <div>
                <h3>
                    <?php echo ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                    <?php if($entity->getIsPrivate()):?>
                        <i class="fa fa-lock" title="<?php echo $view['translator']->trans('list_working.content.private_as_freelancer', array(), 'vlance')?>"></i>
                    <?php endif;?>
                    <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                        <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                    <?php endif;?>
                    <?php if($entity->getIsReviewed() == true): ?>
                        <span class="label-tag label-large label-green"
                              title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
                    <?php endif; ?>
                    <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                        <?php if($entity->getFeaturedFb() == false): ?>
                            <span class="label-tag label-large label-blue">NỔI BẬT</span>
                        <?php else: ?>
                        <?php //Feature facebook job-view ?>
                            <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($entity->getIsWillEscrow() == false): ?>
                        <span class="label-tag label-large label-blue" 
                              title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
                    <?php endif; ?>
                    <?php if($entity->getRequireEscrow() == true): ?>
                        <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                    <?php endif; ?>
                    <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                        <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                    <?php endif; ?>
                </h3>
                <div class="infor-job">Ngân sách <b><?php echo $entity->getJobBudget($view['vlance']->isAuthen()); ?></b> - 
                    <span class="remain">  
                        <?php 
                            $remain = JobCalculator::ago($entity->getCloseAt(), $now);
                            if($entity->getStatus() == Job::JOB_FINISHED){
                                echo $view['translator']->trans('list_job.right_content.has_finished', array(), 'vlance');
                            } elseif($entity->getStatus() == Job::JOB_WORKING){
                                echo $view['translator']->trans('list_job.right_content.is_working', array(), 'vlance');
                            } elseif($entity->getStatus() == Job::JOB_AWARDED){
                                echo $view['translator']->trans('list_job.right_content.has_awarded', array(), 'vlance');
                            } elseif($remain == false){
                                echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</div>';
                            }  else {
                                echo $view['translator']->trans('list_job.right_content.bidding_dealine_2', array(), 'vlance').': <b>'.$remain . '</b>' ;
                            }
                        ?>
                    </span>
                </div>
            </div>
            <div>
                <a class="btn btn-vl-medium btn-vl-orange" 
                   href="<?php echo $view['router']->generate('job_new'); ?>" title="Đăng dự án tương tự"
                   onclick="vtrack('Click Create similar job', {'authenticated':'false','location':'view job page', 'category':'<?php echo $entity->getCategory()?$entity->getCategory()->getTitle():'';?>', 'job':'<?php echo $entity->getTitle();?>'})">
                    Đăng dự án tương tự
                </a>
            </div>
        </div>    
    </div>
    <?php //echo $modulo; ?>
    <div class="description-view-job">
        <div class="container">
            <div class="row-fluid">
                <div class="span7">
                    <h4>Thông tin dự án</h4>
                    <div class="row-fluid body-view">
                        <?php if($entity->getService()): ?>
                            <div class="row-fluid">
                                <div class="service-need-hire">
                                    <div class="service-title">
                                        <?php echo $view['translator']->trans('view_job_page.job_detail.service.label', array(), 'vlance') ?>: <span><?php echo $entity->getService()->getTitle(); ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php /* AB test 1: Ẩn nội dung công việc và yêu cầu đăng nhập */ ?>
                            <?php /* Case "Test1-NEW" */ ?>
                            <?php if ($modulo < 9): ?>
                                <div class="description blurry-text-bao">
                                    <?php if(!$entity->getIsCompleted()): ?>
                                        <div class="login_view_job">
                                            <div class="span3 btn-connect">
                                                <p class="first">
                                                    <?php // echo $view['translator']->trans('site.menu.got_account', array(), 'vlance') ?> 
                                                    <strong>Để đọc thông tin, vui lòng đăng nhập bằng một trong hai cách sau:</strong>
                                                </p>
                                                <div class="center">
                                                    <div class="job-login-socail">
                                                        <a class="btn-facebook-login" onclick="fb_login()">
                                                            <span><img src="/img/icon-facebook.png"></span>
                                                        </a>
                                                        <a class="btn-google-plus-login" onclick="google_login()">
                                                            <span><img src="/img/icon-google+.png"></span>
                                                        </a>
                                                        <a class="btn-linkedin-login" onclick="linkedin_login()">
                                                            <span><img src="/img/icon-linkedin.png"></span>
                                                        </a>
                                                    </div>
            <!--                                                    <a class="btn btn-facebook" onclick="fb_login()">
                                                        <?php // echo $view['translator']->trans('site.menu.facebook_job', array(), 'vlance') ?>
                                                    </a>-->
                                                    <div class="job-login-vlance">
                                                        Hoặc
                                                        <a class="btn btn-primary" href="<?php echo $view['router']->generate('fos_user_security_login')?>">
                                                        <?php echo $view['translator']->trans('site.menu.login_job', array(), 'vlance') ?>
                                                        </a>
                                                    </div>
                                                </div>
                                                <p class="last"> 
                                                    Bạn chưa có tài khoản vLance? 
                                                    <a class="" href="<?php echo $view['router']->generate('account_register') ?>">
                                                        Đăng ký ngay
                                                    </a>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="<?php if(!$entity->getIsCompleted()): ?>blurry-text<?php endif; ?>">
                                        <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                                            <?php if (trim($line)): ?>
                                                <p><?php echo $line; ?></p>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php /* Case "Test1-OLD" */ ?>
                            <?php else: ?>
                                <div class="description">
                                    <?php if($entity->getDescription()): ?>
                                        <?php echo htmlentities(JobCalculator::cut(JobCalculator::replacement($entity->getDescription()),500), ENT_SUBSTITUTE, "UTF-8") ?> 
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

                        <?php // Attachment files ?>
                        <?php $files = $entity->getFiles(); ?>
                        <?php if(count($files)>0): ?>
                            <div class="attach">
                                <label><?php echo $view['translator']->trans('view_job_page.job_detail.attachment', array(), 'vlance').':' ?></label>
                                <ul>
                                    <?php foreach($files as $file) : ?>
                                        <li class="row-fluid">
                                            <div class="i32 i32-attach"></div>
                                            <a href="#popup-login" data-toggle="modal" 
                                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                            </a>
                                            <?php echo $view->render('VlanceJobBundle:Job:popup_login.html.php', array()) ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="span5">
                    <h4>Kỹ năng yêu cầu</h4>
                    <?php $skills = $entity->getSkills(); ?>
                    <?php if(count($skills) > 0 || $entity->getService()): ?>
                        <div class="skill-list">
                            <span>
                                <?php foreach ($skills as $skill): ?>
                                    <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                       title="Việc freelance <?php print $skill->getTitle(); ?>"><?php print $skill->getTitle() ?></a>
                                <?php endforeach;?>
                            </span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>  
    </div>   
    <div class="guide-fl-find-job">
        <div class="container">
            <h4>Bạn muốn tìm việc để tăng thu nhập?</h4>
            <div class="row-fluid">
                <div class="span7">
                    <ul>
                        <li>Gửi chào giá, nói rõ ngân sách dự kiến</li>
                        <li>Đưa ra kế hoạch thực hiện dự án</li>
                        <li>Nhận tiền thanh toán sau khi xong việc</li>
                    </ul>
                </div>
                <div class="span5">
                    <a href="<?php echo $view['router']->generate('account_register', array('type' => 'freelancer')) ?>" class="btn btn-vl-medium btn-vl-green" title="Gửi chào giá & Nhận việc">Gửi chào giá & Nhận việc</a>
                </div>
            </div>
        </div>
    </div>
    <div class="project-view-fl">
        <div class="container">
            <h4>Thuê freelancer đã chào giá cho dự án này</h4>
            <div class="job-view-inner">
                <ul class="job-view-usercard-list">
                    <?php if(count($bids) > 0): ?>
                        <?php foreach($bids as $b): ?>
                        <li class="usercard">
                            <div class="usercard-img">
                                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $b->getHash())) ?>" 
                                    title="<?php echo $b->getFullName() ?>">
                                     <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($b->getUploadDir().DS.$b->getPath(), '182x182', 182, 182, 1);?>
                                     <?php if($resize) : ?>
                                     <img src="<?php echo $resize; ?>" alt="<?php echo $b->getFullName(); ?>" title="<?php echo $b->getFullName(); ?>" />
                                     <?php else: ?>
                                     <img width="182" height="182" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $b->getFullName(); ?>" title="<?php echo $b->getFullName(); ?>" />
                                     <?php endif; ?>
                                 </a>
                            </div>
                            <div class="usercard-info">
                                <div class="usercard-name">
                                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $b->getHash())) ?>"><?php echo $b->getFullName(); ?></a>
                                    <img style="margin-top:-3px;margin-left:3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                                </div>
                                <div>
                                    <a class="btn btn-vl-small btn-vl-orange btn-vl-block" data-toggle="modal" href="#pay-sms" data-request='{"fid":<?php echo $b->getId();?>, "pid":-1}'
                                    onclick="vtrack('Click contact', {'authenticated':'false','location':'view job page', 'category':'<?php echo $b->getCategory()?$b->getCategory()->getTitle():''?>', 'freelancer':'<?php echo $b->getFullName()?>', 'freelancer_id':'<?php echo $b->getId()?>'})" title="Liên hệ trực tiếp">
                                    <?php echo $view['translator']->trans('list_freelancer.right_content.contact', array(), 'vlance') ?></a>
                                </div>
                            </div>
                            <div class="total-paid">
                                <span>Đã làm <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $b->getHash())) ?>"><?php echo number_format($b->getInCome(), 0, '', '.')  ?></a> VNĐ</span>
                            </div>
                            <?php if($b->getNumReviews() != 0): ?>
                            <div class="usercard-feedback">
                                <div class="rating-box" data-toggle="popover" 
                                    data-placement="right"  
                                    data-content="<?php echo number_format($b->getScore() / $b->getNumReviews(),'1',',','.') ?>"  
                                    data-trigger="hover">
                                    <?php $rating= ($b->getScore() / $b->getNumReviews()) * 20; ?>
                                    <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                                </div> 
                                <span class="number-job"><?php echo $b->getNumJobFinish() ?> việc</span>
                            </div>
                            <?php else: ?>
                            <div class="usercard-feedback">
                                <div class="rating-box">
                                    <div class="rating" style="width:0%"></div>
                                </div>
                                <span class="number-job"><?php echo $b->getNumJobFinish() ?> việc</span>
                            </div>
                            <?php endif; ?>
                                
                        </li>
                        <?php endforeach; ?>
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.rating-box').popover();
                            })
                        </script>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="more-bid-fl">
                <?php if (count($entity->getBids()) > 4): ?>
                    Và <?php echo count($entity->getBids()) - 4 ?> freelancer khác.
                <?php endif; ?>
                <a rel="nofollow" href="<?php echo $view['router']->generate('fos_user_security_login')?>">Đăng nhập để xem tất cả »</a>
            </div>
        </div>
    </div>
    <div class="professional-apply-section">
        <div class="container">
            <h3 class="title lh8 mp-lh6">
                Bạn có trên <span>3 năm kinh nghiệm</span>
                Và muốn tìm việc để <span>tăng thu nhập?</span>
            </h3>
            <div class="professional-apply-button">
                <a class="btn btn-vl btn-vl-special btn-vl-green" title="Đăng ký làm chuyên gia" href="<?php echo $view['router']->generate('account_register', array('type' => 'freelancer')) ?>">Đăng ký làm chuyên gia</a>
            </div>
        </div>      
    </div>
    <div class="landing-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php // SMS Payment popup?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>
<?php /* End detail page */?>
<?php $view['slots']->stop(); ?>