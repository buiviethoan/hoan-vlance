<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $view['slots']->start('content')?>
<?php /* clientview page */?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<?php /* @var $entity Vlance\JobBundle\Entity\Job */ ?>
<?php echo $view->render('VlanceJobBundle:Job/show_as_client:guider_block_after_post_job.html.php', array('current_step' => $entity->getCurrentStepStatus(), 'entity' => $entity, 'current_user' => $current_user, 'is_test_deposit_client' => $is_test_deposit_client)); ?>

<div class="content-section container client-view">    
    <div class="row-fluid">
        <div class="span12">
            <div class="row-fluid">
                <?php echo $view->render('VlanceJobBundle:Job/show:breadcrumb.html.php', array('entity' => $entity)) ?>
            </div>
            <div class="row-fluid">
                <h1 class="title">
                    <?php echo  ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                    <?php if($entity->getIsPrivate()):?>
                        <i class="fa fa-lock" title="<?php echo $view['translator']->trans('list_working.content.private_as_client', array(), 'vlance')?>"></i>
                    <?php endif;?>
                    <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                        <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                    <?php endif;?>
                    <?php if($entity->getIsReviewed() == true): ?>
                        <span class="label-tag label-large label-green"
                              title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
                    <?php endif; ?>
                    <?php if($entity->getIsWillEscrow() == false): ?>
                        <span class="label-tag label-large label-blue" 
                              title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
                    <?php endif; ?>
                    <?php if($entity->getRequireEscrow() == true): ?>
                        <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                    <?php endif; ?>
                    <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                        <?php if($entity->getFeaturedFb() == false): ?>
                            <span class="label-tag label-large label-blue">NỔI BẬT</span>
                        <?php else: ?>
                        <?php //Feature facebook job-view ?>
                            <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && is_null($entity->getUpfrontAt()) && is_object($current_user)): ?>
                        <?php if($entity->getAccount()->getId() == $current_user->getId()): ?>
                            <span class="label-tag label-large label-blue" title="Công việc chờ được kích hoạt">Chờ kích hoạt</span>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                        <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                    <?php endif; ?>
                </h1>
                <div class="edit-view span2">
                    <?php if(HasPermission::hasAdminPermission($current_user) == FALSE ) : ?>
                        <?php if($entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_OPEN) : ?>
                            <a href="<?php echo $view['router']->generate('job_edit',array('id' => $entity->getId())) ?>" class="edit-link"
                               title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                            </a>
                        <?php endif; ?>
                    <?php else : ?>
                        <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link"
                           title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                            <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid page-tabs">
                <ul>
                    <li class="active">
                        <a href="#">
                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                        </a>
                    </li>
                    <?php $workrooms = $entity->getWorkshopsJob(); ?>
                    <?php if(count($workrooms) > 0) : ?>
                        <li>
                            <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $entity->getId())) ?>">
                                <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="border"></div>
            </div>
            <div class="row-fluid">
                <div class="span7 news-client-view">
                    <?php echo $view->render('VlanceJobBundle:Job/show_as_client:job_description.html.php', array('entity' => $entity, 'abs_url' => $abs_url, 'current_user' => $current_user)); ?>
                </div>

                <div class="span4 job-information offset1">
                    <div class="job-information-inner">
                        <?php echo $view->render('VlanceJobBundle:Job/show_as_client:job_information.html.php', array('entity' => $entity, 'abs_url' => $abs_url, 'current_user' => $current_user, 'remain' => $remain)); ?>
                    </div>
                    
                    <div class="job-view-fb-page-box">
                        <h3>Like page để nhận được nhiều ưu đãi</h3>
                        <?php echo $view->render('VlanceJobBundle:Job/show:fanpage.html.php', array('entity' => $entity)) ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid">
                <?php if($entity->getFeaturedUntil() < new \DateTime()): ?>
                    <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:feature_job.html.php', array('job' => $entity, 'acc' => $current_user)); ?>
                <?php endif; ?>
                <?php if(!$is_test_deposit_client): ?>
                    <?php echo $view->render('VlanceJobBundle:Job/show_as_client:guider_block.html.php', array('current_step' => $entity->getCurrentStepStatus(), 'entity' => $entity)); ?>
                <?php endif; ?>
            </div>
            
            <div class="row-fluid">
                <?php /*acc duoc invite*/ ?>
                <div class="client-view-bottom span12">
                    <?php //"VlanceJobBundle:JobInvite:list.html.php"?>
                    <?php echo $view['actions']->render($view['router']->generate('jobinvite_list',array('jobId' => $entity->getId())));?>
                </div>
            </div>
            
            <div class="row-fluid">
                <?php /*List bid*/ ?>
                <div class="client-view-bottom span12">
                    <?php echo $view['actions']->render($view['router']->generate('job_show_bid',array('id' => $entity->getId())));?>
                </div>
            </div>
        </div>
    </div>
    <?php //popup share network social ?>
    <?php /* @var $session Symfony\Component\HttpFoundation\Session\Session */ ?>
    <?php $session = $app->getSession(); ?>
    <?php if($session->has('createjob')): ?>
        <?php echo $view->render('VlanceJobBundle:Job/show:share.html.php', array('url' => $view['router']->generate('job_show_freelance_job', array('hash' => $entity->getHash()), \Symfony\Component\Routing\Generator\UrlGenerator::ABSOLUTE_URL), 'title'=> $entity->getCategory()->getTitle() )) ?>
        <?php $session->remove('createjob'); ?>
    <?php endif; ?>
</div>
<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>

<?php /* End client page */?>
<?php $view['slots']->stop(); ?>
