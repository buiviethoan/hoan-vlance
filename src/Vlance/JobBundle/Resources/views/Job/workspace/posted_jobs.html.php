<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('list_working.project_created', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>

<div class="container link-to-thuengay">
    <div class="row-fluid">
        <div class="span12">
            <?php if($view['vlance']->autoHideBanner('21-09-2017 00:00:00', '29-09-2017 23:59:59') === true): ?>
                <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;width: 320px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150--banner-thuengay.jpg') ?>"/>
                </a>
            <?php else: ?>
                <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150.jpg') ?>"/>
                </a>
                <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150.jpg') ?>"/>
                </a>
                <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                </a>
                <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                </a>
                <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;width: 320px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150.jpg') ?>"/>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="content-section container">
    <div class="row-fluid">
        <div class="span12">
            <h1 class="title"><?php echo $view['translator']->trans('list_working.title', array(), 'vlance') ?></h1>
            <div class="row-fluid page-tabs">
                <?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
                <ul>
                    <li class="active">
                        <a href="<?php echo $view['router']->generate('jobs_create_project') ?>">
                            <?php echo $view['translator']->trans('list_working.job_create', array(), 'vlance') ?>
                        </a>
                    </li>
                    <?php /* <li>
                        <a href="#">Cuộc thi đã đăng</a>
                    </li> */ ?>
                </ul>
                <?php /* <ul>
                    <?php if($current_user->getType() == Vlance\AccountBundle\Entity\Account::TYPE_FREELANCER) : ?>
                        <li>
                            <a href="<?php echo $view['router']->generate('jobs_bid_project') ?>">
                                <?php echo $view['translator']->trans('list_working.job_bid', array(), 'vlance') ?>
                            </a>
                        </li>
                        <li class="active">
                            <a href="<?php echo $view['router']->generate('jobs_create_project') ?>">
                                <?php echo $view['translator']->trans('list_working.job_create', array(), 'vlance') ?>
                            </a>
                        </li>
                    <?php else : ?>
                        <li class="active">
                            <a href="<?php echo $view['router']->generate('jobs_create_project') ?>">
                                <?php echo $view['translator']->trans('list_working.job_create', array(), 'vlance') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $view['router']->generate('jobs_bid_project') ?>">
                                <?php echo $view['translator']->trans('list_working.job_bid', array(), 'vlance') ?>
                            </a>
                        </li> 
                    <?php endif; ?>
                </ul> */ ?>
                <div class="border"></div>
            </div>
            <div class="row-fluid">
                <div class="span12 news-client-view">
                    <?php // Không có job nào được tìm thấy ?>
                    <?php if(count($jobs) < 1) : ?>
                        <?php //Lọc không có kết quả ?>
                        <?php if($status['filters'] != ""): ?>
                            <div class="span4">
                                <label for="status_working">
                                    <?php echo $view['translator']->trans('list_working.content.status', array(), 'vlance').':' ?>
                                </label> 
                                <select class="span8" id="status_working" 
                                        onchange="window.open(this.options[this.selectedIndex].value,'_parent')">
                                    <?php echo $view['actions']->render($view['router']->generate('project_list_status', array('type' => 'client', 'filters' => $status['filters'])))?>
                                </select>
                            </div>
                            <div class="row-fluid filter-no-job-bid">
                                <p class="span12"><?php echo $view['translator']->trans('list_working.content.status_nojob', array(), 'vlance') ?></p>
                            </div>
                            <?php //Account chưa tạo job
                             else:  
                             ?>
                                <p><?php echo $view['translator']->trans('list_working.content.no_projects_job', array(), 'vlance') ?></p>
                                <div class="clear"></div>
                                <a class="btn btn-large create_job_free" href="<?php echo $view['router']->generate('job_new') ?>"
                                   onclick="vtrack('Click post job', {'location':'Workspace'})">
                                    <?php echo $view['translator']->trans('list_working.content.create_job_free', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                     <?php else: ?>
                        <div class="span4">
                            <label for="status_working"><?php echo $view['translator']->trans('list_working.content.status', array(), 'vlance').':' ?></label> 
                            <select class="span8" id="status_working" onchange="window.open(this.options[this.selectedIndex].value,'_parent')">
                                <?php echo $view['actions']->render($view['router']->generate('project_list_status', array('type' => 'client', 'filters' => $status['filters'])))?>
                            </select>
                        </div>
                    
                        <!--popover icon startus-->
                        <div class="span8">
                            <a href="javascript:void(0)" class="startus_icon"><?php echo $view['translator']->trans('list_working.icon', array(), 'vlance'); ?></a>
                        </div>
                        <div class="block-hidden">
                           <div class="startus_icon_show">
                               <div class="left startus-job">
                                   <h3><?php echo $view['translator']->trans('popover.icon_show.header1', array(), 'vlance') ?></h3>
                                   <ul>
                                       <li><i class="job_open"></i><?php echo $view['translator']->trans('popover.icon_show.job_open_client', array(), 'vlance') ?></li>
                                       <li><i class="job_expire"></i><?php echo $view['translator']->trans('popover.icon_show.job_expire', array(), 'vlance') ?></li>
                                       <li><i class="job_awarded"></i><?php echo $view['translator']->trans('popover.icon_show.job_awarded_client', array(), 'vlance') ?></li>
                                       <li><i class="job_working"></i><?php echo $view['translator']->trans('popover.icon_show.job_working_client', array(), 'vlance') ?></li>
                                       <li><i class="job_cancel_request"></i><?php echo $view['translator']->trans('popover.icon_show.job_cancel_request', array(), 'vlance') ?></li>
                                       <li><i class="job_cancel"></i><?php echo $view['translator']->trans('popover.icon_show.job_cancel', array(), 'vlance') ?></li>
                                       <li><i class="job_finished"></i><?php echo $view['translator']->trans('popover.icon_show.job_finished_client', array(), 'vlance') ?></li>
                                   </ul>
                               </div>
                               <div class="right payment-icon">
                                   <h3><?php echo $view['translator']->trans('popover.icon_show.header2', array(), 'vlance') ?></h3>
                                   <ul>
                                       <li><i class="payment_waiting_escrow"></i><?php echo $view['translator']->trans('popover.icon_show.payment_waiting_escrow_client', array(), 'vlance') ?></li>
                                       <li><i class="payment_escrowed"></i><?php echo $view['translator']->trans('popover.icon_show.payment_escrowed_client', array(), 'vlance') ?></li>
                                       <li><i class="payment_paied"></i><?php echo $view['translator']->trans('popover.icon_show.payment_paied', array(), 'vlance') ?></li>
                                       <li><i class="payment_refunded"></i><?php echo $view['translator']->trans('popover.icon_show.payment_refunded', array(), 'vlance') ?></li>
                                   </ul>
                               </div>
                           </div>
                           <script type="text/javascript">
                                $(document).ready(function() {
                                    $(".startus_icon").click(function(e) {
                                        $(".startus_icon_show").toggle();
                                        e.stopPropagation();
                                    });
                                    $('body').click(function() {
                                        $('.startus_icon_show').hide();
                                    });
                                    $('.startus_icon_show').click(function(e){
                                        e.stopPropagation();
                                    });
                                });
                            </script>                    
                        </div>
                        <!--end popover icon startus-->
                                
                        <table class="table">
                            <tr class="head-title-tb">
                                <th class="project-freelancer"><?php echo $view['translator']->trans('list_working.content.posted_projects', array(), 'vlance')?></th>
                                <th class="bid-freelancer"><?php echo $view['translator']->trans('list_working.content.bid_projects', array(), 'vlance')?></th>
                                <th class="payment-th"><?php echo $view['translator']->trans('list_working.content.payment', array(), 'vlance')?></th>
                                <th class="startus-th"><?php echo $view['translator']->trans('list_working.content.status', array(), 'vlance')?></th>
                                <th class="price-th"><?php echo $view['translator']->trans('list_working.content.sum_money', array(), 'vlance')?></th>
                            </tr>
                            <?php $countodd = 0?>
                            <?php foreach ($jobs as $job) : ?>
                            <tr class="<?php echo ++$countodd%2 ? "odd" : "even"  ?> updated">
                                <?php $new_message = $job->getNewMessages($current_user);?>
                                <td class="project-freelancer <?php if($new_message == true) { echo 'doing'; } ?>">
                                    <div class="title-job">
                                        <?php
                                            if($job->getType() === Job::TYPE_BID){
                                                $jurl = $view['router']->generate('job_show_freelance_job',array('hash' => $job->getHash()));
                                            } elseif($job->getType() === Job::TYPE_ONSITE) {
                                                $jurl = $view['router']->generate('job_onsite_view',array('hash' => $job->getHash()));
                                            } else {
                                                $jurl = $view['router']->generate('job_contest_view',array('hash' => $job->getHash()));
                                            }
                                        ?>
                                        <a href="<?php echo $jurl; ?>">
                                            <?php echo ucfirst(htmlentities($job->getTitle(), ENT_SUBSTITUTE, "UTF-8")) ?>
                                        </a>
                                        <?php if($job->getType() === Job::TYPE_CONTEST): ?>
                                            <span class="label-contest">Cuộc thi</span>
                                        <?php elseif($job->getType() === Job::TYPE_ONSITE): ?>
                                            <span class="label-tag label-purple">ONSITE</span>
                                        <?php endif; ?>
                                        <?php if($job->getIsPrivate()):?>
                                        <i class="icon-lock"
                                               data-toggle="popover" 
                                               data-placement="bottom" 
                                               data-content="<?php echo $view['translator']->trans('list_working.content.private_as_client', array(), 'vlance')?>" 
                                               data-trigger="hover"
                                               data-html="true">
                                        </i>
                                        <?php endif;?>
                                        
                                        <?php if($job->getType() === Job::TYPE_BID): ?>
                                            <?php if($job->getFeaturedUntil() >= new \DateTime()): ?>
                                                <?php if($job->getFeaturedFb() == false): ?>
                                                    <span class="label-tag label-blue">NỔI BẬT</span>
                                                <?php else: ?>
                                                    <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                                <?php endif; ?>
                                            <?php elseif($job->isOpen()): ?>
                                                <a class="btn btn-primary float-right" data-toggle="modal" href="#pay-feature-job" 
                                                    data-request='{"jid":<?php echo $job->getId();?>,"pid":<?php echo $current_user->getId();?>}'
                                                    onclick="vtrack('Click feature-job', {'location':'workspace'})"><?php echo $view['translator']->trans('list_working.content.featured', array(), 'vlance')?></a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        
                                    </div>

                                    <?php if($job->getType() !== Job::TYPE_ONSITE): ?>
                                    <div class="freelancer">
                                        <?php if($job->getWorker()): ?>
                                            <?php echo $view['translator']->trans('list_working.content.freelancer', array(), 'vlance').':'?>
                                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $job->getWorker()->getHash())) ?>"> 
                                                <?php echo $job->getWorker() ? htmlentities($job->getWorker()->getFullName(), ENT_SUBSTITUTE, "UTF-8") : '';  ?>
                                            </a>
                                        <?php else :  ?>
                                            <span><?php echo $view['translator']->trans('list_working.content.not_assigned', array(), 'vlance')?></span>
                                        <?php endif; ?>
                                    </div>
                                    <?php endif; ?>

                                    <?php /* Nếu job được giao việc thì lấy ngày giao việc còn ko thỳ lấy ngày bắt đầu của job */?>
                                    <?php if($job->getStartedAt()): ?>
                                        <div class="start_date">
                                            <?php echo $view['translator']->trans('list_working.content.start_date', array(), 'vlance').':'?> 
                                            <?php echo $job->getStartedAt()->format('d/m/Y'); ?>
                                        </div>
                                        <?php /*
                                         * * An ngay bat dau cong viec
                                        else: ?>
                                        <div class="start_date">
                                            <?php echo $view['translator']->trans('list_working.content.start_date', array(), 'vlance').':'?> 
                                            <?php echo $job->getStartedAt() ? $job->getStartedAt()->format('d/m/Y') :$job->getStartDate()->format('d/m/Y'); ?>
                                        </div>
                                         * */ ?>
                                    <?php endif; ?>
                                </td>
                                <td class="num-bid"><p><?php echo count($job->getBids())- $job->getfilterBids(); ?></p></td>
                                <td class="payment-icon">
                                    <?php       
                                    $class_popup_payment =  "payment_waiting_escrow";
                                    $data_content_popup_payment =  $view['translator']->trans('job.status_client.payment_waiting_escrow', array(), 'vlance');
                                    switch ($job->getPaymentStatusText()) {
                                        case 'job.status.payment_waiting_escrow': 
                                            $class_popup_payment =  "payment_waiting_escrow";
                                            $data_content_popup_payment = $view['translator']->trans('job.status_client.payment_waiting_escrow', array(), 'vlance');
                                            break;
                                        case 'job.status.payment_escrowed':
                                            $class_popup_payment =  "payment_escrowed";
                                            $data_content_popup_payment = $view['translator']->trans('job.status_client.payment_escrowed', array(), 'vlance');
                                            break;
                                        case 'job.status.payment_paied':
                                            $class_popup_payment =  "payment_paied";
                                            $data_content_popup_payment = $view['translator']->trans('job.status_client.payment_paied', array(), 'vlance');
                                            break;
                                        case 'job.status.payment_refunded':
                                            $class_popup_payment =  "payment_refunded";
                                            $data_content_popup_payment = $view['translator']->trans('job.status.payment_refunded', array(), 'vlance');
                                            break;
                                        default:
                                            $class_popup_payment =  "payment_waiting_escrow";
                                            $data_content_popup_payment = $view['translator']->trans('job.status_client.payment_waiting_escrow', array(), 'vlance');
                                            break;
                                    } ?>
                                    <i class='<?php echo $class_popup_payment; ?>'
                                        data-toggle="popover" 
                                        data-placement="bottom" 
                                        data-content="<?php echo $data_content_popup_payment; ?>" 
                                        data-trigger="hover">
                                    </i>
                                </td>
                                <td class="startus-job">
                                    <?php             
                                    $class_popup =  "job_open";
                                    $data_content_popup =  $view['translator']->trans('job.status_client.job_open', array(), 'vlance');
                                    switch ($job->getStatusText()) {                                            
                                        case 'job.status.job_open':
                                             $now = new DateTime('now');
                                             $remain = Vlance\BaseBundle\Utils\JobCalculator::ago($job->getCloseAt(), $now);
                                             if($remain) {
                                                $class_popup =  "job_open";
                                                $data_content_popup =  $view['translator']->trans('job.status_client.job_open', array(), 'vlance');
                                             } else {
                                                $class_popup = "job_expire";
                                                $data_content_popup = $view['translator']->trans('job.status_client.job_expire', array(), 'vlance');
                                                }

                                            break;
                                        case 'job.status.job_awarded':
                                            $class_popup =  "job_awarded";
                                            $data_content_popup =  $view['translator']->trans('job.status_client.job_awarded', array('%acc%' => $job->getWorker() ? $job->getWorker()->getFullName() : ''), 'vlance');
                                            break;
                                        case 'job.status.job_closed':
                                            $class_popup =  "job_closed";
                                            $data_content_popup = $view['translator']->trans('job.status_client.job_closed', array(), 'vlance');
                                            break;
                                        case 'job.status.job_working':
                                            $class_popup =  "job_working";
                                            $data_content_popup = $view['translator']->trans('job.status_client.job_working', array(), 'vlance');
                                            break;
                                        case 'job.status.job_finish_request':
                                            $class_popup =  "job_finish_request";
                                            $data_content_popup =  $view['translator']->trans('job.status_client.job_finish_request', array(), 'vlance');
                                            break;
                                        case 'job.status.job_finished':
                                            $class_popup =  "job_finished";
                                            $data_content_popup =  $view['translator']->trans('job.status_client.job_finished', array(), 'vlance');
                                            break;
                                        case 'job.status.job_cancel_request':
                                            $class_popup =  "job_cancel_request";
                                            $data_content_popup =  $view['translator']->trans('job.status.job_cancel_request', array(), 'vlance');
                                            break;
                                        case 'job.status.job_canceled':
                                            $class_popup =  "job_cancel";
                                            $data_content_popup =  $view['translator']->trans('job.status.job_canceled', array(), 'vlance');
                                            break;
                                        default:
                                            $class_popup =  "job_open";
                                            $data_content_popup =  $view['translator']->trans('job.status_client.job_open', array(), 'vlance');
                                            break;
                                    } ?>

                                    <i class='<?php echo $class_popup; ?>'
                                        data-toggle="popover" 
                                        data-placement="bottom" 
                                        data-content="<?php echo $data_content_popup; ?>" 
                                        data-trigger="hover">
                                    </i>
                                </td>
                                <td class="price-td">
                                    <?php echo number_format( $job->getBudgetBided() ? $job->getBudgetBided() : $job->getBudget() ,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                     <div class="row-fluid">
                        <?php if($pager->getNbPages() != 1) : ?>
                            <div class="layered-footer results-paging span12">
                                    <?php echo $pagerView->render($pager, $view['router']); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                   <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>

<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_featurejob.html.php', 
    array('acc'             => $current_user, 
        'form_name'         => 'pay-feature-job', 
        'context_msg_path'  => 'paysms.featurejob'))?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.news-client-view .table i').popover();
    })
</script>
<?php $view['slots']->stop(); ?>
