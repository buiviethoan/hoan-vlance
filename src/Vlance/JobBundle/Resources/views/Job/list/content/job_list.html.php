<?php use Vlance\AccountBundle\Entity\Account; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $acc_login = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<?php if($first_page === true): ?>
    <?php /*@var $entity Job */ ?>
    <?php $countOnsite = 0; ?>
    <?php foreach ($onsite as $o) :?>
        <?php if($o->getType() === Job::TYPE_ONSITE): ?>
            <?php $countOnsite++; ?>
            <?php if($countOnsite < 2): ?>
                <div class="row-fluid row-result job-onsite">
                    <div class="fr-info span12">
                        <div class="row-fluid">
                            <div class="span10">
                                <div class="row-fluid">
                                    <div class="span2">
                                        <a href="<?php echo $view['router']->generate('job_onsite_view',array('hash' => $o->getHash())) ?>">
                                            <?php $resize = ResizeImage::resize_image($o->getFullPath(), '96x96', 96, 96, 1);?>
                                            <?php if($resize) : ?>
                                            <img src="<?php echo $resize; ?>" alt="<?php echo $o->getTitle(); ?>" title="<?php echo $o->getTitle(); ?>" />
                                            <?php else: ?>
                                                <img width="96" height="96" src="/media/img01-job-onsite.jpg" alt="<?php echo $o->getTitle(); ?>"  title="<?php echo $o->getTitle(); ?>" />
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="span10">
                                        <h3 class="fr-name">
                                            <a href="<?php echo $view['router']->generate('job_onsite_view', array('hash' => $o->getHash())); ?>" title="<?php echo $o->getTitle(); ?>"><?php echo $o->getTitle(); ?></a>
                                            <span class="label-tag label-purple">Onsite</span>
                                        </h3>
                                        <div class="info-client">
                                            <div class="company-name-cl" title="Tên công ty"><?php echo $o->getOnsiteCompanyName(); ?></div>
                                            <div class="salary-fl" title="Mức lương"><i></i><?php echo number_format($o->getBudget(),'0',',','.'); ?> ₫ - <?php echo number_format($o->getBudgetMax(),'0',',','.'); ?> ₫</div>
                                        </div>
                                        <?php $skills = $o->getSkills(); ?>
                                        <?php if(count($skills) > 0 || $o->getService()): ?>
                                            <div class="skill-list span8 row-fluid">
                                                <span>
                                                    <?php foreach ($skills as $skill): ?>
                                                        <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                                           title="Việc freelance <?php print $skill->getTitle(); ?>"><?php print $skill->getTitle() ?></a>
                                                    <?php endforeach;?>
                                                </span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="block-right span2">
                                <ul>
                                    <li class="info-job place-work" title="Nơi làm việc"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $o->getCity()->getName(); ?></li>
                                    <li class="info-job date-begin-work" title="Ngày bắt đầu làm việc"><i></i><?php echo $o->getStartedAt()->format('d/m/Y'); ?></li>
                                    <li class="info-job time-work" title="Thời gian làm việc"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $o->getDuration(); ?> tháng</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="more-contest">
                        <a href="<?php echo $view['router']->generate('onsite_list', array()) ?>" title="Việc onsite">Xem tất cả việc onsite</a>
                    </div> 
                </div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach; ?>

    <?php $countContest = 0; ?>
    <?php foreach ($contest as $c) :?>
    <?php if($c->getType() === Job::TYPE_CONTEST): ?>
        <?php $countContest++; ?>
        <?php if($countContest < 2): ?>
            <?php if($c->getValid() == true): ?>
                <?php //GET all bids ?>
                <?php $arr = array(); $name = array(); //$arr chứa đường dẫn ảnh, $name chứa tên freelancer gửi ảnh?>
                <?php if(count($c->getBidsSuitable()) > 0): ?>
                    <?php foreach($c->getBidsSuitable() as $b): ?>
                        <?php foreach($b->getFiles() as $f): ?>
                            <?php $arr[] = $f->getPath().'/'.$f->getstoredName(); ?>
                            <?php $name[] = $b->getAccount()->getFullName(); ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php //END get all bids ?>

                <div class="row-fluid row-result job-contest">
                    <div class="fr-info span12">
                        <div class="span7">
                            <h3 class="fr-name">
                                <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>" title="<?php echo $c->getTitle()?>"><?php echo $c->getTitle()?></a>
                                <span class="label-tag label-yellow">Cuộc thi</span> <span class="label-tag label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                            </h3>
                            <div class="fr-title">
                                <div id="client-rating-box" class="list-job-rating">
                                    <?php $money_pay = $c->getAccount()->getOutCome();
                                        $info_client = '<div>Tiền đã thuê : <b>'.number_format($money_pay,0,',','.').'</b> VNĐ</div>'
                                                . '<div>Việc đã đăng : <b>'.$c->getAccount()->getNumJob().'</b> việc</div>'
                                                . '<div>Việc đã giao : <b>'.$c->getAccount()->getNumJobDeposit().'</b> việc</div>';
                                        $ratio = 0;
                                        if($money_pay > 0 && $money_pay <= 1000000) {
                                            $ratio = 20;
                                        } elseif($money_pay > 1000000 && $money_pay <= 5000000) {
                                            $ratio = 40;
                                        } elseif($money_pay > 5000000 && $money_pay <= 10000000) {
                                            $ratio = 60;
                                        } elseif($money_pay > 10000000 && $money_pay <= 50000000) {
                                            $ratio = 80;
                                        }elseif($money_pay > 50000000 ) {
                                            $ratio = 100;
                                        }
                                    ?>
                                    <div class="box-rating-client" data-toggle="popover" data-placement="right" 
                                         data-content="<?php echo $info_client; ?>" 
                                         data-trigger="hover" data-html="true" data-original-title="" title="">
                                        <div class="rating-box-client">
                                            <div class="rating-client" style="width:<?php echo $ratio.'%'; ?>"></div>
                                        </div>
                                    </div> 
                                </div>     
                                <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $c->getAccount()->getHash())) ?>" 
                                   title="<?php echo $c->getAccount()->getFullName() ?>">
                                    <span><?php echo $c->getAccount()->getFullName() ?></span>
                                </a>
                                <i class="icon-info icon-user" title="Tài khoản khách hàng đã có đủ thông tin cơ bản"></i>
                            </div>
                            <div class="data-contest">
                                <div class="row-fluid">
                                    <div class="span4"><?php echo number_format($c->getBudget(), 0, '', '.') ?> đ</div>
                                    <div class="span4"><?php echo count($arr); ?> bài dự thi</div>
                                    <div class="span4">
                                        <?php $remain = JobCalculator::ago($c->getCloseAt(), $now);
                                            if($c->getStatus() == Job::JOB_FINISHED){
                                                echo '<div>'.$view['translator']->trans('list_job.right_content.has_finished', array(), 'vlance').'</div>';
                                            } elseif($c->getStatus() == Job::JOB_WORKING){
                                                echo '<div>'.$view['translator']->trans('list_job.right_content.is_working', array(), 'vlance').'</div>';
                                            } elseif($c->getStatus() == Job::JOB_AWARDED){
                                                echo '<div>'.$view['translator']->trans('list_job.right_content.has_awarded', array(), 'vlance').'</div>';
                                            } elseif($remain == false){
                                                echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.' . ($c->getType() == Job::TYPE_BID ? "has_expired_quote" : "has_expired_apply"), array(), 'vlance').'</div>';
                                            }  else {
                                                echo $remain ;
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    <div class="span5">
                    <?php if($c->getIsPrivateContest() === false): ?>
                        <?php if(count($c->getBidsSuitable()) > 0): ?>
                            <?php if(count($c->getBidsSuitable()) > 2): ?>
                                <?php for($i = 0; $i < 3; $i++): ?>
                                    <?php //Resize image ?>
                                    <?php $urlImg = "/uploads/bid/".$arr[$i]; ?>
                                    <?php $resize = ResizeImage::resize_image(ROOT_DIR.$urlImg, '80x64', 80, 64, 1);?>
                                    <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                        <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                            <?php if($resize): ?>
                                                <div class="composition" style="background:url(<?php echo $resize;?>);background-repeat: no-repeat;background-size: cover;">
                                                    <?php if($i == 2): ?>
                                                        <?php if(count($c->getBidsSuitable()) > 3): ?>
                                                            <div class="background_banner"></div>
                                                            <div class="num-apply">+<span><?php echo count($arr) - 2; ?></span></div>    
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php else: ?>
                                                <img src="/img/job-contest/no-image.png" title="<?php echo $name[$i]; ?>" alt="<?php echo $name[$i]; ?>">
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                <?php endfor; ?>
                            <?php else: ?>
                                <?php for($i = 0; $i < count($c->getBids()); $i++): ?>
                                    <?php //Resize image ?>
                                    <?php if(isset($arr[$i])): ?>
                                        <?php $urlImg = "/uploads/bid/".$arr[$i]; ?>
                                        <?php $resize = ResizeImage::resize_image(ROOT_DIR.$urlImg, '80x64', 80, 64, 1);?>
                                        <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                            <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                                <?php if($resize): ?>
                                                    <div class="composition" style="background:url(<?php echo $resize;?>);background-repeat: no-repeat;background-size: cover;"></div>
                                                <?php else: ?>
                                                    <img src="/img/job-contest/no-image.png" title="<?php echo $name[$i]; ?>" alt="<?php echo $name[$i]; ?>">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    <?php else: ?>
                                        <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                            <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                                <img src="/img/job-contest/no-image.png" title="" alt="">
                                            </a>
                                        </div>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="product-fl no-apply">
                                <img src="/img/job-contest/no-image.png" title="Chưa có bài thi của freelancer" alt="Chưa có bài thi của freelancer">
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($c->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : ?>
                            <?php if(count($c->getBidsSuitable()) > 0): ?>
                                <?php if(count($c->getBidsSuitable()) > 2): ?>
                                    <?php for($i = 0; $i < 3; $i++): ?>
                                        <?php //Resize image ?>
                                        <?php $urlImg = "/uploads/bid/".$arr[$i]; ?>
                                        <?php $resize = ResizeImage::resize_image(ROOT_DIR.$urlImg, '80x64', 80, 64, 1);?>
                                        <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                            <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                                <?php if($resize): ?>
                                                    <div class="composition" style="background:url(<?php echo $resize;?>);background-repeat: no-repeat;background-size: cover;">
                                                        <?php if($i == 2): ?>
                                                            <?php if(count($c->getBidsSuitable()) > 3): ?>
                                                                <div class="background_banner"></div>
                                                                <div class="num-apply">+<span><?php echo count($arr) - 2; ?></span></div>    
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php else: ?>
                                                    <img src="/img/job-contest/no-image.png" title="<?php echo $name[$i]; ?>" alt="<?php echo $name[$i]; ?>">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    <?php endfor; ?>
                                <?php else: ?>
                                    <?php for($i = 0; $i < count($c->getBids()); $i++): ?>
                                        <?php //Resize image ?>
                                        <?php if(isset($arr[$i])): ?>
                                            <?php $urlImg = "/uploads/bid/".$arr[$i]; ?>
                                            <?php $resize = ResizeImage::resize_image(ROOT_DIR.$urlImg, '80x64', 80, 64, 1);?>
                                            <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                                <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                                    <?php if($resize): ?>
                                                        <div class="composition" style="background:url(<?php echo $resize;?>);background-repeat: no-repeat;background-size: cover;"></div>
                                                    <?php else: ?>
                                                        <img src="/img/job-contest/no-image.png" title="<?php echo $name[$i]; ?>" alt="<?php echo $name[$i]; ?>">
                                                    <?php endif; ?>
                                                </a>
                                            </div>
                                        <?php else: ?>
                                            <div class="product-fl <?php if($i == 2) echo "more"?> apply-fl">
                                                <a href="<?php echo $view['router']->generate('job_contest_view',array('hash' => $c->getHash()))?>">
                                                    <img src="/img/job-contest/no-image.png" title="" alt="">
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="product-fl no-apply">
                                    <img src="/img/job-contest/no-image.png" title="Chưa có bài thi của freelancer" alt="Chưa có bài thi của freelancer">
                                </div>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="product-fl no-apply">
                                <img src="/img/job-contest/no-image.png" title="Cuộc thi bí mật" alt="Cuộc thi bí mật">
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    </div>
                </div>
                <div class="more-contest">
                    <a href="<?php echo $view['router']->generate('contest_list', array()) ?>" title="Xem tất cả cuộc thi">Xem tất cả cuộc thi ></a>
                </div>     
            </div>
        <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
    <?php endforeach; ?>
<?php endif;?>

<?php foreach ($entities as $entity) :?>
<?php if($entity->getType() == Job::TYPE_BID): ?>
<div class="row-fluid row-result <?php echo ($entity->getFeaturedUntil() >= $now)?"featured-job":"" ?>">
    <div class="fr-info span12">
        <?php if ($entity->getFeaturedUntil() >= $now): ?>
        <?php if($entity->getFeaturedFb() == false): ?>
        <div class="special-banner">
            <div class="banner-featured-job" data-toggle="tooltip" data-placement="top" title="Công việc được nổi bật" alt="Công việc được nổi bật">Nổi bật</div>
        </div>
        <?php else: ?>
        <?php //feature fanpage ?>
        <div class="special-banner">
            <div class="banner-featured-job featured-fanpage" data-toggle="tooltip" data-placement="top" title="Công việc được nổi bật trên fanpage của vLance">Nổi bật</div>
        </div>
        <?php endif; ?>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready(function(){
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip()
                    })        
                });
            </script>
        <?php endif; ?>
        <h3 class="fr-name">
            <?php if($entity->getTitle()): ?>    
                <a href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())) ?>">
                    <?php echo ucfirst (htmlentities($entity->getTitle(), ENT_SUBSTITUTE, "UTF-8")); ?>
                </a>
                <?php if($entity->getIsPrivate() === true): ?>
                    <i class="fa fa-lock" title="Việc riêng" alt="Việc riêng"></i>
                <?php endif; ?>
            <?php endif; ?>
            <?php // print NEW label if job created within 24h ?>
            <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                <span class="label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
            <?php endif;?>
            <?php if($entity->getIsReviewed() == true): ?>
                <span class="label-tag label-green"
                      title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
            <?php endif; ?>
            <?php if($entity->getRequireEscrow() == true): ?>
                <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
            <?php endif; ?>
            <?php if($entity->getPaymentStatus() >= Job::ESCROWED): ?>
                <span class="label-tag label-green"><?php echo $view['translator']->trans('list_job.right_content.job_funded', array(), 'vlance'); ?></span>
            <?php endif; ?>
            <?php if($entity->getIsWillEscrow() == false): ?>
                <span class="label-tag label-blue" 
                      title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
            <?php endif; ?>
                <?php //print_r($current_user); die; ?>
            <?php if($entity->getPaymentStatus() < Job::ESCROWED && $entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && is_null($entity->getUpfrontAt()) && is_object($current_user)): ?>
                <?php if($entity->getAccount()->getId() == $current_user->getId()): ?>
                    <span class="label-tag label-blue" title="Công việc chờ được kích hoạt">Chờ kích hoạt</span>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($entity->getPaymentStatus() < Job::ESCROWED && $entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                <span class="label-tag label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
            <?php endif; ?>
        </h3>
        <div class="fr-title">
            <?php /* @var $acc Account */ ?>
            <?php $acc = $entity->getAccount(); ?>
            <?php if($acc): ?>  
            <div id="client-rating-box" class="list-job-rating">
                <?php 
                    $money_pay = $acc->getOutCome();
                    $info_client = '<div>Tiền đã thuê : <b>'.number_format($money_pay,0,',','.').'</b> VNĐ</div>'
                            . '<div>Việc đã đăng : <b>'.$acc->getNumJob().'</b> việc</div>'
                            . '<div>Việc đã giao : <b>'.$acc->getNumJobDeposit().'</b> việc</div>';
                    $ratio = 0;
                    if($money_pay > 0 && $money_pay <= 1000000) {
                        $ratio = 20;
                    } elseif($money_pay > 1000000 && $money_pay <= 5000000) {
                        $ratio = 40;
                    } elseif($money_pay > 5000000 && $money_pay <= 10000000) {
                        $ratio = 60;
                    } elseif($money_pay > 10000000 && $money_pay <= 50000000) {
                        $ratio = 80;
                    }elseif($money_pay > 50000000 ) {
                        $ratio = 100;
                    }
                ?>
                <div class="box-rating-client" data-toggle="popover" 
                    data-placement="right"  
                    data-content="<?php echo $info_client ?>"  
                    data-trigger="hover" data-html="true">
                    <div class="rating-box-client">
                        <div class="rating-client" style="width:<?php echo $ratio.'%'; ?>"></div>
                    </div>
                </div> 
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.box-rating-client').popover();
                    })
                </script>
            </div>     
            <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" 
               title="<?php echo $acc->getFullName() ?>">
                <span><?php echo $acc->getFullName() ?> </span>
            </a>
            <?php if($acc->getIsMissingClientInfo()):?>
                <i class="icon-info icon-user inactive" title="Tài khoản khách hàng thiếu thông tin cơ bản"></i>
            <?php else:?>
                <i class="icon-info icon-user" title="Tài khoản khách hàng đã có đủ thông tin cơ bản"></i>
            <?php endif;?>
            <?php endif; ?>
        </div>
        <div class="fr-summary row-fluid">
            <div class="history span8">
                <span class="location">
                    <?php echo $entity->getCity() ? $entity->getCity()->getName() : '' ?>
                </span>
                <span class="separator">|</span>
                <?php $category = $entity->getCategory(); ?>
                <?php if($category): ?> 
                    <span class="category"><?php echo $category->getTitle() ?></span>
                <?php endif; ?>
                <span class="separator hidejs" style="display:none">|</span>

                <?php /*
                 * An thoi luong cua job
                <?php if($entity->getDuration()): ?> 
                    <?php echo $entity->getDuration().' '.$view['translator']->trans('list_job.right_content.date', array(), 'vlance') ?>
                    <span class="separator">|</span>
                <?php endif; ?>
                 * */ ?>

                <span class="history-job hidejs" style="display:none"><?php echo $entity->getJobBudget($view['vlance']->isAuthen()); ?>
                    <?php if(!$view['vlance']->isAuthen()): ?>
                    <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" data-placement="bottom" data-html="true" data-content="Vui lòng <a href='#'>đăng nhập</a> để biết mức ngân sách thực tế."></a>
                    <?php endif; ?>
                </span>
            </div>
            <div class="span4 remain-job hidejs" style="display:none">
                <div class="remain">  
                    <?php 
                        $remain = JobCalculator::ago($entity->getCloseAt(), $now);
                        if($entity->getStatus() == Job::JOB_FINISHED){
                            echo '<div>'.$view['translator']->trans('list_job.right_content.has_finished', array(), 'vlance').'</div>';
                        } elseif($entity->getStatus() == Job::JOB_WORKING){
                            echo '<div>'.$view['translator']->trans('list_job.right_content.is_working', array(), 'vlance').'</div>';
                        } elseif($entity->getStatus() == Job::JOB_AWARDED){
                            echo '<div>'.$view['translator']->trans('list_job.right_content.has_awarded', array(), 'vlance').'</div>';
                        } elseif($remain == false){
                            echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</div>';
                        }  else {
                        echo $view['translator']->trans('list_job.right_content.bidding_dealine', array(), 'vlance').': '.$remain ;
                        }
                    ?>
                    <?php /*
                        $remain = JobCalculator::remain($entity->getCloseAt());
                        $remain_format = JobCalculator::remainFormat($entity->getCloseAt());

                        // Nếu số ngày còn lại là âm, thì hiện text "đã hết hạn"
                        if($remain->invert == 1){
                            echo '<div class="has_expired_quote">'.$view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance').'</div>';
                        }  else {
                            echo $view['translator']->trans('list_job.right_content.remain', array(), 'vlance').': '.$remain_format ;
                        }
                    */ ?>
                </div>
            </div>
        </div>
        <div class="fr-service">
            <?php if($entity->getIsPrivate() === false): ?>
                <?php if($entity->getDescription()): ?>
                    <?php echo htmlentities(JobCalculator::cut(JobCalculator::replacement($entity->getDescription()),150), ENT_SUBSTITUTE, "UTF-8") ?>
                    <a class="read_more" href="<?php echo $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())) ?>">
                        <?php echo $view['translator']->trans('list_job.right_content.read_more', array(), 'vlance') ?>
                    </a>
                <?php endif; ?>
            <?php else: ?>
            <p>
                <i>Việc bí mật. Chỉ khách hàng và freelancer được mời xem được nội dung.</i>
            </p>
            <?php endif; ?>
        </div>
        <div class="fr-profile row-fluid">
            <?php $skills = $entity->getSkills(); ?>
            <?php if(count($skills) > 0 || $entity->getService()): ?>
                <div class="skill-list span8 row-fluid">
                    <?php if($entity->getService()):?>
                        <span class="service-tag"><a title='Cần thuê dịch vụ "<?php echo $entity->getService()->getTitle(); ?>"'><?php echo $entity->getService()->getTitle(); ?></a></span>
                    <?php endif; ?>
                    <span>
                        <?php foreach ($skills as $skill): ?>
                            <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                               title="Việc freelance <?php print $skill->getTitle(); ?>"><?php print $skill->getTitle() ?></a>
                        <?php endforeach;?>
                    </span>
                </div>
            <?php endif; ?>
            <div class="span4">
                <div class="number_bid">
                    <?php if($entity->getStatus() !== Job::JOB_OPEN) : ?>
                        <span>
                            <?php echo $view['translator']->trans('list_job.right_content.num_bid_job', array('%num%'=> '<strong>' . count($entity->getBids()) . '</strong>'), 'vlance') ?>
                        </span><br/>
                        <span>
                            <?php if($entity->getWorker()): ?>
                                <?php echo '<a href="'.$view['router']->generate('account_show_freelancer',array('hash' => $entity->getWorker()->getHash())).'">'.$entity->getWorker()->getFullName().'</a> ' . $view['translator']->trans('list_job.right_content.awared', array(), 'vlance')?>
                            <?php endif; ?>
                        </span>
                    <?php else:?>
                        <span>
                            <?php echo $view['translator']->trans('list_job.right_content.num_bid_job', array('%num%'=> '<strong>' . count($entity->getBids()) . '</strong>'), 'vlance') ?> 
                        </span>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<?php endforeach; ?>