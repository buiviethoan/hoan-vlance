<div class="search-content row-fluid">
    <div class="layered-block search-results span12">
        <div>
            <?php echo $view->render('VlanceJobBundle:Job/list/content:heading.html.php', array('pager' => $pager, 'listing_switch' => $listing_switch)) ?>
        </div>
        <div class="block-content">
            <?php if($pager->getNbResults() > 0): ?>
                <?php echo $view->render('VlanceJobBundle:Job/list/content:job_list.html.php', array('entities' => $entities, 'current_user' => $current_user, 'onsite' => $onsite, 'contest' => $contest, 'first_page' => $first_page)) ?>
            <?php else: ?>
                <div class="row-fluid row-result">
                    <div class="fr-info span12">
                        <?php echo $view['translator']->trans('list_job.right_content.no_num_job_search', array(), 'vlance') ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php if($pager->getNbPages() != 1) : ?>
        <div>
            <p class="layered-footer results-paging">
                <?php echo $pagerView->render($pager, $view['router']); ?>   
            </p>
        </div>
        <?php endif; ?>
    </div>
</div>
