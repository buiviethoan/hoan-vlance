<?php use Vlance\BaseBundle\Utils\HasPermission; ?> 
<?php use Vlance\JobBundle\Entity\JobOnsiteOption; ?>

<?php /* if(count($bid_current) > 0) : ?>
    <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:feature_bid.html.php', array('job' => $job, 'acc' => $acc_login, 'bid' => $bid_current[0])); ?>
<?php endif; */ ?>

<?php echo $view->render('VlanceJobBundle:Job/onsite/list_bid:statistics.html.php', array( 'job' => $job, 'bids' => $bids, 'bid_current' => $bid_current, 'number_declined' => $number_declined)) ?>

<?php //Tính số CV đc hiện
    $num = 10 + (int)($view['vlance_job']->countCvJobOnsite($job->getId(), JobOnsiteOption::ONSITE_OPTION_EXTEND_10)) * 10
            + (int)($view['vlance_job']->countCvJobOnsite($job->getId(), JobOnsiteOption::ONSITE_OPTION_EXTEND_30)) * 30; 
?>


<?php /* Dưới số đấu thầu */?>
<div class="row-fluid">
    <?php // There is no bid ?>
    <?php if(count($bids) == 0 && count($bid_current) == 0) : ?>
        <?php if($number_declined > 0): ?>
            <div class="no-profile-job">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php else: ?> 
            <div class="no-profile-job">
                Hiện tại chưa ai gửi CV cho công việc này.
            </div>
        <?php endif; ?> 

    <?php // There are some bids ?>
    <?php else : ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : ?>
            <?php $temp = 'VlanceJobBundle:Job/onsite/show:as_client.html.php'; ?>
        <?php else: ?>
            <?php $temp = 'VlanceJobBundle:Job/onsite/show:as_freelancer.html.php'; ?>
        <?php endif; ?>
    
        <?php // View bid account current ?>
        <?php if(count($bid_current) > 0) : ?>
            <?php echo $view->render('VlanceJobBundle:Job/onsite/show:as_bid_owner.html.php',array('entity' => $bid_current[0], 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
        <?php endif; ?>
        
        <?php // Calculating distance for intersect ad in the list bids ?>
        <?php $distance = ceil(count($bids) / ((round(count($bids)/10)==0)?1:round(count($bids)/10))); ?>
        <?php $i = 0; ?>
        
        <?php // View all bid ?>
        <?php foreach ($bids as $entity) :?>
            <?php $i++; ?>
            <?php if ($entity->getAccount() == $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : // As Admin?>
                <?php echo $view->render('VlanceJobBundle:Job/show_as_client/bid:as_client.html.php',array('entity' => $entity, 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
                <?php else : // As Client or Freelancer, check $temp ?>
                    <?php if($job->getAccount() === $acc_login): ?>
                        <?php if($i <= $num): ?>
                            <?php echo $view->render($temp,array('entity' => $entity, 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php echo $view->render($temp,array('entity' => $entity, 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
                    <?php endif; ?>
                    <?php // Show intersect Ads ?>
                    <?php // if($i%$distance==0 && $i!=count($bids)):?>
                        <?php // echo $view->render('VlanceJobBundle:Job/show/list_bid:ad_intersect.html.php',array()) ?>
                    <?php // endif;?>
                <?php endif; ?>    
        <?php endforeach;?>

        <?php // số bid bị từ chối ?>
        <?php  if($number_declined > 0): ?>
            <div class="count_bid_declined span12">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php endif; ?>

        <?php // Popups ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE): ?>
            <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:popups.html.php',array('acc_login' => $acc_login, 'entity' => $entity, 'job' => $job)) ?>
        <?php endif; ?>
    <?php endif; ?>       
</div>

<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function() {
        jQuery("a.formmessage-call-popup").click(function(){
            jQuery("#formmessage form").attr("action", jQuery(this).attr("data-action")); 
            jQuery("#formmessage h3 span").html(jQuery(this).attr("data-uname")); 
        });
        
        jQuery("a.report-violations-call-popup").click(function(){
            jQuery("#popup-report_violations .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
        });
        
        jQuery("a.refusal-call-popup").click(function(){
            jQuery("#popup-refusal .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
        });        
        
        jQuery("a.formassignment-call-popup").click(function(){
            var arr = jQuery.parseJSON(jQuery(this).attr("data-file"));
            var files = "";
            
            for (i=0; i < arr.length; i++){
                files += "<li class='row-fluid upload'><span class='upload'>"+ arr[i] +"<span></li>";
            }
            jQuery("#formassignment").attr("tabindex", jQuery(this).attr("data-id")); 
            jQuery("#formassignment .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
            jQuery("#formassignment .modal-body #assignment-full-name span").html(jQuery(this).attr("data-uname")); 
            jQuery("#formassignment .modal-body #assignment-description").html(jQuery(this).attr("data-description")); 
            jQuery("#formassignment .modal-body #assignment-city span").html(jQuery(this).attr("data-city")); 
            jQuery("#formassignment .modal-body #assignment-price span").html(jQuery(this).attr("data-price")); 

             if(files !== ""){
                 jQuery('#formassignment .modal-body .assigment-file .title-assigment-file').html("<strong><?php echo $view['translator']->trans('view_bid__page.popup.file', array(), 'vlance').':' ?></storng>");
             };
            jQuery("#formassignment .modal-body .assigment-file ul").html(files); 
        });
        
        $("#formmessage form .button input").click(function() {
          <?php  // Validate cac input file ?>       
           var validate = true;
           if(validate_fileinput('#file_upload_container .new_file_input') == false){
               validate_fileinput('#file_upload_container .new_file_input');
               validate = false;
           }
           if(validate_fileinput('#file_upload_container .new_file_input') == true ){
           $('#file_upload_container .new_file_input label.error').remove();
               if($("#formmessage #fileupload").valid()){
                   $("#formmessage form .button input").submit();
               }
           }

       });
    });
    /* ]]> */
</script>
