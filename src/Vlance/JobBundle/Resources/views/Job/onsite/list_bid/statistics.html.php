<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php $now = new DateTime('now'); ?>
<div class="row-fluid">
    <div class="information-bidding row-fluid">
        <div class="span4 client-bidding-left">
            <span class="bid-counter">
                Số lượng CV:
                <span class="value">
                    <?php $countbid = count($bids) + $number_declined + count($bid_current); ?>
                    <b><?php echo $countbid; ?></b>
                </span>
            </span>
        </div>

        <div class="span4 client-bidding-right">
            <span class="duration-average">
                Hạn nộp CV:
                <span class="value">
                    <?php $remain = JobCalculator::ago($job->getCloseAt(), $now);?>
                    <b><?php echo ($remain == false) ? 'Đã hết hạn' : $remain ; ?></b>
                </span>
            </span>
        </div>
    </div>
</div>