<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>

<?php $view['slots']->set('og_image', 'http://'.$app->getRequest()->getHost() . $view['assets']->getUrl('img/og/dang-viec-freelance.jpg'));?>
<?php $view['slots']->set('title', $view['translator']->trans('Đăng việc onsite', array(), 'vlance'));?>
<?php $view['slots']->set('og_title', "Đăng việc onsite mới");?>
<?php $view['slots']->set('og_url', 'http://'.$app->getRequest()->getHost() . $view['router']->generate('job_onsite_new'));?>
<?php // $view['slots']->set('description', "Ngay sau khi đăng, bạn sẽ ngay lập tức nhận được hàng chục chào giá từ các freelancer hàng đầu Việt Nam.");?>

<?php $view['slots']->start('content')?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<div class="content-section container create-job post-job-onsite">
    <h1 class="new_job_title">Đăng tin tuyển dụng việc On-site</h1>
    
    <?php /* Form đăng nhập/đăng ký */ ?>
    <?php if(!is_object($acc)): ?>
        <?php echo $view->render('VlanceBaseBundle:Block/RegLog:form.html.php', array()) ?>
    <?php endif; ?>
    <script type="text/javascript">
        var logged = '<?php if(is_object($acc)){echo "true";} else {echo "false";}?>';
        var reg_form_in_job = '#reg-form';
        var reg_form_in_job_params = {"url":"<?php echo $view['router']->generate('register_job') ?>"};
        var login_form_in_job = '#log-form';
        var login_form_in_job_params = {"url":"<?php echo $view['router']->generate('login_job') ?>"};
        var credit = '<?php if(is_object($acc)){echo $acc->getCredit()->getBalance();} else {echo 0;}; ?>';
    </script>
    
    <div class="row-fluid block-guide">
        <div class="title">Lợi ích khi đăng việc on-site:</div>
        <div class="content">
            <p>Phỏng vấn và trao đổi trực tiếp với các ứng viên trên hệ thống của vLance.</p>
            <p>Tin tuyển dụng sẽ được hiển thị liên tục trong <b>7 ngày</b> chỉ với <b>20 credit</b>.</p>
            <p>Nhận đến <b>10 CV chất lượng</b> trong số các CV ứng tuyển.</p>
        </div>
    </div>
    
    <?php /* Form đăng ký onsite */ ?>
    <div class="post-job">    
        <form id="edit-job-form" action="<?php echo $view['router']->generate('job_onsite_submit'); ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
            <?php echo $view->render('VlanceJobBundle:Job/onsite/edit:form.html.php', array('form' => $form, 'entity' => $entity, 'type' => 'new')) ?>
        </form>
    </div> 
    <div class="row-fluid">
        <div class="offset2 span10 form-cread-job-btn">
            <p>Bạn bấm nút "Đăng tuyển dụng", bạn xác nhận đồng ý điều khoản sử dụng vLance.vn</p>
            <?php if(!is_object($acc)): ?>
                <input id="btn-submit-job" type="button" class="btn btn-primary btn-large popup-validate span12" value="Đăng tuyển dụng" 
                  onclick="vtrack('Submit onsite job')" />
            <?php else: ?>
                <?php // if($form_able === true): ?>
                    <!--<input id="btn-submit-job" type="button" class="btn btn-primary btn-large popup-validate span12" value="Đăng tuyển dụng"--> 
                    <!--onclick="vtrack('Submit onsite job')" />-->
                <?php // else: ?>
                    <a href="#creditOnsite" role="button" class="btn btn-primary btn-large span12" data-toggle="modal" onclick="countCredit();"/>Đăng tuyển dụng</a>
                <?php // endif; ?>
            <?php endif; ?>
        </div>
    </div>    
</div>
 
<!-- Modal -->
<div id="creditOnsite" class="modal hide fade popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Đăng tin tuyển dụng việc On-site</h4>
    </div>
    <div class="modal-body">
        <p>Ngay sau việc on-site được đăng thành công bạn sẽ nhận được CV và thông tin liên lạc của các ứng viên để phỏng vấn.</p>
        <br/>
        <p id="totalCredit" style="text-align: center">
            Tổng chi phí: <b>20 CREDIT</b>
        </p>
        <br/>
        <p style="text-align: center">
            <span style="color: #999999">Bạn hiện có: <?php if(is_object($acc)){echo $acc->getCredit()->getBalance();} ;?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
        </p>
        <p id="sys-message" style="text-align: center"></p>
    </div>
    <div class="modal-footer">
        <span style="margin-right: 10px;"><a href="#" data-dismiss="modal" aria-hidden="true">Quay lại</a></span>
        <span><input id="btn-submit-job" type="button" class="btn btn-primary btn-large popup-validate" value="Đăng tuyển dụng" onclick="vtrack('Submit onsite job')" /></span>
    </div>
</div>
<?php $view['slots']->stop();?>