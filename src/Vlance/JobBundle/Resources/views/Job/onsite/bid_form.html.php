<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<?php /* @var $acc_login Vlance\AccountBundle\Entity\Account; */ ?>
<?php $acc_login = $app->getSecurity()->getToken()->getUser(); ?>

<div class="content-cv">
    <h2 class="title">Gửi CV để nhận việc</h2>
    <div class="row-fluid">
        <form id="bid_form" action="<?php echo $view['router']->generate('bid_onsite_submit',array('id' => $id)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
            <div class="row-fluid">
                <div class="content-col-left span7">
                    <div class="file-attach">
                        <h3 class="title">Đính kèm CV cá nhân ở đây <span class="text-red">*</span></h4>
                        <div class="fileupload_wrapper">
                            <?php echo $view['form']->row($form['files']) ?>
                        </div>
                    </div>
                    <div class="contact-info">
                        <div class="row-fluid">
                            <h3 class="title">Thông tin liên hệ của bạn <span class="text-red">*</span></h4>
                            <div class="input-prepend telephone span6">
                                <span class="add-on">
                                    <i class="fa fa-phone block-contact-icon" aria-hidden="true"></i>
                                </span>
                                <input required="required" id="bidform_telephone" name="input_telephone" value="<?php echo $acc_login->getTelephone() ?>" placeholder="Số điện thoại">
                            </div>
                            <div class="input-prepend skype span6">
                                <span class="add-on"><i class="fa fa-skype block-contact-icon" aria-hidden="true"></i></span>
                                <input required="required" id="bidform_skype" name="input_skype" value="<?php echo $acc_login->getSkype() ?>" placeholder="Skype">
                            </div>
                        </div>
                    </div>
                    <div class="description">
                        <?php echo $view['form']->row($form['description'])?>
                    </div>
                    <div class="row-fluid">
                        <div class="block-submit span12">
                            <div class="block-submit-btn">
                                <?php echo $view['form']->row($form['_token']);?>
                                <input type="submit" id="btn-submit" value="Gửi CV" class="btn btn-large btn-vl-blue btn-block"/>
                            </div>
                            <div class="block-submit-label">
                                <p>
                                    Khi gửi CV, bạn đã xác nhận đồng ý các <a target="_blank" href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung'));?>" title="<?php echo $view['translator']->trans('footer.vlance.term', array(), 'vlance') ?>">điều khoản sử dụng</a> của vLance.vn
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="suggest-cv-fl span5">
                    <h3 class="title">Gợi ý để có 1 cv thành công</h3>
                    <div class="guide-fl">
                        <ul>
                            <li><i class="fa fa-check" aria-hidden="true"></i> <p>Viết ngắn gọn, không quá 2 trang A4.</p></li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> <p>Nêu rõ học vấn và kỹ năng, càng khớp với công việc muốn ứng tuyển thì càng tốt.</p></li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> <p>Liệt kê sản phẩm, dự án cụ thể từng làm.</p></li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> <p>Nếu công việc yêu cầu tiếng Anh thì gửi CV tiếng Anh.</p></li>
                            <li><i class="fa fa-check" aria-hidden="true"></i> <p>Kiểm tra kỹ chính tả, dấu câu, ngày tháng, trình bày.</p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>   