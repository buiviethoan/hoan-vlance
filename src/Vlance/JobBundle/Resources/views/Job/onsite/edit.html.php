<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>

<?php $view['slots']->set('og_image', 'http://'.$app->getRequest()->getHost() . $view['assets']->getUrl('img/og/dang-viec-freelance.jpg'));?>
<?php $view['slots']->set('title', $view['translator']->trans('Sửa việc onsite', array(), 'vlance'));?>
<?php $view['slots']->set('og_title', "Sửa việc onsite");?>
<?php $view['slots']->set('og_url', 'http://'.$app->getRequest()->getHost() . $view['router']->generate('job_onsite_edit', array('id' => $entity->getId())));?>
<?php // $view['slots']->set('description', "Ngay sau khi đăng, bạn sẽ ngay lập tức nhận được hàng chục chào giá từ các freelancer hàng đầu Việt Nam.");?>

<?php $view['slots']->start('content')?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<div class="content-section container create-job post-job post-job-onsite">
    <h1 class="new_job_title">Đăng tin tuyển dụng việc On-site</h1>
    <script type="text/javascript">
        var logged = '<?php if(is_object($acc)){echo "true";} else {echo "false";}?>';
    </script>
    <div class="post-job">
        <form id="edit-job-form" action="<?php echo $view['router']->generate('job_onsite_update', array('id' => $entity->getId())); ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
            <?php echo $view->render('VlanceJobBundle:Job/onsite/edit:form.html.php', array('form' => $form, 'entity' => $entity, 'type' => 'edit')) ?>

            <?php $js_skill = array(); ?>
            <?php $skills = $entity->getSkills(); ?>
            <?php foreach($skills as $skill): ?>
                <?php $js_skill[] = array($skill->getTitle(),$skill->getId()); ?>
            <?php endforeach; ?>
            <script type="text/javascript">
                var prefilled_skills = <?php echo json_encode($js_skill) ?>;
            </script>
        </form>
        <div class="row-fluid">
            <div class="offset2 span10 form-cread-job-btn">
                <input id="btn-submit-job" type="button" class="btn btn-primary btn-large popup-validate span12" value="Cập nhật việc tuyển dụng"/>
            </div>
        </div>
    </div>    
</div>
<?php $view['slots']->stop();?>