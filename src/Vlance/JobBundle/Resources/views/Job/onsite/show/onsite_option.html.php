<?php use Vlance\JobBundle\Entity\JobOnsiteOption; ?>
<?php $acc_login = $app->getSecurity()->getToken()->getUser(); ?>

<script type="text/javascript">
    var creditShow = '<?php if(is_object($acc_login)){echo $acc_login->getCredit()->getBalance();} else {echo 0;}; ?>';
    var jobId = '<?php echo $entity->getId(); ?>';
    var payOption = '<?php echo $view['router']->generate('onsite_option_view', array()) ?>';
</script>

<div class="block-onsite-option">
    <h3>Bạn muốn tăng số lượng và chất lượng CV nhận được?</h3>
    <p>Những lựa chọn dưới đây sẽ giúp bạn thu hút được rất nhiều freelancer.</p>
    <div class="row-fluid onsite-option first">
        <div class="span10">
            <label>
                <input type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS;?>" data-val="20" onclick="updateCredit()">
                    Tin tuyển dụng nổi bật 03 ngày trên vLance. <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Tin được hiển thị ở đầu danh sách liên tục trong 3 ngày. Được hiển thị nổi bật và thu hút hơn so với tin thông thường."></i>
            </label>
        </div>
        <div class="payment-credit span2">20 Credit</div>
    </div> 
    <div class="row-fluid onsite-option">
        <div class="span10">
            <label>
                <input type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_EXTEND_10;?>" data-val="20" onclick="updateCredit()">
                Xem thêm 10 CV trong số các CV ứng tuyển và thêm 7 ngày hiển thị tin tuyển dụng. <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Bạn sẽ được nhận thêm 10 CV nữa và có thêm 7 ngày hiển thị tin tuyển dụng này."></i>
            </label>
        </div> 
        <div class="payment-credit span2">20 Credit</div>
    </div> 
    <div class="row-fluid onsite-option last">
        <div class="span10">
            <label>
                <input id="move" type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_EXTEND_30;?>" data-val="50" onclick="updateCredit()">
                     Xem thêm 30 CV trong số các CV ứng tuyển và thêm 30 ngày hiển thị tin tuyển dụng. <i class="fa fa-question-circle" id="move" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Bạn sẽ được nhận thêm 30 CV nữa và có thêm 30 ngày hiển thị tin tuyển dụng này."></i>
            </label>
        </div>  
        <div class="payment-credit span2">
            50 Credit
        </div>
    </div>
    
    <div class="row-fluid amount">
        <div class="span10">Tổng chi phí:</div>
        <div class="span2" id="total-amount">0 Credit</div>
    </div>
    
    <div class="row-fluid systerm-message" id="systerm-message-id">
        <p><span class="text-red">Số dư Credit <?php if(is_object($acc_login)){echo $acc_login->getCredit()->getBalance();};?></span> - <a href="<?php echo $view['router']->generate('credit_balance',array());?>">Mua thêm</a></p>
    </div>
    
    <div class="row-fluid button" id="block-button">
        <div class="span8 message">
            <i>Ngay sau khi thanh toán thành công, chúng tôi sẽ gửi email thông báo xác nhận cho bạn.</i>
        </div>
        <div class="span4 button-submit">
<!--            <a href="#creditOnsite" id="btn-open-popup" role="button" class="btn btn-blue-small span12" data-toggle="modal" onclick="countCreditShow();"/>Xác nhận thanh toán</a>-->
            <a href="#" id="btn-open-popup" role="button" class="btn btn-blue-small span12" data-toggle="modal" onclick="countCreditShow();"/>Xác nhận thanh toán</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<!-- Modal -->
<div id="creditOnsite" class="modal hide fade popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Thêm tùy chọn tin tuyển dụng On-site</h4>
    </div>
    <div class="modal-body">
        <p>Ngay sau khi thanh toán thành công, chúng tôi sẽ gửi email thông báo xác nhận cho bạn.</p>
        <br/>
        <p id="totalCredit" style="text-align: center">
            Tổng chi phí: <b>0 CREDIT</b>
        </p>
        <br/>
        <p style="text-align: center">
            <span style="color: #999999">Bạn hiện có: <?php if(is_object($acc_login)){echo $acc_login->getCredit()->getBalance();} ;?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
        </p>
        <p id="sys-message" style="text-align: center"></p>
    </div>
    <div class="modal-footer">
        <span style="margin-right: 10px;"><a href="#" data-dismiss="modal" aria-hidden="true">Quay lại</a></span>
        <span><input id="btn-confirm" type="button" class="btn btn-primary btn-large popup-validate" value="Xác nhận thanh toán" onclick="vtrack('Submit onsite job')" /></span>
    </div>
</div>