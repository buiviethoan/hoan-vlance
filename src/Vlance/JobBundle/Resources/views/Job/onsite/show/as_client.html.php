<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
<?php $jobs_count = isset($jobs_worked[$acc->getId()]) ? $jobs_worked[$acc->getId()] : 0; ?>

<?php if($job->getTestType() != Job::TEST_TYPE_UPFRONT_ESCROW || ($job->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($job->getUpfrontAt()))): ?>
    <?php $is_allowed = true; ?>
<?php else: ?>
    <?php $is_allowed = false; ?>
<?php endif; ?>

<div class="profile-job span12 onsite-cv view-client
        <?php if($entity->getIsFeatured()): ?> featured-bid <?php endif; ?> 
        <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>
        <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?>">
    <div class="row-fluid">
        <!-- Left block -->
        <div class="span7 profile-job-left onsite">
            <div class="profile-job-left-bottom">
                <!--<div class="title span6"></div>-->
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <div class="drop-menu">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <div class="body-view row-fluid cv-apply">
                    <div class="span12">
                        <div class="cv-info-block cv-attachment">
                            <h5>CV CỦA ỨNG VIÊN</h5>
                            <?php $files = $entity->getFiles(); ?>
                            <?php if(count($files)>0): ?>
                                <ul>
                                    <?php foreach($files as $file) : ?>
                                    <li class="row-fluid">
                                        <i class="fa fa-paperclip" aria-hidden="true"></i>
                                        <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                           class="file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>

                        <?php if($entity->getFullDescription()): ?>
                        <div class="cv-info-block cv-message">
                            <h5>TIN NHẮN TỪ ỨNG VIÊN</h5>
                            <p>
                                <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        <div class="creatbid">
                            <span>ID<?php echo $entity->getId(); ?></span> -
                            <span>
                            <?php 
                                $today = new DateTime('now');
                                $yesterday = new DateTime('yesterday');
                                $job_creat = $entity->getCreatedAt();
                                $hours = JobCalculator::convertHours($job_creat, $today); 
                            ?>
                            <?php if($hours <= 6 ) : ?>
                                <?php if($hours < 1) : ?>
                                    <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                                <?php else: ?>
                                    <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60) .$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                                <?php endif; ?>
                            <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                            <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                            <?php else : ?>
                                <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                            <?php endif; ?>
                            </span>
                        </div>
                    </div>

                    <?php // Only admin can edit and deactivate ?>
                    <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                        <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                        </a>
                        <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
        <!-- Right block -->
        <div class="span5 profile-job-right onsite">
            <!-- Profile -->
            <div class="row-fluid profile-job-right-top">
                <div class="span12">
                    <div class="row-fluid">
                        <!-- Avatar -->
                        <div class="freelancer-row-img span3 offset1">
                            <div class="images">
                                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                                    <?php $resize = ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                                    <?php if($resize) : ?>
                                    <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                                    <?php else: ?>
                                    <img src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div>

                        <!-- Freelancer's information -->
                        <div class="span8 freelancer-row-contact-block">
                            <div class="title">
                                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                                    <?php echo $acc->getFullName() ?>
                                </a>
                                <?php if ($acc->getIsCertificated()) : ?>
                                <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                                    title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                                <?php endif;?>
                                <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                                    <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                                <?php endif; ?>
                                <?php if (!is_null($acc->getPersonId())): ?>
                                    <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                                    <?php endif; ?>
                                <?php endif; ?>
                                <script type="text/javascript" language="javascript">
                                    jQuery(document).ready(function(){
                                        $(function () {
                                            $('[data-toggle="tooltip"]').tooltip()
                                            })        
                                        });
                                </script>    
                            </div>

                            <div class="work-title"><?php echo $acc->getTitle(); ?></div>

                            <dl class="dl-horizontal">
                                <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                                <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance'); ?></dd>
                                <dt>Số điện thoại</dt><dd><?php echo $acc->getTelephone(); ?></dd>
                                <dt>Skype</dt><dd><?php echo $acc->getSkype(); ?></dd>
                            </dl>
                        </div>
                    </div>
                    
                    <div class="row-fluid">
                        <div class="span11 offset1 freelancer-row-contact-block">
                            <!-- Send message button -->
                            <?php // working user is creator of the job, or the admin of vLance ?>
                            <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                                <?php if($job->getAccount() == $current_user): ?>
                                    <?php // When client misses basic info, they can not send message ?>
                                    <?php if($current_user->getIsMissingClientInfo()):?>
                                        <a href="#popup-missing-info" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                           onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing info'})"
                                           data-content="<?php echo "data-content-missing"?>"
                                           title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                               <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                        </a>
                                    <?php else:?>
                                        <?php if(!$is_allowed): // khách hàng chưa nạp tiền Upfront escrow (với các KH đc test) ?>
                                            <a href="#popup-upfront-escrow" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                                onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing Upfront Escrow'})"
                                                title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                            </a>
                                        <?php else: ?>
                                            <?php $chatted = false;?>
                                            <?php foreach($job->getWorkshopsJob() as $wk):?>
                                                <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                                                    <?php $chatted = true; break;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                            <?php if($chatted):?>
                                                <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $entity->getId())) ?>" 
                                                        role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup"
                                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>">
                                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>
                                                </a>
                                            <?php else: ?>
                                                <a href="#formmessage" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                                        onclick="vtrack('Click send message', {'location':'Job view page'})"
                                                        data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                                                        data-id="<?php echo $entity->getId() ?>" 
                                                        data-uname="<?php echo $acc->getFullname();?>"
                                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif;?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>