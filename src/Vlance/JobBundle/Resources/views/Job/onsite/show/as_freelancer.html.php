<?php use Vlance\BaseBundle\Utils\Url; ?>

<?php $acc = $entity->getAccount(); ?>
<?php $jobs_count = isset($jobs_worked[$acc->getId()]) ? $jobs_worked[$acc->getId()] : 0; ?>
<div class="profile-job span12 
            <?php if($entity->getIsFeatured()): ?> featured-bid <?php endif; ?> 
            <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?>">
    <div class="row-fluid">
        <div class="span2">
            <div class="freelancer-row-img">
                <div class="images">
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                       title="<?php echo $acc->getFullName() ?>">
                        <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                        <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                        <?php else: ?>
                        <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                        <?php endif; ?>
                    </a>
                </div>
                <?php if($acc->getNumReviews() != 0): ?>
                    <div class="rating-box" data-toggle="popover" 
                        data-placement="right"  
                        data-content="<?php echo number_format($acc->getScore() / $acc->getNumReviews(),'1',',','.') ?>"  
                        data-trigger="hover">
                        <?php $rating= ($acc->getScore() / $acc->getNumReviews()) * 20; ?>
                        <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                    </div> 
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('.rating-box').popover();
                        })
                    </script>
                <?php else: ?>
                    <div class="rating-box">
                        <div class="rating" style="width:0%"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="span6 profile-job-left bid-freelancer">
            <div class="profile-job-left-bottom">
                <h3 class="title">
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                                      title="<?php echo $acc->getFullName() ?>">
                        <?php echo $acc->getFullName() ?>
                    </a>
                    <?php if ($acc->getIsCertificated()) : ?>                   
                    <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                        title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                    <?php endif;?>
                    <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                    <?php endif; ?>
                    <?php if (!is_null($acc->getPersonId())): ?>
                        <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                        <?php endif; ?>
                    <?php endif; ?>
                </h3>
                <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                <div class="body-view">
                     <?php if($acc->getDescription()): ?> 
                    <div class="textbody">
                        <div>
                            <?php echo \Vlance\BaseBundle\Utils\JobCalculator::cut(Vlance\BaseBundle\Utils\JobCalculator::replacement($acc->getDescription()),260) ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <?php $skills = $acc->getSkills() ?>
                    <?php if(count($skills) > 0): ?>
                        <?php $counter = 0; ?>
                        <div class="skill">
                            <label><?php echo $view['translator']->trans('common.skill', array(), 'vlance').':' ?></label>
                            <div class="list-skill">
                                <?php foreach ($skills as $skill): ?>
                                    <?php $counter++; ?>
                                    <a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>"
                                       title="<?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle() ?></a><?php echo ($counter < count($skills))?",":""; ?>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php endif; ?>
            </div>
        </div>
        <div class="span4 profile-job-right">
            <div class="profile-job-right-top">

                <div class="span12">
                    <dl class="dl-horizontal">
                        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.createdat', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCreatedAt()->format('d-m-Y') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job', array(), 'vlance')?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                               title="<?php echo $jobs_count ?>">
                            <?php echo $jobs_count ?> việc
                            </a>
                        </dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.income', array(), 'vlance') ?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>"
                               title="<?php echo number_format($acc->getInCome(),'0',',','.') 
                                            . ' ' . $view['translator']->trans('common.currency', array(), 'vlance')?>">
                                <?php echo number_format($acc->getInCome(),'0',',','.') 
                                         . ' ' . $view['translator']->trans('common.currency', array(), 'vlance')?>
                            </a>
                        </dd>
                    </dl>
                </div>
            </div>
            <?php if($entity->getAwardedAt()): ?>
            <div class="profile-job-right-bottom span12">
                <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="winning span5 offset1"></div>
                        <script type="text/javascript">
                        /* <![CDATA[ */
                            $(".profile-job-left-bottom .body-view .textbody").addClass('textbody-winning');
                        /* ]]> */
                        </script>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php if($entity->getIsFeatured()): ?> 
    <div class="featured-bid-info"><span><i class="icon-star"></i> <span>Chào giá được tài trợ</span></span></div>
    <?php endif; ?>
</div>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
jQuery(document).ready(function(){
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })        
});
/* ]]> */
</script>