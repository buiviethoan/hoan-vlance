<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
<?php $jobs_count = isset($jobs_worked[$acc->getId()]) ? $jobs_worked[$acc->getId()] : 0; ?>
<div class="profile-job span12 
            <?php if($entity->getIsFeatured()): ?> featured-bid <?php endif; ?> 
            <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>
            <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?>">
    <div class="row-fluid">
        <div class="span6 profile-job-right onsite">
            <div class="profile-job-right-top">
                <div class="freelancer-row-img span4">
                    <div class="images">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                            <?php if($resize) : ?>
                            <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php else: ?>
                            <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
                <div class="span8">
                    <div class="title">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php echo $acc->getFullName() ?>
                        </a>
                        <?php if ($acc->getIsCertificated()) : ?>
                        <img style="margin-left: -10px; width: 20px; height: 20px; margin-top: -6px;" src="/img/logo_certificated.png" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                             title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                    </div>
                    <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                    <dl class="dl-horizontal">
                        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.createdat', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCreatedAt()->format('d-m-Y') ?></dd>
                    </dl>

                    <?php // working user is creator of the job, or the admin of vLance ?>
                    <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                        <?php if($job->getAccount() == $current_user): ?>
                            <?php // When client misses basic info, they can not send message ?>
                            <?php if($current_user->getIsMissingClientInfo()):?>
                                <a href="#popup-missing-info" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                   onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing info'})"
                                   data-content="<?php echo "data-content-missing"?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                </a>
                            <?php else:?>
                                <?php $chatted = false;?>
                                <?php foreach($job->getWorkshopsJob() as $wk):?>
                                    <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                                        <?php $chatted = true;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                                <a href="#formmessage" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                   onclick="vtrack('Click send message', {'location':'Job view page'})"
                                   data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                                   data-id="<?php echo $entity->getId() ?>" 
                                   data-uname="<?php echo $acc->getFullname();?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                        <?php if($chatted):?><i class="icon-white icon-share-alt"></i><?php endif;?>
                                </a>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="span6 profile-job-left onsite">
            <div class="profile-job-left-bottom">
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <div class="offset6 span6 drop-menu">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#popup-refusal" role="button" class="refusal-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_isdeclined',array('id' => $entity->getId())) ?>"
                                   >
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <div class="body-view row-fluid">
                    <?php if($entity->getFullDescription()): ?>                 
                        <div class="textbody">
                            <div>
                                <b>Tin nhắn của ứng viên cho khách hàng</b>:<br/>
                                <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                            </div>
                            <div class="creatbid">
                                <span>ID<?php echo $entity->getId(); ?></span> -
                                <span>
                                <?php 
                                    $today = new DateTime('now');
                                    $yesterday = new DateTime('yesterday');
                                    $job_creat = $entity->getCreatedAt();
                                    $hours = JobCalculator::convertHours($job_creat, $today); 
                                ?>
                                <?php if($hours <= 6 ) : ?>
                                    <?php if($hours < 1) : ?>
                                        <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                                    <?php else: ?>
                                        <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60) .$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                                    <?php endif; ?>
                                <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php else : ?>
                                    <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                                <?php endif; ?>
                                </span>
                            </div>
                        </div>

                        <?php // Only admin can edit and deactivate ?>
                        <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                            <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                            </a>
                            <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid">
                <?php $files = $entity->getFiles(); ?>
                <?php if(count($files)>0): ?>
                    <div class="span5 offset1 attach">
                        <ul>
                            <?php foreach($files as $file) : ?>
                            <li class="row-fluid">
                                <div class="i32 i32-attach span2"></div>
                                <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php if($entity->getIsFeatured()): ?> 
        <div class="featured-bid-info"><span><i class="icon-star"></i> <span>Chào giá được tài trợ</span></span></div>
    <?php endif; ?>
</div>