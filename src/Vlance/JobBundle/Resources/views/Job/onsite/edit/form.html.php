<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\JobBundle\Entity\JobOnsiteOption; ?>
<div class="control-group primary-type">
    <div class="row-fluid">
        <div class="span2">
            <div class="sprites-job-posting sprites-2"></div>
        </div>
        <div class="span10">
            <h4>Vị trí cần tuyển dụng</h4>
            <div class="form-title">
                    <?php echo $view['form']->row($form['title']); ?>
            </div>
        </div> 
    </div>
</div>
<div class="control-group">
    <div class="row-fluid">
        <div class="span10 offset2">
            <div class="form-category">
                <?php echo $view['form']->row($form['category']); ?>
            </div>
        </div> 
    </div>
</div>      
<div class="control-group">
    <div class="row-fluid">
        <div class="span10 offset2">
            <div class="form-description">
                <?php echo $view['form']->row($form['description']); ?>
                <?php echo $view['form']->row($form['files']); ?>
            </div>
        </div> 
    </div>
</div> 
<div class="control-group">
    <div class="row-fluid">
        <div class="span10 offset2">
            <div class="form-skills">
                <label class="required" for="vlance_jobbundle_jobonsitetype_skill"><?php echo $view['translator']->trans('Kỹ năng', array(), 'vlance') ?></label>
                <div class="form-add-skill">    
                    <div class="inner-skill">
                        <input type="text" id="vlance_jobbundle_jobonsitetype_skill" data-provide="typeahead" name="hiddenTagListA" placeholder="<?php echo $view['translator']->trans('VD: PHP, NodeJs, React Native,...', array(), 'vlance') ?>" class="tm-input"/>
                        <script type="text/javascript">
                            <?php // Mảng skill ?>
                            var source = <?php echo $view['actions']->render($view['router']->generate('skill_list')) ?>;
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-2 control-group primary-type row-fluid">
    <div class="span2">
        <div class="sprites-job-posting sprites-4"></div>
    </div>
    <div class="span10">
        <h4>Chi phí và thời gian</h4>
        <div>
            <div class="control-group">
                <label class="required" for="vlance_jobbundle_jobonsitetype_budget">Ngân sách dự kiến</label>
                <div class="form-budget row-fluid">
                    <div class="span6 form-min-budget">
                        <?php echo $view['form']->widget($form['budget']); ?>
                    </div>
                    <div class="span6 form-max-budget">
                        <?php echo $view['form']->widget($form['budgetMax']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-date-hired row-fluid">
            <div class="form-start-date span6">
                <?php echo $view['form']->row($form['startedAt']); ?>
            </div>
            <div class="form-time-hired span6">
                <?php echo $view['form']->row($form['duration']); ?>
            </div>
        </div>
    </div>
</div>

<div class="block-3 control-group primary-type row-fluid">
    <div class="span2">
        <div class="sprites-job-posting sprites-3"></div>
    </div>
    <div class="span10">
        <h4>Yêu cầu về Freelancer</h4>
        <div class="form-exp">
            <?php echo $view['form']->row($form['onsiteRequiredExp']); ?>
        </div>
        <div class="row-fluid">
            <div class="form-city span6">
                <?php echo $view['form']->row($form['city']); ?>
            </div>
            <div class="form-location span6">
                <?php echo $view['form']->row($form['onsiteLocation']); ?>
            </div>
        </div>
    </div>
</div>

<div class="block-4 control-group primary-type row-fluid">
    <div class="span2">
        <div class="sprites-job-posting sprites-1"></div>
    </div>
    <div class="span10">
        <h4>Thông tin về nhà tuyển dụng</h4>
        <div class="row-fluid">
            <div class="form-company span6">
                <?php echo $view['form']->row($form['onsiteCompanyName']); ?>
            </div>
            <div class="form-company-logo span6">
                <div class="row-fluid">
                    <?php if(!is_null($entity->getPath())):?>
                    <div class="span3" style="margin-top: 15px;">
                        <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '90x90', 90, 90, 1);?>
                        <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="" title="" />
                        <?php else: ?>
                            <img width="90" height="90" src="<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt=""  title="" />
                        <?php endif; ?>
                    </div>
                    <div class="span9">
                        <label class="required" for="vlance_jobbundle_jobonsitetype_file">Logo công ty</label>
                        <?php echo $view['form']->widget($form['file']); ?>
                    </div>
                    <?php else: ?>
                        <div class="span12">
                            <?php echo $view['form']->row($form['file']); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if($type === 'new'): ?>
<div class="block-5 control-group primary-type row-fluid">
    <div class="span2">
        <div class="sprites-job-posting sprites-5"></div>
    </div>
    <div class="span10">
        <h4>Tùy chọn khác (không bắt buộc)</h4>
        <div class="row-fluid first">
            <div class="span9">
                <label>
                    <input type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_FEATURE_3DAYS;?>" data-val="20">
                        Tin tuyển dụng nổi bật 03 ngày trên vLance.
                </label>
            </div>
            <div class="payment-credit span3">20 Credit</div>
        </div> 
        <div class="row-fluid">
            <div class="span9">
                <label>
                    <input type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_EXTEND_10;?>" data-val="20">
                        Xem thêm 10 CV trong số các CV ứng tuyển và thêm 7 ngày hiển thị tin tuyển dụng.
                </label>
            </div> 
            <div class="payment-credit span3">20 Credit</div>
        </div> 
        <div class="row-fluid">
            <div class="span9">
                <label>
                    <input type="checkbox" name="onsite_option[]" value="<?php echo JobOnsiteOption::ONSITE_OPTION_EXTEND_30;?>" data-val="50">
                        Xem thêm 30 CV trong số các CV ứng tuyển và thêm 30 ngày hiển thị tin tuyển dụng
                </label>
            </div>  
            <div class="payment-credit span3">
                50 Credit
            </div>
        </div>    
    </div>        
</div>
<?php endif; ?>
<?php echo $view['form']->row($form['_token'])?>