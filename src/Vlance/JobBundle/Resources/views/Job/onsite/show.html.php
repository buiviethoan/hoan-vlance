<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_onsite_view',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $image = "img/og/danh-sach-viec-freelance.jpg";?>
<?php if($entity->getCategory()): ?>
    <?php $cat_id = $entity->getCategory()->getId(); ?>
    <?php if(in_array($cat_id, array(22))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/dich-thuat.jpg"; ?>
    <?php elseif(in_array($cat_id, array(3))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/lap-trinh-ung-dung-di-dong.jpg"; ?>
    <?php elseif(in_array($cat_id, array(2))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/lap-trinh-web.jpg"; ?>
    <?php elseif(in_array($cat_id, array(26,27,28,30,50,51,52,53))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/online-marketing.jpg"; ?>
    <?php elseif(in_array($cat_id, array(21))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/quan-ly-fanpage.jpg"; ?>
    <?php elseif(in_array($cat_id, array(10))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/seo.jpg"; ?>
    <?php elseif(in_array($cat_id, array(17))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/tao-chinh-sua-video.jpg"; ?>
    <?php elseif(in_array($cat_id, array(45))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/thiet-ke-ung-dung-di-dong.jpg"; ?>
    <?php elseif(in_array($cat_id, array(44,14))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/thiet-ke-web.jpg"; ?>
    <?php elseif(in_array($cat_id, array(61,6,49,48,24,23))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/viet-bai.jpg"; ?>
    <?php endif; ?>
<?php endif; ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl($image));?>

<?php $view['slots']->start('content'); ?>

<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<div class="content-section container detail job-onsite-content">
    <div class="row-fluid">
        <div class="span12">
            <?php  
                /* @var $entity Job */
                $workrooms = $entity->getWorkshopsJob();
                if(is_object($current_user)) :
                    foreach ($workrooms as $workroom) {
                        if($current_user === $workroom->getWorker() || $current_user === $workroom->getOwner() || $current_user === $workroom->getBid()) {
                            $workroom_current = $workroom;
                            break;
                        }
                    }
                    if(count($workrooms) > 0 && ($view_as_job_owner || ($view_as_bid_owner && isset($workroom_current)))) : // is Job owner OR Freelancer who got message ?>
                        <div class="row-fluid">
                            <h1 class="title clampMe">
                                <?php echo ucfirst(htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")); ?>
                                <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                    <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                <?php endif;?>
                                <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                    <?php if($entity->getFeaturedFb() == false): ?>
                                        <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                    <?php else: ?>
                                    <?php //Feature facebook job-view?> 
                                        <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php if(HasPermission::hasAdminPermission($current_user) == FALSE ) : ?>
                                    <?php if($entity->getAccount() === $current_user): ?>
                                        <?php if($entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_OPEN) : ?>
                                        <span class="edit-link">
                                            <a href="<?php echo $view['router']->generate('job_onsite_edit',array('id' => $entity->getId())) ?>" class="edit-link"
                                               title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                                <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                            </a>
                                        </span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <span class="edit-link">
                                        <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link"
                                           title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                        </a>
                                    </span>
                                <?php endif; ?>
                            </h1>
                            <div class="clear"></div>
                            <div class="title-sub">
                                <span>ID dự án: <?php echo $entity->getId();?></span>
                                <span>Ngày đăng: <?php echo JobCalculator::countDays($entity->getCreatedAt());?></span>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="row-fluid page-tabs">
                                <ul>
                                    <li class="active">
                                        <a href="#">
                                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                    <li>
                                        <a href="<?php echo $view['router']->generate('workshop_show',array('job_hash' => $entity->getHash())) ?>">
                                            <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                </ul>
                                <div class="border"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            <div class="row-fluid">
                <div class="row-fluid">
                    <div class="span7 news-detail onsite-detail">
                       <?php if(!is_object($current_user) || !isset($workroom_current)) : ?>
                            <div class="row-fluid ">
                                <h1 class="title clampMe">
                                    <?php echo ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                                    <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                        <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                    <?php endif;?>
                                    <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                        <?php if($entity->getFeaturedFb() == false): ?>
                                            <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                        <?php else: ?>
                                        <?php //Feature facebook job-view ?>
                                            <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if(HasPermission::hasAdminPermission($current_user) == FALSE ) : ?>
                                        <?php if($entity->getAccount() === $current_user): ?>
                                            <?php if($entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_OPEN) : ?>
                                            <span class="edit-link">
                                                <a href="<?php echo $view['router']->generate('job_onsite_edit',array('id' => $entity->getId())) ?>" class="edit-link"
                                                   title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                                </a>
                                            </span>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <span class="edit-link">
                                            <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link"
                                               title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                                <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                            </a>
                                        </span>
                                    <?php endif; ?>
                                </h1>
                                <div class="clear"></div>
                                <div class="title-sub">
                                    <span>ID dự án: <?php echo $entity->getId();?></span>
                                    <span>Ngày đăng: <?php echo JobCalculator::countDays($entity->getCreatedAt());?></span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        <?php endif; ?>

                        <?php // Block thông tin tổng hợp về việc onsite ?>
                        <div class="row-fluid">
                            <div class="span12 onsite-properties">
                                <div class="row-fluid">
                                    <div class="span6 onsite-properties-left">
                                        <div class="row-fluid">
                                            <div class="span12 prop-company">
                                                <div class="row-fluid">
                                                    <div class="span3 prop-company-logo">
                                                        <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '60x60', 60, 60, 1);?>
                                                        <img src="<?php echo $resize ? $resize : $view['assets']->getUrl('img/unknown.png') ?>" alt="Logo"  title="<?php echo $entity->getOnsiteCompanyName();?>" />
                                                    </div>
                                                    <div class="span8 prop-company-name">
                                                        <?php echo $entity->getOnsiteCompanyName();?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12 prop-salary">
                                                <div class="prop-salary-label">Mức lương tháng (VNĐ)</div>
                                                <div class="prop-salary-range">
                                                    <?php echo number_format($entity->getBudget(),'0',',','.') ?> - <?php echo number_format($entity->getBudgetMax(),'0',',','.') ?>
                                                </div>
                                                <div class="prop-apply-deadline">
                                                    <?php $remain = JobCalculator::ago($entity->getCloseAt(), $now);?>
                                                    <em>Hạn nhận CV: <?php echo ($remain == false) ? 'Đã hết hạn' : $remain ; ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6 onsite-properties-right">
                                        <div class="prop-mixed">
                                            <dl class="dl-horizontal">
                                                <dt>Ngày bắt đầu làm</dt>       <dd><?php echo $entity->getStartedAt()->format('d/m/Y') ?></dd>
                                                <div class="clear"></div>
                                                <dt>Thời hạn</dt>               <dd><?php echo $entity->getDuration() ?> tháng</dd>
                                                <div class="clear"></div>
                                                <dt>Yêu cầu kinh nghiệm</dt>    <dd><?php echo $entity->getTextOnsiteRequiredExp(); ?></dd>
                                                <div class="clear"></div>
                                                <dt>Nơi làm việc</dt>           <dd><?php echo $entity->getCity()->getName();?></dd>
                                                <div class="clear"></div>
                                                <dt>Hình thức</dt>              <dd><?php echo $entity->getTextOnsiteLocation();?></dd>
                                            </dl>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php // Block nội dung chi tiết về công việc ?>
                        <div class="row-fluid body-view">
                            <div class="span10 description <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text-bao<?php endif; ?>">

                                <?php // Nếu chưa đăng nhập thì hiện popup thông báo "cần phải đăng nhập để xem" ?>
                                <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>
                                    <div class="login_view_job">
                                        <div class="span3 btn-connect">
                                            <p class="first">
                                                <?php // echo $view['translator']->trans('site.menu.got_account', array(), 'vlance') ?> 
                                                <strong>Để đọc thông tin, vui lòng đăng nhập bằng một trong hai cách sau:</strong>
                                            </p>
                                            <div class="center">
                                                <div class="job-login-socail">
                                                    <a class="btn-facebook-login" onclick="fb_login()">
                                                        <span><img src="/img/icon-facebook.png"></span>
                                                    </a>
                                                    <a class="btn-google-plus-login" onclick="google_login()">
                                                        <span><img src="/img/icon-google+.png"></span>
                                                    </a>
                                                    <a class="btn-linkedin-login" onclick="linkedin_login()">
                                                        <span><img src="/img/icon-linkedin.png"></span>
                                                    </a>
                                                </div>
                                                <div class="job-login-vlance">
                                                    Hoặc
                                                    <a class="btn btn-primary" href="<?php echo $view['router']->generate('fos_user_security_login')?>">
                                                    <?php echo $view['translator']->trans('site.menu.login_job', array(), 'vlance') ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <p class="last"> 
                                                Bạn chưa có tài khoản vLance? 
                                                <a class="" href="<?php echo $view['router']->generate('account_register') ?>">
                                                    Đăng ký ngay
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <div class="<?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text<?php endif; ?>">
                                    <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                                        <?php if (trim($line)): ?>
                                            <p><?php echo $line; ?></p>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="clear"></div>

                            <?php // Files đính kèm ?>
                            <?php $files = $entity->getFiles(); ?>
                            <?php if(count($files)>0): ?>
                                <div class="attach">
                                    <label><?php echo $view['translator']->trans('view_job_page.job_detail.attachment', array(), 'vlance').':' ?></label>
                                    <ul>
                                        <?php foreach($files as $file) : ?>
                                            <li class="row-fluid">
                                                <div class="i32 i32-attach"></div>
                                                <?php if(!is_object($current_user)) :?>
                                                <a href="#popup-login" data-toggle="modal" 
                                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                                </a>
                                                <?php echo $view->render('VlanceJobBundle:Job:popup_login.html.php', array()) ?>
                                                <?php else : ?>
                                                <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_job',array('job_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                                </a>

                                                <?php endif; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>


                        <?php // Danh sách Skills ?>
                        <?php $skills = $entity->getSkills() ?>
                        <?php if(count($skills) > 0): ?>
                            <div class="row-fluid body-view">
                                <?php $counter = 0; ?>
                                <div class="span10 skill">
                                    <span><?php echo $view['translator']->trans('common.skill', array(), 'vlance')?></span>
                                    <?php foreach ($skills as $skill): ?>
                                        <?php $counter++; ?>
                                       <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                          title="<?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle() ?></a><?php echo ($counter < count($skills))?",":""; ?>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php // Facebook likes ?>
                        <div class="row-fluid fb-like" data-href="<?php echo $abs_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                    </div>

                    <?php // Job information & client description ?>
                    <div class="span4 job-information offset1">
                        <div class="job-information-inner">
                            <div class="row-fluid">
                                <?php echo $view->render('VlanceJobBundle:Job/show:client_information.html.php', array('entity' => $entity)) ?>
                            </div>
                        </div>

                        <div class="job-view-fb-page-box">
                            <h3>Like page để nhận được việc sớm nhất</h3>
                            <?php echo $view->render('VlanceJobBundle:Job/show:fanpage.html.php', array('entity' => $entity)) ?>
                        </div>
                    </div>
                </div>
                <?php //Block option ?>
                <?php if($entity->getStatus() == Job::JOB_OPEN) : ?>
                    <?php if(is_object($current_user)): ?>
                        <?php if($current_user === $entity->getAccount()): ?>
                            <?php echo $view->render('VlanceJobBundle:Job/onsite/show:onsite_option.html.php', array('entity' => $entity)) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
                
                <?php //END Block option ?>
                
                <?php // Bid form ?>
                <?php if($entity->getStatus() == Job::JOB_OPEN) : ?>
                    <?php if(is_object($current_user)): ?>
                        <?php if(!$entity->isBiddedBy($current_user)):?>
                            <?php // Template: VlanceJobBundle:Job:onsite/bid_form.html.php?>
                            <?php if($current_user !== $entity->getAccount()): ?>
                                <?php if(!$entity->isBiddedBy($current_user)):?>
                                    <div class="row-fluid form-bid">
                                        <?php echo $view['actions']->render($view['router']->generate('bid_onsite_new',array('id' => $entity->getId())));?>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif;?>
                <?php endif; ?>
                <?php // END Bid form ?>
                
                <?php // list_bid.html.php ?>
                <div class="detail-bottom span12 onsite-cv-list">
                    <?php echo $view['actions']->render($view['router']->generate('job_show_bid',array('id' => $entity->getId())));?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /* End detail page */?>
<?php $view['slots']->stop(); ?>