<?php use Vlance\BaseBundle\Utils\ResizeImage;?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('og_image', 'http://'.$app->getRequest()->getHost() . $view['assets']->getUrl('img/og/dang-viec-freelance.jpg'));?>
<?php $view['slots']->set('title', $view['translator']->trans('new_job_page.title', array(), 'vlance'));?>
<?php $view['slots']->set('og_title', "Đăng việc freelance mới");?>
<?php $view['slots']->set('og_url', 'http://'.$app->getRequest()->getHost() . $view['router']->generate('job_new'));?>
<?php $view['slots']->set('description', "Ngay sau khi đăng, bạn sẽ ngay lập tức nhận được hàng chục chào giá từ các freelancer hàng đầu Việt Nam.");?>
<?php $view['slots']->start('content')?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'ViewCreateJobPage');
</script>
<?php endif; ?>
<?php // End Tracking ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<?php /* create-job page */?>
<div class="content-section container create-job">
    <div class="row-fluid">
        <div class="span12 left-side">
            
            <script type="text/javascript">
                var logged = '<?php if(is_object($acc)){echo "true";} else {echo "false";}?>';
            </script>
            
            <h1 class="new_job_title"><?php echo $view['translator']->trans('new_job_page.title', array(), 'vlance')?></h1>
            
            <?php if(!is_object($acc)): ?>
            <?php //START form dang ki ?>
            <div class="block-lr" style="margin-bottom: 20px;">
                <div class="row-fluid">
                    <div class="span2">
                        <div class="sprites-job-posting sprites-6"></div>
                    </div>    
                    <div class="span10">
                        <h4>Thông tin người đăng việc</h4>
                        <div class="row-fluid block-type-user">
                            <div class="span6">
                                <label class="radio checked"><input id="new_user" type="radio" name="typeofuser" value="new" checked> Tôi là người mới</label>
                            </div>
                            <div class="span6">
                                <label class="radio"><input id="old_user" type="radio" name="typeofuser" value="old"> Tôi đã có tài khoản</label>
                            </div>
                        </div> 
                    </div>    
                </div>
                <div class="row-fluid">
                    <div class="block-form-lr span10 offset2">
                        <div class="block-form-reg">
                            <form id="reg-form" method="post">
                                <div class="row-fluid field">
                                    <div class="span6">
                                        <label>Họ và tên</label>
                                        <input id="reg_name" type="text" name="reg_name" placeholder="Ví dụ: Trần Văn A">
                                    </div>
                                    <div class="span6 input-email">
                                        <label>Email</label>
                                        <input id="reg_email" type="email" name="reg_email" placeholder="Ví dụ: name@domain.com">
                                    </div>
                                </div>
                                <div class="row-fluid field">
                                <div class="span6">
                                        <label>Mật khẩu</label>
                                        <input id="reg_pass" type="password" name="reg_pass" placeholder="********">
                                    </div>
                                    <div class="span6 re-pass">
                                        <label>Nhập lại mật khẩu</label>
                                        <input id="reg_re_pass" type="password" name="reg_re_pass" placeholder="********">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="block-form-log deactive">
                            <form id="log-form" method="post">
                                <div class="row-fluid field">
                                    <div class="span6 block-username-log">
                                        <label>Email</label>
                                        <input type="text" id="login_email" name="login_email" placeholder="Email">
                                    </div>
                                    <div class="span6 block-password-log">
                                        <label>Mật khẩu</label>
                                        <input type="password" id="login_pass" name="login_pass" placeholder="Mật khẩu đăng nhập">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <script type="text/javascript">
                            var reg_form_in_job = '#reg-form';
                            var reg_form_in_job_params = {"url":"<?php echo $view['router']->generate('register_job') ?>"};
                            var login_form_in_job = '#log-form';
                            var login_form_in_job_params = {"url":"<?php echo $view['router']->generate('login_job') ?>"};
                        </script>
                        <div class="row-fluid">
                            <p class="login-socail-network-label">Hoặc dùng luôn tài khoản sau</p>
                            <div class="login-socail-network">
                                <a class="btn-facebook btn-social-fb-post-job" onclick="fb_login()"><span><img src="/img/icon-facebook.png">Facebook</span></a>
                                <a class="btn-google-plus btn-social-nfb-post-job" onclick="google_login()"><span><img src="/img/icon-google+.png">Google</span></a>
                                <a class="btn-linkedin btn-social-nfb-post-job" onclick="linkedin_login()"><span><img src="/img/icon-linkedin.png">LinkedIn</span></a>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <?php //END form dang ki ?>
            <?php endif; ?>
            
            <?php // Mieu tả job ?>
            <?php // <div class="description"><?php echo $view['translator']->trans('new_job_page.subtitle', array(), 'vlance')</div> ?>
            
            <div class="post-job">
                <?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
                <form id="edit-job-form" action="<?php echo $view['router']->generate('job_new_submit') ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
                    <?php echo $view->render('VlanceJobBundle:Job/edit:form.html.php', array('form' => $form, /*'acc' => $acc*/)) ?>
                    
                    <?php // Preview Job before submit ?>
                    <div class="row-fluid">
                        <div class="span10 offset2">
                            <div class="form-cread-job-btn">
                                <div class="row-fluid">
                                    <div class="accept-agreement span12">
                                        <p><?php echo $view['translator']->trans('form.job.agree_info', array(), 'vlance'); ?></p>
                                    </div>
                                </div>    
                                <div class="row-fluid">    
                                    <div class="span12 ">    
                                        <input id="btn-submit-job" type="button" class="btn btn-primary btn-large popup-validate span12" value="<?php echo $view['translator']->trans('new_job_page.submit', array(), 'vlance') ?>"/>
                                    </div>
                                </div>
                            </div>    
                        </div>    
                    </div>    
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo $view->render('VlanceJobBundle:Job/popup_admin:block_popup.html.php', array()) ?>

<?php /* end create-job page */?>
<?php $view['slots']->stop();?>
