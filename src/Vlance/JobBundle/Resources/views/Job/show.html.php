<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>

<?php $image = "img/og/danh-sach-viec-freelance.jpg";?>
<?php if($entity->getCategory()): ?>
    <?php $cat_id = $entity->getCategory()->getId(); ?>
    <?php if(in_array($cat_id, array(22))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/dich-thuat.jpg"; ?>
    <?php elseif(in_array($cat_id, array(3))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/lap-trinh-ung-dung-di-dong.jpg"; ?>
    <?php elseif(in_array($cat_id, array(2))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/lap-trinh-web.jpg"; ?>
    <?php elseif(in_array($cat_id, array(26,27,28,30,50,51,52,53))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/online-marketing.jpg"; ?>
    <?php elseif(in_array($cat_id, array(21))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/quan-ly-fanpage.jpg"; ?>
    <?php elseif(in_array($cat_id, array(10))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/seo.jpg"; ?>
    <?php elseif(in_array($cat_id, array(17))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/tao-chinh-sua-video.jpg"; ?>
    <?php elseif(in_array($cat_id, array(45))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/thiet-ke-ung-dung-di-dong.jpg"; ?>
    <?php elseif(in_array($cat_id, array(44,14))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/thiet-ke-web.jpg"; ?>
    <?php elseif(in_array($cat_id, array(61,6,49,48,24,23))):   // dich-thuat?>
        <?php $image = "img/og/cong-viec/viet-bai.jpg"; ?>
    <?php endif; ?>
<?php endif; ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl($image));?>

<?php $view['slots']->start('content'); ?>

<?php // Tracking ?>
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
<script type='text/javascript'>
    fbq('track', 'ViewJob');
</script>
<?php endif; ?>
<?php // End Tracking ?>

<?php /* detail page */?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<div class="container link-to-thuengay">
    <div class="row-fluid">
        <div class="span12">
            <?php if($view['vlance']->autoHideBanner('21-09-2017 00:00:00', '29-09-2017 23:59:59') === true): ?>
                <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="width: 100%;margin: auto;display: block;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                </a>
                <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="width: 100%;margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150--banner-thuengay.jpg') ?>"/>
                </a>
            <?php else: ?>
                <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="width: 100%;margin: auto;display: block;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150.jpg') ?>"/>
                </a>
                <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150.jpg') ?>"/>
                </a>
                <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                </a>
                <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                </a>
                <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-job-view-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="width: 100%;margin: auto;margin-top: 20px;">
                    <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150.jpg') ?>"/>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="content-section container detail fl-view" 
     <?php if($entity->getCategory()->getId() === 2 || $entity->getCategory()->getId() === 3){echo 'data-display-track="view-job-credit-bid"';}; ?>>
    <div class="row-fluid">
        <div class="span12">
            <?php echo $view->render('VlanceJobBundle:Job/show:breadcrumb.html.php', array('entity' => $entity)) ?>
            <?php  
                /* @var $entity Job */
                $workrooms = $entity->getWorkshopsJob();
                if(is_object($current_user)) :
                    foreach ($workrooms as $workroom) {
                        if($current_user === $workroom->getWorker()) {
                            $workroom_current = $workroom;
                            break;
                        }
                    }
                    if(count($workrooms) > 0 && isset($workroom_current)) : ?>
                        <div class="row-fluid">
                            <h1 class="title">
                                <?php echo ucfirst(htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")); ?>
                                <?php if($entity->getIsPrivate()):?>
                                    <i class="fa fa-lock" title="<?php echo $view['translator']->trans('list_working.content.private_as_freelancer', array(), 'vlance')?>"></i>
                                <?php endif;?>
                                <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                    <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                <?php endif;?>
                                <?php if($entity->getIsReviewed() == true): ?>
                                    <span class="label-tag label-large label-green"
                                          title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
                                <?php endif; ?>
                                <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                    <?php if($entity->getFeaturedFb() == false): ?>
                                        <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                    <?php else: ?>
                                    <?php //Feature facebook job-view?> 
                                        <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                                <?php if($entity->getIsWillEscrow() == false): ?>
                                    <span class="label-tag label-large label-blue" 
                                          title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
                                <?php endif; ?>
                                <?php if($entity->getRequireEscrow() == true): ?>
                                    <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                                <?php endif; ?>
                                <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                                    <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                <?php endif; ?>
                            </h1>
                        </div>
                        <div class="row-fluid">
                            <div class="row-fluid page-tabs">
                                <ul>
                                    <li class="active">
                                        <a href="#">
                                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                    <li>
                                        <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $entity->getId(), 'bid' => $workroom_current->getBid()->getId())) ?>">
                                            <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                                        </a>
                                    </li>                                      
                                </ul>
                                <div class="border"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>

            <div class="row-fluid">
                <div class="span7 news-detail">
                   <?php if(!is_object($current_user) || !isset($workroom_current)) : ?>
                        <div class="row-fluid ">
                            <h1 class="title">
                                <?php echo ucfirst (htmlentities(JobCalculator::replacement($entity->getTitle()), ENT_SUBSTITUTE, "UTF-8")) ?>
                                <?php if($entity->getIsPrivate()):?>
                                    <i class="fa fa-lock" title="<?php echo $view['translator']->trans('list_working.content.private_as_freelancer', array(), 'vlance')?>"></i>
                                <?php endif;?>
                                <?php if($entity->getCreatedAt()->getTimestamp() + 86400 > $now->getTimestamp()):?>
                                    <span class="label-tag label-large label-new"><?php echo $view['translator']->trans('list_job.right_content.new_label', array(), 'vlance') ?></span>
                                <?php endif;?>
                                <?php if($entity->getIsReviewed() == true): ?>
                                    <span class="label-tag label-large label-green"
                                          title="vLance đã trao đổi với khách hàng, và bổ sung các thông tin về dự án.">REVIEWED</span>
                                <?php endif; ?>
                                <?php if($entity->getFeaturedUntil() >= new \DateTime()): ?>
                                    <?php if($entity->getFeaturedFb() == false): ?>
                                        <span class="label-tag label-large label-blue">NỔI BẬT</span>
                                    <?php else: ?>
                                    <?php //Feature facebook job-view ?>
                                        <span class="label-tag label-large label-facebook">NỔI BẬT</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                    
                                <?php if($entity->getIsWillEscrow() == false): ?>
                                    <span class="label-tag label-large label-blue" 
                                          title="Khách hàng muốn thanh toán trực tiếp, và chấp nhận các rủi ro nếu có. Dự án chỉ được nhận tối đa 10 chào giá.">Thanh toán trực tiếp</span>
                                <?php endif; ?>
                                <?php if($entity->getRequireEscrow() == true): ?>
                                    <i class="fa fa-shield require-escrow" title="<?php echo $view['translator']->trans('view_job_page.job_detail.require_escrow', array(), 'vlance') ?>"></i>
                                <?php endif; ?>
                                <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($entity->getUpfrontAt())): ?>
                                    <span class="label-tag label-large label-blue" title="Công việc đã được nạp tiền trước">Đã nạp tiền</span>
                                <?php endif; ?>
                            </h1>
                            <div class="clear"></div>
                        </div>
                    <?php endif; ?>
                    
                    <?php // Block nội dung chi tiết về công việc ?>
                    <div class="row-fluid body-view">
                        
                        <?php // Block thông tin về "dịch vụ" liên quan đến công việc đang xem ?>
                        <?php if($entity->getService()): ?>
                            <div class="row-fluid">
                                <div class="service-need-hire">
                                    <div class="service-title">
                                        <?php echo $view['translator']->trans('view_job_page.job_detail.service.label', array(), 'vlance') ?>: <b><?php echo $entity->getService()->getTitle(); ?></b>
                                    </div>
                                    <?php if(is_object($current_user)): ?>
                                    <div class="service-suggest">
                                        Bạn có thể cung cấp dịch vụ này? <a href="<?php echo $view['router']->generate("account_edit", array('id' => $current_user->getId())); ?>#service-can-provide">Thêm vào hồ sơ làm việc</a>.
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        
                        <div class="span10 description <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text-bao<?php endif; ?>">
                            <?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>
                                <div class="login_view_job">
                                    <div class="span3 btn-connect">
                                        <p class="first">
                                            <?php // echo $view['translator']->trans('site.menu.got_account', array(), 'vlance') ?> 
                                            <strong>Để đọc thông tin, vui lòng đăng nhập bằng một trong hai cách sau:</strong>
                                        </p>
                                        <div class="center">
                                            <div class="job-login-socail">
                                                <a class="btn-facebook-login" onclick="fb_login()">
                                                    <span><img src="/img/icon-facebook.png"></span>
                                                </a>
                                                <a class="btn-google-plus-login" onclick="google_login()">
                                                    <span><img src="/img/icon-google+.png"></span>
                                                </a>
                                                <a class="btn-linkedin-login" onclick="linkedin_login()">
                                                    <span><img src="/img/icon-linkedin.png"></span>
                                                </a>
                                            </div>
<!--                                                    <a class="btn btn-facebook" onclick="fb_login()">
                                                <?php // echo $view['translator']->trans('site.menu.facebook_job', array(), 'vlance') ?>
                                            </a>-->
                                            <div class="job-login-vlance">
                                                Hoặc
                                                <a class="btn btn-primary" href="<?php echo $view['router']->generate('fos_user_security_login')?>">
                                                <?php echo $view['translator']->trans('site.menu.login_job', array(), 'vlance') ?>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="last"> 
                                            Bạn chưa có tài khoản vLance? 
                                            <a class="" href="<?php echo $view['router']->generate('account_register') ?>">
                                                Đăng ký ngay
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="<?php if(!is_object($current_user) && !$entity->getIsCompleted()): ?>blurry-text<?php endif; ?>">
                                <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                                    <?php if (trim($line)): ?>
                                        <p><?php echo $line; ?></p>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <?php // echo nl2br(htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) ?>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <?php // Attachment files ?>
                        <?php $files = $entity->getFiles(); ?>
                        <?php if(count($files)>0): ?>
                            <div class="attach">
                                <label><?php echo $view['translator']->trans('view_job_page.job_detail.attachment', array(), 'vlance').':' ?></label>
                                <ul>
                                    <?php foreach($files as $file) : ?>
                                        <li class="row-fluid">
                                            <div class="i32 i32-attach"></div>
                                            <?php if(!is_object($current_user)) :?>
                                            <a href="#popup-login" data-toggle="modal" 
                                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                            </a>
                                            <?php echo $view->render('VlanceJobBundle:Job:popup_login.html.php', array()) ?>
                                            <?php else : ?>
                                            <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_job',array('job_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                            </a>

                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                    
                    <?php // Danh sách Skills ?>
                    <?php $skills = $entity->getSkills() ?>
                    <?php if(count($skills) > 0): ?>
                        <div class="row-fluid body-view">
                            <?php $counter = 0; ?>
                            <div class="span10 skill">
                                <span><?php echo $view['translator']->trans('common.skill', array(), 'vlance')?></span>
                                <?php foreach ($skills as $skill): ?>
                                    <?php $counter++; ?>
                                   <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => Url::buildUrl(array('kynang' => $skill->getHash())))); ?>" 
                                      title="<?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle() ?></a><?php echo ($counter < count($skills))?",":""; ?>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php // Facebook likes ?>
                    <div class="row-fluid fb-like" data-href="<?php echo $abs_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

                    <?php // Freelancer can give feedback to job ?>
                    <?php // Remove due unnecessary ?>
                    <?php // echo $view->render('VlanceJobBundle:Job/show:give_feedback.html.php', array('entity' => $entity, 'current_user' => $current_user)) ?>

                    <?php // Report abuse job ?>
                    <?php // Remove due unnecessary ?>
                    <?php /*if(is_object($current_user) && $entity->getIsViolated() == false): ?>
                        <div class="report-abuse">
                            <a href="#form_report_abuse" data-toggle="modal" 
                               title="<?php echo $view['translator']->trans('view_job_page.job_detail.violat', array(), 'vlance') ?>">
                                <?php echo $view['translator']->trans('view_job_page.job_detail.violat', array(), 'vlance') ?>
                            </a>
                            <?php //  popup báo cáo vi phạm ?>
                            <div class="modal fade" id="form_report_abuse">
                                <div class="modal-header">
                                    <h3><?php echo $view['translator']->trans('view_job_page.job_detail.violat', array(), 'vlance') ?></h3>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo $view['translator']->trans('view_job_page.job_detail.notice_violat', array(), 'vlance')?></p>
                                </div>
                                <div class="modal-footer">
                                    <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
                                    <a href="<?php echo $view['router']->generate('job_violate',array('id' => $entity->getId())) ?>" 
                                        class="btn btn-primary" title="<?php echo $view['translator']->trans('view_job_page.job_detail.violat', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('view_job_page.job_detail.violat', array(), 'vlance') ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; */?>
                </div>

                <?php // Job information & client description ?>
                <div class="span4 job-information offset1">
                    <div class="job-information-inner">
                    
                        <div class="row-fluid">
                            <?php echo $view->render('VlanceJobBundle:Job/show:job_information.html.php', array('entity' => $entity, 'remain' => $remain)) ?>
                        </div>
                        <div class="row-fluid">
                            <?php echo $view->render('VlanceJobBundle:Job/show:client_information.html.php', array('entity' => $entity)) ?>
                        </div>

                        <?php // Freelancer can pay to get client's contact ?>
                        <?php $job_owner = $entity->getAccount(); ?>
                        <?php // Client has telephone number ?>
                        <?php if(is_object($current_user) && !is_null($job_owner->getTelephone())): ?>                        
                            <?php if ($level > 0): ?>
                                <div class="row-fluid">
                                    <div class="span12 contact-client">
                                        <a class="btn-flat btn-flat-large btn-light-green" data-toggle="modal" href="#pay-credit-contact-client" 
                                            data-request='{"cid":<?php echo $job_owner->getId();?>, "pid":<?php echo is_object($current_user) ? $current_user->getId() : -1;?>}'
                                            data-display-track="show-contact-client-button"
                                            onclick="vtrack('Click contact client', {
                                                    'authenticated':<?php echo is_object($current_user)?'true':'false'?>, 
                                                    'category':'<?php echo $job_owner->getCategory()?$job_owner->getCategory()->getTitle():''?>', 
                                                    'client':'<?php echo $job_owner->getFullname()?>', 
                                                    'client_id':'<?php echo $job_owner->getId()?>', 
                                                    'job_open':<?php echo $entity->getStatus() == Job::JOB_OPEN ? 'true' : 'false'; ?>, 
                                                    'viewer_bidded':<?php echo $entity->isBiddedBy($current_user) ? 'true':'false'; ?>,
                                                    'level':<?php echo $level; ?>
                                            });">
                                            <i class="fa fa-dollar"></i> Liên hệ trực tiếp <span class="label-tag label-dark-green">BETA</span>
                                        </a>
                                    </div>
                                </div>
                                <?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_client.html.php', array('acc' => $current_user, 'form_name' => 'pay-credit-contact-client', 'level' => $level)); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php // END Freelancer can pay to get client's contact ?>
                    </div>
                    
                    <div class="job-view-fb-page-box">
                        <h3>Like page để nhận được việc sớm nhất</h3>
                        <?php echo $view->render('VlanceJobBundle:Job/show:fanpage.html.php', array('entity' => $entity)) ?>
                    </div>
                </div>

                <?php // Bid form ?>
                <?php if($entity->getStatus() == Job::JOB_OPEN) : ?>
                    <?php if($entity->getIsWillEscrow() == true || ($entity->getIsWillEscrow() == false && count($entity->getBids()) < 10)): ?>
                        <?php if(!$entity->isBiddedBy($current_user)):?>
                            <div class="row-fluid form-bid">
                                <div class="span12 container"> 
                                    <h2 class="title"><?php echo $view['translator']->trans('view_job_page.job_detail.bidinfo', array(), 'vlance') ?></h2>
                                    <hr class="hr-line-0">
                                    <?php // Template: VlanceJobBundle:Job:show/bid_form.html.php?>
                                    <?php echo $view['actions']->render($view['router']->generate('bid_new',array('id' => $entity->getId())));?>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif; ?>
                <?php // END Bid form ?>

                <?php // list_bid.html.php ?>
                <div class="detail-bottom span12">
                    <?php echo $view['actions']->render($view['router']->generate('job_show_bid',array('id' => $entity->getId())));?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // SMS Payment popup?>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_contact_freelancer.html.php', array('acc' => $current_user, 'form_name' => 'pay-sms')); ?>
<?php // End SMS Payment popup ?>
<?php /* End detail page */?>
<?php $view['slots']->stop(); ?>