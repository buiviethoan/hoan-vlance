<?php use Vlance\JobBundle\Entity\Job; ?>

<?php $var_array = array(
            'bids'              => $bids,
            'nb_featured_bids'  => $nb_featured_bids,
            'bid_current'       => $bid_current,
            'number_declined'   => $number_declined,
            'job'               => $job,
            'acc_login'         => $acc_login,
            'jobs_worked'       => $jobs_worked
); ?>
<?php if ($job->getType() === Job::TYPE_BID): ?>
    <?php echo $view->render('VlanceJobBundle:Job/show:list_bid.html.php', $var_array); ?>
<?php else: ?>
    <?php echo $view->render('VlanceJobBundle:Job/onsite:list_bid.html.php', $var_array); ?>
<?php endif; ?>