<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $view['slots']->set('title', $entity->getTitle());?>
<?php $view['slots']->start('content')?>
    <div class="information-job-detail">
        <div><?php echo $entity->getCategory() ?></div>
         <div><?php echo $entity->getTitle() ?></div>
         <div><?php echo $entity->getDescription() ?></div>
         <div>ID Công việc: <?php echo $entity->getId() ?></div>
         <?php 
            $now = new \DateTime('now');
            $finish = $entity->getCloseAt();
            $interval = $now->diff($finish);
         ?>
         <div> Chỉ còn: <?php echo $interval->format('%y years,%m months, %d days, %h hours, %i minutes'); ?></div>
         <div> Địa điểm: <?php echo $entity->getCity()? $entity->getCity()->getName():'' ?></div>
         <div>Bắt đầu: <?php echo $entity->getStartDate()->format('d-m-Y') ?></div>
         <div>Ngân sách: <?php echo $entity->getBudget() ?></div>
         <div class="thong-tin-nha-tuyen-dung">
            <h2><?php echo $view['translator']->trans('Thông tin nhà tuyển dụng') ?></h2>
            <?php $acc = $entity->getAccount(); ?>
            <div class="image"><img src="" /></div>
            <div><?php echo $acc->getTitle() ?></div>
            <div><?php echo $acc->getCity()? $entity->getCity()->getName():''; ?></div>
            <div><?php echo $view['translator']->trans('Ngày gia nhập').': ' ?></div>
            <?php  ?>
            <div><?php echo $view['translator']->trans('Việc đã đăng').': '.$number ?></div>
         </div>
    </div>
    <div class="form-bo-thau">
        <?php echo $view['actions']->render($view['router']->generate('bid_new',array('id' => $entity->getId())));?>
    </div>
    <div class="messages send-message"></div>
    <h3>Danh sách bỏ thầu</h3>
    <div class="danh-sach-bo-thau">
        <?php echo $view['actions']->render($view['router']->generate('bid_list_job',array('id' => $entity->getId())));?>
    </div>
<?php $view['slots']->stop(); ?>