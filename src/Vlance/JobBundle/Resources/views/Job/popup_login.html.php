<?php /* popup login */ ?>
            <div class="modal fade" id="popup-login">
                <div class="modal-header">
                  <a class="close" data-dismiss="modal">x</a>
                  <h3><?php echo $view['translator']->trans('common.login', array(), 'vlance') ?></h3>
                </div>
                <div class="modal-body">
                    <?php echo $view['translator']->trans('common.popup_login_before', array(), 'vlance') ?>
                    <a href="<?php echo $view['router']->generate('fos_user_security_login') ?>"><?php echo $view['translator']->trans('common.popup_login', array(), 'vlance') ?></a>
                    <?php echo $view['translator']->trans('common.popup_login_after', array(), 'vlance')."." ?>
                </div>
                <div class="modal-footer">
                  <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
                </div>
            </div>