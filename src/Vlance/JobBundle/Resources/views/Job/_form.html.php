<form action="<?php echo $view['router']->generate('job_preview') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->widget($form)?>
    <div>
        <input type="submit" value="<?php echo $view['translator']->trans('job_preview.button_preview', array(), 'vlance'); ?>"/>
    </div>
</form>