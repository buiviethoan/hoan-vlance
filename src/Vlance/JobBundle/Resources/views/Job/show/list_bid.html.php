<?php use Vlance\BaseBundle\Utils\HasPermission; ?> 

<?php if(count($bid_current) > 0) : ?>
    <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:feature_bid.html.php', array('job' => $job, 'acc' => $acc_login, 'bid' => $bid_current[0])); ?>
<?php endif; ?>

<?php echo $view->render('VlanceJobBundle:Job/show/list_bid:statistics.html.php', array( 'job' => $job, 'bids' => $bids, 'bid_current' => $bid_current, 'number_declined' => $number_declined)) ?>

<?php /* Dưới số đấu thầu */?>
<div class="row-fluid">
    <?php // There is no bid ?>
    <?php if(count($bids) == 0 && count($bid_current) == 0) : ?>
        <?php if($number_declined > 0): ?>
            <div class="no-profile-job">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php else: ?> 
            <div class="no-profile-job">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.no_tender_bid', array(), 'vlance') ?>
            </div>
        <?php endif; ?> 

    <?php // There are some bids ?>
    <?php else : ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : ?>
            <?php $temp = 'VlanceJobBundle:Job/show_as_client/bid:as_client.html.php'; ?>
        <?php else: ?>
            <?php $temp = 'VlanceJobBundle:Job/show/bid:as_freelancer.html.php'; ?>
        <?php endif; ?>
    
        <?php // View bid account current ?>
        <?php if(count($bid_current) > 0) : ?>
            <?php echo $view->render('VlanceJobBundle:Job/show/bid:as_bid_owner.html.php',array('entity' => $bid_current[0], 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
        <?php endif; ?>
        
        <?php // Calculating distance for intersect ad in the list bids ?>
        <?php $distance = ceil(count($bids) / ((round(count($bids)/10)==0)?1:round(count($bids)/10))); ?>
        <?php $i = 0; ?>
        
        <?php // View all bid ?>
        <?php foreach ($bids as $entity) :?>
            <?php $i++; ?>
            <?php if ($entity->getAccount() == $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE) : // As Admin?>
                <?php echo $view->render('VlanceJobBundle:Job/show_as_client/bid:as_client.html.php',array('entity' => $entity, 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
            <?php else : // As Client or Freelancer, check $temp ?>
                <?php echo $view->render($temp,array('entity' => $entity, 'job' => $job,'current_user' => $acc_login, 'jobs_worked' => $jobs_worked)) ?>
                <?php // Show intersect Ads ?>
                <?php if($i%$distance==0 && $i!=count($bids)):?>
                    <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:ad_intersect.html.php',array()) ?>
                <?php endif;?>
            <?php endif; ?>    
        <?php endforeach;?>
    
        <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:ad_bottom.html.php',array()) ?>
    
        <?php // số bid bị từ chối ?>
        <?php  if($number_declined > 0): ?>
            <div class="count_bid_declined span12">
                <?php echo $view['translator']->trans('view_bid__page.bid_job.count_bid_declined', array('%n%' => $number_declined), 'vlance') ?>
            </div>
        <?php endif; ?>

        <?php // Popups ?>
        <?php if($job->getAccount() === $acc_login || HasPermission::hasAdminPermission($acc_login) == TRUE): ?>
            <?php echo $view->render('VlanceJobBundle:Job/show/list_bid:popups.html.php',array('acc_login' => $acc_login, 'entity' => $entity, 'job' => $job)) ?>
        <?php endif; ?>
    <?php endif; ?>       
</div>

<div id="fullchat" class="modal hide fade popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">Nhắn tin</h4>
    </div>
    <div class="modal-body">
        <p>
            Bạn đã dùng hết <b>2</b> lượt liên hệ miễn phí cho freelancer trong dự án này. <br/>
            Với mỗi người tiếp theo, để liên hệ cho họ, bạn cần sử dụng <b>1</b> Credit. <br/>
        </p>
        <br/>
        <p id="totalCredit" style="text-align: center">
            Tổng chi phí: <b>1 CREDIT</b>
        </p>
        <br/>
        <p style="text-align: center">
            <span style="color: #999999">Bạn hiện có: <?php if(is_object($acc_login)){echo $acc_login->getCredit()->getBalance();} ;?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>">MUA THÊM CREDIT</a>
        </p>
        <?php if(is_object($acc_login)): ?>
            <?php if($acc_login->getCredit()->getBalance() < 1): ?>
                <p id="sys-message" style="text-align: center; color: red"><i>Bạn không đủ Credit.</i></p>
            <?php endif; ?>
        <?php else: ?>
            <p id="sys-message" style="text-align: center; color: red"></p>
        <?php endif; ?>
    </div>
    <div class="modal-footer">
        <span style="margin-right: 10px;"><a href="#" data-dismiss="modal" aria-hidden="true">Quay lại</a></span>
        <?php if(is_object($acc_login)): ?>
            <?php if($acc_login->getCredit()->getBalance() < 1): ?>
                <span><a id="btn-buy-credit" class="btn btn-primary btn-large popup-validate" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" onclick="vtrack('Click buy credit to interview')">MUA THÊM CREDIT</a></span>
            <?php else: ?>
                <span><input id="btn-chat" type="button" class="btn btn-primary btn-large popup-validate" value="Nhắn tin" onclick="vtrack('click-confirm-spent-credit-interview')"/></span>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<?php //form hire bid ?>
<div id="form_hire_bid" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Ẩn chào giá</h3>
    </div>
    <div class="modal-body form-hire">
        <form action="" method="post" class="admin_form" style="width: 100%;">
            <input type="hidden" id="bid-token" name="_csrf_token" value=""/>
            <div class="form-actions">
                <div style="width: 100%">
                    <label for="reason">Lý do:</label>
                    <textarea required="required" name="reason" id="reason-hire-bid"></textarea>
                </div>    
<!--                <div style="float: left">
                    <p>
                        <span>Số chào giá vi phạm:</span>
                        <span id="num-bid-hire"></span>
                    </p>
                    <p>
                        <span>Chào giá gần nhất:</span>
                        <span id="bid-latest"></span>
                    </p>
                </div>-->
                <div style="float: right">
                    <label for="banned" style="float: left; margin: 0px 5px">Khóa chào giá 7 ngày</label>
                    <input type="checkbox" name="banned" value="1" style="margin: 4px 5px; float: left"/>
                    <button type="submit" class="btn" style="float: left; margin: 0px 5px">
                        <i class="icon-remove"></i><?php echo $view['translator']->trans('admin.bid.deactive.button.confirm', array(), 'vlance');?>
                    </button>
                </div>    
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var job = "<?php echo $job->getId(); ?>";
    var urlChat = "<?php echo $view['router']->generate('credit_workroom',array()); ?>";
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    $(document).ready(function() {
        jQuery("a.formmessage-call-popup").click(function(){
            jQuery("#formmessage form").attr("action", jQuery(this).attr("data-action")); 
            jQuery("#formmessage h3 span").html(jQuery(this).attr("data-uname")); 
        });
        
        jQuery("a.report-violations-call-popup").click(function(){
            jQuery("#popup-report_violations .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
        });
        
        jQuery("a.refusal-call-popup").click(function(){
            jQuery("#popup-refusal .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
        });        
        
        jQuery("a.formassignment-call-popup").click(function(){
            var arr = jQuery.parseJSON(jQuery(this).attr("data-file"));
            var files = "";
            
            for (i=0; i < arr.length; i++){
                files += "<li class='row-fluid upload'><span class='upload'>"+ arr[i] +"<span></li>";
            }
            jQuery("#formassignment").attr("tabindex", jQuery(this).attr("data-id")); 
            jQuery("#formassignment .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
            jQuery("#formassignment .modal-body #assignment-full-name span").html(jQuery(this).attr("data-uname")); 
            jQuery("#formassignment .modal-body #assignment-description").html(jQuery(this).attr("data-description")); 
            jQuery("#formassignment .modal-body #assignment-city span").html(jQuery(this).attr("data-city")); 
            jQuery("#formassignment .modal-body #assignment-price span").html(jQuery(this).attr("data-price")); 

             if(files !== ""){
                 jQuery('#formassignment .modal-body .assigment-file .title-assigment-file').html("<strong><?php echo $view['translator']->trans('view_bid__page.popup.file', array(), 'vlance').':' ?></storng>");
             };
            jQuery("#formassignment .modal-body .assigment-file ul").html(files); 
        });
        
        $("#formmessage form .button input").click(function() {
          <?php  // Validate cac input file ?>       
           var validate = true;
           if(validate_fileinput('#file_upload_container .new_file_input') == false){
               validate_fileinput('#file_upload_container .new_file_input');
               validate = false;
           }
           if(validate_fileinput('#file_upload_container .new_file_input') == true ){
           $('#file_upload_container .new_file_input label.error').remove();
               if($("#formmessage #fileupload").valid()){
                   $("#formmessage form .button input").submit();
               }
           }

       });
       
       jQuery("a.form-hire-bid").click(function(){
            jQuery("#form_hire_bid form").attr("action", jQuery(this).attr("data-action")); 
            jQuery("#bid-token").attr("value", jQuery(this).attr("data-token"));
            document.getElementById('reason-hire-bid').value = "";
        });
    });
    /* ]]> */
</script>
