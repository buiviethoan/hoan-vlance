<h2><?php echo $view['translator']->trans('view_job_page.job_detail.jobtitle', array(), 'vlance') ?></h2>
<div class="description-job">
    <dl class="dl-horizontal">
        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.id', array(), 'vlance') ?></dt>
        <dd><?php echo $entity->getId() ?></dd>
        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.date_post', array(), 'vlance') ?></dt>
            <?php 
                $today = new DateTime('now');
                $yesterday = new DateTime('yesterday');
                $job_creat = $entity->getCreatedAt();
                $hours = \Vlance\BaseBundle\Utils\JobCalculator::convertHours($job_creat, $today); 
            ?>
            <dd>
                <?php if($hours <= 6 ) : ?>
                    <?php if($hours < 1) : ?>
                        <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                    <?php else: ?>
                        <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60).' '.$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                    <?php endif; ?>
                <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                <?php else : ?>
                    <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                <?php endif; ?>
            </dd>
        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.remain', array(), 'vlance') ?></dt>
        <?php if($remain == false || $entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_FINISHED || $entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_AWARDED) : ?>
            <dd class="has_expired_quote"><?php echo $view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance') ?></dd>
        <?php else: ?>
            <dd><?php echo $remain ?></dd>
        <?php endif; ?>

        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.city', array(), 'vlance') ?></dt>
        <dd><?php echo $entity->getCity()? $entity->getCity()->getName(): '' ?></dd>

        <?php /*
         * * An ngay bat dau cong viec
        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.start_date', array(), 'vlance').':' ?></dt>
        <dd><?php echo $entity->getStartDate()->format('d/m/Y') ?></dd>*/?>

        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.budget', array(), 'vlance') ?></dt>
        <dd><?php echo $entity->getJobBudget($view['vlance']->isAuthen()); ?> 
            <?php if(!$view['vlance']->isAuthen()): ?>
            <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" data-placement="bottom" data-html="true" data-content="Vui lòng <a href='#'>đăng nhập</a> để biết mức ngân sách thực tế."></a>
            <?php endif; ?>
        </dd>
    </dl>
</div>