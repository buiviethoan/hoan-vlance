<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>

<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
<?php $jobs_count = isset($jobs_worked[$acc->getId()]) ? $jobs_worked[$acc->getId()] : 0; ?>
<div class="profile-job span12 
            <?php if($entity->getIsFeatured()): ?> featured-bid <?php endif; ?> 
            <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>
            <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?>">
    <div class="row-fluid">
        <div class="span7 profile-job-left">
            <div class="profile-job-left-bottom">
                <div class="title span6"><?php echo $view['translator']->trans('view_bid__page.bid_detail.info_bid', array(), 'vlance') ?></div>
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <div class="span6 drop-menu">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#popup-refusal" role="button" class="refusal-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_isdeclined',array('id' => $entity->getId())) ?>"
                                   >
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <div class="body-view row-fluid">

                    <?php if($entity->getFullDescription()): ?>                 
                        <div class="textbody">
                            <div>
                                <?php // Hide bid content when client account missing info ?>
                                <?php if($current_user->getId() == $job->getAccount()->getId() && $current_user->getIsMissingClientInfo()):?>
                                    <p><i class="icon-exclamation-sign"></i> Bạn chưa thể xem được nội dung chào giá của freelancer.</p>
                                    <br/>
                                    <p><a href="#popup-missing-info-viewbid" role="button" class="btn-flat btn-light-green btn-flat-large formassignment-call-popup" data-toggle="modal" 
                                        onclick="vtrack('Click view bid', {'location':'Job view page', 'case':'Missing info'})"
                                        data-content="<?php echo "data-content-missing"?>"
                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                            Xem chi tiết
                                    </a></p>
                                <?php else: ?>
                                    <?php if(!$entity->getIntroDescription()): ?>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                    <?php else: ?>
                                        <b>Giới thiệu về kinh nghiệm và kỹ năng</b>:<br/>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getIntroDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                        <br/><br/>
                                        <b>Kế hoạch thực hiện công việc</b>:<br/>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                    <?php endif; ?>
                                <?php endif;?>
                            </div>
                            <div class="creatbid">
                                <span>ID<?php echo $entity->getId(); ?></span> -
                                <span>
                                <?php 
                                    $today = new DateTime('now');
                                    $yesterday = new DateTime('yesterday');
                                    $job_creat = $entity->getCreatedAt();
                                    $hours = JobCalculator::convertHours($job_creat, $today); 
                                ?>
                                <?php if($hours <= 6 ) : ?>
                                    <?php if($hours < 1) : ?>
                                        <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                                    <?php else: ?>
                                        <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60) .$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                                    <?php endif; ?>
                                <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php else : ?>
                                    <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                                <?php endif; ?>
                                </span>
                            </div>
                        </div>

                        <?php // Only admin can edit and deactivate ?>
                        <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                            <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                            </a>
                            <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid">
                <?php $files = $entity->getFiles(); ?>
                <?php if(count($files)>0): ?>
                    <div class="span5 offset1 attach">
                        <ul>
                            <?php foreach($files as $file) : ?>
                            <li class="row-fluid">
                                <div class="i32 i32-attach span2"></div>
                                <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <?php if(is_object($entity->getBidRate())): ?>
            <div class="footer-bid-client">
                <?php $score = $entity->getBidRate()->getRating(); ?>
                <div class="fb-rating-label">
                    <p class="title">Khách hàng đánh giá chào giá của bạn</p>
                    <div class="fb-ratings fb-stars">
                        <?php if((int)$score > 0): ?>
                            <?php for($i = 0; $i < $score; $i++): ?>
                                <a class="fb-star-rating active fb-star-on"></a>
                            <?php endfor; ?>
                        <?php endif; ?>
                        <?php if(5 - (int)$score > 0): ?>
                            <?php for($i = 0; $i < (5 - $score); $i++): ?>
                                <a class="fb-star-rating active"></a>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="span5 profile-job-right">
            <div class="profile-job-right-top">
                <div class="freelancer-row-img span4">
                    <div class="images">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                            <?php if($resize) : ?>
                            <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php else: ?>
                            <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                   <?php if($acc->getNumReviews() != 0): ?>
                        <div class="rating-box" data-toggle="popover" 
                            data-placement="right"  
                            data-content="<?php echo number_format($acc->getScore() / $acc->getNumReviews(),'1',',','.') ?>"  
                            data-trigger="hover">
                            <?php $rating= ($acc->getScore() / $acc->getNumReviews()) * 20; ?>
                            <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                        </div> 
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.rating-box').popover();
                            })
                        </script>
                    <?php else: ?>
                        <div class="rating-box">
                            <div class="rating" style="width:0%"></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="span8">
                    <div class="title">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php echo $acc->getFullName() ?>
                        </a>
                        <?php if ($acc->getIsCertificated()) : ?>
                        <img style="margin-left: -10px; width: 20px; height: 20px; margin-top: -6px;" src="/img/logo_certificated.png" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                             title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if ($acc->getIsCertificated()) : ?>
                        <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                            title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                        <?php endif; ?>
                        <?php if (!is_null($acc->getPersonId())): ?>
                            <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                                <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                    <dl class="dl-horizontal">
                        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.createdat', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCreatedAt()->format('d-m-Y') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job', array(), 'vlance')?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $jobs_count ?>">
                            <?php echo $jobs_count ?> việc
                            </a>
                        </dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.income', array(), 'vlance') ?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                               title="<?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>">
                                <?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                            </a>
                        </dd>
                    </dl>

                    <?php // working user is creator of the job, or the admin of vLance ?>
                    <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                        <?php if($job->getAccount() == $current_user): ?>
                            <?php // When client misses basic info, they can not send message ?>
                            <?php if($current_user->getIsMissingClientInfo()):?>
                                <a href="#popup-missing-info" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                   onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing info'})"
                                   data-content="<?php echo "data-content-missing"?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                </a>
                            <?php else:?>
                                <?php $chatted = false;?>
                                <?php foreach($job->getWorkshopsJob() as $wk):?>
                                    <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                                        <?php $chatted = true;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                                <a href="#formmessage" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                   onclick="vtrack('Click send message', {'location':'Job view page'})"
                                   data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                                   data-id="<?php echo $entity->getId() ?>" 
                                   data-uname="<?php echo $acc->getFullname();?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                        <?php if($chatted):?><i class="icon-white icon-share-alt"></i><?php endif;?>
                                </a>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="profile-job-right-bottom span12">
                <div class="row-fluid">
                    <div class="price">
                        <?php if($current_user->getIsMissingClientInfo()):?>
                            <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                                <?php echo preg_replace('/[0-9]/','x',(string)(number_format( $entity->getAmount(),'0',',','.'))) ?>
                            </span>
                        <?php else:?>
                            <span><?php echo number_format( $entity->getAmount(),'0',',','.') ?></span>
                        <?php endif;?>
                        <span class="less-important"><?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></span>
                        <span class="less-important"> - </span>
                        <?php if($current_user->getIsMissingClientInfo()):?>
                            <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                                <?php echo preg_replace('/[0-9]/','x',(string)($entity->getDuration())) ?>
                            </span>
                        <?php else:?>
                            <span><?php echo $entity->getDuration()?></span>
                        <?php endif;?>
                        <span class="less-important"><?php echo $view['translator']->trans('common.days', array(), 'vlance') ?></span>
                    </div>

                    <?php // Hide when client account missing info ?>
                    <?php if(!$current_user->getIsMissingClientInfo()):?>
                    <div class="row-fluid links">
                        <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                            <?php if($status_job == Vlance\JobBundle\Entity\Job::JOB_OPEN && $job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE && $status_job == Vlance\JobBundle\Entity\Job::JOB_OPEN) : ?>                                    
                            <?php $des = \Vlance\BaseBundle\Utils\JobCalculator::replacement($entity->getFullDescription()); ?>
                                <?php $files_list = array(); ?>
                                <?php foreach ($files as $file):?> 
                                    <?php array_push($files_list, $file->getFilename());?>
                                <?php endforeach;?>                            
                                <a href="#formassignment" role="button" data-toggle="modal" class="btn-flat btn-light-green btn-flat-xlarge formassignment-call-popup"
                                   onclick="vtrack('Click award job', {'location':'Job view page'})"
                                   data-id="<?php echo $entity->getId(); ?>"
                                   data-uname="<?php echo htmlentities($acc->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>"
                                   data-city="<?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?>"
                                   data-description="<?php echo htmlentities(\Vlance\BaseBundle\Utils\JobCalculator::cut(nl2br($des),260), ENT_SUBSTITUTE, "UTF-8")?>"
                                   data-file="<?php echo htmlspecialchars(json_encode($files_list)); ?>"
                                   <?php /* data-date-completion="<?php echo $date_finish ?>"*/?>
                                   data-price="<?php echo number_format( $entity->getAmount(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_awardeat',array('id' => $entity->getId())) ?>" 
                                   title="Giao việc">Giao việc</a>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if($entity->getAwardedAt() ): ?>
                            <div class="winning"></div>
                        <?php endif; ?>
                    </div>
                    <?php endif;?>

                </div>
            </div>

        </div>
    </div>
    <?php if($entity->getIsFeatured()): ?> 
        <div class="featured-bid-info"><span><i class="icon-star"></i> <span>Chào giá được tài trợ</span></span></div>
    <?php endif; ?>
</div>