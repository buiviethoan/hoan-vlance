<h2><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.title', array(), 'vlance') ?></h2>
<div class="info-employment">
    <div class="info-employment-top row-fluid">
        <div class="span4">
            <?php 
                /* @var $acc \Vlance\AccountBundle\Entity\Account */
                $acc = $entity->getAccount(); 
            ?>
            <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '75x75', 75, 75, 1);?>
                <?php if($resize) : ?>
                <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                <?php else: ?>
                    <img width="75" height="75" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" />
                <?php endif; ?>
            </a>
        </div>
        <div class="span8">

            <h3 class="title">
                <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" 
                    title="<?php echo $acc->getFullName() ?>">
                        <?php echo $acc->getFullName() ?>
                </a>
                <?php if($acc->getIsMissingClientInfo()):?>
                    <i class="icon-info icon-user inactive" title="Tài khoản khách hàng thiếu thông tin cơ bản"></i>
                <?php else:?>
                    <i class="icon-info icon-user" title="Tài khoản khách hàng đã có đủ thông tin cơ bản"></i>
                <?php endif;?>
            </h3>
            <div class="work-title"><?php echo $acc->getTitle(); ?></div>
            <div id="client-rating-box">
                <?php 
                    $money_pay = $acc->getOutCome();
                    $info_client = '<div>Tiền đã thuê : <b>'.number_format($money_pay,0,',','.').'</b> VNĐ</div> '
                            . '<div>Việc đã đăng : <b>'.$acc->getNumJob().'</b> việc </div>'
                            . '<div>Việc đã giao : <b>'.$acc->getNumJobDeposit().'</b> việc</div>';
                    $ratio = 0;
                    if($money_pay > 0 && $money_pay <= 1000000) {
                        $ratio = 20;
                    } elseif($money_pay > 1000000 && $money_pay <= 5000000) {
                        $ratio = 40;
                    } elseif($money_pay > 5000000 && $money_pay <= 10000000) {
                        $ratio = 60;
                    } elseif($money_pay > 10000000 && $money_pay <= 50000000) {
                        $ratio = 80;
                    }elseif($money_pay > 50000000 ) {
                        $ratio = 100;
                    }
                ?>
                <div class="box-rating-client" data-toggle="popover" 
                    data-placement="right"  
                    data-content="<?php echo $info_client ?>"  
                    data-trigger="hover" data-html="true">
                    <div class="rating-box-client">
                        <div class="rating-client" style="width:<?php echo $ratio.'%'; ?>"></div>
                    </div>
                </div> 
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('.box-rating-client').popover();
                    })
                </script>
            </div>        
        </div>
    </div>
    <div class="info-employment-bottom row-fluid">
        <dl class="dl-horizontal">
            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
            <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.createat', array(), 'vlance') ?></dt>
            <dd><?php echo $acc->getCreatedAt()->format('d/m/Y') ?></dd>
            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.createnumber', array(), 'vlance') ?></dt>
            <dd><a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" 
                   title="<?php echo $acc->getNumJob()  ?>">
                       <?php echo $acc->getNumJob()  ?> việc
                </a>
            </dd>
        </dl>
    </div>
</div>