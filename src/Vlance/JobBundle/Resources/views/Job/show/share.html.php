
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#popup_share_job").modal('show');
    });
    // Popup window code
    function newPopup(url,url_track) {
        popupWindow = window.open(
                    url,'popUpWindow','height=300,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
            $.ajax(url_track);
            
    }
    
</script>

<div id="popup_share_job" class="modal hide fade popup_share_main" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <b id="myModalLabel">Việc đã được đăng thành công!</b>
  </div>
  <div class="modal-body">
      <p>Bạn có biết, chia sẻ công việc này lên mạng xã hội sẽ tăng cơ hội tìm được freelancer của bạn lên gấp <span class="count-x">3 lần</span>.</p>
        <div class="modal-body-in">
            <div class="ul-pop-x">Chia sẻ ngay lên</div>
            <ul class="ul-pop-share">
                <li class="icon-fb" >
                    <?php $url_track = 'http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=interactive&ea=click&el=job-share-facebook&cs=&cm=page&cn=job' ?>
                    <a href="javascript:void(0);" onclick="return newPopup('https://www.facebook.com/sharer.php?u=<?php echo $url ?>&t=<?php echo $title ?>','<?php echo $url_track ?>')" ></a>
                </li>
                <li class="icon-tw">
                    <?php $url_track = 'http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=interactive&ea=click&el=job-share-twitter&cs=&cm=page&cn=job' ?>
                    <a href="javascript:void(0);" onclick="return newPopup('http://twitter.com/share?text=<?php echo urlencode($title); ?>&url=<?php echo $url ?>','<?php echo $url_track ?>');"> </a>            
                </li>
                <li class="icon-gg">
                    <?php $url_track = 'http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=interactive&ea=click&el=job-share-google&cs=&cm=page&cn=job' ?>
                    <a href="javascript:void(0);" onclick="return newPopup('https://plus.google.com/share?url=<?php echo $url ?>', '<?php echo $url_track ?>');" > </a>
                </li>
                <li class="icon-in">
                    <?php $url_track = 'http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=interactive&ea=click&el=job-share-linkedin&cs=&cm=page&cn=job' ?>
                    <a href="javascript:void(0);" onclick="return newPopup('https://www.linkedin.com/cws/share?title=<?php echo urlencode($title); ?>&url=<?php echo $url ?>', '<?php echo $url_track ?>');"> </a>
                </li>
            </ul>
        </div>
     <div class="link_share">Hoặc gửi liên kết sau tới bạn bè</div>
     <div class="input-share">
         <strong><input type="text" value="<?php echo $url ?>"></strong>      
    </div> 
  </div>
  
</div>

 
