<!DOCTYPE html>
<?php $current_route = $app->getRequest()->attributes->get('_route'); ?>
<html>
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('routeName' => $current_route, '_route_params' => $app->getRequest()->attributes->get('_route_params'))));?>
    <body class="one-column error-page">
        <?php /* Header block */?>
            <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_header', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
            ); ?>

        <div class="no-permission error-section container">
            <div class="row-fluid">
                <h1>Việc bí mật!</h1>
                <p>Rất tiếc, chỉ những freelancers được mời mới có thể xem nội dung công việc này.</p>
                <p>Quay lại trang <b><a href="<?php echo $view['router']->generate('freelance_job_list') ?>">Việc làm</a></b></p>
                <div class="map-error-500"></div>
            </div>
		</div>
		
		<!-- Footer -->
        <?php /* Footer block */?>
        <?php echo $view['actions']->render(
            $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true),
            array('strategy' => 'esi')
        ); ?>
    </body>
</html>