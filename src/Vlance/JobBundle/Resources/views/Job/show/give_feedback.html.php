<?php if (is_object($current_user) && $entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_OPEN && !$current_user->isBanned()) : ?>
    <div class="row-fluid feedback-block">
        <h3 class="feedback-header">Gợi ý cho khách hàng
            <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" 
               data-placement="bottom" data-html="true" 
               data-content="Nếu bạn thấy <strong>thông tin về dự án</strong> vẫn chưa đầy đủ, hoặc <strong>ngân sách dự án</strong> chưa hợp lý, hãy sử dụng một trong hai biểu mẫu dưới đây để gợi ý giúp khách hàng cập nhật thông tin đầy đủ hơn."></a>
        </h3>
        <div class="feedback-content">
            <?php echo $view['actions']->render($view['router']->generate('jfb_new',array('id' => $entity->getId())));?>
        </div>
    </div>
<?php endif;?>