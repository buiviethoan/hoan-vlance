<?php //Snippet ?>
<div style="display: none;">
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
      <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" itemprop="url"> 
        <span itemprop="title">Việc freelance</span>
      </a> 
    </div>  
    <div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
        <?php $cpath = $entity->getCategory() ? $entity->getCategory()->getHash() : '' ; ?>
        <?php $filters = ''; ?>
        <?php if($entity->getCategory()) : ?>
            <?php $chash = $entity->getCategory()->getHash(); ?>
            <?php $cpath = $entity->getCategory()->getParent()->getHash(); ?>
            <?php $filters = 'cpath_'.$cpath.'_chash_'.$chash ; ?>
            › <a href="<?php echo $view['router']->generate('freelance_job_list', array('filters' => $filters)) ?>" itemprop="url">
                <span itemprop="title"><?php echo $entity->getCategory()->getTitle(); ?></span>
            </a>
        <?php endif; ?>
    </div>
</div>
<?php // End Snippet ?>
<?php //Block breadcrumbs ?>
<div class="breadcrumbs">
    <ul>
        <?php $category = $entity->getCategory() ?>
        <?php if($category) : ?>
            <?php $category_parent = $category->getParent() ?>
            <?php if($category_parent) : ?>
                <li>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>" title="Việc freelance">
                        <span>Việc freelance</span>
                    </a>
                </li>
                <li><span>›</span></li>
                <li>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>/cpath_<?php echo $category_parent->getHash(); ?>" title="<?php echo $category_parent->getTitle() ?>">
                        <span><?php echo $category_parent->getTitle() ?></span>
                    </a>
                </li>
                <li><span>›</span></li>
                <li> 
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>/cpath_<?php echo $category_parent->getHash(); ?>_chash_<?php echo $category->getHash(); ?>" title="<?php echo $category->getTitle() ?>">
                        <span><?php echo $category->getTitle() ?></span>
                    </a>
                </li>
            <?php endif; ?>
        <?php endif; ?>
    </ul>
</div>