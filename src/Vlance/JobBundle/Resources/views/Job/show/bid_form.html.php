<?php use Vlance\AccountBundle\Entity\Account; ?>
<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<?php /* @var $current_user Vlance\AccountBundle\Entity\Account; */ ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new \DateTime('now'); ?>
<?php $ready = true; ?>

<?php $is_vip = false; ?>
<?php if(is_object($current_user)): ?>
    <?php if(!is_null($current_user->getTypeVIP())): ?>
        <?php if(\Vlance\BaseBundle\Utils\JobCalculator::ago($current_user->getExpiredDateVip(), $now) !== false): ?>
            <?php $is_vip = true; ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<?php if(isset($bannedTo)): ?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-banned-account-when-bid">
        <h2><?php echo $view['translator']->trans('form.bid.banned.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.banned.reason', array('%bannedTo%' => $bannedTo), 'vlance')?></p>
        <p><?php echo $view['translator']->trans('form.bid.banned.support', array(), 'vlance')?></p>
    </div>
<?php elseif (count($missing_info) > 0) :?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-missing-info-when-bid">
        <h2><?php echo $view['translator']->trans('form.bid.missing_info.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.missing_info.reason', array(), 'vlance')?>:</p>
        <p><b><i><?php echo implode(' - ', $missing_info); ?></i></b></p>
        <p class="action-buttons">
            <a target="_blank" href="<?php echo $view['router']->generate('account_edit', array('id' => $current_user->getId())); ?>?bidform" class="btn btn-large btn-primary">
                <strong><?php echo $view['translator']->trans('form.bid.missing_info.action', array(), 'vlance')?></strong>
            </a>
        </p>
    </div>
<?php /* elseif (isset($service)) :?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-service-when-bid">
        <h2><?php echo $view['translator']->trans('form.bid.service.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.service.reason', array("%service_name%" => $job->getService()->getTitle()), 'vlance')?></p>
        <p><?php echo $view['translator']->trans('form.bid.service.suggest', array(), 'vlance')?></p>
        <p class="action-buttons">
            <a target="_blank" href="<?php echo $view['router']->generate('account_edit', array('id' => $current_user->getId())); ?>?bidform" class="btn btn-large btn-primary">
                <strong><?php echo $view['translator']->trans('form.bid.missing_info.action', array(), 'vlance')?></strong>
            </a>
        </p>
    </div> */ ?>
<?php elseif (isset($require_credit) && is_array($require_credit)) :?>
    <?php if(isset($require_credit['cost']) && isset($require_credit['limit'])): ?>
    <?php $credit_cost = $require_credit['cost']; ?>
    <div class="require-credit-popup">
        <div class="overlay-bidform"></div>    
        <div class="out-of-bid">
            <h2><?php echo $view['translator']->trans('form.bid.require_credit.heading', array(), 'vlance')?></h2>
            <p><?php echo $view['translator']->trans('form.bid.require_credit.reason', array("%%limit%%" => $require_credit['limit']), 'vlance')?></p>
            <p><?php echo $view['translator']->trans('form.bid.require_credit.cost', array("%%cost%%" => $require_credit['cost']), 'vlance')?></p>
            <p class="action-buttons">
                <?php if($require_credit['cost'] > $credit_balance): ?>
                    <?php $ready = false; ?>
                    <a href="<?php echo $view['router']->generate('credit_balance', array()); ?>" class="btn btn-large btn-primary" data-display-track="show-require-credit-bid-not-enough"
                       onclick="vtrack('Click Go to buy credit', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>'})">
                        <strong><?php echo $view['translator']->trans('form.bid.require_credit.action_buy', array(), 'vlance')?></strong>
                    </a>
                <?php else: ?>
                    <a class="btn btn-large btn-primary close-popup" data-display-track="show-require-credit-bid-enough"
                       onclick="vtrack('Click Close require credit popup', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>'})">
                        <strong><?php echo $view['translator']->trans('form.bid.require_credit.action_ok', array(), 'vlance')?></strong>
                    </a>
                <?php endif; ?>
            </p>
        </div>
    </div>
    <?php endif;?>
<?php /* elseif ($current_user->getIsRequireVerifyId() && !$current_user->getIsVerifiedPersonId()) :?>
    <?php $ready = false; ?>
    <div class="overlay-bidform"></div>    
    <div class="out-of-bid" data-display-track="show-block-require-verify-id">
        <h2><?php echo $view['translator']->trans('form.bid.require_verify_id.heading', array(), 'vlance')?></h2>
        <p><?php echo $view['translator']->trans('form.bid.require_verify_id.reason', array("%notice_link%" => "https://www.facebook.com/vlance.vn/posts/1119138941484522"), 'vlance')?></p>
        <p class="action-buttons">
            <a target="_blank" href="<?php echo $view['router']->generate('account_verify_information', array()); ?>?bidform" class="btn btn-large btn-primary">
                <strong><?php echo $view['translator']->trans('form.bid.require_verify_id.action', array(), 'vlance')?></strong>
            </a>
        </p>
    </div>
<?php */ endif;?>
<form <?php if($bid_able):?>
        <?php if($ready): ?>
            id="submit_bid_form" action="<?php echo $view['router']->generate('bid_create',array('id' => $id)) ?>" 
            method="post" <?php echo $view['form']->enctype($form)?>
            data-display-track="show-bid-form-bidable"
        <?php endif; ?>
      <?php else:?>
            class="blurry-content"
            data-display-track="show-bid-form-not-bidable"
      <?php endif;?>>
    <div class="row-fluid">
        <div class="span5">
            <div class="block-amount vlance_jobbundle_bidtype_amount">
                <label class="title-in-form"><?php echo $view['translator']->trans('form.bid.budget', array(), 'vlance') ?><span class="text-red">*</span></label>
                <div class="block-amount-detail">
                    <dl class="dl-horizontal">
                        <dt class="block-amount-label price">
                            Bạn muốn nhận
                        </dt>
                        <dd class="block-amount-price">
                            <div class="input-append">
                                <input id="vlance_jobbundle_bidtype_customers_pay" class="span9" autocomplet="off"
                                       type="text" pattern="^(0|[1-9][0-9/.]*)$" disabled>
                                <span class="add-on bonus"><?php echo $view['translator']->trans('site.append.currency', array(), 'vlance') ?></span>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal block-commision">
                        <dt class="block-amount-label commision">
                            +10% phí vLance
                        </dt>
                        <dd class="block-amount-price">
                            <div class="input-append">
                                <input id="vlance_jobbundle_bidtype_vlance_commision" class="span9" autocomplet="off"
                                       type="text" pattern="^(0|[1-9][0-9/.]*)$" disabled>
                                <span class="add-on bonus"><?php echo $view['translator']->trans('site.append.currency', array(), 'vlance') ?></span>
                            </div>
                        </dd>
                    </dl>
                    
                    <hr class="hr-line-10">
                    
                    <dl class="dl-horizontal">
                        <dt class="block-amount-label price">
                            Khách hàng trả
                        </dt>
                        <dd class="block-amount-price">
                            <div class="bidtype_pay">
                                <?php echo $view['form']->errors($form['amount']) ?>
                                <?php echo $view['form']->widget($form['amount']) ?>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="form-text block-duration">
                <label class="title-in-form"><?php echo $view['translator']->trans('form.bid.duration', array(), 'vlance') ?><span class="text-red">*</span></label>
                <?php echo $view['form']->errors($form['duration']) ?>
                <?php echo $view['form']->widget($form['duration']) ?>            
            </div>
        </div>
        <div class="span7">
            <hr class="hr-line-0">
            <label class="title-in-form"><?php echo $view['translator']->trans('form.bid.description', array(), 'vlance') ?><span class="text-red">*</span></label>
            
            <div class="row-fluid">
                <label class="ques-text"><?php echo $view['translator']->trans('form.bid.intro_description_label', array(), 'vlance') ?><span class="text-red">*</span></label>
                <div class="bidtype_description">
                    <?php echo $view['form']->errors($form['introDescription']) ?>
                    <?php echo $view['form']->row($form['introDescription'])?>
                </div>
            </div>
            
            <div class="row-fluid">
                <label class="ques-text"><?php echo $view['translator']->trans('form.bid.description_label', array(), 'vlance') ?><span class="text-red">*</span></label>
                <div class="bidtype_introdescription">
                    <?php echo $view['form']->errors($form['description']) ?>
                    <?php echo $view['form']->row($form['description'])?>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="fileupload_wrapper">
                    <?php echo $view['form']->row($form['files']) ?>
                </div>
            </div>
            
            <hr class="hr-line-0">
            
            <div class="row-fluid">
                <label class="title-in-form"><?php echo $view['translator']->trans('form.bid.contact', array(), 'vlance') ?></label>
                <div class="input-prepend telephone span6">
                    <span class="add-on <?php if($current_user->getTelephoneVerifiedAt()){echo "verified";} ?>">
                        <i class="fa fa-phone block-contact-icon <?php if($current_user->getTelephoneVerifiedAt()){echo "verified";} ?>" aria-hidden="true"></i>
                    </span>
                    <input id="bidform_telephone" name="input_telephone" <?php if($current_user->getTelephoneVerifiedAt()){echo "disabled";} ?> value="<?php echo $current_user->getTelephone() ?>" placeholder="Số điện thoại">
                </div>
                <div class="input-prepend skype span6">
                    <span class="add-on"><i class="fa fa-skype block-contact-icon" aria-hidden="true"></i></span>
                    <input id="bidform_skype" name="input_skype" value="<?php echo $current_user->getSkype() ?>" placeholder="Skype">
                </div>
            </div>
            
            <div class="row-fluid block-submit">
                <div class="block-submit-label">
                    <p>
                        Khi gửi chào giá, bạn xác nhận đồng ý các <a target="_blank" href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung'));?>" title="<?php echo $view['translator']->trans('footer.vlance.term', array(), 'vlance') ?>">điều khoản sử dụng</a> của vLance.vn, và không để lộ bất kỳ thông tin liên lạc cá nhân nào trong phần đề xuất thuyết phục khách hàng.
                    </p>
                    <p style="font-size: 14px;">
                        <?php if(is_object($current_user)): ?>
                            <?php if(\Vlance\BaseBundle\Utils\JobCalculator::ago($current_user->getExpiredDateVip(), $now) === false): ?>
                                Thoải mái chào giá mà không cần CREDIT <a href="<?php echo $view['router']->generate('credit_balance'); ?>?bidform">NÂNG CẤP VIP NGAY</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </p>
                </div>
                <div class="block-submit-btn">
                    <?php if(is_object($current_user)): ?>
                        <?php // Đã đăng ký làm tài khoản VIP ?>
                        <?php if($is_vip === true):?>
                            <?php // Tài khoản VIP thông thường ?>
                            <?php if($current_user->getTypeVIP() === Account::TYPE_VIP_NORMAL):?>
                                <?php echo $view['form']->row($form['_token']);?>
                                <input type="button" id="btn-submit" value="<?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?>" class="btn btn-primary btn-large span12"
                                    onclick="vtrack('Click Submit bid', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>', 'version':'v2'})"/>
                            <?php // Tài khoản MiniVIP ?>
                            <?php elseif($current_user->getTypeVIP() === Account::TYPE_VIP_MINI):  // Tài khoản MiniVIP ?>
                                <?php if(is_object($current_user->getCategoryVIP())): ?>
                                    <?php if($current_user->getCategoryVIP()->getId() === $job->getCategory()->getId()): ?>
                                        <?php echo $view['form']->row($form['_token']);?>
                                        <input type="button" id="btn-submit" value="<?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?>" class="btn btn-primary btn-large span12"
                                            onclick="vtrack('Click Submit bid', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>', 'version':'v2'})"/>
                                    <?php else: ?>
                                        <a href="#popup-credit-bid" role="button" class="btn btn-primary btn-large span12" data-toggle="modal" onclick="vtrack('Show Popup Expend Credit Bid', {'category':'Thiết kế web, Ứng dụng di động', 'job_budget':'<?php echo $job->getBudget(); ?>'})"><?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?></a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a href="#popup-credit-bid" role="button" class="btn btn-primary btn-large span12" data-toggle="modal" onclick="vtrack('Show Popup Expend Credit Bid', {'category':'Thiết kế web, Ứng dụng di động', 'job_budget':'<?php echo $job->getBudget(); ?>'})"><?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?></a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php // Chưa đăng ký thành tài khoản VIP ?>
                        <?php else: ?>
                            <a href="#popup-credit-bid" role="button" class="btn btn-primary btn-large span12" data-toggle="modal" onclick="vtrack('Show Popup Expend Credit Bid', {'category':'Thiết kế web, Ứng dụng di động', 'job_budget':'<?php echo $job->getBudget(); ?>'})"><?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?></a>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php echo $view['form']->row($form['_token']);?>
                        <input type="button" id="btn-submit" value="<?php echo $view['translator']->trans('form.bid.text_bid', array(), 'vlance'); ?>" class="btn btn-primary btn-large span12"
                            onclick="vtrack('Click Submit bid', {'authenticated':'<?php echo is_object($current_user)?'true':'false'?>', 'location':'View job', 'job_category':'<?php echo $job->getCategory()->getTitle(); ?>', 'job_budget':'<?php echo $job->getBudget(); ?>', 'version':'v2'})"/>
                    <?php endif; ?>
                        
                    <?php if(is_object($current_user)): ?>
                    <!-- Modal credit bid -->
                    <div id="popup-credit-bid" class="modal hide fade popup-credit-bid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4>Chào giá dự án [<?php echo $job->getId(); ?>]</h4>
                        </div>
                        <div class="popup-body-credit-bid">
                            <p class="message-credit-bid">Bạn cần trả <b>1 Credit</b> để chào giá dự án. <br/> Bạn có muốn chào giá dự án?</p>
                            <p class="show-credit-user">
                                <span style="color: #999999">Bạn hiện có: <?php echo $current_user->getCredit()->getBalance();?> CREDIT</span> - <a id="btn-buy-credit" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
                            </p>
                            <p><?php if($current_user->getCredit()->getBalance() < 1): ?><i><span style="color: red;">Bạn không đủ Credit</span></i><?php endif; ?></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
                            <?php if($current_user->getCredit()->getBalance() < 1): ?>
                                <a class="btn btn-primary" href="<?php echo $view['router']->generate('credit_balance', array()); ?>" data-display-track="show-require-credit-bid-not-enough">MUA THÊM CREDIT</a>
                            <?php else: ?>
                                <?php echo $view['form']->row($form['_token']);?>
                                <button id="btn-submit-bid-form" onclick="vtrack('Confirm Expend Credit Bid', {'category':'Thiết kế web, Ứng dụng di động', 'job_budget':'<?php echo $job->getBudget(); ?>'})" class="btn btn-primary">Gửi chào giá</button>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</form>
    


<?php if($bid_able): ?>
<script type="text/javascript">
    var quote_min = <?php echo $budget * 0.25 ?>;
    var quote_max = 2000000000;
    var placeholder = "<?php echo $view['translator']->trans('form.bid.budget_placeholder', array(), 'vlance') ?>";
    var placeholder_pay = "<?php echo number_format($budget>500000?$budget:500000, 0, ',', '.')?>";
    var placeholder_get = "<?php echo number_format(($budget>500000?$budget:500000)*(100-$view['vlance']->getVlanceCommission())/100, 0, ',', '.')?>";
    var placeholder_commision = "<?php echo number_format(($budget>500000?$budget:500000)*($view['vlance']->getVlanceCommission())/100, 0, ',', '.')?>";
    var translations = {
        'attach_empty': "<?php echo $view['translator']->trans('form.bid.attachments_empty', array(), 'vlance')?>"
    };
    var vlance_commission = <?php echo $view['vlance']->getVlanceCommission() ?>;
</script>
<?php endif; ?>