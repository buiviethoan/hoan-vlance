<?php if(!$bid->getIsFeatured()): ?>
<div class="row-fluid">
    <div class="span12 guider-block feature-bid">
        <div class="span6 content inner ">
            <img width="460px" height="250px" src="/img/credit/feature-bid.png" alt="Nhận chào giá" title="Nhận chào giá">
        </div>

        <div class="span6 content detail">
            <div class="note">
                <h4>Bạn muốn tăng cơ hội được giao việc lên gấp <b>3 lần</b>?</h4>
                <p>Hãy đăng <b>nổi bật</b> chào giá của bạn để được:</p>
                <li>Luôn đứng trong <b>Top đầu tiên</b> danh sách chào giá</li>
                <li>Hiển thị nổi bật hơn so với các chào giá thông thường</li>
                <li>Tăng 80% tỉ lệ được khách hàng đọc và nhắn tin</li>
                <p>Số lượng chào giá nổi bật trong mỗi dự án là có giới hạn. Đăng ngay, để thấy hiệu quả tức thì!</p>
                <p class="cta-button">
                    <a class="btn btn-large btn-primary" data-toggle="modal" href="#pay-feature-bid" 
                            data-request='{"bid":"<?php echo $bid->getId();?>","pid":<?php echo $acc->getId();?>}'
                            onclick="vtrack('Click feature-bid')">
                        HIỂN THỊ NỔI BẬT</a>
                </p>
            </div>    
        </div>    
    </div>
</div>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_featurebid.html.php', 
        array('acc'             => $acc, 
            'form_name'         => 'pay-feature-bid', 
            'context_msg_path'  => 'paysms.featurebid')); ?>
<?php endif; ?>
