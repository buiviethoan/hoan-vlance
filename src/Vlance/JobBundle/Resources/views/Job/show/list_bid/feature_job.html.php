<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>
<?php $service_id = CreditExpenseTransaction::SERVICE_FEATURE_JOB; ?>
<?php $service = $view['vlance_payment']->getParameter('credit.spend.' . $service_id); ?>

<div class="span12 guider-block featured-job" data-display-track="show-feature-job-block">
    <div class="span6 content inner ">
        <img width="460px" height="250px" src="/img/credit/feature-job.png" alt="Nhận chào giá" title="Nhận chào giá">
    </div>
    
    <div class="span6 content detail">
        <div class="note">
            <h4><?php echo $view['translator']->trans('view_job_client_page.featured_job.heading_guider_block', array(), 'vlance') ?></h4>
            <li><?php echo $view['translator']->trans('view_job_client_page.featured_job.guider_block_one_description', array('%%duration%%' => $service['duration']), 'vlance') ?></li>
            <li><?php echo $view['translator']->trans('view_job_client_page.featured_job.guider_block_two_description', array(), 'vlance') ?></li>
            <p><?php echo $view['translator']->trans('view_job_client_page.featured_job.guider_block_three_description', array(), 'vlance') ?></p>
            <p class="cta-button">
                <a class="btn btn-large btn-primary" data-toggle="modal" href="#pay-feature-job" 
                        data-request='{"jid":<?php echo $job->getId();?>,"pid":<?php echo $acc->getId();?>}'
                        onclick="vtrack('Click feature-job')"><?php echo $view['translator']->trans('view_job_client_page.featured_job.featured', array(), 'vlance') ?></a>
            </p>
        </div>    
    </div>    
</div>
<?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_featurejob.html.php', 
        array('acc'             => $acc, 
            'form_name'         => 'pay-feature-job', 
            'context_msg_path'  => 'paysms.featurejob')); ?>