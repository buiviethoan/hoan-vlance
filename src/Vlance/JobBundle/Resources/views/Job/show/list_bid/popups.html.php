<?php use Vlance\JobBundle\Entity\Job; ?>
<?php /* @var $job Vlance\JobBundle\Entity\Job */ ?>
<?php // popup gửi tin nhắn cho freelancer ?>
<div id="formmessage" class="formmessage modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">
            <?php echo $view['translator']->trans('view_bid__page.bid_job.messager_to', array(), 'vlance')?>
            <span><?php echo $entity->getAccount()->getFullname(); ?></span>
        </h3>
     </div>
    <div class="modal-body">
            <div>
                <p>
                    <?php if($job->getType() === Job::TYPE_BID): ?>
                       Bạn còn <b><span id="avai-conve"><?php echo $job->getAvailableConversation(); ?></span>/4</b> lượt liên hệ miễn phí với Freelancer trong dự án này.
                        <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="left" title="" data-original-title="Mỗi dự án, bạn được liên hệ miễn phí với 2 Freelancers. Từ người thứ 3 trở đi, chi phí mỗi lượt liên hệ là 1 Credit."></i>
                    <?php endif; ?>
                </p>
            </div>
        <div>
            <?php if(is_object($acc_login)) : ?>
                <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => '25')));?>
            <?php endif; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#formmessage').on('shown', function () {
        $('#vlance_jobbundle_messagetype_content').focus();
    })
</script>

<?php if($acc_login->getIsMissingClientInfo()):?>
    <?php $infos = $acc_login->getMissingClientInfo();?>
    <?php if($infos[0] == "avatar" || $infos[0] == "city" || $infos[0] == "telephone"): ?>
        <?php $edit_form = "account_basic_edit"; ?>
    <?php else: ?>
        <?php $edit_form = "account_edit"; ?>
    <?php endif; ?>
    <?php foreach($infos as $key => $inf):?>
        <?php $infos[$key] = $view['translator']->trans('controller.job.showFreelanceJob.' . $infos[$key], array(), 'vlance'); ?>
    <?php endforeach;?>
    <?php /*popup báo khách hàng thiếu thông tin nên không gửi được tin nhắn cho freelancer */ ?>
    <div id="popup-missing-info" class="popup-refusal modal hide fade" 
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title', array(), 'vlance') ?></h3>
        </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_send_message', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
            <a href="<?php echo $view['router']->generate($edit_form, array('id' => $job->getAccount()->getId(), 'referer' => 'redirect')) ?>" 
               class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
            </a>
        </div>
    </div>

    <?php /*popup báo khách hàng thiếu thông tin nên không xem được nội dung chào giá */ ?>
    <div id="popup-missing-info-viewbid" class="popup-refusal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title_view_bid', array(), 'vlance') ?></h3>
        </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_view_bid', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
            <a href="<?php echo $view['router']->generate($edit_form, array('id' => $job->getAccount()->getId(), 'referer' => 'redirect')) ?>" 
               class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
            </a>
        </div>
    </div>
<?php endif;?>

<?php /*popup báo khách hàng chưa nạp tiền trước nên không liên hệ, nhắn tin, giao việc đc cho freelancer */ ?>
<div id="popup-upfront-escrow" class="popup-refusal modal hide fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_upfront_escrow.popup_title', array(), 'vlance') ?></h3>
     </div>
    <div class="modal-body">
        <p><?php echo $view['translator']->trans('view_bid__page.missing_upfront_escrow.popup_message', array(), 'vlance') ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
    </div>
</div>

<?php /*popup từ chối chào giá */ ?>
<div id="popup-refusal" class="popup-refusal modal hide fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?></h3>
     </div>
    <div class="modal-body">
        <p><?php echo $view['translator']->trans('view_bid__page.popup.notice', array(), 'vlance').'?' ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance'); ?></a>
        <a href="#" 
           class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
               <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
        </a>
    </div>
</div>

<?php /*popup Báo cáo sai pham */ ?>
<div id="popup-report_violations" 
     class="popup-report_violations modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?></h3>
     </div>
    <div class="modal-body">
        <p><?php echo $view['translator']->trans('view_bid__page.popup.notice_report', array(), 'vlance').'?' ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
        <a href="#" 
           class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
               <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
        </a>
    </div>
</div> 

<?php /* Popup xác nhận giao việc cho freelancer */ ?>
<div id="formassignment" class="formassignment modal hide fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?></h3>
    </div>
    <div class="modal-body">
        <div id="assignment-full-name">
            <strong class="full-name"><?php echo $view['translator']->trans('view_bid__page.popup.fullname', array(), 'vlance').': '?> </strong>
            <span></span>
        </div>
        <div class="clear"></div>

        <div class="assignment-description">
            <strong class="description"><?php echo $view['translator']->trans('view_bid__page.popup.description', array(), 'vlance').': '?> </strong>
            <div id="assignment-description"></div>
        </div>
        <div class="clear"></div>
        <?php // Lay file name ?>
        <div class="assigment-file">
            <div class="title-assigment-file"></div>
            <ul>
                <?php // foreach ($files as $file) : ?>
                <li class="row-fluid upload"><span class="upload"><?php // echo $file->getFilename();?></span></li>
                <?php // endforeach;?>
            </ul>
        </div>
        <div class="clear"></div>
        <?php //  end lay file name ?>
        <div id="assignment-city">
            <strong><?php echo $view['translator']->trans('view_bid__page.popup.city', array(), 'vlance').':' ?> </strong>
            <span></span>
        </div>

        <div class="clear"></div>
        <div id="assignment-price">
            <strong><?php echo $view['translator']->trans('view_bid__page.popup.remuneration', array(), 'vlance').': '?></strong>
            <span></span>
        </div>

    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance')?></a>
        <a href="#" 
           class="btn btn-primary actions" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?>">
               <?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?>
        </a>
    </div>
</div>