<div class="row-fluid">
    <div class="information-bidding row-fluid">
        <?php 
        $min = $job->getMinAmount();
        $max = $job->getMaxAmount(); 
        $sum_amount = $job->getSumAmount(); 
        $sum_time = $job->getSumDuration(); 
        $status_job = $job->getStatus();  
        $avg_amount = 0; $avg_time = 0; 
        $num_bids = count($bids) + count($bid_current);
        if($num_bids) {
            $avg_amount = $sum_amount/$num_bids ;
            $avg_time = $sum_time/$num_bids;
        }
        ?>
        <div class="span2 client-bidding-left">
            <span class="bid-counter">
                <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_bid', array(), 'vlance').':' ?>
                    <?php $countbid = count($bids) + $number_declined + count($bid_current); ?>
                    <span class="value">
                        <?php echo $countbid; ?>
                    </span>
            </span>

        </div>

        <?php // Statistic of all bids ?>
        <div class="span6 client-bidding-between">
            <span class="bid-lowest">
                <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_bid_min', array(), 'vlance').':' ?>
                <span class="value"><?php echo number_format($min,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance')?></span>
            </span>
            <span>|</span>

            <span class="bid-average">
                <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_bid_avg', array(), 'vlance').':' ?>
                <span class="value"><?php echo number_format($avg_amount,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance')?></span>
            </span>
            <span>|</span>

            <span class="bid-highest">
                <?php echo $view['translator']->trans('view_bid__page.bid_detail.number_bid_max', array(), 'vlance').':' ?>
                <span class="value"><?php echo number_format($max,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?></span>
            </span>
        </div>
        <div class="span3 client-bidding-right">
            <span class="duration-average">
                <?php echo $view['translator']->trans('view_bid__page.bid_detail.time_bid_avg', array(), 'vlance').':' ?>
                <span class="value"><?php echo round($avg_time).' '.$view['translator']->trans('common.date', array(), 'vlance') ?></span>
            </span>
        </div>
    </div>
</div>