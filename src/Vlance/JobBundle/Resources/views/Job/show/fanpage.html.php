<?php /* Cần check category để hiện ra fanpage tương ứng */ ?>
<?php $page = "https://www.facebook.com/vlance.vn"; ?>

<?php /* @var $entity \Vlance\JobBundle\Entity\Job; */ ?>
<?php if($entity): ?>
    <?php if($entity->getCategory()): ?>
        <?php
        if ($entity->getCategory()->getId() == 2){
            $page = "https://www.facebook.com/vLance.vn.Freelancer.Web";
        } elseif ($entity->getCategory()->getId() == 3){
            $page = "https://www.facebook.com/vLance.vn.Freelancer.MobileApp";
        } elseif ($entity->getCategory()->getParent()){
            if ($entity->getCategory()->getParent()->getId() == 4){
                $page = "https://www.facebook.com/vLance.vn.Freelancer.Thietke";
            } elseif ($entity->getCategory()->getParent()->getId() == 5){
                $page = "https://www.facebook.com/vLance.vn.Freelancer.Copywriting";
            }
        }
        ?>
    <?php endif;?>
<?php endif;?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.7&appId=197321233758025";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-page" data-href="<?php echo $page?>" data-small-header="false" data-hide-cover="false" data-show-facepile="true">
    <blockquote cite="<?php echo $page?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $page?>">VLance.vn</a></blockquote>
</div>