<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>

<?php $currency = "VNĐ"; ?>

<?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && is_null($entity->getUpfrontAt())): ?>
    <?php if($current_step == 'open'): ?>
    <div class="contentnt-section container client-view">
        <div class="row-fluid">
            <div class="span8 offset2 guider-block-after-post-job" data-display-track="show-upfront-escrow-popup">
                <h2 class="title">
                    Chúc mừng bạn đã đăng việc thành công <img src="/img/icon_success-01.png">
                </h2>
                <p><br/></p>
                <p><b>Tiếp theo, bạn cần làm gì?</b></p>
                <p>Hãy hoàn thành nốt 2 bước sau để công việc của bạn được kích hoạt hoàn toàn, và thu hút được các <b>freelancers chất lượng nhất</b>.</p>
                <dl class="dl-horizontal">
                    <dt><span class="label-tag label-blue label-large <?php if($entity->getAccount()->getTelephoneVerifiedAt()):?>strikethrough-text<?php endif; ?>">Bước 1</span></dt>
                    <dd>
                        <?php if($entity->getAccount()->getTelephoneVerifiedAt()):?>
                            <span class="strikethrough-text">Xác thực số điện thoại của người đăng việc: <b><a class="verify-tel-menu"><i class="fa fa-phone"></i> XÁC THỰC</a></b> <i class="icon-ok"></i></span>
                        <?php else: ?>
                            <span>Xác thực số điện thoại của người đăng việc: <b>
                                <a class="verify-tel-menu" href="<?php echo $view['router']->generate('account_verify_information'); ?>"
                                    onclick="vtrack('Click verify telephone', {'location':'job dc', 'amount':<?php echo $view['vlance']->getParameter('payment_gateway.onepay.otp.105.amount') ?>})">
                                    <i class="fa fa-phone"></i> XÁC THỰC
                                </a>
                            </b></span>
                        <?php endif; ?>
                    </dd>
                    <dt><span class="label-tag label-blue label-large">Bước 2</span></dt>
                    <dd>
                        Đặt cọc cho dự án theo hướng dẫn nạp tiền bên dưới đây.
                    </dd>
                </dl>
                <p>Để việc tuyển dụng được hiệu quả, bạn vẫn nhận được tất cả các chào giá từ freelancers. Tuy nhiên bạn chỉ có thể trao đổi và làm việc với freelancers <b>sau khi công việc được kích hoạt</b> theo hướng dẫn bên trên.</p>
                
                <!--Update 30/06/2017: thu 10% DC Copywriting-->
                <?php if($entity->getCategory()->getId() === 48): ?>
                <p class="warning"><strong>Chú ý: Trong trường hợp bạn hủy dự án khi dự án đang thực hiện, vLance sẽ thu của bạn 10% phí quản lý.</strong></p>
                <?php endif; ?>
            </div>

            <div class="span10 offset1 guider-block-after-post-job guider-block">
                <div class="span3 content inner ">
                    <img width="205px" height="205px" src="/img/CHON-FREELANCER.png" alt="Nhận chào giá" title="Nhận chào giá">
                </div>

                <div class="span9 content detail">
                    <div class="project">
                        <h4>Hướng dẫn nạp tiền đặt cọc cho dự án</h4>
                        <p>Số tiền cần đặt cọc: <span div class="code"><?php echo number_format($entity->getBudget(),'0',',','.') . " " . $currency; ?></span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p2', array(), 'vlance'); ?> <span div class="code">DC<?php echo $entity->getId(); ?></span></p>
                    </div>

                    <div class="accordion" id="accordion2">
                        <!-- Thanh toán bằng Chuyển khoản -->
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                                    <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1', array(), 'vlance'); ?> <img width="13px" height="13px" src="/img/chevron-down.png">
                                </a>
                            </div>
                            <div id="collapseOne" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p5_info', array(), 'vlance'); ?></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2_info', array(), 'vlance'); ?></span></p>
                                    <br/>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1_info', array(), 'vlance'); ?></span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3_info', array(), 'vlance'); ?></span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4_info', array(), 'vlance'); ?></span></p>
                                    <br/>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Á Châu ACB</span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">228085579</span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                                    <br/>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Kỹ Thương Techcombank</span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">19030881539011</span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- Thanh toán tại văn phòng -->
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                                    <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2', array(), 'vlance'); ?><img width="13px" height="13px" src="/img/chevron-down.png">
                                </a>
                            </div>
                            <div id="collapseFour" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p1_info', array(), 'vlance'); ?></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p2', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p2_info', array(), 'vlance'); ?></span></p>
                                    <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p3', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p3_info', array(), 'vlance'); ?></span></p>
                                </div>
                            </div>
                        </div>    
                    </div>

                    <div class="sub-info"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3', array(), 'vlance'); ?> <a href="/tro-giup/quan-ly-tien-du-an-181" target="_blank" title="hướng dẫn thanh toán"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3_link', array(), 'vlance'); ?></a> <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3_info', array(), 'vlance'); ?></div>
                </div>    
            </div>
        </div>    
    </div>
    <?php endif; ?>
<?php else: ?>

<?php endif; ?>
