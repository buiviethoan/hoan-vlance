
<?php switch($current_step):?>
<?php case 'open':?>
<div class="span12 guider-block">
    <div class="span3 content inner ">
        <img width="205px" height="205px" src="/img/NHAN-CHAO-GIA.png" alt="Nhận chào giá" title="Nhận chào giá">
    </div>
    
    <div class="span9 content detail">
        <div class="note">
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.heading_guider', array(), 'vlance'); ?></p>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p1', array(), 'vlance'); ?> <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" data-placement="bottom" data-html="true" data-content="<li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.icon_question1', array(), 'vlance'); ?><li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.icon_question2', array(), 'vlance'); ?><li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.icon_question3', array(), 'vlance'); ?>" data-original-title="" title=""></a></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p2', array(), 'vlance'); ?></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p3', array(), 'vlance'); ?> <a href="<?php echo $view['router']->generate('freelancer_list') ?>" target="_blank" title="danh mục freelancer"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p3_link', array(), 'vlance'); ?></a></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p4', array(), 'vlance'); ?> <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" data-placement="bottom" data-html="true" data-content="<?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.icon_question4', array(), 'vlance'); ?>" data-original-title="" title=""></a></li>
            
        </div>    
        <div class="sub-info"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p5', array(), 'vlance'); ?> <a href="/page/dieu-khoan-su-dung-danh-cho-khach-hang" target="_blank" title="điều khoản sử dụng"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p5_link', array(), 'vlance'); ?></a> <?php echo $view['translator']->trans('view_job_client_page.guider_block.block1.guide_p5_info', array(), 'vlance'); ?></div>
    </div>    
</div>
<?php break;?>

<?php case 'awarded':?>
<div class="span12 guider-block">
    <div class="span3 content inner ">
        <img width="205px" height="205px" src="/img/CHON-FREELANCER.png" alt="Nhận chào giá" title="Nhận chào giá">
    </div>
    
    <div class="span9 content detail">
        <div class="project">
            <ul>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p1', array(), 'vlance'); ?></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p2', array(), 'vlance'); ?> <span div class="code">VC<?php echo $entity->getId(); ?></span></li>
            </ul>   
        </div>
            
        <div class="accordion" id="accordion2">
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                        <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1', array(), 'vlance'); ?> <img width="13px" height="13px" src="/img/chevron-down.png">
                    </a>
                </div>
                <div id="collapseOne" class="accordion-body collapse">
                    <div class="accordion-inner">
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2_info', array(), 'vlance'); ?></span></p>
                        <br/>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1_info', array(), 'vlance'); ?></span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3_info', array(), 'vlance'); ?></span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4_info', array(), 'vlance'); ?></span></p>
                        <br/>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Á Châu ACB</span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">228085579</span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                        <br/>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <span div class="information">Ngân hàng TMCP Kỹ Thương Techcombank</span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <span div class="information">19030881539011</span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <span div class="information">Hà Nội</span></p>
                    </div>
                </div>
            </div>
            
            <?php /*    
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                        2.Chuyển khoản qua hệ thống 123 Pay
                    </a>
                </div>
                <div id="collapseTwo" class="accordion-body collapse">
                    <div class="accordion-inner">
                        Anim pariatur cliche...
                    </div>
                </div>
            </div>
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                        3.Chuyển khoản qua hệ thống Paypal
                    </a>
                </div>
                <div id="collapseThree" class="accordion-body collapse">
                    <div class="accordion-inner">
                        Anim pariatur cliche...
                    </div>
                </div>
            </div> */ ?>
            
            <div class="accordion-group">
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
                        <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2', array(), 'vlance'); ?><img width="13px" height="13px" src="/img/chevron-down.png">
                    </a>
                </div>
                <div id="collapseFour" class="accordion-body collapse">
                    <div class="accordion-inner">
                        <p><span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p1_info', array(), 'vlance'); ?></span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p2', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p2_info', array(), 'vlance'); ?></span></p>
                        <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p3', array(), 'vlance'); ?>: <span div class="information"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction2_p3_info', array(), 'vlance'); ?></span></p>
                    </div>
                </div>
            </div>    
        </div>
              
        <div class="sub-info"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3', array(), 'vlance'); ?> <a href="/tro-giup/quan-ly-tien-du-an-181" target="_blank" title="hướng dẫn thanh toán"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3_link', array(), 'vlance'); ?></a> <?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.guide_p3_info', array(), 'vlance'); ?></div>
    </div>    
</div>
<?php break;?>

<?php case 'escrowed':?>
<div class="span12 guider-block">
    <div class="span3 content inner ">
        <img width="205px" height="205px" src="/img/CAM-KET-&-LAM-VIEC.png" alt="Nhận chào giá" title="Nhận chào giá">
    </div>
    
    <div class="span9 content detail">
        <div class="step-three">
            <ul>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p1', array(), 'vlance'); ?> <span><b><a href="<?php echo $view['router']->generate('workshop_bid', array('job_id' => $entity->getId(), 'bid' => ($entity->getAwardedBid())?$entity->getAwardedBid()->getId():null)) ?>" target="_blank" title="tin nhắn"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p1_link', array(), 'vlance'); ?></a></b></span></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p2', array(), 'vlance'); ?> <a href="/tro-giup/mau-cam-ket-lam-viec-cho-khach-hang-va-freelancer-164" target="_blank" title="Cam kết làm việc"><?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p2_link', array(), 'vlance'); ?></a> <?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p2_info', array(), 'vlance'); ?></li>
                <li><?php echo $view['translator']->trans('view_job_client_page.guider_block.block3.guide_p3', array(), 'vlance'); ?></li>
            </ul>
        </div>    
    </div>   
</div>   
<?php break;?>

<?php default:?>
<?php break;?>

<?php endswitch;?>