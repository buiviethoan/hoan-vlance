<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>

<div class="body-view hidebody">
    <?php if($entity->getService()): ?>
        <div class="row-fluid">
            <div class="service-need-hire">
                <div class="service-title">
                    <?php echo $view['translator']->trans('view_job_page.job_detail.service.label', array(), 'vlance') ?>: <b><?php echo $entity->getService()->getTitle(); ?></b>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row-fluid textbody">
        <div class="description">
            <?php if($entity->getType() == Job::TYPE_BID): ?>
                <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                    <?php if (trim($line)): ?>
                        <p><?php echo $line; ?></p>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php elseif($entity->getType() == Job::TYPE_CONTEST): ?>
            <div <?php if($entity->getValid() == false) echo 'class="blurry-text"'?>>
                <?php foreach (explode("\n", htmlentities(JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                    <?php if (trim($line)): ?>
                        <p><?php echo $line; ?></p>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
                    
            <?php // echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement($entity->getDescription()), ENT_SUBSTITUTE, "UTF-8")) ?>
        </div>
        <?php $files = $entity->getFiles(); ?>
        <?php if(count($files)>0): ?>
            <div class="attach">
                <label>
                    <?php echo $view['translator']->trans('view_job_page.job_detail.attachment', array(), 'vlance').':' ?>
                </label>
                <ul>
                    <?php foreach($files as $file) : ?>                                                
                        <li class="row-fluid">
                            <div class="i32 i32-attach"></div>
                            <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_job',array('job_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                               class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>">
                                   <?php echo $file->getFilename() ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
        <?php $skills = $entity->getSkills() ?>
        <?php if(count($skills) > 0): ?>
            <div class="skill">
                <label><?php echo $view['translator']->trans('common.skill', array(), 'vlance').':' ?></label>
                <div class="list-skill">
                    <?php foreach ($skills as $skill): ?>
                       <a title="<?php print $skill->getTitle(); ?>"> <?php print $skill->getTitle().',' ?></a>
                    <?php endforeach;?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="row-fluid fb-like" data-href="<?php echo $abs_url?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

    <?php // Freelancer can give feedback ?>
    <?php // Remove due unnecessary ?>
    <?php /* if (is_object($current_user)) :?>

    <div class="row-fluid feedback-block">
        <h3 class="feedback-header">Freelancer gợi ý gì về dự án này của bạn?
            <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" 
               data-placement="bottom" data-html="true" 
               data-content="Rất nhiều freelancer muốn thực hiện dự án của bạn, tuy nhiên nếu thông tin <strong>mô tả về dự án</strong> quá sơ sài, hoặc <strong>ngân sách dự án</strong> chưa hợp lý sẽ khiến cho freelancer chần chừ hoặc không muốn gửi chào giá. Dưới đây là những gợi ý của freelancer về dự án của bạn, bao gồm cả những người chưa chính thức gửi chào giá."></a>
            <span class="label-new">Mới</span>
        </h3>
        <div class="feedback-content">
            <?php  echo $view['actions']->render($view['router']->generate('jfb_show',array('job_id' => $entity->getId())));?>
        </div>
    </div>
    <?php endif; */?>
    <?php // End Freelancer can give feedback ?>
</div>