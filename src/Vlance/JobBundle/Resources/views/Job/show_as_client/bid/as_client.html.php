<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
<?php $jobs_count = isset($jobs_worked[$acc->getId()]) ? $jobs_worked[$acc->getId()] : 0; ?>

<?php if($job->getTestType() != Job::TEST_TYPE_UPFRONT_ESCROW || ($job->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW && !is_null($job->getUpfrontAt()))): ?>
    <?php $is_allowed = true; ?>
<?php else: ?>
    <?php $is_allowed = false; ?>
<?php endif; ?>

<div class="profile-job span12 
        <?php if($entity->getIsFeatured()): ?> featured-bid <?php endif; ?> 
        <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>
        <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?>">
    <div class="row-fluid">
        <!-- Left block -->
        <div class="span7 profile-job-left">
            <div class="profile-job-left-bottom">
                <div class="title span6"><?php echo $view['translator']->trans('view_bid__page.bid_detail.info_bid', array(), 'vlance') ?>
                    <?php if($acc->getTelephone() && $acc->getEmail() && !($acc->isBanned()) && $acc->isTypeFreelancer()):?>
                        <span class="contact-block">
                            <a class="btn btn-primary hidejs" data-toggle="modal" href="#pay-sms" 
                                    data-request='{"fid":<?php echo $acc->getId();?>, "pid":<?php echo is_object($current_user) ? $current_user->getId() : -1;?>}'
                                    onclick="vtrack('Click contact', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>','location':'job view as client', 'category':'<?php echo $acc->getCategory()?$acc->getCategory()->getTitle():''?>', 'freelancer':'<?php echo $acc->getFullname()?>', 'freelancer_id':'<?php echo $acc->getId()?>'})">
                                <i class="fa fa-phone fa-lg"></i> <?php echo $view['translator']->trans('view_bid__page.contact', array(), 'vlance') ?></a>
                        </span>
                    <?php endif;?>
                </div>
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <div class="span6 drop-menu">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <div class="body-view row-fluid">
                    <?php if($entity->getFullDescription()): ?>                 
                        <div class="textbody">
                            <div>
                                <?php // Hide bid content when client account missing info ?>
                                <?php if($current_user->getId() == $job->getAccount()->getId() && $current_user->getIsMissingClientInfo()):?>
                                    <p><i class="icon-exclamation-sign"></i> Bạn chưa thể xem được nội dung chào giá của freelancer.</p>
                                    <br/>
                                    <p><a href="#popup-missing-info-viewbid" role="button" class="btn-flat btn-light-green btn-flat-large formassignment-call-popup" data-toggle="modal" 
                                        onclick="vtrack('Click view bid', {'location':'Job view page', 'case':'Missing info'})"
                                        data-content="<?php echo "data-content-missing"?>"
                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                            Xem chi tiết
                                    </a></p>
                                <?php else: ?>
                                    <?php if(!$entity->getIntroDescription()): ?>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                    <?php else: ?>
                                        <b>Giới thiệu về kinh nghiệm và kỹ năng</b>:<br/>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getIntroDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                        <br/><br/>
                                        <b>Kế hoạch thực hiện công việc</b>:<br/>
                                        <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                    <?php endif; ?>
                                <?php endif;?>
                            </div>
                            <div class="creatbid">
                                <span>ID<?php echo $entity->getId(); ?></span> -
                                <span>
                                <?php 
                                    $today = new DateTime('now');
                                    $yesterday = new DateTime('yesterday');
                                    $job_creat = $entity->getCreatedAt();
                                    $hours = JobCalculator::convertHours($job_creat, $today); 
                                ?>
                                <?php if($hours <= 6 ) : ?>
                                    <?php if($hours < 1) : ?>
                                        <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                                    <?php else: ?>
                                        <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60) .$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                                    <?php endif; ?>
                                <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                                <?php else : ?>
                                    <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                                <?php endif; ?>
                                </span>
                            </div>
                        </div>

                        <?php // Only admin can edit and deactivate ?>
                        <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                            <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                            </a>
                            <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid">
                <?php $files = $entity->getFiles(); ?>
                <?php if(count($files)>0): ?>
                    <div class="span12 attach">
                        <ul>
                            <?php foreach($files as $file) : ?>
                            <li class="row-fluid">
                                <div class="i32 i32-attach span1"></div>
                                <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <?php $now = new \DateTime('now'); ?>
            <?php if(($job->getStatus() === Job::JOB_OPEN) && (JobCalculator::ago($job->getCloseAt(), $now) !== false)): ?>
            <div class="footer-bid-client">
                <?php if(!is_object($entity->getBidRate())): ?>
                    <p class="title">Bạn đánh giá chất lượng chào giá thế nào?</p>
                    <div class="fb-rating-label">
                        <div class="fb-ratings fb-stars">
                            <?php /*<a class="fb-star-rating star1 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 1);" onmouseout="funcOut(<?php echo $entity->getId(); ?>);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 1);"></a>
                            <a class="fb-star-rating star2 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 2);" onmouseout="funcOut(<?php echo $entity->getId(); ?>);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 2);"></a>
                            <a class="fb-star-rating star3 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 3);" onmouseout="funcOut(<?php echo $entity->getId(); ?>);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 3);"></a>
                            <a class="fb-star-rating star4 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 4);" onmouseout="funcOut(<?php echo $entity->getId(); ?>);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 4);"></a>
                            <a class="fb-star-rating star5 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 5);" onmouseout="funcOut(<?php echo $entity->getId(); ?>);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 5);"></a> */ ?>
                            <a class="fb-star-rating star1 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 1);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 1);"></a>
                            <a class="fb-star-rating star2 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 2);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 2);"></a>
                            <a class="fb-star-rating star3 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 3);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 3);"></a>
                            <a class="fb-star-rating star4 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 4);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 4);"></a>
                            <a class="fb-star-rating star5 <?php echo $entity->getId(); ?>" onmouseover="funcLabel(<?php echo $entity->getId(); ?>, 5);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 5);"></a>
                        </div>
                        <div class="fb-text">
                            <p class="fb-text-star star1-<?php echo $entity->getId(); ?>">Rất kém, spam</p>
                            <p class="fb-text-star star2-<?php echo $entity->getId(); ?>">Kém, không liên quan đến việc cần làm</p>
                            <p class="fb-text-star star3-<?php echo $entity->getId(); ?>">Tạm được, cần bổ sung thêm</p>
                            <p class="fb-text-star star4-<?php echo $entity->getId(); ?>">Khá ổn, chưa thực sự xuất sắc</p>
                            <p class="fb-text-star star5-<?php echo $entity->getId(); ?>">Chào giá chuyên nghiệp, tôi rất thích!</p>
                        </div>
                        <?php $num = 5 - count($job->getBidRates()); ?>
                        <?php if($num > 0): ?>
                            <div class="fb-note">
                                <p>Đánh giá <span class="count-rated"><?php echo $num;?> lần</span> bạn sẽ được tặng 1 credit/job 
                                    <i class="fa fa-question-circle"
                                        data-toggle="popover" 
                                        data-placement="top" 
                                        data-content="Với điều kiện job đã được nạp tiền vào hệ thống" 
                                        data-trigger="click"
                                        data-html="true">
                                    </i>
                                </p>
                            </div>
                        <?php endif; ?>
                    </div>
<!--                    <div class="fb-finish">Đã gửi đánh giá</div>-->
                    <script type="text/javascript">
                        var urlRating = "<?php echo $view['router']->generate('rating_bid',array()); ?>";
                    </script>
                    <script type="text/javascript">$(document).ready(function() {$('i[data-toggle=popover]').popover();});</script>
                <?php else: ?>
                    <?php $score = $entity->getBidRate()->getRating(); ?>
                    <div class="fb-rating-label">
                        <p class="title">Cám ơn bạn đã đánh giá</p>
                        <div class="fb-ratings fb-stars">
                            <?php if((int)$score > 0): ?>
                                <?php for($i = 0; $i < $score; $i++): ?>
                                    <a class="fb-star-rating active fb-star-on"></a>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <?php if(5 - (int)$score > 0): ?>
                                <?php for($i = 0; $i < (5 - $score); $i++): ?>
                                    <a class="fb-star-rating active"></a>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
        
        <!-- Right block -->
        <div class="span5 profile-job-right">
            <!-- Profile -->
            <div class="profile-job-right-top">
                <!-- Avatar -->
                <div class="freelancer-row-img span4">
                    <div class="images">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php $resize = ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                            <?php if($resize) : ?>
                            <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php else: ?>
                            <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                   <?php if($acc->getNumReviews() != 0): ?>
                        <div class="rating-box" data-toggle="popover" 
                            data-placement="right"  
                            data-content="<?php echo number_format($acc->getScore() / $acc->getNumReviews(),'1',',','.') ?>"  
                            data-trigger="hover">
                            <?php $rating= ($acc->getScore() / $acc->getNumReviews()) * 20; ?>
                            <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                        </div> 
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.rating-box').popover();
                            })
                        </script>
                    <?php else: ?>
                        <div class="rating-box">
                            <div class="rating" style="width:0%"></div>
                        </div>
                    <?php endif; ?>
                </div>
                
                <!-- Freelancer's information -->
                <div class="span8">
                    <div class="title">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php echo $acc->getFullName() ?>
                        </a>
                        <?php if ($acc->getIsCertificated()) : ?>
                        <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                            title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                        <?php endif; ?>
                        <?php if (!is_null($acc->getPersonId())): ?>
                            <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                                <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php /* comment        
                        <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                        */ ?>   
                        <script type="text/javascript" language="javascript">
                            jQuery(document).ready(function(){
                                $(function () {
                                    $('[data-toggle="tooltip"]').tooltip()
                                    })        
                                });
                        </script>    
                    </div>
                    
                    <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                    
                    <dl class="dl-horizontal">
                        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.createdat', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCreatedAt()->format('d-m-Y') ?></dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job', array(), 'vlance')?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $jobs_count ?>">
                            <?php echo $jobs_count ?> việc
                            </a>
                        </dd>
                        <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.income', array(), 'vlance') ?></dt>
                        <dd>
                            <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                               title="<?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>">
                                <?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                            </a>
                        </dd>
                    </dl>

                    <!-- Send message button -->
                    <?php // working user is creator of the job, or the admin of vLance ?>
                    <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                        <?php if($job->getAccount() == $current_user): ?>
                            <?php // When client misses basic info, they can not send message ?>
                            <?php if($current_user->getIsMissingClientInfo()):?>
                                <a href="#popup-missing-info" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                   onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing info'})"
                                   data-content="<?php echo "data-content-missing"?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                </a>
                            <?php else:?>
                                <?php if(!$is_allowed): // khách hàng chưa nạp tiền Upfront escrow (với các KH đc test) ?>
                                    <a href="#popup-upfront-escrow" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                        onclick="vtrack('Click send message', {'location':'Job view page', 'case':'Missing Upfront Escrow'})"
                                        title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                    </a>
                                <?php else: ?>
                                    <?php $chatted = false;?>
                                    <?php foreach($job->getWorkshopsJob() as $wk):?>
                                        <?php if($wk->getWorker()->getId() == $entity->getAccount()->getId()):?>
                                            <?php $chatted = true; break;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                    <?php if($chatted):?>
                                        <a href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $entity->getId())) ?>" 
                                                role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup"
                                                title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.view_message', array(), 'vlance') ?>
                                        </a>
                                    <?php else: ?>
                                        <?php if($job->getAvailableConversation() > 0): ?>
                                        <a href="#formmessage" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                                onclick="vtrack('Click send message', {'location':'Job view page'})"
                                                data-action="<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>" 
                                                data-id="<?php echo $entity->getId() ?>" 
                                                data-uname="<?php echo $acc->getFullname();?>"
                                                title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                        </a>
                                        <?php else: ?>
                                            <?php if($job->getAccount()->getCredit()->getBalance() < 1): ?>
                                            <a href="#fullchat" role="button" class="btn-flat btn-light-blue btn-flat-large" data-toggle="modal" 
                                                    onclick="setBid(
                                                            '<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>',
                                                            '<?php echo $entity->getId(); ?>',
                                                            '<?php echo $acc->getFullname();?>'
                                                            ), vtrack('show-require-credit-interview',{'enough-credit': 'false'})"
                                                    title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                            </a>
                                            <?php else: ?>
                                            <a href="#fullchat" role="button" class="btn-flat btn-light-blue btn-flat-large" data-toggle="modal" 
                                                    onclick="setBid(
                                                            '<?php echo $view['router']->generate('message_new',array('bid' => $entity->getId())) ?>',
                                                            '<?php echo $entity->getId(); ?>',
                                                            '<?php echo $acc->getFullname();?>'
                                                            ), vtrack('show-require-credit-interview',{'enough-credit': 'true'})"
                                                    title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>
                                            </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <!-- Proposal -->
            <div class="profile-job-right-bottom span12">
                <div class="row-fluid">
                    <!-- Quotation -->
                    <div class="price">
                        <!-- Price -->
                        <?php if($current_user->getIsMissingClientInfo()):?>
                            <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                                <?php echo preg_replace('/[0-9]/','x',(string)(number_format( $entity->getAmount(),'0',',','.'))) ?>
                            </span>
                        <?php else:?>
                            <span><?php echo number_format( $entity->getAmount(),'0',',','.') ?></span>
                        <?php endif;?>
                        <span class="less-important"><?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></span>
                        <span class="less-important"> - </span>
                        
                        <!-- Duration -->
                        <?php if($current_user->getIsMissingClientInfo()):?>
                            <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                                <?php echo preg_replace('/[0-9]/','x',(string)($entity->getDuration())) ?>
                            </span>
                        <?php else:?>
                            <span><?php echo $entity->getDuration()?></span>
                        <?php endif;?>
                        <span class="less-important"><?php echo $view['translator']->trans('common.days', array(), 'vlance') ?></span>
                    </div>

                    <!-- Buttons -->
                    <?php // Hide when client account missing info ?>
                    <?php if(!$current_user->getIsMissingClientInfo()):?>
                    <div class="row-fluid links">
                        <!-- Award -->
                        <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                            <?php if($status_job == Job::JOB_OPEN && $job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE && $status_job == Job::JOB_OPEN) : ?>
                                <?php if(!$is_allowed): // khách hàng chưa nạp tiền Upfront escrow (với các KH đc test) ?>
                                    <a href="#popup-upfront-escrow" role="button" class="btn-flat btn-light-blue btn-flat-large formmessage-call-popup" data-toggle="modal" 
                                        onclick="vtrack('Click award job', {'location':'Job view page', 'case':'Missing Upfront Escrow'})" title="Giao việc">Giao việc</a>
                                <?php else: ?>
                                    <?php $des = JobCalculator::replacement($entity->getFullDescription()); ?>
                                        <?php $files_list = array(); ?>
                                        <?php foreach ($files as $file):?> 
                                            <?php array_push($files_list, $file->getFilename());?>
                                        <?php endforeach;?>                            
                                        <a href="#formassignment" role="button" data-toggle="modal" class="btn-flat btn-light-green btn-flat-xlarge formassignment-call-popup"
                                           onclick="vtrack('Click award job', {'location':'Job view page'})"
                                           data-id="<?php echo $entity->getId(); ?>"
                                           data-uname="<?php echo htmlentities($acc->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>"
                                           data-city="<?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?>"
                                           data-description="<?php echo htmlentities(JobCalculator::cut(nl2br($des),260), ENT_SUBSTITUTE, "UTF-8")?>"
                                           data-file="<?php echo htmlspecialchars(json_encode($files_list)); ?>"
                                           <?php /* data-date-completion="<?php echo $date_finish ?>"*/?>
                                           data-price="<?php echo number_format( $entity->getAmount(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>"
                                           data-action="<?php echo $view['router']->generate('bid_update_awardeat',array('id' => $entity->getId())) ?>" 
                                           title="Giao việc">Giao việc</a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <!-- Refuse -->
                        <?php if(!$entity->getAwardedAt() ): ?>
                            <?php if(!$is_allowed): // khách hàng chưa nạp tiền Upfront escrow (với các KH đc test) ?>
                                <a href="#popup-upfront-escrow" role="button" class="btn-flat btn-flat-no-border btn-light-red btn-flat-xlarge formmessage-call-popup" data-toggle="modal" 
                                    onclick="vtrack('Click refuse bid', {'location':'Job view page', 'case':'Missing Upfront Escrow'})"
                                    title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                                        <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                </a>
                            <?php else: ?>
                                <a href="#popup-refusal" role="button" class="refusal-call-popup btn-flat btn-flat-no-border btn-light-red btn-flat-xlarge" data-toggle="modal"
                                   onclick="vtrack('Click refuse bid', {'location':'Job view page'})"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_isdeclined',array('id' => $entity->getId())) ?>">
                                        <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <!-- Badge -->
                        <?php if($entity->getAwardedAt() ): ?>
                            <div class="winning"></div>
                        <?php endif; ?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>