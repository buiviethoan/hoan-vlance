<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>

<?php /* @var $entity Job */ ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php $now = new DateTime('now'); ?>

<div class="row-fluid">
    <h2><?php echo $view['translator']->trans('view_job_page.job_detail.jobtitle', array(), 'vlance') ?></h2>
    <div class="description-job">
        <dl class="dl-horizontal">
            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.id', array(), 'vlance') ?></dt>
            <dd><?php echo $entity->getId() ?></dd>
            <?php // Created on ?>
            <dt><?php echo $view['translator']->trans('view_job_client_page.job_detail.creatAt', array(), 'vlance') ?></dt>
            <?php 
                $today = new DateTime('now');
                $yesterday = new DateTime('yesterday');
                $job_creat = $entity->getCreatedAt();
                $hours = JobCalculator::convertHours($job_creat, $today); 
            ?>
            <dd>
                <?php if($hours <= 6 ) : ?>
                    <?php if($hours < 1) : ?>
                        <?php echo round($hours*60).' '.$view['translator']->trans('view_job_client_page.job_detail.befor_minute', array(), 'vlance'); ?>
                    <?php else: ?>
                        <?php echo (int)($hours).' '.$view['translator']->trans('view_job_client_page.job_detail.hour', array(), 'vlance').', '. round(($hours- (int)($hours))*60).' '.$view['translator']->trans('view_job_client_page.job_detail.minute', array(), 'vlance') ?>
                    <?php endif; ?>
                <?php elseif ($today->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.now', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                <?php elseif($yesterday->format('d/m/Y') == $job_creat->format('d/m/Y') && $hours > 6) : ?>
                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.yesterday', array(), 'vlance').', '.$entity->getCreatedAt()->format('H:i') ?> 
                <?php else : ?>
                    <?php echo $entity->getCreatedAt()->format('d/m/Y, H:i'); ?>
                <?php endif; ?>
            </dd>

            <?php // Expired date ?>
            <?php if($remain == false || $entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_FINISHED || $entity->getStatus() == Vlance\JobBundle\Entity\Job::JOB_AWARDED) : ?>
                <dt></dt>
                <dd class="has_expired_quote">
                    <?php echo $view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance') ?>
                </dd>
            <?php else: ?>
                <dt><?php echo $view['translator']->trans('view_job_client_page.job_detail.remain', array(), 'vlance') ?></dt>
                <dd><?php echo $remain ?></dd>
            <?php endif; ?>

            <?php // City ?>
            <dt><?php echo $view['translator']->trans('view_job_client_page.job_detail.city', array(), 'vlance') ?></dt>
            <dd>
                <?php echo $entity->getCity()? $entity->getCity()->getName() : $view['translator']->trans('common.country', array(), 'vlance') ?>
            </dd>

            <?php // Budget ?>
            <?php /*
             * * An ngay bat dau cong viec
            <dt><?php echo $view['translator']->trans('view_job_client_page.job_detail.start_date', array(), 'vlance').':' ?></dt>
            <dd><?php echo $entity->getStartDate()->format('d/m/Y') ?></dd>*/?>
            <dt><?php echo $view['translator']->trans('view_job_client_page.job_detail.budget', array(), 'vlance') ?></dt>
            <dd>
                <?php echo $entity->getJobBudget($view['vlance']->isAuthen()); ?>
                <?php if(!$view['vlance']->isAuthen()): ?>
                <a href="#" class="vlance-popover icon-question-sign" data-trigger="hover" data-placement="bottom" data-html="true" data-content="Vui lòng <a href='#'>đăng nhập</a> để biết mức ngân sách thực tế."></a>
                <?php endif; ?>
            </dd>
            
            <?php //Hiện số tiền DC đã nạp ?>
            <?php if($entity->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW):?>
            <dt>Đã nạp</dt>
            <dd>
                <?php echo number_format($entity->getCash()->getBalance(),'0',',','.') ?> đ
            </dd>
            <?php endif; ?>
            
            <?php if($entity->getWorker()) : // Job has been awarded to freelancer?>
            <dt><?php echo $view['translator']->trans('list_working.content.status_view_job', array(), 'vlance') ?></dt>
            <dd>
                <?php if ($entity->getPaymentStatus() === Vlance\JobBundle\Entity\Job::WAITING_ESCROW) : ?>
                    <span class="label label-warning"><?php echo $view['translator']->trans('job.status_client.wait_fund_project', array(), 'vlance') ?></span>
                <?php elseif ($entity->getPaymentStatus() === Vlance\JobBundle\Entity\Job::ESCROWING) :?>
                    <span class="label label-warning"><?php echo $view['translator']->trans('job.status.payment_escrowing', array(), 'vlance') ?></span>
                <?php elseif ($entity->getPaymentStatus() === Vlance\JobBundle\Entity\Job::ESCROWED) :?>
                    <span class="label label-success"><?php echo $view['translator']->trans('job.status.payment_escrowed', array(), 'vlance') ?></span>
                <?php else:?>
                    <span class="label label-success"><?php echo $view['translator']->trans('job.status.payment_paied', array(), 'vlance') ?></span>
                <?php endif; ?>
                </span>
            </dd>
            <?php endif;?>
        </dl>
    </div>

    <?php /*if($entity->getStatus()!= Vlance\JobBundle\Entity\Job::JOB_AWARDED && !$acc_bid): ?>
        <div class="edit-job">
     * <a href="<?php echo $view['router']->generate('job_edit',array('id' => $entity->getId())) ?>" 
     * title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
     * <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
     * </a>
     * </div>
    <?php endif; */?>
    <?php if($entity->getWorker()) : // Job has been awarded to freelancer?>
        <div class="recharge">
            <div class="recipients">
                <?php echo '('.$view['translator']->trans('view_job_client_page.job_detail.assign', array(), 'vlance') ?> 
                <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $entity->getWorker()->getHash())) ?>" 
                   title="<?php echo $entity->getWorker()->getFullName() ?>">
                    <?php echo $entity->getWorker()->getFullName() ?></a>)
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
    <div class="row-fluid">
        <h2><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.title', array(), 'vlance') ?></h2>
        <div class="info-employment">
            <div class="info-employment-top row-fluid">
                <div class="span4">
                    <?php /* @var $acc \Vlance\AccountBundle\Entity\Account */ ?>
                    <?php $acc = $entity->getAccount(); ?>
                    <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                        <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '75x75', 75, 75, 1);?>
                        <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                        <?php else: ?>
                            <img width="75" height="75" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" />
                        <?php endif; ?>
                    </a>
                </div>
                <div class="span8">

                    <h3 class="title">
                        <a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" 
                            title="<?php echo $acc->getFullName() ?>">
                                <?php echo $acc->getFullName() ?>
                        </a>
                    </h3>
                    <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                    <div id="client-rating-box">
                        <?php 
                            $money_pay = $acc->getOutCome();
                            $info_client = '<div>Tiền đã thuê : <b>'.number_format($money_pay,0,',','.').'</b> VNĐ</div> '
                                    . '<div>Việc đã đăng : <b>'.$acc->getNumJob().'</b> việc </div>'
                                    . '<div>Việc đã giao : <b>'.$acc->getNumJobDeposit().'</b> việc</div>';
                            $ratio = 0;
                            if($money_pay > 0 && $money_pay <= 1000000) {
                                $ratio = 20;
                            } elseif($money_pay > 1000000 && $money_pay <= 5000000) {
                                $ratio = 40;
                            } elseif($money_pay > 5000000 && $money_pay <= 10000000) {
                                $ratio = 60;
                            } elseif($money_pay > 10000000 && $money_pay <= 50000000) {
                                $ratio = 80;
                            }elseif($money_pay > 50000000 ) {
                                $ratio = 100;
                            }
                        ?>
                        <div class="box-rating-client" data-toggle="popover" 
                            data-placement="right"  
                            data-content="<?php echo $info_client ?>"  
                            data-trigger="hover" data-html="true">
                            <div class="rating-box-client">
                                <div class="rating-client" style="width:<?php echo $ratio.'%'; ?>"></div>
                            </div>
                        </div> 
                        <script type="text/javascript">
                            $(document).ready(function() {
                                $('.box-rating-client').popover();
                            });
                        </script>
                    </div>        
                </div>
            </div>
            <div class="info-employment-bottom row-fluid">
                <dl class="dl-horizontal">
                    <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                    <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                    <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.createat', array(), 'vlance') ?></dt>
                    <dd><?php echo $acc->getCreatedAt()->format('d/m/Y') ?></dd>
                    <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.createnumber', array(), 'vlance') ?></dt>
                    <dd><a href="<?php echo $view['router']->generate('account_show_client',array('hash' => $acc->getHash())) ?>" 
                           title="<?php echo $acc->getNumJob()  ?>">
                               <?php echo $acc->getNumJob()  ?> việc
                        </a>
                    </dd>
                </dl>
            </div>
        </div>
    </div>
<?php endif; ?>