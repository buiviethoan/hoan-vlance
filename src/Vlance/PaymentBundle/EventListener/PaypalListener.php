<?php

namespace Vlance\PaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\UnitOfWork;
use Vlance\BaseBundle\Event\VlanceEvents;
use Vlance\PaymentBundle\Event\TransactionEvent;
use Vlance\PaymentBundle\Entity\PaymentMethods\Paypal;

class PaypalListener implements EventSubscriberInterface
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager
     */
    private $_em;
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $_container;
    
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
        $this->_container = $container;
        $this->_em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            VlanceEvents::TRANS_DEPOSIT_AFTER_CREATE => 'onCreateCompleted',
        );
    }

    public function onCreateCompleted(TransactionEvent $event)
    {
        /** @var $transaction \Vlance\PaymentBundle\Entity\Transaction */
        $transaction = $event->getTransaction();
        $paymentMethod = $transaction->getPaymentMethod();
        // If this transaction is use paypal payment method
        if ($paymentMethod->getCode() == Paypal::CODE) {
            $paypal = new Paypal($this->_container, $transaction);
            $event->setResponse(new RedirectResponse($paypal->getRedirectUrl()));
        }
    }
}
