<?php

namespace Vlance\PaymentBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Vlance\JobBundle\Entity\Job;
use Vlance\PaymentBundle\Entity\Transaction;

class TransactionListener
{
    protected $_container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }
    
    /**
     * This function is used to handle the transaction entity updated. Base on transaction type :
     * - Deposit: If status is terminated, call action to create escrowed transaction
     * 
     * @param \Doctrine\Common\Persistence\Event\LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        /* @var $entity \Vlance\PaymentBundle\Entity\Transaction */
        if ($entity instanceof Transaction) {
            // If this transaction is deposit & this transaction is terminated - when vlance receipt the money from client
            if ($entity->getType() === Transaction::TRANS_TYPE_DEPOSIT && $entity->getStatus() === Transaction::TRANS_TERMINATED) {
                if($entity->getJob()->getTestType() != Job::TEST_TYPE_UPFRONT_ESCROW){
                    /**
                     * Update user cash
                     */
                    $em = $args->getEntityManager();
                    $user_cash = $entity->getReceiver()->getCash();
                    // REMOVE update processing_balance usercash
                    // WHEN Admin confirm that received client's deposit for project
                    // $user_cash->setProcessingBalance($user_cash->getProcessingBalance() - $entity->getAmount());
                    $user_cash->setBalance($user_cash->getBalance() + $entity->getAmount());
                    $em->persist($user_cash);
                    $em->flush();
                    $job_id = $entity->getJob()->getId();
                    $this->_container->get('vlance.payment.controller.transaction')->setContainer($this->_container);
                    $this->_container->get('vlance.payment.controller.transaction')->escrowAction($job_id);
                }
            }
            
            // If this transaction is withdraw & vlance is sent money to freelancer
            elseif ($entity->getType() === Transaction::TRANS_TYPE_WITHDRAW && $entity->getStatus() === Transaction::TRANS_TERMINATED) {
                $this->_container->get('vlance.payment.controller.transaction')->setContainer($this->_container);
                $this->_container->get('vlance.payment.controller.transaction')->confirmWithdraw($entity);
            }
        }
    }
}
