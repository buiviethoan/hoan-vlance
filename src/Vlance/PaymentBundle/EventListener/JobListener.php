<?php

namespace Vlance\PaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;

use Vlance\PaymentBundle\Entity\JobCash;
use Vlance\JobBundle\Event\JobEvent;

class JobListener implements EventSubscriberInterface
{
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            JobEvent::JOB_CREATE_COMPLETED => 'onCreateCompleted',
        );
    }

    public function onCreateCompleted(JobEvent $event)
    {
        $job = $event->getJob();
        $jobCast = $job->getCash();
        if (is_null($jobCast)) {
            $jobCash = new JobCash();
            $jobCash->setBalance(0);
            $jobCash->setJob($job);
            $this->em->persist($jobCash);
            $this->em->flush();
        }
    }
}
