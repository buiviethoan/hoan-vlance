<?php

namespace Vlance\PaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\EntityManager;
use Vlance\BaseBundle\Event\VlanceEvents;
use Vlance\PaymentBundle\Event\TransactionEvent;
use Vlance\PaymentBundle\Entity\PaymentMethods\Vlance;

class VlanceListener implements EventSubscriberInterface
{
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $_container;
    
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            VlanceEvents::TRANS_DEPOSIT_BEFORE_CREATE => 'onBeforeCreate',
        );
    }
    
    public function onBeforeCreate(TransactionEvent $event) {
        /** @var $transaction \Vlance\PaymentBundle\Entity\Transaction */
        $transaction = $event->getTransaction();
        $paymentMethod = $transaction->getPaymentMethod();
        // If this transaction is use paypal payment method
        if ($paymentMethod->getCode() == Vlance::CODE) {
            $event->setResponse(new RedirectResponse($this->_container->get('router')->generate('transaction_escrow', array('job_id' => $transaction->getJob()->getId()))));
        }
    }
}
