<?php

namespace Vlance\PaymentBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use Vlance\PaymentBundle\Entity\UserCash;
use Vlance\PaymentBundle\Entity\CreditAccount as CreditAccount;
use Doctrine\ORM\UnitOfWork;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vlance\PaymentBundle\Entity\CreditBuyTransaction;
use Vlance\AccountBundle\Entity\Account as Account;
use Symfony\Component\HttpFoundation\JsonResponse;



class AccountRegistrationListener implements EventSubscriberInterface
{
    private $em;
    private $container;
    
    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        );
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();
        
        $userCash = $user->getCash();
        if (is_null($userCash)) {
            $userCash = new UserCash();
            $userCash->setAccount($user);
            $userCash->setBalance(0);
            $this->em->persist($userCash);
            $this->em->flush($userCash);
        }
        
        /* var $creditAccount CreditAccount */
        $creditAccount = $user->getCredit();
        if(is_null($creditAccount)) {
            $creditAccount = new CreditAccount();
            $creditAccount->setAccount($user);
            $creditAccount->setBalance(0);
            $this->em->persist($creditAccount);
            $this->em->flush();
        }
        
        /**
         * Invitee promo credit
         * Reason: many people abuse this function.
         */
        
//        $invitee = $this->em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id'=>$user->getID()));
        
        /* Check if user was invited before */
        $mail = $this->em->getRepository('VlanceAccountBundle:EmailInvite')->findOneByEmail($user->getEmail());
        if(is_object($mail)){
//            /* add promo credit */
//            /* @var $credit_account \Vlance\PaymentBundle\Entity\CreditAccount */
//            $credit_account = $this->em->getRepository('VlancePaymentBundle:CreditAccount')->findOneBy(array('id' => $invitee->getCredit()->getId()));
//            if(!$credit_account->getId()){
//                return new JsonResponse(array('error' => 102));
//            }
//            
//            $detail = array(
//                'invitor_id'    => $mail->getAccount()->getId(),
//                'invitee_id'    => $invitee->getId(),
//                'case'          => 'Invitee registered.',
//                'qty'           => '1',
//            );
            
//            $promo_transaction = new CreditBuyTransaction();
//            $promo_transaction->setPayer($invitee);
//            $promo_transaction->setCreditAccount($credit_account);
//            $promo_transaction->setType(CreditBuyTransaction::TYPE_PROMO_REFERRAL);
//            $promo_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_OTHER);
//            $promo_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
//            $promo_transaction->setAmount(0); // amount = 0 because this is promotion
//            $promo_transaction->setCredit(1);
//            $promo_transaction->setPreBalance(0);
//            $promo_transaction->setPostBalance(1);
//            $promo_transaction->setDetail(json_encode($detail));
//            $this->em->persist($promo_transaction);

//            $credit_account->setBalance($promo_transaction->getPostBalance());
//            $this->em->persist($credit_account);
//
//            $this->em->flush(); 
//            
//            /* sent mail */
//            $from_vlance = $this->container->getParameter('from_email');
//            
//            $context_invitee = array(
//                'invitee'   => $invitee,
//            );
//            $tpl_invitee = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitee.html.twig';
            
            // Send tracking event to Mixpanel
            $this->container->get('vlance_base.helper')->trackAll(NULL, "Registration by referral");
            // End Mixpanel
            
//            try{
//                // Send confirmation mail to payer
//                $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitee, $context_invitee, $from_vlance, $invitee->getEmail());
//            } catch (Exception $ex) {
//                return new JsonResponse(array('error' => 104));
//            }
        } else {
            if(isset($_COOKIE['referrer_invite'])){
//                /* @var $credit_account \Vlance\PaymentBundle\Entity\CreditAccount */
//                $credit_account = $this->em->getRepository('VlancePaymentBundle:CreditAccount')->findOneBy(array('id' => $invitee->getCredit()->getId()));
//                if(!$credit_account->getId()){
//                    return new JsonResponse(array('error' => 106));
//                }

//                $detail = array(
//                    'invitor_id'    => $_COOKIE['referrer_invite'],
//                    'invitee_id'    => $invitee->getId(),
//                    'case'          => 'Registration on facebook.',
//                    'url'           => 'facebook',
//                    'qty'           => '1',
//                );
                
//                $promo_transaction = new CreditBuyTransaction();
//                $promo_transaction->setPayer($invitee);
//                $promo_transaction->setCreditAccount($credit_account);
//                $promo_transaction->setType(CreditBuyTransaction::TYPE_PROMO_REFERRAL);
//                $promo_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_OTHER);
//                $promo_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
//                $promo_transaction->setAmount(0); // amount = 0 because this is promotion
//                $promo_transaction->setCredit(1);
//                $promo_transaction->setPreBalance(0);
//                $promo_transaction->setPostBalance(1);
//                $promo_transaction->setDetail(json_encode($detail));
//                $this->em->persist($promo_transaction);

//                $credit_account->setBalance($promo_transaction->getPostBalance());
//                $this->em->persist($credit_account);

//                $this->em->flush(); 

                 /* sent mail */
//                $from_vlance = $this->container->getParameter('from_email');
//            
//                $context_invitee = array(
//                    'invitee'   => $invitee,
//                );
//                $tpl_invitee = 'VlanceAccountBundle:Email/AccountInvite:offer_credit_invitee.html.twig';
                
                // Send tracking event to Mixpanel
                $this->container->get('vlance_base.helper')->trackAll(NULL, "Registration by referral");
                // End Mixpanel
                    
//                try{
//                    // Send confirmation mail to payer
//                    $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_invitee, $context_invitee, $from_vlance, $invitee->getEmail());
//                } catch (Exception $ex) {
//                    return new JsonResponse(array('error' => 108));
//                }
            }
        }
        
        /* END Check if user was invited before */
    }
}
