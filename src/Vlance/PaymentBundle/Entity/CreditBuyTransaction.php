<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Credit Buy Transaction,
 * When user by credits, transaction will be stored here
 *
 * @ORM\Table(name="credit_buy_transaction")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\CreditBuyTransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CreditBuyTransaction
{
    const STATUS_WAITING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_CANCELLED = 3;
    const STATUS_TERMINATED = 4;
    
    const TYPE_BUY = 1;
    const TYPE_PROMO = 2;
    const TYPE_REFUND = 3;
    const TYPE_PROMO_REFERRAL = 4;
    
    const PAYMENT_METHOD_ONEPAY_OTP     = 10;
    const PAYMENT_METHOD_ONEPAY_BANK    = 11;
    const PAYMENT_METHOD_ONEPAY_VISA    = 12;
    const PAYMENT_METHOD_BANK_TRANSFER  = 20;
    const PAYMENT_METHOD_PAYPAL         = 30;
    const PAYMENT_METHOD_CASH           = 40;
    const PAYMENT_METHOD_USERCASH       = 50;
    const PAYMENT_METHOD_OTHER          = 60;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $payer;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\PaymentBundle\Entity\CreditAccount")
     * @ORM\JoinColumn(name="credit_account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creditAccount;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_method", type="smallint")
     */
    private $paymentMethod;

    /**
     * Number of credits
     * @var integer
     *
     * @ORM\Column(name="credit", type="integer")
     */
    private $credit;

    /**
     * Amount of money
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="pre_balance", type="integer")
     */
    private $preBalance;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="post_balance", type="integer")
     */
    private $postBalance;
    
    /**
     * Amount of Discount
     * @var integer
     *
     * @ORM\Column(name="discount", type="integer")
     */
    private $discount = 0;

    /**
     * Information is stored as an array in JSON format
     * @var string
     *
     * @ORM\Column(name="detail", type="text", nullable=true)
     */
    private $detail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_response", type="text", nullable=true)
     */
    private $paymentResponse = "";

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return CreditOrderTransaction
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CreditOrderTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     * @return CreditOrderTransaction
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    
        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set credit
     *
     * @param integer $credit
     * @return CreditOrderTransaction
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
    
        return $this;
    }

    /**
     * Get credit
     *
     * @return integer 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return CreditOrderTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return CreditOrderTransaction
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return CreditOrderTransaction
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    
        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return CreditOrderTransaction
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CreditOrderTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CreditOrderTransaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set payer
     *
     * @param \Vlance\AccountBundle\Entity\Account $payer
     * @return CreditOrderTransaction
     */
    public function setPayer(\Vlance\AccountBundle\Entity\Account $payer = null)
    {
        $this->payer = $payer;
    
        return $this;
    }

    /**
     * Get payer
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set creditAccount
     *
     * @param \Vlance\PaymentBundle\Entity\CreditAccount $creditAccount
     * @return CreditOrderTransaction
     */
    public function setCreditAccount(\Vlance\PaymentBundle\Entity\CreditAccount $creditAccount = null)
    {
        $this->creditAccount = $creditAccount;
    
        return $this;
    }

    /**
     * Get creditAccount
     *
     * @return \Vlance\PaymentBundle\Entity\CreditAccount 
     */
    public function getCreditAccount()
    {
        return $this->creditAccount;
    }

    /**
     * Set preBalance
     *
     * @param integer $preBalance
     * @return CreditBuyTransaction
     */
    public function setPreBalance($preBalance)
    {
        $this->preBalance = $preBalance;
    
        return $this;
    }

    /**
     * Get preBalance
     *
     * @return integer 
     */
    public function getPreBalance()
    {
        return $this->preBalance;
    }

    /**
     * Set postBalance
     *
     * @param integer $postBalance
     * @return CreditBuyTransaction
     */
    public function setPostBalance($postBalance)
    {
        $this->postBalance = $postBalance;
    
        return $this;
    }

    /**
     * Get postBalance
     *
     * @return integer 
     */
    public function getPostBalance()
    {
        return $this->postBalance;
    }

    /**
     * Set paymentResponse
     *
     * @param string $paymentResponse
     * @return CreditBuyTransaction
     */
    public function setPaymentResponse($paymentResponse)
    {
        $this->paymentResponse = $paymentResponse;
    
        return $this;
    }

    /**
     * Get paymentResponse
     *
     * @return string 
     */
    public function getPaymentResponse()
    {
        return $this->paymentResponse;
    }
    
    public function addPaymentResponse($paymentResponse)
    {
        $res_text = $this->getPaymentResponse();
        if($res_text == ""){
            $this->paymentResponse = json_encode(array($paymentResponse));
        } else {
            $res_json = json_decode($res_text, true);       // decode to array, instead of object
            array_push($res_json, $paymentResponse);        // Add new response to the array
            $this->paymentResponse = json_encode($res_json);// Save the responses
        }
        return $this;
    }
}