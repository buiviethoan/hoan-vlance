<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * VlanceCash
 *
 * @ORM\Table(name="vlance_cash")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\VlanceCashRepository")
 */
class VlanceCash
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="balance", type="integer", options={"default" : "0"})
     */
    private $balance;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\PaymentBundle\Entity\Transaction")
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    private $transaction;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return VlanceCash
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    
        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $createdAt
     * @return VlanceCash
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set transaction
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transaction
     * @return VlanceCash
     */
    public function setTransaction(\Vlance\PaymentBundle\Entity\Transaction $transaction = null)
    {
        $this->transaction = $transaction;
    
        return $this;
    }

    /**
     * Get transaction
     *
     * @return \Vlance\PaymentBundle\Entity\Transaction 
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}