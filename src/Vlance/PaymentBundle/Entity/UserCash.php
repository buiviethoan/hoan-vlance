<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * UserCash
 *
 * @ORM\Table(name="user_cash")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\UserCashRepository")
 */
class UserCash
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="balance", type="integer", options={"default" : "0"})
     */
    private $balance;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="processing_balance", type="integer", options={"default" : "0"})
     */
    private $processingBalance = 0;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="cash")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $account;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return UserCash
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    
        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getActualBalance()
    {
        return $this->balance + $this->processingBalance;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return UserCash
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return UserCash
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return UserCash
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set processingBalance
     *
     * @param integer $processingBalance
     * @return UserCash
     */
    public function setProcessingBalance($processingBalance)
    {
        $this->processingBalance = $processingBalance;
    
        return $this;
    }

    /**
     * Get processingBalance
     *
     * @return integer 
     */
    public function getProcessingBalance()
    {
        return $this->processingBalance;
    }
}