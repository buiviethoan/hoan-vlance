<?php
namespace Vlance\PaymentBundle\Entity\PaymentMethods;

class Vlance extends PaymentMethodAbstract {
    // these constain need to override for each payment method
    const CODE = 'vlance1.0';
    const NAME = 'Vlance';
    const INFO = 'payment.method.vlance.info';
    const ISREDIRECT = false;
    
    public function getRedirectUrl() {
        
    }

    public function handleAccepted() {
        
    }

    public function handleRefused() {
        
    }
}
