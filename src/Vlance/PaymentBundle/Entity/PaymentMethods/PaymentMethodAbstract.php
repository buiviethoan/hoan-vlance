<?php
namespace Vlance\PaymentBundle\Entity\PaymentMethods;

use Vlance\PaymentBundle\Entity\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class PaymentMethodAbstract implements PaymentMethodInterface {
    
    const CODE = 'abstract';
    const NAME = 'Abstract Method';
    const INFO = 'No information for abstract class';
    const ISREDIRECT = false;
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $_container;
    
    /**
     *
     * @var \Vlance\PaymentBundle\Entity\Transaction 
     */
    protected $_transaction;
    
    public function __construct(ContainerInterface $container, Transaction $transaction) {
        $this->_container = $container;
        $this->_transaction = $transaction;
    }
    
    public function setContainer(ContainerInterface $container) {
        $this->_container = $container;
    }
    
    public function setTransaction(Transaction $transaction) {
        $this->_transaction = $transaction;
    }
}
