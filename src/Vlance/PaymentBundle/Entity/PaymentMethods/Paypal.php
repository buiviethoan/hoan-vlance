<?php
namespace Vlance\PaymentBundle\Entity\PaymentMethods;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Exception\PPConnectionException;

use Vlance\PaymentBundle\Entity\PaypalTransaction;

class Paypal extends PaymentMethodAbstract {
    // these constain need to override for each payment method
    const CODE = 'paypal1.0';
    const NAME = 'Paypal';
    const INFO = 'payment.method.paypal.info';
    const ISREDIRECT = true;
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $_container;
    
    /**
     *
     * @var \Vlance\PaymentBundle\Entity\Transaction 
     */
    protected $_transaction;
    
    /**
     *
     * @var \Vlance\PaymentBundle\Entity\PaypalTransaction 
     */
    protected $_paypalTransaction;
    
    // these variables are used for paypal
    /** @todo Need to store in database */
//    protected $_clientID = "AXsgKBBIcuDHOnQx7Js9ngEVeAjmkC18YNsh_dKQG3iiTE61meio-sjrEC7T";
//    protected $_secret = "EMMP5hD0iIakmCgI-4811B5n7f06GtzKpK1EEJoti4ZLCOYbSo3-h1QQAz4d";
//    protected $_endpoint = "api.sandbox.paypal.com";
//    protected $_mode = "sandbox";
    
    /**
     * These variables are used on production server
     */
    protected $_clientID = "AQIMEhB5YQ9NPHZHjQyBg-cMKDpOCC1iCWX-GFmEKmP-Xgo4dx9vPgvxcz-x";
    protected $_secret = "ED8MvxA-0y_wTsuXQX-TRQcZtQ4ALypDZYBYMAOgPa692PqAO3ou9IPi4LkX";
    protected $_endpoint = "api.paypal.com";
    protected $_mode = "live";
    
    public function getRedirectUrl() {
        if (!self::ISREDIRECT) {
            return "";
        }
        $sdkConfig = array(
            "mode" => $this->_mode,
        );
        //Get access token & access type
        $cred = new OAuthTokenCredential($this->_clientID,$this->_secret);
        $apiContext = new ApiContext($cred, 'Request' . time());
        $apiContext->setConfig($sdkConfig);

        $payer = new Payer();
        $payer->setPayment_method("paypal");

        // Convert VND -> USD
        $amountUSD = $this->getUSD($this->_transaction->getAmount());
        
        $item = new Item();
        $item->setName($this->_container->get('translator')->trans('transaction.paypal.description', array("%%job_title%%" => $this->_transaction->getJob()->getTitle()), 'vlance'));
        $item->setPrice($amountUSD);
        $item->setQuantity(1);
        $item->setSku('job' . $this->_transaction->getJob()->getId());
        $item->setCurrency("USD");
        
        $items = new ItemList();
        $items->setItems(array($item));
        
        $amount = new Amount();
        $amount->setCurrency("USD");
        $amount->setTotal($amountUSD);
//        $amount->setDetails('Test here');
//        $amount->setTotal(number_format($amountUSD, 2));

        
        $transaction = new Transaction();
        $transaction->setDescription($this->_container->get('translator')->trans('transaction.paypal.description', array("%%job_title%%" => $this->_transaction->getJob()->getTitle()), 'vlance'));
        $transaction->setAmount($amount);
        $transaction->setItemList($items);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturn_url($this->_container->get('router')->generate('transaction_paypal_approval', array(), true));
        $redirectUrls->setCancel_url($this->_container->get('router')->generate('transaction_paypal_cancel', array(), true));

        $payment = new Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setRedirect_urls($redirectUrls);
        $payment->setTransactions(array($transaction));
        
        try {
            $payment->create($apiContext);
        } catch (PPConnectionException $ex) {
            /* @var $logger \Psr\Log\LoggerInterface */
            $logger = $this->_container->get("monolog.logger.paypal");
            $logger->info("Create paypal payment error");
            $logger->info($ex->getMessage());
            print_r($ex->getData());
            die('create payment transaction error');
            return "";
        }
        $links = $payment->getLinks();
        /**
         * Store paypal payment into database
         */
        
        $urlTmp = parse_url($links[1]->getHref());
        $varTmp = array();
        parse_str($urlTmp['query'], $varTmp);
        $paypalTransaction = new PaypalTransaction();
        $paypalTransaction->setAmount($amount->getTotal());
        $paypalTransaction->setPaypalId($payment->getId());
        $paypalTransaction->setToken($varTmp['token']);
        $paypalTransaction->setTransaction($this->_transaction);
        
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->_container->get('doctrine')->getManager();
        $em->persist($paypalTransaction);
        $em->flush();
        
        
        return $links[1]->getHref();
    }

    public function handleAccepted() {
        if (!$this->_paypalTransaction) {
            return false;
        }
        $sdkConfig = array(
            "mode" => $this->_mode,
        );
        //Get access token & access type
        $cred = new OAuthTokenCredential($this->_clientID,$this->_secret);
        $apiContext = new ApiContext($cred, 'Request' . time());
        $apiContext->setConfig($sdkConfig);
        
        $payment = new Payment();
        $payment->setId($this->_paypalTransaction->getPaypalId());
        $execution = new PaymentExecution();
        $execution->setPayer_id($this->_paypalTransaction->getPayerId());
        try {
            $payment->execute($execution, $apiContext);
        } catch (PPConnectionException $ex) {
            /* @var $logger \Psr\Log\LoggerInterface */
            $logger = $this->_container->get("monolog.logger.paypal");
            $logger->info("Handle accept error");
            $logger->info($ex->getMessage());
            return FALSE;
        }
        
        $transaction = $this->_paypalTransaction->getTransaction();
        $transaction->setStatus(\Vlance\PaymentBundle\Entity\Transaction::TRANS_TERMINATED);
        
        $this->_paypalTransaction->setStatus(PaypalTransaction::PAYPAL_VERIFIED);
        
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->_container->get('doctrine')->getManager();
        $em->persist($this->_paypalTransaction);
        $em->persist($transaction);
        $em->flush();
        return true;
    }

    public function handleRefused() {
        
    }
    
    public function setPaypalTransaction (PaypalTransaction $paypalTransaction) {
        $this->_paypalTransaction = $paypalTransaction;
    }

    protected function getUSD($vnd) {
        $helper = $this->_container->get('vlance_base.helper');
        $currencyRate = $helper->getParameter('vlance_system.currency_rate');
        $paypalFee = $helper->getParameter('vlance_system.paypal_fee');
        $withdrawFee = $helper->getParameter('vlance_system.paypal_withdraw_fee');
        $usd = $vnd / $currencyRate;
        return number_format($usd, 2);
    }
    
    /**
     * Get exchange rate from USD -> VND
     * @todo Get this exchange rate automatically from database
     */
    protected function getCurrencyRate() {
        $vlance_parameter = $this->_container->getParameter('vlance_system');
        return $vlance_parameter['currency_rate'];
    }
    
    protected function getPaypalFee() {
        $vlance_parameter = $this->_container->getParameter('vlance_system');
        return $vlance_parameter['paypal_fee'];
    }
}
