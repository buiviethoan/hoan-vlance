<?php
namespace Vlance\PaymentBundle\Entity\PaymentMethods;

interface PaymentMethodInterface {
    
    public function getRedirectUrl();
    
    public function handleAccepted();
    
    public function handleRefused();
}
