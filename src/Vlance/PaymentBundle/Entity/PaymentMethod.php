<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * PaymentMethod
 *
 * @ORM\Table(name="payment_method")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\PaymentMethodRepository")
 */
class PaymentMethod
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text", nullable=true)
     */
    protected $info = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="obj_class", type="string", nullable=true)
     */
    private $objClass = null;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\PaymentBundle\Entity\Transaction", mappedBy="paymentMethod")
     */
    protected $transactions;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set code
     *
     * @param string $code
     * @return PaymentMethod
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PaymentMethod
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set info
     *
     * @param string $info
     * @return PaymentMethod
     */
    public function setInfo($info)
    {
        $this->info = $info;
    
        return $this;
    }

    /**
     * Get info
     *
     * @return string 
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set objClass
     *
     * @param string $objClass
     * @return PaymentMethod
     */
    public function setObjClass($objClass)
    {
        $this->objClass = $objClass;
    
        return $this;
    }

    /**
     * Get objClass
     *
     * @return string 
     */
    public function getObjClass()
    {
        return $this->objClass;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add transactions
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactions
     * @return PaymentMethod
     */
    public function addTransaction(\Vlance\PaymentBundle\Entity\Transaction $transactions)
    {
        $this->transactions[] = $transactions;
    
        return $this;
    }

    /**
     * Remove transactions
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transactions
     */
    public function removeTransaction(\Vlance\PaymentBundle\Entity\Transaction $transactions)
    {
        $this->transactions->removeElement($transactions);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}