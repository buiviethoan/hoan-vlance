<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Credit Expense Transaction,
 * When user spends credit, transaction will be stored here
 *
 * @ORM\Table(name="credit_expense_transaction")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\CreditExpenseTransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CreditExpenseTransaction
{
    const STATUS_WAITING = 1;
    const STATUS_PROCESSING = 2;
    const STATUS_CANCELLED = 3;
    const STATUS_TERMINATED = 4;

    const TYPE_EXPENSE = 1;
    const TYPE_PROMO = 2;
    const TYPE_REFUND = 3;
    
    const SERVICE_CONTACT_FREELANCER                    = 101;
    const SERVICE_CONTACT_CLIENT                        = 102;
    const SERVICE_FEATURE_JOB                           = 103;
    const SERVICE_FEATURE_BID                           = 104;
    //const SERVICE_VERIFY_TEL                          = 105;
    const SERVICE_FEATURE_FREELANCER                    = 106;
    //const SERVICE_BUY_CREDIT                          = 107;
    const SERVICE_MAKE_BID                              = 108;
    const SERVICE_RESPOND_INTERVIEW                     = 109;
    const SERVICE_FEATURE_JOB_FANPAGE                   = 110;
    const SERVICE_JOB_ONSITE_POST                       = 120;
    const SERVICE_JOB_ONSITE_OPTION_FEATURE_3DAYS       = 121;
    const SERVICE_JOB_ONSITE_OPTION_EMAIL               = 122;
    const SERVICE_JOB_ONSITE_OPTION_BANNER              = 123;
    const SERVICE_JOB_ONSITE_OPTION_EXTEND_10           = 124;
    const SERVICE_JOB_ONSITE_OPTION_EXTEND_30           = 125;
    const SERVICE_BID_INTERVIEW                         = 130;
    const SERVICE_VERIFY_ID                             = 140;
    const SERVICE_ADMIN_POST_JOB                        = 141;
    const SERVICE_BUY_DISPLAY_BID			= 150;
    const SERVICE_BUY_APPLY_BID				= 151;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="service", type="integer", nullable=true)
     */
    private $service;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\PaymentBundle\Entity\CreditAccount")
     * @ORM\JoinColumn(name="credit_account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $creditAccount;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="pre_balance", type="integer")
     */
    private $preBalance;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="post_balance", type="integer")
     */
    private $postBalance;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;
    
    /**
     * Information is stored as an array in JSON format, with at least these fields:
     * 
     * @var string
     *
     * @ORM\Column(name="detail", type="text", nullable=true)
     */
    private $detail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return CreditExpenseTransaction
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set preBalance
     *
     * @param integer $preBalance
     * @return CreditExpenseTransaction
     */
    public function setPreBalance($preBalance)
    {
        $this->preBalance = $preBalance;
    
        return $this;
    }

    /**
     * Get preBalance
     *
     * @return integer 
     */
    public function getPreBalance()
    {
        return $this->preBalance;
    }

    /**
     * Set postBalance
     *
     * @param integer $postBalance
     * @return CreditExpenseTransaction
     */
    public function setPostBalance($postBalance)
    {
        $this->postBalance = $postBalance;
    
        return $this;
    }

    /**
     * Get postBalance
     *
     * @return integer 
     */
    public function getPostBalance()
    {
        return $this->postBalance;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return CreditExpenseTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return CreditExpenseTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return CreditExpenseTransaction
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    
        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return CreditExpenseTransaction
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CreditExpenseTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CreditExpenseTransaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set creditAccount
     *
     * @param \Vlance\PaymentBundle\Entity\CreditAccount $creditAccount
     * @return CreditExpenseTransaction
     */
    public function setCreditAccount(\Vlance\PaymentBundle\Entity\CreditAccount $creditAccount = null)
    {
        $this->creditAccount = $creditAccount;
    
        return $this;
    }

    /**
     * Get creditAccount
     *
     * @return \Vlance\PaymentBundle\Entity\CreditAccount 
     */
    public function getCreditAccount()
    {
        return $this->creditAccount;
    }

    /**
     * Set service
     *
     * @param integer $service
     * @return CreditExpenseTransaction
     */
    public function setService($service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return integer 
     */
    public function getService()
    {
        return $this->service;
    }
}