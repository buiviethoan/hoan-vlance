<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Paypal Transaction
 *
 * @ORM\Table(name="transaction_paypal")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\PaypalTransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PaypalTransaction
{
    const PAYPAL_NEW = 1;
    const PAYPAL_VERIFIED = 2;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="paypal_id", type="string")
     */
    private $paypalId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string")
     */
    private $token;
    
    /**
     * @var string
     *
     * @ORM\Column(name="payer_id", type="string", nullable=true)
     */
    private $payerId = null;
    
    /**
     * @var double
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\PaymentBundle\Entity\Transaction", cascade={"persist"})
     * @ORM\JoinColumn(name="transaction_id", referencedColumnName="id")
     */
    protected $transaction;
    
    public function __construct() {
        $this->setStatus(self::PAYPAL_NEW);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payId
     *
     * @param string $paypalId
     * @return PaypalTransaction
     */
    public function setPaypalId($paypalId)
    {
        $this->paypalId = $paypalId;
    
        return $this;
    }

    /**
     * Get paypalId
     *
     * @return string 
     */
    public function getPaypalId()
    {
        return $this->paypalId;
    }
    
    /**
     * Set token
     *
     * @param string $token
     * @return PaypalTransaction
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }
    
    /**
     * Set payerId
     *
     * @param string $payerId
     * @return PaypalTransaction
     */
    public function setPayerId($payerId = null)
    {
        $this->payerId = $payerId;
    
        return $this;
    }

    /**
     * Get payerId
     *
     * @return string 
     */
    public function getPayerId()
    {
        return $this->payerId;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return PaypalTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PaypalTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PaypalTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PaypalTransaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set transaction
     *
     * @param \Vlance\PaymentBundle\Entity\Transaction $transaction
     * @return PaypalTransaction
     */
    public function setTransaction(\Vlance\PaymentBundle\Entity\Transaction $transaction = null)
    {
        $this->transaction = $transaction;
    
        return $this;
    }

    /**
     * Get transaction
     *
     * @return \Vlance\PaymentBundle\Entity\Transaction 
     */
    public function getTransaction()
    {
        return $this->transaction;
    }
}