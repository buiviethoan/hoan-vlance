<?php

namespace Vlance\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vlance\BaseBundle\Utils\JobCalculator;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\TransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Transaction
{
    const TRANS_WAITING_PAYMENT = 1;
    const TRANS_PAIED           = 2;
    const TRANS_PROCESSING      = 3;
    const TRANS_TERMINATED      = 4;
    const TRANS_STATUS_CANCEL   = 5;
    
    const TRANS_TYPE_DEPOSIT            = 1;           // [7]: $sender (sys), $receiver, $job, $amount, $status, $comment, $paymentMethod
    const TRANS_TYPE_DEPOSIT_UPFRONT    = 11;  // [5]:                $receiver, $job, $amount, $status, $comment
    const TRANS_TYPE_ESCROW             = 2;            // [5]: $sender,                  $job, $amount, $status
    const TRANS_TYPE_FEE                = 3;
    const TRANS_TYPE_PAYMENT            = 4;
    const TRANS_TYPE_WITHDRAW           = 5;
    const TRANS_TYPE_REFUND             = 6;            // [6]: $job, $receiver, $sender (sys), $amount, $status, $comment
    const TRANS_TYPE_BUY_CREDIT         = 7;
    const TRANS_TYPE_TOPUP_USERCASH     = 20;
    const TRANS_TYPE_UPGRADE_VIP        = 30;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="consolidated_at", type="datetime", nullable=true)
     */
    private $consolidatedAt;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $sender;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $receiver;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="transactions")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $job;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\PaymentBundle\Entity\PaymentMethod", inversedBy="transactions")
     * @ORM\JoinColumn(name="method_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $paymentMethod;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="editor_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $editor;
    
    /**
     * @Assert\File(maxSize = "8192k")
     * @Vich\UploadableField(mapping="transaction_file", fileNameProperty="file_path")
     * @var File $file
     */
    private $file;
    
    /**
     * @ORM\Column(type="string", length=255, name="file_name", nullable=true)
     *
     * @var string $fileName
     */
    protected $fileName;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="file_path", nullable=true)
     * @var string $filePath
     */
    protected $filePath;
    
    protected $sendEmail;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Transaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Transaction
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Transaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set sender
     *
     * @param \Vlance\AccountBundle\Entity\Account $sender
     * @return Transaction
     */
    public function setSender(\Vlance\AccountBundle\Entity\Account $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \Vlance\AccountBundle\Entity\Account $receiver
     * @return Transaction
     */
    public function setReceiver(\Vlance\AccountBundle\Entity\Account $receiver = null)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return Transaction
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Transaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set paymentMethod
     *
     * @param \Vlance\PaymentBundle\Entity\PaymentMethod $paymentMethod
     * @return Transaction
     */
    public function setPaymentMethod(\Vlance\PaymentBundle\Entity\PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;
    
        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \Vlance\PaymentBundle\Entity\PaymentMethod 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set editor
     *
     * @param \Vlance\AccountBundle\Entity\Account $editor
     * @return Transaction
     */
    public function setEditor(\Vlance\AccountBundle\Entity\Account $editor = null)
    {
        $this->editor = $editor;
    
        return $this;
    }
    
    /**
     * Set sendEmail
     *
     * @param boolean $sendEmail
     * @return Transaction
     */
    public function setSendEmail($sendEmail)
    {
        $this->sendEmail = $sendEmail;
    
        return $this;
    }

    /**
     * Get sendEmail
     *
     * @return boolean 
     */
    public function getSendEmail()
    {
        return $this->sendEmail;
    }

    /**
     * Get editor
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getEditor()
    {
        return $this->editor;
    }
    
    /**
     * 
     * @return string
     */
    public function getAbsolutePath() {
         return null === $this->filePath
            ? null
            : UPLOAD_DIR . DS ."transaction". DS . $this->filePath;
    }
    
    /**
     * 
     * @return string
     */
    public function getWebPath() {
        return null === $this->filePath
            ? null
            : UPLOAD_URL . DS . "transaction". DS . $this->filePath;
    }
    
     /**
     * Sets file.
     * @param UploadedFile $file
     * @return Transaction
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR . DS . "transaction";
    }
    
   /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL . DS . "transaction";
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if($this->getFile() !== null) {
            $ext_name  = $this->getFile()->getClientOriginalExtension();
            $base_name = basename($this->getFile()->getClientOriginalName(), "." . $ext_name);
            $this->filePath = JobCalculator::url_slug($base_name, array('delimiter' => '_')) . "_" . time() . '.' . $ext_name;
            $this->fileName = $this->getFile()->getClientOriginalName();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        if (!file_exists($this->getUploadDir())) {
            mkdir($this->getUploadDir(), 0777, true);
        }       
        $this->getFile()->move($this->getUploadDir(), $this->filePath);
        $this->file = null;
    }

   /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }
    
     /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }
    
    /**
     * Set filePath
     * @param string $filePath
     * 
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
        return $this;
    }

    /**
     * Get filePath
     * @return string 
     * 
     */
    public function getFilePath()
    {
        return $this->filePath;
    }
    
    public function getTypeText() {
        $typeText = array(
            self::TRANS_TYPE_DEPOSIT        => 'transaction.type.deposit',
            self::TRANS_TYPE_DEPOSIT_UPFRONT=> 'transaction.type.depositupfront',
            self::TRANS_TYPE_ESCROW         => 'transaction.type.escrow',
            self::TRANS_TYPE_FEE            => 'transaction.type.fee',
            self::TRANS_TYPE_PAYMENT        => 'transaction.type.payment',
            self::TRANS_TYPE_WITHDRAW       => 'transaction.type.withdraw',
            self::TRANS_TYPE_REFUND         => 'transaction.type.refund',
            self::TRANS_TYPE_BUY_CREDIT     => 'transaction.type.buycredit',
            self::TRANS_TYPE_TOPUP_USERCASH => 'transaction.type.topupusercash',
            self::TRANS_TYPE_UPGRADE_VIP    => 'transaction.type.upgradevip'
        );
        return $typeText[$this->getType()];
    }
    
    public function getStatusText() {
        $statusText = array(
            self::TRANS_WAITING_PAYMENT => 'transaction.status.waiting_payment',
            self::TRANS_PAIED => 'transaction.status.paied',
            self::TRANS_PROCESSING => 'transaction.status.processing',
            self::TRANS_TERMINATED => 'transaction.status.terminated',
        );
        return $statusText[$this->getStatus()];
    }

    /**
     * Set consolidatedAt
     *
     * @param \DateTime $consolidatedAt
     * @return Transaction
     */
    public function setConsolidatedAt($consolidatedAt)
    {
        $this->consolidatedAt = $consolidatedAt;
    
        return $this;
    }

    /**
     * Get consolidatedAt
     *
     * @return \DateTime 
     */
    public function getConsolidatedAt()
    {
        return $this->consolidatedAt;
    }
}