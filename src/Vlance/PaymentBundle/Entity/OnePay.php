<?php

namespace Vlance\PaymentBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction_onepay")
 * @ORM\Entity(repositoryClass="Vlance\PaymentBundle\Entity\OnePayRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OnePay
{    
    // php console doctrine:generate:entities Vlance/PaymentBundle/Entity/OnePay
    
    const TRANS_TYPE_OTP = 1;
    const TRANS_TYPE_SMS = 2;
    const TRANS_TYPE_BANK = 3;
    const TRANS_TYPE_CREDITCARD = 4;
    
    const TRANS_STATUS_INITIAL = 1;
    const TRANS_STATUS_WAITCONFIRM = 2;
    const TRANS_STATUS_TERMINATED = 4;
    
    const SERV_OTP_CONTACT_FREELANCER   = 101;
    const SERV_OTP_CONTACT_CLIENT       = 102;
    const SERV_OTP_FEATURE_JOB          = 103;
    const SERV_OTP_FEATURE_BID          = 104;
    const SERV_OTP_VERIFY_TEL           = 105;
    const SERV_OTP_FEATURE_FREELANCER   = 106;
    const SERV_OTP_BUY_CREDIT           = 107;
    const SERV_OTP_UPGRADE_ACCOUNT      = 108;
    
    const OPERATOR_OTHER        = 0;
    const OPERATOR_VIETTEL      = 1;
    const OPERATOR_MOBIFONE     = 2;
    const OPERATOR_VINAPHONE    = 3;
    
    const OTP_RATE_OTHER        = 0;
    const OTP_RATE_VIETTEL      = 0.65;
    const OTP_RATE_MOBIFONE     = 0.60;
    const OTP_RATE_VINAPHONE    = 0.65;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="service", type="integer", nullable=true)
     */
    private $service;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $payer;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=15, nullable=true)
     * @Assert\Regex(
     *     pattern="/^[0-9\-\+]{8,15}$/",
     *     message="account.form.telephone.valid_phone"
     * )
     * @Assert\Length(
     *      min = "10",
     *      max = "11",
     *      minMessage = "account.form.telephone.min",
     *      maxMessage = "account.form.telephone.max"
     * )
     */
    protected $telephone = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="operator", type="integer", length=10, nullable=true)
     */
    protected $operator = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="request_id", type="string", length=30, nullable=true)
     */
    protected $requestId = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="trans_id", type="string", length=10, nullable=true)
     */
    protected $transId = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="onepay_response", type="text", nullable=true)
     */
    private $onepayResponse = "";
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return OnePay
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return OnePay
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return OnePay
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OnePay
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return OnePay
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return OnePay
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return OnePay
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    
        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set payer
     *
     * @param \Vlance\AccountBundle\Entity\Account $payer
     * @return OnePay
     */
    public function setPayer(\Vlance\AccountBundle\Entity\Account $payer = null)
    {
        $this->payer = $payer;
    
        return $this;
    }

    /**
     * Get payer
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set requestId
     *
     * @param string $requestId
     * @return OnePay
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
    
        return $this;
    }

    /**
     * Get requestId
     *
     * @return string 
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set transId
     *
     * @param string $transId
     * @return OnePay
     */
    public function setTransId($transId)
    {
        $this->transId = $transId;
    
        return $this;
    }

    /**
     * Get transId
     *
     * @return string 
     */
    public function getTransId()
    {
        return $this->transId;
    }

    /**
     * Set service
     *
     * @param integer $service
     * @return OnePay
     */
    public function setService($service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return integer 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set operator
     *
     * @param integer $operator
     * @return OnePay
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
    
        return $this;
    }

    /**
     * Get operator
     *
     * @return integer 
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set onepayResponse
     *
     * @param string $onepayResponse
     * @return OnePay
     */
    public function setOnepayResponse($onepayResponse)
    {
        $this->onepayResponse = $onepayResponse;
    
        return $this;
    }

    /**
     * Get onepayResponse
     *
     * @return string 
     */
    public function getOnepayResponse()
    {
        return $this->onepayResponse;
    }
    
    /**
     * Add onepayResponse
     *
     * @param string $onepayResponse
     * @return OnePay
     */
    public function addOnepayResponse($onepayResponse)
    {
        $res_text = $this->getOnepayResponse();
        if($res_text == ""){
            $this->onepayResponse = json_encode(array($onepayResponse));
        } else {
            $res_json = json_decode($res_text, true);       // decode to array, instead of object
            array_push($res_json, $onepayResponse);         // Add new response to the array
            $this->onepayResponse = json_encode($res_json); // Save the responses
        }
    
        return $this;
    }
}