<?php

namespace Vlance\PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CreditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'form.title_name',
                    'data-provide' => 'typeahead',
                    'data-items'    => 7,
                    'data-source'   => '["Ngân hàng TMCP Á Châu (ACB)","Ngân hàng Tiên Phong (TP Bank)","Ngân hàng Đông Á (DongA Bank)","Ngân hàng Đông Nam Á (SeABank)","Ngân hàng Đại Dương (Oceanbank)","Ngân hàng An Bình (ABBank)","Ngân hàng Bắc Á (Bac A Bank)","Ngân hàng Dầu Khí Toàn Cầu (GPBank)","Ngân hàng Bản Việt (VCCB)","Ngân hàng Hàng Hải Việt Nam (Maritime Bank)","Ngân hàng Kỹ Thương Việt Nam (Techcombank)","Ngân hàng Kiên Long (KienLongBank)","Ngân hàng Nam Á (Nam A Bank)","Ngân hàng Quốc Dân (NCB)","Ngân hàng Việt Nam Thịnh Vượng (VPBank)","Ngân hàng Phát triển Thành phố Hồ Chí Minh (HDBank)","Ngân hàng Phương Nam (Southern Bank)","Ngân hàng Phương Đông (OCB)","Ngân hàng Quân Đội (MB Bank)","Ngân hàng Đại chúng (PVcom Bank)","Ngân hàng Quốc tế (VIB Bank)","Ngân hàng Sài Gòn (SCB)","Ngân hàng Sài Gòn Công Thương (Saigonbank)","Ngân hàng Sài Gòn-Hà Nội (SHB)","Ngân hàng Sài Gòn Thương Tín (Sacombank)","Ngân hàng Việt Á (Viet A Bank)","Ngân hàng Bảo Việt (Bao Viet Bank)","Ngân hàng Việt Nam Thương Tín (VietBank)","Ngân hàng Xăng dầu Petrolimex (PG Bank)","Ngân hàng Xuất Nhập Khẩu Việt Nam (Eximbank)","Ngân hàng Bưu Điện Liên Việt (LienVietPost Bank)","Ngân hàng Ngoại thương Việt Nam (Vietcombank, VCB)","Ngân hàng Phát Triển Mê Kông (MDB)","Ngân hàng Xây dựng Việt Nam (VNCB)","Ngân hàng Công Thương Việt Nam (Vietinbank)","Ngân hàng Đầu tư và Phát triển Việt Nam (BIDV)","Ngân hàng Nông nghiệp và Phát triển Nông thôn VN (Agribank)","Ngân hàng Phát triển Nhà Đồng bằng sông Cửu Long (MHB)","Ngân hàng TNHH một thành viên ANZ Việt Nam (ANZ)","Ngân hàng Deutsche Bank Việt Nam (Deutsche Bank AG)","Ngân hàng Citibank Việt Nam (Citibank)","Ngân hàng TNHH một thành viên HSBC Việt Nam (HSBC)","Ngân hàng Standard Chartered (Standard Chartered)"]',
                    'class' => 'popovers-input')
                ))
            ->add('info', null, array(
                'label' => false,
                'translation_domain' => 'vlance',
                'attr' => array(
                    'placeholder' => 'form.info',
                    'data-toggle' => 'popover',
                    'data-placement' => 'bottom',
                    'data-content' => 'form.popover.info',
                    'data-trigger' => 'hover',
                    'class' => 'popovers-input')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\PaymentBundle\Entity\UserPaymentMethod'
        ));
    }

    public function getName()
    {
        return 'vlance_paymentbundle_userpaymentmethodtype';
    }
}
