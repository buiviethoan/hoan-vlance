<?php

namespace Vlance\PaymentBundle\Form;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Entity\UserCash;

class TopUpUserCashTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'label'                 => 'credit_buy_transaction.formtype.type.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                'choices'               => array(Transaction::TRANS_TYPE_TOPUP_USERCASH => 'Nạp tiền',),
                ))
            ->add('amount', 'number', array(
                'label'                 => 'credit_buy_transaction.formtype.amount.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                'append'                => 'site.append.currency',
                'attr'                  => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'class' => 'span9 popovers-input',
                    'data-toggle' => "popover",
                    'data-placement' => "top",
                    'data-trigger' => "focus",
                    'autocomplete' => 'off'
                )
                ))
//            ->add('paymentMethod', 'choice', array(
//                'label'                 => 'credit_buy_transaction.formtype.payment_method.label',
//                'translation_domain'    => 'vlance',
//                'required'              => true,
//                'choices'               => array(
//                    1 => 'Chuyển khoản ngân hàng',
//                    2 => 'Nạp tiền trực tiếp'
//                ),
//                ))
            ->add('comment', null, array(
                'label'                 => 'credit_buy_transaction.formtype.comment.label',
                'translation_domain'    => 'vlance',
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\PaymentBundle\Entity\Transaction'
        ));
    }

    public function getName()
    {
        return 'vlance_paymentbundle_topupusercashtransactiontype';
    }
}
