<?php

namespace Vlance\PaymentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class PaymentMethodType extends AbstractType {
    
    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'paymentmethod';
    }
    
    public function getParent() {
        return 'entity';
    }
}