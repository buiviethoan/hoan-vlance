<?php

namespace Vlance\PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paymentMethod', 'paymentmethod', array(
                'label' => 'transaction.paymentmethod.label',
                'class' => 'VlancePaymentBundle:PaymentMethod',
                'property' => 'name',
                'translation_domain' => 'vlance',
                'required' => true,
                ))
            ->add('comment', null, array(
                    'label' => false,
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\PaymentBundle\Entity\Transaction'
        ));
    }

    public function getName()
    {
        return 'vlance_paymentbundle_transactiontype';
    }
}
