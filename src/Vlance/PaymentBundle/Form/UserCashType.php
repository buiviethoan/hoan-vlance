<?php

namespace Vlance\PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserCashType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('balance')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('account')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\PaymentBundle\Entity\UserCash'
        ));
    }

    public function getName()
    {
        return 'vlance_paymentbundle_usercashtype';
    }
}
