<?php

namespace Vlance\PaymentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Vlance\PaymentBundle\Entity\CreditBuyTransaction;

class CreditBuyTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', array(
                'label'                 => 'credit_buy_transaction.formtype.type.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                'choices'               => array(
                    CreditBuyTransaction::TYPE_BUY              => 'credit_buy_transaction.type.' . CreditBuyTransaction::TYPE_BUY,
                    CreditBuyTransaction::TYPE_PROMO            => 'credit_buy_transaction.type.' . CreditBuyTransaction::TYPE_PROMO,
                    CreditBuyTransaction::TYPE_REFUND           => 'credit_buy_transaction.type.' . CreditBuyTransaction::TYPE_REFUND,
                    // CreditBuyTransaction::TYPE_PROMO_REFERRAL   => 'credit_buy_transaction.type.' . CreditBuyTransaction::TYPE_PROMO_REFERRAL
                ),
                ))
            ->add('credit', 'number', array(
                'label'                 => 'credit_buy_transaction.formtype.credit.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                ))
            ->add('amount', 'number', array(
                'label'                 => 'credit_buy_transaction.formtype.amount.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                'append'                => 'site.append.currency',
                'attr'                  => array(
                    'pattern' => '^(0|[1-9][0-9/.]*)$',
                    'class' => 'span9 popovers-input',
                    'data-toggle'=> "popover",
                    'data-placement'=>"top",
                    'data-trigger'=>"focus",
                    'autocomplete' => 'off'
                )
                ))
            ->add('paymentMethod', 'choice', array(
                'label'                 => 'credit_buy_transaction.formtype.payment_method.label',
                'translation_domain'    => 'vlance',
                'required'              => true,
                'choices'               => array(
                    CreditBuyTransaction::PAYMENT_METHOD_BANK_TRANSFER  => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_BANK_TRANSFER,
                    CreditBuyTransaction::PAYMENT_METHOD_PAYPAL         => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_PAYPAL,
                    //CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_OTP     => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_OTP,
                    //CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_BANK    => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_BANK,
                    //CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_VISA    => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_VISA,
                    CreditBuyTransaction::PAYMENT_METHOD_CASH           => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_CASH,
                    //CreditBuyTransaction::PAYMENT_METHOD_USERCASH       => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_USERCASH,
                    CreditBuyTransaction::PAYMENT_METHOD_OTHER          => 'credit_buy_transaction.method.' . CreditBuyTransaction::PAYMENT_METHOD_OTHER
                ),
                ))
            ->add('comment', null, array(
                'label'                 => 'credit_buy_transaction.formtype.comment.label',
                'translation_domain'    => 'vlance',
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\PaymentBundle\Entity\CreditBuyTransaction'
        ));
    }

    public function getName()
    {
        return 'vlance_paymentbundle_creditbuytransactiontype';
    }
}
