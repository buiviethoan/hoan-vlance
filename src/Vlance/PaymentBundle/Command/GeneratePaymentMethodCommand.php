<?php
namespace Vlance\PaymentBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Vlance\PaymentBundle\Entity\PaymentMethod;
use PayPal\Test\Rest\CallTest;

class GeneratePaymentMethodCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
                ->setName('vlance:payment:method:create')
                ->setDescription('Create data for payment method')
                ->addArgument('method',  InputArgument::REQUIRED,'The method that you want to create data');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $methods = explode(',',$input->getArgument('method'));
        foreach ($methods as $method) {
            $this->create($method);
        }
    }
    
    protected function create($methodClass) {
        /* @var $method Vlance\PaymentBundle\Entity\PaymentMethods\PaymentMethodAbstract */
        $methodClass = "Vlance\\PaymentBundle\\Entity\\PaymentMethods\\$methodClass";
//        $transaction = new \Vlance\PaymentBundle\Entity\Transaction();
//        $transaction->setAmount(500000);
//        $method = new $methodClass($this->getContainer(), $transaction);
//        $method->getRedirectUrl();
//        die('test paypal is ok');
        /* @var $em EntityManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $code = $methodClass::CODE;
        $name = $methodClass::NAME;
        $info = $methodClass::INFO;
        
        $entity = $em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('code' => $code));
        
        if (!$entity) {
            $entity = new PaymentMethod();
            $entity->setCode($code);
            $entity->setName($name);
            $entity->setInfo($info);
            $entity->setObjClass($methodClass);
            $em->persist($entity);
            $em->flush();
        }
    }
}