<?php

namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\PaymentBundle\Entity\UserPaymentMethod;
use Vlance\PaymentBundle\Form\UserPaymentMethodType;

/**
 * UserPaymentMethod controller.
 *
 * @Route("/userpaymentmethod")
 */
class UserPaymentMethodController extends Controller
{
    /**
     * Lists all UserPaymentMethod entities.
     *
     * @Route("/", name="userpaymentmethod")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction()
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();

        //Get all user payment methods
        $entities = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->findBy(array('account' => $acc_login));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new user payment method
     *
     * @Route("/create", name="userpaymentmethod_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $entity  = new UserPaymentMethod();
        $form = $this->createForm(new UserPaymentMethodType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $user = $this->get('security.context')->getToken()->getUser();
            /* @var $user \Vlance\AccountBundle\Entity\Account */
            $entity->setAccount($user);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.userpaymentmethod.created', array(), 'vlance'));
            return $this->redirect($this->generateUrl('userpaymentmethod'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new UserPaymentMethod entity.
     *
     * @Route("/new", name="userpaymentmethod_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction()
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $entity = new UserPaymentMethod();
        $form   = $this->createForm(new UserPaymentMethodType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a UserPaymentMethod entity.
     *
     * @Route("/{id}", name="userpaymentmethod_show")
     * @Method("GET")
     * @Template()
     */
//    public function showAction($id)
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $entity = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->find($id);
//
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find UserPaymentMethod entity.');
//        }
//
//        $deleteForm = $this->createDeleteForm($id);
//
//        return array(
//            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),
//        );
//    }

    /**
     * Displays a form to edit an existing UserPaymentMethod entity.
     *
     * @Route("/{id}/edit", name="userpaymentmethod_edit")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function editAction($id)
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->find($id);
        /* @var $entity \Vlance\PaymentBundle\Entity\UserPaymentMethod */
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.userpaymentmethod.entitynotfound', array(), 'vlance'));
        }
        
        $user = $this->get('security.context')->getToken()->getUser();
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        
        if ($user !== $entity->getAccount()) {
            throw new \Exception($this->get('translator')->trans('controller.userpaymentmethod.wronguser', array(), 'vlance'), 500);
        }

        $editForm = $this->createForm(new UserPaymentMethodType(), $entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Edits an existing UserPaymentMethod entity.
     *
     * @Route("/{id}", name="userpaymentmethod_update")
     * @Method("PUT")
     * @Template("VlancePaymentBundle:UserPaymentMethod:edit.html.php", engine="php")
     */
    public function updateAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $entity UserPaymentMethod */
        $entity = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->find($id);
        /* @var $entity \Vlance\PaymentBundle\Entity\UserPaymentMethod */
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.userpaymentmethod.entitynotfound', array(), 'vlance'));
        }
        
        $user = $this->get('security.context')->getToken()->getUser();
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        
        if ($user !== $entity->getAccount()) {
            throw new \Exception($this->get('translator')->trans('controller.userpaymentmethod.wronguser', array(), 'vlance'), 500);
        }
        
        $editForm = $this->createForm(new UserPaymentMethodType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            /* if user edited, the verified become false */
            $entity->setVerified(false);
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.userpaymentmethod.updated', array(), 'vlance'));
            return $this->redirect($this->generateUrl('userpaymentmethod'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Deletes a UserPaymentMethod entity.
     *
     * @Route("/{id}/{token}", name="userpaymentmethod_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id, $token)
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->find($id);
        /* @var $entity \Vlance\PaymentBundle\Entity\UserPaymentMethod */
        if (!$entity) {
            throw $this->createNotFoundException($this->get('translator')->trans('controller.userpaymentmethod.entitynotfound', array(), 'vlance'));
        }
        
        $user = $this->get('security.context')->getToken()->getUser();
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        
        if ($user !== $entity->getAccount()) {
            throw new \Exception($this->get('translator')->trans('controller.userpaymentmethod.wronguser', array(), 'vlance'), 500);
        }
        
        if ($token !== $entity->getDeleteToken()) {
            throw new \Exception($this->get('translator')->trans('controller.userpaymentmethod.wrongdeletetoken', array(), 'vlance'), 500);
        }
        
        $title = $entity->getTitle();
        
        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.userpaymentmethod.removed', array('%title%' => $title), 'vlance'));
        return $this->redirect($this->generateUrl('userpaymentmethod'));
    }
}
