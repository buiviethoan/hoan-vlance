<?php

namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Vlance\AccountBundle\Entity\Account;
use Vlance\AccountBundle\Entity\AccountVIP;
use Vlance\PaymentBundle\Entity\OnePay;
use Vlance\PaymentBundle\Entity\CreditBuyTransaction as CreditBuyTransaction;
use Vlance\AccountBundle\Entity\Connection;
use Vlance\BaseBundle\Utils\ResizeImage;
use Vlance\PaymentBundle\Entity\Transaction as Transaction;
use Vlance\BaseBundle\Utils\JobCalculator;

/**
 * OnePay (1Pay) controller.
 *
 * @Route("/trans")
 */
class OnePayController extends Controller
{
    /**
     * Initialize OTP request 1Pay for charging client
     */
    public function otpRequest($telephone, $content ='', $amount, $service = 0)
    {
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 10));  // User is not signed in yet
        }
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        // Validating data
        if(!isset($telephone) || !isset($content) || !isset($amount)){
            return new JsonResponse(array('error' => 11));  // Missing data
        }        
        $helper = $this->get('vlance_payment.helper');
        $operator = $helper->getTelephoneOperator($telephone);
        if(is_null($operator)){
            return new JsonResponse(array('error' => 12));  // Telephone is not valid
        }
        if($operator['code'] == OnePay::OPERATOR_OTHER){
            return new JsonResponse(array('error' => 13));  // Not support
        }
        
        // Generating random request-id
        $request_id = (string)$service . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        // Initialising transaction in database
        $em = $this->getDoctrine()->getManager();
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_OTP);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        $trans->setService($service);
        $trans->setTelephone($telephone);   // $msisdn: telephone number of payer
        $trans->setOperator($operator['code']);
        $trans->setAmount($amount);
        $trans->setDescription($content);
        $trans->setRequestId($request_id);  // $requestId: invoice id on vlance xxx-xxxxxxxxxxxxxx-x
        $em->persist($trans);
        $em->flush();
        
        // Initializing 1Pay data request
        $data = $this->encapDataRequest(array(
            'amount'    => $amount,
            'content'   => ($operator['code'] == OnePay::OPERATOR_VINAPHONE) ? $this->get('translator')->trans("onepay.otp.request.service_name." . $service, array(), 'vlance') : $content,
            'msisdn'    => $telephone,
            'requestId' => $request_id
        ));
        
        // Processing 1Pay request
        $json_otpCharging = $this->execPostRequest("http://api.1pay.vn/direct-charging/charge/request", $data);
        $decode_response = json_decode($json_otpCharging, true);  // decode json
        
        // Add response to the database
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        // Validating reponse from 1pay
        if(!isset($decode_response['errorMessage']) || !isset($decode_response['requestId']) || !isset($decode_response['transId']) || !isset($decode_response['errorCode'])){
            return new JsonResponse(array(
                'error' => 12,
                'errorCode' => $decode_response['errorCode'],
                'errorMessage' => $decode_response['errorMessage'])); // Send quick error notification
        }
        
        if($decode_response['errorCode'] != "18"){ // OTP is not sent
            return new JsonResponse(array(
                'error' => 13,
                'errorCode' => $decode_response['errorCode'],
                'errorMessage' => $decode_response['errorMessage'])); // Send quick error notification
        }  
        
        $response = array(
            'response'  => $decode_response,
            'id'        => $trans->getId(),
            'error'     => 0,
            'operator'  => $operator['code'],
        );
        
        // Update transaction if everything is ok
        $trans->setStatus(OnePay::TRANS_STATUS_WAITCONFIRM);
        $trans->setTransId($decode_response['transId']);
        $em->persist($trans);
        $em->flush();
        
        // Redirect to confirmation action
        return new JsonResponse($response);
    }
    
    /**
     * Execute the post request
     *
     * @Method("POST")
     */
    protected function execPostRequest($url, $data)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);        
        curl_close($ch);
        $logger->info($result);
        return $result;
    }
    
    /**
     * Encapsulate 1pay data
     */
    protected function encapDataRequest($data)
    {
        $helper = $this->get('vlance_payment.helper');
        $access_key = $helper->getParameter('payment_gateway.onepay.access_key');
        $secret = $helper->getParameter('payment_gateway.onepay.secret');
        
        $data_str = "access_key=".$access_key;
        foreach ($data as $key => $val){
            $data_str .= "&" . $key . "=" . $val;
        }
        
        $signature = hash_hmac("sha256", $data_str, $secret);
        $data_str.= "&signature=" . $signature;
        
        return $data_str;
    }
    
    /**
     * Encapsulate 1pay data
     */
    protected function encapDataVisaRequest($data)
    {
        $helper = $this->get('vlance_payment.helper');
        $access_key = $helper->getParameter('payment_gateway.onepay.access_key');
        $secret = $helper->getParameter('payment_gateway.onepay.secret');
        
        $signature_str = "access_key=".$access_key;
        $data_str = "access_key=".$access_key;
        
        foreach ($data as $key => $val){
            $signature_str .= "&" . $key . "=" . $val;
            if($key == "order_info"){
                break;
            }
        }
        
        $signature = hash_hmac("sha256", $signature_str, $secret);
        
        foreach ($data as $key => $val){
            $data_str .= "&" . $key . "=" . $val;
            if($key == "return_url"){
                $data_str .= "&signature=" . $signature;
            }
        }
        
        return $data_str;
    }
    
    /**
     * Request 1Pay for charging client
     *
     * @Route("/onepay/otp/req/{service_id}", name="transaction_onepay_otp_request")
     * @Method("POST")
     */
    public function otpReqAction(Request $request, $service_id)
    {
        // Early validation
        if(!isset($service_id)){
            return new JsonResponse(array('error' => 1000, '_post' => $_POST));
        }
        
        if(!isset($_POST['data_request'])){
            return new JsonResponse(array('service_id' => $service_id, 'error' => 1010, '_post' => $_POST));
        }
        
        $data_request = $_POST['data_request'];
        if(!isset($data_request['telephone'])){
            return new JsonResponse(array('service_id' => $service_id, 'error' => 1020, '_post' => $_POST));
        }
        $telephone = preg_replace('/\D/s', '', (string)($data_request['telephone'])); // Remove all non-digits characters
        if(strlen($telephone) > 11 || strlen($telephone) < 10){
            return new JsonResponse(array('service_id' => $service_id, 'error' => 1021, '_post' => $_POST));
        }
        
        $helper = $this->get('vlance_payment.helper');
        /*if($service_id == OnePay::SERV_OTP_CONTACT_CLIENT && (int)($data_request['level']) > 0 && (int)($data_request['level']) <= 10){
            $amount = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.amount.level.' . $data_request['level']));
        } elseif($service_id == OnePay::SERV_OTP_BUY_CREDIT){*/
        if($service_id == OnePay::SERV_OTP_BUY_CREDIT){
            if(!isset($data_request['package'])){
                return new JsonResponse(array('service_id' => $service_id, 'error' => 1031, '_post' => $_POST));
            }
            $package_id = $data_request['package']['id'];
            $amount = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
            // Amount Validation 
            if($amount == 0){ // Packages id don't match
                return new JsonResponse(array('service_id' => $service_id, 'error' => 1032, '_post' => $_POST));
            }
            if($amount != (int)($data_request['package']['amount'])){ // Amounts don't match, maybe fake amount from user
                return new JsonResponse(array('service_id' => $service_id, 'error' => 1033, '_post' => $_POST));
            }
            // END Amount Validation
        } else {
            $amount = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.amount'));
        }
        if($amount <= 0){   // Validate $amount
            return new JsonResponse(array('service_id' => $service_id, 'error' => 1030, '_post' => $_POST));
        }
        // END Early validation
        
        // Validation for required date, for respective service
        $required_data = true;
        switch($service_id){
            /*case OnePay::SERV_OTP_CONTACT_CLIENT:       // 102
                $required_data = (!isset($data_request['aid'])) ? false : true;
                break;*/
            case OnePay::SERV_OTP_VERIFY_TEL:           // 105
                $required_data = (!isset($data_request['aid'])) ? false : true;
                break;
            case OnePay::SERV_OTP_BUY_CREDIT:           // 107
                $required_data = (!isset($data_request['package'])) ? false : true;
                break;
            // TODO: Add new service (case xxxx) above this line
            default:
                return new JsonResponse(array('service_id' => $service_id, 'error' => 1040, '_post' => $_POST));
                break;
        }
        if($required_data == false){
            return new JsonResponse(array('service_id' => $service_id, 'error' => 1041, '_post' => $_POST));
        }
        // END Validation for required date, for respective service
        
        // TEST VINAPHONE
        /*$operator = $helper->getTelephoneOperator($telephone);
        if($operator['code'] == OnePay::OPERATOR_VINAPHONE && $service_id == OnePay::SERV_OTP_CONTACT_CLIENT){
            return $this->otpRequest($telephone, json_encode($data_request), 1000, $service_id);
        }*/
        
        return $this->otpRequest($telephone, json_encode($data_request), $amount, $service_id);
    }
    
    /**
     * Webservice for Vinaphone after charging
     *
     * @Route("/onepay/otp/redirect/vinaphone", name="transaction_onepay_redirect_vinaphone")
     * @Method("GET")
     */
    public function otpVinaphoneRedirect(Request $request)
    {
        // Verify data
        $logger = $this->get("monolog.logger.onepay");
        $logger->info(json_encode($request->query));
        
        $logger->info((int)($request->query->get('errorCode')));
        if((int)($request->query->get('errorCode')) != 18){
            $logger->error("Error OTP_VINA " . "2010");
            return new JsonResponse(array('status' => 0, 'sms' => 'FAILED', 'type' =>' text'));
        }
        
        $logger->info((int)($request->query->get('requestId')));
        if(!($request->query->get('requestId'))){
            $logger->error("Error OTP_VINA " . "2011");
            return new JsonResponse(array('status' => 0, 'sms' => 'FAILED', 'type' =>' text'));
        }
        
        // Do some finalization works: sending confirmation emails
        $this->finalizeSuccessTransaction($request->query->get('requestId'));
        
        $em = $this->getDoctrine()->getManager();
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('requestId' => $request->query->get('requestId')));
        $trans->addOnepayResponse(json_encode($request->query));
        
        if(!$trans){
            $logger->error("Error OTP_VINA " . "2012");
            return new JsonResponse(array('status' => 0, 'sms' => 'FAILED', 'type' =>' text'));
        }
        
        $response = array(
            "status"    => 1,
            "sms"       => $this->get('translator')->trans("onepay.otp.confirm.success", array('%%email%%' => $trans->getPayer()->getEmail()), 'vlance'),
            "type"      => "text"
        );
        $logger->info(json_encode($response));
        return new JsonResponse($response);
    }
    
    /**
     * Finalize process after successful charged
     * 
     * @param type $requestId
     * @return boolean
     */
    protected function finalizeSuccessTransaction($requestId)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('requestId' => $requestId));
        
        // Initiate common variables
        $helper = $this->get('vlance_payment.helper');
        $payer = $trans->getPayer();
        $transDetail = json_decode($trans->getDescription(), true);
        $from_vlance = $this->container->getParameter('from_email');
        
        // Change status of transaction
        $trans->setStatus(OnePay::TRANS_STATUS_TERMINATED);
        $em->persist($trans);
        
        // Pre-processing for respective service
        switch($trans->getService()){
            case OnePay::SERV_OTP_VERIFY_TEL:               // 105
                if(isset($transDetail['aid'])){
                    $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $transDetail['aid']));
                    $account->setTelephone($transDetail['telephone']);
                    $account->setTelephoneVerifiedAt(new \DateTime('NOW'));
                    $em->persist($account);

                    $tpl_payer = 'VlancePaymentBundle:Email/verify_tel:confirm_payer.html.twig';
                    $context_payer = array(
                        'payer'     => $payer,
                        'amount'    => number_format($trans->getAmount(),0,',','.'),
                        'telephone' => $transDetail['telephone']
                    );
                }
                break;
            case OnePay::SERV_OTP_BUY_CREDIT:               // 107
                if(isset($transDetail['package'])){
                    $package = $helper->getParameter('payment_gateway.onepay.otp.' . $trans->getService() . '.package.' . $transDetail['package']['id']);
                    
                    $buy_transaction = new CreditBuyTransaction();
                    $buy_transaction->setPayer($payer);
                    $buy_transaction->setCreditAccount($payer->getCredit());
                    $buy_transaction->setType(CreditBuyTransaction::TYPE_BUY);
                    $buy_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
                    $buy_transaction->setAmount($package['amount']);
                    $buy_transaction->setCredit($package['qty']);
                    $buy_transaction->setPreBalance($payer->getCredit()->getBalance());
                    $buy_transaction->setPostBalance($payer->getCredit()->getBalance() + (int)($package['qty']));
                    $buy_transaction->setDetail(json_encode(array('package' => $package, 'onepay_transaction' => $trans->getId())));
                    switch($trans->getType()){
                        case OnePay::TRANS_TYPE_OTP:
                            $buy_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_OTP);
                            $tpl_payer = 'VlancePaymentBundle:Email/buy_credit:confirm_payer.html.twig';
                            break;
                        case OnePay::TRANS_TYPE_BANK:
                            $buy_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_BANK);
                            $tpl_payer = 'VlancePaymentBundle:Email/buy_credit:confirm_bank_payer.html.twig';
                            break;
                        case OnePay::TRANS_TYPE_CREDITCARD:
                            $buy_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_VISA);
                            $tpl_payer = 'VlancePaymentBundle:Email/buy_credit:confirm_bank_visa.html.twig';
                            break;
                        default:
                            $buy_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_OTP);
                            $tpl_payer = 'VlancePaymentBundle:Email/buy_credit:confirm_payer.html.twig';
                            break;
                    }
                    $em->persist($buy_transaction);
                    
                    /* @var $credit_account \Vlance\PaymentBundle\Entity\CreditAccount */
                    $credit_account = $payer->getCredit();
                    $credit_account->setBalance($buy_transaction->getPostBalance());
                    $em->persist($credit_account);
                    $em->flush();
                    
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $buy_transaction->getAmount(),
                        'qty'           => $buy_transaction->getCredit(),
                        'credit_balance'=> $credit_account->getBalance()
                    );
                    
                    // Google Adwords
                    $adword_conversion =   '<!-- Google Code for Buy Credit '. ($trans->getType() == OnePay::TRANS_TYPE_OTP ? "SMS" : "" ) .' Conversion Page -->
                                            <script type="text/javascript">
                                            /* <![CDATA[ */
                                            var google_conversion_id = 925105271;
                                            var google_conversion_language = "en";
                                            var google_conversion_format = "3";
                                            var google_conversion_color = "ffffff";
                                            var google_conversion_label = "'. ($trans->getType() == OnePay::TRANS_TYPE_OTP ? "QtjaCPOsrWYQ9_iPuQM" : "JkdACOXlk2QQ9_iPuQM" ) .'";
                                            var google_conversion_value = '.$buy_transaction->getAmount().';
                                            var google_conversion_currency = "VND";
                                            var google_remarketing_only = false;
                                            /* ]]> */
                                            </script>
                                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                            </script>
                                            <noscript>
                                            <div style="display:inline;">
                                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$buy_transaction->getAmount().'&amp;currency_code=VND&amp;label=' . ($trans->getType() == OnePay::TRANS_TYPE_OTP ? "QtjaCPOsrWYQ9_iPuQM" : "JkdACOXlk2QQ9_iPuQM" ) . '&amp;guid=ON&amp;script=0"/>
                                            </div>
                                            </noscript>';
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', array('type' => 'html', 'html' => $adword_conversion)));
                    
                    /* Promotion Money Lover */
                    // Send promo code to payer
                    $promo_ml_start = new \DateTime("2016-02-22 00:00:00");
                    $promo_ml_end   = new \DateTime("2016-02-29 00:00:00");
                    $promo_ml_limit = 50000;
                    $now            = new \DateTime("now");
                    if($buy_transaction->getAmount() >= $promo_ml_limit){
                        if($promo_ml_start < $now && $now < $promo_ml_end){
                            $detail = array(
                                'transaction'   => "Buy Credit",
                                'amount'        => $buy_transaction->getAmount(),
                                'qty_credit'    => $buy_transaction->getCredit(),
                                'pay_method'    => $buy_transaction->getPaymentMethod()
                            );
                            $this->get('vlance_base.helper')->partnerMoneyloverSendCode($em, $buy_transaction->getPayer()->getId(), $detail);
                        }
                    }
                    /* END Promotion Money Lover */
                    
                    /* Promotion when buying, get double credit */
                    
                    $promo_start = new \DateTime("2016-06-09 13:00:00");
                    $promo_end   = new \DateTime("2016-06-12 23:59:59");
                    $now         = new \DateTime("now");
                    if($promo_start < $now && $now < $promo_end){
                        if($buy_transaction->getPaymentMethod() == CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_BANK || $buy_transaction->getPaymentMethod() == CreditBuyTransaction::PAYMENT_METHOD_USERCASH){
                            $promo_transaction = new CreditBuyTransaction();
                            $promo_transaction->setPayer($payer);
                            $promo_transaction->setCreditAccount($payer->getCredit());
                            $promo_transaction->setType(CreditBuyTransaction::TYPE_PROMO);
                            $promo_transaction->setPaymentMethod($buy_transaction->getPaymentMethod());
                            $promo_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
                            $promo_transaction->setAmount(0); // amount = 0 because this is promotion
                            $promo_transaction->setCredit($package['qty']);
                            $promo_transaction->setPreBalance($payer->getCredit()->getBalance());
                            $promo_transaction->setPostBalance($payer->getCredit()->getBalance() + (int)($package['qty']));
                            $em->persist($promo_transaction);

                            $credit_account->setBalance($promo_transaction->getPostBalance());
                            $em->persist($credit_account);

                            $em->flush();
                            try{
                                $context_promo = array(
                                    'payer'         => $payer,
                                    'qty'           => $promo_transaction->getCredit(),
                                    'credit_balance'=> $credit_account->getBalance(),
                                    'reason'        => "Bạn được tăng thêm 100% số Credit nhân dịp vLance.vn đón thành viên thứ 100.000"
                                );
                                $tpl_promo = 'VlancePaymentBundle:Email/buy_credit:confirm_promotion_payer.html.twig';
                                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_promo, $context_promo, $from_vlance, $payer->getEmail());
                            } catch (Exception $ex) {
                                return new JsonResponse(array('error' => 301));
                            }
                        }
                    }
                }
                break;
            case OnePay::SERV_OTP_UPGRADE_ACCOUNT:               // 108
                if(isset($transDetail['package'])){
                    $package = $helper->getParameter('payment_gateway.onepay.otp.' . $trans->getService() . '.package.' . $transDetail['package']['id']);
                    
                    $vip_tran = new Transaction();
                    $vip_tran->setAmount($package['amount']);
                    $vip_tran->setSender($payer);
                    $vip_tran->setReceiver(null);
                    $vip_tran->setJob(null);
                    $vip_tran->setType(Transaction::TRANS_TYPE_UPGRADE_VIP);
                    $vip_tran->setStatus(Transaction::TRANS_TERMINATED);
                    $em->persist($vip_tran);

                    if($transDetail['package']['id'] <= 3){
                        $vip_tran->setComment('Nâng cấp VIP ' . $package['amount'] . 'VNĐ');
                    } else {
                        $vip_tran->setComment('Nâng cấp MINI VIP ' . $package['amount'] . 'VNĐ');
                    }
                    $em->persist($vip_tran);

                    $acc_vip = new AccountVIP();
                    $acc_vip->setAccount($payer);
                    $acc_vip->setPackage($transDetail['package']['id']);
                    $acc_vip->setPrice($package['amount']);
                    $acc_vip->setPaymentMethod(AccountVIP::PAYMENT_METHOD_ONEPAY_VISA);
                    $em->persist($acc_vip);

                    if($transDetail['package']['id'] <= 3){
                        $payer->setTypeVIP(Account::TYPE_VIP_NORMAL);
                    } else {
                        $payer->setTypeVIP(Account::TYPE_VIP_MINI);
                    }
                    $payer->setCategoryVIP(NULL);
                    $em->persist($payer);

                    $now = new \DateTime('now');
                    if(is_null($payer->getExpiredDateVip())){
                        $expired = $now->modify('+'.$package['qty'].' months');    
                        $payer->setExpiredDateVip($expired);
                        $em->persist($payer);
                    } else {
                        if(JobCalculator::ago($payer->getExpiredDateVip(), $now) == false){//Da het han
                            $expired = $now->modify('+'.$package['qty'].' months');
                            $payer->setExpiredDateVip($expired);
                            $em->persist($payer);
                        } else {//Van con han
                            $payer->setExpiredDateVip($payer->getExpiredDateVip()->modify('+'.$package['qty'].' months'));
                            $em->persist($payer);
                        }
                    }
                    $em->flush();
                    
                    $context_payer = array(
                        'amount'        => number_format($acc_vip->getPrice(),0,',','.'),
                        'account'       => $payer,
                    );
                    $tpl_payer = 'VlanceAccountBundle:Email/Upgrade:freelancer_upgrade_vip.html.twig';
                }
            default:
                break;
        }
        $em->flush();
        
        try {
            // Send tracking event to Mixpanel
            $tracking_event_name = $helper->getParameter('payment_gateway.onepay.otp.' . $trans->getService() . '.tracking_event');
            $this->get('vlance_base.helper')->trackAll(NULL, $tracking_event_name);
            $this->get('vlance_base.helper')->mixpanelTrackRevenue($payer, $trans->getAmount(), $tracking_event_name, array("channel" => "SMS", "telephone" => $trans->getTelephone()));
            // End Mixpanel

            // Send confirmation mail to payer
            if(isset($tpl_payer) && isset($context_payer) && isset($payer)){
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $payer->getEmail());
            }

            // Send notification to to concerned person, if there is
            if(isset($tpl_concerner) && isset($context_concerner) && isset($to_concerner)){
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_concerner, $context_concerner, $from_vlance, $to_concerner);
            }
            
            //VIP
            if($trans->getService() == OnePay::SERV_OTP_UPGRADE_ACCOUNT){
                $admin1 = "tuan.tran@vlance.vn";
                $admin3 = "vlanceantispam@gmail.com";
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin1);
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $admin3);
            }
        } catch (Exception $ex) {
            return false;
        }
        return true;
    }
    
    /**
     * SERVICE ID: 105
     * Confirm OTP code from payer
     *
     * @Route("/onepay/otp/confirm/verifytel", name="transaction_onepay_confirm_verifytel")
     * @Method("POST")
     */
    public function otpConfirmVerifytelAction(Request $request)
    {
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 520)); // Send quick error notification
        }
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        $confirm_url = "http://api.1pay.vn/direct-charging/charge/confirm";
        
        if(!isset($_POST['data_confirm'])){
            return new JsonResponse(array('error' => 521, '_post' => $_POST));
        }
        
        $data_confirm = $_POST['data_confirm'];
        $otp = $data_confirm['otp']; // otp code submitted by payer
        $tid = $data_confirm['tid']; // id of transaction, stored in database
        $aid = $data_confirm['aid'];  // id of job that will be featured on the list
        if($otp == "" || $tid == "" || $aid == ""){
            return new JsonResponse(array('error' => 522)); // Send quick error notification
        }
        
        $em = $this->getDoctrine()->getManager();
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $tid));
        $requestId = $trans->getRequestId();
        $transId = $trans->getTransId();
        
        // Processing 1Pay confirmation request
        $data = $this->encapDataRequest(array(
            'otp'       => $otp,
            'requestId' => $requestId,
            'transId'   => $transId
        ));
        $json_otpCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response=json_decode($json_otpCharging,true);
        
        // Add response to the database
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        if($decode_response['errorCode'] != "00"){
            return new JsonResponse(array(
                'error' => 523,
                'errorCode' => $decode_response['errorCode'],
                'errorMessage' => $decode_response['errorMessage'])); // Send quick error notification
        }
        
        $this->finalizeSuccessTransaction($requestId);
        
        // Response
        $response = array(
            'response'  => $decode_response,
            'error'     => 0
        );
        return new JsonResponse($response);
    }
    
    /**
     * SERVICE ID: 107
     * Confirm OTP code from payer
     *
     * @Route("/onepay/otp/confirm/buy_credit", name="transaction_onepay_confirm_buy_credit")
     * @Method("POST")
     */
    public function otpConfirmBuyCreditAction(Request $request)
    {
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 720)); // Send quick error notification
        }
        
        $confirm_url = "http://api.1pay.vn/direct-charging/charge/confirm";
        
        if(!isset($_POST['data_confirm'])){
            return new JsonResponse(array('error' => 721, '_post' => $_POST));
        }
        
        $data_confirm = $_POST['data_confirm'];
        $otp = $data_confirm['otp']; // otp code submitted by payer
        $tid = $data_confirm['tid']; // id of transaction, stored in database
        $package = $data_confirm['package'];  // id of freelancer who will be contacted
        if($otp == "" || $tid == "" || $package == ""){
            return new JsonResponse(array('error' => 722)); // Send quick error notification
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $tid));
        $requestId = $trans->getRequestId();
        $transId = $trans->getTransId();
        
        // Processing 1Pay confirmation request
        $data = $this->encapDataRequest(array(
            'otp'       => $otp,
            'requestId' => $requestId,
            'transId'   => $transId
        ));
        $json_otpCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response=json_decode($json_otpCharging,true);
        
        // Add response to the database
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        if($decode_response['errorCode'] != "00"){
            return new JsonResponse(array(
                'error' => 723,
                'errorCode' => $decode_response['errorCode'],
                'errorMessage' => $decode_response['errorMessage'])); // Send quick error notification
        }
        
        $this->finalizeSuccessTransaction($requestId);
        
        // Response
        $response = array(
            'response'  => $decode_response,
            'error'     => 0,
        );
        
        if($trans->getPayer()){
            $response['credit_balance'] = $trans->getPayer()->getCredit()->getBalance();
        }
        return new JsonResponse($response);
    }
    
    /**
     * When user submit request to pay via bank card, initiate the transaction,
     * then redirect end-user to the 1Pay landing page.
     *
     * @Route("/onepay/bank/request", name="credit_buy_bankcard_request")
     * @Method("POST")
     */
    public function buyCreditBankcardRequestAction(Request $request)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101)); // Send quick error notification
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $data_request = $request->request->get('data_request');
                
        if(!isset($data_request['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_request['package']['id'];
        $service_id = OnePay::SERV_OTP_BUY_CREDIT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        
        // Amount Validation 
        if($package['amount'] == 0){ // Packages id don't match
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_request['package']['amount'])){ // Amounts don't match, maybe fake amount from user
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        // END Amount Validation
        
        // Generating random request-id
        $request_id = (string)(OnePay::SERV_OTP_BUY_CREDIT) . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        // Initialising transaction in database
        $em = $this->getDoctrine()->getManager();
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_BANK);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        $trans->setService(OnePay::SERV_OTP_BUY_CREDIT);
        $trans->setAmount($package['amount']);
        $trans->setDescription(json_encode(array('package' => $package)));
        $trans->setRequestId($request_id);  // $requestId: invoice id on vlance xxx-xxxxxxxxxxxxxx-x
        $em->persist($trans);
        $em->flush();
        
        $return_url = $this->generateUrl('credit_buy_bankcard_request_return', array(), true);
        $logger->info($return_url);
        
        // Request Pay with Bank card
        $data = $this->encapDataRequest(array(
            'amount'        => $package['amount'],
            'command'       => 'request_transaction',
            'order_id'      => $trans->getId(), //@TODO Use real order_id here
            'order_info'    => "[" . $trans->getPayer()->getId() . "] vLance.vn - Mua " . $package['qty'] . " Credit (" . number_format($package['amount'], 0, ",", ".") . " VND)",
            'return_url'    => $return_url
        ));

        $confirm_url = "http://api.1pay.vn/bank-charging/service";
        
        $json_otpCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response=json_decode($json_otpCharging,true);
        if(!isset($decode_response['pay_url'])){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 201, '_post' => $_POST, 'data' => $data, 'json_otpCharging' => $json_otpCharging, 'decode_response' => $decode_response));
        }
        
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        $response = array(
            'error'     => 0,
            'response'  => $decode_response
        );
        
        return new JsonResponse($response);
    }
    
    /**
     * When the end-user successfully finishes the payment, 1Pay return result to the return_url
     * with all the transaction's detail.
     *
     * @Route("/onepay/bank/request_return", name="credit_buy_bankcard_request_return")
     * @Method("GET")
     */
    public function buyCreditBankcardRequestResponseAction(Request $request)
    {
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        $logger = $this->get("monolog.logger.onepay");
        
        $logger->info(json_encode($_GET));
        
        /* Assertion */
        // access_key
        if(($request->query->get('access_key')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1010), 'vlance'));
            $logger->info(1010);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        if($request->query->get('access_key') != $helper->getParameter('payment_gateway.onepay.access_key')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1011), 'vlance'));
            $logger->info(1011);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $access_key = $request->query->get('access_key');
        $logger->info($access_key);
        
        // amount
        if(($request->query->get('amount')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1020), 'vlance'));
            $logger->info(1020);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $amount = (int)($request->query->get('amount'));
        $logger->info($amount);
        
        // card_name
        /*if(($request->query->get('card_name')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1030), 'vlance'));
            $logger->info(1030);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_name = $request->query->get('card_name');
        $logger->info($card_name);
        
        // card_type
        /*if(($request->query->get('card_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1040), 'vlance'));
            $logger->info(1040);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_type = $request->query->get('card_type');
        $logger->info($card_type);
        
        // order_id
        if(($request->query->get('order_id')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1050), 'vlance'));
            $logger->info(1050);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_id = (int)($request->query->get('order_id'));
        $logger->info($order_id);
        
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $order_id));
        if(!$trans){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 2000), 'vlance'));
            $logger->info(2000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans->addOnepayResponse($_GET);
        $em->persist($trans);
        $em->flush();
        
        // order_info
        if(($request->query->get('order_info')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1060), 'vlance'));
            $logger->info(1060);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_info = $request->query->get('order_info');
        $logger->info($order_info);
        
        // order_type
        if(($request->query->get('order_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1070), 'vlance'));
            $logger->info(1070);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_type = $request->query->get('order_type');
        $logger->info($order_type);
        
        // request_time
        if(($request->query->get('request_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1080), 'vlance'));
            $logger->info(1080);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $request_time = new \DateTime($request->query->get('request_time'));
        $logger->info($request_time->getTimestamp());
        
        // response_code
        if(($request->query->get('response_code')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1090), 'vlance'));
            $logger->info(1090);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_code = $request->query->get('response_code');
        $logger->info($response_code);
        if($response_code != "00"){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1091), 'vlance'));
            $logger->info(1091);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        
        // response_message
        if(($request->query->get('response_message')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1100), 'vlance'));
            $logger->info(1100);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_message = $request->query->get('response_message');
        $logger->info($response_message);
        
        // response_time
        if(($request->query->get('response_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1110), 'vlance'));
            $logger->info(1110);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_time = new \DateTime($request->query->get('response_time'));
        $logger->info($response_time->getTimestamp());
        
        // trans_ref
        if(($request->query->get('trans_ref')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1120), 'vlance'));
            $logger->info(1120);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_ref = $request->query->get('trans_ref');
        $logger->info($trans_ref);
        
        // trans_status
        if(($request->query->get('trans_status')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1130), 'vlance'));
            $logger->info(1130);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_status = $request->query->get('trans_status');
        $logger->info($trans_status);
        
        // signature
        if(($request->query->get('signature')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1140);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $signature = $request->query->get('signature');
        $logger->info($signature);
        $logger->info("Finish paramaters");
        /* END Assertion */
        
        if($this->finalizeSuccessTransaction($trans->getRequestId()) != true){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 3000), 'vlance'));
            $logger->info(3000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.credit.buy.bankcard.success', array(), 'vlance'));
        $logger->info("Success");
        return $this->redirect($this->generateUrl('credit_balance'));
    }
    
    
    /**
     * When user submit request to pay via visa card, initiate the transaction,
     * then redirect end-user to the 1Pay landing page.
     *
     * @Route("/onepay/visa/request", name="credit_buy_visacard_request")
     * @Method("POST")
     */
    public function buyCreditVisacardRequestAction(Request $request)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101)); // Send quick error notification
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $data_request = $request->request->get('data_request');
                
        if(!isset($data_request['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_request['package']['id'];
        $service_id = OnePay::SERV_OTP_BUY_CREDIT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        
        // Amount Validation 
        if($package['amount'] == 0){ // Packages id don't match
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_request['package']['amount'])){ // Amounts don't match, maybe fake amount from user
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        // END Amount Validation
        
        // Generating random request-id
        $request_id = (string)(OnePay::SERV_OTP_BUY_CREDIT) . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        // Initialising transaction in database
        $em = $this->getDoctrine()->getManager();
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_CREDITCARD);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        $trans->setService(OnePay::SERV_OTP_BUY_CREDIT);
        $trans->setAmount($package['amount']);
        $trans->setDescription(json_encode(array('package' => $package)));
        $trans->setRequestId($request_id);  // $requestId: invoice id on vlance xxx-xxxxxxxxxxxxxx-x
        $em->persist($trans);
        $em->flush();
        
        $return_url = $this->generateUrl('credit_buy_visacard_request_return', array(), true);
        $logger->info($return_url);

        // Request Pay with Bank card
        $data = $this->encapDataVisaRequest(array(
            'amount'        => $package['amount'],
            'order_id'      => $trans->getId(), //@TODO Use real order_id here
            'order_info'    => "[" . $trans->getPayer()->getId() . "] vLance.vn - Mua " . $package['qty'] . " Credit (" . number_format($package['amount'], 0, ",", ".") . " VND)",
            'return_url'    => $return_url,
            'cus_fname'     => $trans->getPayer()->getFullName(),
            'cus_email'     => $trans->getPayer()->getEmail(),
            'cus_phone'     => $trans->getPayer()->getTelephone() ? $trans->getPayer()->getTelephone() : ''
        ));

        $confirm_url = "http://visa.1pay.vn/visa-charging/api/handle/request";
        
        $json_visaCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response = json_decode($json_visaCharging, true);

        if(!isset($decode_response['pay_url'])){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 201, '_post' => $_POST, 'data' => $data, 'json_visaCharging' => $json_visaCharging, 'decode_response' => $decode_response));
        }
        
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        $response = array(
            'error'     => 0,
            'response'  => $decode_response
        );
        
        return new JsonResponse($response);
    }
    
    
    /**
     * When the end-user successfully finishes the payment, 1Pay return result to the return_url
     * with all the transaction's detail.
     *
     * @Route("/onepay/visa/request_return", name="credit_buy_visacard_request_return")
     * @Method("GET")
     */
    public function buyCreditVisacardRequestResponseAction(Request $request)
    {
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        $logger = $this->get("monolog.logger.onepay");
        
        $logger->info(json_encode($_GET));
        
        /* Assertion */
        // access_key
        if(($request->query->get('access_key')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1010), 'vlance'));
            $logger->info(1010);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        if($request->query->get('access_key') != $helper->getParameter('payment_gateway.onepay.access_key')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1011), 'vlance'));
            $logger->info(1011);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $access_key = $request->query->get('access_key');
        $logger->info($access_key);
        
        // amount
        if(($request->query->get('amount')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1020), 'vlance'));
            $logger->info(1020);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $amount = (int)($request->query->get('amount'));
        $logger->info($amount);
        
        // card_name
        /*if(($request->query->get('card_name')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1030), 'vlance'));
            $logger->info(1030);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_name = $request->query->get('card_name');
        $logger->info($card_name);
        
        // card_type
        /*if(($request->query->get('card_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1040), 'vlance'));
            $logger->info(1040);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_type = $request->query->get('card_type');
        $logger->info($card_type);
        
        // order_id
        if(($request->query->get('order_id')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1050), 'vlance'));
            $logger->info(1050);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_id = (int)($request->query->get('order_id'));
        $logger->info($order_id);
        
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $order_id));
        if(!$trans){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 2000), 'vlance'));
            $logger->info(2000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans->addOnepayResponse($_GET);
        $em->persist($trans);
        $em->flush();
        
        // order_info
        if(($request->query->get('order_info')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1060), 'vlance'));
            $logger->info(1060);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_info = $request->query->get('order_info');
        $logger->info($order_info);
        
        // order_type
        if(($request->query->get('order_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1070), 'vlance'));
            $logger->info(1070);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_type = $request->query->get('order_type');
        $logger->info($order_type);
        
        // request_time
        if(($request->query->get('request_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1080), 'vlance'));
            $logger->info(1080);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $request_time = new \DateTime($request->query->get('request_time'));
        $logger->info($request_time->getTimestamp());
        
        // response_code
        if(($request->query->get('response_code')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1090), 'vlance'));
            $logger->info(1090);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_code = $request->query->get('response_code');
        $logger->info($response_code);
        if($response_code != "00"){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1091), 'vlance'));
            $logger->info(1091);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        
        // response_message
//        if(($request->query->get('response_message')) == null){
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1100), 'vlance'));
//            $logger->info(1100);
//            return $this->redirect($this->generateUrl('credit_balance'));
//        }
        $response_message = $request->query->get('response_message');
        $logger->info($response_message);
        
        // response_time
        if(($request->query->get('response_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1110), 'vlance'));
            $logger->info(1110);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_time = new \DateTime($request->query->get('response_time'));
        $logger->info($response_time->getTimestamp());
        
        // trans_ref
        if(($request->query->get('trans_ref')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1120), 'vlance'));
            $logger->info(1120);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_ref = $request->query->get('trans_ref');
        $logger->info($trans_ref);
        
        // trans_status
        if(($request->query->get('trans_status')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1130), 'vlance'));
            $logger->info(1130);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_status = $request->query->get('trans_status');
        $logger->info($trans_status);
        
        // signature
        if(($request->query->get('signature')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1140);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $signature = $request->query->get('signature');
        $logger->info($signature);
        $logger->info("Finish paramaters");
        /* END Assertion */
        
        if($this->finalizeSuccessTransaction($trans->getRequestId()) != true){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 3000), 'vlance'));
            $logger->info(3000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.credit.buy.bankcard.success', array(), 'vlance'));
        $logger->info("Success");
        return $this->redirect($this->generateUrl('credit_balance'));
    }
    
    /**
     * When user submit request to pay via visa card, initiate the transaction,
     * then redirect end-user to the 1Pay landing page.
     *
     * @Route("/onepay/visa/account/request", name="upgrade_account_visacard_request")
     * @Method("POST")
     */
    public function upgradeAccountVisacardRequestAction(Request $request)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101)); // Send quick error notification
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $data_request = $request->request->get('data_request');
                
        if(!isset($data_request['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_request['package']['id'];
        $service_id = OnePay::SERV_OTP_UPGRADE_ACCOUNT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));

        // Amount Validation 
        if($package['amount'] == 0){ // Packages id don't match
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_request['package']['amount'])){ // Amounts don't match, maybe fake amount from user
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        // END Amount Validation
        
        // Generating random request-id
        $request_id = (string)(OnePay::SERV_OTP_UPGRADE_ACCOUNT) . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        // Initialising transaction in database
        $em = $this->getDoctrine()->getManager();
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_CREDITCARD);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        $trans->setService(OnePay::SERV_OTP_UPGRADE_ACCOUNT);
        $trans->setAmount($package['amount']);
        $trans->setDescription(json_encode(array('package' => $package)));
        $trans->setRequestId($request_id);  // $requestId: invoice id on vlance xxx-xxxxxxxxxxxxxx-x
        $em->persist($trans);
        $em->flush();
        
        $return_url = $this->generateUrl('upgrade_account_visacard_request_return', array(), true);
        $logger->info($return_url);

        $label = "";
        if($package_id < 4){
            $label = 'Vip ' . $package['qty'] . '';
        } else {
            $label = 'Mini ' . $package['qty'] . '';
        }
        
        // Request Pay with Bank card
        $data = $this->encapDataVisaRequest(array(
            'amount'        => ($auth_user->getId() == 4) ? 11000 : $package['amount'],
            'order_id'      => $trans->getId(),
            'order_info'    => "vLance.vn - ID#" . $trans->getPayer()->getId() . "  - Goi ". $label ." (" . number_format($package['amount'], 0, ",", ".") . " VND)",
            'return_url'    => $return_url,
            'cus_fname'     => $trans->getPayer()->getFullName(),
            'cus_email'     => $trans->getPayer()->getEmail(),
            'cus_phone'     => $trans->getPayer()->getTelephone() ? $trans->getPayer()->getTelephone() : ''
        ));

        $confirm_url = "http://visa.1pay.vn/visa-charging/api/handle/request";
        
        $json_visaCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response = json_decode($json_visaCharging, true);

        if(!isset($decode_response['pay_url'])){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 201, '_post' => $_POST, 'data' => $data, 'json_visaCharging' => $json_visaCharging, 'decode_response' => $decode_response));
        }
        
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        $response = array(
            'error'     => 0,
            'response'  => $decode_response
        );
        
        return new JsonResponse($response);
    }
    
    /**
     * When the end-user successfully finishes the payment, 1Pay return result to the return_url
     * with all the transaction's detail.
     *
     * @Route("/onepay/visa/account/request_return", name="upgrade_account_visacard_request_return")
     * @Method("GET")
     */
    public function upgradeAccountVisacardRequestResponseAction(Request $request)
    {
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        $logger = $this->get("monolog.logger.onepay");
        
        $logger->info(json_encode($_GET));
        
        /* Assertion */
        // access_key
        if(($request->query->get('access_key')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1010), 'vlance'));
            $logger->info(1010);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        if($request->query->get('access_key') != $helper->getParameter('payment_gateway.onepay.access_key')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1011), 'vlance'));
            $logger->info(1011);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $access_key = $request->query->get('access_key');
        $logger->info($access_key);
        
        // amount
        if(($request->query->get('amount')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1020), 'vlance'));
            $logger->info(1020);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $amount = (int)($request->query->get('amount'));
        $logger->info($amount);
        
        // card_name
        /*if(($request->query->get('card_name')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1030), 'vlance'));
            $logger->info(1030);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_name = $request->query->get('card_name');
        $logger->info($card_name);
        
        // card_type
        /*if(($request->query->get('card_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1040), 'vlance'));
            $logger->info(1040);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_type = $request->query->get('card_type');
        $logger->info($card_type);
        
        // order_id
        if(($request->query->get('order_id')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1050), 'vlance'));
            $logger->info(1050);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_id = (int)($request->query->get('order_id'));
        $logger->info($order_id);
        
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $order_id));
        if(!$trans){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 2000), 'vlance'));
            $logger->info(2000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans->addOnepayResponse($_GET);
        $em->persist($trans);
        $em->flush();
        
        // order_info
        if(($request->query->get('order_info')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1060), 'vlance'));
            $logger->info(1060);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_info = $request->query->get('order_info');
        $logger->info($order_info);
        
        // order_type
        if(($request->query->get('order_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1070), 'vlance'));
            $logger->info(1070);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_type = $request->query->get('order_type');
        $logger->info($order_type);
        
        // request_time
        if(($request->query->get('request_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1080), 'vlance'));
            $logger->info(1080);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $request_time = new \DateTime($request->query->get('request_time'));
        $logger->info($request_time->getTimestamp());
        
        // response_code
        if(($request->query->get('response_code')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1090), 'vlance'));
            $logger->info(1090);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_code = $request->query->get('response_code');
        $logger->info($response_code);
        if($response_code != "00"){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1091), 'vlance'));
            $logger->info(1091);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        
        // response_message
//        if(($request->query->get('response_message')) == null){
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1100), 'vlance'));
//            $logger->info(1100);
//            return $this->redirect($this->generateUrl('credit_balance'));
//        }
        $response_message = $request->query->get('response_message');
        $logger->info($response_message);
        
        // response_time
        if(($request->query->get('response_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1110), 'vlance'));
            $logger->info(1110);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_time = new \DateTime($request->query->get('response_time'));
        $logger->info($response_time->getTimestamp());
        
        // trans_ref
        if(($request->query->get('trans_ref')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1120), 'vlance'));
            $logger->info(1120);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_ref = $request->query->get('trans_ref');
        $logger->info($trans_ref);
        
        // trans_status
        if(($request->query->get('trans_status')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1130), 'vlance'));
            $logger->info(1130);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_status = $request->query->get('trans_status');
        $logger->info($trans_status);
        
        // signature
        if(($request->query->get('signature')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1140);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $signature = $request->query->get('signature');
        $logger->info($signature);
        $logger->info("Finish paramaters");
        /* END Assertion */
        
        if($this->finalizeSuccessTransaction($trans->getRequestId()) != true){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 3000), 'vlance'));
            $logger->info(3000);
            return $this->redirect($this->generateUrl('credit_balance'));   
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.buy.bankcard.success', array(), 'vlance'));
        $logger->info("Success");
        return $this->redirect($this->generateUrl('credit_balance'));
    }
    
    /**
     * When user submit request to pay via bank card, initiate the transaction,
     * then redirect end-user to the 1Pay landing page.
     *
     * @Route("/onepay/bankcard/account/request", name="upgrade_account_bankcard_request")
     * @Method("POST")
     */
    public function upgradeAccountBankcardRequestAction(Request $request)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $data_request = $request->request->get('data_request');
                
        if(!isset($data_request['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_request['package']['id'];
        $service_id = OnePay::SERV_OTP_UPGRADE_ACCOUNT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        
        if($package['amount'] == 0){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_request['package']['amount'])){
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        
        $request_id = (string)(OnePay::SERV_OTP_UPGRADE_ACCOUNT) . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        $em = $this->getDoctrine()->getManager();
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_BANK);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        $trans->setService(OnePay::SERV_OTP_UPGRADE_ACCOUNT);
        $trans->setAmount($package['amount']);
        $trans->setDescription(json_encode(array('package' => $package)));
        $trans->setRequestId($request_id);
        $em->persist($trans);
        $em->flush();
        
        $return_url = $this->generateUrl('upgrade_account_bankcard_request_return', array(), true);
        $logger->info($return_url);
        
        $label = "";
        if($package_id < 4){
            $label = 'Vip ' . $package['qty'] . '';
        } else {
            $label = 'Mini ' . $package['qty'] . '';
        }
        
        $data = $this->encapDataRequest(array(
            'amount'        => ($auth_user->getId() == 4) ? 11000 : $package['amount'],
            'command'       => 'request_transaction',
            'order_id'      => $trans->getId(),
            'order_info'    => "vLance.vn - ID#" . $trans->getPayer()->getId() . "  - Goi ". $label ." (" . number_format($package['amount'], 0, ",", ".") . " VND)",
            'return_url'    => $return_url
        ));

        $confirm_url = "http://api.1pay.vn/bank-charging/service";
        
        $json_otpCharging = $this->execPostRequest($confirm_url, $data);
        $decode_response=json_decode($json_otpCharging,true);
        if(!isset($decode_response['pay_url'])){
            return new JsonResponse(array('package_id' => $package_id, 'error' => 201, '_post' => $_POST, 'data' => $data, 'json_otpCharging' => $json_otpCharging, 'decode_response' => $decode_response));
        }
        
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        $response = array(
            'error'     => 0,
            'response'  => $decode_response
        );
        
        return new JsonResponse($response);
    }
    
    /**
     * When the end-user successfully finishes the payment, 1Pay return result to the return_url
     * with all the transaction's detail.
     *
     * @Route("/onepay/bankcard/account/request_return", name="upgrade_account_bankcard_request_return")
     * @Method("GET")
     */
    public function upgradeAccountBankcardRequestResponseAction(Request $request)
    {
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        $logger = $this->get("monolog.logger.onepay");
        
        $logger->info(json_encode($_GET));
        
        /* Assertion */
        // access_key
        if(($request->query->get('access_key')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1010), 'vlance'));
            $logger->info(1010);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        if($request->query->get('access_key') != $helper->getParameter('payment_gateway.onepay.access_key')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1011), 'vlance'));
            $logger->info(1011);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $access_key = $request->query->get('access_key');
        $logger->info($access_key);
        
        // amount
        if(($request->query->get('amount')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1020), 'vlance'));
            $logger->info(1020);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $amount = (int)($request->query->get('amount'));
        $logger->info($amount);
        
        // card_name
        /*if(($request->query->get('card_name')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1030), 'vlance'));
            $logger->info(1030);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_name = $request->query->get('card_name');
        $logger->info($card_name);
        
        // card_type
        /*if(($request->query->get('card_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1040), 'vlance'));
            $logger->info(1040);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_type = $request->query->get('card_type');
        $logger->info($card_type);
        
        // order_id
        if(($request->query->get('order_id')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1050), 'vlance'));
            $logger->info(1050);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_id = (int)($request->query->get('order_id'));
        $logger->info($order_id);
        
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $order_id));
        if(!$trans){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 2000), 'vlance'));
            $logger->info(2000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans->addOnepayResponse($_GET);
        $em->persist($trans);
        $em->flush();
        
        // order_info
        if(($request->query->get('order_info')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1060), 'vlance'));
            $logger->info(1060);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_info = $request->query->get('order_info');
        $logger->info($order_info);
        
        // order_type
        if(($request->query->get('order_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1070), 'vlance'));
            $logger->info(1070);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_type = $request->query->get('order_type');
        $logger->info($order_type);
        
        // request_time
        if(($request->query->get('request_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1080), 'vlance'));
            $logger->info(1080);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $request_time = new \DateTime($request->query->get('request_time'));
        $logger->info($request_time->getTimestamp());
        
        // response_code
        if(($request->query->get('response_code')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1090), 'vlance'));
            $logger->info(1090);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_code = $request->query->get('response_code');
        $logger->info($response_code);
        if($response_code != "00"){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1091), 'vlance'));
            $logger->info(1091);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        
        // response_message
        if(($request->query->get('response_message')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1100), 'vlance'));
            $logger->info(1100);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_message = $request->query->get('response_message');
        $logger->info($response_message);
        
        // response_time
        if(($request->query->get('response_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1110), 'vlance'));
            $logger->info(1110);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_time = new \DateTime($request->query->get('response_time'));
        $logger->info($response_time->getTimestamp());
        
        // trans_ref
        if(($request->query->get('trans_ref')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1120), 'vlance'));
            $logger->info(1120);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_ref = $request->query->get('trans_ref');
        $logger->info($trans_ref);
        
        // trans_status
        if(($request->query->get('trans_status')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1130), 'vlance'));
            $logger->info(1130);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_status = $request->query->get('trans_status');
        $logger->info($trans_status);
        
        // signature
        if(($request->query->get('signature')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1140);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $signature = $request->query->get('signature');
        $logger->info($signature);
        $logger->info("Finish paramaters");
        /* END Assertion */
        
        if($this->finalizeSuccessTransaction($trans->getRequestId()) != true){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.account.buy.bankcard.error', array('%error_code%' => 3000), 'vlance'));
            $logger->info(3000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.account.buy.bankcard.success', array(), 'vlance'));
        $logger->info("Success");
        return $this->redirect($this->generateUrl('credit_balance'));
    }
}
