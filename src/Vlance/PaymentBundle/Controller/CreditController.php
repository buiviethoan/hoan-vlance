<?php

namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\BaseBundle\Utils\ResizeImage;
use Vlance\JobBundle\Entity\Job as Job;
use Vlance\JobBundle\Entity\Bid as Bid;
use Vlance\AccountBundle\Entity\Account as Account;
use Vlance\AccountBundle\Entity\Connection;
use Vlance\PaymentBundle\Entity\CreditExpenseTransaction as CreditExpenseTransaction;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Entity\OnePay;
use Vlance\PaymentBundle\Entity\CreditAccount;
use Vlance\PaymentBundle\Entity\CreditBuyTransaction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pagerfanta\View\DefaultView;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;


/**
 * Credit controller.
 *
 * @Route("/credit")
 */
class CreditController extends Controller
{    
    /**
     * Balance credit of user
     *
     * @Route("/balance/{referer}", name="credit_balance")
     * @Method("GET")
     * @Template("VlancePaymentBundle:Credit:balance.html.php",engine="php")
     */
    public function balanceAction(Request $request, $referer = null)
    {
        $referer = base64_encode($referer == null ? $request->headers->get('referer') : $this->generateUrl('credit_balance', array('referer' => $referer)));
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.notvalid', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page Credit Vip", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "credit_balance",
                    'authenticated' => is_object($this->get('security.context')->getToken()->getUser()) ? "TRUE" : "FALSE"
                ));
            }
        }

        
        if(is_object($acc)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($acc->getEmail())){
                if($helper->emailNotFacebook($acc->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $acc->getId())));
                }
            }
        }
        
        $credit_account = $acc->getCredit();
        $usercash_balance = $acc->getCash()->getBalance();
        
        if(!$credit_account){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.balance.credit_account_not_exist', array(), 'vlance'));
        }
        $credit_balance = $credit_account->getBalance();
        
        // Promotion Money Lover
        $promo_ml_start = new \DateTime("2016-02-22 00:00:00");
        $promo_ml_end   = new \DateTime("2016-02-29 00:00:00");
        $promo_ml_limit = 50000;
        
        $em = $this->getDoctrine()->getManager();
        
        return array(
            'acc'               => $acc,
            'credit_balance'    => $credit_balance,
            'usercash_balance'  => $usercash_balance,
            'promo_ml'          => array(
                'promo_ml_start'=> $promo_ml_start,
                'promo_ml_end'  => $promo_ml_end,
                'promo_ml_limit'=> $promo_ml_limit,
                'available'     => $this->get('vlance_base.helper')->getAvailableForPartnerMoneyloverCode($em, $acc->getId())
            )
        );
    }
    
    /**
     * User buy credit by Usercash
     *
     * @Route("/buy_confirm", name="credit_buy_confirm")
     * @Method("POST")
     */
    public function buyConfirmAction(Request $request)
    {
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101)); // Send quick error notification
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $data_confirm = $request->request->get('data_confirm');
                
        if(!isset($data_confirm['package'])){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        $package_id = $data_confirm['package']['id'];
        $service_id = OnePay::SERV_OTP_BUY_CREDIT;
        $package = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id);
        $package['amount'] = (int)($helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.package.' . $package_id . '.amount'));
        
        // Amount Validation 
        if($package['amount'] == 0){ // Packages id don't match
            return new JsonResponse(array('package_id' => $package_id, 'error' => 103, '_post' => $_POST));
        }
        if($package['amount'] != (int)($data_confirm['package']['amount'])){ // Amounts don't match, maybe fake amount from user
            return new JsonResponse(array('amount' => $package['amount'], 'error' => 104, '_post' => $_POST));
        }
        // END Amount Validation
        
        $em = $this->getDoctrine()->getManager();
        
        /* @var $usercash \Vlance\PaymentBundle\Entity\UserCash */
        $usercash = $em->getRepository('VlancePaymentBundle:UserCash')->findOneBy(array('id' => $auth_user->getCash()->getId()));
        if(!$usercash->getId()){
            return new JsonResponse(array('usercash_id' => $auth_user->getCash()->getId(), 'error' => 105));
        }
        if($usercash->getActualBalance() < $package['amount']){
            return new JsonResponse(array('balance' => $auth_user->getCash()->getActualBalance(), 'error' => 106, 'errorMessage' => "Rất tiếc, số dư trong ví không đủ để thanh toán.", '_post' => $_POST));
        }
        
        /* @var $credit_account \Vlance\PaymentBundle\Entity\CreditAccount */
        $credit_account = $em->getRepository('VlancePaymentBundle:CreditAccount')->findOneBy(array('id' => $auth_user->getCredit()->getId()));
        if(!$credit_account->getId()){
            return new JsonResponse(array('creditaccount_id' => $auth_user->getCredit()->getId(), 'error' => 107));
        }
        
        $from_vlance = $this->container->getParameter('from_email');
        
        // Usercash Transaction
        $usercash_trans = new Transaction();
        $usercash_trans->setAmount($package['amount']);
        $usercash_trans->setSender($auth_user);
        $usercash_trans->setReceiver(null);
        $usercash_trans->setJob(null);
        $usercash_trans->setType(Transaction::TRANS_TYPE_BUY_CREDIT);
        $usercash_trans->setStatus(Transaction::TRANS_TERMINATED);
        $usercash_trans->setComment('Mua credit ' . $package['amount']);
        $em->persist($usercash_trans);
        
        // Update Usercash after paid
        $usercash_balance = $usercash->getBalance() - $usercash_trans->getAmount();
        $usercash->setBalance($usercash_balance);
        $em->persist($usercash);
        
        // Buy Credit transaction
        $buy_credit_trans = new CreditBuyTransaction();
        $buy_credit_trans->setPayer($auth_user);
        $buy_credit_trans->setCreditAccount($auth_user->getCredit());
        $buy_credit_trans->setType(CreditBuyTransaction::TYPE_BUY);
        $buy_credit_trans->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
        $buy_credit_trans->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_USERCASH);
        $buy_credit_trans->setCredit($package['qty']);
        $buy_credit_trans->setAmount($package['amount']);
        $buy_credit_trans->setPreBalance($auth_user->getCredit()->getBalance());
        $buy_credit_trans->setDetail(json_encode(array('package' => $package)));
        
        // Add new credit to user
        $credit_balance = $auth_user->getCredit()->getBalance() + $buy_credit_trans->getCredit();
        $credit_account->setBalance($credit_balance);
        $em->persist($credit_account);
        
        // Save buy credit transaction
        $buy_credit_trans->setPostBalance($credit_balance);
        $em->persist($buy_credit_trans);
        
        $em->flush();
        
        // Update Usercash transaction id in Buy credit transaction detail
        $buy_credit_trans->setDetail(json_encode(array('package' => $package, 'usercash_transaction' => $usercash_trans->getId())));
        $em->persist($buy_credit_trans);
        $em->flush();
        
        try{
            // Send tracking event to Mixpanel
            $tracking_event_name = $helper->getParameter('payment_gateway.onepay.otp.' . $service_id . '.tracking_event');
            $this->get('vlance_base.helper')->trackAll(NULL, $tracking_event_name);
            $this->get('vlance_base.helper')->mixpanelTrackRevenue($auth_user, $package['amount'], $tracking_event_name, array("channel" => "Usercash"));
            // End Mixpanel

            // Send confirmation mail to payer
            $context_payer = array(
                'payer'         => $auth_user,
                'amount'        => number_format($usercash_trans->getAmount(),0,',','.'),
                'qty'           => $buy_credit_trans->getCredit(),
                'balance'       => number_format($usercash->getActualBalance(),0,',','.'),
                'credit_balance'=> $credit_account->getBalance(),
                'trans'         => $buy_credit_trans
            );
            $tpl_payer = 'VlancePaymentBundle:Email/buy_credit:confirm_usercash_payer.html.twig';
            $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $auth_user->getEmail());
            
            // Send confirmation mail to Admin
            $admin_tpl_payer = 'VlancePaymentBundle:Email/buy_credit:admin_confirm_usercash_payer.html.twig';
            $admin1 = "tuan.tran@vlance.vn";
            $admin2 = "hien.do@vlance.vn";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_payer, $from_vlance, $admin1);
            $this->get('vlance.mailer')->sendTWIGMessageBySES($admin_tpl_payer, $context_payer, $from_vlance, $admin2);
            
            // Google Adwords
            $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                array('type' => 'html', 'html' =>
                    '<!-- Google Code for Buy Credit Conversion Page -->
                    <script type="text/javascript">
                    /* <![CDATA[ */
                    var google_conversion_id = 925105271;
                    var google_conversion_language = "en";
                    var google_conversion_format = "3";
                    var google_conversion_color = "ffffff";
                    var google_conversion_label = "JkdACOXlk2QQ9_iPuQM";
                    var google_conversion_value = '.$buy_credit_trans->getAmount().';
                    var google_conversion_currency = "VND";
                    var google_remarketing_only = false;
                    /* ]]> */
                    </script>
                    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                    </script>
                    <noscript>
                    <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$buy_credit_trans->getAmount().'&amp;currency_code=VND&amp;label=JkdACOXlk2QQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                    </div>
                    </noscript>'
                )
            ));
        } catch (Exception $ex) {
            return new JsonResponse(array('error' => 301));
        }
        
        // Promotion Money Lover
        $promo_ml_start = new \DateTime("2016-02-22 00:00:00");
        $promo_ml_end   = new \DateTime("2016-02-29 00:00:00");
        $promo_ml_limit = 50000;
        $now            = new \DateTime("now");
        if($buy_credit_trans->getAmount() >= $promo_ml_limit){
            if($promo_ml_start < $now && $now < $promo_ml_end){
                $detail = array(
                    'transaction'   => "Buy Credit",
                    'amount'        => $buy_credit_trans->getAmount(),
                    'qty_credit'    => $buy_credit_trans->getCredit(),
                    'pay_method'    => CreditBuyTransaction::PAYMENT_METHOD_USERCASH
                );
                $this->get('vlance_base.helper')->partnerMoneyloverSendCode($em, $buy_credit_trans->getPayer()->getId(), $detail);
            }
        }

        // Promotion theo ngày
        $promo_start = new \DateTime("2016-06-09 13:00:00");
        $promo_end   = new \DateTime("2016-06-12 23:59:59");
        $now         = new \DateTime("now");
        //if($buy_credit_trans->getPaymentMethod() == CreditBuyTransaction::PAYMENT_METHOD_USERCASH){
        if($promo_start < $now && $now < $promo_end){
            if($buy_credit_trans->getPaymentMethod() == CreditBuyTransaction::PAYMENT_METHOD_ONEPAY_BANK || $buy_credit_trans->getPaymentMethod() == CreditBuyTransaction::PAYMENT_METHOD_USERCASH){
                $promo_credit = ceil((int)($package['qty']));
                $promo_transaction = new CreditBuyTransaction();
                $promo_transaction->setPayer($auth_user);
                $promo_transaction->setCreditAccount($auth_user->getCredit());
                $promo_transaction->setType(CreditBuyTransaction::TYPE_PROMO);
                $promo_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_USERCASH);
                $promo_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
                $promo_transaction->setAmount(0); // amount = 0 because this is promotion
                $promo_transaction->setCredit($promo_credit);
                $promo_transaction->setPreBalance($auth_user->getCredit()->getBalance());
                $promo_transaction->setPostBalance($auth_user->getCredit()->getBalance() + $promo_credit);
                $em->persist($promo_transaction);

                $credit_account->setBalance($promo_transaction->getPostBalance());
                $em->persist($credit_account);

                $em->flush();
                try{
                    $context_promo = array(
                        'payer'         => $auth_user,
                        'qty'           => $promo_transaction->getCredit(),
                        'credit_balance'=> $credit_account->getBalance(),
                        'reason'        => "Bạn được tăng thêm 100% số Credit nhân dịp vLance.vn đón thành viên thứ 100.000"
                    );
                    $tpl_promo = 'VlancePaymentBundle:Email/buy_credit:confirm_promotion_payer.html.twig';
                    $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_promo, $context_promo, $from_vlance, $auth_user->getEmail());
                } catch (Exception $ex) {
                    return new JsonResponse(array('error' => 301));
                }
            }
        }
        //}

        return new JsonResponse(array(
            'error' => 0,
            'balance'  => array(
                'usercash'  => number_format($usercash->getActualBalance(),0,',','.'),
                'credit'    => $credit_account->getBalance()
            )
        ));
    }
            
    
    /**
     * List of credit spending transaction
     *
     * @Route("/spend", name="credit_spend")
     * @Method("POST")
     */
    public function spendAction(Request $request)
    {
        /*** Validation ***/
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->responseError(101);
        }
        $auth_user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('vlance_payment.helper');
        $from_vlance = $this->container->getParameter('from_email');
        
        $service_id = (int)($request->request->get('service_id'));
        if($service_id <= 0){
            return $this->responseError(102);
        }
        
        // Verify payer's credit account authentication, credit balance
        $pid = (int)($request->request->get('pid'));
        if($pid <= 0){
            return $this->responseError(103);
        }
        /* @var $payer Account */
        $payer = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $pid));
        if(!$payer) {
            return $this->responseError(104);
        }
        if($payer->getId() != $auth_user->getId()) {
            return $this->responseError(105);
        }
        $service = $helper->getParameter('credit.spend.' . $service_id);
        
        // Only when SERVICE_CONTACT_CLIENT
        // Need to check the level of job's budget so we can process the number of credit correctly.
        if($service_id == CreditExpenseTransaction::SERVICE_CONTACT_CLIENT){
            $path = $request->request->get('path');
            $router = $this->get('router');
            $routes = $router->getRouteCollection();
            $context = new RequestContext("/");
            $matcher = new UrlMatcher($routes, $context);
            try {
                $match = $matcher->match($path);
                if(isset($match['_route']) && isset($match['hash']) && $match['_route'] == "job_show_freelance_job"){
                    $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('hash' => $match['hash']));
                    if(($level = $this->get('vlance_payment.helper')->getLevelPayingContactClient($job)) > 0){
                        $service['amount'] = $service['amount'][$level];
                    } else{
                        return $this->responseError(108);
                    }
                }
            } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $e) {
            } catch (\Symfony\Component\Routing\Exception\MethodNotAllowedException $e) {
            }            
        }
        
        if(!isset($service['amount']) || !isset($service['tracking_event']) || !isset($service['id'])){
            return $this->responseError(106);
        }
        if((int)($service['amount']) > $payer->getCredit()->getBalance()){
            return $this->responseError(107, "Không đủ credit.");
        }
        /*** END Validation ***/

        $credit_trans = new CreditExpenseTransaction();
        $credit_trans->setCreditAccount($payer->getCredit());
        $credit_trans->setPreBalance($payer->getCredit()->getBalance());
        $credit_trans->setStatus(CreditExpenseTransaction::STATUS_WAITING);
        $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
        $credit_trans->setAmount($service['amount']);
        $credit_trans->setService($service['id']);
        
        $tracking_properties = array(
            "channel"       => "Credit", 
            "payer_id"      => $payer->getId(), 
            "payer_category"=> $payer->getCategory()->getTitle(),
            "credit_spent"  => $service['amount']
        );
        
        $credit_account = $payer->getCredit();
        $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());
        
        $success_data = array(
            'error'         => 0,
            'errorMsg'      => null,
            'credit_spent'  => $service['amount'],
            'credit_balance'=> $payer->getCredit()->getBalance()
        );
        
        switch($service_id){
            case CreditExpenseTransaction::SERVICE_FEATURE_JOB:
                // Verify job id and job owner
                $jid = (int)($request->request->get('jid'));
                if($jid <= 0){
                    return $this->responseError(201);
                }
                /* @var $job Job */
                $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('id' => $jid));
                if(!$job){ 
                    return $this->responseError(202);
                }
                if($job->getAccount()->getId() != $payer->getId()){
                    return $this->responseError(203);
                }
                
                // Set feature job until xx days
                $number_day = (int)($service['duration']);
                if($number_day <= 0){
                    return $this->responseError(204);
                }
                $featuredUntil = new \DateTime();
                $job->setFeaturedUntil($featuredUntil->add(new \DateInterval('P' . $number_day . 'D')));
                if($job->getIsPrivate() === true){
                    $job->setIsPrivate(false);
                }
                $em->persist($job);
                
                $credit_trans->setDetail(json_encode(array('job_id' => $job->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/feature_job:confirm_credit_payer.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'jid'           => $job->getId(),
                        'jtitle'        => $job->getTitle()
                    );
                    
                // Set addtional tracking event properties
                $tracking_properties['job_category']    = $job->getCategory()->getTitle();
                $tracking_properties['job_budget']      = $job->getBudget();
                break;   
            case CreditExpenseTransaction::SERVICE_FEATURE_BID:
                // Verify bid id and bid owner
                $id = (int)($request->request->get('bid'));
                if($id <= 0){
                    return $this->responseError(301);
                }
                /* @var $bid Bid */
                $bid = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $id));
                if(!$bid){ 
                    return $this->responseError(302);
                }
                if($bid->getAccount()->getId() != $payer->getId()){
                    return $this->responseError(303);
                }
                
                // Set feature bid
                $bid->setIsFeatured(true);
                $em->persist($bid);
                
                $credit_trans->setDetail(json_encode(array('bid_id' => $bid->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/feature_bid:confirm_credit_payer.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'bid'           => $bid->getId(),
                        'jtitle'        => $bid->getJob()->getTitle()
                    );
                    
                // Set addtional tracking event properties
                $tracking_properties['job_category']    = $bid->getJob()->getCategory()->getTitle();
                $tracking_properties['job_budget']      = $bid->getJob()->getBudget();
                $tracking_properties['bid_amount']      = $bid->getAmount();
                break;
            case CreditExpenseTransaction::SERVICE_FEATURE_FREELANCER:
                // Verify freelancer id
                $id = (int)($request->request->get('fid'));
                if($id <= 0){
                    return $this->responseError(401);
                }
                /* @var $freelancer Account */
                $freelancer = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $id));
                if(!$freelancer){ 
                    return $this->responseError(402);
                }
                if($freelancer->getId() != $payer->getId()){
                    return $this->responseError(403);
                }
                
                // Set feature freelancer until xx days
                $number_day = (int)($service['duration']);
                if($number_day <= 0){
                    return $this->responseError(404);
                }
                $featuredUntil = new \DateTime();
                $freelancer->setFeaturedUntil($featuredUntil->add(new \DateInterval('P' . $number_day . 'D')));
                $em->persist($freelancer);
                
                $credit_trans->setDetail(json_encode(array('freelancer_id' => $freelancer->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/feature_freelancer:confirm_credit_payer.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'duration'      => $service['duration'],
                        'fid'           => $freelancer->getId(),
                        'fname'         => $freelancer->getFullName()
                    );
                    
                // Set addtional tracking event properties
                $tracking_properties['freelancer_category'] = $freelancer->getCategory()->getTitle();
                $tracking_properties['freelancer_city']     = $freelancer->getCity()->getName();
                break;
            case CreditExpenseTransaction::SERVICE_CONTACT_FREELANCER:
                // Verify freelancer id
                $id = (int)($request->request->get('fid'));
                if($id <= 0){
                    return $this->responseError(501);
                }
                /* @var $freelancer Account */
                $freelancer = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $id));
                if(!$freelancer){ 
                    return $this->responseError(502);
                }
                
                $connection = new Connection();
                $connection->setOwner($payer);
                $connection->setContact($freelancer);
                $em->persist($connection);        
                
                $credit_trans->setDetail(json_encode(array('freelancer_id' => $freelancer->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/pay_contact:confirm_credit_payer.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'freelancer'    => $freelancer,
                        'fid'           => $freelancer->getId(),
                        'fname'         => $freelancer->getFullName()
                    );
                
                // Concerned person is the freelancer, he will get notification when someone contact
                $tpl_concerner = 'VlancePaymentBundle:Email/pay_contact:notify_freelancer.html.twig';
                $context_concerner = array(
                    'client'        => $payer,
                    'freelancer'    => $freelancer
                );
                $to_concerner = $freelancer->getEmail();
                    
                // Set addtional tracking event properties
                $tracking_properties['freelancer_category'] = $freelancer->getCategory()->getTitle();
                $tracking_properties['freelancer_city']     = $freelancer->getCity()->getName();
                $tracking_properties['payer_category']      = $payer->getCategory()->getTitle();
                $tracking_properties['payer_city']          = $payer->getCity()->getName();
                
                $success_data['freelancer'] = array(
                    'name'          => $freelancer->getFullName(),
                    'email'         => $freelancer->getEmail(),
                    'tel'           => $freelancer->getTelephone(),
                    'avatar_url'    => $freelancer->getFullPath() ? ResizeImage::resize_image($freelancer->getFullPath(), '99x99', 99, 99, 1) : "/img/unknown.png"
                );
                break;
            case CreditExpenseTransaction::SERVICE_CONTACT_CLIENT:
                // Verify client id
                $id = (int)($request->request->get('cid'));
                if($id <= 0){
                    return $this->responseError(501);
                }
                /* @var $client Account */
                $client = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $id));
                if(!$client){ 
                    return $this->responseError(502);
                }
                
                $connection = new Connection();
                $connection->setOwner($payer);
                $connection->setContact($client);
                $em->persist($connection);        
                
                $credit_trans->setDetail(json_encode(array('client_id' => $client->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/pay_contactclient:confirm_credit_payer.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'client'        => $client,
                        'cid'           => $client->getId(),
                        'cname'         => $client->getFullName()
                    );
                
                // Concerned person is the client, he will get notification when someone contact
                $tpl_concerner = 'VlancePaymentBundle:Email/pay_contactclient:notify_client.html.twig';
                $context_concerner = array(
                    'payer'     => $payer,
                    'client'    => $client
                );
                $to_concerner = $client->getEmail();
                    
                // Set addtional tracking event properties
                $tracking_properties['client_category'] = $client->getCategory()->getTitle();
                $tracking_properties['client_city']     = $client->getCity()->getName();
                $tracking_properties['payer_category']  = $payer->getCategory()->getTitle();
                $tracking_properties['payer_city']      = $payer->getCity()->getName();
                
                $success_data['client'] = array(
                    'name'          => $client->getFullName(),
                    'email'         => $client->getEmail(),
                    'tel'           => $client->getTelephone(),
                    'avatar_url'    => $client->getFullPath() ? ResizeImage::resize_image($client->getFullPath(), '99x99', 99, 99, 1) : "/img/unknown.png"
                );
                break;
            case CreditExpenseTransaction::SERVICE_FEATURE_JOB_FANPAGE:
                // Verify job id and job owner
                $jid = (int)($request->request->get('jid'));
                if($jid <= 0){
                    return $this->responseError(601);
                }
                /* @var $job Job */
                $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('id' => $jid));
                if(!$job){ 
                    return $this->responseError(602);
                }
                if($job->getAccount()->getId() != $payer->getId()){
                    return $this->responseError(603);
                }
                
                // Set feature job until xx days
                $number_day = (int)($service['duration']);
                if($number_day <= 0){
                    return $this->responseError(604);
                }
                $featuredUntil = new \DateTime();
                $job->setFeaturedUntil($featuredUntil->add(new \DateInterval('P' . $number_day . 'D')));
                $job->setFeaturedFb(true);
                $em->persist($job);
                
                $credit_trans->setDetail(json_encode(array('job_id' => $job->getId())));
                
                $tpl_payer = 'VlancePaymentBundle:Email/feature_job:confirm_credit_payer_fanpage.html.twig';
                    $context_payer = array(
                        'payer'         => $payer,
                        'amount'        => $service['amount'],
                        'jid'           => $job->getId(),
                        'jtitle'        => $job->getTitle()
                    );
                    
                // Set addtional tracking event properties
                $tracking_properties['job_category']    = $job->getCategory()->getTitle();
                $tracking_properties['job_budget']      = $job->getBudget();
                
                /* sent mail to admin */
                $context_ad = array(
                    'jid'       => $job->getId(),
                    'jtitle'    => $job->getTitle(),
                    'jurl'      => $this->generateUrl('job_show_freelance_job',array('hash' => $job->getHash()), true),
                );
                $tpl_ad = 'VlancePaymentBundle:Email/feature_job:admin_feature_fanpage.html.twig';

                try{
                    // Send confirmation mail to admin
                    $this->container->get('vlance.mailer')->sendTWIGMessageBySES($tpl_ad, $context_ad, $from_vlance, $service['email_fp_admin']);
                } catch (Exception $ex) {
                    return new JsonResponse(array('error' => 605));
                }
                break; 
            default:
                return $this->responseError(901);
        }
        $credit_trans->setPostBalance($credit_account->getBalance());
        $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
        
        $em->persist($credit_trans);
        $em->persist($credit_account);
        $em->flush();
        
        // Send tracking event to Mixpanel
        $this->get('vlance_base.helper')->trackAll($payer, $service['tracking_event'], $tracking_properties);
        
        // Send confirmation mail to payer
        
        if(isset($tpl_payer) && isset($context_payer) && isset($payer)){
            try {
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $payer->getEmail());
            } catch (Exception $ex) {
                return $this->responseError(902);
            }
        }
        
        // Send notification to to concerned person, if there is
        if(isset($tpl_concerner) && isset($context_concerner) && isset($to_concerner)){
            try {
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_concerner, $context_concerner, $from_vlance, $to_concerner);
            } catch (Exception $ex) {
                return $this->responseError(903);
            }
        } 
        return new JsonResponse($success_data);
    }

    protected function responseError($code, $msg = "")
    {
        error_log("Spend credit error #" . $code . ": " . $msg);
        return new JsonResponse(array('error' => $code, 'errorMsg' => ($msg == "" ? "Rất tiếc, đã có lỗi xảy ra. Vui lòng liên hệ vLance để được hỗ trợ." : $msg)));
    }
    
    /**
     * History of credit buying transaction
     *
     * @Route("/history/buy/{filters}", name="credit_history_buy", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlancePaymentBundle:Credit:history_buy.html.php",engine="php")
     */
    public function historyBuyAction($filters = '')
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $pager = null;
        try {
            $pager = $em->getRepository('VlancePaymentBundle:CreditBuyTransaction')->getPager($params, $acc_login->getId());
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        }
        catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', "Rất tiếc, đã có lỗi xảy ra. Vui lòng thử lại sau.");
            return $this->redirect($this->generateUrl('credit_history_buy'));
        }
        $pagerView = new DefaultView(new PagerTemplate($params, 'credit_history_buy'));
       
        return array(
            'pager' => $pager,
            'pagerView' => $pagerView,
            'entities' => $pager->getCurrentPageResults(),
        );
    }
    
    /**
     * List of credit spending transaction
     *
     * @Route("/history/spend/{filters}", name="credit_history_spend", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template("VlancePaymentBundle:Credit:history_spend.html.php",engine="php")
     */
    public function spendingAction($filters = '')
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $pager = null;
        try {
            $pager = $em->getRepository('VlancePaymentBundle:CreditExpenseTransaction')->getPager($params, $acc_login->getCredit()->getId());
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        }
        catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', "Rất tiếc, đã có lỗi xảy ra. Vui lòng thử lại sau.");
            return $this->redirect($this->generateUrl('credit_history_spend'));
        }
        $pagerView = new DefaultView(new PagerTemplate($params, 'credit_history_spend'));
       
        return array(
            'pager' => $pager,
            'pagerView' => $pagerView,
            'entities' => $pager->getCurrentPageResults(),
        );
    }
}
