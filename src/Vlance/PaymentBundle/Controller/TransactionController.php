<?php

namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Form\TransactionType;
use Vlance\JobBundle\Entity\Message;
use Pagerfanta\View\DefaultView;
use Vlance\BaseBundle\Paginator\View\Template\Template as PagerTemplate;
use Vlance\AccountBundle\Entity\Account;
use Vlance\PaymentBundle\Event\TransactionEvent;
use Vlance\BaseBundle\Event\VlanceEvents;

/**
 * Transaction controller.
 *
 * @Route("/trans")
 */
class TransactionController extends Controller
{
    
    /**
     * Client deposit to vlance account
     *
     * @Route("/deposit", name="transaction_deposit")
     * @Method("GET")
     * @Template()
     */
    public function depositAction(Request $request) {
        
    }
    
    /**
     * Client withdraw to his bank account
     *
     * @Route("/withdraw", name="transaction_withdraw")
     * @Method({"GET"})
     * @Template(engine="php")
     */
    public function withdrawAction(Request $request) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        
        if(is_object($acc)){
            $helper = $this->get('vlance_account.helper');
            if(!is_null($acc->getEmail())){
                if($helper->emailNotFacebook($acc->getEmail()) == false){
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('Email đăng ký của bạn không đúng. Hãy sửa lại.', array(), 'vlance'));
                    return $this->redirect($this->generateUrl('account_basic_edit', array('id' => $acc->getId())));
                }
            }
        }
        
        return array();
    }
    
    /**
     * Client withdraw to his bank account
     *
     * @Route("/withdraw/submit", name="transaction_withdraw_submit")
     * @Method({"POST"})
     * @Template(engine="php")
     */
    public function withdrawSubmitAction(Request $request) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        $user = $this->get('security.context')->getToken()->getUser();
        $user_cash = $user->getCash();
        
        if(!$user_cash){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transaction.withdraw.missing_usercash', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        
        if(!$request->request->get('userpaymentmethod')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transaction.withdraw.missing_payment_method', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        $em = $this->getDoctrine()->getManager();
        $paymentMethod = $em->getRepository('VlancePaymentBundle:UserPaymentMethod')->find($request->request->get('userpaymentmethod'));
        if (!$paymentMethod) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.userpaymentmethod.entitynotfound', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        if ($paymentMethod->getAccount() !== $user) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.userpaymentmethod.wronguser', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        
        if(!$request->request->get('amount')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transaction.withdraw.invalid_amount', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        $amount = $request->request->get('amount');
        //Allow only the amount is below the user cash balance - user cash balance in processing
        if ($amount > $user_cash->getBalance() + $user_cash->getProcessingBalance()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transaction.withdraw.invalid_amount', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }
        if($amount < 50000 || $amount > 20000000){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transaction.withdraw.invalid_amount', array(), 'vlance'));
            return $this->redirect($this->generateUrl('transaction_withdraw'));
        }

        //Set amount to processing in user cash
        $user_cash->setProcessingBalance($user_cash->getProcessingBalance() - $amount);
        $em->persist($user_cash);

        $trans = new Transaction();
        $trans->setReceiver($user);
        $trans->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
        $trans->setAmount($amount);
        $trans->setStatus(Transaction::TRANS_WAITING_PAYMENT);
        $trans->setType(Transaction::TRANS_TYPE_WITHDRAW);
        $trans->setComment($paymentMethod->getTitle() . "\n" . $paymentMethod->getInfo());
        $em->persist($trans);

        $em->flush();
        
        // Send message to admin
        $template = 'VlancePaymentBundle:Email:admin_withdraw_account.html.twig';
        $context = array(
            'transaction' => $trans,
        );
        $from = $this->container->getParameter('from_email');
        $to = $this->container->getParameter('to_email');

        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);

        // Send message to user
        $template_user = 'VlancePaymentBundle:Email:user_withdraw_account.html.twig';
        $context_user = array(
            'transaction' => $trans,
        );

        $this->get('vlance.mailer')->sendTWIGMessageBySES($template_user, $context_user, $from, $user->getEmail());

        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.transsaction.withdraw.created', array(), 'vlance'));
        return $this->redirect($this->generateUrl('transaction_withdraw'));
    }
    
    public function confirmWithdraw(Transaction $trans) {
        if ($trans->getType() !== Transaction::TRANS_TYPE_WITHDRAW || $trans->getStatus() !== Transaction::TRANS_TERMINATED) {
            return $this->redirect($this->generateUrl("500_page"), 301);
            throw new \Exception($this->get('translator')->trans('controller.transaction.confirmescrow.wrong.transaction', array(), 'vlance'), 500);
        }
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        $user = $this->get('security.context')->getToken()->getUser();
        
        if ($user->getType() !== Account::TYPE_VLANCE_SYSTEM || false === $this->get('security.context')->isGranted(Account::ROLE_VLANCE_SUPER_ADMIN)) {
            return $this->redirect($this->generateUrl("500_page"), 301);
            throw new \Exception($this->get('translator')->trans('transaction.escrow.wrong.user', array(), 'vlance'), 500);
        }
        
        $userCash = $trans->getReceiver()->getCash();
        $userCash->setProcessingBalance($userCash->getProcessingBalance() + $trans->getAmount());
        $userCash->setBalance($userCash->getBalance() - $trans->getAmount());
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($userCash);
        $em->flush();
    }
    
    /**
     * Client escrow to his project
     *
     * @Route("/escrow/{job_id}", name="transaction_escrow")
     * @Template()
     */
    public function escrowAction($job_id) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl("500_page"), 301);
            throw new \Exception($this->get('translator')->trans('controller.login.required', array(), 'vlance'), 500);
        }
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $user = $this->get('security.context')->getToken()->getUser();
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        $job_user = $job->getAccount();
        /* Check if current login user is owner of this job or vlance admin user */
        if ($user !== $job_user && false === $this->get('security.context')->isGranted(Account::ROLE_VLANCE_SUPER_ADMIN)) {
            return $this->redirect($this->generateUrl("500_page"), 301);
            throw new \Exception($this->get('translator')->trans('transaction.escrow.wrong.user', array(), 'vlance'), 500);
        }
        $user_cash = $job_user->getCash();
        
        if ($user_cash->getBalance() < $job->getBudgetBided()) {
            return new \Symfony\Component\HttpFoundation\Response(json_encode(array('error' => 1, 'message' => $this->get('translator')->trans('controller.transaction.escrow.fault'))));
        }
        
        //Check if this job is already escrowed
        $jobTrans = $job->getTransactions();
        foreach ($jobTrans as $trans) {
            if ($trans->getType() === Transaction::TRANS_TYPE_ESCROW) {
                return new \Symfony\Component\HttpFoundation\Response(json_encode(array('error' => 1, 'message' => $this->get('translator')->trans('controller.transaction.escrow.exist'))));
            }
        }
        //Decrease the balance in user cash of job owner
        $user_cash->setBalance($user_cash->getBalance() - $job->getBudgetBided());
        $em->persist($user_cash);

        //Create transaction of escrow type
        $escrow_trans = new Transaction();
        $escrow_trans->setType(Transaction::TRANS_TYPE_ESCROW);
        $escrow_trans->setSender($job_user);
        $escrow_trans->setAmount($job->getBudgetBided());
        $escrow_trans->setJob($job);
        $escrow_trans->setStatus(Transaction::TRANS_TERMINATED);
        $escrow_trans->setComment($this->get('translator')->trans('transaction.escrow.default_comment', array(), 'vlance'));
        $em->persist($escrow_trans);

        //Increase job cash
        $job_cash = $job->getCash();
        $job_cash->setBalance($job->getBudgetBided());
        $em->persist($job_cash);

        //Change job status to working & payment status to escrowed
        $job->setPaymentStatus(\Vlance\JobBundle\Entity\Job::ESCROWED);
        $job->setStatus(\Vlance\JobBundle\Entity\Job::JOB_WORKING);
        $em->persist($job);
        
        /**
         * @TODO create system message
         */
        //Create system message for freelancer to announce that job is escrowed
        $freelancer_message = new Message();
        $freelancer_message->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
        $freelancer_message->setReceiver($job->getWorker());
        $freelancer_message->setBid($em->getRepository('VlanceJobBundle:Job')->getAwardedBid($job));
        $freelancer_message->setContent($this->get('translator')->trans('message.escrow.freelancer', array(), 'vlance'));
        $freelancer_message->setStatus(Message::NEW_MESSAGE);
        $em->persist($freelancer_message);
        
        //Create system message for client to announce that job is escrowed
        $client_message = new Message();
        $client_message->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
        $client_message->setReceiver($job->getAccount());
        $client_message->setBid($em->getRepository('VlanceJobBundle:Job')->getAwardedBid($job));
        $client_message->setContent($this->get('translator')->trans('message.escrow.client', array('%project%' => $job->getTitle()), 'vlance'));
        $client_message->setStatus(Message::NEW_MESSAGE);
        $em->persist($client_message);
        
        $em->flush();
                    
        return new \Symfony\Component\HttpFoundation\Response(json_encode(array('error' => 0, 'message' => $this->get('translator')->trans('controller.transaction.escrow.success'))));
    }
    
    
    /**
     * Client Add escrow to his project
     *
     * @Route("/escrow_job_add/{job_id}/{amount}", name="transaction_escrow_job_add")
     * @Template()
     */
    public function addEscrowJobAction($job_id, $amount) {
        /* Validation */
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 10, 'message' => $this->container->get('translator')->trans('controller.login.required', array(), 'vlance')));
        }
        $auth_user  = $this->get('security.context')->getToken()->getUser(); /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        if(!is_object($auth_user)){
            return new JsonResponse(array('error' => 20, 'message' => $this->container->get('translator')->trans('controller.login.required', array(), 'vlance')));
        }
        
        $em = $this->getDoctrine()->getManager();        
        // Check job
        if(!isset($job_id)){
            return new JsonResponse(array('error' => 100, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.error.100_general', array(), 'vlance')));
        }
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id); /* @var $job \Vlance\JobBundle\Entity\Job */
        if(!$job){
            return new JsonResponse(array('error' => 100, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.error.100_general', array(), 'vlance')));
        }
        // Check amount
        if(!isset($amount)){
            return new JsonResponse(array('error' => 100, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.error.100_general', array(), 'vlance')));
        }
        if($amount < 0){
            return new JsonResponse(array('error' => 100, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.error.100_general', array(), 'vlance')));
        }
        
        // Check if current login user is owner of this job or vlance admin user
        if ($auth_user !== $job->getAccount() && false === $this->get('security.context')->isGranted(Account::ROLE_VLANCE_SUPER_ADMIN)) {
            return new JsonResponse(array('error' => 30, 'message' => $this->container->get('translator')->trans('transaction.escrow.wrong.user', array(), 'vlance')));
        }
        
        // Check if usercash enough
        $usercash = $job->getAccount()->getCash();
        if($usercash->getBalance() < $amount){
            return new JsonResponse(array('error' => 110, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.error.110_usercash_not_enough', array(), 'vlance')));
        }
        /* END Validation */

        //Create transaction of escrow type
        $escrow_trans = new Transaction();
        $escrow_trans->setType(Transaction::TRANS_TYPE_ESCROW);
        $escrow_trans->setSender($job->getAccount());
        $escrow_trans->setAmount($amount);
        $escrow_trans->setJob($job);
        $escrow_trans->setStatus(Transaction::TRANS_TERMINATED);
        $escrow_trans->setComment($this->container->get('translator')->trans('transaction.controller.addEscrowJob.comment_escrow_trans', array(), 'vlance'));
        $em->persist($escrow_trans);
        
        //Increase job cash
        $job_cash = $job->getCash();
        $job_cash->setBalance($job_cash->getBalance() + $amount);
        $em->persist($job_cash);
        
        $usercash->setBalance($usercash->getBalance() - $amount);
        $em->persist($usercash);
        $em->flush();
                    
        return new JsonResponse(array('error' => 0, 'message' => $this->container->get('translator')->trans('transaction.controller.addEscrowJob.success')));
    }
    
    /**
     * Display left menu for manager page
     *
     * @Route("/left", name="transaction_leftmenu")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function leftMenuAction(Request $request) {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new \Symfony\Component\HttpFoundation\Response();
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        
        $menus = array();
        $menus[] = array(
            'action' => 'account_basic_edit',
            'action_options' => array('id' => $acc->getId()),
            'class' => 'menu_general',
            'title' => $this->get('translator')->trans('transaction.menu.left.profile', array(), 'vlance'),
            'active' => (($request->get('routeName') == 'account_edit') 
                    || ($request->get('routeName') == 'account_basic_edit') 
                    || ($request->get('routeName') == 'account_portfolio_edit') 
                    || ($request->get('routeName') == 'account_verify_information')
                    || ($request->get('routeName') == 'portfolio_edit')));
        $menus[] = array(
            'action' => 'config_user',
            'class' => 'menu_general',
            'title' => $this->get('translator')->trans('transaction.menu.left.configuration', array(), 'vlance'),
            'active' => ($request->get('routeName') == 'config_user'));
        $menus[] = array(
            'action' => 'credit_balance',
            'class' => 'menu_general',
            'title' => $this->get('translator')->trans('transaction.menu.left.credit', array(), 'vlance'),
            'active' => (($request->get('routeName') == 'credit_history_buy') || ($request->get('routeName') == 'credit_balance') || ($request->get('routeName') == 'credit_history_spend')));
        $menus[] = array(
            'action' => 'email_invite',
            'class' => 'menu_invite_email',
            'title' => $this->get('translator')->trans('transaction.menu.left.emailinvite', array(), 'vlance'),
            'active' => ($request->get('routeName') == 'email_invite'));
        $menus[] = array(
            'action' => 'userpaymentmethod',
            'class' => 'menu_userpaymentmethod',
            'title' => $this->get('translator')->trans('transaction.menu.left.userpaymentmethod', array(), 'vlance'),
            'active' => ($request->get('routeName') == 'userpaymentmethod'));
        $menus[] = array(
            'action' => 'transaction',
            'class' => 'menu_transaction',
            'title' => $this->get('translator')->trans('transaction.menu.left.transaction', array(), 'vlance'),
            'active' => ($request->get('routeName') == 'transaction'));
        $menus[] = array(
            'action' => 'transaction_withdraw',
            'class' => 'menu_transaction_withdraw',
            'title' => $this->get('translator')->trans('transaction.menu.left.withdraw', array(), 'vlance'),
            'active' => ($request->get('routeName') == 'transaction_withdraw'));
        return array('menus' => $menus);
    }    
    
    /**
     * Lists all Transaction entities.
     *
     * @Route("/{filters}", name="transaction", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction($filters = '')
    {
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $params = $this->get('vlance_base.url')->pasteParameter($filters);
        $acc_login = $this->get('security.context')->getToken()->getUser();
        $params['acc'] = $acc_login->getId();
        
        $em = $this->getDoctrine()->getManager();
        $pager = null;
        try {
            $pager = $em->getRepository('VlancePaymentBundle:Transaction')->getPager($params);
        }
        catch (\Pagerfanta\Exception\NotIntegerMaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1MaxPerPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\NotIntegerCurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\LessThan1CurrentPageException $e) {
            
        }
        catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $e) {
            
        }
        catch (\Exception $e) {
//            echo $e->getMessage();die;
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        
        $pagerView = new DefaultView(new PagerTemplate($params, 'transaction'));
        $options = array('proximity' => 3);
        
        return array(
            'pager' => $pager,
            'pagerView' => $pagerView,
            'entities' => $pager->getCurrentPageResults(),
        );
    }
    
    
}
