<?php
namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\UnitOfWork;
use Pagerfanta\View\DefaultView;
use Symfony\Component\HttpFoundation\JsonResponse;

use Vlance\AccountBundle\Entity\Account;
use Vlance\BaseBundle\Utils\HasPermission;

use Vlance\PaymentBundle\Entity\CreditBuyTransaction;
use Vlance\PaymentBundle\Entity\Transaction;
use Vlance\PaymentBundle\Form\TransactionType;
use Vlance\PaymentBundle\Form\TopUpUserCashTransactionType;
use Vlance\PaymentBundle\Form\CreditBuyTransactionType;
use Vlance\PaymentBundle\Form\UserCashType;

/**
 * Admin Payment controller.
 *
 * @Route("/admin/payment")
 */
class AdminController extends Controller {
    /**
     * @Route("/credit/buy/{uid}", name="admin_buy_credit")
     * @Method("GET")
     * @Template("VlancePaymentBundle:Admin:dashboard/credit/buy_credit.html.php",engine="php")
     */
    public function buyCreditAction(Request $request, $uid) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("Không có quyền Admin");
        }

        if(!$uid){
            return new Response("Không thể xác định được ID của user");
        }
        
        $em = $this->getDoctrine()->getManager();
        $profile = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$profile){
            return new Response("Không thể tìm được profile của user");
        }
        
        $transaction = new CreditBuyTransaction();
        $form = $this->createForm(new CreditBuyTransactionType(), $transaction);
        
        return array(
            'uid'       => $uid,
            'form'      => $form->createView(),
            'referer'   => base64_encode($this->generateUrl('account_show_freelancer', array('hash' => $profile->getHash())))
        );
    }
    
    /**
     * @Route("/credit/buy/{uid}/submit/{referer}", name="admin_buy_credit_submit")
     * @Method("POST")
     * @Template("VlancePaymentBundle:Admin:dashboard/credit/buy_credit.html.php",engine="php")
     */
    public function buyCreditSubmitAction(Request $request, $uid, $referer = null) {
        $referer = base64_decode($referer == null ? $this->generateUrl('vlance_homepage', array()) : $referer);
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            $this->get('session')->getFlashBag()->add('error', "Không có quyền Admin");
            return $this->redirect($referer);
        }
        $admin_user = $this->get('security.context')->getToken()->getUser();
        
        if(!$uid){
            $this->get('session')->getFlashBag()->add('error', "Không thể xác định được ID của user");
            return $this->redirect($referer);
        }
        
        $em = $this->getDoctrine()->getManager();
        $payer = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$payer){
            $this->get('session')->getFlashBag()->add('error', "Không thể tìm được profile của user");
            return $this->redirect($referer);
        }

        $credit_trans = new CreditBuyTransaction();
        $form = $this->createForm('vlance_paymentbundle_creditbuytransactiontype', $credit_trans);
        $form->bind($request);
        
        if ($form->isValid()) {
            /* Add new payment transaction */
            $payment_transaction = new Transaction();
            $payment_transaction->setSender($admin_user);
            $payment_transaction->setReceiver($payer);
            $payment_transaction->setType(Transaction::TRANS_TYPE_BUY_CREDIT);
            $payment_transaction->setAmount($credit_trans->getAmount());
            $payment_transaction->setComment("Mua credit " . $credit_trans->getCredit());
            $payment_transaction->setStatus(Transaction::TRANS_TERMINATED);
            $em->persist($payment_transaction);
            
            $credit_trans->setPayer($payer);
            $credit_trans->setCreditAccount($payer->getCredit());
            $credit_trans->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
            $credit_trans->setPreBalance($payer->getCredit()->getBalance());
            $credit_trans->setPostBalance($payer->getCredit()->getBalance() + $credit_trans->getCredit());
            $em->persist($credit_trans);
            
            $credit_account = $payer->getCredit();
            $credit_account->setBalance($credit_account->getBalance() + $credit_trans->getCredit());
            $em->persist($credit_account);
            
            $em->flush();
            
            try{
                $from_vlance_payment = $this->container->getParameter('from_email_payment');
                
                // Send confirmation mail to beneficier
                $context_payer = array(
                    'transaction' => $credit_trans
                );
                
                switch($credit_trans->getType()){
                    case CreditBuyTransaction::TYPE_BUY:
                        $tpl_payer = 'VlancePaymentBundle:Email/buy_credit_admin_topup:type_buy.html.twig';
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance_payment, $payer->getEmail());

                        // Google Adwords
                        $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                            array('type' => 'html', 'html' =>
                                '<!-- Google Code for Buy Credit Conversion Page -->
                                <script type="text/javascript">
                                /* <![CDATA[ */
                                var google_conversion_id = 925105271;
                                var google_conversion_language = "en";
                                var google_conversion_format = "3";
                                var google_conversion_color = "ffffff";
                                var google_conversion_label = "JkdACOXlk2QQ9_iPuQM";
                                var google_conversion_value = '.$credit_trans->getAmount().';
                                var google_conversion_currency = "VND";
                                var google_remarketing_only = false;
                                /* ]]> */
                                </script>
                                <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                                </script>
                                <noscript>
                                <div style="display:inline;">
                                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$credit_trans->getAmount().'&amp;currency_code=VND&amp;label=JkdACOXlk2QQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                                </div>
                                </noscript>'
                            )
                        ));
                        break;
                    case CreditBuyTransaction::TYPE_REFUND:
                        $tpl_payer = 'VlancePaymentBundle:Email/buy_credit_admin_topup:type_refund.html.twig';
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance_payment, $payer->getEmail());
                        break;
                    case CreditBuyTransaction::TYPE_PROMO:
                    case CreditBuyTransaction::TYPE_PROMO_REFERRAL:
                        $tpl_payer = 'VlancePaymentBundle:Email/buy_credit_admin_topup:type_promo.html.twig';
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance_payment, $payer->getEmail());
                        break;
                }
            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', "Lỗi khi gửi mail");
                return $this->redirect($referer);
            }
            $em = $this->getDoctrine()->getManager();
        }
        
        $this->get('session')->getFlashBag()->add('success', "Đã thêm Credit thành công cho user");
        return $this->redirect($referer);
    }
    
    /**
     * @Route("/credit/list_creditbuytransaction/{uid}", name="admin_list_creditbuytransaction")
     * @Method("GET")
     * @Template("VlancePaymentBundle:Admin:dashboard/credit/list_creditbuytransaction.html.php",engine="php")
     */
    public function listCreditBuyTransactionAction(Request $request, $uid) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("Không có quyền Admin");
        }
        
        if(!$uid){
            return new Response("Không thể xác định được ID của user");
        }
        
        $em = $this->getDoctrine()->getManager();
        $payer = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$payer){
            $this->get('session')->getFlashBag()->add('error', "Không thể tìm được profile của user");
            return new Response("Không thể tìm được profile của user");
        }
        
        $transactions = $em->getRepository('VlancePaymentBundle:CreditBuyTransaction')->findByPayer($payer);
        
        return array(
            'uid'           => $uid,
            'transactions'  => $transactions
        );
    }
    
    /**
     * @Route("/usercash/topup/{uid}", name="admin_topup_usercash")
     * @Method("GET")
     * @Template("VlancePaymentBundle:Admin:dashboard/usercash/topup_usercash.html.php",engine="php")
     */
    public function topUpUserCashAction(Request $request, $uid) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("Không có quyền Admin");
        }

        if(!$uid){
            return new Response("Không thể xác định được ID của user");
        }
        
        $em = $this->getDoctrine()->getManager();
        $profile = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$profile){
            return new Response("Không thể tìm được profile của user");
        }
        
        /* @var $userCash \Vlance\PaymentBundle\Entity\UserCash */
        $userCash = $em->getRepository('VlancePaymentBundle:UserCash')->findOneBy(array('account' => $profile));
        if(!is_object($userCash)){
            return new Response("Không thể tìm được usercash của user");
        }
        
        $transaction = new Transaction();
        $form = $this->createForm(new TopUpUserCashTransactionType(), $transaction);
        
        return array(
            'uid'       => $uid,
            'form'      => $form->createView(),
            'referer'   => base64_encode($this->generateUrl('account_show_freelancer', array('hash' => $profile->getHash()))),
            'userCash'  => $userCash
        );
    }
    
    /**
     * @Route("/usercash/topup/{uid}/submit/{referer}", name="admin_topup_usercash_submit")
     * @Method("POST")
     * @Template("VlancePaymentBundle:Admin:dashboard/usercash/topup_usercash.html.php",engine="php")
     */
    public function topUpUserCashSubmitAction(Request $request, $uid, $referer = null) {
        $referer = base64_decode($referer == null ? $this->generateUrl('vlance_homepage', array()) : $referer);
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            $this->get('session')->getFlashBag()->add('error', "Không có quyền Admin");
            return $this->redirect($referer);
        }
        $admin_user = $this->get('security.context')->getToken()->getUser();
        
        if(!$uid){
            $this->get('session')->getFlashBag()->add('error', "Không thể xác định được ID của user");
            return $this->redirect($referer);
        }
        
        $em = $this->getDoctrine()->getManager();
        $payer = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$payer){
            $this->get('session')->getFlashBag()->add('error', "Không thể tìm được profile của user");
            return $this->redirect($referer);
        }
        
        $paymentMethod = $request->request->get('vlance_paymentbundle_topupusercashtransactiontype_paymentMethod');
        if($paymentMethod === ""){
            $this->get('session')->getFlashBag()->add('error', "Thiếu hình thức thanh toán");
            return $this->redirect($referer);
        }
        
        $tran = new Transaction();
        if($paymentMethod === '1'){
            $tran->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 1)));
        }else{
            $tran->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 2)));
        }
        $tran->setSender($admin_user);
        $tran->setReceiver($payer);
        $tran->setStatus(Transaction::TRANS_TERMINATED);
        $em->persist($tran);
        $form = $this->createForm('vlance_paymentbundle_topupusercashtransactiontype', $tran);
        $form->bind($request);
        if ($form->isValid()) {
            /* @var $userCash \Vlance\PaymentBundle\Entity\UserCash */
            $userCash = $em->getRepository('VlancePaymentBundle:UserCash')->findOneBy(array('account' => $payer));
            if(!is_object($userCash)){
                $this->get('session')->getFlashBag()->add('error', "User không có usercash.");
                return $this->redirect($referer);
            }
            
            $userCash->setBalance($userCash->getBalance() + $tran->getAmount());
            $em->persist($userCash);
            
            $em->flush();
            
            try{
                $from_vlance_payment = $this->container->getParameter('from_email_payment');
                
                $context_payer = array(
                    'payer'         => $payer,
                    'transaction'   => $tran,
                    'userCash'      => $userCash,
                );
                
                $tpl_payer = 'VlancePaymentBundle:Email/topup_usercash:topup_usercash_user.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance_payment, $payer->getEmail());

                $tpl_admin = 'VlancePaymentBundle:Email/topup_usercash:topup_usercash_admin.html.twig';
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_admin, $context_payer, $from_vlance_payment, 'tuan.tran@vlance.vn');
                
                // Google Adwords
                $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                        '<!-- Google Code for Buy Credit Conversion Page -->
                        <script type="text/javascript">
                        /* <![CDATA[ */
                        var google_conversion_id = 925105271;
                        var google_conversion_language = "en";
                        var google_conversion_format = "3";
                        var google_conversion_color = "ffffff";
                        var google_conversion_label = "JkdACOXlk2QQ9_iPuQM";
                        var google_conversion_value = '.$tran->getAmount().';
                        var google_conversion_currency = "VND";
                        var google_remarketing_only = false;
                        /* ]]> */
                        </script>
                        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                        </script>
                        <noscript>
                        <div style="display:inline;">
                        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$tran->getAmount().'&amp;currency_code=VND&amp;label=JkdACOXlk2QQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                        </div>
                        </noscript>'
                    )
                ));
            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', "Lỗi khi gửi mail");
                return $this->redirect($referer);
            }
            $this->get('session')->getFlashBag()->add('success', "Đã thêm nạp tiền thành công cho user");
            return $this->redirect($referer);
        } else {
            var_dump($form->getErrors());die;
        }

        $this->get('session')->getFlashBag()->add('error', "Nạp tiền không thành công cho user");
        return $this->redirect($referer);
    }
    
    /**
     * @Route("/usercash/list_topupusercashtransaction/{uid}", name="admin_list_topupusercashtransaction")
     * @Method("GET")
     * @Template("VlancePaymentBundle:Admin:dashboard/usercash/list_topuptransaction.html.php",engine="php")
     */
    public function listTopUpUserCashTransactionAction(Request $request, $uid) {
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            return new Response("Không có quyền Admin");
        }
        
        if(!$uid){
            return new Response("Không thể xác định được ID của user");
        }
        
        $em = $this->getDoctrine()->getManager();
        $payer = $em->getRepository('VlanceAccountBundle:Account')->findOneById($uid);
        if(!$payer){
            $this->get('session')->getFlashBag()->add('error', "Không thể tìm được profile của user");
            return new Response("Không thể tìm được profile của user");
        }
        
        $transactions = $em->getRepository('VlancePaymentBundle:Transaction')->findBy(array('receiver' => $payer, 'type' => Transaction::TRANS_TYPE_TOPUP_USERCASH));
        
        return array(
            'uid'           => $uid,
            'transactions'  => $transactions
        );
    }
}
