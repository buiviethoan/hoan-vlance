<?php

namespace Vlance\PaymentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Vlance\PaymentBundle\Entity\PaypalTransaction;
use Vlance\PaymentBundle\Entity\PaymentMethods\Paypal;
/**
 * Paypal controller.
 *
 * @Route("/trans")
 */
class PaypalController extends Controller
{
    
    /**
     * Displays a form to create a new Transaction entity.
     *
     * @Route("/paypal/approval", name="transaction_paypal_approval")
     * @Method("GET")
     */
    public function paypalApprovalAction(Request $request) {
        $token = $request->query->get('token');
        $payerId = $request->query->get('PayerID');
        $em = $this->getDoctrine()->getManager();
        /* @var $paypalTransaction \Vlance\PaymentBundle\Entity\PaypalTransaction */
        $paypalTransaction = $em->getRepository('VlancePaymentBundle:PaypalTransaction')->findOneBy(array('token' => $token));
        if (!$paypalTransaction) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        $paypalTransaction->setPayerId($payerId);
        
        $paymentMethod = new Paypal($this->container, $paypalTransaction->getTransaction());
        $paymentMethod->setPaypalTransaction($paypalTransaction);
        if ($paymentMethod->handleAccepted()) {
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.transsaction.paypal.success', array(), 'vlance'));
        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.transsaction.paypal.error', array(), 'vlance'));
        }
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $paypalTransaction->getTransaction()->getJob()->getHash())));
    }
    
    /**
     * Displays a form to create a new Transaction entity.
     *
     * @Route("/paypal/cancel", name="transaction_paypal_cancel")
     * @Method("GET")
     */
    public function paypalCancelAction(Request $request) {
        $token = $request->query->get('token');
        $em = $this->getDoctrine()->getManager();
        /* @var $paypalTransaction \Vlance\PaymentBundle\Entity\PaypalTransaction */
        $paypalTransaction = $em->getRepository('VlancePaymentBundle:PaypalTransaction')->findOneBy(array('token' => $token));
        if (!$paypalTransaction) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        $transaction = $paypalTransaction->getTransaction();
        $job = $transaction->getJob();
        $job->setPaymentStatus(\Vlance\JobBundle\Entity\Job::WAITING_ESCROW);
        $em->persist($job);
        $em->remove($transaction);
        $em->remove($paypalTransaction);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('controller.transsaction.paypal.cancel', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $paypalTransaction->getTransaction()->getJob()->getHash())));
    }
}
