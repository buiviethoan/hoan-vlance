<?php

namespace Vlance\PaymentBundle\Controller\AdminTransaction;
use Symfony\Component\Form\Form;
use Admingenerated\VlancePaymentBundle\BaseAdminTransactionController\EditController as BaseEditController;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Vlance\PaymentBundle\Entity\Transaction;

class EditController extends BaseEditController
{
    /**
     *
     * @param \Symfony\Component\Form\Form $form
     * @param \Vlance\PaymentBundle\Entity\Transaction $transaction
     */
    public function preSave(Form $form, Transaction $transaction)
    {
    }
    
    /**
     *
     * @param \Symfony\Component\Form\Form $form
     * @param \Vlance\PaymentBundle\Entity\Transaction $transaction
     */
    public function postSave(Form $form, Transaction $transaction) {
        try {
            $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            $bcc[] = $this->get('vlance_base.helper')->getParameter('vlance_email.from.hotro');
            $em = $this->getDoctrine()->getManager();
            $base_helper = $this->get('vlance_base.helper');
            
            // Admin confirm that received deposit for the project
            if($transaction->getType() == Transaction::TRANS_TYPE_DEPOSIT && $transaction->getStatus() == Transaction::TRANS_TERMINATED) {
                $job = $transaction->getJob();  /* @var $job Vlance\JobBundle\Entity\Job */
                $freelancer = $transaction->getJob()->getWorker();
                $client = $transaction->getJob()->getAccount();
                
                /* In DC job case, add more fund to the job */
                if($job->getTestType() == Job::TEST_TYPE_UPFRONT_ESCROW){
                    // Increase Usercash
                    $usercash = $client->getCash();
                    $usercash->setBalance($usercash->getBalance() + $transaction->getAmount());
                    $em->persist($usercash);
                    
                    // Mixpanel
                    $this->get('vlance_base.helper')->trackAll($client, "Job funded add");
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Google Code for Job paid Conversion Page -->
                            <script type="text/javascript">
                            /* <![CDATA[ */
                            var google_conversion_id = 925105271;
                            var google_conversion_language = "en";
                            var google_conversion_format = "3";
                            var google_conversion_color = "ffffff";
                            var google_conversion_label = "eJJVCK3Kx1oQ9_iPuQM";
                            var google_conversion_value = '.$transaction->getAmount().';
                            var google_conversion_currency = "VND";
                            var google_remarketing_only = false;
                            /* ]]> */
                            </script>
                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                            </script>
                            <noscript>
                            <div style="display:inline;">
                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$transaction->getAmount().'&amp;currency_code=VND&amp;label=eJJVCK3Kx1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                            </div>
                            </noscript>'
                        )
                    ));
                    
                    // Escrow cash from usercash to jobcash
                    $this->get('vlance.payment.controller.transaction')->setContainer($this->container);
                    $response = json_decode($this->get('vlance.payment.controller.transaction')->addEscrowJobAction($job->getId(), $transaction->getAmount()), true);
                    $jobcash = $job->getCash();
                    if($response['error'] == 0){
                         //Giao dịch chuyển vào ví vLance
                        $tran_topup = new Transaction();
                        $tran_topup->setAmount($transaction->getAmount());
                        $tran_topup->setComment('Nạp tiền vào ví vLance - ' . $transaction->getAmount() . '');
                        $tran_topup->setJob($transaction->getJob());
                        $tran_topup->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
                        $tran_topup->setReceiver($job->getAccount());
                        $tran_topup->setType(Transaction::TRANS_TYPE_TOPUP_USERCASH);
                        $tran_topup->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 1)));
                        $tran_topup->setStatus(Transaction::TRANS_TERMINATED);
                        $em->persist($tran_topup);

                        $em->flush();
                        
                        // TODO: Cần xử lý trong cả 3 trường hợp: ==, >, <
                        if($job->getBudgetBided() == $jobcash->getBalance()){ // Job get enough funded
                            // Change job's payment status
                            $job->setPaymentStatus(Job::ESCROWED);
                            $job->setStatus(Job::JOB_WORKING);
                            $em->persist($job);
                            $em->flush();
                            
                            // Send in-system notification
                            $exact_msg_client       = $base_helper->createSystemMessage($job->getAwardedBid(), $client,     $this->get('translator')->trans('controller.job.escrow.message.escrow_add_success_client',     array("%%add_amount%%" => $base_helper->formatCurrency($transaction->getAmount(), "VNĐ"), "%%total_amount%%" => $base_helper->formatCurrency($jobcash->getBalance(), "VNĐ")), 'vlance'));
                            $exact_msg_freelancer   = $base_helper->createSystemMessage($job->getAwardedBid(), $freelancer, $this->get('translator')->trans('controller.job.escrow.message.escrow_add_success_freelancer', array("%%add_amount%%" => $base_helper->formatCurrency($transaction->getAmount(), "VNĐ"), "%%total_amount%%" => $base_helper->formatCurrency($jobcash->getBalance(), "VNĐ")), 'vlance'));

                            $this->get('session')->getFlashBag()->add('success', "Job đã được nạp đầy đủ tiền");
                            
                            // Send confirm emails
                            if($form->get('sendEmail')->getData()) { // SendEmail option is checked
                                /* Gui email confirm voi khach hang */
                                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                                        "VlancePaymentBundle:Email/job_deposit_dc:confirm_add_deposit_and_funded_complete__client.html.twig", 
                                        array(
                                            'job'       => $job, 
                                            'client'    => $client
                                        ), 
                                        $email_hotro, 
                                        $client->getEmail()
                                );
                                
                                /* Gui email confirm toi freelancer */
                                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                                        "VlancePaymentBundle:Email/job_deposit_dc:confirm_add_deposit_and_funded_complete__freelancer.html.twig", 
                                        array(
                                            'job'       => $job,
                                            'freelancer'=> $freelancer
                                        ), 
                                        $email_hotro, 
                                        $freelancer->getEmail()
                                );
                                $this->get('session')->getFlashBag()->add('success', "Đã thông báo qua email");
                            }
                        }
                        
                        elseif($job->getBudgetBided() < $jobcash->getBalance()){
                            // Change job's payment status
                            $job->setPaymentStatus(Job::ESCROWED);
                            $job->setStatus(Job::JOB_WORKING);
                            $em->persist($job);
                            
                            // Refund to usercash
                            $refund_diff_trans = new Transaction();
                            $refund_diff_trans->setType(Transaction::TRANS_TYPE_REFUND);
                            $refund_diff_trans->setAmount($jobcash->getBalance() - $job->getBudgetBided());
                            $refund_diff_trans->setReceiver($client);
                            $refund_diff_trans->setJob($job);
                            $refund_diff_trans->setComment($this->get('translator')->trans('controller.job.escrow.transaction.comment_refund_different_job_cash', array(), 'vlance'));
                            $refund_diff_trans->setStatus(Transaction::TRANS_TERMINATED);
                            $em->persist($refund_diff_trans);
                            
                            $jobcash = $job->getCash();
                            $jobcash->setBalance($job->getBudgetBided());
                            $em->persist($jobcash);
                            
                            $user_cash = $client->getCash();
                            $user_cash->setBalance($user_cash->getBalance() + $refund_diff_trans->getAmount());
                            $em->persist($user_cash);
                
                            $em->flush();
                            
                            // Send in-system notification
                            $exact_msg_client               = $base_helper->createSystemMessage($job->getAwardedBid(), $client,     $this->get('translator')->trans('controller.job.escrow.message.escrow_add_success_client',      array("%%add_amount%%" => $base_helper->formatCurrency($transaction->getAmount(), "VNĐ"), "%%total_amount%%" => $base_helper->formatCurrency($jobcash->getBalance(), "VNĐ")), 'vlance'));
                            $refund_different_msg_client    = $base_helper->createSystemMessage($job->getAwardedBid(), $client,     $this->get('translator')->trans('controller.job.escrow.message.escrow_refund_different_client', array("%%refund_diff_amount%%" => $base_helper->formatCurrency($refund_diff_trans->getAmount(), "VNĐ")), 'vlance'));
                            $exact_msg_freelancer           = $base_helper->createSystemMessage($job->getAwardedBid(), $freelancer, $this->get('translator')->trans('controller.job.escrow.message.escrow_add_success_freelancer',  array("%%add_amount%%" => $base_helper->formatCurrency($transaction->getAmount(), "VNĐ"), "%%total_amount%%" => $base_helper->formatCurrency($jobcash->getBalance(), "VNĐ")), 'vlance'));
                
                            $this->get('session')->getFlashBag()->add('success', "Job đã được nạp đầy đủ tiền, chỗ tiền còn thừa lại của khách hàng đã được chuyển vào ví.");
                            
                            // Send confirm emails
                            if($form->get('sendEmail')->getData()) { // SendEmail option is checked
                                /* Gui email confirm voi khach hang */
                                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                                        "VlancePaymentBundle:Email/job_deposit_dc:confirm_add_deposit_and_funded_complete_with_partial_refund__client.html.twig", 
                                        array(
                                            'job'       => $job, 
                                            'refund'    => $refund_diff_trans->getAmount(),
                                            'client'    => $client
                                        ), 
                                        $email_hotro, 
                                        $client->getEmail()
                                );
                                
                                /* Gui email confirm toi freelancer */
                                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                                        "VlancePaymentBundle:Email/job_deposit_dc:confirm_add_deposit_and_funded_complete__freelancer.html.twig", 
                                        array(
                                            'job'       => $job,
                                            'freelancer'=> $freelancer
                                        ), 
                                        $email_hotro, 
                                        $freelancer->getEmail()
                                );
                                $this->get('session')->getFlashBag()->add('success', "Đã thông báo qua email");
                            }
                        }
                        
                        else{
                            /* Paypal */
                            $currencyRate   = (double)($base_helper->getParameter('vlance_system.currency_rate')    ? $base_helper->getParameter('vlance_system.currency_rate') : 0);
                            $paypalFee      = (double)($base_helper->getParameter('vlance_system.paypal_fee')       ? $base_helper->getParameter('vlance_system.paypal_fee')    : 0);
                            
                            // Add additional deposit transaction
                            $add_trans = new Transaction();
                            $add_trans->setType(Transaction::TRANS_TYPE_DEPOSIT);
                            $add_trans->setAmount($job->getAwardedBid()->getAmount() - $job->getCash()->getBalance());
                            $add_trans->setReceiver($client);
                            $add_trans->setJob($job);
                            $add_trans->setComment($this->get('translator')->trans('controller.job.escrow.transaction.comment_add_escrow_partial_client', array(), 'vlance'));
                            $add_trans->setStatus(Transaction::TRANS_WAITING_PAYMENT);
                            $em->persist($add_trans);
                            $em->flush();
                            
                            // Send in-system notification
                            $escrow_partial_msg_client = $base_helper->createSystemMessage($job->getAwardedBid(), $client, 
                                    $this->get('translator')->trans('controller.job.escrow.message.escrow_partial_client', array(
                                        "%%total_amount%%"  => $base_helper->formatCurrency($job->getAwardedBid()->getAmount(), "VNĐ"),
                                        "%%funded_amount%%" => $base_helper->formatCurrency($job->getCash()->getBalance(), "VNĐ"),
                                        "%%add_amount%%"    => $base_helper->formatCurrency($job->getAwardedBid()->getAmount() - $job->getCash()->getBalance(), "VNĐ")
                                    ), 'vlance') . "<br/>"
                                    . "<br/>"
                                    . $this->get('translator')->trans('controller.job.escrow.message.make_payment_guide', array(
                                        "%%jobId%%"         => $job->getId(),
                                        "%%currencyRate%%"  => number_format($currencyRate,0,',','.'),
                                        "%%paypalFee%%"     => number_format($paypalFee, 1,',','.'),
                                        "%%usdAmount%%"     => number_format($add_trans->getAmount() / $currencyRate / (1 - $paypalFee/100), 2,',','.')
                                    ), 'vlance')
                            );
                            
                            $this->get('session')->getFlashBag()->add('success', "Job đã được nạp thêm, nhưng vẫn chưa đủ. Đã thông báo để khách hàng nạp thêm.");
                            // Send email confirm job AWARDED but need to add more fund
                            if($form->get('sendEmail')->getData()) { // SendEmail option is checked
                            // To client
                                $this->get('vlance.mailer')->sendTWIGMessageBySES(
                                        "VlancePaymentBundle:Email/job_deposit_dc:confirm_add_deposit_need_add_fund__client.html.twig",
                                        array(
                                            'job'               => $job,
                                            'recent_fund'       => $transaction->getAmount(),
                                            'current_job_cash'  => $job->getCash()->getBalance(),
                                            'add_fund'          => $add_trans->getAmount(),
                                            'paypal'            => array(
                                                'currencyRate'      => $currencyRate,
                                                'paypalFee'         => $paypalFee
                                            ),
                                            'pixel_tracking'    => $base_helper->mixpanelTrackMail("Mail Open - confirm_add_deposit_need_add_fund__client", $client->getId())
                                        ),
                                        $email_hotro,
                                        $client->getEmail()
                                );
                                $this->get('session')->getFlashBag()->add('success', "Đã thông báo qua email");
                            }
                        }
                    } else{
                        if(isset($response['message'])){
                            $this->get('session')->getFlashBag()->add('error', $response['message']);
                        }
                    }
                }
                /* In normal (VC) job */
                else {
                    // Mixpanel
                    $this->get('vlance_base.helper')->trackAll($client, "Job funded");
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                        array('type' => 'html', 'html' =>
                            '<!-- Google Code for Job paid Conversion Page -->
                            <script type="text/javascript">
                            /* <![CDATA[ */
                            var google_conversion_id = 925105271;
                            var google_conversion_language = "en";
                            var google_conversion_format = "3";
                            var google_conversion_color = "ffffff";
                            var google_conversion_label = "eJJVCK3Kx1oQ9_iPuQM";
                            var google_conversion_value = '.$transaction->getAmount().';
                            var google_conversion_currency = "VND";
                            var google_remarketing_only = false;
                            /* ]]> */
                            </script>
                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                            </script>
                            <noscript>
                            <div style="display:inline;">
                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$transaction->getAmount().'&amp;currency_code=VND&amp;label=eJJVCK3Kx1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                            </div>
                            </noscript>'
                        )
                    ));

                    //Giao dịch chuyển vào ví vLance
                    $tran_topup = new Transaction();
                    $tran_topup->setAmount($transaction->getAmount());
                    $tran_topup->setComment('Nạp tiền vào ví vLance - ' . $transaction->getAmount() . '');
                    $tran_topup->setJob($transaction->getJob());
                    $tran_topup->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
                    $tran_topup->setReceiver($job->getAccount());
                    $tran_topup->setType(Transaction::TRANS_TYPE_TOPUP_USERCASH);
                    $tran_topup->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 1)));
                    $tran_topup->setStatus(Transaction::TRANS_TERMINATED);
                    $em->persist($tran_topup);
                    
                    /* update field numJobDeposit */
                    $client->setNumJobDeposit($client->getNumJobDeposit() + 1);
                    $em->persist($client);
                    $em->flush();

                    if($form->get('sendEmail')->getData()) {
                        /* Gui email thong bao freelancer*/
                        $email_context = array('job' => $job, 'transaction' => $transaction, 'freelancer' => $freelancer, 'client' => $client );
                        $template = "VlancePaymentBundle:Email/job_deposit_vc:confirm_deposited_freelancer.html.twig";
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $freelancer->getEmail(), $bcc);
                        /* Gui email confirm voi khach hang */
                        $email_context = array('job' => $job, 'transaction' => $transaction, 'freelancer' => $freelancer, 'client' => $client);
                        $template = "VlancePaymentBundle:Email/job_deposit_vc:confirm_deposited_client.html.twig";
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $client->getEmail());
                    }
                }
                
                //Check rate bid
                $this->get('vlance_job.helper')->checkBidRate($job);
            }
            /**
             *  Khách hàng nạp tiền dự án DC (nạp trước toàn bộ)
             * 1. (done) Update việc sang trạng thái đã nạp tiền
             * 2. (done) Thêm giao dịch escrow cho số tiền đã nạp
             * 3. (done) Update jobcash thành số tiền đã nạp
             */
            elseif($transaction->getType() == Transaction::TRANS_TYPE_DEPOSIT_UPFRONT && $transaction->getStatus() == Transaction::TRANS_TERMINATED){
                /* Sửa lại để deposit phải tăng  usercash của khách hàng, sau đó thì mới dùng các giao dịch escrow để nạp tiền cho dự án */
                /* 1. Update việc sang trạng thái đã nạp tiền
                 * 2. Tăng usercash của khách hàng */
                $job = $transaction->getJob();
                $job->setUpfrontAt(new \DateTime('now'));
                $em->persist($job);
                
                $client = $job->getAccount();
                $client->setNumJobDeposit($client->getNumJobDeposit() + 1);
                $em->persist($client);
                
                $usercash = $client->getCash();
                $usercash->setBalance($usercash->getBalance() + $transaction->getAmount());
                $em->persist($usercash);
                
                //Giao dịch chuyển vào ví vLance
                $tran_topup = new Transaction();
                $tran_topup->setAmount($transaction->getAmount());
                $tran_topup->setComment('Nạp tiền vào ví vLance - ' . $transaction->getAmount() . '');
                $tran_topup->setJob($transaction->getJob());
                $tran_topup->setSender($em->getRepository('VlanceAccountBundle:Account')->find(Account::USER_VLANCE_SYS));
                $tran_topup->setReceiver($job->getAccount());
                $tran_topup->setType(Transaction::TRANS_TYPE_TOPUP_USERCASH);
                $tran_topup->setPaymentMethod($em->getRepository('VlancePaymentBundle:PaymentMethod')->findOneBy(array('id' => 1)));
                $tran_topup->setStatus(Transaction::TRANS_TERMINATED);
                $em->persist($tran_topup);
                
                $em->flush();
                
                // Mixpanel
                $this->get('vlance_base.helper')->trackAll($client, "Job funded DC");
                $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                        '<!-- Google Code for Job funded Conversion Page -->
                        <script type="text/javascript">
                        /* <![CDATA[ */
                        var google_conversion_id = 925105271;
                        var google_conversion_language = "en";
                        var google_conversion_format = "3";
                        var google_conversion_color = "ffffff";
                        var google_conversion_label = "eJJVCK3Kx1oQ9_iPuQM";
                        var google_conversion_value = '.$transaction->getAmount().';
                        var google_conversion_currency = "VND";
                        var google_remarketing_only = false;
                        /* ]]> */
                        </script>
                        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                        </script>
                        <noscript>
                        <div style="display:inline;">
                        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$transaction->getAmount().'&amp;currency_code=VND&amp;label=eJJVCK3Kx1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                        </div>
                        </noscript>'
                    )
                ));
                
                /* 3. Update jobcash thành số tiền đã nạp */
                $this->get('vlance.payment.controller.transaction')->setContainer($this->container);
                $response = json_decode($this->get('vlance.payment.controller.transaction')->addEscrowJobAction($job->getId(), $transaction->getAmount()), true);
                if($response['error'] == 0){
                    if($form->get('sendEmail')->getData()) { // SendEmail option is checked
                        /* Gui email confirm voi khach hang */
                        $email_context = array('job' => $job, 'transaction' => $transaction, 'client' => $client);
                        //Chia trường hợp
                        if($job->getType() === Job::TYPE_BID){//Type: Bid
                            $template = "VlancePaymentBundle:Email/job_deposit_dc:confirm_upfront_client.html.twig";
                        } else {//Type: Contest
                            $template = "VlancePaymentBundle:Email/job_deposit_dc:confirm_upfront_client_contest.html.twig";
                        }
                        
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $client->getEmail());
                        $this->get('session')->getFlashBag()->add('success', "Đã thông báo qua email");
                    }
                } else{
                    if(isset($response['message'])){
                        $this->get('session')->getFlashBag()->add('error', $response['message']);
                    }
                }
                
                //Chuyển trạng thái khi khách hàng nạp tiền đăng dự thi
                if($job->getType() === Job::TYPE_CONTEST){
                    $helper = $this->get('vlance_payment.helper');
                    $now = new \DateTime('now');
                    
                    $job->setPaymentStatus(Job::ESCROWED);
                    //Set closeAt 5 days
                    $days = $helper->getParameter('contest.days.bid');
                    $close = ($now->modify('+'.$days.' days'));
                    $job->setCloseAt($close->setTime($now->format('H'), $now->format('i'), $now->format('s')));
                    $job->setValid(true); //Chuyển trạng thái của Job sang chế độ public khi đã đầy đủ điều kiện
                    $job->setPublish(Job::PUBLISH);
                    
                    // Mixpanel: Test chưa thấy hiện trên mixpanel
                    $this->get('vlance_base.helper')->trackAll($client, "Online contest");
                    $this->get('session')->getJSBag()->add('mixpanel', $this->renderView('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                    array('type' => 'html', 'html' =>
                            '<!-- Google Code for Job funded Conversion Page -->
                            <script type="text/javascript">
                            /* <![CDATA[ */
                            var google_conversion_id = 925105271;
                            var google_conversion_language = "en";
                            var google_conversion_format = "3";
                            var google_conversion_color = "ffffff";
                            var google_conversion_label = "eJJVCK3Kx1oQ9_iPuQM";
                            var google_conversion_value = '.$transaction->getAmount().';
                            var google_conversion_currency = "VND";
                            var google_remarketing_only = false;
                            /* ]]> */
                            </script>
                            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
                            </script>
                            <noscript>
                            <div style="display:inline;">
                            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/925105271/?value='.$transaction->getAmount().'&amp;currency_code=VND&amp;label=eJJVCK3Kx1oQ9_iPuQM&amp;guid=ON&amp;script=0"/>
                            </div>
                            </noscript>'
                        )
                    ));
                    
                    $em->persist($job);
                    $em->flush();
                }
                
                //Check rate bid
                if($job->getType() === Job::TYPE_BID){
                    $this->get('vlance_job.helper')->checkBidRate($job);
                }
            }

            // Freelancer rút tiền
            elseif($transaction->getType() == Transaction::TRANS_TYPE_WITHDRAW && $transaction->getStatus() == Transaction::TRANS_TERMINATED) {
                $receiver = $transaction->getReceiver();

                if($form->get('sendEmail')->getData()) {
                    /* Gui email thong bao freelancer*/
                    $email_context = array('transaction' => $transaction, 'acc' => $receiver );
                    $template = "VlancePaymentBundle:Email:payment_success_account.html.twig";
                    $file_attach = $transaction->getUploadDir(). DS .$transaction->getFilePath();

                    $array = array();
                    foreach(preg_split("/((\r?\n)|(\r\n?))/", $transaction->getComment()) as $line){
                        $array[] = $line;
                    }
                        
                    
                    if($transaction->getFilePath() == '' || !file_exists($file_attach)) {
                        $email_context = array('transaction' => $transaction, 'bank' => $array, 'acc' => $receiver, 'file_attach' => '' );
                        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $receiver->getEmail(), $bcc);
                    } else {
                        $email_context = array('transaction' => $transaction, 'bank' => $array, 'acc' => $receiver, 'file_attach' => 'Chi tiết, bạn vui lòng xem thông tin bên dưới và file đính kèm.' );
                        $this->get('vlance.mailer')->sendEmailAttachBySES($template, $email_context, $email_hotro, $receiver->getEmail(), $file_attach, $transaction->getFileName(), $bcc);
                    }
                }
            }

        } catch (Exception $ex) {
            /**
            * @todo Write log for email exception
            */
        }
    }
}