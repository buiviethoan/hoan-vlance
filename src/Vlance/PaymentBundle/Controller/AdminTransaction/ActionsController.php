<?php

namespace Vlance\PaymentBundle\Controller\AdminTransaction;

use Admingenerated\VlancePaymentBundle\BaseAdminTransactionController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    public function attemptObjectViewjob($id) {
        $transaction = $this->getObject($id);
        $job = $transaction->getJob();
        if($job){
            return $this->redirect($this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash())));
        } else {
            return $this->redirect($this->generateUrl('Vlance_PaymentBundle_AdminTransaction_list', array()));
        }
    }
}
