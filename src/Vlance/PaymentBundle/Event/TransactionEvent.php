<?php
namespace Vlance\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Vlance\PaymentBundle\Entity\Transaction;

class TransactionEvent extends Event {
    
    protected $_trans;
    protected $_request;
    protected $_response;

    public function __construct(Transaction $trans, Request $request) {
        $this->_trans = $trans;
        $this->_request = $request;
    }
    
    public function setResponse(Response $response) {
        $this->_response = $response;
    }
    
    /**
     * /!\ NOTE: If we need to redirect, the response has:
     * - status code : 302
     * - location | content = url to redirect
     * Or the response is an entity of RedirectResponse
     * 
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getResponse() {
        return $this->_response;
    }
    
    /**
     * 
     * @return \Symfony\Component\HttpFoundation\Response|null
     */
    public function getRequest() {
        return $this->_request;
    }
    
    /**
     * 
     * @return \Vlance\PaymentBundle\Entity\Transaction
     */
    public function getTransaction() {
        return $this->_trans;
    }
}
