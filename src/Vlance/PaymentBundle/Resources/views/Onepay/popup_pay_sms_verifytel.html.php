<?php /**
 * 
 * required input variables
 * $acc                         : current Account
 * $context_msg_path            : translation path (ex: "paysms.contact")
 * $form_name             (opt) : name of the form, avoid conflict
 * 
 */ ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php $service_id = OnePay::SERV_OTP_VERIFY_TEL; ?>
<?php $amount = $view['vlance_payment']->getParameter('payment_gateway.onepay.otp.' . $service_id . '.amount'); ?>

<?php // initializing text messages
$context_msg = array();
$context_msg['request']['service_name']  = $view['translator']->trans($context_msg_path . '.request.service_name', array(), 'vlance');
$context_msg['success']['success_msg_1'] = $view['translator']->trans($context_msg_path . '.success.success_msg_1', array(), 'vlance');
$context_msg['success']['success_msg_2'] = $view['translator']->trans($context_msg_path . '.success.success_msg_2', array(), 'vlance');

$message = array();
$message['request'] = array(
    'service_name' => ($context_msg['request']['service_name']) ? ($context_msg['request']['service_name']) : ''
);
$message['success'] = array(
    'success_msg_1' => ($context_msg['success']['success_msg_1']) ? ($context_msg['success']['success_msg_1']) : '',
    'success_msg_2' => ($context_msg['success']['success_msg_2']) ? ($context_msg['success']['success_msg_2']) : ''
);
    
$form_name = isset($form_name) ? $form_name : 'pay-sms';
$show_for_anonymous = isset($show_for_anonymous) ? $show_for_anonymous : true;
?>

<?php // Form for contact button when unauthenticated ?>
<div class="modal modal-new fade modal-paysms" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="redirect-spinner">
        <img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/>
        <p><?php echo $view['translator']->trans('paysms.redirect_spinner', array(), 'vlance') ?></p>
    </div>
    <div class="modal-body">
        <?php if(is_object($acc)):?>
        <div class="click-contact loading">
            <div class="pay-dialog pay-request">
                <h3 class="title"><?php echo $message['request']['service_name']; ?></h3>
                <?php $notif_start = new \DateTime("2016-08-04 00:00:00"); ?>
                <?php $notif_end   = new \DateTime("2016-08-08 00:00:00"); ?>
                <?php $now            = new \DateTime("now"); ?>
                <?php if($notif_start < $now && $now < $notif_end): ?>
                <p class="text-red">Lưu ý với thuê bao <b>Vinaphone:</b> Hiện tại cổng kết nối với nhà mạng đang quá tải. Nếu bạn gặp lỗi, vui lòng gọi cho chúng tôi theo số <b>024.6684.1818</b> để được xác thực SĐT. Mong các bạn thông cảm.</p>
                <?php endif; ?>
                <p><br/></p>
                <div class="progress-sms-wrapper">
                    <ul>
                        <li class="first active-sms">
                            <div class="number">1</div>
                            <div class="link"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">2</div>
                            <div class="link"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">3</div>
                        </li>
                    </ul>
                    <div class="row-fluid">
                        <div class="tally-sms done first span4"><?php echo $view['translator']->trans('paysms.pop_up.step1.payment_step1', array(), 'vlance') ?></div>
                        <div class="tally-sms span4"><?php echo $view['translator']->trans('paysms.pop_up.step2.payment_step2', array(), 'vlance') ?></div>
                        <div class="tally-sms last span4"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="telephone-sms span8">
                        <p><?php echo $view['translator']->trans('paysms.pop_up.step1.enter_number_phone', array(), 'vlance') ?></p>
                        <p><input id="sms-telephone" type="text" value="<?php echo $acc->getTelephone() ?>" placeholder="<?php echo $acc->getTelephone() ?>"></p>
                        <p class="button-group">
                            <a onclick="vtrack('Confirm telephone', {'case': 'verify-tel','answer':'YES','preview_otp':'YES'})" class="btn btn-primary btn-large yes"><?php echo $view['translator']->trans('paysms.pop_up.step1.button1', array(), 'vlance') ?></a>
                            <a onclick="vtrack('Confirm telephone', {'case': 'verify-tel','answer':'NO','preview_otp':'YES'});" class="btn btn-large no"><?php echo $view['translator']->trans('paysms.pop_up.step1.button2', array(), 'vlance') ?></a>
                        </p>
                    </div>
                    <div class="content-telephone span4">
                        <div class="note-telephone">
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step1.guide1', array("%%service_name%%" => $message['request']['service_name'],"%%amount%%" => "<span>" .number_format($amount,0,',','.')."</span>"), 'vlance') ?></p>
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step1.guide4', array(), 'vlance') ?></p>
                            <img src="/img/viettel_mobi_vina.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="pay-dialog pay-confirm" style="display:none;">
                <h3 class="title"><?php echo $view['translator']->trans('paysms.pop_up.title', array(), 'vlance') ?> <?php echo $message['request']['service_name']; ?></h3>
                <div class="progress-sms-wrapper">
                    <ul>
                        <li class="first active-sms">
                            <div class="number">1</div>
                            <div class="link"></div>
                        </li>
                        <li class="active-sms">
                            <div class="number">2</div>
                            <div class="link-after"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">3</div>
                        </li>
                    </ul>
                    <div class="row-fluid">
                        <div class="tally-sms done first span4"><?php echo $view['translator']->trans('paysms.pop_up.step1.payment_step1', array(), 'vlance') ?></div>
                        <div class="tally-sms done span4"><?php echo $view['translator']->trans('paysms.pop_up.step2.payment_step2', array(), 'vlance') ?></div>
                        <div class="tally-sms last span4"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="telephone-sms span8">
                        <p><?php echo $view['translator']->trans('paysms.pop_up.step2.enter_code', array(), 'vlance') ?></p>
                <p><input id="sms-confirm-code" type="text" placeholder="<?php echo $view['translator']->trans('paysms.pop_up.step2.code_mobile_sms', array(), 'vlance') ?>"></p>
                <p class="button-group">
                    <a onclick="vtrack('Confirm pay', {'case': 'verify-tel','answer':'YES'})" class="btn btn-primary btn-large yes"><?php echo $view['translator']->trans('paysms.pop_up.step1.button3', array(), 'vlance') ?></a>
                    <a onclick="vtrack('Confirm pay', {'case': 'verify-tel','answer':'NO' });" class="btn btn-large no"><?php echo $view['translator']->trans('paysms.pop_up.step1.button2', array(), 'vlance') ?></a>
                </p>
                <p class="deposit"><?php echo number_format($amount,0,',','.');?> <?php echo $view['translator']->trans('paysms.pop_up.step2.deposit', array(), 'vlance') ?></p>
            </div>
                    <div class="content-telephone span4">
                        <div class="note-telephone">
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step2.guide1', array(), 'vlance') ?> <span><?php echo $acc->getTelephone() ?></span> <?php echo $view['translator']->trans('paysms.pop_up.step2.guide2', array(), 'vlance') ?></p>
                            <i>"<?php echo $view['translator']->trans('paysms.pop_up.step2.guide3', array(), 'vlance') ?> <?php echo $amount; ?> <?php echo $view['translator']->trans('paysms.pop_up.step2.guide4', array(), 'vlance') ?>"</i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pay-dialog pay-result" style="display:none;">
                <div class="title"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?> <img src="<?php echo $view['assets']->getUrl('img/icon_success-01.png') ?>"/>
                </div>
                <p class="first"><?php echo $message['success']['success_msg_1'] //Thông tin liên hệ của freelancer đã được gửi cho bạn qua email. ?></p>
                <p><?php echo $message['success']['success_msg_2'] //Chúc bạn và freelancer có những dự án thành công! ?></p>
                <p class="button-group">
                    <a class="btn btn-large no"><?php echo $view['translator']->trans('paysms.pop_up.step3.button1', array(), 'vlance') ?></a>
                </p>
            </div>
            <div class="pay-dialog pay-error" style="display:none;">
                <p class="first"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.first', array(), 'vlance') ?><br/><br/></p>
                <p class="error-reason"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.reason', array(), 'vlance') ?> <span></span> <br/><br/></p>
                <p><?php echo $view['translator']->trans('paysms.pop_up.pay_error.guide1', array(), 'vlance') ?><br/></p>
                <p><?php echo $view['translator']->trans('paysms.pop_up.pay_error.guide2', array(), 'vlance') ?></p>
                <p class="button-group">
                    <a class="btn btn-primary btn-large retry"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.button', array(), 'vlance') ?></a>
                    <a class="btn btn-large no"><?php echo $view['translator']->trans('paysms.pop_up.step3.button1', array(), 'vlance') ?></a>
                </p>
            </div>

            <script type="text/javascript">
                var form_name_verifytelephone = "#<?php echo $form_name ?>";
                $(document).ready(function($){
                    $('[data-toggle="modal"]').on('click', function() {
                        $($(this).attr('href')).data('request', $(this).data('request'));
                        $($(this).attr('href')).data('confirm', null);
                        $($(this).attr('href')).data('urlRequest', $(this).data('urlRequest'));
                        $($(this).attr('href')).data('urlConfirm', $(this).data('urlConfirm'));
                    });
                    
                    $(form_name_verifytelephone).on('show', function(e){
                        $(this).find('.click-contact .pay-dialog').hide();
                        $(this).find('.click-contact .pay-request').show();
                    });
                    
                    $(form_name_verifytelephone + ' .click-contact .pay-request a.yes').click(function(){
                        $(form_name_verifytelephone).data('request').telephone = $(form_name_verifytelephone + ' #sms-telephone').val();
                        $(form_name_verifytelephone + " .pay-confirm .note-telephone p span").text($(form_name_verifytelephone + ' #sms-telephone').val());
                        // Check Operator
                        var operator = getOperator($(form_name_verifytelephone + ' #sms-telephone').val());
                        if (operator == 3){  // VINAPHONE
                            $(form_name_verifytelephone).addClass('redirecting');
                            setTimeout(function(){
                                $.ajax({
                                    url: $(form_name_verifytelephone).data('urlRequest'),
                                    type: "POST",
                                    data: {
                                        data_request: $(form_name_verifytelephone).data('request'),
                                    }
                                }).done(function(result){
                                    $(form_name_verifytelephone).removeClass('loading');
                                    if(result.error == 0 && result.response.errorCode === "18" && result.response.redirectUrl){ // Đã gửi OTP cho khách hàng.
                                        $(form_name_verifytelephone).data('confirm', $(form_name_verifytelephone).data('request'));
                                        $(form_name_verifytelephone).data('confirm').tid = result.id;
                                        vtrack('Confirm telephone status', {'case': 'verify-tel','status': 'SUCCESS'});
                                        window.location.href = result.response.redirectUrl;
                                    }
                                    else{
                                        $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                                        error_show(result);
                                        $(form_name_verifytelephone + ' .pay-dialog.pay-error .btn-primary').addClass('request');
                                        vtrack('Confirm telephone status', {'case': 'verify-tel','status': 'FAIL'});
                                    }
                                });
                            }, 4000);
                        } else {
                            $(form_name_verifytelephone).addClass('loading');
                            $.ajax({
                                url: $(form_name_verifytelephone).data('urlRequest'),
                                type: "POST",
                                data: {
                                    data_request: $(form_name_verifytelephone).data('request'),
                                }
                            }).done(function(result){
                                $(form_name_verifytelephone).removeClass('loading');
                                if(result.error == 0 && result.response.errorCode === "18"){ // Đã gửi OTP cho khách hàng.
                                    $(form_name_verifytelephone).data('confirm', $(form_name_verifytelephone).data('request'));
                                    $(form_name_verifytelephone).data('confirm').tid = result.id;
                                    vtrack('Confirm telephone status', {'case': 'verify-tel','status': 'SUCCESS'}); //UPDATE
                                }
                                else{
                                    $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                                    error_show(result);
                                    $(form_name_verifytelephone + ' .pay-dialog.pay-error .btn-primary').addClass('request');
                                    vtrack('Confirm telephone status', {'case': 'verify-tel','status': 'FAIL'}); //UPDATE
                                }
                            });
                            $(form_name_verifytelephone + ' .click-contact .pay-confirm .first strong').html($('#sms-telephone').val());
                            $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                            $(form_name_verifytelephone + ' .click-contact .pay-confirm').show();
                        }
                    });
                    
                    $(form_name_verifytelephone + ' .click-contact .pay-confirm a.yes').click(function(){
                        $(form_name_verifytelephone).addClass('loading');
                        $(form_name_verifytelephone).data('confirm').otp = $(form_name_verifytelephone + ' #sms-confirm-code').val();
                        $.ajax({
                            url: $(form_name_verifytelephone).data('urlConfirm'),
                            type: "POST",
                            data: {
                                data_confirm: $(form_name_verifytelephone).data('confirm')
                            }
                        }).done(function(result){
                            $(form_name_verifytelephone).removeClass('loading');
                            if(result.error == 0 && result.response.errorCode === "00"){ // Success
                                $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                                $(form_name_verifytelephone + ' .click-contact .pay-result').show();
                                vtrack('Confirm pay status', {'case': 'verify-tel','status': 'SUCCESS'});
                            }
                            else{
                                $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                                error_show(result);
                                vtrack('Confirm pay status', {'case': 'verify-tel','status': 'FAIL'});
                            }
                        });
                        $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                        $(form_name_verifytelephone + ' .click-contact .pay-confirm').show();
                    });
                    
                    $(form_name_verifytelephone + ' .pay-dialog.pay-error .btn-primary').click(function(){
                        error_hide();
                        $(form_name_verifytelephone + ' .click-contact .pay-error .retry').hide();
                        $(form_name_verifytelephone + ' #sms-confirm-code').val('');
                        $(form_name_verifytelephone + ' .click-contact .pay-dialog').hide();
                        $(form_name_verifytelephone + ' .click-contact .pay-request').show();
                        $(form_name_verifytelephone + ' .pay-dialog.pay-error .btn-primary').removeClass('request');
                    });
                    
                        // Hide popup
                    $(form_name_verifytelephone + ' .click-contact a.no').click(function(){
                        $(form_name_verifytelephone).modal('hide');
                        $(form_name_verifytelephone + ' #sms-confirm-code').removeAttr('tid');
                        $(form_name_verifytelephone + ' #sms-confirm-code').removeAttr('fid');
                        $(form_name_verifytelephone + ' #sms-confirm-code').val('');
                        error_hide();
                    });
                    
                    $(form_name_verifytelephone + ' .pay-dialog.pay-result .btn.no').click(function(){
                        window.location.href = "<?php echo $view['router']->generate('account_show_freelancer', array('hash' => $acc->getHash())); ?>"
                    });
                    
                    // Show error popup
                    function error_show(response){
                        $(form_name_verifytelephone + ' .click-contact .pay-error .error-reason').hide();
                        if(response.errorMessage !== ""){
                            $(form_name_verifytelephone + ' .click-contact .pay-error .error-reason span').html(response.errorMessage);
                            $(form_name_verifytelephone + ' .click-contact .pay-error .error-reason').show();
                        }
                        $(form_name_verifytelephone + ' .click-contact .pay-error').show();
                        if(response.errorCode == "10" || response.errorCode == "11"){ // Incorrect OTP or insufficient balance
                            $(form_name_verifytelephone + ' .click-contact .pay-error .retry').show();
                        }
                    }

                    // Hide error popup
                    function error_hide(){
                        $(form_name_verifytelephone + ' .click-contact .pay-error .error-reason').hide();
                        $(form_name_verifytelephone + ' .click-contact .pay-error').hide();
                    }
                });
                
            </script>
        </div>
        <?php endif;?>
    </div>
</div>