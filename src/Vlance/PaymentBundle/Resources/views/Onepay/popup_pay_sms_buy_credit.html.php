<?php /**
 * 
 * required input variables
 * $acc                         : current Account
 * $context_msg_path            : translation path (ex: "paysms.contact")
 * $form_name             (opt) : name of the form, avoid conflict
 * $show_for_anonymous    (opt) : show for anonymous (default = true)
 * $pay_id                (opt) : 
 * $pay_content           (opt) : 
 * 
 */ ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php $service_id = OnePay::SERV_OTP_BUY_CREDIT; ?>
<?php $amount = $view['vlance']->getParameter('payment_gateway.onepay.otp.' . $service_id . '.amount'); ?>

<?php // initializing text messages
$context_msg = array();
$context_msg['request']['service_name']  = $view['translator']->trans($context_msg_path . '.request.service_name', array(), 'vlance');
$context_msg['success']['success_msg_1'] = $view['translator']->trans($context_msg_path . '.success.success_msg_1', array(), 'vlance');
$context_msg['success']['success_msg_2'] = $view['translator']->trans($context_msg_path . '.success.success_msg_2', array(), 'vlance');

$message = array();
$message['request'] = array(
    'service_name' => ($context_msg['request']['service_name']) ? ($context_msg['request']['service_name']) : ''
);
$message['success'] = array(
    'success_msg_1' => ($context_msg['success']['success_msg_1']) ? ($context_msg['success']['success_msg_1']) : '',
    'success_msg_2' => ($context_msg['success']['success_msg_2']) ? ($context_msg['success']['success_msg_2']) : ''
);
    
$form_name = isset($form_name) ? $form_name : 'pay-sms';
$show_for_anonymous = isset($show_for_anonymous) ? $show_for_anonymous : true;
?>

<?php // Form for contact button when unauthenticated ?>
<div class="modal modal-new fade modal-paysms" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="redirect-spinner">
        <img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/>
        <p><?php echo $view['translator']->trans('paysms.redirect_spinner', array(), 'vlance') ?></p>
    </div>
    <div class="modal-body">
        <div class="click-contact loading">
            <div class="pay-dialog pay-request">
                <h3 class="title"><?php echo $view['translator']->trans('paysms.pop_up.title', array(), 'vlance') ?> <b><?php echo $message['request']['service_name'] ?></b></h3>
                <?php $notif_start = new \DateTime("2016-08-04 00:00:00"); ?>
                <?php $notif_end   = new \DateTime("2016-08-08 00:00:00"); ?>
                <?php $now            = new \DateTime("now"); ?>
                <?php if($notif_start < $now && $now < $notif_end): ?>
                <p class="text-red">Lưu ý với thuê bao <b>Vinaphone:</b> Hiện tại cổng kết nối với nhà mạng Vinaphone đang quá tải. Nếu bạn gặp lỗi, vui lòng thanh toán theo hình thức khác. Mong các bạn thông cảm.</p>
                <?php endif; ?>
                <div class="progress-sms-wrapper">
                    <ul>
                        <li class="first active-sms">
                            <div class="number">1</div>
                            <div class="link"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">2</div>
                            <div class="link"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">3</div>
                        </li>
                    </ul>
                    <div class="row-fluid">
                        <div class="tally-sms done first span4"><?php echo $view['translator']->trans('paysms.pop_up.step1.payment_step1', array(), 'vlance') ?></div>
                        <div class="tally-sms span4"><?php echo $view['translator']->trans('paysms.pop_up.step2.payment_step2', array(), 'vlance') ?></div>
                        <div class="tally-sms last span4"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="telephone-sms span8">
                        <p><?php echo $view['translator']->trans('paysms.pop_up.step1.enter_number_phone', array(), 'vlance') ?></p>
                        <p><input id="sms-telephone" type="text" value="<?php echo $acc->getTelephone() ?>" placeholder="<?php echo $acc->getTelephone() ?>"></p>
                        <p class="button-group">
                            <a onclick="vtrack('Confirm telephone', {'case': 'pay-buy-credit','answer':'YES','preview_otp':'YES'})" class="btn btn-primary btn-large yes"><?php echo $view['translator']->trans('paysms.pop_up.step1.button1', array(), 'vlance') ?></a>
                            <a onclick="vtrack('Confirm telephone', {'case': 'pay-buy-credit','answer':'NO','preview_otp':'YES'});" class="btn btn-large no" data-dismiss="modal"><?php echo $view['translator']->trans('paysms.pop_up.step1.button2', array(), 'vlance') ?></a>
                        </p>
                    </div>
                    <div class="content-telephone span4">
                        <div class="note-telephone">
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step1.guide1', array("%%service_name%%" => $message['request']['service_name'],"%%amount%%" => "<span class=\"amount\">" .number_format($amount,0,',','.')."</span>"), 'vlance') ?></p>
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step1.guide4', array(), 'vlance') ?></p>
                            <img src="/img/viettel_mobi_vina.png">
                        </div>
                    </div>
                </div>
            </div>
            <div class="pay-dialog pay-confirm" style="display:none;">
                <h3 class="title"><?php echo $view['translator']->trans('paysms.pop_up.title', array(), 'vlance') ?> <b><?php echo $message['request']['service_name'] ?></b></h3>
                <p><br/></p>
                <div class="progress-sms-wrapper">
                    <ul>
                        <li class="first active-sms">
                            <div class="number">1</div>
                            <div class="link"></div>
                        </li>
                        <li class="active-sms">
                            <div class="number">2</div>
                            <div class="link-after"></div>
                        </li>
                        <li class="unactive-sms">
                            <div class="number">3</div>
                        </li>
                    </ul>
                    <div class="row-fluid">
                        <div class="tally-sms done first span4"><?php echo $view['translator']->trans('paysms.pop_up.step1.payment_step1', array(), 'vlance') ?></div>
                        <div class="tally-sms done span4"><?php echo $view['translator']->trans('paysms.pop_up.step2.payment_step2', array(), 'vlance') ?></div>
                        <div class="tally-sms last span4"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?></div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="telephone-sms span8">
                        <p><?php echo $view['translator']->trans('paysms.pop_up.step2.enter_code', array(), 'vlance') ?></p>
                        <p><input id="sms-confirm-code" type="text" placeholder="Nhập mã xác nhận"></p>
                        <p class="button-group">
                            <a onclick="vtrack('Confirm pay', {'case': 'pay-buy-credit','answer':'YES'})" class="btn btn-primary btn-large yes"><?php echo $view['translator']->trans('paysms.pop_up.step1.button3', array(), 'vlance') ?></a>
                            <a onclick="vtrack('Confirm pay', {'case': 'pay-buy-credit','answer':'NO' });" class="btn btn-large no" data-dismiss="modal"><?php echo $view['translator']->trans('paysms.pop_up.step1.button2', array(), 'vlance') ?></a>
                        </p>
                        <p class="deposit"><span class="amount"><?php echo number_format($amount,0,',','.');?></span> <?php echo $view['translator']->trans('paysms.pop_up.step2.deposit', array(), 'vlance') ?></p>
                    </div>
                    <div class="content-telephone span4">
                        <div class="note-telephone">
                            <p><?php echo $view['translator']->trans('paysms.pop_up.step2.guide1', array(), 'vlance') ?> <span><?php echo $acc->getTelephone() ?></span> <?php echo $view['translator']->trans('paysms.pop_up.step2.guide2', array(), 'vlance') ?></p>
                            <i>"<?php echo $view['translator']->trans('paysms.pop_up.step2.guide3', array(), 'vlance') ?> <span class="amount"><?php echo $amount; ?></span> <?php echo $view['translator']->trans('paysms.pop_up.step2.guide4', array(), 'vlance') ?>"</i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pay-dialog pay-result" style="display:none;">
                <div class="title"><?php echo $view['translator']->trans('paysms.pop_up.step3.payment_step3', array(), 'vlance') ?> <img src="<?php echo $view['assets']->getUrl('img/icon_success-01.png') ?>"/></div>
                <p class="first"><?php echo $message['success']['success_msg_1'] //Thông tin liên hệ của freelancer đã được gửi cho bạn qua email. ?></p>
                <p><?php echo $message['success']['success_msg_2'] //Chúc bạn và freelancer có những dự án thành công! ?></p>
                <p class="button-group">
                    <a class="btn btn-large no" data-dismiss="modal"><?php echo $view['translator']->trans('paysms.pop_up.step3.button1', array(), 'vlance') ?></a>
                </p>
            </div>
            <div class="pay-dialog pay-error" style="display:none;">
                <p class="first"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.first', array(), 'vlance') ?><br/><br/></p>
                <p class="error-reason"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.reason', array(), 'vlance') ?> <span></span> <br/><br/></p>
                <p><?php echo $view['translator']->trans('paysms.pop_up.pay_error.guide1', array(), 'vlance') ?><br/></p>
                <p><?php echo $view['translator']->trans('paysms.pop_up.pay_error.guide2', array(), 'vlance') ?></p>
                <p class="button-group">
                    <a class="btn btn-primary btn-large retry"><?php echo $view['translator']->trans('paysms.pop_up.pay_error.button', array(), 'vlance') ?></a>
                    <a class="btn btn-large no" data-dismiss="modal"><?php echo $view['translator']->trans('paysms.pop_up.step3.button1', array(), 'vlance') ?></a>
                </p>
            </div>

            <script type="text/javascript">
                var form_name_buy_credit = "#<?php echo $form_name ?>";
                var creditPackage = <?php echo json_encode($packages); ?>
            </script>
        </div>
    </div>
</div>