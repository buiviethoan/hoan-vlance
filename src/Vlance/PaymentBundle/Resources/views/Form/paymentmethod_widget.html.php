<select <?php echo $view['form']->block($form, 'widget_attributes') ?>>
        <?php if (!is_null($empty_value)) :?>
    <option <?php if ($required) :?> disabled="disabled" <?php if (is_null($value)) :?> selected="selected"<?php endif;?><?php else:?> value=""<?php endif;?>><?php echo $view['translator']->trans($empty_value, array(), 'vlance');?></option>
        <?php endif;?>
        <?php foreach ($choices as $idx => $method) :?>
    <option value="<?php echo $method->value?>" data-info="<?php echo nl2br($view['translator']->trans($method->data->getInfo(), array(), 'vlance'));?>"><?php echo $method->label?></option>
        <?php endforeach;?>
</select>
<div id="<?php echo $id?>_payment_info" style="margin-bottom: 10px;">
</div>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#<?php echo $id?>_payment_info').html(jQuery(this).find(":selected").data('info'));
        jQuery('#<?php echo $id?>').change(function(){
            jQuery('#<?php echo $id?>_payment_info').html(jQuery(this).find(":selected").data('info'));
        });
    });
</script>