<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('transaction_withdraw.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<?php
/* @var $user \Vlance\AccountBundle\Entity\Account */
$user = $app->getUser();
?>

<?php $paymentMethods = $user->getPaymentMethods(); ?>
<?php $is_withdrawable = ($user->getCash()->getBalance() + $user->getCash()->getProcessingBalance()) > 0 ? true : false; ?>

<?php /* Trang list transaction */ ?>
<div class="row-fluid">
    <h1><?php echo $view['translator']->trans('transaction_withdraw.title', array(), 'vlance') ?></h1>
    <?php if($user->getCash()): ?>    
        <form id="form-withdrawal" action="<?php echo $view['router']->generate('transaction_withdraw_submit') ?>" method="POST">
            <div class="withdrawal-method span7">
                <?php if($is_withdrawable): ?>
                    <h2><?php echo $view['translator']->trans('transaction_withdraw.withdrawal_methods', array(), 'vlance') ?></h2>
                    <div class="select_bank">
                        <select class="span6" id="bank" name="userpaymentmethod" required="required">
                            <option value="" disabled="" selected="">- Chọn -</option>
                            <?php foreach ($paymentMethods as $method) : ?>
                                <option value="<?php echo $method->getId() ?>"><?php echo $method->getTitle() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <p class="offset6"><a href="#" data-toggle="modal" data-target="#userpaymentmethod_new"><?php echo $view['translator']->trans('list_userpaymentmethod.bank.new', array(), 'vlance') ?></a></p>
                <?php endif; ?>
                <table class="table">
                    <tbody>
                        <tr>
                            <td><?php echo $view['translator']->trans('transaction_withdraw.money_in_acc', array(), 'vlance') ?></td>  
                            <td>
                                <?php if($user->getCash()): ?>
                                <?php echo number_format($user->getCash()->getBalance(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php endif; ?>
                            </td> 
                        </tr>
                        <tr>
                            <td><?php echo $view['translator']->trans('transaction_withdraw.money_in_processing', array(), 'vlance') ?></td>  
                            <td>
                                <?php if($user->getCash()): ?>
                                <?php echo number_format($user->getCash()->getProcessingBalance(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                                <?php endif; ?>
                            </td> 
                        </tr>
                        <tr>
                            <td><?php echo $view['translator']->trans('transaction_withdraw.remaining_amount', array(), 'vlance') ?></td>
                            <td><?php echo number_format(($user->getCash()->getBalance() + $user->getCash()->getProcessingBalance()),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?></td>
                        </tr>
                        <?php if($is_withdrawable): ?>
                        <tr>
                            <?php /** @todo check input number must below than $user->getCash()->getBalance() */ ?>
                            <td><strong><?php echo $view['translator']->trans('transaction_withdraw.withdraw', array(), 'vlance') ?></strong></td> 
                            <td class="amount input-append"><input id="amount" class="span7 text-right" type="text" name="amount" required="required"><span class="add-on">VNĐ</span></td> 
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                <?php if($is_withdrawable): ?>
                    <div class="more-hourlies">
                        <a class="btn btn-large btn-primary"><?php echo $view['translator']->trans('transaction_withdraw.title', array(), 'vlance') ?></a>
                        <input type="submit" class="btn hide" 
                               value="<?php echo $view['translator']->trans('transaction_withdraw.title', array(), 'vlance') ?>" name="submit"/>
                        <input type="reset" class="btn btn-large" 
                               value="<?php echo $view['translator']->trans('common.cancellation', array(), 'vlance') ?>"/>
                    </div>
                    <div style="color:red; margin-top: 10px;font-size: 15px;"><b>Yêu cầu rút tiền sẽ được giải quyết trong 5 ngày (trừ lễ, Tết, thứ 7, Chủ Nhật)</b></div>
                <?php else: ?>
                    <div class="row-fluid"><?php echo $view['translator']->trans('transaction_withdraw.not_withdraw2', array(), 'vlance') ?></div>
                <?php endif; ?>
            </div>
        </form>
        &nbsp;<br/>
        <h3>CÂU HỎI THƯỜNG GẶP</h3>
        <ol>
            <li>
                <b>Số tiền tối thiểu mỗi lần tôi có thể rút là bao nhiêu?</b><br/>
                Số tiền tối thiểu mỗi lần rút là 50.000 VNĐ.
            </li>
            <li>
                <b>Chi phí rút tiền là bao nhiêu?</b><br/>
                Nếu bạn rút về tài khoản Vietcombank, phí rút là 3.300 VNĐ cho 1 lần rút. Với các ngân hàng khác là 11.000 VNĐ cho 1 lần rút.
            </li>
            <li>
                <b>Sau khi gửi yêu cầu rút tiền thì bao lâu sau yêu cầu của tôi sẽ được thực hiện?</b><br/>
                Chúng tôi sẽ xử lý yêu cầu rút tiền trong 5 ngày làm việc (không tính ngày lễ, tết hoặc Thứ 7 & Chủ nhật)
            </li>
            <li>
                <b>Kể từ khi yêu cầu rút tiền của tôi được vLance xử lý, sau bao lâu thì tôi sẽ nhận được tiền trong tài khoản?</b><br/>
                Sau khi vLance thực hiện chuyển tiền, bạn sẽ nhận được email xác nhận của vLance. Sau đó, tùy vào ngân hàng của bạn thì thời gian để tiền vào tới tài khoản của bạn sẽ khác nhau.<br/>
                Nếu là Vietcombank thì bạn sẽ nhận được trong vòng 15 phút.<br/>
                Nếu là ngân hàng khác, thông thường sẽ không quá 24h.
            </li>
            <li>
                <b>Tôi có thể rút tiền bao nhiêu lần 1 tuần?</b><br/>
                Hiện tại vLance không giới hạn số lần rút tiền của bạn. Tuy nhiên mỗi lần chuyển khoản đều mất phí từ 3.300 VNĐ đến 11.000 VND, vì vậy bạn nên cân nhắc hạn chế số lần rút để tiết kiệm chi phí cho mình.
            </li>
            <li>
                <b>Bạn có câu hỏi khác?</b><br/>
                Vui lòng liên hệ với chúng tôi 024.6684.1818 hoặc <a>hotro@vlance.vn</a>
            </li>
        </ol>

        <?php /* popup nhap tai khoan moi */ ?>
        <div class="modal fade" id="userpaymentmethod_new">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">x</a>
                <h3><?php echo $view['translator']->trans('list_userpaymentmethod.bank.new', array(), 'vlance') ?></h3>
            </div>
            <div class="modal-body">
                <?php echo $view['actions']->render($view['router']->generate('userpaymentmethod_new')); ?>
            </div>
        </div>
        <script type="text/javascript">
            /* <![CDATA[ */
            var user_balance = <?php if($user->getCash()){echo $user->getCash()->getBalance();} ?>;
            var user_processing_balance = <?php if($user->getCash()){ echo $user->getCash()->getProcessingBalance();}?>;
            /* ]]> */
        </script>
    <?php else: ?>
        <p><?php echo $view['translator']->trans('transaction_withdraw.not_withdraw', array('%g%'=>'<a href="'.$view['router']->generate('jobs_bid_project').'">góc làm việc</a>'), 'vlance') ?></p>
    <?php endif; ?>
</div>
<?php $view['slots']->stop(); ?>
