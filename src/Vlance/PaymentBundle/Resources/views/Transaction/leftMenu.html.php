<div class="dropdown clearfix">
    <ul class="nav-left">
        <?php foreach ($menus as $menu) : ?>
        <li class="tf200 <?php echo $menu['active']?'active':'' ?>">
            <a href="<?php echo $view['router']->generate($menu['action'], isset($menu['action_options'])?$menu['action_options']:array()); ?>" class="<?php echo $menu['class'] ?> <?php if ($menu['active']) : ?> active <?php endif;?>" >
                <?php echo $menu['title']?>
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
</div>