<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('list_transaction.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<?php $user = $app->getUser(); ?>
<?php /* Trang list transaction */ ?>
<div class="row-fluid">
    <h1><?php echo $view['translator']->trans('list_transaction.title', array(), 'vlance') ?></h1>
    <?php if (count($entities)) : ?>
    <?php /*<div class="time-bill">
        <span>Thời gian</span>
        <select class="span2">
            <option value="" selected="">30 Ngày</option>
            <option value="">30 Ngày</option>
            <option value="">60 Ngày</option>
            <option value="">90 Ngày</option>
            <option value="">120 Ngày</option>
        </select>
    </div> */ ?>
    <div class="list">
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo $view['translator']->trans('list_transaction.content.list.date', array(), 'vlance')?></th>
                    <th><?php echo $view['translator']->trans('list_transaction.content.list.project', array(), 'vlance')?></th>
                    <th><?php echo $view['translator']->trans('list_transaction.content.list.type', array(), 'vlance')?></th>
                    <th><?php echo $view['translator']->trans('list_transaction.content.list.status', array(), 'vlance')?></th>
                    <th><?php echo $view['translator']->trans('list_transaction.content.list.amount', array(), 'vlance')?></th>
                </tr>
            </thead>
            <tbody>
                <?php //  bill 1 ?>
                <?php foreach ($entities as $transaction) : ?>
                <?php /* @var $transaction \Vlance\PaymentBundle\Entity\Transaction */ ?>
                <tr>
                    <td class="transaction-date"><?php echo $transaction->getCreatedAt()->format('d-m-Y H.i A'); ?></td>
                    <td class="transaction-title-job">
                        <?php if ($transaction->getJob()) : ?>
                        <a href="<?php echo $view['router']->generate('job_show', array('id' => $transaction->getJob()->getId())) ?>">
                            <?php echo htmlentities($transaction->getJob()->getTitle(), ENT_SUBSTITUTE, "UTF-8") ?>
                        </a><br>
                        <?php endif;?>
                        <?php /*<span>
                         <strong>
                         <?php echo $view['translator']->trans(($user == $transaction->getJob()->getAccount()) ? 'freelancer' : 'client' , array(), 'vlance') ?>:&nbsp;</strong>
                         <?php echo ($user == $transaction->getJob()->getAccount()) ? $transaction->getJob()->getWorker()->getFullName() : $transaction->getJob()->getAccount()->getFullName() ?>
                         </span>*/?>
                    </td>
                    <td class="transaction-type"><?php echo $view['translator']->trans($transaction->getTypeText(), array(), 'vlance')?></td>
                    <td class="transaction-status"><?php echo $view['translator']->trans($transaction->getStatusText(), array(), 'vlance')?></td>
                    <td class="transaction-price">
                        <?php echo number_format($transaction->getAmount(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                </tr>
                <?php endforeach;?>
            </tbody>

        </table>
    </div>
    <?php if ($pager->getNbPages() != 1) : ?>
            <div>
                <p class="layered-footer results-paging">
                    <?php echo $pagerView->render($pager, $view['router']); ?>   
                </p>
            </div>
    <?php endif; ?>
<?php else:?>
    <p><?php echo $view['translator']->trans('list_transaction.content.empty', array(), 'vlance'); ?></p>
<?php endif;?>
</div>
<?php $view['slots']->stop(); ?>
