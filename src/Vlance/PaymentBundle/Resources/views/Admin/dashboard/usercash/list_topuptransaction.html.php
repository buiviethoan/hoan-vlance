<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#usercash-listtransaction">
            Lịch sử nạp tiền (<b><?php echo count($transactions);?></b>)
        </a>
    </div>
    <div id="usercash-listtransaction" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Loại GD</th>
                                <th class="text-right">Số tiền</th>
                                <th>Hình thức TT</th>
                                <th>Trạng thái</th>
                                <th>Ngày</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transactions as $tr): ?>
                            <tr>
                                <td><?php echo $tr->getId();?></td>
                                <td>[<?php echo $tr->getType() ?>] Nạp tiền</td>
                                <td class="text-right"><?php echo number_format($tr->getAmount(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance')?></td>
                                <td>[<?php echo $tr->getPaymentMethod()->getId(); ?>] <?php if($tr->getPaymentMethod()->getId() === 1){echo "Chuyển khoản ngân hàng";}else{echo "Nạp tiền trực tiếp";};?></td>
                                <td>[<?php echo $tr->getStatus() ?>] <?php echo $view['translator']->trans('credit_buy_transaction.status.' . $tr->getStatus(), array(), 'vlance')?></td>
                                <td><?php echo date_format($tr->getCreatedAt(), "d/m/Y h:i")?></td>
                                <td class="text-right">
                                    <a href="<?php echo $view['router']->generate('Vlance_PaymentBundle_AdminTransaction_edit',array('pk' => $tr->getId())) ?>">Edit</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
