<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#usercash-topup">
            Nạp tiền
        </a>
    </div>
    <div id="usercash-topup" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid" style="font-size: 20px; margin-top: 15px;"><p>Số tiền trong ví vLance: <b><?php echo number_format($userCash->getBalance(),'0',',','.') ?> </b>VNĐ</p></div>
            <div class="row-fluid">
                <form id="usercash-form" action="<?php echo $view['router']->generate('admin_topup_usercash_submit', array('uid' => $uid, 'referer' => $referer)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
                    <div class="row-fluid">
                        <div class="span4"><?php echo $view['form']->row($form['type'])?></div>
                        <div class="span4">
                            <div class="control-group ">
                                <input type="hidden" id="vlance_paymentbundle_topupusercashtransactiontype_paymentMethod" name="vlance_paymentbundle_topupusercashtransactiontype_paymentMethod" value="1" />
                                <label class="required" for="paymentMethod">Hình thức thanh toán</label>
                                <select id="paymentMethod" required="required">
                                    <option value="1" selected>Chuyển khoản ngân hàng</option>
                                    <option value="2">Nạp tiền mặt</option>
                                </select>
                            </div>
                        </div>
                        <div class="span4"><?php echo $view['form']->row($form['amount'])?></div>
                    </div>
                    <div class="row-fluid"><?php echo $view['form']->row($form['comment'])?></div>
                    <div style="margin-top: 10px;">
                        <?php echo $view['form']->row($form['_token'])?>
                        <p><input id="btn-form-submit" class="btn btn-primary btn-large" type="button" value="<?php echo $view['translator']->trans('button.confirm', array(), 'vlance'); ?> & gửi mail thông báo"/></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-form-submit').on('click', function(){
            document.getElementById('btn-form-submit').disabled = true;
            if($('#usercash-form').valid()){
                $('#usercash-form').submit();
            } else {
                document.getElementById('btn-form-submit').disabled = false;
            }
        });
        $('#paymentMethod').on('change', function(){
            var e = document.getElementById("paymentMethod");
            var value = e.options[e.selectedIndex].value;
            document.getElementById('vlance_paymentbundle_topupusercashtransactiontype_paymentMethod').value = value;
        });
    });
</script>