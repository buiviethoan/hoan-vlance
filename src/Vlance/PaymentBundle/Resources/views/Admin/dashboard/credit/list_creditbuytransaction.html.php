<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\PaymentBundle\Entity\CreditBuyTransaction; ?>

<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#credit--list-creditbuytransaction">
            Lịch sử mua Credit (<b><?php echo count($transactions);?></b>)
        </a>
    </div>
    <div id="credit--list-creditbuytransaction" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Loại GD</th>
                                <th class="text-right">Số lượng Credit</th>
                                <th class="text-right">Số tiền</th>
                                <th>Hình thức TT</th>
                                <th>Trạng thái</th>
                                <th>Ngày</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transactions as $tr): ?>
                            <tr>
                                <td><?php echo $tr->getId();?></td>
                                <td>[<?php echo $tr->getType() ?>] <?php echo $view['translator']->trans('credit_buy_transaction.type.' . $tr->getType(), array(), 'vlance'); ?></td>
                                <td class="text-right"><?php echo $tr->getCredit()?></td>
                                <td class="text-right"><?php echo number_format($tr->getAmount(),'0',',','.') ?> <?php echo $view['translator']->trans('common.currency', array(), 'vlance')?></td>
                                <td>[<?php echo $tr->getPaymentMethod() ?>] <?php echo $view['translator']->trans('credit_buy_transaction.method.' . $tr->getPaymentMethod(), array(), 'vlance')?></td>
                                <td>[<?php echo $tr->getStatus() ?>] <?php echo $view['translator']->trans('credit_buy_transaction.status.' . $tr->getStatus(), array(), 'vlance')?></td>
                                <td><?php echo date_format($tr->getCreatedAt(), "d/m/Y h:i")?></td>
                                <td class="text-right">
                                    <a href="<?php echo $view['router']->generate('Vlance_PaymentBundle_AdminCreditBuyTransaction_edit',array('pk' => $tr->getId())) ?>">Edit</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
