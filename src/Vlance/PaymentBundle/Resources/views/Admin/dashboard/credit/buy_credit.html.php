<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\PaymentBundle\Entity\CreditBuyTransaction; ?>

<?php $packages = $view['vlance']->getParameter('payment_gateway.onepay.otp.' . OnePay::SERV_OTP_BUY_CREDIT . '.package'); ?>
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-display" href="#credit--buy-credit">
            Nạp credit
        </a>
    </div>
    <div id="credit--buy-credit" class="accordion-body collapse in">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span6">
                <form action="<?php echo $view['router']->generate('admin_buy_credit_submit', array('uid' => $uid, 'referer' => $referer)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
                    <?php echo $view['form']->widget($form)?>
                    <div>
                        <p><input class="btn btn-primary btn-large" type="submit" value="<?php echo $view['translator']->trans('button.confirm', array(), 'vlance'); ?> & gửi mail thông báo"/></p>
                    </div>
                </form>
                </div>
                <div class="span6">
                    <h3>Danh sách các gói Credit - Chuyển khoản:</h3>
                    <ul>
                    <?php foreach ($packages as $package): ?>
                    <li>
                        <b><?php echo number_format($package['amount'],0,',','.'); ?></b> VNĐ = <b><?php echo $package['qty']; ?></b> Credit
                        <?php if(isset($package['discount'])): ?>
                            <?php if((int)($package['discount']) > 0): ?>
                            <span>
                                <i>(<?php echo number_format($package['amount']/$package['qty'],0,',','.'); ?> VNĐ/Credit)</i>
                                <span class="label-tag <?php echo isset($package['color_discount']) ? $package['color_discount'] : ""; ?>">TIẾT KIỆM <?php echo $package['discount']?>%</span>                            
                            </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; ?>
                    </ul>
                    <h3>Danh sách các gói Credit - Paypal:</h3>
                    <ul>
                        <li>Gói 6 Credits = <b>$4.90</b></li>
                        <li>Gói 15 Credits = <b>$8.90</b></li>
                        <li>Gói 50 Credits = <b>$21.90</b></li>
                        <li>Gói 120 Credits = <b>$43.90</b></li>
                        <li>Gói 300 Credits = <b>$87.80</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var transaction_type_buy = "<?php echo CreditBuyTransaction::TYPE_BUY?>";
    var transaction_payment_method_other = "<?php echo CreditBuyTransaction::PAYMENT_METHOD_OTHER?>";
    jQuery(document).ready(function($){
        showAmountAndPaymentMethod();
        
        $("#vlance_paymentbundle_creditbuytransactiontype_type").change(function(){
            if($(this).val() == transaction_type_buy){
                showAmountAndPaymentMethod();
            } else {
                hideAmountAndPaymentMethod();
            }
        })
        function hideAmountAndPaymentMethod(){
            $('#vlance_paymentbundle_creditbuytransactiontype_amount').val(0);
            $('#vlance_paymentbundle_creditbuytransactiontype_amount').parent().parent().hide(200);
            $("#vlance_paymentbundle_creditbuytransactiontype_paymentMethod").val(transaction_payment_method_other);
            $("#vlance_paymentbundle_creditbuytransactiontype_paymentMethod").parent().hide(200);
        }
        function showAmountAndPaymentMethod(){
            $('#vlance_paymentbundle_creditbuytransactiontype_amount').parent().parent().show(200);
            $("#vlance_paymentbundle_creditbuytransactiontype_paymentMethod").parent().show(200);
        }
    });
    
    
</script>