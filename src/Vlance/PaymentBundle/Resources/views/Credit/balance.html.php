<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\AccountBundle\Entity\Account; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Số dư Credit', array()));?>
<?php $view['slots']->start('content')?>

<?php /* @var $acc Account */?>
<?php $packages = $view['vlance']->getParameter('payment_gateway.onepay.otp.' . OnePay::SERV_OTP_BUY_CREDIT . '.package'); ?>
<?php $now = new \DateTime('now'); ?>

 <div class="subpage-tab">
    <ul class="nav nav-tabs">
        <li class="active blance-credit">
            <a>
                <i class="fa fa-database"></i>Số dư credit
            </a>
        </li>
        <li class="nav-tabs-history">
            <a href="<?php echo $view['router']->generate('credit_history_buy',array()) ?>">
                <i class="fa fa-shopping-cart"></i>Lịch sử mua
            </a>
        </li>
        <li class="nav-tabs-spend-credit">
            <a href="<?php echo $view['router']->generate('credit_history_spend',array()) ?>">
                <i class="fa fa-history"></i>Lịch sử sử dụng
            </a>
        </li>
    </ul>
</div> 

<!-- Buy VIP -->
<?php $packages_vip = $view['vlance']->getParameter('payment_gateway.onepay.otp.' . OnePay::SERV_OTP_UPGRADE_ACCOUNT . '.package'); ?>
<div class="block-buy-vip">
    <h3 style="padding: 13px 10px 10px;font-size: 20px;margin: 5px 0 10px 0px;text-transform: uppercase;letter-spacing: -0.02em;font-weight: 400;background: #f5f5f5;border-radius: 3px;font-family: 'Open Sans',sans-serif;line-height: 20px;">Nâng cấp tài khoản VIP (dành cho Freelancer) <span class="label-tag">MỚI</span></h3>
    
    <div style="margin-bottom: 8px;">
        Tài khoản :
        <span style="font-size: 16px;">
            <b>
                <?php if(is_object($acc)){
                        if(!is_null($acc->getExpiredDateVip())){
                            if(\Vlance\BaseBundle\Utils\JobCalculator::ago($acc->getExpiredDateVip(), $now) !== false){
                                if($acc->getTypeVIP() === Account::TYPE_VIP_NORMAL){
                                    echo 'VIP | ' . date_format($acc->getExpiredDateVip(),"d-m-Y H:i:s");
                                } elseif($acc->getTypeVIP() === Account::TYPE_VIP_MINI){
                                    $cat = "NULL";
                                    if(!is_null($acc->getCategoryVIP())){
                                        $cat = $acc->getCategoryVIP()->getTitle();
                                    }
                                    echo 'MINI VIP | ' . $cat . ' | ' . date_format($acc->getExpiredDateVip(),"d-m-Y H:i:s");
                                }
                            } else {
                                echo "Thường";
                            }
                        } else {
                            echo "Thường";
                        }
                    }
                ?>
            </b>
        </span>
    </div>
    
    <div class="row-fluid explain-content">
        <p>
            <b><i>Nâng cấp gói VIP để làm gì?</i></b><br/>
            <i>Nâng cấp gói VIP giúp Freelancer có thể thoải mái chào giá mọi dự án trong mọi lĩnh vực mà không phải mất thêm Credit.</i>
        </p>
        <p>
            <b><i>Nâng cấp gói Mini Vip để làm gì? Nâng cấp Mini Vip được lĩnh vực gì?</i></b><br/>
            <i>Nâng cấp gói Mini Vip giúp Freelancer có thể thoải mái chào giá mọi dự án trong <b>1 lĩnh vực nhỏ</b> mà không phải mất thêm Credit.</i><br/>
            <i>Nâng cấp Mini Vip được <b>lĩnh vực nhỏ</b> thuộc các lĩnh vực lớn:</i><br/>
        </p>
        <ul>
            <li>IT và lập trình</li>
            <li>Marketing & Bán hàng</li>
            <li>Viết lách & dịch thuật</li>
            <li>Thiết kế</li>
            <li>Kinh doanh, Nhập liệu & Hành chính</li>
            <li>Kế toán, Thuế & Pháp lý</li>
            <li>Lĩnh vực khác</li>
        </ul>
    </div>
    
    <div class="row-fluid">
        <div id="upgrade-account" class="row-fluid">
            <?php $countMini = 0; ?>
            <div class="menu-package-mini span6" id="menu-package-mini">
                <h3>Gói Mini Vip<a href="#" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Nâng cấp gói Mini VIP giúp Freelancer có thể thoải mái chào giá mọi dự án trong 1 lĩnh vực mà không phải mất thêm Credit."><i class="fa fa-question-circle" aria-hidden="true"></i></a></h3>
                <?php foreach($packages_vip as $pv): ?>
                    <?php $countMini++; ?>
                    <?php if($countMini > 3): ?>
                        <label>
                            <input id="packageVip<?php echo $pv['id']; ?>" name="buyPackageVip" type="radio" value="<?php echo $pv['id']; ?>" <?php echo isset($pv['default'])?"checked":""?>>
                            <b><?php echo $pv['qty']; ?></b> tháng = <b><?php echo number_format($pv['amount'],0,',','.'); ?></b> VNĐ
                            <?php if(isset($pv['promo'])){
                                if($view['vlance']->autoHideBanner('20-10-2017 00:00:00', '21-10-2017 23:59:59') === true){
                                    if(isset($pv['price_old'])){
                                        echo '<span style="text-decoration: line-through;">' . number_format($pv['price_old'],0,',','.') . '</span>';
                                    }
                                    echo '<span class="label-tag label-orange">' . $pv['promo'] . '</span>';
                                }
                            } ?>
                        </label>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            
            <?php $countVip = 0; ?>
            <div class="menu-package-vip span6">
                <h3>Gói Vip<a href="#" data-toggle="tooltip" data-placement="top" data-trigger="hover" data-original-title="Nâng cấp gói VIP giúp Freelancer có thể thoải mái chào giá mọi dự án mà không phải mất thêm Credit."><i class="fa fa-question-circle" aria-hidden="true"></i></a></h3>
                <?php foreach($packages_vip as $pv): ?>
                    <?php $countVip++; ?>
                    <?php if($countVip < 4): ?>
                        <label>
                            <input id="packageVip<?php echo $pv['id']; ?>" name="buyPackageVip" type="radio" value="<?php echo $pv['id']; ?>" <?php echo isset($pv['default'])?"checked":""?>>
                            <b><?php echo $pv['qty']; ?></b> tháng = <b><?php echo number_format($pv['amount'],0,',','.'); ?></b> VNĐ
                            <?php if(isset($pv['promo'])){
                                if($view['vlance']->autoHideBanner('20-10-2017 00:00:00', '23-10-2017 23:59:59') === true){
                                    if(isset($pv['price_old'])){
                                        echo '<span style="text-decoration: line-through;">' . number_format($pv['price_old'],0,',','.') . '</span> VNĐ ';
                                    }
                                    echo '<span class="label-tag label-orange">' . $pv['promo'] . '</span>';
                                }
                            } ?>
                        </label>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            
            <?php //Giảm giá Mini Vip 6 - 12. Ngày 14/10 - 15/10 ?>
            <?php if($view['vlance']->autoHideBanner('14-10-2017 00:00:00', '15-10-2017 23:59:59') === true): ?>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $("#upgrade-account input[name=buyPackageVip]").click(function(){
                            var val = $('input[name="buyPackageVip"]:checked').val();
                            if((val == 5) || (val == 6)){
                                $('#pay-upgrade-by-usercash').hide();
                            } else {
                                $('#pay-upgrade-by-usercash').show();
                            }
                        });
                    });
                </script>
            <?php endif; ?>
            
            <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip(); 
                });
            </script>
        </div>
        <div class="payment-methods row-fluid">
            <p>
            <?php /*    <a id="pay-upgrade-by-bankcard" class="btn btn-large" href="#pay-buy-credit-bankcard" 
                    data-request-url="<?php echo $view['router']->generate('credit_buy_bankcard_request'); ?>"
                    onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bankcard'})">
                        <i class="fa fa-credit-card"></i> Thẻ ngân hàng/ATM nội địa</a> */ ?>
                <?php if(is_object($acc)): ?>
                <a id="pay-upgrade-by-usercash" class="btn btn-large" data-toggle="modal" href="#pay-upgrade-account-usercash" 
                    data-url-confirm="<?php echo $view['router']->generate('upgrade_account_vip'); ?>"
                    onclick="vtrack('Click upgrade account', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Usercash'})">
                        Ví vLance</a>
                <?php endif; ?>
                <a id="pay-upgrade-by-banktrannsfer" class="btn btn-large" data-toggle="modal" href="#pay-upgrade-account-banktransfer" 
                    onclick="vtrack('Click upgrade account', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bank transfer'})">
                        <i class="fa fa-bank"></i> Chuyển khoản ngân hàng</a>
                <a id="pay-upgrade-by-paypal" class="btn btn-large" data-toggle="modal" href="#pay-upgrade-account-paypal" 
                    onclick="vtrack('Click upgrade account', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Paypal'})">
                        <i class="fa fa-cc-paypal"></i> Paypal</a>
            </p>
            <?php if(is_object($acc)): ?>
                <?php // if(HasPermission::hasAdminPermission($acc) == true): ?>
                <p>
                    <a id="pay-upgrade-by-bankcard" class="btn btn-large" href="#pay-upgrade-account-bankcard" 
                        data-request-url="<?php echo $view['router']->generate('upgrade_account_bankcard_request'); ?>"
                        onclick="vtrack('Click upgrade account', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bankcard Local'})">
                            <i class="fa fa-credit-card"></i> Thẻ ngân hàng/ATM nội địa </a>
                    <a id="pay-upgrade-by-visacard" class="btn btn-primary btn-large" href="#pay-upgrade-account-visacard"
                            data-request-url="<?php echo $view['router']->generate('upgrade_account_visacard_request'); ?>"
                            onclick="vtrack('Click upgrade account', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bankcard VISA'})">
                                <i class="fa fa-cc-visa"></i> Thẻ VISA/Mastercard <span class="label-tag">MỚI</span></a>
                </p>
                <?php // endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var accountPackage = <?php echo json_encode($packages_vip); ?>;
</script>
<!-- Pay by Bankcard -->
<?php echo $view->render('VlancePaymentBundle:Credit/upgrade:pay_by_bankcard.html.php', array()); ?>

<!-- Pay by Bank transfer 
<?php echo $view->render('VlancePaymentBundle:Credit/upgrade:pay_by_bank_transfer.html.php', array('acc' => $acc)); ?>

<!-- Pay by Visacard -->
<?php echo $view->render('VlancePaymentBundle:Credit/upgrade:pay_by_visacard.html.php', array()); ?>

<!-- Pay by Paypal transfer -->
<?php echo $view->render('VlancePaymentBundle:Credit/upgrade:pay_by_paypal.html.php', array('acc' => $acc)); ?>

<!-- Pay by Usercash -->
<?php echo $view->render('VlancePaymentBundle:Credit/upgrade:pay_by_usercash.html.php', array('acc' => $acc, 'form_name' => 'pay-upgrade-account-usercash')); ?>

<div class="subpage-content">
    <!-- Balance -->
    <h3>Số dư Credit</h3>
    <div class="row-fluid inner-content">
        <p>Bạn đang có: <b><?php echo $credit_balance; ?> CREDIT</b></p>
    </div>
    <p><br/></p>
    
    <div class="row-fluid explain-content">
        <p>
            <b><i>CREDIT dùng để làm gì?</i></b><br/>
            <i>Credit dùng để thanh toán các dịch vụ bổ sung trên vLance như: <b style="font-weight:600;">Liên hệ trực tiếp, đăng Việc nổi bật, đăng Chào giá nổi bật, đăng Hồ sơ Freelancer nổi bật, v.v...</b> và rất nhiều các dịch vụ khác sẽ tiếp tục được bổ sung trong thời gian tới. Các dịch vụ này sẽ giúp freelancer và khách hàng kết nối với nhau nhanh chóng và làm việc hiệu quả hơn trên vLance.</i>
        </p>
        <p>
            <b><i>Mua CREDIT bằng cách nào?</i></b><br/>
            <i>Hiện tại vLance đang hỗ trợ thanh toán mua Credit bằng SMS (Vinaphone, Mobifone, Viettel), Ví vLance. Sắp tới bạn sẽ có thể thanh toán bằng thẻ ngân hàng, và chuyển khoản. </i>
        </p>
    </div>
    <p><br/></p>
    
    <!-- Buy credit -->
    <h3>Mua thêm Credit</h3>    
    
    <?php // Promotion Money Lover ?>
    <?php $now = new DateTime("now"); ?>
    <?php if(isset($promo_ml)): ?>
        <?php if($promo_ml['promo_ml_start'] < $now && $now < $promo_ml['promo_ml_end']): ?>
            <div class="block-promotion" data-display-track="show-promotion-money-lover">
                <div class="alert alert-success">
                    <br/>
                    <b>Quà tặng đặc biệt khi mua Credit</b>:<br/>
                    Tặng ngay bản quyền trọn đời của phần mềm quản lý chi tiêu <b><a href="https://moneylover.me/" target="_blank">Money Lover</a></b> phiên bản PREMIUM (giá trên các store là 99.000 VNĐ)
                    khi mua bất kỳ gói Credit nào mệnh giá từ <b><?php echo number_format($promo_ml['promo_ml_limit'],0,',','.'); ?></b> VNĐ trở lên (tối đa 1 mã quà tặng cho 1 tài khoản).<br/>
                    Mã quà tặng sẽ được gửi trực tiếp đến email của bạn: <b><?php echo $acc->getEmail();?></b><br/>
                    <br/>
                    Chương trình tặng quà sẽ <b>kết thúc vào 23:59 ngày 28/02/2016</b>, tối đa <b>200 người đầu tiên</b>.<br/>
                    <br/>
                    <b><?php if(!$promo_ml['available']): ?>
                        Mỗi người chỉ có thể nhận tối đa 1 mã quà tặng. Bạn có thể kích hoạt hoặc kiểm tra lại mã quà tặng của mình <a href="<?php echo $view['router']->generate('partner_moneylover_gift',array()); ?>">tại đây</a>.</b>
                    <?php else: ?>
                        Sau khi nhận được mã quà tặng, bạn có thể <a href="<?php echo $view['router']->generate('partner_moneylover_gift',array()); ?>">kích hoạt tại đây</a><br/>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <?php // END Promotion Money Lover ?>
    
    <!-- Promotion Block -->
    <?php $promo_start = new DateTime("2016-06-09 13:00:00"); ?>
    <?php $promo_end   = new DateTime("2016-06-12 23:59:59"); ?>
    <?php $now         = new DateTime("now"); ?>
    <?php if($now > $promo_start && $now < $promo_end): ?>
        <div class="block-promotion">
            <div class="alert" style="background: #FFADAD;border: none;border-radius: 0;color: #333333;text-shadow: none;">
                <h2>Khuyến mại <b>NHẬN x2 LẦN CREDIT</b></h2>
                Nhân dịp vLance.vn đón thành viên thứ <b>100.000</b>, bạn sẽ được nhận <b>gấp đôi số Credit</b> khi mua bất kỳ gói Credit nào.</br>
                Áp dụng duy nhất trong bốn ngày từ 14h00 ngày 09/06/2016 đến 23h59 ngày 12/06/2016, khi thanh toán bằng thẻ ATM, Paypal, Ví vLance và Chuyển khoản ngân hàng.
                <br/>
                <br/>
            </div>
        </div>
    <?php endif; ?>
    <!-- END Promotion Block-->
    
    <!-- Credit Packages & Payment -->
    <div class="row-fluid inner-content">
        <form id="buyCredit" data-display-track="show-buy-credit-form">
            <h4>Bước 1: Chọn gói Credit</h4>
            <p>Credit không có hạn sử dụng. Vì vậy, càng mua nhiều bạn sẽ càng tiết kiệm được nhiều.</p>
            <div class="credit-packages row-fluid">
                <?php foreach ($packages as $package): ?>
                <label class="radio">
                    <input type="radio" name="buyCreditPackage" id="package<?php echo $package['id']; ?>" value="<?php echo $package['id']; ?>" <?php echo isset($package['default'])?"checked":""?>>
                    <b><?php echo number_format($package['amount'],0,',','.'); ?></b> VNĐ = <b><?php echo $package['qty']; ?></b> Credit
                    <?php if(isset($package['discount'])): ?>
                        <?php if((int)($package['discount']) > 0): ?>
                        <span>
                            <i>(<?php echo number_format($package['amount']/$package['qty'],0,',','.'); ?> VNĐ/Credit)</i>
                            <span class="label-tag <?php echo isset($package['color_discount']) ? $package['color_discount'] : ""; ?>">TIẾT KIỆM <?php echo $package['discount']?>%</span>
                        </span>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php // Promotion Money Lover ?>
                    <?php if(isset($promo_ml)): ?>
                        <?php if($promo_ml['available'] && $promo_ml['promo_ml_start'] < $now && $now < $promo_ml['promo_ml_end']): ?>
                            <?php if($promo_ml['promo_ml_limit'] <= $package['amount']):?>
                                + Tặng <div title="Tặng mobile app Money Lover" style="display:inline-block;background: url(../img/logo_footer_partners.png);background-repeat: no-repeat;height: 20px;background-position: 0px -131px;width: 80px;margin-bottom: -7px;background-size: 100%;"></div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php // END Promotion Money Lover ?>
                </label>
                <?php endforeach; ?>
            </div>
            <p><br/></p>
            <h4>Bước 2: Chọn hình thức thanh toán</h4>
            <div class="payment-methods row-fluid">
                <p>
                    <a id="pay-by-bankcard" class="btn btn-large" href="#pay-buy-credit-bankcard" 
                        data-request-url="<?php echo $view['router']->generate('credit_buy_bankcard_request'); ?>"
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bankcard'})">
                            <i class="fa fa-credit-card"></i> Thẻ ngân hàng/ATM nội địa </a>
                    <a id="pay-by-usercash" class="btn btn-large" data-toggle="modal" href="#pay-buy-credit-usercash" 
                        data-url-confirm="<?php echo $view['router']->generate('credit_buy_confirm'); ?>"
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Usercash'})">
                            Ví vLance</a>
                    <a id="pay-by-sms" class="btn btn-large" href="#pay-buy-credit" 
                        data-url-request="<?php echo $view['router']->generate('transaction_onepay_otp_request', array('service_id' => OnePay::SERV_OTP_BUY_CREDIT)); ?>"
                        data-url-confirm="<?php echo $view['router']->generate('transaction_onepay_confirm_buy_credit'); ?>"
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'SMS OTP'})">
                            Tin nhắn SMS</a>
                </p>
                <p>
                    <a id="pay-by-visacard" class="btn btn-primary btn-large" href="#pay-buy-credit-visacard"
                        data-request-url="<?php echo $view['router']->generate('credit_buy_visacard_request'); ?>"
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bankcard VISA'})">
                            <i class="fa fa-cc-visa"></i> Thẻ VISA/Mastercard <span class="label-tag">MỚI</span></a>
                </p>
                <p><br/></p>
                <p>Hoặc thanh toán bằng</p>
                <p>
                    <a id="pay-by-banktrannsfer" class="btn btn-large" data-toggle="modal" href="#pay-buy-credit-banktransfer" 
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Bank transfer'})">
                            <i class="fa fa-bank"></i> Chuyển khoản ngân hàng</a>
                    <a id="pay-by-paypal" class="btn btn-large" data-toggle="modal" href="#pay-buy-paypal" 
                        onclick="vtrack('Click buy credit', {'authenticated':'<?php echo is_object($acc)?'true':'false'?>', 'location':'Balance page', 'method': 'Paypal'})">
                            <i class="fa fa-cc-paypal"></i> Paypal</a>
                </p>
            </div>
        </form>
    </div>
    <!-- END Credit Packages & Payment -->
</div>

<!-- Pay by SMS OTP -->
<?php echo $view->render('VlancePaymentBundle:Onepay:popup_pay_sms_buy_credit.html.php', 
        array('acc'             => $acc, 
            'form_name'         => 'pay-buy-credit', 
            'context_msg_path'  => 'paysms.buycredit',
            'packages'          => $packages)); ?>
<!-- Pay by Bankcard -->
<?php echo $view->render('VlancePaymentBundle:Credit/balance:pay_by_bankcard.html.php', array()); ?>

<!-- Pay by Visacard -->
<?php echo $view->render('VlancePaymentBundle:Credit/balance:pay_by_visacard.html.php', array()); ?>

<!-- Pay by Bank transfer -->
<?php echo $view->render('VlancePaymentBundle:Credit/balance:pay_by_bank_transfer.html.php', array('acc' => $acc)); ?>

<!-- Pay by Paypal transfer -->
<?php echo $view->render('VlancePaymentBundle:Credit/balance:pay_by_paypal.html.php', array('acc' => $acc)); ?>

<!-- Pay by Usercash -->
<?php echo $view->render('VlancePaymentBundle:Credit/balance:pay_by_usercash.html.php', array('acc' => $acc, 'form_name' => 'pay-buy-credit-usercash')); ?>

<?php $view['slots']->stop();?>