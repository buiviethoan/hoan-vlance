<?php /**
 * 
 * required input variables
 * $acc                         : current Account
 * $context_msg_path            : translation path (ex: "paysms.contact")
 * $form_name             (opt) : name of the form, avoid conflict
 * 
 */ ?>

<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>
<?php $service_id = CreditExpenseTransaction::SERVICE_CONTACT_CLIENT; ?>
<?php $service = $view['vlance_payment']->getParameter('credit.spend.' . $service_id); ?>

<?php // Form for contact button when unauthenticated ?>
<div class="modal modal-new fade modal-credit modal-paysms pay-credit-get-contact" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="redirect-spinner">
        <img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/>
        <p><?php echo $view['translator']->trans('paysms.redirect_spinner', array(), 'vlance') ?></p>
    </div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Liên hệ Khách hàng</h2>
    </div>
    
    <?php if(!is_object($acc)):?>
    <div class="screen-confirm">
        <div class="modal-body">
            <p class="first"><?php echo $view['translator']->trans('paysms.pop_up.login_title_p1', array(), 'vlance') ?> <strong>Liên hệ với khách hàng</strong>, <?php echo $view['translator']->trans('paysms.pop_up.login_title_p2', array(), 'vlance') ?></p>
            <div class="center">
                <div class="btn-create-account">
                    <a class="btn btn-primary btn-large" href="<?php echo $view['router']->generate('account_register', array('type' => 'client')) ?>"
                       onclick="vtrack('Contact registration', {'type':'normal'})"><?php echo $view['translator']->trans('paysms.pop_up.login_title_button1', array(), 'vlance') ?></a>
                </div>
                <p><?php echo $view['translator']->trans('paysms.pop_up.login_title_p4', array(), 'vlance') ?></p>
                <div class="job-login-socail">
                    <a class="btn-flat-new btn-facebook-flat" onclick="vtrack('Contact registration', {'type':'facebook'});fb_login();">Facebook</a>
                    <a class="btn-flat-new btn-google-flat" onclick="vtrack('Contact registration', {'type':'google'});google_login();">Google</a>
                    <a class="btn-flat-new btn-linkedin-flat" onclick="vtrack('Contact registration', {'type':'linkedin'});linkedin_login();">Linkedin</a>
                </div>
            </div>
        </div>
    </div>
    <?php else:?>
    <!-- Confirmation screen -->
    <div class="screen-confirm">
        <div class="modal-body">
            <div class="preview"><img width="460px" height="250px" src="/img/credit/contact-client.jpg" alt="Liên hệ khách hàng" title="Liên hệ khách hàng"></div>
            <div class="content">
                <p>Cần liên hệ trực tiếp với Khách hàng này? Bạn sẽ nhận số điện thoại và email của khách hàng ngay bây giờ.</p>
                <p><i>Chi phí: <b><span class="credit-amount"><?php echo $service['amount'][$level];?></span> CREDIT</b></i></p>
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php if(is_object($acc)){ echo $acc->getCredit()->getBalance();} else {echo  0;} ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn-link" data-dismiss="modal">Không, tôi chưa cần</a>
            <a href="#" class="btn btn-primary pay-credit-confirm">Liên hệ ngay</a>
        </div>
    </div>
    
    <!-- Success screen -->
    <div class="screen-success">
        <div class="modal-body">
            <div class="content">
                <div class="contact-sms">
                    <p>Tuyệt vời, từ bây giờ bạn có thể liên hệ với khách hàng theo email và SĐT sau:</p>
                    <div class="row-fluid contact-info">
                        <div class="info-photo span2 offset2">
                            <img src="<?php echo $view['assets']->getUrl('img/unknown.png') ?>" data-replace="avatar_url"/>
                        </div>
                        <div class="info-detail span7 offset1">
                            <p class="full-name"><span data-replace="fullName">Tên Khách hàng</span></p>
                            <p class="email"><span class="info-label">Email:</span> <span data-replace="email">client@domain.com</span></p>
                            <p class="cellphone"><span class="info-label">Điện thoại:</span> <span data-replace="telephone">0123456789</span></p>
                        </div>
                    </div>
                    <div class="connect-email payer">
                        <p><?php echo $view['translator']->trans('paysms.pop_up.step3.guide1', array(), 'vlance') ?> <a data-replace="email"><?php echo $acc->getEmail(); ?></a></p>
                    </div>
                    <div class="suggest-create-job">
                        <p class="first">
                            <b>Lưu ý:</b><br/>
                            - Hãy yêu cầu khách hàng đăng việc vào đặt cọc dự án qua vLance để bạn được đảm bảo thanh toán sau khi làm việc.<br/>
                            - Nếu nhận việc, hãy đảm bảo dịch vụ của bạn có chất lượng cao nhất. vLance sẽ tạm ngưng dịch vụ từ với những tài khoản freelancer bị khách hàng gửi nhiều cảnh báo xấu về chất lượng công việc.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn pay-credit-close" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
    
    <!-- Error screen -->
    <div class="screen-error">
        <div class="modal-body">
            <div class="content">
                <p class="error-message">Rất tiếc, đã có lỗi xảy ra. Vui lòng liên hệ vLance để được hỗ trợ.</p>
                <!--<p class="error-contact"><b><i>Số điện thoại hỗ trợ: 024.6684.1818 - Email: hotro@vlance.vn</i></b></p>-->
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php echo is_object($acc) ? $acc->getCredit()->getBalance() : 0; ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    var form_spend_credit_contact_client = "#<?php echo $form_name ?>";
    var form_spend_credit_contact_client_params = {"service_id":<?php echo $service_id; ?>,"url":"<?php echo $view['router']->generate('credit_spend',array()) ?>","balance":<?php echo is_object($acc) ? $acc->getCredit()->getBalance() : 0;?>};
</script>