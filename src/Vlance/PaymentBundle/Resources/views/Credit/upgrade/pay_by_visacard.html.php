<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="pay-upgrade-account-visacard">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Thanh toán qua thẻ visa</h2>
    </div>
    <div class="modal-body">
        <div class="screen-content screen-waiting">
            <p class="waiting-icon"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></p>
            <p class="message">Bạn sẽ được chuyển sang cổng thanh toán của <b>1Pay</b> ngay bây giờ để thực hiện giao dịch.</p>
        </div>
        <div class="screen-content screen-error" style="display:none;">
            <p class="message">Rất tiếc, đã có lỗi xảy ra. Vui lòng quay trở lại và thực hiện thanh toán sau.</p>
        </div>
    </div>
</div>