<!-- Paypal transfer -->
<div class="modal modal-new fade modal-credit" style="display:none;" id="pay-upgrade-account-paypal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Thanh toán qua Paypal</h2>
    </div>
    <div class="modal-body">
        <div class="content">
            <p>Vui lòng ghi rõ ID tài khoản vLance của bạn <b class="text-error credit-code">#<?php echo $acc->getId(); ?></b> trong nội dung thanh toán.<br/><br/></p>
            <p>Mệnh giá các gói VIP khi thanh toán bằng Paypal (đơn vị tính USD):</p>
            <ul>
                <?php if($view['vlance']->autoHideBanner('20-10-2017 00:00:00', '21-10-2017 23:59:59') === true): ?>
                    <li>Gói 180K = <b>$8.95</b></li>
                    <li>Gói 300K = <b>$13.95</b></li>
                    <li>Gói 360K = <b>$16.11</b> <span class="label-tag label-orange">Giảm 10%</span></li>
                    <li>Gói 480K = <b>$21.45</b></li>
                    <li>Gói 600K = <b>$23.72</b> <span class="label-tag label-orange">Giảm 15%</span></li>
                    <li>Gói 960K = <b>$34.32</b> <span class="label-tag label-orange">Giảm 20%</span></li>
                <?php else: ?>
                    <li>Gói 180K = <b>$8.95</b></li>
                    <li>Gói 300K = <b>$13.95</b></li>
                    <li>Gói 360K = <b>$17.90</b></li>
                    <li>Gói 480K = <b>$21.45</b></li>
                    <li>Gói 600K = <b>$27.90</b></li>
                    <li>Gói 960K = <b>$42.90</b></li>
                <?php endif; ?>
            </ul>
            <p>Tài khoản nhận thanh toán: <a>invoice@vlance.vn</a></p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">OK, tôi đã hiểu</a>
    </div>
</div>
<!-- END Bank transfer -->