<?php /**
 * 
 * required input variables
 * $acc                         : current Account
 * $context_msg_path            : translation path (ex: "paysms.contact")
 * $form_name             (opt) : name of the form, avoid conflict
 * 
 */ ?>

<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>
<?php $service_id = CreditExpenseTransaction::SERVICE_FEATURE_JOB; ?>
<?php $service = $view['vlance_payment']->getParameter('credit.spend.' . $service_id); ?>
<?php $packet_arr = array(CreditExpenseTransaction::SERVICE_FEATURE_JOB, CreditExpenseTransaction::SERVICE_FEATURE_JOB_FANPAGE);?>

<?php // Form for contact button when unauthenticated ?>
<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="redirect-spinner">
        <img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/>
        <p><?php echo $view['translator']->trans('paysms.redirect_spinner', array(), 'vlance') ?></p>
    </div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Đăng việc nổi bật</h2>
    </div>
    
    <!-- Confirmation screen -->
    <div class="screen-confirm">
        <div class="modal-body">
            <div class="preview"><img width="460px" height="250px" src="/img/credit/feature-job.png" alt="Việc nổi bật" title="Việc nổi bật"></div>
            <div class="content">
                <div>
                    <p>Công việc sẽ được đăng nổi bật 
                        <b><span class="feature-job-duration"><?php echo "3"//$service['duration'];?></span> ngày</b> 
                        liên tục trên danh sách việc làm để thu hút được nhiều chào giá hơn.<br/>
                        <i>Lưu ý: Việc đăng bí mật sẽ tự động chuyển sang công khai.</i></p>
                </div>
                <div style="margin-left: 80px;">
                    <?php $count = 0; ?>
                    <?php foreach($packet_arr as $p): ?>
                    <?php $count++; ?>
                    <?php $s = $view['vlance_payment']->getParameter('credit.spend.' . $p); ?>
                    <div class="radio">
                        <label>
                            <input type="radio" name="feature-promote-fanpage" value="<?php echo $p; ?>" <?php if($count == 1) echo "checked"?>>
                            <?php echo $s['label'] ?><i><strong> (<?php echo $s['amount'] ?> Credits)</strong></i>
                        </label>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php echo $acc->getCredit()->getBalance(); ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn-link" data-dismiss="modal">Không, nhận ít chào giá thôi</a>
            <a href="#" class="btn btn-primary pay-credit-confirm">Đăng nổi bật</a>
        </div>
    </div>
    
    <!-- Success screen -->
    <div class="screen-success">
        <div class="modal-body">
            <div class="content">
                <p>Thật tuyệt vời. Công việc của bạn đã được đăng nổi bật.</p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn pay-credit-close" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
    
    <!-- Error screen -->
    <div class="screen-error">
        <div class="modal-body">
            <div class="content">
                <p class="error-message">Rất tiếc, đã có lỗi xảy ra. Vui lòng liên hệ vLance để được hỗ trợ.</p>
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php echo $acc->getCredit()->getBalance(); ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    var form_spend_credit_feature_job = "#<?php echo $form_name ?>";
    var form_spend_credit_feature_job_params = {"service_id":<?php echo $service_id; ?>,"url":"<?php echo $view['router']->generate('credit_spend',array()) ?>","balance":<?php echo $acc->getCredit()->getBalance(); ?>};
</script>