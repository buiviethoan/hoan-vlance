<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Số dư Credit', array()));?>
<?php $view['slots']->start('content')?>

<div class="subpage-tab">
    <ul class="nav nav-tabs">
        <li class="active"><a>Số dư</a></li>
        <li><a href="<?php echo $view['router']->generate('credit_history_list',array()) ?>">Mua</a></li>
        <li><a href="<?php echo $view['router']->generate('credit_spending_list',array()) ?>">Sử dụng</a></li>
    </ul>
</div> 
<?php // echo $view->render('VlanceAccountBundle:Profile/edit_basic:content.html.php', array('form' => $edit_form,'entity' => $entity, 'referer' => $referer)) ?>
<?php $view['slots']->stop();?>