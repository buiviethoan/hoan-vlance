<!-- Bank transfer -->
<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="pay-buy-credit-banktransfer">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Chuyển khoản ngân hàng</h2>
    </div>
    <div class="modal-body">
        <div class="content">
            <p>Vui lòng ghi rõ tên gói Credit (<b>100K</b> hoặc <b>200K</b> hoặc <b>500K</b> hoặc <b>1000K</b> hoặc <b>2000K</b>) và ID tài khoản vLance của bạn <b class="text-error credit-code">#<?php echo $acc->getId(); ?></b> trong nội dung chuyển khoản.<br/><br/></p>
            <p><b>Thông tin chuyển khoản:</b></p>
            <!--<p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p5_info', array(), 'vlance'); ?></p>-->
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2', array(), 'vlance'); ?>: <b><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p2_info', array(), 'vlance'); ?></b></p>
            <br/>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <b><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1_info', array(), 'vlance'); ?></b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <b><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3_info', array(), 'vlance'); ?></b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <b><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4_info', array(), 'vlance'); ?></b></p>
            <br/>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <b>Ngân hàng TMCP Á Châu ACB</b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <b>228085579</b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <b>Hà Nội</b></p>
            <br/>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p1', array(), 'vlance'); ?>: <b>Ngân hàng TMCP Kỹ Thương Techcombank</b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p3', array(), 'vlance'); ?>: <b>19030881539011</b></p>
            <p><?php echo $view['translator']->trans('view_job_client_page.guider_block.block2.transaction1_p4', array(), 'vlance'); ?>: <b>Hà Nội</b></p>
        </div>
        <div class="buy-credit">
            <p>
            </p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">OK. Tôi ra ngân hàng chuyển tiền luôn đây</a>
    </div>
</div>
<!-- END Bank transfer -->