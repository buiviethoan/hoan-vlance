<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Tiền trong ví vLance</h2>
    </div>
    <div class="buy-confirm">
        <div class="modal-body">
            <div class="content">
                <p>Số tiền bạn cần thanh toán: <b><span class="usercash-amount-confirm">100.000</span> VNĐ</b>?</p>
            </div>
            <div class="buy-credit">
                <p>
                    Số dư trong ví vLance: <b><span class="credit-balance" data-usercash="balance"><?php echo is_object($acc) ? number_format($acc->getCash()->getActualBalance(),0,',','.') : 0; ?></span> VNĐ</b>.
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn-link btn-large" data-dismiss="modal">Không, tôi chưa cần</a>
            <a href="#" class="btn btn-large btn-primary confirm-pay-usercash">Đồng ý thanh toán</a>
        </div>
    </div>
    
    <div class="buy-success">
        <div class="modal-body">
            <div class="content">
                <p>Tuyệt vời, giao dịch đã thành công. Cảm ơn bạn.</p>
            </div>
            <div class="buy-credit">
                <p>
                    Số dư trong ví vLance: <b><span class="credit-balance" data-usercash="balance"><?php echo is_object($acc) ? number_format($acc->getCash()->getActualBalance(),0,',','.') : 0; ?></span> VNĐ</b>.
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-large" data-dismiss="modal">Xong</a>
        </div>
    </div>
    
    <div class="buy-error">
        <div class="modal-body">
            <div class="content">
                <p class="error-message">Rất tiếc đã có lỗi xảy ra.</p>
            </div>
            <div class="buy-credit">
                <p>
                    Số dư trong ví vLance: <b><span class="credit-balance" data-usercash="balance"><?php echo is_object($acc) ? number_format($acc->getCash()->getActualBalance(),0,',','.') : 0; ?></span> VNĐ</b>.
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-large" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
</div>
<script type="text/javascript">
    var form_name_buy_credit_usercash = "#<?php echo $form_name ?>"; 
    var form_name_buy_credit_usercash_balance = <?php echo $acc->getCash()->getActualBalance();?>;
</script>