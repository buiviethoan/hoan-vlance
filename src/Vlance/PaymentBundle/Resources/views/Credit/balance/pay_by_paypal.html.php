<!-- Paypal transfer -->
<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="pay-buy-paypal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Thanh toán qua Paypal</h2>
    </div>
    <div class="modal-body">
        <div class="content">
            <p>Vui lòng ghi rõ ID tài khoản vLance của bạn <b class="text-error credit-code">#<?php echo $acc->getId(); ?></b> trong nội dung thanh toán.<br/><br/></p>
            <p>Mệnh giá các gói Credit khi thanh toán bằng Paypal (đơn vị tính USD):</p>
            <ul>
                <li>Gói 100K = <b>$4.90</b></li>
                <li>Gói 200K = <b>$8.90</b></li>
                <li>Gói 500K = <b>$21.90</b></li>
                <li>Gói 1000K = <b>$43.90</b></li>
                <li>Gói 2000K = <b>$87.80</b></li>
            </ul>
            <p>Tài khoản nhận thanh toán: <a>invoice@vlance.vn</a></p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">OK, tôi đã hiểu</a>
    </div>
</div>
<!-- END Bank transfer -->