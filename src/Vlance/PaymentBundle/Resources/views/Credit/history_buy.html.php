<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\AccountBundle\Entity\Account; ?>

<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Số dư Credit', array()));?>
<?php $view['slots']->start('content')?>

<div class="subpage-tab">
    <ul class="nav nav-tabs">
        <li class="blance-credit">
            <a href="<?php echo $view['router']->generate('credit_balance',array()) ?>">
                <i class="fa fa-database"></i>Số dư Credit
            </a>
        </li>
        <li class="active nav-tabs-history">
            <a><i class="fa fa-shopping-cart"></i>Lịch sử mua</a>
        </li>
        <li class="nav-tabs-spend-credit">
            <a href="<?php echo $view['router']->generate('credit_history_spend',array()) ?>">
                <i class="fa fa-history"></i>Lịch sử sử dụng
            </a>
        </li>
    </ul>
</div> 
<div class="transaction-history-credit">
    <div class="subpage-content">
        <div class="buy-credits">
            <div class="row-fluid">
                <h3>Mua Credit</h3> 
                <div class="list">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><?php echo $view['translator']->trans('list_transaction.content.list.date', array(), 'vlance')?></th>
                                <th><?php echo $view['translator']->trans('list_transaction.content.list.type', array(), 'vlance')?></th>
                                <th class="credit-payment"><?php echo $view['translator']->trans('list_transaction.content.list.payments', array(), 'vlance')?></th>
                                <th class="credit-number"><?php echo $view['translator']->trans('list_transaction.content.list.number', array(), 'vlance')?></th>
                                <th class="credit-price"><?php echo $view['translator']->trans('list_transaction.content.list.fee', array(), 'vlance')?></th>
                                <th><?php echo $view['translator']->trans('list_transaction.content.list.status', array(), 'vlance')?></th>
                                <th class="credit-note"><?php echo $view['translator']->trans('list_transaction.content.list.note', array(), 'vlance')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($entities as $credit_history_buy) : ?>
                                <tr>
                                    <td class="transaction-date">
                                        <?php echo $credit_history_buy->getCreatedAt()->format('d-m-Y H:i'); ?>
                                    </td>
                                    <td class="transaction-type">
                                        <?php echo $view['translator']->trans("credit_buy_transaction.type." . $credit_history_buy->getType(), array(), 'vlance')?>
                                    </td>
                                    <td class="payment-methods">
                                        <?php echo $view['translator']->trans("credit_buy_transaction.method." . $credit_history_buy->getPaymentMethod(), array(), 'vlance')?>
                                    </td>
                                    <td class="transaction-number">
                                        <?php echo $view['translator']->trans($credit_history_buy->getCredit(), array(), 'vlance')?>
                                    </td>
                                    <td class="transaction-price">
                                        <?php echo number_format($credit_history_buy->getAmount(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                                    </td>
                                    <td>
                                        <?php echo $view['translator']->trans("credit_buy_transaction.status." . $credit_history_buy->getStatus(), array(), 'vlance')?>
                                    </td>
                                    <td class="transaction-note">
                                        <?php echo $view['translator']->trans($credit_history_buy->getComment(), array(), 'vlance')?>
                                    </td>
                                 </tr>
                            <?php endforeach;?>
                       </tbody>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>
<?php if ($pager->getNbPages() != 1) : ?>
    <div>
        <p class="layered-footer results-paging">
            <?php echo $pagerView->render($pager, $view['router']); ?>   
        </p>
    </div>
<?php endif; ?>
<?php // echo $view->render('VlanceAccountBundle:Profile/edit_basic:content.html.php', array('form' => $edit_form,'entity' => $entity, 'referer' => $referer)) ?>
<?php $view['slots']->stop();?>