<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php')?>
<?php // $view['slots']->set('title', $view['translator']->trans('profile.edit_profile.title_tab_page', array('%username%' =>  $entity->getFullName())));?>
<?php $view['slots']->set('title', $view['translator']->trans('Số dư Credit', array()));?>
<?php $view['slots']->start('content')?>

<div class="subpage-tab">
    <ul class="nav nav-tabs">
        <li class="blance-credit">
            <a href="<?php echo $view['router']->generate('credit_balance',array()) ?>">
                <i class="fa fa-database"></i>Số dư Credit
            </a>
        </li>
        <li class="nav-tabs-history">
            <a href="<?php echo $view['router']->generate('credit_history_buy',array()) ?>">
                <i class="fa fa-shopping-cart"></i></i>Lịch sử mua
            </a>
        </li>
        <li class="active nav-tabs-spend-credit">
            <a>
                <i class="fa fa-history"></i>Lịch sử sử dụng
            </a>
        </li>
    </ul>
</div>
<div class="spent-credit">
    <div class="row-fluid">
        <h3>Sử dụng Credit</h3>
        <div class="list">
            <table class="table">
                <thead>
                    <tr>
                        <th><?php echo $view['translator']->trans('list_transaction.content.list.date', array(), 'vlance')?></th>
                        <th><?php echo $view['translator']->trans('list_transaction.content.list.detail', array(), 'vlance')?></th>
                        <th class="credit-number"><?php echo $view['translator']->trans('list_transaction.content.list.number', array(), 'vlance')?></th>
                        <th><?php echo $view['translator']->trans('list_transaction.content.list.status', array(), 'vlance')?></th>
                        <th class="credit-note"><?php echo $view['translator']->trans('list_transaction.content.list.note', array(), 'vlance')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entities as $credit_history_spend) : ?>
                    <tr>
                        <td class="transaction-date">
                            <?php echo $credit_history_spend->getCreatedAt()->format('d-m-Y H.i A'); ?>
                        </td>
                        <td>
                            <?php echo $view['translator']->trans("credit_spend_transaction.method." . $credit_history_spend->getService(), array(), 'vlance')?>
                        </td>
                        <td class="transaction-number">
                            <?php echo $view['translator']->trans($credit_history_spend->getAmount(), array(), 'vlance')?>
                        </td>
                        <td>
                            <?php echo $view['translator']->trans("credit_spend_transaction.status." . $credit_history_spend->getStatus(), array(), 'vlance')?>
                        </td>
                        <td class="transaction-note">
                            <?php echo $view['translator']->trans($credit_history_spend->getComment(), array(), 'vlance')?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if ($pager->getNbPages() != 1) : ?>
    <div>
        <p class="layered-footer results-paging">
            <?php echo $pagerView->render($pager, $view['router']); ?>   
        </p>
    </div>
<?php endif; ?>
<?php // echo $view->render('VlanceAccountBundle:Profile/edit_basic:content.html.php', array('form' => $edit_form,'entity' => $entity, 'referer' => $referer)) ?>
<?php $view['slots']->stop();?>