<?php /**
 * 
 * required input variables
 * $acc                         : current Account
 * $form_name             (opt) : name of the form, avoid conflict
 * 
 */ ?>

<?php use Vlance\PaymentBundle\Entity\CreditExpenseTransaction; ?>
<?php $service_id = CreditExpenseTransaction::SERVICE_FEATURE_FREELANCER; ?>
<?php $service = $view['vlance_payment']->getParameter('credit.spend.' . $service_id); ?>

<?php // Form for contact button when unauthenticated ?>
<div class="modal modal-new fade modal-credit modal-paysms" style="display:none;" id="<?php echo $form_name ?>">
    <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
    <div class="redirect-spinner">
        <img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/>
        <p><?php echo $view['translator']->trans('paysms.redirect_spinner', array(), 'vlance') ?></p>
    </div>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2>Đăng "Hồ sơ nổi bật"</h2>
    </div>
    
    <?php if(!is_object($acc)):?>
    <div class="screen-confirm">
        <div class="modal-body">
            <p class="first"><?php echo $view['translator']->trans('paysms.pop_up.login_title_p1', array(), 'vlance') ?> <strong>Liên hệ với freelancer</strong>, <?php echo $view['translator']->trans('paysms.pop_up.login_title_p2', array(), 'vlance') ?></p>
            <div class="center">
                <div class="btn-create-account">
                    <a class="btn btn-primary btn-large" href="<?php echo $view['router']->generate('account_register', array('type' => 'client')) ?>"
                       onclick="vtrack('Contact registration', {'type':'normal'})"><?php echo $view['translator']->trans('paysms.pop_up.login_title_button1', array(), 'vlance') ?></a>
                </div>
                <p><?php echo $view['translator']->trans('paysms.pop_up.login_title_p4', array(), 'vlance') ?></p>
                <div class="job-login-socail">
                    <a class="btn-flat-new btn-facebook-flat" onclick="vtrack('Contact registration', {'type':'facebook'});fb_login();">Facebook</a>
                    <a class="btn-flat-new btn-google-flat" onclick="vtrack('Contact registration', {'type':'google'});google_login();">Google</a>
                    <a class="btn-flat-new btn-linkedin-flat" onclick="vtrack('Contact registration', {'type':'linkedin'});linkedin_login();">Linkedin</a>
                </div>
            </div>
        </div>
    </div>
    <?php else:?>
    <!-- Confirmation screen -->
    <div class="screen-confirm">
        <div class="modal-body">
            <div class="preview"><img width="460px" height="250px" src="/img/credit/feature-freelancer.jpg" alt="Hồ sơ nổi bật" title="Hồ sơ nổi bật"></div>
            <div class="content">
                <p>Hồ sơ của bạn sẽ được đăng nổi bật <b><span class="feature-job-duration"><?php echo $service['duration'];?></span> ngày</b> liên tục trên danh sách freelancer để có thêm nhiều cơ hội được khách hàng liên lạc hơn.</p>
                <p><i>Chi phí: <b><span class="credit-amount"><?php echo $service['amount'];?></span> CREDIT</b></i></p>
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php echo is_object($acc) ? $acc->getCredit()->getBalance() : 0; ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn-link" data-dismiss="modal">Không, liên hệ tôi ít thôi</a>
            <a href="#" class="btn btn-primary pay-credit-confirm">Đăng nổi bật</a>
        </div>
    </div>
    
    <!-- Success screen -->
    <div class="screen-success">
        <div class="modal-body">
            <div class="content">
                <p>Thật tuyệt vời. Hồ sơ của bạn đã được đăng nổi bật.</p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn pay-credit-close" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
    
    <!-- Error screen -->
    <div class="screen-error">
        <div class="modal-body">
            <div class="content">
                <p class="error-message">Rất tiếc, đã có lỗi xảy ra. Vui lòng liên hệ vLance để được hỗ trợ.</p>
            </div>
            <div class="buy-credit">
                <p>
                    Bạn hiện có <b><span class="credit-balance" data-credit="balance"><?php echo is_object($acc) ? $acc->getCredit()->getBalance() : 0; ?></span> CREDIT</b> -
                    <a href="<?php echo $view['router']->generate('credit_balance') ?>">MUA THÊM CREDIT</a>
                </p>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Đóng lại</a>
        </div>
    </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    var form_spend_credit_feature_freelancer = "#<?php echo $form_name ?>";
    var form_spend_credit_feature_freelancer_params = {"service_id":<?php echo $service_id; ?>,"url":"<?php echo $view['router']->generate('credit_spend',array()) ?>","balance":<?php echo is_object($acc) ? $acc->getCredit()->getBalance() : 0; ?>};
</script>