<form id="userpaymentmethod_create" action="<?php echo $view['router']->generate('userpaymentmethod_create') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php echo $view['form']->widget($form)?>
    <input type="submit" class="btn btn-primary submit" value="<?php echo $view['translator']->trans('userpaymentmethod.create', array(), 'vlance') ?>"/>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        
        var validate_popup_create_payment = function() {
            $('#form-userpaymentmethod_create').validate({
                    rules: {
                           "vlance_paymentbundle_userpaymentmethodtype[title]": {
                               required: true,
                           },
                           "vlance_paymentbundle_userpaymentmethodtype[info]": {
                               required: true
                           }
                    },
                });
            };
        
        
        
        <?php // popver khi hover vào các input ?>
        $('#userpaymentmethod_create .popovers-input').popover();
        $('#userpaymentmethod_edit .popovers-input').popover();
        
        $("#userpaymentmethod_create input.submit").click(function() {
            validate_popup_create_payment();
            if($("#userpaymentmethod_create").valid()== true){
                $('#userpaymentmethod_new').modal('hide');
                $(this).submit();
            }
        })
    });
</script>