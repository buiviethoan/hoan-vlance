<?php $view->extend('VlanceBaseBundle::layout-2columns-left.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('userpaymentmethod_create.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<?php /* Trang list transaction */ ?>
<div class="bank-account-active">
    <h2><?php echo $view['translator']->trans('userpaymentmethod_create.title', array(), 'vlance')?></h2>
    <form action="<?php echo $view['router']->generate('userpaymentmethod_create') ?>" method="post" <?php echo $view['form']->enctype($form)?>>
        <?php echo $view['form']->widget($form)?>
        <input type="submit" class="btn btn-primary" value="<?php echo $view['translator']->trans('userpaymentmethod.create', array(), 'vlance') ?>"/>
    </form>
</div>
<?php $view['slots']->stop(); ?>
