<?php $view->extend('VlanceBaseBundle::layout-2columns-left-small.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('list_userpaymentmethod.title', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<?php $user = $app->getUser(); ?>
<?php /* Trang list transaction */ ?>
<div class="bank-account-active">
    <h1><?php echo $view['translator']->trans('list_userpaymentmethod.active_accounts', array(), 'vlance'); ?></h1>
    <div class="account-bank">
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo $view['translator']->trans('list_userpaymentmethod.bank_account', array(), 'vlance'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($entities as $paymentMethod) : ?>
                <?php /* @var $paymentMethod \Vlance\PaymentBundle\Entity\UserPaymentMethod */ ?>
                <tr>
                    <td><?php echo $paymentMethod->getTitle() ?></td>
                    <td class="a-edit-delete">
                        <a class="a-edit" href="#" data-toggle="modal" data-target="#popup_userpaymentmethod_edit_<?php echo $paymentMethod->getId()?>">
                            <?php echo $view['translator']->trans('common.edit', array(), 'vlance'); ?>
                        </a>
                        
                        <?php /*popup sua tai khoan */ ?>
                            <div class="modal fade" id="popup_userpaymentmethod_edit_<?php echo $paymentMethod->getId()?>">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal">x</a>
                                    <h3><?php echo $view['translator']->trans('common.edit', array(), 'vlance') ?></h3>
                                </div>
                                <div class="modal-body">
                                    <?php echo $view['actions']->render($view['router']->generate('userpaymentmethod_edit',array('id' => $paymentMethod->getId()))); ?>
                                </div>
                            </div>
                        <span class="separater">|</span>
                        <a class="a-delete" href="#" data-toggle="modal" data-target="#popup_userpaymentmethod_delete<?php echo $paymentMethod->getId()?>">
                            <?php echo $view['translator']->trans('common.delete', array(), 'vlance'); ?>
                        </a>
                        <?php // TODO: Khi ấn xóa thỳ ra trang lỗi?>
                        <?php /*popup xoa tai khoan */ ?>
                            <div class="modal fade popup_userpaymentmethod_delete" id="popup_userpaymentmethod_delete<?php echo $paymentMethod->getId()?>">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal">x</a>
                                    <h3><?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?></h3>
                                </div>
                                <div class="modal-body">
                                    <?php echo $view['translator']->trans('list_userpaymentmethod.popup.delete', array('%title%' => $paymentMethod->getTitle()), 'vlance'); ?>
                                </div>
                                <div class="modal-footer">
                                    <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
                                    <a href="<?php echo $view['router']->generate('userpaymentmethod_delete',array('id' => $paymentMethod->getId(), 'token' => $paymentMethod->getDeleteToken())) ?>" 
                                        class="btn btn-primary" title="<?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?>">
                                            <?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?>
                                    </a>
                                </div>
                            </div>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <div class="row-fluid">
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#userpaymentmethod_new"><i class="icon-plus"></i> <?php echo $view['translator']->trans('list_userpaymentmethod.bank.new', array(), 'vlance')?></a>
        </div>
        <?php /*popup nhap tai khoan moi*/ ?>
        <div class="modal fade" id="userpaymentmethod_new">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">x</a>
                <h3><?php echo $view['translator']->trans('list_userpaymentmethod.bank.new', array(), 'vlance') ?></h3>
            </div>
            <div class="modal-body">
                <?php echo $view['actions']->render($view['router']->generate('userpaymentmethod_new')); ?>
            </div>
        </div>
    </div>
    <?php /* will be used later
    <div class="credit-card">
        <table class="table">
            <thead>
                <tr>
                    <th>Thẻ tín dụng</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Chưa có</td>                                        
                </tr>
            </tbody>
        </table>
        <div class="row-fluid">
            <a href="#" class="btn">Nhập tài khoản mới</a>
        </div>
    </div>
    <div class="account-paypal">
        <table class="table">
            <thead>
                <tr>
                    <th>Tài khoản PayPal</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Chưa có</td>
                </tr>
            </tbody>
        </table>
        <div class="row-fluid">
            <a href="#" class="btn">Nhập tài khoản mới</a>
        </div>
    </div>
     */?>
</div>
<?php $view['slots']->stop(); ?>
