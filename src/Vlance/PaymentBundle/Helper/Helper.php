<?php

namespace Vlance\PaymentBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper as BaseHelper;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\Job;
use Vlance\PaymentBundle\Entity\CreditBuyTransaction;
use Vlance\PaymentBundle\Entity\CreditExpenseTransaction;
use Vlance\PaymentBundle\Entity\OnePay;

class Helper extends BaseHelper {
    
    protected $_container;
    protected $_em;
    
    public function __construct(ContainerInterface $container, EntityManager $em) {
        $this->_container = $container;
        $this->_em = $em;
    }
    
    public function getName() {
        return 'vlance_payment';
    }
    
    public function getParameter($path) {
        $paths = explode('.', $path);
        if ($this->_container->hasParameter($paths[0])) {
            $root = $this->_container->getParameter($paths[0]);
            for ($i = 1; $i < count($paths); $i++) {
                if (isset($root[$paths[$i]])) {
                    $root = $root[$paths[$i]];
                } else {
                    return null;
                }
            }
            return $root;
        } else {
            return null;
        }
    }
    
    /**
     * Detect Telephone operator
     * 
     * @param type $telephone
     * $return array
     */
    public function getTelephoneOperator($telephone)
    {
        $telephone = preg_replace('/\D/s', '', (string)($telephone)); // Remove all non-digits characters
        if(strlen($telephone) > 11 || strlen($telephone) < 10){
            return null;
        }
        
        $pattern_viettel        = '/^(086|096|097|098|0162|0163|0164|0165|0166|0167|0168|0169)/';
        $pattern_mobifone       = '/^(089|090|093|0120|0121|0122|0126|0128)/';
        $pattern_vinaphone      = '/^(088|091|094|0123|0124|0125|0127|0129)/';
        // $pattern_vietnamobile   = '/^(092|0188)/';
        
        if(preg_match($pattern_viettel, (string)($telephone))){
            return array (
                'code'      => OnePay::OPERATOR_VIETTEL,
                'opt_rate'  => OnePay::OTP_RATE_VIETTEL
            );
        } elseif(preg_match($pattern_mobifone, (string)($telephone))){
            return array (
                'code'      => OnePay::OPERATOR_MOBIFONE,
                'opt_rate'  => OnePay::OTP_RATE_MOBIFONE
            );
        } elseif(preg_match($pattern_vinaphone, (string)($telephone))){
            return array (
                'code'      => OnePay::OPERATOR_VINAPHONE,
                'opt_rate'  => OnePay::OTP_RATE_VINAPHONE
            );
        } else{
            error_log("Tel: " . $telephone);
            error_log("Operator Other: " . $telephone);
            return array (
                'code'      => OnePay::OPERATOR_OTHER,
                'opt_rate'  => OnePay::OTP_RATE_OTHER
            );
        }
    }
    
    /**
     * add promo credit
     */
    public function addPromotionCredit($user, $amount, $credit, $detail = null){
        if(!($user->getId())){
            return false;
        }
        
        if(!($user->getCredit())){
            return false;
        }
        
        $amount = (int)($amount);
        $credit = (int)($credit);
        
        $promo_transaction = new CreditBuyTransaction();
        $promo_transaction->setPayer($user);
        $promo_transaction->setCreditAccount($user->getCredit());
        $promo_transaction->setType(CreditBuyTransaction::TYPE_PROMO_REFERRAL);
        $promo_transaction->setPaymentMethod(CreditBuyTransaction::PAYMENT_METHOD_OTHER);
        $promo_transaction->setStatus(CreditBuyTransaction::STATUS_TERMINATED);
        $promo_transaction->setAmount($amount); // amount = 0 because this is promotion
        $promo_transaction->setCredit($credit);
        $promo_transaction->setPreBalance($user->getCredit()->getBalance());
        $promo_transaction->setPostBalance($user->getCredit()->getBalance() + $credit);
        $promo_transaction->setDetail(json_encode($detail));
        $this->_em->persist($promo_transaction);

        $user->getCredit()->setBalance($promo_transaction->getPostBalance());
        $this->_em->persist($user->getCredit());

        $this->_em->flush();
        return true;
    }
    
    public function createCreditExpenseTransaction($acc, $amount, $service, $detail = null){
        if(!($acc->getId())){
            return false;
        }
        
        if(!($acc->getCredit())){
            return false;
        }
        
        $amount = (int)($amount);
        
        $credit_trans = new CreditExpenseTransaction();
        $credit_trans->setCreditAccount($acc->getCredit());
        $credit_trans->setPreBalance($acc->getCredit()->getBalance());
        $credit_trans->setStatus(CreditExpenseTransaction::STATUS_TERMINATED);
        $credit_trans->setType(CreditExpenseTransaction::TYPE_EXPENSE);
        $credit_trans->setAmount($amount);
        $credit_trans->setService($service);
        $credit_trans->setDetail($detail);

        $credit_account = $acc->getCredit();
        $credit_account->setBalance($credit_trans->getPreBalance() - $credit_trans->getAmount());

        $credit_trans->setPostBalance($credit_account->getBalance());
        $this->_em->persist($credit_trans);
        $this->_em->persist($credit_account);
        $this->_em->flush();
        return true;
    }
    
    /**
     * Get paying level when contact client
     */
    public function getLevelPayingContactClient($job)
    {
        /* @var $job Job */
        if(!$job){
            return 0;
        }
        if(!is_numeric($job->getBudget())){
            return 0;
        }
        $budget = $job->getBudget();
        $level = 0;
        if($budget <= 500000 && $job->getCategory()->getId() == 22){
            $level = 6;
        } elseif ($budget >= 10000000 && $budget < 35000000){
            $level = 9;
        } elseif($budget > 35000000){
            $level = 10;
        }
        return $level;
    }
}