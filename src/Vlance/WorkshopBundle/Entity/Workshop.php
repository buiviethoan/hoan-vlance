<?php

namespace Vlance\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Workshop
 *
 * @ORM\Table(name="workshop")
 * @ORM\Entity(repositoryClass="Vlance\WorkshopBundle\Entity\WorkshopRepository")
 */
class Workshop
{
    const WORKSHOP_OPEN = 1;
    const WORKSHOP_CLOSE = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\OneToOne(targetEntity="Vlance\JobBundle\Entity\Bid", inversedBy="workshop")
     * @ORM\JoinColumn(name="bid_id", referencedColumnName="id")
     */
    private $bid;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="workshopsJob")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $worker;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Workshop
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Workshop
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Workshop
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set owner
     *
     * @param \Vlance\AccountBundle\Entity\Account $owner
     * @return Workshop
     */
    public function setOwner(\Vlance\AccountBundle\Entity\Account $owner = null)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set workder
     *
     * @param \Vlance\AccountBundle\Entity\Account $workder
     * @return Workshop
     */
    public function setWorker(\Vlance\AccountBundle\Entity\Account $worker = null)
    {
        $this->worker = $worker;
    
        return $this;
    }

    /**
     * Get workder
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * Set bid
     *
     * @param \Vlance\JobBundle\Entity\Bid $bid
     * @return Workshop
     */
    public function setBid(\Vlance\JobBundle\Entity\Bid $bid = null)
    {
        $this->bid = $bid;
    
        return $this;
    }

    /**
     * Get bid
     *
     * @return \Vlance\JobBundle\Entity\Bid 
     */
    public function getBid()
    {
        return $this->bid;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return Workshop
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }
}