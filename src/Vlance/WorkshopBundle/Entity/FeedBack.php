<?php

namespace Vlance\WorkshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * FeedBack
 *
 * @ORM\Table(name="feedback")
 * @ORM\Entity(repositoryClass="Vlance\WorkshopBundle\Entity\FeedBackRepository")
 */
class FeedBack
{
   
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $sender;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="feedBacksReceiver")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $receiver;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Vlance\JobBundle\Entity\Job", inversedBy="feedBacksJob")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $job;
    
   /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;
    
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="quality", nullable=true, type="integer", options={"default" : "0"})
     */
    private $quality = 0;
   
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="degree", type="integer", nullable=true, options={"default" : "0"})
     */
    private $degree = 0;
    
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="price", nullable=true, type="integer", options={"default" : "0"})
     */
    private $price = 0;
    
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="duration", nullable=true, type="integer", options={"default" : "0"})
     */
    private $duration = 0;
    
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="reply_sms", nullable=true, type="integer", options={"default" : "0"})
     */
    private $reply_sms = 0;
    
    /**
     * @var integer
     * @Assert\Range(
     *          min = 0,
     *          max = 5
     *          )
     * @ORM\Column(name="professional", nullable=true, type="integer", options={"default" : "0"})
     */
    private $professional = 0;
    
    /**
     * @var boolean
     * @ORM\Column(name="recommend", nullable=true, type="boolean", options={"default" : "1"})
     * 
     */
    private $recommend = 1;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return FeedBack
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set quality
     *
     * @param integer $quality
     * @return FeedBack
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    
        return $this;
    }

    /**
     * Get quality
     *
     * @return integer 
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set sender
     *
     * @param \Vlance\AccountBundle\Entity\Account $sender
     * @return FeedBack
     */
    public function setSender(\Vlance\AccountBundle\Entity\Account $sender = null)
    {
        $this->sender = $sender;
    
        return $this;
    }

    /**
     * Get sender
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \Vlance\AccountBundle\Entity\Account $receiver
     * @return FeedBack
     */
    public function setReceiver(\Vlance\AccountBundle\Entity\Account $receiver = null)
    {
        $this->receiver = $receiver;
    
        return $this;
    }

    /**
     * Get receiver
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set job
     *
     * @param \Vlance\JobBundle\Entity\Job $job
     * @return FeedBack
     */
    public function setJob(\Vlance\JobBundle\Entity\Job $job = null)
    {
        $this->job = $job;
    
        return $this;
    }

    /**
     * Get job
     *
     * @return \Vlance\JobBundle\Entity\Job 
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set degree
     *
     * @param integer $degree
     * @return FeedBack
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;
    
        return $this;
    }

    /**
     * Get degree
     *
     * @return integer 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return FeedBack
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return FeedBack
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set reply_sms
     *
     * @param integer $replySms
     * @return FeedBack
     */
    public function setReplySms($replySms)
    {
        $this->reply_sms = $replySms;
    
        return $this;
    }

    /**
     * Get reply_sms
     *
     * @return integer 
     */
    public function getReplySms()
    {
        return $this->reply_sms;
    }

    /**
     * Set professional
     *
     * @param integer $professional
     * @return FeedBack
     */
    public function setProfessional($professional)
    {
        $this->professional = $professional;
    
        return $this;
    }

    /**
     * Get professional
     *
     * @return integer 
     */
    public function getProfessional()
    {
        return $this->professional;
    }

    /**
     * Set recommend
     *
     * @param boolean $recommend
     * @return FeedBack
     */
    public function setRecommend($recommend)
    {
        $this->recommend = $recommend;
    
        return $this;
    }

    /**
     * Get recommend
     *
     * @return boolean 
     */
    public function getRecommend()
    {
        return $this->recommend;
    }
}