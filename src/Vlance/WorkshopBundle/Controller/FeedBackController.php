<?php

namespace Vlance\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Vlance\WorkshopBundle\Entity\FeedBack;
use Vlance\WorkshopBundle\Form\FeedBackType;
use Vlance\WorkshopBundle\Entity\FeedBackRepository;
use Vlance\BaseBundle\Event\VlanceEvents;
use Doctrine\Common\Collections\Criteria;
use Vlance\JobBundle\Entity\Job;

/**
 * Project controller.
 *
 * @Route("/feedback")
 */
class FeedBackController extends Controller
{
    
    /**
     * Creates a new FeedBack entity.
     *
     * @Route("/new/{job_id}/{bid}", name="feedback_create")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function createAction(Request $request, $job_id, $bid)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        //Chuyen ve trang chu neu trang thai cong viec chua ket thuc
        if($job->getStatus() != Job::JOB_FINISH_REQUEST
                && $job->getStatus() != Job::JOB_FINISHED
                && $job->getStatus() != Job::JOB_CANCEL_REQUEST
                && $job->getStatus() != Job::JOB_CANCELED) {
            return $this->redirect($this->generateUrl("500_page"), 301);
        }
        
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $bid));
        
        if($job->getAccount() == $acc){
            // Client send the feedback
            $feedback = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy(array('job' => $job,'sender' => $acc, 'receiver' => $entity_bid->getAccount()));
        } else{
            // Freelancer sends the feedback
            $feedback = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy(array('job' => $job,'sender' => $acc, 'receiver' => $job->getAccount()));
        }
        
        //Chuyen ve trang chu neu tai khoan dang nhap da danh gia
        if(!empty($feedback)) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        $entity  = new FeedBack();
        $form = $this->createForm(new FeedBackType(), $entity);
        $form->bind($request);
        
        $acc_worker = $job->getWorker();
        if($form->isValid()){
            $entity->setSender($acc);
            if($acc == $acc_worker) {
                 $entity->setReceiver($job->getAccount());
            }
            elseif($acc == $job->getAccount()) {
                // sua lai truong score va numjobfinish cho freelancer
                $entity->setReceiver($acc_worker);
                $acc_worker->setNumReviews($acc_worker->getNumReviews() + 1);
                $score = ($entity->getDegree() + $entity->getDuration() + $entity->getPrice() + $entity->getProfessional() + $entity->getQuality())/5;
                $acc_worker->setScore($acc_worker->getScore() + $score);
            
                $em->persist($acc_worker);
            }
            $entity->setJob($job);
            $em->persist($entity);
            
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',$this->get('translator')->trans('controller.feedback.sendsuccess',array(),'vlance'));
            return $this->redirect($this->generateUrl('workshop_bid',array('job_id' => $job_id, 'bid' => $bid)));
        }
        $this->get('session')->getFlashBag()->add('error',$this->get('translator')->trans('controller.feedback.notvalid',array(),'vlance'));
        return $this->redirect($this->generateUrl('workshop_bid',array('job_id' => $job_id, 'bid' => $bid)));
    }

    /**
     * Displays a form to create a new FeedBack entity.
     *
     * @Route("/new/{job_id}/{bid}", name="feedback_new")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function newAction($job_id, $bid)
    {
//        Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $acc = $this->get('security.context')->getToken()->getUser();
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        //$feedback = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy(array('job' => $job,'sender' => $job->getAccount()));
        $entity = new FeedBack();
        $form   = $this->createForm(new FeedBackType(), $entity);
        if($acc == $job->getWorker()) {
            return $this->render('VlanceWorkshopBundle:FeedBack:new_freelancer.html.php',array(
                                           'entity' => $entity,
                                           'form' => $form->createView(),
                                           'job_id' => $job_id,
                                           'bid'    => $bid,
                                         )
                                );
        }
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'job_id'  => $job_id,
            'bid'     => $bid,
        );
    }
    
    /**
     * Lists all FeedBack entities.
     *
     * @Route("/{filters}", name="feedback_list", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request, $filters = "")
    {
        $param = $this->get('vlance_base.url')->pasteParameter($filters);
        $this->get('session')->set('filter',$param);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy($param);
        return array(
            'entities' => $entities,
        );
    }
   
}
