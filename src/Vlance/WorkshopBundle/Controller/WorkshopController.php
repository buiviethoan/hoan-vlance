<?php

namespace Vlance\WorkshopBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Vlance\WorkshopBundle\Entity\Workshop;
use Vlance\JobBundle\Entity\Bid;
use Vlance\JobBundle\Entity\Message;
use Vlance\WorkshopBundle\Form\WorkshopType;
use Symfony\Component\HttpFoundation\Response;
use Vlance\JobBundle\Entity\Job;
use Vlance\JobBundle\Entity\BidRepository;
use Vlance\BaseBundle\Utils\HasPermission;

/**
 * Workshop controller.
 *
 * @Route("/")
 */
class WorkshopController extends Controller
{
    
    /**
     * Display all freelancer bid or information of a job
     *
     * @Route("/workshop/account/{job_id}/{bid_idms}", name="workshop_account_bid")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function leftWorkroomAction($job_id, $bid_idms = '')
    {
        
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        $bid_awarded = $em->getRepository('VlanceJobBundle:Bid')->isNotNull($job, 'awardedAt');
        $bids = $em->getRepository('VlanceJobBundle:Bid')->findBy(array('job' => $job, 'awardedAt' => null));
        if(count($bid_awarded) < 1 && count($bids) < 1) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
        if($job->getType() == Job::TYPE_CONTEST){
            $bid_awarded = $em->getRepository('VlanceJobBundle:Bid')->isNotNull($job, 'wonAt');
        }
        
        $allowed_bids = array();
        $declined_bids = array();
        /* @var $b \Vlance\JobBundle\Entity\Bid */
        foreach($bids as $b){
            if($b->getIsDeclined()){
                $declined_bids[] = $b;
            } else {
                $allowed_bids[] = $b;
            }
        }
        // Lay feed back cua job hien tai
        $acc = $this->get('security.context')->getToken()->getUser();
        $current_bid = $em->getRepository('VlanceJobBundle:Bid')->findOneBy(array('id' => $bid_idms));
        if($job->getAccount() == $acc){
            // Client is viewing
            if(is_object($current_bid)){
                $feedback = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy(array('sender' => $acc, 'job' => $job, 'receiver' => $current_bid->getAccount()));
            }
        } else{
            // Freelancer is viewing
            $feedback = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findBy(array('sender' => $acc, 'job' => $job, 'receiver' => $job->getAccount()));
        }
        
        
        return array(
                'bid_awarde'    => $bid_awarded,
                'bids'          => $allowed_bids,
                'declined_bids' => $declined_bids,
                'job'           => $job,
                'num_feed'      => isset($feedback) ? count($feedback) : 0,
                'bid_idms'      => $bid_idms
        );
    }

    /**
     * Finds and displays a Workshop entity.
     *
     * @Route("/workroom/{job_hash}/", name="workshop_show")
     * @Method("GET")
     * @Template(engine = "php")
     */
    public function showWorkroomAction(Request $request, $job_hash)
    {
        // Check referer
        $referer = $request->headers->get('referer');
        if($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
        
        // Check authentication
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        // Check job
        $job = $em->getRepository('VlanceJobBundle:Job')->findOneByHash($job_hash);
        if(!$job) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Check workroom
        $workshops = $em->getRepository('VlanceWorkshopBundle:Workshop')->findBy(array('job' => $job));
        if(!$workshops) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Nếu là khách hàng hoặc admin, redirect sang workroom có bid được giao việc, hoặc là bid đầu tiên
        if($acc_login === $job->getAccount() || HasPermission::hasAdminPermission($acc_login) == true){
            $bid_awarded = $em->getRepository('VlanceJobBundle:Bid')->isNotNull($job, 'awardedAt');
            if(count($bid_awarded) > 0){
                if($bid_awarded[0]){
                    return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $bid_awarded[0]->getId())), 302);
                }
            }
        }
        
        // Nếu là freelancer, Redirect sang đúng workroom của họ
        else{
            foreach($workshops as $wr){
                if($wr->getBid()){
                    if($acc_login === $wr->getBid()->getAccount()){
                        return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $wr->getBid()->getId())), 302);
                    }
                }
            }
        }
        
        // Nếu ko, redirect sang bid đầu tiên
        $first_workshop = reset($workshops);
        return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $first_workshop->getBid()->getId())), 302);
    }
    
    /**
     * Finds and displays a Workshop entity.
     *
     * @Route("/workshop/{job_id}/{bid}", name="workshop_bid", requirements={"job_id" = "\d+", "bid" = "\d+"})
     * @Method("GET")
     * @Template(engine = "php")
     */
    public function showAction(Request $request, $job_id, $bid = null)
    {
        $bid_idms = $request->get('_route_params');
        $referer = $request->headers->get('referer');
        if($referer == '') {
            $referer = $this->generateUrl('vlance_homepage');
        }
         /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        if(is_null($acc_login->getLastLogin())){
            $now = new \DateTime('now');
            $acc_login->setLastLogin($now); 
            $em->persist($acc_login);
            $em->flush();
        }
        
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
        $workshops = $em->getRepository('VlanceWorkshopBundle:Workshop')->findBy(array('job' => $job));
        // Khong ton tai job trong database
        if(!$job || !$workshops) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Redirect to the first workshop if bid_id is not set
        if(!$bid_idms['bid']){
            // Nếu là khách hàng hoặc admin, redirect sang workroom có bid được giao việc, hoặc là bid đầu tiên
            if($acc_login === $job->getAccount() || HasPermission::hasAdminPermission($acc_login) == true){
                $bid_awarded = $em->getRepository('VlanceJobBundle:Bid')->isNotNull($job, 'awardedAt');
                if(count($bid_awarded) > 0){
                    if($bid_awarded[0]){
                        return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $bid_awarded[0]->getId())), 302);
                    }
                }
                $first_workshop = reset($workshops);
                return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $first_workshop->getBid()->getId())), 302);
            } 
            // Nếu là freelancer, Redirect sang đúng workroom của họ
            else{
                foreach($workshops as $wr){
                    if($wr->getBid()){
                        if($acc_login === $wr->getBid()->getAccount()){
                            return $this->redirect($this->generateUrl("workshop_bid", array('job_id' => $job->getId(), 'bid' => $first_workshop->getBid()->getId())), 302);
                        }
                    }
                }
            }
        }
        
        // mang luu tru tin nhan moi.
        $new_messages = array();
     
        $this->get('session')->set('current_job', '');
        if($job_id){
            $this->get('session')->set('current_job', $job_id);
        }
        
        //Mac dinh bid = null
        if($bid == null) {
             //Job da duoc awarded
            if($job->getStatus() != Job::JOB_OPEN) {
                $entity_bids = $em->getRepository('VlanceJobBundle:Bid')->isNotNull($job, 'awardedAt');
                if(!$entity_bids) {
                    return $this->redirect($this->generateUrl("404_page"), 301);
                }
                
                // Account dang nhap khong phai nguoi tao job hoac nguoi bid viec
                if($acc_login !== $entity_bids[0]->getAccount() && $acc_login !== $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.show.not_permission', array(), 'vlance'));
                    return new RedirectResponse($referer);
                }
                $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('bid' => $entity_bids[0]),array('createdAt' => 'DESC'));
                
            } else {
                $entity_bids = array();
                $users_bid = array();
                foreach($workshops as $workshop) {
                    $entity_bids[] = $workshop->getBid();
                    $users_bid[] = $workshop->getWorker()->getId();
                    if($acc_login === $workshop->getWorker()) {
                        $entity_bids[0] = $workshop->getBid();
                    }
                }

                // Account dang nhap khong phai nguoi tao job hoac nguoi bid viec
                if(!in_array($acc_login->getId(), $users_bid) && $acc_login != $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
                    $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.show.not_permission', array(), 'vlance'));
                    return new RedirectResponse($referer);
                }
                $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('bid' => $entity_bids[0]),array('createdAt' => 'DESC'));
            }
            
            //update trang thai message thanh da doc, luu tin nhan moi vao mang new_messages
            foreach($entities as $entity) {
                $new_messages[$entity->getId()] = 0;
                if($entity->getStatus() == Message::NEW_MESSAGE && $entity->getSender() !== $acc_login && HasPermission::hasAdminPermission($acc_login) == FALSE) {
                    $new_messages[$entity->getId()] = 1;
                    $entity->setStatus(Message::READ_MESSAGE);
                    $em->persist($entity);
                }
            }
            $em->flush();
            
            return array(
                'entities'      => $entities,
                'bid'           => $entity_bids[0],
                'job'           => $job,
                'new_messages'  => $new_messages,
                'bid_idms'      => $bid_idms,
            );
        }
            
        $entity_bid = $em->getRepository('VlanceJobBundle:Bid')->find($bid);
        
        //Khong ton tai bid trong database
        if(!$entity_bid) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Account dang nhap khong phai nguoi tao job hoac nguoi bid viec
        if($acc_login !== $entity_bid->getAccount() && $acc_login !== $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.workshop.show.not_permission', array(), 'vlance'));
            return new RedirectResponse($referer);
        }
        
        $entities = $em->getRepository('VlanceJobBundle:Message')->findBy(array('bid' => $bid),array('createdAt' => 'DESC'));
        
         //update trang thai message thanh da doc, luu tin nhan moi vao mang new_messages
        foreach($entities as $entity) {
            $new_messages[$entity->getId()] = 0;
            // Trang thai tin nhan la NEW_MESSAGE va nguoi nhan message la account dang dang nhap 
            if($entity->getStatus() == Message::NEW_MESSAGE && $entity->getReceiver() === $acc_login) {
                $new_messages[$entity->getId()] = 1;
                $entity->setStatus(Message::READ_MESSAGE);
                $em->persist($entity);
            }
        }
        $em->flush();
     
        return array(
            'entities'      => $entities,
            'bid'           => $entity_bid,
            'job'           => $job,
            'new_messages'  => $new_messages,
            'bid_idms'      => $bid_idms,
        );
    }
    
    /**
     * Request cancel job by client when they found that freelancer didn't work well
     * They need to send a message
     *
     * @Route("/workshop/cancel/{job_id}", name="workshop_cancel")
     * @Method("GET")
     */
    public function cancelAction($job_id) {
        // Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $acc_login = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        /* @var $job \Vlance\JobBundle\Entity\Job */
        $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);

        // If job is not found
        if (!$job) {
            return $this->redirect($this->generateUrl("404_page"), 302);
        }
        //If job is not working, can not request to cancel
        if ($job->getStatus() !== \Vlance\JobBundle\Entity\Job::JOB_WORKING) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        // If current user is not job owner
        if ($acc_login !== $job->getAccount() && HasPermission::hasAdminPermission($acc_login) == FALSE) {
            return $this->redirect($this->generateUrl("500_page"), 302);
        }
        
//        $job->setStatus(Job::JOB_CANCEL_REQUEST);
        $job->setCancelStatus(Job::STATUS_CANCEL_REQUEST);
        $em->persist($job);

        $awardedBid = $em->getRepository('VlanceJobBundle:Job')->getAwardedBid($job);
        // Create message to send to freelancer in workshop
        $message = new Message();
        $message->setSender($em->getRepository('VlanceAccountBundle:Account')->find(\Vlance\AccountBundle\Entity\Account::USER_VLANCE_SYS));
        $message->setReceiver($job->getWorker());
        $message->setStatus(Message::NEW_MESSAGE);
        $message->setBid($awardedBid);
        $message->setContent($this->get('translator')->trans('controller.workshop.cancel.message.freelancer', array("%%client%%" => $acc_login->getFullName()), 'vlance'));
        $em->persist($message);

        $em->flush();

        // Gui email thong bao cong viec da bi huy bo boi khach hang
        // to admin 
        $email_hotro = $this->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
        $email_admin = $this->get('vlance_base.helper')->getParameter('vlance_email.to.antispam');
        
        $email_context = array('job' => $job);
        $template = "VlanceWorkshopBundle:Email:job_cancel_admin.html.twig";
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $email_context, $email_hotro, $email_admin);
        
        $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('controller.workshop.cancel.request.success', array(), 'vlance'));
        return $this->redirect($this->generateUrl('workshop_bid', array('job_id' => $job_id)));
    }

    /**
     * Lists all Workshop entities.
     *
     * @Route("/workshop/{filters}", name="workshop", requirements={"filters" = ".+"})
     * @Method("GET")
     * @Template(engine = "php")
     */
    public function indexAction(Request $request, $filters = "")
    {
        $params = $this->get('vlance_base.url') ->pasteParameter($filters);
        $this->get('session')->set('filter',$params);
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceWorkshopBundle:Workshop')->findBy($params);

        return array(
            'entities' => $entities,
        );
    }
}
