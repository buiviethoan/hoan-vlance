<?php

namespace Vlance\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WorkshopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('project')
            ->add('owner')
            ->add('workder')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\WorkshopBundle\Entity\Workshop'
        ));
    }

    public function getName()
    {
        return 'vlance_workshopbundle_workshoptype';
    }
}
