<?php

namespace Vlance\WorkshopBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FeedBackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quality','choice',array(
                'label' => 'workshop.feedback.quality',
                'required' => true,
                'expanded'=> true,
                'translation_domain' => 'vlance',
                'empty_data' => null,
                'choices' => array(
                    '1' => false,
                    '2' => false,
                    '3' => false,
                    '4' => false,
                    '5' => false,
                )
            ))
          ->add('degree','choice',array(
              'label' => 'workshop.feedback.mailer_degrees',
              'required' => true,
              'expanded'=> true,
              'translation_domain' => 'vlance',
              'empty_data' => null,
              'choices' => array(
                '1' => false,
                '2' => false,
                '3' => false,
                '4' => false,
                '5' => false,
                )
            ))
          ->add('price','choice',array(
              'label' => 'workshop.feedback.price',
             'required' => true,
             'expanded'=> true,
             'translation_domain' => 'vlance',
             'empty_data' => null,
             'choices' => array(
                    '1' => false,
                    '2' => false,
                    '3' => false,
                    '4' => false,
                    '5' => false,
                )
            ))
         ->add('duration','choice',array(
             'label' => 'workshop.feedback.time',
             'required' => true,
             'expanded'=> true,
             'translation_domain' => 'vlance',
             'empty_data' => null,
             'choices' => array(
                    '1' => false,
                    '2' => false,
                    '3' => false,
                    '4' => false,
                    '5' => false,
                )
            ))
        ->add('reply_sms','choice',array(
            'label' => 'workshop.feedback.really',
            'required' => true,
            'expanded'=> true,
            'translation_domain' => 'vlance',
            'empty_data' => null,
            'choices' => array(
                '1' => false,
                '2' => false,
                '3' => false,
                '4' => false,
                '5' => false,
            )
            ))
       ->add('professional','choice',array(
           'label' => 'workshop.feedback.professional',
           'required' => true,
            'expanded'=> true,
            'translation_domain' => 'vlance',
            'empty_data' => null,
            'choices' => array(
                '1' => false,
                '2' => false,
                '3' => false,
                '4' => false,
                '5' => false,
            )
       ))
       ->add('recommend','choice',array(
           'label' => 'workshop.feedback.recommended',
           'required' => true,
           'translation_domain' => 'vlance',
           'empty_data' => null,
           'wrapper_class' => 'recommend',
           'choices' => array(
               '1' => 'common.okay',
               '0' => 'common.no',
           )
       ))
       ->add('comment','textarea',array(
           'label' => 'workshop.feedback.comment',
           'translation_domain' => 'vlance',
           'required' => true,
           'wrapper_class' => 'comment',
       ))

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\WorkshopBundle\Entity\FeedBack'
        ));
    }

    public function getName()
    {
        return 'vlance_workshopbundle_feedbacktype';
    }
}
