<?php

namespace Vlance\WorkshopBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Event\BidEvent;
use Vlance\JobBundle\Event\JobEvent;
use Vlance\WorkshopBundle\Entity\Workshop;

class WorkshopListener implements EventSubscriberInterface
{
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            JobEvent::JOB_AWARDED_SUCCESS   => 'createOrUpdateWorkshop',
            JobEvent::JOB_FIRST_MESSAGE     => 'createOrUpdateWorkshop',
        );
    }

    public function createOrUpdateWorkshop(BidEvent $event)
    {
        /* @var $bid \Vlance\JobBundle\Entity\Bid */
        $bid = $event->getBid();
        $entity = $bid->getWorkshop();
        if (is_null($entity)) {
            $entity = new Workshop();
            $entity->setBid($bid);
            $entity->setJob($bid->getJob());
            $entity->setOwner($bid->getJob()->getAccount());
            $entity->setWorker($bid->getAccount());
            $entity->setStatus(Workshop::WORKSHOP_OPEN);
            $this->em->persist($entity);
        }
    }
}
