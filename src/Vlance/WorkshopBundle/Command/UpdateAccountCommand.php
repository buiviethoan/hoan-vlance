<?php
namespace Vlance\WorkshopBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;

use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManager;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Utils\JobCalculator;

class UpdateAccountCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
                ->setName('vlance:account:score')
                ->setDescription('Update field score of account')
                ->addArgument('action',  InputArgument::REQUIRED,'Action that you want to execute');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $action = $input->getArgument('action');
        switch ($action) {
            case 'updateAccount' :
                $output->writeln('================= Start update account');
                $this->updateAccount();
                $output->writeln('================= Finished update account');
                break;
            
            default:
                break;
        }
        
    }

    protected function updateAccount() {
        /* @var $em EntityManager*/
        $em = $this->getContainer()->get('doctrine')->getManager();
        //Neu la Account slug = getFullName
            $feedbacks = $em->getRepository('VlanceWorkshopBundle:FeedBack')->findAll();
            /* @var $feedback \Vlance\WorkshopBundle\Entity\FeedBack*/
            foreach ($feedbacks as $feedback) {
                if($feedback->getPrice() && $feedback->getQuality() && $feedback->getDegree() && $feedback->getDuration() && $feedback->getProfessional()) {
                    $score = ($feedback->getPrice() + $feedback->getQuality() + $feedback->getDegree() + $feedback->getDuration() + $feedback->getProfessional())/5;
                    $account = $feedback->getReceiver();
                    $account->setScore($account->getScore() + $score);
                    $em->persist($account);
                }
            }
            $em->flush();

    }
    
}