<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('list_working.project', array(), 'vlance')); ?>
<?php $view['slots']->start('content') ?>
<div class="content-section container">
    <div class="row-fluid">
        <div class="span12">
            <h1 class="title"><?php echo $view['translator']->trans('list_working.title', array(), 'vlance') ?></h1>
            <div class="row-fluid page-tabs">
                <ul>
                    <li class="active"><a href="#"><?php echo $view['translator']->trans('list_working.project', array(), 'vlance') ?></a></li>
                </ul>
                <div class="border"></div>
            </div>
            <div class="row-fluid">
                <div class="span12 news-client-view">
                    <?php 
                        $msg = '';
                        if(!isset($jobs) && $type == Vlance\AccountBundle\Entity\Account::TYPE_CLIENT ) {
                           $msg = 'Bạn đang chưa có dự án nào. Hàng ngàn freelandcer đang sẵn sàng nhận việc, hãy tối ưu hóa đồng tiền của bạn.'; 
                        }
                        if(!isset($jobs) && $type = Vlance\AccountBundle\Entity\Account::TYPE_FREELANCER) {
                            $msg = 'Bạn chưa có dự án nào. Bạn có thể làm profile của mình sexy hơn theo hướng dẫn sau.';
                        }
                         
                    ?>
                    <?php if(isset($jobs)) : ?>
                    <div class="span3"><label for="time_working"><?php echo $view['translator']->trans('list_working.content.time', array(), 'vlance').':' ?></label> <select class="span6" id="time_working" onchange="window.open(this.options[this.selectedIndex].value,'_parent')"><?php echo $view['actions']->render($view['router']->generate('project_list_duration')) ?></select></div>
                    <div class="span4 offset1"><label for="status_working"><?php echo $view['translator']->trans('list_working.content.status', array(), 'vlance').':' ?></label> <select class="span8" id="status_working" onchange="window.open(this.options[this.selectedIndex].value,'_parent')"><?php echo $view['actions']->render($view['router']->generate('project_list_status'))?></select></div>
                    <script>
                        $(document).ready(function() {
                            var urlcurrent=window.location.pathname ;
                            $("select#time_working option").filter(function() {
                                    return $(this).val() == urlcurrent; 
                                }).prop('selected', true);
                            $("select#status_working option").filter(function() {
                                    return $(this).val() == urlcurrent; 
                                }).prop('selected', true);
                        });
                    </script>
                    
                     <div class="row-fluid span12"><?php echo $msg; ?></div>
                     
                    <?php if(count($jobs) != 0): ?>
                    <table class="table">
                            <tr>
                                <th class="project-freelancer"><?php echo $view['translator']->trans('list_working.content.customer_projects', array(), 'vlance')?></th>
                                <th><?php echo $view['translator']->trans('list_working.content.payment', array(), 'vlance')?></th>
                                <th><?php echo $view['translator']->trans('list_working.content.status', array(), 'vlance')?></th>
                                <th><?php echo $view['translator']->trans('list_working.content.sum_money', array(), 'vlance')?></th>
                            </tr>
                            <?php $count = 0?>
                            <?php foreach ($jobs as $job) : ?>
                            <tr class="<?php echo ++$count%2 ? "odd" : "even"  ?>">
                                    <td class="project-freelancer doing">
                                        <div class="title-job"><a href="<?php echo $view['router']->generate('job_show',array('id' => $job['job_id'])) ?>"><?php echo ucfirst($job['job_title']) ?></a></div>
                                        <?php if(isset($job['acc_job_id']) && $acc_id == $job['acc_job_id']) {?>
                                        <div class="freelancer"><?php echo $view['translator']->trans('list_working.content.freelancer', array(), 'vlance').':'?><a href="#"> <?php echo $job['bid_name'];  ?></a></div>
                                        <?php } else { ?>
                                        <div class="client"><?php echo $view['translator']->trans('list_working.content.client', array(), 'vlance').':'?><a href="#"> <?php echo $job['job_name']; ?></a></div>
                                        <?php } ?>
                                        <div class="start_date"><?php echo $view['translator']->trans('list_working.content.start_date', array(), 'vlance').':'?> <?php echo $job['pro_id'] ? $job['pro_start']->format('d/m/Y') : $job['job_start']->format('d/m/Y') ?></div>
                                    </td>
                                    <td><?php //echo $job['pro_payment'] ? \Vlance\BaseBundle\Utils\GetStatus::getPaymentProject($job['pro_payment']) :  $view['translator']->trans('list_working.content.no_deposit', array(), 'vlance') ?></td>
                                    <td class="doing"><?php //echo $job['pro_status'] ? \Vlance\BaseBundle\Utils\GetStatus::getStatusProject($job['pro_status']) :  $view['translator']->trans('list_working.content.not_assigned', array(), 'vlance') ?></td>
                                    <td>
                                        <?php echo number_format( $job['pro_budget'] ? $job['pro_budget'] : $job['job_budget'] ,'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                     <div class="row-fluid">
                        <p class="layered-footer results-paging span12">
                            <span>1</span> 
                            <a href="#">2</a> 
                            <a href="#">3</a> 
                            <a href="#">4</a> 
                            <a href="#">5</a> 
                            <span>...</span>
                            <a href="#">Trang cuối</a>
                        </p>
                    </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>
                    

    
<?php $view['slots']->stop(); ?>
