<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\JobBundle\Entity\Message; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $status = $job->getStatus(); ?>
<?php $count_message = array(); ?>
<?php $finish = $job->getCloseAt()->format('d-m-Y H:i'); ?>
<?php $now = new \DateTime('now'); ?>
<?php $remain = JobCalculator::ago($finish,$now->format('d-m-Y H:i')); ?>
<?php $i = 0; ?>

<?php // View of client (job owner) or vLance Admin ?>
<?php if($acc == $job->getAccount() || HasPermission::hasAdminPermission($acc) == TRUE) : ?>
    <h2><?php echo $view['translator']->trans('workshop.menu_left.title_list_free', array(), 'vlance') ?></h2>
    <?php /*<!--Messenger vs freelance duoc giao viec-->*/?>
    <?php if($job->getStatus() != Job::JOB_OPEN && count($bid_awarde) > 0) : ?>
        <?php 
            $count = 0;
            $messages_worker = $bid_awarde[0]->getMessages();
            foreach($messages_worker as $message_worker) {
                if($message_worker->getStatus() == Message::NEW_MESSAGE && $message_worker->getReceiver() == $acc){
                    $count++;
                }
            }
        ?>
        <?php if($job->getType() === Job::TYPE_BID): ?>
        <ul>
            <li>
                <a class="awarde <?php if($bid_idms == $bid_awarde[0]->getId()){echo 'active';}?>" 
                   href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $bid_awarde[0]->getId())) ?>">
                    <span><?php echo htmlentities($bid_awarde[0]->getAccount()->getFullName(), ENT_SUBSTITUTE, "UTF-8"); ?><?php if($count>0):?> (<?php echo $count; ?>)<?php endif;?></span>
                    <span class="i16 i16-bid-awarde"></span>
                </a>
            </li>
        </ul>
        <?php endif; ?>
    <?php endif; ?>
    <?php /*<!--End messenger vs freelance duoc giao viec-->*/?>

    <?php /*<!--Messenger vs freelance ko duoc giao viec-->*/?>
    <ul class="freelancer-list">
    <?php
        $count_message = array();
        foreach($bids as $bid) {
            $messages = $bid->getMessages();
            if(count($messages) > 0) { ?>
                <?php 
                    $count_message[$bid->getId()] = 0;
                    foreach($messages as $message) {                       
                        if($message->getStatus() == Message::NEW_MESSAGE && $message->getReceiver() == $acc){
                            $count_message[$bid->getId()] ++;
                        }
                    }
                ?>
                <li>
                    <a class="<?php if($bid_idms == $bid->getId()){echo 'active';}?>" 
                       href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $bid->getId())) ?>">
                    <?php echo $bid->getAccount()->getFullName(); ?><?php if($count_message[$bid->getId()]>0):?> (<?php echo $count_message[$bid->getId()] ?>)<?php endif;?>
                    <?php if($job->getType() === Job::TYPE_CONTEST):?>
                        <?php if($bid->getWonAt()): ?>
                            <span class="i16 i16-bid-awarde"></span>
                        <?php endif; ?>
                    <?php endif; ?>
                    </a>
                </li>
            <?php   }
        } ?>
    </ul>
    <?php /*<!--end Messenger vs freelance ko duoc giao viec-->*/?>
    
    <?php /*<!-- Freelancer bị từ chối -->*/?>
    <?php if(count($declined_bids) > 0): ?>
        <h2 class="declined"><?php echo $view['translator']->trans('workshop.menu_left.title_list_declined', array(), 'vlance') ?></h2>
        <ul class="freelancer-list declined">
        <?php
            $count_message = array();
            foreach($declined_bids as $bid) {
                $messages = $bid->getMessages();
                if(count($messages) > 0) { ?>
                    <?php 
                        $count_message[$bid->getId()] = 0;
                        foreach($messages as $message) {                       
                            if($message->getStatus() == Message::NEW_MESSAGE && $message->getReceiver() == $acc){
                                $count_message[$bid->getId()] ++;
                            }
                        }
                    ?>
                    <li>
                        <a class="<?php if($bid_idms == $bid->getId()){echo 'active';}?>" 
                           title="<?php echo $view['translator']->trans('workshop.menu_left.title_list_declined', array(), 'vlance') ?>"
                           href="<?php echo $view['router']->generate('workshop_bid',array('job_id' => $job->getId(),'bid' => $bid->getId())) ?>">
                            <?php echo $bid->getAccount()->getFullName(); ?><?php if($count_message[$bid->getId()]>0):?> (<?php echo $count_message[$bid->getId()] ?>)<?php endif;?>
                        </a>
                    </li>
                <?php   }
            } ?>
        </ul>
    <?php endif; ?>
    <?php /*<!-- END Freelancer bị từ chối -->*/?>
    
    <?php if(count($bid_awarde) > 0): ?>
        <?php if($bid_idms == $bid_awarde[0]->getId()): ?>
            <div class='below-listing row-fluid'>
                <?php /* Display button allow client to request to cancel this job */?>
                <?php if($status == Job::JOB_WORKING) : ?>
                    <div class="workroom-remind grey">
                        <div class='row-fluid'>
                            <h5 class='span12 inner'><?php echo $view['translator']->trans('form.job.workroom-remind.block2.heading_guider', array(), 'vlance'); ?></h5>
                        </div>
                        <div class='row-fluid'>
                            <div class='span12 inner'>
                                <a class="btn btn-primary" href="#"
                                        onclick="if(confirm('Bạn chắc chắn đã nhận được đầy đủ toàn bộ sản phẩm từ freelancer? Vui lòng xác nhận dự án đã hoàn thành.')){location.href = '<?php echo $view['router']->generate('job_update_status',array('bid' => $bid_awarde[0]->getId(), 'status' => Job::JOB_FINISHED )) ?>'; }return true;">
                                    <?php echo $view['translator']->trans('workshop.menu_left.button.finish.accept', array(), 'vlance') ?>
                                </a>
                            </div>
                        </div>
                        <div class='row-fluid'>
                            <div class='span12 inner'>
                                <p class='text-small'><?php echo $view['translator']->trans('form.job.workroom-remind.block2.guide_p1', array(), 'vlance'); ?>
                                    <a class='text-red' href="#" onclick="if(confirm('BẠN ĐANG YÊU CẦU HỦY DỰ ÁN.\n\nBạn có chắc chắn muốn hủy dự án?')){location.href = '<?php echo $view['router']->generate('workshop_cancel',array('job_id' => $job->getId())) ?>';} return true;">
                                         <?php echo $view['translator']->trans('workshop.menu_left.button.cancel.request', array(), 'vlance') ?>
                                    </a>
                                    <?php echo $view['translator']->trans('form.job.workroom-remind.block2.guide_p1_more', array(), 'vlance'); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php /* Nếu freelance được giao việc gửi tin nhắn kết thúc cviec thỳ hiển thị cho client nút này*/?>
                <?php if($status == Job::JOB_FINISH_REQUEST): ?>
                    <?php /* TODO: Chỗ này cần làm giống như giao diện mới bên trên (JOB_WORKING) */ ?>
                    <div class='span12'>
                        <p>Freelancer vừa gửi bạn yêu cầu kết thúc dự án.</p>
                    </div>
                    <div class="clear action-buttons action-accept">
                        <a class="btn btn-primary" href="#"
                           onclick="if(confirm('Bạn chắc chắn đã nhận được đầy đủ toàn bộ sản phẩm từ freelancer? Vui lòng xác nhận dự án đã hoàn thành.')){location.href = '<?php echo $view['router']->generate('job_update_status',array('bid' => $bid_awarde[0]->getId(), 'status' => Job::JOB_FINISHED )) ?>';} return true;">
                            <?php echo $view['translator']->trans('workshop.menu_left.button.finish.accept', array(), 'vlance') ?>
                        </a>
                    </div>
                    <div class="clear action-buttons action-refuse">
                        <a class="btn btn-danger" href="#"
                           onclick="if(confirm('Bạn chắc chắn muốn yêu cầu freelancer làm lại?')){location.href = '<?php echo $view['router']->generate('job_update_status',array('bid' => $bid_awarde[0]->getId(), 'status' => Job::JOB_WORKING )) ?>';} return true;">
                            <?php echo $view['translator']->trans('workshop.menu_left.button.finish.refuse', array(), 'vlance') ?>
                        </a>
                    </div>
                <?php endif; ?>

                <?php if(($status == Job::JOB_FINISH_REQUEST || $status == Job::JOB_FINISHED || $status == Job::JOB_CANCEL_REQUEST || $status == Job::JOB_CANCELED) && $num_feed == 0) : ?>
                    <div class="workroom-remind grey">
                        <div class='row-fluid'>
                            <h5 class='span12 inner'>Bạn hài lòng với kết quả nhận được?</h5>
                        </div>
                        <div class='row-fluid'>
                            <div class='span12 inner'>
                                <a class="btn btn-primary btn-large" href="#formmessage_feedback_client" data-toggle="modal">
                                    <?php echo $view['translator']->trans('workshop.menu_left.feedback.to_freelancer', array(), 'vlance') ?>
                                </a>
                            </div>
                        </div>
                        <div class='row-fluid'>
                            <div class='span12 inner'>
                                <p class='text-small'>Gửi <b>đánh giá chất lượng freelancer</b> giúp bạn luôn được làm việc cùng các freelancer hàng đầu tại vLance.vn</p>
                            </div>
                        </div>
                    </div>
                    <?php //Form feedback cua client ?>
                    <div id="formmessage_feedback_client" class="formmessage_feedback_client modal fade">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 id="myModalLabel"><?php echo $view['translator']->trans('workshop.feedback.title_popup', array(), 'vlance') ?></h3>
                        </div>
                        <div class="modal-body">
                            <div class="description_popup_service"><?php echo $view['translator']->trans('workshop.feedback.description_popup_service', array(), 'vlance') ?></div>
                            <div class ="form-message-feedback">
                                <?php echo $view['actions']->render($view['router']->generate('feedback_new',array('job_id' => $job->getId(),'bid' => $bid_awarde[0]->getId())));?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    
<?php // View of Freelancer ?>
<?php else : ?>
    <?php /*hiển thi của freelancer khi vào xem messenger*/ ?>
    <div class="description-job">
        <h2><?php echo $view['translator']->trans('workshop.menu_left.title_client', array(), 'vlance') ?></h2>
        <dl class="dl-horizontal">
            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.id', array(), 'vlance') ?></dt>
            <dd><?php echo $job->getId() ?></dd>                        

            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.city', array(), 'vlance') ?></dt>
            <dd><?php echo $job->getCity() ? $job->getCity()->getName() : $view['translator']->trans('common.country', array(), 'vlance') ?></dd>

            <dt><?php echo $view['translator']->trans('view_job_page.job_detail.budget', array(), 'vlance') ?></dt>
            <dd><?php echo $job->getJobBudget($view['vlance']->isAuthen()); ?></dd>
        </dl>

    </div>
        
    <?php /*Nếu hoàn thành thỳ gửi tin nhắn cho người tạo job*/?>
    <?php if($acc == $job->getWorker() || HasPermission::hasAdminPermission($acc) == TRUE) : ?>
        <?php if($status == Job::JOB_WORKING) : ?>
            <div class="workroom-remind grey">
                <div class='row-fluid'>
                    <h5 class='span12 inner'><?php echo $view['translator']->trans('form.job.workroom-remind.block2.heading_guider', array(), 'vlance'); ?></h5>
                </div>
                <div class='row-fluid'>
                    <div class='span12 inner'>
                        <a class="btn btn-primary btn-large" href="#"
                                onclick="if(confirm('Bạn đã làm xong chưa? Bạn có chắc chắn muốn gửi XÁC NHẬN KẾT THÚC DỰ ÁN tới khách hàng không?')){location.href = '<?php echo $view['router']->generate('job_update_status',array('bid' => $bid_awarde[0]->getId(), 'status' => Job::JOB_FINISH_REQUEST )) ?>'; }return true;">
                            <?php echo $view['translator']->trans('workshop.menu_left.button.finish.request', array(), 'vlance') ?>
                        </a>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 inner'>
                        <p class='text-small'>Dự án chỉ thực sự kết thúc sau khi nhận được xác nhận của cả Bạn và Khách hàng.</p>
                    </div>
                </div>
            </div>
        <?php elseif(($status == Job::JOB_FINISH_REQUEST || $status == Job::JOB_FINISHED || $status == Job::JOB_CANCEL_REQUEST || $status == Job::JOB_CANCELED) && $num_feed == 0): ?>
            <div class="workroom-remind grey">
                <div class='row-fluid'>
                    <h5 class='span12 inner'>Bạn đánh giá thế nào về khách hàng?</h5>
                </div>
                <div class='row-fluid'>
                    <div class='span12 inner'>
                        <a class="btn btn-primary btn-large" href="#formmessage_feedback_freelance" data-toggle="modal">
                            <?php echo $view['translator']->trans('workshop.menu_left.feedback.to_client', array(), 'vlance') ?>
                        </a>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 inner'>
                        <p class='text-small'>Gửi <b>nhận xét về khách hàng</b> giúp bạn tăng thêm nhiều cơ hội việc làm hơn.</p>
                    </div>
                </div>
            </div>
            <div id="formmessage_feedback_freelance" class="formmessage_feedback_freelance modal fade">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel"><?php echo $view['translator']->trans('workshop.feedback.title_popup', array(), 'vlance') ?></h3>
                </div>
                <div class="modal-body">
                    <div class="description_popup_service"><?php echo $view['translator']->trans('workshop.feedback.description_popup_service_freelance', array(), 'vlance') ?></div>
                    <div class ="form-message-feedback">
                        <?php echo $view['actions']->render($view['router']->generate('feedback_new',array('job_id' => $job->getId(),'bid' => $bid_awarde[0]->getId())));?>
                    </div>
                </div>
           </div>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
    
<script type="text/javascript" language="javascript">
    jQuery(document).ready(function($){ 
        $('#feedback_client #vlance_workshopbundle_feedbacktype .control-group input').rating(); 
        $('#feedback_client #vlance_workshopbundle_feedbacktype .control-group input').rating('select','5'); 
    });
</script>
