<?php use Vlance\JobBundle\Entity\Job; ?>

<?php /* @var $job \Vlance\JobBundle\Entity\Job */ ?>
<?php if($job->getStatus() == Job::JOB_WORKING) :?>
    <?php $est_duration = (int)ceil($job->getAwardedBid()->getDuration() * 0.5); // 50% of the estimated duration ?>
    <?php $est_deadline = $job->getUpdatedAt()->add(new DateInterval('P' . $est_duration . 'D')); ?>
    <?php if($est_deadline <= new \DateTime()): ?>
        <div class="workroom-remind">
            <div class='row-fluid'>
                <div class='span12 inner'>
                    <a class="hide-popup" href="#" id="setting__workroom__popup__reminder_finish_project__hide" 
                       data-url="<?php echo $view['router']->generate('set_account_setting',array()) ?>"
                       title="Ẩn thông báo"><i class="fa fa-times fa-lg" aria-hidden="true"></i></a>
                    <h5><?php echo $view['translator']->trans('form.job.workroom-remind.block1.heading_guider', array(), 'vlance'); ?></h5>
                </div>
            </div>
            <div class='row-fluid'>
                <div class='span12 inner'>
                    <p><?php echo $view['translator']->trans('form.job.workroom-remind.block1.guide_p1', array(), 'vlance'); ?></p>
                    <p><?php echo $view['translator']->trans('form.job.workroom-remind.block1.guide_p2', array(), 'vlance'); ?></p>
                    <button id="client-finish-job" class="btn btn-large btn-primary"><?php echo $view['translator']->trans('workshop.menu_left.button.finish.accept', array(), 'vlance') ?></button>
                </div>
            </div>
            <?php /* <div class='row-fluid'>
                <div class='span12 inner'>
                    <p class='text-small'>Hoặc bạn có thể
                        <b>
                            <a class='text-red' href="#" onclick="if(confirm('BẠN ĐANG YÊU CẦU HỦY DỰ ÁN.\n\nBạn có chắc chắn muốn hủy dự án?')){location.href = '<?php echo $view['router']->generate('workshop_cancel',array('job_id' => $job->getId())) ?>'; return true;}">
                             <?php echo $view['translator']->trans('workshop.menu_left.button.cancel.request', array(), 'vlance') ?>
                            </a>
                        </b>
                        nếu kết quả không được như mong đợi</p>
                </div>
            </div> */ ?>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                <?php // Calculate if deadline (in hours) is over. NEGATIVE = over, POSITIVE = ontime ?>
                <?php $deadline = $job->getUpdatedAt()->add(new DateInterval('P' . $job->getAwardedBid()->getDuration() . 'D')); ?>
                <?php $diff_time = date_diff($deadline, new \DateTime()); ?>
                <?php $diff_time_hour = ($diff_time->format('%R') == "-" ? (1) : (-1)) * ($diff_time->format('%a')*24 + $diff_time->format('%h')); ?>

                vtrack('Show remind client finish job', {'job_id': <?php echo $job->getId();?>, 'deadline': '<?php echo $diff_time_hour?>'});

                var href_finish = '<?php echo $view['router']->generate('job_update_status',array('bid' => $bid->getId(), 'status' => Job::JOB_FINISHED )) ?>';
                $("#client-finish-job").click(function(){
                    $(this).attr('disabled', 'disabled');
                    vtrack('Click finished job', {'location':'reminder', 'case':'client-direct'});
                    if(confirm('Bạn chắc chắn đã nhận được đầy đủ toàn bộ sản phẩm từ freelancer? Vui lòng xác nhận dự án đã hoàn thành.')){
                        location.href = href_finish;
                    }
                });
            });
        </script>
    <?php endif;?>
<?php endif;?>