<?php 
    use Vlance\BaseBundle\Utils\HasPermission; 
    use Vlance\BaseBundle\Utils\JobCalculator;
?>
<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
    
<div class="profile-job span12 
        <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>
        <?php if($entity->getAwardedAt()): ?> profile-winning <?php endif; ?> ">
    <div class="span5 profile-job-right">
        <div class="profile-job-right-top">
            <div class="freelancer-row-img span4">
                <div class="images">
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                        <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                        <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                        <?php else: ?>
                        <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                        <?php endif; ?>
                    </a>
                </div>
               <?php if($acc->getNumReviews() != 0): ?>
                    <div class="rating-box" data-toggle="popover" 
                        data-placement="right"  
                        data-content="<?php echo number_format($acc->getScore() / $acc->getNumReviews(),'1',',','.') ?>"  
                        data-trigger="hover">
                        <?php $rating= ($acc->getScore() / $acc->getNumReviews()) * 20; ?>
                        <div class="rating" style="width:<?php echo number_format($rating,'1','.','.').'%'; ?>"></div>
                    </div> 
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            $('.rating-box').popover();
                        })
                    </script>
                <?php else: ?>
                    <div class="rating-box">
                        <div class="rating" style="width:0%"></div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="span8">
                <div class="span10 title">
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                        <?php echo $acc->getFullName() ?>
                    </a>
                    <?php if ($acc->getIsCertificated()) : ?>
                    <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                        title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                    <?php endif;?>
                    <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                        <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                    <?php endif; ?>
                    <?php if (!is_null($acc->getPersonId())): ?>
                        <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php /* comment        
                    <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                    */ ?>                
                    <script type="text/javascript" language="javascript">
                    jQuery(document).ready(function(){
                        $(function () {
                            $('[data-toggle="tooltip"]').tooltip()
                            })        
                        });
                    </script>    
                </div>
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <div class="span2 drop-menu">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                   title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                       <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                </a>
                            </li>
                            <?php if(!($entity->getIsDeclined())): ?>
                            <li>
                                <a href="#popup-refusal" role="button" class="refusal-call-popup" data-toggle="modal"
                                   data-id="<?php echo $acc->getId(); ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_isdeclined',array('id' => $entity->getId())) ?>"
                                   >
                                <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                </a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </div>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
                
                <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                <dl class="dl-horizontal">
                    <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                    <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                    <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.createdat', array(), 'vlance') ?></dt>
                    <dd><?php echo $acc->getCreatedAt()->format('d-m-Y') ?></dd>
                    <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.number_job', array(), 'vlance')?></dt>
                    <dd>
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getNumBid() ?>">
                        <?php echo $acc->getNumBid() ?> việc
                        </a>
                    </dd>
                    <dt><?php echo $view['translator']->trans('view_bid__page.bid_detail.income', array(), 'vlance') ?></dt>
                    <dd>
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" 
                           title="<?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>">
                            <?php echo number_format( $acc->getInCome(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>
                        </a>
                    </dd>
                </dl>
            </div>
        </div>
        
        <div class="profile-job-right-bottom upper span12">
            <div class="row-fluid">
                <div class="price">
                    <?php if($current_user->getIsMissingClientInfo()):?>
                        <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                            <?php echo preg_replace('/[0-9]/','x',(string)(number_format( $entity->getAmount(),'0',',','.'))) ?>
                        </span>
                    <?php else:?>
                        <span><?php echo number_format( $entity->getAmount(),'0',',','.') ?></span>
                    <?php endif;?>
                    <span class="less-important"><?php echo $view['translator']->trans('common.currency', array(), 'vlance') ?></span>    
                </div>
                <div class="price">
                    <span class="less-note"> - </span>
                    <?php if($current_user->getIsMissingClientInfo()):?>
                        <span title="Thông tin bị ẩn vì tài khoản của bạn thiếu thông tin cơ bản. Vui lòng cập nhật tài khoản để xem được ngay.">
                            <?php echo preg_replace('/[0-9]/','x',(string)($entity->getDuration())) ?>
                        </span>
                    <?php else:?>
                        <span><?php echo $entity->getDuration()?></span>
                    <?php endif;?>
                    <span class="less-important"><?php echo $view['translator']->trans('common.days', array(), 'vlance') ?></span>
                </div>
            </div>
        </div>
        <div class="profile-job-right-bottom lower span12">
            <div class="row-fluid">
                <div class="row-fluid links">
                    <?php if($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                        <?php if($status_job == Vlance\JobBundle\Entity\Job::JOB_OPEN 
                                    && ($job->getAccount() == $current_user || HasPermission::hasAdminPermission($current_user) == TRUE) 
                                    && !($entity->getIsDeclined())) : ?>                                    
                            <?php $des = \Vlance\BaseBundle\Utils\JobCalculator::replacement($entity->getFullDescription()); ?>
                            <?php $files_list = array(); ?>
                            <?php $files = $entity->getFiles(); ?>
                            <?php foreach ($files as $file):?>
                                <?php array_push($files_list, $file->getFilename());?>
                            <?php endforeach; ?>
                    
                            <?php if($current_user->getIsMissingClientInfo()):?>
                                <a href="#popup-missing-info" role="button" data-toggle="modal" class="btn-flat btn-light-green btn-flat-xlarge formassignment-call-popup"
                                   onclick="vtrack('Click award job', {'case': 'Missing info', 'location':'workroom'})"
                                   title="Giao việc">Giao việc</a>
                            <?php else: ?>
                                <a href="#formassignment" role="button" data-toggle="modal" class="btn-flat btn-light-green btn-flat-xlarge formassignment-call-popup"
                                   onclick="vtrack('Click award job', {'location':'workroom'})"
                                   data-id="<?php echo $entity->getId(); ?>"
                                   data-uname="<?php echo htmlentities($acc->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>"
                                   data-city="<?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?>"
                                   data-description="<?php echo htmlentities(\Vlance\BaseBundle\Utils\JobCalculator::cut(nl2br($des),260), ENT_SUBSTITUTE, "UTF-8")?>"
                                   data-file="<?php echo htmlspecialchars(json_encode($files_list)); ?>"
                                   data-price="<?php echo number_format( $entity->getAmount(),'0',',','.').' '.$view['translator']->trans('common.currency', array(), 'vlance') ?>"
                                   data-action="<?php echo $view['router']->generate('bid_update_awardeat',array('id' => $entity->getId())) ?>" 
                                   title="Giao việc">Giao việc</a>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($entity->getAwardedAt() ): ?>
                        <div class="winning"></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>    
    </div>        
    <div class="span7 profile-job-left">
        <div class="profile-job-left-bottom">
            <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
            <?php endif; ?>
            <div class="body-view row-fluid">
                
                <?php if($entity->getFullDescription()): ?>                 
                <div class="textbody">
                    <?php // Hide bid content when client account missing info ?>
                    <?php if($current_user->getId() == $job->getAccount()->getId() && $current_user->getIsMissingClientInfo()):?>
                        <p><i class="icon-exclamation-sign"></i> Bạn chưa thể xem được nội dung chào giá của freelancer.</p>
                        <br/>
                        <p><a href="#popup-missing-info-viewbid" role="button" class="btn-flat btn-light-green btn-flat-large" data-toggle="modal" 
                            onclick="vtrack('Click view bid', {'location':'workroom', 'case':'Missing info'})"
                            data-content="<?php echo "data-content-missing"?>"
                            title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                Xem chi tiết
                        </a></p>
                    <?php else: ?>
                        <div class="inner textbody hidden">
                            <?php if(!$entity->getIntroDescription()): ?>
                                <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                            <?php else: ?>
                                <b>Giới thiệu về kinh nghiệm và kỹ năng</b>:<br/>
                                <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getIntroDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                <br/><br/>
                                <b>Kế hoạch thực hiện công việc</b>:<br/>
                                <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                            <?php endif; ?>
                        </div>         
                        <a href="#" class="read-more-toggle"><?php echo $view['translator']->trans('view_bid__page.bid_detail.more', array(), 'vlance') ?></a>        
                        <script type="text/javascript" language="javascript">
                            jQuery(document).ready(function($){
                                $(".read-more-toggle").click(function(){
                                    $(".textbody .inner.textbody").removeClass("hidden");
                                    $(this).hide();
                                    return false;
                                });
                            });
                        </script>
                    <?php endif;?>
                </div>
                
                <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                </a>
                <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row-fluid">
        <?php $files = $entity->getFiles(); ?>
            <?php if(count($files)>0): ?>
                <div class="span12 attach">
                    <ul>
                        <?php foreach($files as $file) : ?>

                            <li class="row-fluid">
                                <div class="i32 i32-attach span2"></div>
                                <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                   class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                </a>
                            </li>

                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        
        <?php if($job->getAccount() == $current_user): ?>
        <div class="footer-bid-client">
            <?php if(!is_object($entity->getBidRate())): ?>
                <p class="title">Bạn đánh giá chất lượng chào giá thế nào?</p>
                <div class="fb-rating-label">
                    <div class="fb-ratings fb-stars">
                        <?php /* <a class="fb-star-rating star1" onmouseover="funcLabel(1);" onmouseout="funcOut();" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 1);"></a>
                        <a class="fb-star-rating star2" onmouseover="funcLabel(2);" onmouseout="funcOut();" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 2);"></a>
                        <a class="fb-star-rating star3" onmouseover="funcLabel(3);" onmouseout="funcOut();" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 3);"></a>
                        <a class="fb-star-rating star4" onmouseover="funcLabel(4);" onmouseout="funcOut();" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 4);"></a>
                        <a class="fb-star-rating star5" onmouseover="funcLabel(5);" onmouseout="funcOut();" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 5);"></a> */ ?>
                        <a class="fb-star-rating star1" onmouseover="funcLabel(1);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 1);"></a>
                        <a class="fb-star-rating star2" onmouseover="funcLabel(2);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 2);"></a>
                        <a class="fb-star-rating star3" onmouseover="funcLabel(3);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 3);"></a>
                        <a class="fb-star-rating star4" onmouseover="funcLabel(4);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 4);"></a>
                        <a class="fb-star-rating star5" onmouseover="funcLabel(5);" onclick="funcBidToRate(this, <?php echo $entity->getId() ?>, 5);"></a>
                    </div>
                    <div class="fb-text">
                        <p class="fb-text-star star1">Rất kém, spam</p>
                        <p class="fb-text-star star2">Kém, không liên quan đến việc cần làm</p>
                        <p class="fb-text-star star3">Tạm được, cần bổ sung thêm</p>
                        <p class="fb-text-star star4">Khá ổn, chưa thực sự xuất sắc</p>
                        <p class="fb-text-star star5">Chào giá chuyên nghiệp, tôi rất thích!</p>
                    </div>
                    <?php $num = 5 - count($job->getBidRates()); ?>
                    <?php if($num > 0): ?>
                        <div class="fb-note">
                            <p>Đánh giá <span class="count-rated"><?php echo $num;?> lần</span> bạn sẽ được tặng 1 credit/job 
                                <i class="fa fa-question-circle"
                                    data-toggle="popover" 
                                    data-placement="top" 
                                    data-content="Với điều kiện job đã được nạp tiền vào hệ thống" 
                                    data-trigger="click"
                                    data-html="true">
                                </i>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>
                <script type="text/javascript">$(document).ready(function() {$('i[data-toggle=popover]').popover();});</script>
                <!--<div class="fb-finish">Đã gửi đánh giá</div>-->
            <?php else: ?>
                <?php $score = $entity->getBidRate()->getRating(); ?>
                <div class="fb-rating-label">
                    <p class="title">Cám ơn bạn đã đánh giá</p>
                    <div class="fb-ratings fb-stars">
                        <?php if((int)$score > 0): ?>
                            <?php for($i = 0; $i < $score; $i++): ?>
                                <a class="fb-star-rating active fb-star-on"></a>
                            <?php endfor; ?>
                        <?php endif; ?>
                        <?php if(5 - (int)$score > 0): ?>
                            <?php for($i = 0; $i < (5 - $score); $i++): ?>
                                <a class="fb-star-rating active"></a>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <script type="text/javascript">
            var urlRating = "<?php echo $view['router']->generate('rating_bid',array()); ?>";
        </script>
        <?php endif; ?>
    </div>    
</div>

<?php // ----------- Common content ----------- ?>
<?php if($current_user->getIsMissingClientInfo()):?>
    <?php // Nếu khách hàng thiếu thông tin cơ bản, sẽ hiện popup này ?>
    <?php $infos = $current_user->getMissingClientInfo();?>
    <?php if($infos[0] == "avatar" || $infos[0] == "city" || $infos[0] == "telephone"): ?>
        <?php $edit_form = "account_basic_edit"; ?>
    <?php else: ?>
        <?php $edit_form = "account_edit"; ?>
    <?php endif; ?>
    <?php foreach($infos as $key => $inf):?>
        <?php $infos[$key] = $view['translator']->trans('controller.job.showFreelanceJob.' . $infos[$key], array(), 'vlance'); ?>
    <?php endforeach;?>
    <?php /*popup báo khách hàng thiếu thông tin nên không gửi được tin nhắn cho freelancer */ ?>
    <div id="popup-missing-info" class="popup-refusal modal hide fade" 
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title_award', array(), 'vlance') ?></h3>
         </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_award', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
            <a href="<?php echo $view['router']->generate($edit_form, array('id' => $current_user->getId(), 'referer' => 'redirect')) ?>" 
               class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
            </a>
        </div>
    </div>

    <?php /*popup báo khách hàng thiếu thông tin nên không xem được nội dung chào giá */ ?>
    <div id="popup-missing-info-viewbid" class="popup-refusal modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_title_view_bid', array(), 'vlance') ?></h3>
         </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.missing_info.popup_view_bid', array('%%missing_info%%' => implode(" - ", $infos)), 'vlance') ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="close-link" data-dismiss="modal"><?php echo $view['translator']->trans('common.close', array(), 'vlance'); ?></a>
            <a href="<?php echo $view['router']->generate($edit_form, array('id' => $current_user->getId(), 'referer' => 'rederect')) ?>" 
               class="btn actions btn-primary btn-large" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.missing_info.update_account', array(), 'vlance') ?>
            </a>
        </div>
    </div>
    <?php // Hết popup ?>
<?php else:?>
    <?php /*popup từ chối chào giá */ ?>
    <div id="popup-refusal" class="popup-refusal modal hide fade" 
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?></h3>
         </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.popup.notice', array(), 'vlance').'?' ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance'); ?></a>
            <a href="#" 
               class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
            </a>
        </div>
    </div>

    <?php /*popup Báo cáo sai pham */ ?>
    <div id="popup-report_violations" 
         class="popup-report_violations modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?></h3>
         </div>
        <div class="modal-body">
            <p><?php echo $view['translator']->trans('view_bid__page.popup.notice_report', array(), 'vlance').'?' ?></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
            <a href="#" 
               class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
            </a>
        </div>
    </div> 

    <?php /* Popup xác nhận giao việc cho freelancer */ ?>
    <div id="formassignment" class="formassignment modal hide fade" 
         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?></h3>
        </div>
        <div class="modal-body">
            <div id="assignment-full-name">
                <strong class="full-name"><?php echo $view['translator']->trans('view_bid__page.popup.fullname', array(), 'vlance').': '?> </strong>
                <span></span>
            </div>
            <div class="clear"></div>

            <div class="assignment-description">
                <strong class="description"><?php echo $view['translator']->trans('view_bid__page.popup.description', array(), 'vlance').': '?> </strong>
                <div id="assignment-description"></div>
            </div>
            <div class="clear"></div>
            <?php // Lay file name ?>
            <div class="assigment-file">
                <div class="title-assigment-file"></div>
                <ul>
                    <?php // foreach ($files as $file) : ?>
                    <li class="row-fluid upload"><span class="upload"><?php // echo $file->getFilename();?></span></li>
                    <?php // endforeach;?>
                </ul>
            </div>
            <div class="clear"></div>
            <?php //  end lay file name ?>
            <div id="assignment-city">
                <strong><?php echo $view['translator']->trans('view_bid__page.popup.city', array(), 'vlance').':' ?> </strong>
                <span></span>
            </div>

            <div class="clear"></div>
            <div id="assignment-price">
                <strong><?php echo $view['translator']->trans('view_bid__page.popup.remuneration', array(), 'vlance').': '?></strong>
                <span></span>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance')?></a>
            <a href="#" 
               class="btn btn-primary actions" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?>">
                   <?php echo $view['translator']->trans('view_bid__page.bid_detail.assignment', array(), 'vlance') ?>
            </a>
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        jQuery(document).ready(function($) {
            jQuery("a.report-violations-call-popup").click(function(){
                jQuery("#popup-report_violations .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
            });

            jQuery("a.refusal-call-popup").click(function(){
                jQuery("#popup-refusal .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
            });  

            jQuery("a.formassignment-call-popup").click(function(){
                var arr = jQuery.parseJSON(jQuery(this).attr("data-file"));
                var files = "";

                for (i=0; i < arr.length; i++){
                    files += "<li class='row-fluid upload'><span class='upload'>"+ arr[i] +"<span></li>";
                }
                jQuery("#formassignment").attr("tabindex", jQuery(this).attr("data-id")); 
                jQuery("#formassignment .modal-footer a.actions").attr("href", jQuery(this).attr("data-action")); 
                jQuery("#formassignment .modal-body #assignment-full-name span").html(jQuery(this).attr("data-uname")); 
                jQuery("#formassignment .modal-body #assignment-description").html(jQuery(this).attr("data-description")); 
                jQuery("#formassignment .modal-body #assignment-city span").html(jQuery(this).attr("data-city")); 
                <?php // jQuery("#formassignment .modal-body #assignment-date-completion span").html(jQuery(this).attr("data-date-completion")); ?>
                jQuery("#formassignment .modal-body #assignment-price span").html(jQuery(this).attr("data-price")); 

                 if(files !== ""){
                     jQuery('#formassignment .modal-body .assigment-file .title-assigment-file').html("<strong><?php echo $view['translator']->trans('view_bid__page.popup.file', array(), 'vlance').':' ?></storng>");
                 };
                jQuery("#formassignment .modal-body .assigment-file ul").html(files); 
            });
        })
    </script>
    <?php /* Hết Popup xác nhận giao việc cho freelancer */ ?>
<?php endif;?>