<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php //Neu khong phai tin nhan he thong ?>
<?php if($entity->getSender() && $entity->getSender()->getId() != $acc_system ): ?>
    <tr class="
        <?php if($acc->getFullName() != $entity->getSender()->getFullName()){echo "odd";}  else {echo "even";}?>
        <?php echo $hide_more ? "hide-more" : "" ?>
        updated">
        <?php $sender_profile = "#"; ?>
        <?php if(HasPermission::hasAdminPermission($entity->getSender()) == FALSE): ?>
            <?php if ($entity->getBid()->getAccount() == $entity->getSender()): ?>
                <?php $sender_profile = $view['router']->generate('account_show_freelancer',array('hash' => $entity->getSender()->getHash())); ?>
            <?php else: ?>
                <?php $sender_profile = $view['router']->generate('account_show_client',array('hash' => $entity->getSender()->getHash())); ?>
            <?php endif; ?>
        <?php endif; ?>
        <td class="first">
            <?php if($new_messages[$entity->getId()] == 1) : ?>
                <i class="name-freelancer doing"></i>
            <?php endif; ?>
            <?php if(!$hide_avatar): ?>
            <a href="<?php echo $sender_profile; ?>">  
                <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($entity->getSender()->getUploadDir().DS.$entity->getSender()->getPath(), '62x62', 62, 62, 1); ?>
                <?php if($resize) : ?>
                <img src="<?php echo $resize; ?>" alt="<?php echo $entity->getSender()->getFullName(); ?>" title="<?php echo $entity->getSender()->getFullName(); ?>" />
                <?php else: ?>
                <img width="62" height="62" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $entity->getSender()->getFullName(); ?>" title="<?php echo $entity->getSender()->getFullName(); ?>" />
                <?php endif; ?>
            </a>
            <?php endif; ?>
        </td>
        <td class="center">
            <?php if(!$hide_avatar): ?>
            <div class="sender-name">
                <a href="<?php echo $sender_profile ?>">
                    <?php echo htmlentities($entity->getSender()->getFullName(), ENT_SUBSTITUTE, "UTF-8") ?>
                    <?php if(HasPermission::hasAdminPermission($entity->getSender()) == FALSE): ?>
                        <?php if(!is_null($entity->getSender()->getLastLogin())): ?>
                            <?php if($entity->getSender()->getLastLogin()->add(new DateInterval('PT60M')) > new DateTime()): ?>
                                <i class="online-status online" title="Online (đang thử nghiệm)"></i>
                            <?php else: ?>
                                <i class="online-status offline" title="Offline (đang thử nghiệm)"></i>
                            <?php endif; ?>
                        <?php else: ?>
                            <i class="online-status offline" title="Offline (đang thử nghiệm)"></i>
                        <?php endif; ?>
                    <?php endif; ?>
                </a>
            </div>
            
            <?php endif; ?>
            <div class="clear"></div>
            
            <?php // Convert nl to paragraph in the message's content ?>
            <?php foreach (explode("\n", htmlentities($entity->getContent(), ENT_SUBSTITUTE, "UTF-8")) as $line): ?>
                <?php if (trim($line)): ?>
                    <p><?php echo $line; ?></p>
                <?php endif; ?>
            <?php endforeach; ?>
            <?php // echo nl2br(htmlentities($entity->getContent(), ENT_SUBSTITUTE, "UTF-8")) ?>
            <?php
                $files = $entity->getFiles(); 
                if(count($files)>0) { ?>
                <div class="attach">
                    <ul>
                        <?php // echo $entity->getContent() ?>
                        <?php foreach ($files as $file) { ?>
                            <li class="row-fluid">
                                <div class="i32 i32-attach span1"></div>
                                    <a class="span10" href="<?php echo $view['router']->generate('download_message',array('message_id' => $entity->getId(), 'file_id'=> $file->getId()))?>">
                                        <?php echo $file->getFilename() ?>
                                    </a>
                            </li>
                        <?php
                            }
                         ?>
                    </ul>
                </div>
                <?php } ?>
        </td>
        <?php 
            $time = $entity->getCreatedAt()->format('d/m/Y H:i');
            $today = new DateTime('now');
            $yesterday = new DateTime('yesterday');
            $createat = $entity->getCreatedAt();
            if($today->format('d/m/Y') == $createat->format('d/m/Y')) {
                $time = $createat->format('H.i A');
            }elseif($createat->format('d/m/Y') == $yesterday->format('d/m/Y')) {
                $time = $view['translator']->trans('workshop.time.yesterday', array(), 'vlance') .', ' .$createat->format('d/m/Y H:i');
            } 
        ?>
        <td class="time last">
            <?php echo $time; ?>
            <?php if(HasPermission::hasAdminPermission($acc) == TRUE): ?>
                <br />
                <span>
                    <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminMessage_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link link_admin"
                        title="<?php echo $view['translator']->trans('admin.edit', array(), 'vlance') ?>">
                         <?php echo $view['translator']->trans('admin.edit', array(), 'vlance') ?>
                     </a>
                </span>
            <?php endif; ?>
        </td>
    </tr>
<?php elseif($entity->getSender() && $entity->getSender()->getId() == $acc_system && $entity->getReceiver()->getId() == $acc->getId() || HasPermission::hasAdminPermission($acc) == TRUE ) : ?>
    <tr class="system updated <?php echo $hide_more ? "hide-more" : "" ?>">
        <td class="first">
            <div class="notice-vlance-left span2">
                <div class="notice-vlance-logo"></div>
            </div>
        </td>
        <td class="center">
            <?php echo nl2br($entity->getContent()) ?>

        </td>
        <?php 
            $time = $entity->getCreatedAt()->format('d/m/Y H:i');
            $today = new DateTime('now');
            $yesterday = new DateTime('yesterday');
            $createat = $entity->getCreatedAt();
            if($today->format('d/m/Y') == $createat->format('d/m/Y')) {
                $time = $createat->format('H.i A');
            }elseif($createat->format('d/m/Y') == $yesterday->format('d/m/Y')) {
                $time = $view['translator']->trans('workshop.time.yesterday', array(), 'vlance') .', ' .$createat->format('d/m/Y H:i');
            } 
        ?>
        <td class="time last">
            <?php echo $time; ?>
            <?php if(HasPermission::hasAdminPermission($acc) == TRUE): ?>
                <br />
                <span>
                    <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminMessage_edit',array('pk' => $entity->getId())) ?>" target="_blank" class="edit-link link_admin"
                        title="<?php echo $view['translator']->trans('admin.edit', array(), 'vlance') ?>">
                         <?php echo $view['translator']->trans('admin.edit', array(), 'vlance') ?>
                     </a>
                </span>
            <?php endif; ?>
        </td>
    </tr>
<?php endif; ?>