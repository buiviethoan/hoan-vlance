<?php 
    use Vlance\BaseBundle\Utils\HasPermission; 
    use Vlance\BaseBundle\Utils\JobCalculator;
    use Vlance\JobBundle\Entity\Job;
?>
<?php $status_job = $job->getStatus(); ?>
<?php $acc = $entity->getAccount(); ?>
    
<div class="profile-job span12 
        <?php if($acc == $current_user || HasPermission::hasAdminPermission($acc) == TRUE): ?> profile-job-bid <?php endif; ?>">
    <div class="row-fluid">
        <div class="span7 profile-job-right onsite">
            <div class="profile-job-right-top">
                <div class="freelancer-row-img span4">
                    <div class="images">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php $resize = \Vlance\BaseBundle\Utils\ResizeImage::resize_image($acc->getUploadDir().DS.$acc->getPath(), '100x100', 100, 100, 1);?>
                            <?php if($resize) : ?>
                            <img src="<?php echo $resize; ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php else: ?>
                            <img width="100" height="100" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $acc->getFullName(); ?>" title="<?php echo $acc->getFullName(); ?>" />
                            <?php endif; ?>
                        </a>
                    </div>
                </div>
                <div class="span8">
                    <div class="span10 title">
                        <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $acc->getHash())) ?>" title="<?php echo $acc->getFullName() ?>">
                            <?php echo $acc->getFullName() ?>
                        </a>
                        <?php if ($acc->getIsCertificated()) : ?>
                        <img style="margin-top: -3px;height: 18px;  width: 18px;" src="/img/icon-certificated.png" data-toggle="tooltip" data-placement="top" alt="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"
                            title="<?php echo $view['translator']->trans('account.certificated', array(), 'vlance'); ?>"/>
                        <?php endif;?>
                        <?php if (!is_null($acc->getTelephoneVerifiedAt())): ?>
                            <img style="margin-top:-3px;width:18px; height:18px;" src="/img/icon-verify-contact.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số điện thoại" title="Đã xác thực số điện thoại">
                        <?php endif; ?>
                        <?php if (!is_null($acc->getPersonId())): ?>
                            <?php if (!is_null($acc->getPersonId()->getVerifyAt())): ?>
                                <img style="margin-top:-3px;width:18px; height:18px;" src="/img/verified_id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực số chứng minh nhân dân" title="Đã xác thực số chứng minh nhân dân">
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php /* comment        
                        <img style="margin-top:-3px;width: 18px; height:18px;" src="/img/icon-verify-id.png" data-toggle="tooltip" data-placement="top" alt="đã xác thực CMND" title="Đã xác thực CMND">
                        */ ?>                
                        <script type="text/javascript" language="javascript">
                        jQuery(document).ready(function(){
                            $(function () {
                                $('[data-toggle="tooltip"]').tooltip()
                                })        
                            });
                        </script>    
                    </div>
                    <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                    <div class="span2 drop-menu">
                        <div class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-chevron-down"></i></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a href="#popup-report_violations" role="button" class="report-violations-call-popup" data-toggle="modal"
                                       data-id="<?php echo $acc->getId(); ?>"
                                       data-action="<?php echo $view['router']->generate('bid_violate',array('id' => $entity->getId())) ?>"
                                       title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
                                           <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
                                    </a>
                                </li>
                                <?php if(!($entity->getIsDeclined())): ?>
                                <li>
                                    <a href="#popup-refusal" role="button" class="refusal-call-popup" data-toggle="modal"
                                       data-id="<?php echo $acc->getId(); ?>"
                                       data-action="<?php echo $view['router']->generate('bid_update_isdeclined',array('id' => $entity->getId())) ?>"
                                       >
                                    <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
                                    </a>
                                </li>
                                <?php endif;?>
                            </ul>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="clear"></div>

                    <div class="work-title"><?php echo $acc->getTitle(); ?></div>
                    <dl class="dl-horizontal">
                        <dt><?php echo $view['translator']->trans('view_job_page.job_detail.recruitment.fromcity', array(), 'vlance') ?></dt>
                        <dd><?php echo $acc->getCity() ? $acc->getCity()->getName():$view['translator']->trans('common.country', array(), 'vlance') ?></dd>
                        <dt>Số điện thoại</dt><dd><?php echo $acc->getTelephone(); ?></dd>
                        <dt>Skype</dt><dd><?php echo $acc->getSkype(); ?></dd>
                    </dl>
                </div>
            </div>    
        </div>        
        <div class="span5 profile-job-left onsite">
            <div class="profile-job-left-bottom">
                <?php if(!$entity->getAwardedAt() && $job->getAccount() == $current_user): ?>
                <?php endif; ?>
                <div class="body-view row-fluid">

                    <?php if($entity->getFullDescription()): ?>                 
                    <div class="textbody">
                        <?php // Hide bid content when client account missing info ?>
                        <?php if($current_user->getId() == $job->getAccount()->getId() && $current_user->getIsMissingClientInfo()):?>
                            <p><i class="icon-exclamation-sign"></i> Bạn chưa thể xem được nội dung chào giá của freelancer.</p>
                            <br/>
                            <p><a href="#popup-missing-info-viewbid" role="button" class="btn-flat btn-light-green btn-flat-large" data-toggle="modal" 
                                onclick="vtrack('Click view bid', {'location':'workroom', 'case':'Missing info'})"
                                data-content="<?php echo "data-content-missing"?>"
                                title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.send_message', array(), 'vlance') ?>">
                                    Xem chi tiết
                            </a></p>
                        <?php else: ?>
                            <div class="inner textbody hidden">
                                <?php if($entity->getDescription()): ?>
                                    <b>Tin nhắn của ứng viên</b>:<br/>
                                    <?php echo nl2br(htmlentities(Vlance\BaseBundle\Utils\JobCalculator::replacement(strip_tags($entity->getDescription())), ENT_SUBSTITUTE, "UTF-8")) ?>
                                <?php endif; ?>
                            </div>         
                            <a href="#" class="read-more-toggle"><?php echo $view['translator']->trans('view_bid__page.bid_detail.more', array(), 'vlance') ?></a>        
                            <script type="text/javascript" language="javascript">
                                jQuery(document).ready(function($){
                                    $(".read-more-toggle").click(function(){
                                        $(".textbody .inner.textbody").removeClass("hidden");
                                        $(this).hide();
                                        return false;
                                    });
                                });
                            </script>
                        <?php endif;?>
                    </div>

                    <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?> 
                    <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminBid_edit',array('pk' => $entity->getId()))?>" style="clear: both;float: left;" target="_blank" class="permission_edit">
                        <?php echo $view['translator']->trans('view_bid__page.bid_detail.admin_edit_bid', array(), 'vlance') ?>
                    </a>
                    <?php echo $view['actions']->render($view['router']->generate('bid_deactive_admin',array('id' => $entity->getId())));?>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row-fluid">
            <?php $files = $entity->getFiles(); ?>
                <?php if(count($files)>0): ?>
                    <div class="span12 attach">
                        <ul>
                            <?php foreach($files as $file) : ?>

                                <li class="row-fluid">
                                    <div class="i32 i32-attach span2"></div>
                                    <a href="<?php echo $view['router']->generate('download_file_encoded',array('encoded_url' => base64_encode($view['router']->generate('download_bid',array('bid_id' => $entity->getId(),'file_id' => $file->getId())))))?>" 
                                       class="span10 file_upload_name" title="<?php echo $file->getFilename() ?>"><?php echo $file->getFilename() ?>
                                    </a>
                                </li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>    
    </div>
</div>

<?php // ----------- Common content ----------- ?>
<?php /*popup từ chối chào giá */ ?>
<div id="popup-refusal" class="popup-refusal modal hide fade" 
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?></h3>
     </div>
    <div class="modal-body">
        <p><?php echo $view['translator']->trans('view_bid__page.popup.notice', array(), 'vlance').'?' ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance'); ?></a>
        <a href="#" 
           class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>">
               <?php echo $view['translator']->trans('view_bid__page.bid_detail.denied', array(), 'vlance') ?>
        </a>
    </div>
</div>

<?php /*popup Báo cáo sai pham */ ?>
<div id="popup-report_violations" 
     class="popup-report_violations modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <h3 id="myModalLabel"><?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?></h3>
     </div>
    <div class="modal-body">
        <p><?php echo $view['translator']->trans('view_bid__page.popup.notice_report', array(), 'vlance').'?' ?></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.back', array(), 'vlance') ?></a>
        <a href="#" 
           class="btn actions btn-primary" title="<?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>">
               <?php echo $view['translator']->trans('view_bid__page.bid_detail.report_violations', array(), 'vlance') ?>
        </a>
    </div>
</div> 