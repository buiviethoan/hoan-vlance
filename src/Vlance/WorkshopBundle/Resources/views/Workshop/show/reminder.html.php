<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
    
<?php // Remind freelancer not to start because the project has not been funded yet ?>
<?php if($acc != $job->getAccount() && $job->getPaymentStatus() == Job::WAITING_ESCROW && $job->getStartedAt()): ?>
    <div class="money_messager_workroom row-fluid">
        <span><?php echo $view['translator']->trans('workshop.content_right.not_payment_freelance', array(), 'vlance') ?></span>
    </div>
<?php endif; ?>

<?php // Remind client to click on FINISH PROJECT button ?>
<?php if($acc == $job->getAccount() || HasPermission::hasAdminPermission($acc) == TRUE) : ?>
    <?php if($job->getAccount()->getAccountSettingByProperty("setting__workroom__popup__reminder_finish_project__hide") != 1): ?>
        <?php echo $view->render('VlanceWorkshopBundle:Workshop/show/reminder/client:confirm_finish_project.html.php', array('bid' => $bid, 'job' => $job, 'acc' => $acc)); ?>
    <?php endif; ?>
<?php endif; ?>