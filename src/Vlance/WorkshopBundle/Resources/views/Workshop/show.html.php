<?php use Vlance\AccountBundle\Entity\Account; ?>
<?php use Vlance\JobBundle\Entity\Job; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $current_route = $app->getRequest()->attributes->get('_route'); ?>
<?php $view['slots']->set('title', 'Workroom'); ?>
<?php $view['slots']->start('content') ?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $acc_system               = Account::USER_VLANCE_SYS ?>
<?php $payment_paied            = Job::PAIED ?>
<?php $payment_escrow           = Job::ESCROWED ?>


<div class="content-section container">
    <div class="row-fluid">
        <div class="span12">
            <!-- Page title -->
            <div class="row-fluid">
                <h1 class="title"><?php echo ucfirst ($job->getTitle()) ?></h1>
                <?php /* ROLE ADMIN: Hien thi link edit  */ ?>
                <?php if(HasPermission::hasAdminPermission($acc) == TRUE): ?>
                    <div class="edit-view span2">
                        <a href="<?php echo $view['router']->generate('Vlance_JobBundle_AdminJob_edit',array('pk' => $job->getId())) ?>" target="_blank" class="edit-link"
                            title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                             <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                         </a>
                    </div>
                <?php /* END ROLE ADMIN: Hien thi link edit  */ ?>
                <?php else : ?>
                    <?php if($acc == $job->getAccount()) : ?>
                        <?php if($job->getType() !== Job::TYPE_ONSITE):?>
                            <?php if($job->getStatus() == Job::JOB_OPEN) : ?>
                                <?php /*Neu job o trang thai open thi hien thi link edit  */ ?>
                                <a href="<?php echo $view['router']->generate('job_edit',array('id' => $job->getId())) ?>" class="edit-view span2"
                                   title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                    <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="<?php echo $view['router']->generate('job_onsite_edit',array('id' => $job->getId())) ?>" class="edit-view span2"
                                title="<?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>">
                                 <?php echo $view['translator']->trans('view_job_client_page.job_detail.edit', array(), 'vlance') ?>
                             </a>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            
            <!-- Tabs -->
            <div class="row-fluid page-tabs">
                <ul>
                    <li>
                        <?php if($job->getType() !== Job::TYPE_ONSITE): ?>
                        <a href="<?php echo $view['router']->generate('job_show',array('id' => $job->getId())) ?>">
                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                        </a>
                        <?php else: ?>
                        <a href="<?php echo $view['router']->generate('job_onsite_view',array('hash' => $job->getHash())) ?>">
                            <?php echo $view['translator']->trans('job.workroom.description', array(), 'vlance') ?>
                        </a>
                        <?php endif; ?>
                    </li>
                    <li class="active">
                        <a href="#">
                            <?php echo $view['translator']->trans('job.workroom.workroom', array(), 'vlance') ?>
                        </a>
                    </li>
                </ul>
                <div class="border"></div>
            </div>
            
            <!-- Content -->
            <div class="row-fluid">
                <!-- Sidebar-left -->
                <div class="col2-left span3">
                    <div class="layered-navigation message-left">
                        <?php /* Header block */ ?>
                        <?php $job = $bid->getJob(); ?>
                        <?php echo $view['actions']->render($view['router']->generate('workshop_account_bid',array('job_id' => $job->getId(),'bid_idms' => $bid_idms['bid'])));?>
                    </div>
                </div>
                
                <!-- Messages section --> 
                <div class="col2-right span9">
                    <!-- Bid info -->
                    <?php /* ROLE ADMIN & USER JOB OWNER: Show bid info  */ ?>
                    <?php if($job->getAccount()->getId() == $acc->getId() || HasPermission::hasAdminPermission($acc) == TRUE ) : ?>
                        <?php if ($bid->getJob()->getStatus() >= Job::JOB_WORKING): ?>
                            <?php if($job->getType() == 0):?>
                                <div class="row-fluid"><button type="button" class="btn bid-info-toggle float-right" data-toggle="button"><?php echo $view['translator']->trans('workshop.content_right.bid', array(), 'vlance') ?></button></div>
                            <?php endif; ?>
                            <script type="text/javascript" language="javascript">
                                jQuery(document).ready(function($){
                                    $(".bid-info").hide();
                                    $(".bid-info-toggle").click(function(){
                                        $(".bid-info").toggle(300);
                                    });
                                });
                            </script>
                        <?php endif; ?>
                        <div class="row-fluid bid-info">
                            <?php if($job->getType() !== Job::TYPE_ONSITE): ?>
                                <?php echo $view->render('VlanceWorkshopBundle:Workshop/show:bid_info.html.php', array('entity' => $bid, 'job' => $job, 'current_user' => $acc)) ?>
                            <?php else: ?>
                                <?php echo $view->render('VlanceWorkshopBundle:Workshop/show:onsite_bid_info.html.php', array('entity' => $bid, 'job' => $job, 'current_user' => $acc)) ?>
                            <?php endif;?>
                        </div>
                    <?php endif;?>
                    <?php /* END ROLE ADMIN & USER JOB OWNER: Show bid info  */ ?>
                    
                    <!-- Message form -->
                    <div class ="form-message">
                        <?php echo $view['actions']->render($view['router']->generate('message_new',array('bid' => $bid->getId(), 'entity_bid' => $bid)));?>
                    </div>
                            
                    <!-- Reminder -->
                    <?php if($job->getType() !== Job::TYPE_ONSITE): ?>
                    <?php echo $view->render('VlanceWorkshopBundle:Workshop/show:reminder.html.php', array(
                                'bid' => $bid,
                                'job' => $job, 
                                'acc' => $acc)); ?>
                    <?php endif; ?>
                    
                    <!-- Messages history -->
                    <div class="table-message">
                        <?php if(count($entities) > 0):?>
                            <table class="table row-fluid">
                                <thead>
                                    <tr>
                                        <th class="span2"><?php echo $view['translator']->trans('workshop.content_right.sender', array(), 'vlance') ?></th>
                                        <th class="span6"><?php echo $view['translator']->trans('workshop.content_right.message', array(), 'vlance') ?></th>
                                        <th class="span2 time"><?php echo $view['translator']->trans('workshop.content_right.time', array(), 'vlance') ?></th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <?php $last_entity = NULL; ?>
                                    <?php $counter = 0; ?>
                                    <?php foreach ($entities as $entity) :?>
                                        <?php $counter++; ?>
                                        <?php $hide_avatar = (!is_null($last_entity) && ($last_entity->getSender() == $entity->getSender()) && (HasPermission::hasAdminPermission($entity->getSender()) == FALSE))? true : false ;?>
                                        <?php echo $view->render('VlanceWorkshopBundle:Workshop/show:message.html.php', 
                                                array('entity'      => $entity, 
                                                    'acc_system'    => $acc_system, 
                                                    'acc'           => $acc, 
                                                    'new_messages'  => $new_messages,
                                                    'hide_avatar'   => $hide_avatar,
                                                    'hide_more'     => ($counter > 15) ? true : false)); ?>
                                        <?php $last_entity = $entity; ?>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                            <?php if($counter > 15): ?>
                                <div class="row-fluid"><button type="button" class="btn message-table-toggle">XEM TẤT CẢ TIN NHẮN</button></div>
                                <script type="text/javascript" language="javascript">
                                    jQuery(document).ready(function($){
                                        $(".message-table-toggle").click(function(){
                                            $(".table-message tbody tr.hide-more").show(2000);
                                            $(".message-table-toggle").hide();
                                            vtrack('Show all message', {'location': 'workroom'});
                                        });
                                    });
                                </script>
                            <?php endif; ?>
                        <?php else: ?>
                                <div><?php echo $view['translator']->trans('workshop.content_right.not_message_again', array(), 'vlance') ?></div>
                        <?php endif; ?>
                    </div>
                    <!-- END Messages history -->
                </div>
                <!-- END Messages section --> 
            </div>
            <!-- END Content -->
        </div>
    </div> 
</div>
<?php $view['slots']->stop(); ?>
