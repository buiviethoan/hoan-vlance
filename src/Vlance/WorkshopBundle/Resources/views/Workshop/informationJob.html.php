<?php 
    $job = $entity->getJob();
    $acc = $app->getSecurity()->getToken()->getUser();
    $finish = $job->getCloseAt()->format('d-m-Y H:i');
    $now = new \DateTime('now');
    $remain = Vlance\BaseBundle\Utils\JobCalculator::ago($finish,$now->format('d-m-Y H:i'));
?>
<?php if($remain < 0) : ?>
    <div class="has_expired_quote"><?php echo $view['translator']->trans('list_job.right_content.has_expired_quote', array(), 'vlance') ?></div>
 <?php else: ?>
     <div>Chỉ còn: <?php echo $remain ?></div>
 <?php endif; ?>
<div>Địa điểm: <?php echo $job->getCity() ? $job->getCity()->getName() : '' ?></div>
<div>Bắt đầu: <?php echo $job->getStartedAt() ? $job->getStarteAt()->format('d/m/Y') : '' ?></div>
<div>Ngân sách: <?php echo $job->getBudget() ?></div>
<?php if($job->getStatus() == Vlance\JobBundle\Entity\Job::JOB_AWARDED && $acc === $job->getWorker()): ?>
    <a href="<?php echo $view['router']->generate('job_update_status',array('bid' => $entity->getId(), 'status' => Vlance\JobBundle\Entity\Job::JOB_FINISH_REQUEST ))  ?>">Kết thúc công việc</a>
<?php endif; ?>

