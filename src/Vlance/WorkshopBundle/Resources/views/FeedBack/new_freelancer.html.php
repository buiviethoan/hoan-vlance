<?php  $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<form id="feedback_freelance" action="<?php echo $view['router']->generate('feedback_create',array('job_id' => $job_id, 'bid' => $bid)) ?>" method="post" <?php echo $view['form']->enctype($form)?>>
    <?php // echo $view['form']->widget($form)?>
    <?php echo $view['form']->label($form['comment'],$view['translator']->trans('workshop.feedback.comment', array(), 'vlance')) ?>
    <?php echo $view['form']->errors($form['comment']) ?>
    <?php echo $view['form']->widget($form['comment']) ?>
    <?php echo $view['form']->row($form['_token'])?>
    <div class="message-feedback">
        <input type="submit" class="btn btn-primary" value="<?php echo $view['translator']->trans('workshop.menu_left.input_feedback', array(), 'vlance') ?>"/>
    </div>
</form>
