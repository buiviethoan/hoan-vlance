<?php $view['form']->setTheme($form, array('VlanceBaseBundle:Form')) ?>
<form id="feedback_client" action="<?php echo $view['router']->generate('feedback_create', array('job_id' => $job_id, 'bid' => $bid)) ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
    <?php echo $view['form']->widget($form) ?>
    <div class="message-feedback">
        <input type="submit" class="btn btn-primary" value="<?php echo $view['translator']->trans('workshop.menu_left.input_feedback', array(), 'vlance') ?>"/>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        var validate_popup_mesager = function() {
            $('#feedback_client').validate({
                rules: {
                    "vlance_workshopbundle_feedbacktype[comment]": {
                        required: true
                    }
                },
            });
        };

        $("#feedback_client .message-feedback .btn").click(function(event) {
            <?php //kiem tra neu validateinput = true thy moi bat loi hien thi popup  ?>
            validate_popup_mesager();
            if ($("#feedback_client").valid() == true) {
                $("#feedback_client").submit();
                $("#feedback_client .input-feedback_client").attr("disabled", "disabled");
            }

        });

    });


</script>  
