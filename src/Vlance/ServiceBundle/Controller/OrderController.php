<?php

namespace Vlance\ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Vlance\ServiceBundle\Entity\Order;
use Vlance\ServiceBundle\Entity\OrderItem;
use Vlance\ServiceBundle\Entity\OrderRow;
use Vlance\AccountBundle\Entity\Account;
use Vlance\PaymentBundle\Entity\OnePay;
use Vlance\JobBundle\Entity\Job;

/**
 * @Route("/order")
 */
class OrderController extends Controller
{
    /**
     * @Route("/{id}/thanhtoan", name="service_payment")
     * @Method("GET")
     * @Template("VlanceServiceBundle:ServicePack:thanh_toan.html.php",engine="php")
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceServiceBundle:Order')->findOneBy(array('id' => $id));
        
        $service_pack = "";
        $service_addon = array();
        $qty = "";
        foreach($entity->getOrderRows() as $r){
            
            $item = $r->getOrderItem();
            if($item->getType() == OrderItem::TYPE_PACK){
                $service_pack = $item->getServicePack();
                $qty = $item->getQuantity();
            } elseif($item->getType() == OrderItem::TYPE_ADDON) {
                $service_addon[] = $item->getServiceAddon();
            }
        }
        
        if(!is_object($service_pack)){
            return $this->redirect($this->generateUrl("service_pack_list"), 301);
        }
        
        if($qty < 1){
            return $this->redirect($this->generateUrl("service_pack_list"), 301);
        }
        
        return array(
            'entity'        => $entity,
            'service_pack'  => $service_pack,
            'service_addon' => $service_addon,
            'qty'           => $qty,
        );
    }
    
    protected function createCodeOrder()
    {
        $em = $this->getDoctrine()->getManager();
        
        $exist = true;
        $count = 0;
        $code = "";
        do {
            $number = (int)rand(2000000, 16777215);
            $code = strtoupper((string)dechex($number));
            $count++;
            
            $order_check = $em->getRepository('VlanceServiceBundle:Order')->findOneByCode($code);
            
            if(!$order_check){
                $exist = false;
                break;
            }
            
            if($count == 100){
                break;
            }
        } while ($exist == false);
        
        return $code;
    }
    
    /**
     * @Route("/submit_form", name="submit_form")
     * @Method("POST")
     * @Template(engine="php")
     */
    public function submitAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        /*** Validation ***/
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.notvalid', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        /* @var $acc \Vlance\AccountBundle\Entity\Account */
        $acc = $this->get('security.context')->getToken()->getUser();
        
        //Kiểm tra tồn tại input
        if(!isset($_POST['vlance_service_pack'])){
            $this->get('session')->getFlashBag()->add('error', 'service_pack');
            return $this->redirect($this->generateUrl("service_pack_list"), 301);//chuyen lai ve trang order
        } else {
            $sp_post = $_POST['vlance_service_pack'];
            $service_pack = $em->getRepository('VlanceServiceBundle:ServicePack')->findOneBy(array('hash' => $sp_post));
        }
        if(!isset($_POST['sp_quantity'])){
            $this->get('session')->getFlashBag()->add('error', 'quantity');
            return $this->redirect($this->generateUrl("service_pack_list"), 301);
        } else {
            $qty = (int)$_POST['sp_quantity'];
        }
        
        //Service addon
        $sa_post_list = array();
        if(isset($_POST['sp_addon_list'])){
            foreach($_POST['sp_addon_list'] as $a){
                $sa_post_list[] = $a;
            }
        }
        /*** End Validation ***/
        
        //create order
        $order = new Order();
        $order->setAccount($acc);
        $order->setDiscount(0);
        $order->setOrderStatus(Order::STATUS_OPEN);
        $em->persist($order);
        
        //create order row
        $orderRow = new OrderRow();
        
        //create order item service pack
        $item_sp = new OrderItem();
        $item_sp->setType(OrderItem::TYPE_PACK);
        $item_sp->setQuantity($qty);
        $item_sp->setServicePack($service_pack);
        $item_sp->setOrderRow($orderRow);
        $em->persist($item_sp);
        
        $orderRow->setOrderItem($item_sp);
        $em->persist($orderRow);
        
        $service_addon_list = array();
        if(count($sa_post_list) > 0){ //Service addon không bắt buộc
            foreach($sa_post_list as $s){
                $service_addon = $em->getRepository('VlanceServiceBundle:ServiceAddOn')->findOneBy(array('hash' => $s));
                $service_addon_list[] = $service_addon;
                
                $orderRowSA = new OrderRow();
                $item_sa = new OrderItem();
                $item_sa->setType(OrderItem::TYPE_ADDON);
                $item_sa->setServiceAddon($service_addon);
                $item_sa->setQuantity($qty);
                $item_sa->setOrderRow($orderRowSA);
                $em->persist($item_sa);
                $orderRowSA->setOrderItem($item_sa);
                $em->persist($orderRowSA);
                
                $orderRowSA->setOrder($order);
                $order->addOrderRow($orderRowSA);
                $em->persist($orderRowSA);
                $em->persist($order);
            }
        }
        
        //Tính tổng tiền order
        $totalAmount = 0;
        //service_pack
        $totalAmount = $totalAmount + ($service_pack->getPrice() * $qty);
        foreach($service_addon_list as $oi){
            $totalAmount = $totalAmount + ($oi->getPrice() * $qty);
        }
        $order->setAmount($totalAmount);
        $em->persist($order);
        
        //Tính ngày giao hàng
        $delivery = $service_pack->getDeliveried();
        foreach($service_addon_list as $oi){
            $delivery = $delivery - $oi->getDeliveried();
        }
        if($delivery < 1){
            return $this->redirect($this->generateUrl('show_service_pack', array('hash' => $service_pack->getHash())));
        }
        
        //Tính review
        $review = 0;
        $review = $review + $service_pack->getReviews();
        foreach($service_addon_list as $oi){
            $review = $review + $oi->getReviews();
        }
        
        /** Lưu ý: ngày giao hàng và review chưa có bảng lưu lại giá trị **/
        
        $orderRow->setOrder($order);
        $order->addOrderRow($orderRow);
        $em->persist($orderRow);
        $em->persist($order);
        
        //tạo mã cho order
        $code = $this->createCodeOrder();        
        $order->setCode($code);
        $em->persist($order);
        $em->flush();
        
        //chuyển về trang thanh toán
        return $this->redirect($this->generateUrl('service_payment', array('id' => $order->getId())));
    }
    
    /**
     * Finalize process after successful charged
     * 
     * @param type $requestId
     * @return boolean
     */
    protected function finalizeSuccessTransaction($orderId, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('vlance_payment.helper');
        
        $order = $em->getRepository('VlanceServiceBundle:Order')->findOneBy(array('id' => $orderId));
        $order->setOrderStatus(Order::STATUS_COMPLETE);
        $order->setPaymentStatus(Order::PAYMENT_PAIED);
        $em->persist($order);
        $em->flush();
        
        $payer = $order->getAccount();
        
        //Thông tin người trả
        $tpl_payer = 'VlanceServiceBundle:Email\Order:confirm_payer.html.twig'; //Chưa có nội dung email
        $context_payer = array(
            'payer'         => $payer,
            'amount'        => $order->getAmount(),
        );
        
        //Email cho admin
        $template_admin = 'VlanceJobBundle:Email\Order:confirm_admin.html.twig'; //Chưa có nội dung email
        $context_admin = array(
            'payer'         => $payer,
            'amount'        => $order->getAmount(),
        );
        $to_admin = $this->container->getParameter('to_email');
        
        $from_vlance = $this->container->getParameter('from_email');
        
        //Gửi email thông báo
        try {
            // Send tracking event to Mixpanel
//            $tracking_event_name = $helper->getParameter('payment_gateway.onepay.otp.' . $trans->getService() . '.tracking_event');
//            $this->get('vlance_base.helper')->trackAll(NULL, $tracking_event_name);
//            $this->get('vlance_base.helper')->mixpanelTrackRevenue($payer, $trans->getAmount(), $tracking_event_name, array("channel" => "SMS", "telephone" => $trans->getTelephone()));
            // End Mixpanel

            // Send confirmation mail to payer
            if(isset($tpl_payer) && isset($context_payer) && isset($payer)){
                $this->get('vlance.mailer')->sendTWIGMessageBySES($tpl_payer, $context_payer, $from_vlance, $payer->getEmail());
            }

            // Send notification to to concerned person, if there is
            if(isset($template_admin) && isset($context_admin) && isset($to_admin)){
                $this->get('vlance.mailer')->sendTWIGMessageBySES($template_admin, $context_admin, $from_vlance, $to_admin);
            }
        } catch (Exception $ex) {
            return false;
        }
        
        //chuyển về trang chọn freelancer
//        return $this->redirect($this->generateUrl('show_service_pack', array('hash' => $service_pack->getHash())));
    }
    
    /**
     * @Route("/onepay/bank/request", name="order_buy_bankcard_request")
     * @Method("POST")
     */
    public function buyOrderBankcardRequestAction(Request $request)
    {
        $logger = $this->get("monolog.logger.onepay");
        
        // Authentication check
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101)); // Send quick error notification
        }
        /* @var $auth_user \Vlance\AccountBundle\Entity\Account */
        $auth_user = $this->get('security.context')->getToken()->getUser();
        
        $helper = $this->get('vlance_payment.helper');
        
        $order_id = $request->request->get('order_id');
                
        if(!isset($order_id)){
            return new JsonResponse(array('error' => 102, '_post' => $_POST));
        }
        // END Amount Validation
        
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('VlanceServiceBundle:Order')->findOneBy(array('id' => $order_id));
        
        if(!isset($order)){
            return new JsonResponse(array('error' => 103));
        }
        
        // Generating random request-id
        $request_id = (string)($order_id) . "-" . (string)(microtime(true)*10000) . "-" . (string)(rand(0, 9));
               
        // Initialising transaction in database
        $trans = new OnePay();
        $trans->setPayer($auth_user);
        $trans->setType(OnePay::TRANS_TYPE_BANK);
        $trans->setStatus(OnePay::TRANS_STATUS_INITIAL);
        //$trans->setService(OnePay::SERV_OTP_BUY_CREDIT);
        $trans->setAmount($order->getAmount());
        $trans->setDescription(json_encode(array('order_id' => $order_id, 'amount' => $order->getAmount(), 'discount' => $order->getDiscount())));
        $trans->setRequestId($request_id);  // $requestId: invoice id on vlance xxx-xxxxxxxxxxxxxx-x
        $em->persist($trans);
        $em->flush();
        
        $return_url = $this->generateUrl('credit_buy_bankcard_request_return', array(), true);//sẽ thay đổi
        $logger->info($return_url);
        
        // Request Pay with Bank card
        $data = $this->encapDataRequest(array(
            'amount'        => $order->getAmount(),
            'command'       => 'request_transaction',
            'order_id'      => $trans->getId(), //@TODO Use real order_id here
            'order_info'    => "[" . $trans->getPayer()->getId() . "] vLance.vn - Thanh toán hóa đơn " . $order_id . " số tiền " . number_format($order->getAmount(), 0, ",", ".") . " VND)",
            'return_url'    => $return_url
        ));
        
        $confirm_url = "http://api.1pay.vn/bank-charging/service";
        
        $json_otpCharging = $this->execPostRequest($confirm_url, $data);        
        $decode_response=json_decode($json_otpCharging,true);
        if(!isset($decode_response['pay_url'])){
            return new JsonResponse(array('order_id' => $order_id, 'error' => 201, '_post' => $_POST, 'data' => $data, 'json_otpCharging' => $json_otpCharging, 'decode_response' => $decode_response));
        }
        
        $trans->addOnepayResponse($decode_response);
        $em->persist($trans);
        $em->flush();
        
        $response = array(
            'error'     => 0,
            'response'  => $decode_response
        );
        
        return new JsonResponse($response);
    }
    
    /**
     * When the end-user successfully finishes the payment, 1Pay return result to the return_url
     * with all the transaction's detail.
     *
     * @Route("/onepay/bank/request_return", name="order_buy_bankcard_request_return")
     * @Method("GET")
     */
    public function buyOrderBankcardRequestResponseAction(Request $request)
    {
        $helper = $this->get('vlance_payment.helper');
        $em = $this->getDoctrine()->getManager();
        $logger = $this->get("monolog.logger.onepay");
        
        $logger->info(json_encode($_GET));
        
        /* Assertion */
        // access_key
        if(($request->query->get('access_key')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1010), 'vlance'));
            $logger->info(1010);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        if($request->query->get('access_key') != $helper->getParameter('payment_gateway.onepay.access_key')){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1011), 'vlance'));
            $logger->info(1011);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $access_key = $request->query->get('access_key');
        $logger->info($access_key);
        
        // amount
        if(($request->query->get('amount')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1020), 'vlance'));
            $logger->info(1020);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $amount = (int)($request->query->get('amount'));
        $logger->info($amount);
        
        // card_name
        /*if(($request->query->get('card_name')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1030), 'vlance'));
            $logger->info(1030);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_name = $request->query->get('card_name');
        $logger->info($card_name);
        
        // card_type
        /*if(($request->query->get('card_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1040), 'vlance'));
            $logger->info(1040);
            return $this->redirect($this->generateUrl('credit_balance'));
        }*/
        $card_type = $request->query->get('card_type');
        $logger->info($card_type);
        
        // order_id
        if(($request->query->get('order_id')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1050), 'vlance'));
            $logger->info(1050);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_id = (int)($request->query->get('order_id'));
        $logger->info($order_id);
        
        /* @var $trans \Vlance\PaymentBundle\Entity\OnePay */
        $trans = $em->getRepository('VlancePaymentBundle:OnePay')->findOneBy(array('id' => $order_id));
        if(!$trans){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 2000), 'vlance'));
            $logger->info(2000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans->addOnepayResponse($_GET);
        $em->persist($trans);
        $em->flush();
        
        // order_info
        if(($request->query->get('order_info')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1060), 'vlance'));
            $logger->info(1060);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_info = $request->query->get('order_info');
        $logger->info($order_info);
        
        // order_type
        if(($request->query->get('order_type')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1070), 'vlance'));
            $logger->info(1070);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $order_type = $request->query->get('order_type');
        $logger->info($order_type);
        
        // request_time
        if(($request->query->get('request_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1080), 'vlance'));
            $logger->info(1080);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $request_time = new \DateTime($request->query->get('request_time'));
        $logger->info($request_time->getTimestamp());
        
        // response_code
        if(($request->query->get('response_code')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1090), 'vlance'));
            $logger->info(1090);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_code = $request->query->get('response_code');
        $logger->info($response_code);
        if($response_code != "00"){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1091), 'vlance'));
            $logger->info(1091);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        
        // response_message
        if(($request->query->get('response_message')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1100), 'vlance'));
            $logger->info(1100);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_message = $request->query->get('response_message');
        $logger->info($response_message);
        
        // response_time
        if(($request->query->get('response_time')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1110), 'vlance'));
            $logger->info(1110);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $response_time = new \DateTime($request->query->get('response_time'));
        $logger->info($response_time->getTimestamp());
        
        // trans_ref
        if(($request->query->get('trans_ref')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1120), 'vlance'));
            $logger->info(1120);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_ref = $request->query->get('trans_ref');
        $logger->info($trans_ref);
        
        // trans_status
        if(($request->query->get('trans_status')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1130), 'vlance'));
            $logger->info(1130);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $trans_status = $request->query->get('trans_status');
        $logger->info($trans_status);
        
        // signature
        if(($request->query->get('signature')) == null){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1140);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $signature = $request->query->get('signature');
        $built_sign = hash("sha256", "access_key=$access_key&amount=$amount&card_name=$card_name&card_type=$card_type&order_id=$order_id&order_info=$order_info&order_type=$order_type&request_time=$request_time&response_code=$response_code&response_message=$response_message&response_time=$response_time&trans_ref=$trans_ref&trans_status=$trans_status");
        if($signature != $built_sign){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 1140), 'vlance'));
            $logger->info(1150);
        }
        $logger->info($signature);
        $logger->info("Finish paramaters");
        /* END Assertion */
        
        print_r($signature);die;
        
        if($this->finalizeSuccessTransaction($trans->getRequestId()) != true){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.credit.buy.bankcard.error', array('%error_code%' => 3000), 'vlance'));
            $logger->info(3000);
            return $this->redirect($this->generateUrl('credit_balance'));
        }
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.credit.buy.bankcard.success', array(), 'vlance'));
        $logger->info("Success");
        return $this->redirect($this->generateUrl('credit_balance'));
    }
    
    /**
     * @Route("/pay/transfer", name="pay_transfer")
     * @Method("POST")
     */
    public function payTransferAction(Request $request)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        $order_id = $request->request->get('order_id');
        
        $em = $this->getDoctrine()->getManager();
        
        $order = $em->getRepository('VlanceServiceBundle:Order')->findOneBy(array('id' => $order_id));
        $order->setOrderStatus(Order::STATUS_WAITING);
        $order->setPaymentStatus(Order::PAYMENT_PAYING);
        $em->persist($order);
        $em->flush();
        
        $payer = $order->getAccount();
        
        foreach($order->getOrderRows() as $r){
            $item = $r->getOrderItem();
            if($item->getType() == OrderItem::TYPE_PACK){
                $service_pack = $item->getServicePack();
            }
        }
        
        if(!is_object($service_pack)){
            return new JsonResponse(array('error' => 102));
        }
        
        //Thông tin người trả
        $template = 'VlanceServiceBundle:Email\Order:transfer_email.html.twig';
        $context = array(
            'service_pack'  => $service_pack,
            'code'          => $order->getCode(),
            'payer'         => $payer,
        );
        $from_vlance = $this->container->getParameter('from_email');
        
        $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance, $payer->getEmail());
        
        return new JsonResponse(array('error' => 0));
    }
    
    /**
     * @Route("/{id}/freelancer", name="choose_freelancer")
     * @Method("GET")
     * @Template("VlanceServiceBundle:ServicePack:chon_freelancer.html.php",engine="php")
     */
    public function chooseFreelancerAction($id)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        $order = $em->getRepository('VlanceServiceBundle:Order')->findOneBy(array('id' => $id));
        
        $freelancer = array();
        $entities = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('isProvider' => true));
        foreach($entities as $e){
            if(count($e->getJobActivities()) < 1){
                $freelancer[] = $e;
            }
        }
        
        $service_pack = "";
        $service_addon = array();
        $qty = "";
        foreach($order->getOrderRows() as $r){
            $item = $r->getOrderItem();
            if($item->getType() == OrderItem::TYPE_PACK){
                $service_pack = $item->getServicePack();
                $qty = $item->getQuantity();
            } elseif($item->getType() == OrderItem::TYPE_ADDON) {
                $service_addon[] = $item->getServiceAddon();
            }
        }
        
        if(!is_object($service_pack)){
            return $this->redirect($this->generateUrl("service_pack_list"), 301);
        }
        
        if($qty < 1){
            return $this->redirect($this->generateUrl("service_pack_list"), 301);
        }
        
        return array(
            'entities'      => $freelancer,
            'order'         => $order,
            'service_pack'  => $service_pack,
            'service_addon' => $service_addon,
            'qty'           => $qty,
        );
    }
}
