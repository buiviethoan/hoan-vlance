<?php

namespace Vlance\ServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Vlance\ServiceBundle\Entity\ServicePack;

/**
 * @Route("/goi-dich-vu")
 */
class ServicePackController extends Controller
{
    /**
     * @Route("/", name="service_pack_list")
     * @Method("GET")
     * @Template("VlanceServiceBundle:ServicePack:list.html.php",engine="php")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('VlanceServiceBundle:ServicePack')->findBy(array('enabled' => true));

        return array(
            'entities' => $entities,
        );
    }
    
    /**
     * @Route("/{hash}", name="show_service_pack")
     * @Method("GET")
     * @Template("VlanceServiceBundle:ServicePack:show.html.php",engine="php")
     */
    public function showServicePackAction($hash)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceServiceBundle:ServicePack')->findOneBy(array('hash' => $hash));
        $addons = $em->getRepository('VlanceServiceBundle:ServiceAddOn')->findBy(array('servicePack' => $entity, 'enabled' => true));
        
        if(!$entity){
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        if($entity->getEnabled() == false){
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        return array(
            'entity'    => $entity,
            'addons'     => $addons,
        );
    }
}
