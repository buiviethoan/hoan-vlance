<?php

namespace Vlance\ServiceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Order
 *
 * @ORM\Table(name="order_row")
 * @ORM\Entity(repositoryClass="Vlance\ServiceBundle\Entity\OrderRowRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderRow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\ServiceBundle\Entity\Order", inversedBy="rows")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $order;
    
    /**
     * @ORM\OneToOne(targetEntity="Vlance\ServiceBundle\Entity\OrderItem", mappedBy="orderRow")
     */
    protected $orderItem;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderItems = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param \Vlance\ServiceBundle\Entity\Order $order
     * @return OrderRow
     */
    public function setOrder(\Vlance\ServiceBundle\Entity\Order $order = null)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return \Vlance\ServiceBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set orderItem
     *
     * @param \Vlance\ServiceBundle\Entity\OrderItem $orderItem
     * @return OrderRow
     */
    public function setOrderItem(\Vlance\ServiceBundle\Entity\OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;
    
        return $this;
    }

    /**
     * Get orderItem
     *
     * @return \Vlance\ServiceBundle\Entity\OrderItem 
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }
}