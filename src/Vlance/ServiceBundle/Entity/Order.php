<?php

namespace Vlance\ServiceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Vlance\ServiceBundle\Entity\OrderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Order
{
    const STATUS_OPEN = 0;
    const STATUS_WAITING = 1;
    const STATUS_COMPLETE = 2;
    
    const PAYMENT_BUYING = 0;
    const PAYMENT_PAYING = 1;
    const PAYMENT_PAIED = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="orders")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="amount", type="integer", options={"default":"0"})
     */
    protected $amount = 0;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="discount", type="integer", options={"default":"0"})
     */
    protected $discount = 0;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="order_status", type="smallint")
     */
    protected $orderStatus = self::STATUS_OPEN;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="payment_status", type="smallint")
     */
    protected $paymentStatus = self::PAYMENT_BUYING;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\ServiceBundle\Entity\OrderRow", mappedBy="order")
     */
    protected $orderRows;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    protected $code = null;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="paidAt", type="datetime", nullable=true)
     */
    private $paidAt;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderRows = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Order
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return Order
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add orderRows
     *
     * @param \Vlance\ServiceBundle\Entity\OrderRow $orderRows
     * @return Order
     */
    public function addOrderRow(\Vlance\ServiceBundle\Entity\OrderRow $orderRows)
    {
        $this->orderRows[] = $orderRows;
    
        return $this;
    }

    /**
     * Remove orderRows
     *
     * @param \Vlance\ServiceBundle\Entity\OrderRow $orderRows
     */
    public function removeOrderRow(\Vlance\ServiceBundle\Entity\OrderRow $orderRows)
    {
        $this->orderRows->removeElement($orderRows);
    }

    /**
     * Get orderRows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderRows()
    {
        return $this->orderRows;
    }

    /**
     * Set orderStatus
     *
     * @param integer $orderStatus
     * @return Order
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    
        return $this;
    }

    /**
     * Get orderStatus
     *
     * @return integer 
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set paymentStatus
     *
     * @param integer $paymentStatus
     * @return Order
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    
        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return integer 
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Order
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set paidAt
     *
     * @param \DateTime $paidAt
     * @return Order
     */
    public function setPaidAt($paidAt)
    {
        $this->paidAt = $paidAt;
    
        return $this;
    }

    /**
     * Get paidAt
     *
     * @return \DateTime 
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }
}