<?php

namespace Vlance\ServiceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ServiceAddOn
 *
 * @ORM\Table(name="service_addon")
 * @ORM\Entity(repositoryClass="Vlance\ServiceBundle\Entity\ServiceAddOnRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ServiceAddOn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique = true, nullable=true)
     */
    protected $title = null;
    
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default" : false})
     */
    protected $enabled = false;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", options={"default" : "0"})
     */
    protected $price = 0;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\ServiceBundle\Entity\ServicePack", inversedBy="addons")
     * @ORM\JoinColumn(name="service_pack_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $servicePack;
    
    /**
     * @var integer
     * @ORM\Column(name="delivery", type="integer")
     */
    protected $deliveried = 0;
    
    /**
     * @var integer
     * @ORM\Column(name="review", type="integer")
     */
    protected $reviews = 0;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ServiceAddOn
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ServiceAddOn
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return ServiceAddOn
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ServiceAddOn
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ServiceAddOn
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ServiceAddOn
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set servicePack
     *
     * @param \Vlance\ServiceBundle\Entity\ServicePack $servicePack
     * @return ServiceAddOn
     */
    public function setServicePack(\Vlance\ServiceBundle\Entity\ServicePack $servicePack = null)
    {
        $this->servicePack = $servicePack;
    
        return $this;
    }

    /**
     * Get servicePack
     *
     * @return \Vlance\ServiceBundle\Entity\ServicePack 
     */
    public function getServicePack()
    {
        return $this->servicePack;
    }

    /**
     * Set deliveried
     *
     * @param integer $deliveried
     * @return ServiceAddOn
     */
    public function setDeliveried($deliveried)
    {
        $this->deliveried = $deliveried;
    
        return $this;
    }

    /**
     * Get deliveried
     *
     * @return integer 
     */
    public function getDeliveried()
    {
        return $this->deliveried;
    }

    /**
     * Set reviews
     *
     * @param integer $reviews
     * @return ServiceAddOn
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    
        return $this;
    }

    /**
     * Get reviews
     *
     * @return integer 
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return ServiceAddOn
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }
}