<?php

namespace Vlance\ServiceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Order
 *
 * @ORM\Table(name="order_item")
 * @ORM\Entity(repositoryClass="Vlance\ServiceBundle\Entity\OrderItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderItem
{
    const TYPE_PACK = 0;
    const TYPE_ADDON = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
        
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\ServiceBundle\Entity\ServicePack", inversedBy="orderItems")
     * @ORM\JoinColumn(name="servicepack", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $servicePack;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\ServiceBundle\Entity\ServiceAddOn", inversedBy="orderItems")
     * @ORM\JoinColumn(name="service_addon", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $serviceAddon;

    /**
     * @ORM\ManyToOne(targetEntity="Vlance\ServiceBundle\Entity\OrderRow", inversedBy="orderItems")
     * @ORM\JoinColumn(name="order_row_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $orderRow;
    
    /**
     * @var integer
     * @ORM\Column(name="type", type="smallint")
     */
    protected $type = self::TYPE_PACK;
    
    /**
     * @var integer
     * 
     * @ORM\Column(name="quantity", type="integer", options={"default":"0"})
     */
    protected $quantity = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return OrderItem
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return OrderItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set servicePack
     *
     * @param \Vlance\ServiceBundle\Entity\ServicePack $servicePack
     * @return OrderItem
     */
    public function setServicePack(\Vlance\ServiceBundle\Entity\ServicePack $servicePack = null)
    {
        $this->servicePack = $servicePack;
    
        return $this;
    }

    /**
     * Get servicePack
     *
     * @return \Vlance\ServiceBundle\Entity\ServicePack 
     */
    public function getServicePack()
    {
        return $this->servicePack;
    }

    /**
     * Set serviceAddon
     *
     * @param \Vlance\ServiceBundle\Entity\ServiceAddOn $serviceAddon
     * @return OrderItem
     */
    public function setServiceAddon(\Vlance\ServiceBundle\Entity\ServiceAddOn $serviceAddon = null)
    {
        $this->serviceAddon = $serviceAddon;
    
        return $this;
    }

    /**
     * Get serviceAddon
     *
     * @return \Vlance\ServiceBundle\Entity\ServiceAddOn 
     */
    public function getServiceAddon()
    {
        return $this->serviceAddon;
    }

    /**
     * Set orderRow
     *
     * @param \Vlance\ServiceBundle\Entity\OrderRow $orderRow
     * @return OrderItem
     */
    public function setOrderRow(\Vlance\ServiceBundle\Entity\OrderRow $orderRow = null)
    {
        $this->orderRow = $orderRow;
    
        return $this;
    }

    /**
     * Get orderRow
     *
     * @return \Vlance\ServiceBundle\Entity\OrderRow 
     */
    public function getOrderRow()
    {
        return $this->orderRow;
    }
}