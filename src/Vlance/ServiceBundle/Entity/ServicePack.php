<?php

namespace Vlance\ServiceBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ServicePack
 *
 * @ORM\Table(name="servicepack")
 * @ORM\Entity(repositoryClass="Vlance\ServiceBundle\Entity\ServicePackRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ServicePack
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, unique = true, nullable=true)
     */
    protected $title = null;
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default" : false})
     */
    protected $enabled = false;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", options={"default" : "0"})
     */
    protected $price = 0;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="process", type="text", nullable=true)
     */
    protected $process = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="result", type="text", nullable=true)
     */
    protected $result = null;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\AccountBundle\Entity\Service", inversedBy="servicepacks")
     * @ORM\JoinTable(name="servicepack_service",
     * joinColumns={@ORM\JoinColumn(name="servicepack_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="service_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $services;
    
    /**
     * @var integer
     * @ORM\Column(name="delivery", type="integer")
     */
    protected $deliveried = 0;
    
    /**
     * @var integer
     * @ORM\Column(name="review", type="integer")
     */
    protected $reviews = 0;
    
    /***** SEO *****/
    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=true)
     */
    protected $metaTitle = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="text", nullable=true)
     */
    protected $metaKeyword = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_image", type="text", nullable=true)
     */
    protected $metaImage = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="seo_url", type="text", nullable=true)
     */
    protected $seoUrl = null;
    /***** END SEO *****/

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->services = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ServicePack
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ServicePack
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return ServicePack
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function __toString()
    {
        return $this->title;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ServicePack
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ServicePack
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ServicePack
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return ServicePack
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return ServicePack
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     * @return ServicePack
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->metaKeyword = $metaKeyword;
    
        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string 
     */
    public function getMetaKeyword()
    {
        return $this->metaKeyword;
    }

    /**
     * Set metaImage
     *
     * @param string $metaImage
     * @return ServicePack
     */
    public function setMetaImage($metaImage)
    {
        $this->metaImage = $metaImage;
    
        return $this;
    }

    /**
     * Get metaImage
     *
     * @return string 
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * Set seoUrl
     *
     * @param string $seoUrl
     * @return ServicePack
     */
    public function setSeoUrl($seoUrl)
    {
        $this->seoUrl = $seoUrl;
    
        return $this;
    }

    /**
     * Get seoUrl
     *
     * @return string 
     */
    public function getSeoUrl()
    {
        return $this->seoUrl;
    }

    /**
     * Add services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     * @return ServicePack
     */
    public function addService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services[] = $services;
    
        return $this;
    }

    /**
     * Remove services
     *
     * @param \Vlance\AccountBundle\Entity\Service $services
     */
    public function removeService(\Vlance\AccountBundle\Entity\Service $services)
    {
        $this->services->removeElement($services);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return ServicePack
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set process
     *
     * @param string $process
     * @return ServicePack
     */
    public function setProcess($process)
    {
        $this->process = $process;
    
        return $this;
    }

    /**
     * Get process
     *
     * @return string 
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return ServicePack
     */
    public function setResult($result)
    {
        $this->result = $result;
    
        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set delivered
     *
     * @param integer $delivered
     * @return ServicePack
     */
    public function setDeliveried($deliveried)
    {
        $this->deliveried = $deliveried;
    
        return $this;
    }

    /**
     * Get deliveried
     *
     * @return integer 
     */
    public function getDeliveried()
    {
        return $this->deliveried;
    }

    /**
     * Set reviews
     *
     * @param integer $reviews
     * @return ServicePack
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    
        return $this;
    }

    /**
     * Get reviews
     *
     * @return integer 
     */
    public function getReviews()
    {
        return $this->reviews;
    }
}