<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\ServiceBundle\Entity\ServicePack; ?>
<?php use Vlance\BaseBundle\Utils\ResizeImage;?>

<?php $view->extend('VlanceBaseBundle:thuengay:layout-1column.html.php')?>

<?php $view['slots']->start('content'); ?>
<div class="page-choose-fl">
    <h1 class="title">
        Thanh toán thành công <i class="fa fa-check-circle" aria-hidden="true"></i>
    </h1>
    <div class="block-payment-service">
        <div class="container">
            <h3 class="title">Mã số đơn hàng <b><?php echo $order->getCode(); ?></b></h3>
            <div class="section-payment-info">
                <div class="section-payment-info-top">
                    <div class="row-fluid">
                        <div class="span6">
                            <p>Tên dịch vụ</p>
                        </div>
                        <div class="span2"><p>Số lượng</p></div>
                        <div class="span2"><p>Đơn giá</p></div>
                        <div class="span2"><p>Thành tiền</p></div>
                    </div>
                </div>
                <div class="section-payment-content">
                    <div class="row-fluid">
                        <div class="span6">
                            <h3><?php echo $service_pack->getTitle(); ?></h3>
                            <?php foreach($service_addon as $sa): ?>
                            <p>
                               <i class="fa fa-check" aria-hidden="true"></i> <?php echo $sa->getTitle(); ?>
                            </p>
                            <?php endforeach; ?>
                        </div>
                        <div class="span2"><?php echo $qty; ?></div>
                        <div class="span2"><?php echo number_format($order->getAmount() / $qty,'0','','.'); ?> ₫</div>
                        <div class="span2"><?php echo number_format($order->getAmount(),'0','','.'); ?> ₫</div>
                    </div>
                </div>
            </div> 
            <div class="section-payment-success">
                <div class="row-fluid">
                    <div class="time-payment span6">
                        <p>Thanh toán vào lúc <b><?php echo $order->getPaidAt(); ?></b></p>
                    </div> 
                    <div class="payment-service span6">
                        <div class="total-money row-fluid">
                            <div class="span7">Tổng cộng:</div>   
                            <div class="span5"><?php echo number_format($order->getAmount(),'0','','.'); ?> đ</div>
                        </div>
                        <div class="vat-money row-fluid">
                            <div class="span7">Đã bao gồm trong đó VAT:</div>
                            <div class="span5"><?php echo number_format($order->getAmount() * 10 / 100,'0','','.'); ?> đ</div>
                        </div>
                        <div class="discount-money row-fluid">
                            <div class="span7">Giảm giá:</div>
                            <div class="span5"><?php echo number_format($order->getAmount() * $order->getDiscount(),'0','','.'); ?> đ</div>
                        </div>
                        <div class="pay-money row-fluid">
                            <div class="span7">Số tiền phải thanh toán:</div>
                            <div class="span5"><?php echo number_format($order->getAmount() - ($order->getAmount() * $order->getDiscount()),'0','','.'); ?> đ</div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>    
    </div>
    <div class="block-choose-partner">
        <div class="section-choose-partner-header">
            <h3>Bước tiếp theo</h3>
            <h1 class="title">Chọn đối tác làm việc</h1>
            <p>Bạn còn <b>23 giờ 59 phút 12 giây</b> để chọn đối tác</p>
            <p>Khi hết hạn hệ thống sẽ tự động chọn giúp bạn đối tác phù hợp nhất</p>
        </div>
        <div class="section-partner-content">
            <div class="container">
                <?php //Đang làm đến phần này ?>
                <div class="option-one checked">
                    <label class="radio">
                        <input type="radio" name="typeChoseFL" id="vlance_choose" value="vlance_choose" checked>
                        <p>Tôi muốn <b>Thuêngay.vn</b> chọn giúp tôi người phù hợp nhất</p>
                    </label>
                    <div class="note-option">
                        <p>Bạn sẽ được kết nối ngay với đối tác có nhiều kinh nghiệm nhất.</p>
                    </div>
                </div>
                <div class="option-two">
                    <label class="radio">
                        <input type="radio" name="typeChoseFL" id="client_choose" value="client_choose">
                        <p>Tôi muốn được tự chọn đối tác theo ý mình</p>
                    </label>
                    
                    <?php $profile_avatar = $view['assets']->getUrl('img/unknown.png')?>
                    
                    <div class="note-option">
                        <div class="row-fluid">
                            <div class="span6">
                                <p>Các đối tác đều là người có nhiều kinh nghiệm nhất.</p>
                            </div>
                            <div class="img-partner span6">
                                <?php foreach($entities as $entity): ?>
                                    <?php $real_avatar = ResizeImage::resize_image($entity->getFullPath(), '50x50', 50, 50, 1); ?>
                                    <?php if($real_avatar) : ?>
                                        <?php $profile_avatar = $real_avatar;?>
                                    <?php endif;?>
                                    <i><img src="<?php echo $profile_avatar; ?>"></i>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="choose-partner-show hidden">
                        <p>Các đối tác đều là người có nhiều kinh nghiệm nhất.</p>
                        <div class="row-fluid">
                            <?php foreach($entities as $entity): ?>
                            <div class="partner-info span3">
                                <div class="partner-comment">
                                    <div class="avata-fl">
                                        <?php $real_avatar = ResizeImage::resize_image($entity->getFullPath(), '160x160', 160, 160, 1); ?>
                                        <?php if($real_avatar) : ?>
                                            <?php $profile_avatar = $real_avatar;?>
                                        <?php endif;?>
                                        <i><img src="<?php echo $profile_avatar; ?>"></i>
                                        <p class="name-fl"><?php echo $entity->getFullName(); ?></p>
                                    </div>
                                    <div class="rating-box">
                                        <div class="rating"></div>
                                    </div>
                                    <span class="feedback-partner"><?php echo $entity->getNumReviews(); ?> nhận xét</span>
                                </div>
                                <div class="experience-partner">
                                    <!--<p><?php // echo $entity->getDescription(); ?></p>-->
                                    <p><b>5 năm</b> kinh nghiệm viết bài</p>
                                    <p>Đã làm dự án cho <b>Downy</b></p>
                                    <p>Từng làm việc ở <b>Coca Cola</b></p>
                                </div>
                                <div class="button">
                                    <a class="btn btn-vl btn-vl-small btn-vl-block btn-chose-fl">Chọn</a>
                                </div>
                            </div>
                            <?php /* trường hợp giới thiệu fl phù hợp nhất
                            <div class="partner-info appropriate-fl span3">
                                <div class="partner-hat">
                                    <p>Phù hợp nhất với yêu cầu của bạn</p>
                                </div>
                                <div class="partner-comment">
                                    <div class="avata-fl">
                                        <img src="/media/avatar1.png">
                                        <p class="name-fl"><?php echo $entity->getFullName(); ?></p>
                                    </div>
                                    <div class="rating-box">
                                        <div class="rating"></div>
                                    </div>
                                    <span class="feedback-partner"><?php echo $entity->getNumReviews(); ?> nhận xét</span>
                                </div>
                                <div class="experience-partner">
                                    <p><b>5 năm</b> kinh nghiệm viết bài</p>
                                    <p>Đã làm dự án cho <b>Downy</b></p>
                                    <p>Từng làm việc ở <b>Coca Cola</b></p>
                                </div>
                                <div class="button">
                                    <a class="btn btn-vl btn-vl-small btn-vl-block btn-chose-fl">Chọn</a>
                                </div>
                            </div> */ ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-commit">
        <div class="row-fluid">
            <div class="span1">
                <img src="/img/thue-ngay/Shield-icon.png">
            </div>
            <div class="span11">
                <h3>Cam kết về chất lượng - Hoàn tiền 100% nếu không hài lòng</h3>
                <p>Số tiền của bạn được bảo vệ an toàn trên hệ thống của chúng tôi. Cơ chế đảm bảo chất lượng dịch vụ tuyệt đối, chỉ thực hiện thanh toán cho đối tác nếu bạn hài lòng 100% về chất lượng dịch vụ.</p>
                <i>Hỗ trợ trực tiếp qua số điện thoại <b>024.6684.1818</b> hoặc gửi email <b>hotro@thuengay.vn</b></i>
                <i>Mã số hỗ trợ của bạn là <b><?php echo $order->getCode(); ?></b></i>
            </div>
        </div>
    </div>
    <div class="fotter-payment">
        <div class="button"><a id="btn-chose-start" class="btn btn-vl btn-vl-medium btn-vl-blue btn-vl-block">Xác nhận và bắt đầu làm việc</a></div>
        <p>Sau khi xác nhận, đối tác sẽ chủ động liên hệ với bạn qua trang tin nhắn</p>
    </div>
</div>

<?php $view['slots']->stop(); ?>