<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\ServiceBundle\Entity\ServicePack; ?>

<?php $view->extend('VlanceBaseBundle:thuengay:layout-1column.html.php')?>

<?php $view['slots']->start('content'); ?>

<div class="page-payment-service">
    <div class="container">
        <h1 class="title">Xác nhận đơn hàng</h1>
        <div class="block-step1">
            <h3 class="title">Bước 1: Xem lại thông tin đơn hàng</h3>
            <div class="section-step1">
                <div class="section-step1-top">
                    <div class="row-fluid">
                        <div class="span6">
                            <p>Tên dịch vụ</p>
                        </div>
                        <div class="span2"><p>Số lượng</p></div>
                        <div class="span2"><p>Đơn giá</p></div>
                        <div class="span2"><p>Thành tiền</p></div>
                    </div>
                </div>
                <div class="section-step1-content">
                    <div class="row-fluid">
                        <div class="span6">
                            <h3><?php echo $service_pack->getTitle(); ?></h3>
                            <?php if(count($service_addon) > 0): ?>
                                <?php foreach($service_addon as $sa): ?>
                                <p>
                                   <i class="fa fa-check" aria-hidden="true"></i> <?php echo $sa->getTitle(); ?>
                                </p>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="span2"><?php echo $qty; ?></div>
                        <div class="span2"><?php echo number_format($entity->getAmount() / $qty,'0','','.'); ?> ₫</div>
                        <div class="span2"><?php echo number_format($entity->getAmount(),'0','','.'); ?> ₫</div>
                    </div>
                </div>
            </div>    
            <div class="section-promotion">
                <div class="row-fluid">
                    <div class="code-promotion span6">
                        <h3>Mã khuyến mại</h3>
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" placeholder="Điền mã khuyến mại">
                            </div>
                            <label class="control-label" for="inputEmail">
                                <a class="btn btn-vl btn-vl-white btn-vl-small">Áp dụng</a>
                            </label>
                        </div>
                    </div> 
                    <div class="payment-service span6">
                        <div class="total-money row-fluid">
                            <div class="span7">Tổng cộng:</div>   
                            <div class="span5"><?php echo number_format($entity->getAmount(),'0','','.'); ?> đ</div>
                        </div>
                        <div class="vat-money row-fluid">
                            <div class="span7">Đã bao gồm trong đó VAT:</div>
                            <div class="span5"><?php echo number_format($entity->getAmount() * 10 / 100,'0','','.'); ?> đ</div>
                        </div>
                        <div class="discount-money row-fluid">
                            <div class="span7">Giảm giá:</div>
                            <div class="span5"><?php echo number_format($entity->getAmount() * $entity->getDiscount(),'0','','.'); ?> đ</div>
                        </div>
                        <div class="pay-money row-fluid">
                            <div class="span7">Số tiền phải thanh toán:</div>
                            <div class="span5"><?php echo number_format($entity->getAmount() - ($entity->getAmount() * $entity->getDiscount()),'0','','.'); ?> đ</div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="block-step2">
            <h1 class="title">Bước 2: Chọn hình thức thanh toán</h1>
            <div class="payment-international">
                <label class="radio">
                    <div class="row-fluid">
                        <div class="span6">
                                <input type="radio" name="payment-type" id="international" value="international" checked>
                                <p>Thẻ ngân hàng nội địa</p>
                            <p>Giao dịch được xử lí qua
                                <img src="/img/thue-ngay/napas-1Pay.png">
                            </p>
                        </div>
                        <div class="img-interna span6">
                            <p><img src="/img/thue-ngay/master-card-visa.png"></p>
                        </div>
                    </div>
                </label>    
            </div>
            <div class="payment-bank-vn">
                <label class="radio">
                    <div class="row-fluid">
                        <div class="span6">
                            <input type="radio" name="payment-type" id="bank-vn" value="bank-vn">
                            <p>Thẻ thanh toán quốc tế</p>
                            <p>Giao dịch được xử lí qua
                                <img src="/img/thue-ngay/napas-1Pay.png">
                            </p>
                        </div>
                        <div class="img-bank span6">
                            <p><img src="/img/thue-ngay/bank.png" title=""></p>
                        </div>
                    </div>
                </label>
            </div>
            <div class="payment-paypal">
                <label class="radio">
                    <div class="row-fluid">
                        <div class="span6">
                            <input type="radio" name="payment-type" id="paypal" value="paypal">
                            <p>Thanh toán online Paypal</p>
                        </div>
                        <div class="img-paypal span6">
                            <p><img src="/img/thue-ngay/paypal.png" title=""></p>
                        </div>
                    </div>
                </label>
            </div>
            <div class="payment-transfer">
                <label class="radio">
                   <div class="row-fluid">
                        <div class="span6">
                            <input type="radio" name="payment-type" id="transfer" value="transfer">
                            <p>Chuyển khoản ngân hàng</p>
                        </div>
                        <div class="img-transfer span6">
                            <p><img src="/img/thue-ngay/vietcombank.png"></p>
                        </div>
                    </div>  
                </label>
                <div class="block-info-bank closed">
                    <div class="code-orders">
                        <p>Khi thanh toán vui lòng ghi mã số đơn hàng của bạn vào nội dung chuyển khoản:</p>
                        <b><?php echo $entity->getCode(); ?></b>
                    </div>
                    <div class="note-transfer">
                        <i>Ví dụ minh họa nội dung chuyển khoản: <b>“Thanh toan cho don hang <?php echo $entity->getCode(); ?>”</b></i>
                    </div>
                    <div class="info-transfer">
                        <p class="title">Thông tin chuyển khoản</p>
                        <div class="row-fluid">
                            <div class="span2">
                                <p>Chủ tài khoản</p>
                            </div>
                            <div class="span9">
                                <b>Trần Ngọc Tuân</b>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span2">

                                <p>Số tài khoản</p>
                            </div>
                            <div class="span9">
                                <b>0011004141791</b>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span2">
                                <p>Chi nhánh</p>
                            </div>
                            <div class="span9">
                                <b>Sở giao dịch Hà Nội - Ngân hàng TMCP Ngoại Thương Vietcombank</b>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span2">
                                <p>Ngân hàng</p>
                            </div>
                            <div class="span9">
                                <b>Ngân hàng thương mại cổ phần Ngoại Thương - Vietcombank</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-commit">
            <div class="row-fluid">
                <div class="span1">
                    <img src="/img/thue-ngay/Shield-icon.png">
                </div>
                <div class="span11">
                    <h3>Cam kết về chất lượng - Hoàn tiền 100% nếu không hài lòng</h3>
                    <p>Số tiền của bạn được bảo vệ an toàn trên hệ thống của chúng tôi. Cơ chế đảm bảo chất lượng dịch vụ tuyệt đối, chỉ thực hiện thanh toán cho đối tác nếu bạn hài lòng 100% về chất lượng dịch vụ.</p>
                </div>
            </div>
        </div>
        <div class="fotter-payment">
            <div class="button">
                <input id="btn-pay" class="btn btn-vl btn-vl-medium btn-vl-blue btn-vl-block" value="Thanh toán">
            </div>
            <p>Sau khi bấm, bạn sẽ được tự động chuyển qua cổng thanh toán <b>napas, 1Pay<b></p>
        </div>
    </div>
</div>
<script type="text/javascript">
    var urlTransfer = "<?php echo $view['router']->generate('pay_transfer', array()) ?>";
    var orderId = <?php echo $entity->getId(); ?>;
</script>
<!--<div class="overlay-service-form">
    <div class="payment-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Thanh toán online Paypal</h3>
    </div>
    <div class="payment-body">
        <p>Bạn sẽ được chuyển sang cổng thanh toán <b>Paypal</b> trong 10 giây.</p>
        <p>Bấm vào nút "chuyển ngay" nếu bạn muốn thực hiện giao dịch ngay lập tức.</p>
    </div>
    <div class="button">
        <a class="btn btn-vl btn-vl-small btn-vl-white">Chuyển ngay</a>
    </div>
</div>-->


<?php $view['slots']->stop(); ?>