<?php $view->extend('VlanceBaseBundle:thuengay:layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost(). $view['assets']->getUrl('newsletter/img/logo_share.png'));  ?>
<?php $view['slots']->set('og_image', 'http://' . $request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg')); ?>
<?php $view['slots']->set('og_url', 'http://' . $request->getHost() . $view['router']->generate('thuengay_homepage')); ?>
<?php $view['slots']->set('og_description', $view['translator']->trans('home_page.og.description', array(), 'vlance')); ?>

<?php $view['slots']->start('content') ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<div class="homepage-thuengay">
    <div class="home-banner">
        <div class="container">
            <h1 class="lh8 mp-lh6"><?php echo $view['translator']->trans('home_page.banner.heading', array(), 'vlance');?></h1>
            <div class="button">
                <a class="btn btn-vl btn-vl-medium btn-vl-blue" onclick="vtrack('Click see all service packs', {'location':'Homepage'})" href="#see-all-service-packs" title="<?php echo $view['translator']->trans('home_page.banner_new.postjob', array(), 'vlance') ?>">Xem tất cả dịch vụ</a>
            </div>
        </div>
    </div>
    

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "http://www.vlance.vn/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.google.com/search?q=inurl%3Ahttp%3A%2F%2Fwww.vlance.vn%20{search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>

    <script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "WebSite",
      "name" : "vLance.vn",
      "alternateName" : "vLance.vn",
      "url" : "http://www.vlance.vn"
    }
    </script>
    
    <div class="step-section">
        <div class="container">
            <div class="row-fluid"> 
                <div class="content-image span6">
                    <img src="<?php echo $view['assets']->getUrl('img/thue-ngay/buoc-1.png'); ?>" title="Chọn dịch vụ cần thuê" alt="Chọn dịch vụ cần thuê">
                </div>
                <div class="content-text span6">
                    <b>Bước 1</b>
                    <h3>Chọn dịch vụ cần thuê</h3>
                    <p>Các dịch vụ cung cấp rõ ràng, chi tiết. Bạn có thể dễ dàng lựa chọn mà không mất quá nhiều thời gian.</p>
                </div>
            </div>    
        </div>     
    </div>
    <div class="step-section bl-gray">
        <div class="container">
            <div class="row-fluid">
                <div class="content-text span6">
                    <b>Bước 2</b>
                    <h3>Thanh toán an toàn</h3>
                    <p>Nhanh chóng, tuyệt đối an toàn và hỗ trợ nhiều hình thức thanh toán.</p>
                </div>
                <div class="content-image span6">
                    <img src="<?php echo $view['assets']->getUrl('img/thue-ngay/buoc-2.png'); ?>" title="Cam kết chặt chẽ. Làm việc nhanh chóng" alt="Cam kết chặt chẽ, Làm việc nhanh chóng"> 
                </div>
            </div>  
        </div>     
    </div>
    <div class="step-section">
        <div class="container">
            <div class="row-fluid">
                <div class="content-image span6">
                    <img src="<?php echo $view['assets']->getUrl('img/thue-ngay/buoc-3.png'); ?>" title="Nhận sản phẩm ưng ý. Thanh toán dễ dàng" alt="Nhận sản phẩm ưng ý. Thanh toán dễ dàng">
                </div>
                <div class="content-text span6">
                    <b>Bước 3</b>
                    <h3>Làm việc với chuyên gia</h3>
                    <p>Bạn có thể làm việc ngay với các chuyên gia hàng đầu trong các lĩnh vực, đều là các đối tác tin cậy của vLance.</p>
                </div>
            </div> 
        </div>    
    </div>
    <div class="commitment-section">
        <div class="container">
            <h3 class="title">
                Chất lượng dịch vụ tốt nhất <i class="fa fa-check-circle" aria-hidden="true"></i>
            </h3>
            <div class="row-fluid">
                <div class="commit-step span4">
                    <div><img src="../img/thue-ngay/thong-tin-xac-thuc.png"></div>
                    <div class="content">
                        <h4>Thông tin xác thực 100%</h4>
                        <p>Các chuyên gia đều đã được xác thực thông tin tài khoản, đảm bảo độ tin cậy và an toàn nhất khi làm việc</p>
                    </div>
                </div>
                <div class="commit-step span4">
                    <div><img src="../img/thue-ngay/uy-tin-tuyet-doi.png"></div>
                    <div class="content">
                        <h4>Uy tín tuyệt đối</h4>
                        <p>Chuyên môn cao, phong cách làm việc chuyên nghiệp, hiệu quả, đã được nhiều khách hàng kiểm chứng.</p>
                    </div>
                </div>
                <div class="commit-step span4">
                    <div><img src="../img/thue-ngay/dung-tien-do.png"></div>
                    <div class="content">
                        <h4>Đảm bảo đúng tiến độ</h4>
                        <p>Công việc sẽ bắt đầu ngay  sau khi thanh toán. Đảm bảo hoàn thành và bàn giao theo đúng tiến độ.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="category-section" id="see-all-service-packs">
        <div class="container">
            <div class="category-header">
                <ul>
                    <li class="active">Viết lách</li>
                    <li>Thiết kế</li>
                    <li>Lập trình</li>
                    <li>Dịch thuật</li>
                </ul>
            </div>
            <div class="category-body">
                <div class="row-fluid">
                    <div class="category-package span4">
                        <div class="package-image package-1">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>
                                <h4><a>Viết nội dung cho website</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        1.050.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div> 
                            </div> 
                        </div>     
                    </div> 
                    <div class="category-package span4">
                        <div class="package-image package-2">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>
                                <h4><a>Viết nội dung cho fanpage</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        85.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div>    
                            </div>
                        </div>    
                    </div> 
                    <div class="category-package span4">
                        <div class="package-image package-3">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>
                                <h4><a>Viết bài chuẩn SEO cho web tiếng việt</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box"></div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        120.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div> 
                            </div>  
                        </div>     
                    </div> 
                </div>
                <div class="row-fluid">
                    <div class="category-package span4">
                        <div class="package-image package-4">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>
                                <h4><a>Viết bài quảng cáo, tối ưu hóa</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        1.050.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div> 
                    <div class="category-package span4">
                        <div class="package-image package-5">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>    
                                <h4><a>Viết bài PR sáng tạo nhiều người đọc</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        85.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div>    
                            </div>
                        </div>    
                    </div> 
                    <div class="category-package span4">
                        <div class="package-image package-6">
                            <a class="item-overlay-upper tf200"></a>
                            <div class="item-overlay-inner tf200">
                                <a>123</a>
                            </div>
                        </div>
                        <div class="package-content">
                            <div>    
                                <h4><a>Viết bài cho trang vệ tinh để làm SEO</a></h4>
                                <p class="ellipsis">Tăng độ nhận diện thương hiệu. Giúp tăng gấp 5...</p>
                            </div>
                            <div class="rating-block">
                                <div class="rating-box"></div>
                                <span>(10 nhận xét)</span>
                            </div>
                            <div class="package-bottom">
                                <div class="row-fluid">
                                    <div class="buget span7">
                                        120.000<span>VNĐ</span>
                                    </div>
                                    <div class="button span5">
                                        <a class="btn btn-vl btn-vl-blue btn-vl-small">
                                            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Thuê dịch vụ
                                        </a>
                                    </div>
                                </div> 
                            </div>
                        </div>     
                    </div> 
                </div>
                <div class="view-more">
                    <a class="btn btl-vl btn-vl-white btn-vl-medium">Xem tất cả dịch vụ</a>
                </div>
            </div>    
        </div>
    </div>
    <div class="separator5"></div>
    <div class="reviews-section">
        <h3 class="review-client-title">Hơn <b>4.500 khách hàng</b> đã hài lòng</h3>
        <div class="review-clients container">
            <div class="row-fluid slide-reviews">
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-02.jpg'); ?>" alt="Nguyễn Quang Nhật" title="Nguyễn Quang Nhật">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Nguyễn Quang Nhật</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Freelancer hoàn thành công việc rất tốt và chuyên nghiệp, đáp ứng tất cả những yêu cầu của tôi đưa ra. Sẽ làm việc lại với freelancer này trong tương lai.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-01.jpg'); ?>" alt="Linh Đặng" title="Linh Đặng">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Linh Đặng</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>"Freelancer làm việc nhiệt tình và chiều ý khách. Xu hướng cập nhập nhanh. Giao tiếp lễ phép lịch sự. Quá trình làm việc rất suôn sẻ và cảm thấy dễ chịu. Mong em có nhiều đơn hàng."</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-03.jpg'); ?>" alt="Đặng Đức Toàn" title="Đặng Đức Toàn">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Đặng Đức Toàn</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Nhiệt tình, trách nhiệm, đáp ứng deadline công việc. Freelancer sẵn sàng chỉnh sửa theo các yêu cầu của khách hàng, đồng thời có thể đưa ra nhiều option để khách hàng lựa chọn. Một freelancer có thể hợp tác lâu dài.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-04.jpg'); ?>" alt="Lê Trí Minh" title="Lê Trí Minh">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Lê Trí Minh</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Hoàng Anh là freelancer rất tích cực trong công việc và giao tiếp, nhanh nhẹn và thông minh. Sẵn sàng hợp tác với bạn khi có dự án mới.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.slide-reviews').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3,
                responsive: [
                  {
                    breakpoint: 976,
                    settings: {
                      centerMode: true,  
                      centerPadding: '0px',
                      slidesToShow: 2
                      }
                  },
                  {
                    breakpoint: 639,
                    settings: {
                      centerPadding: '0px',
                      slidesToShow:1,
                      slidesToScroll: 1
                    }
                  }
                ]
              });
            });
        </script>
    
    </div>
</div>

<?php $view['slots']->stop(); ?>
