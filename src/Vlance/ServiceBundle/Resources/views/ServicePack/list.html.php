<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\ServiceBundle\Entity\ServicePack; ?>

<?php $view->extend('VlanceBaseBundle:thuengay:layout-1column.html.php')?>
<?php /*
<?php $view['slots']->set('title', $meta_title);?>
<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('description', $meta_description);?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/logo_share.png'));?>
<?php $view['slots']->set('og_url', 'http://'.$request->getHost() . $view['router']->generate('job_show_freelance_job',array('hash' => $entity->getHash())));?>
<?php $view['slots']->set('og_description', $meta_description);?>
*/ ?>

<?php $request = $this->container->get('request'); ?>

<?php $view['slots']->start('content') ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<div class="service_list">
    <div class="block-head-tn">
        <div class="header-sites-tn">
            <div class="container">
                <nav class="head-site-container">
                    <a href="" class="header-sites-copywriter active"></a>
                    <a href="" class="header-sites-translate"></a>
                    <a href="" class="header-sites-design"></a>
                    <a href="" class="header-sites-web-mobile"></a>
                    <a href="" class="header-sites-accountant-law"></a>
                    <a href="" class="header-sites-marketing"></a>
                </nav>
            </div>
        </div>
        <div class="header-categories-tn">
            <div class="container">
                <ul class="header-categories-links">
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết nội dung</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết báo, tạp chí</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết bài PR, truyền thông</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Chỉnh sửa bài viết</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="page-section">
        <div class="page-section-content theme-copywriter">
            <div class="container">
                <h1>Viết nội dung chất lượng cho website</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
        </div>
    </div>
    <div class="block-content">
        <div class="container">
            <div class="row-fluid">
                <div class="left-sidebar span2">
                    <div class="vertical accordin">
                        <div class="item">
                            <div class="title">Mức giá</div>
                            <div class="content-item">
                                <div class="checkbox">
                                    <label><input type="checkbox">50k - 500k </label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox">500k - 2 triệu</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox">2 triệu - 5 triệu</label>
                                </div>
                                <div class="checkbox">
                                    <label><input type="checkbox">5 triệu trở nên</label>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="title">Đánh giá</div>
                            <div class="content-item">
                                <a class="rating-content">
                                    <div class="rating-box">
                                        <div class="rating" style="width:20%;"></div>
                                    </div>
                                    <span>trở lên</span>
                                </a>
                                <a class="rating-content">
                                    <div class="rating-box">
                                        <div class="rating" style="width:40%;"></div>
                                    </div>
                                    <span>trở lên</span>
                                </a>
                                <a class="rating-content">
                                    <div class="rating-box">
                                        <div class="rating" style="width:60%;"></div>
                                    </div>
                                    <span>trở lên</span>
                                </a>
                                <a class="rating-content">
                                    <div class="rating-box">
                                        <div class="rating" style="width:80%;"></div>
                                    </div>
                                    <span>trở lên</span>
                                </a>
                            </div>    
                        </div>
                        <div class="item">
                            <div class="title">Nhận xét</div>
                            <div class="content-item comment-content">
                                <div><a>1 - 10 nhận xét</a></div>
                                <div><a>10 - 30 nhận xét</a></div>
                                <div><a>30 nhận xét trở nên</a></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="title">Giao sản phẩm</div>
                            <div class="content-item">
                                <div class=""><a>1 ngày - 2 ngày</a></div>
                                <div class=""><a>3 ngày - 7 ngày</a></div>
                                <div class=""><a>14 ngày trở nên</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-container span10">
                    <div class="catalog-status">
                        <div class="row-fluid">
                            <div class="catalog-status-title span9"><?php echo count($entities);?> dịch vụ</div>
                            <div class="catalog-status-filters span3">
                                <div class="dropdown">
                                    <a class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="/page.html">
                                      Giá: Thấp đến cao
                                      <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <li><a>Giá: Cao đến thấp</a></li>
                                        <li><a>Giá: Thấp đến cao</a></li>
                                        <li><a>Phổ biến</a></li>
                                        <li><a>Mới nhất</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $('.dropdown-toggle').dropdown();
                        </script>
                    </div>
                    <div class="wrap-list-item">
                        <div class="row-fluid">
                            <?php foreach($entities as $entity): ?>
                            <div class="item-service span3">
                                <div class="service-img">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <img src="/img/thue-ngay/4.png">
                                    </a>
                                </div>
                                <div class="item-content">
                                    <h4 class="post-title">
                                        <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title=""><?php echo $entity->getTitle(); ?></a>
                                    </h4>
                                    <div class="item-price">                                        
                                        <span class="amount-1">
                                            <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                                <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                            </a>
<!--                                        <span class="amount-2">10.000.000 <i>đ</i></span>
                                        <span class="sale-off">-80%</span>-->
                                    </div>
<!--                                    <div class="item-rating">
                                        <div class="rating-box">
                                            <div class="rating"></div>
                                        </div>
                                        <span class="count-user-rate">(1)</span>
                                    </div>-->
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
<!--                    <div class="layered-footer results-paging">
                        <a>1</a>
                        <span class="current">2</span>
                        <a>3</a>
                        <a>4</a>
                        <span class="dots">...</span>
                        <a>10</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop(); ?>
