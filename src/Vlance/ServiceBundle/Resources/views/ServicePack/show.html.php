<?php use Vlance\BaseBundle\Utils\Url; ?>
<?php use Vlance\BaseBundle\Utils\JobCalculator; ?>
<?php use Vlance\ServiceBundle\Entity\ServicePack; ?>

<?php $view->extend('VlanceBaseBundle:thuengay:layout-1column.html.php')?>

<?php $view['slots']->start('content'); ?>
<div class="show-service-tn">
    <div class="block-head-tn">
        <div class="header-sites-tn">
            <div class="container">
                <nav class="head-site-container">
                    <a href="" class="header-sites-copywriter active"></a>
                    <a href="" class="header-sites-translate"></a>
                    <a href="" class="header-sites-design"></a>
                    <a href="" class="header-sites-web-mobile"></a>
                    <a href="" class="header-sites-accountant-law"></a>
                    <a href="" class="header-sites-marketing"></a>
                </nav>
            </div>
        </div>
        <div class="header-categories-tn">
            <div class="container">
                <ul class="header-categories-links">
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết nội dung</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết báo, tạp chí</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Viết bài PR, truyền thông</a>
                    </li>
                    <li class="header-categories-links-item">
                        <a class="header-categories-main-link">Chỉnh sửa bài viết</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="show-service-content">
        <div class="container">
            <div class="row-fluid">
                <div class="block-content-left span9">
                    <div class="head-content-left">
                        <div class="row-fluid">
                            <div class="image-service span5">
                                <img src="/img/thue-ngay/fanpage.jpg">
                            </div>
                            <div class="detail-service span7">
                                <div class="">
                                    <h3 class="title"><?php echo $entity->getTitle(); ?></h3>
<!--                                    <div class="review-star">
                                        <div class="rating-box">
                                            <div class="rating"></div>
                                        </div>
                                        <div class="count-user-rate">10 nhận xét</div>
                                    </div>-->
                                </div>
                                <div class="item-price">
                                    <span class="amount-1">Giá: <b><?php echo number_format($entity->getPrice(),'0','','.' ) ?><i>₫</i></b></span>
<!--                                    <span class="amount-1">Giá mới: <b>100.000<i>₫</i></b></span>
                                    <span class="amount-2">Giá cũ: 250.000 ₫</span>
                                    <div class="sale-off"> Tiết kiệm: <b>150.000 ₫ (60%)</b></div>-->
                                </div>
                                <div class="time-approval"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <?php echo $entity->getDeliveried(); ?> ngày (hoặc sớm hơn) & <?php echo $entity->getReviews();?> lần duyệt sản phẩm
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="note-product span5">
                            <h3 class="title">Bạn sẽ nhận được</h3>
                            <?php foreach(preg_split("/((\r?\n)|(\r\n?))/", $entity->getResult()) as $line): ?>
                            <p>
                                <i class="fa fa-check" aria-hidden="true"></i><?php echo $line; ?>
                            </p>
                            <?php endforeach; ?>
                        </div>
                        <div class="info-pack-service span7">
                            <h3 class="title">Thông tin gói dịch vụ</h3>
                            <ul>
                                <?php foreach(preg_split("/((\r?\n)|(\r\n?))/", $entity->getDescription()) as $line): ?>
                                <li><?php echo $line; ?></li>
                                <?php endforeach; ?>
                            </ul>
<!--                            <ul>
                                <li>GIới thiệu về viết bài fanpage là gì? Có gì hay? Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. </li>
                                <li>GIới thiệu về viết bài fanpage là gì? Có gì hay? Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. </li>
                            </ul>-->
                            <h4>Cách làm việc</h4>
                            <ul>
                                <?php foreach(preg_split("/((\r?\n)|(\r\n?))/", $entity->getProcess()) as $line): ?>
                                <li><?php echo $line; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="experience-service-fl">
                        <h4 class="title">Kinh nghiệm của các đối tác cung cấp dịch vụ này</h4>
                        <p>Các đối tác cung cấp dịch vụ này đều là các chuyên gia có tối thiểu 3 năm kinh nghiệm, và đã được chúng tôi kiểm chứng. Sau đây là một trong những thành tích và kinh nghiệm nổi bật nhất.</p>
                        <div class="achievements-fl">
                            <div class="achi-featured fea-1">
                                <img src="/img/thue-ngay/kinh-nghiem.png">
                                <p>7 năm kinh nghiệm về viết nội dung, PR contented</p>
                            </div>
                            <div class="achi-featured fea-2">
                                <img src="/img/thue-ngay/lam-viec.png">
                                <p>Từng làm việc ở các công ty lớn Cocacola, Pepsi</p>
                            </div>
                            <div class="achi-featured fea-3">
                                <img src="/img/thue-ngay/du-an.png">
                                <p>Viết PR content cho dự án sản phẩm gia đình của Downy</p>
                            </div>
                        </div>
                    </div>
                    <div class="block-reviews">
                        <h4 class="title">Khách hàng hài lòng với dịch vụ</h4>
                        <div class="reviews-cl row-fluid">
                            <div class="avata span2"><img src="/media/avatar2.png"></div>
                            <div class="content-cl span10">
                                <div class="row-fluid">
                                    <div class="span7">
                                        <div class="name-client">Trần Quốc Trung</div>
                                        <div class="rating-box">
                                            <div class="rating"></div>
                                        </div>
                                    </div>
                                    <div class="time-post span5">2 giờ trước</div>
                                </div>
                                <p>Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã được sử dụng như một văn bản chuẩn cho ngành công nghiệp in ấn từ những năm 1500.</p>
                            </div>
                        </div>
                        <div class="reviews-cl row-fluid">
                            <div class="avata span2"><img src="/media/avatar3.png"></div>
                            <div class="content-cl span10">
                                <div class="row-fluid">
                                    <div class="span7">
                                        <div class="name-client">Trần Quốc Trung</div>
                                        <div class="rating-box">
                                            <div class="rating"></div>
                                        </div>
                                    </div>
                                    <div class="time-post span5">2 giờ trước</div>
                                </div>
                                <p>Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. Lorem Ipsum đã được sử dụng như một văn bản chuẩn cho ngành công nghiệp in ấn từ những năm 1500.</p>
                            </div>
                        </div>
                        <div class="button">
                            <div class="btn btn-vl btn-vl-white btn-vl-small">Xem tất cả đánh giá (20)</div> 
                        </div>
                    </div>
            </div>
            <div class="block-add-to-cart span3">
                <form id="order-form" action="<?php echo $view['router']->generate('submit_form', array()) ?>" method="post">
                    <input name="vlance_service_pack" type="hidden" value="<?php echo $entity->getHash();?>">
                    <div class="checkout">
                        <div class="service-pack row-fluid">
                            <div class="price span8"><span><?php echo number_format($entity->getPrice(),'0','','.' ); ?></span> ₫ <b>x</b></div>
                            <div class="quantity span4">
                                <select id="sp_qty" name="sp_quantity" class="dropdown">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <?php foreach($addons as $a):?>                        
                        <div class="add-on row-fluid">
                            <div class="span9">
                                <label class="checkbox"><input type="checkbox" name="sp_addon_list[]" value="<?php echo $a->getHash();?>">
                                    <?php echo $a->getTitle(); ?>
                                </label>
                            </div>
                            <div class="span3"><?php echo number_format($a->getPrice(),'0','','.' ); ?> ₫</div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    
                    <script type="text/javascript">
                        var price = <?php echo $entity->getPrice(); ?>;
                    </script>
                    
<!--                    <div class="service-total-paid">
                        <div class="total-money">380.000 <i>₫</i></div>
                        <p>Đã bao gồm VAT (10%): <b>38.000</b> ₫</p>
                    </div>-->
                    <div class="button">
                        <button type="submit" id="btn-submit-order" class="btn btn-vl btn-vl-blue btn-vl-medium btn-vl-block">Thanh toán</button>
                    </div>
                </form>    
            </div>
        </div>
        <div class="blog-introduce-more">
            <h4 class="title">Khách hàng đã thuê dịch vụ này sẽ thuê thêm</h4>
            <div class=" slide-view-item container">
                <div class="row-fluid slide-items">
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        10.000.000<i>₫</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 ₫</span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(8 nhận xét)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
            $(document).ready(function(){
                $('.slide-items').slick({
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 5,
                    responsive: [
                      {
                        breakpoint: 976,
                        settings: {
                          centerMode: true,  
                          centerPadding: '0px',
                          slidesToShow: 2
                          }
                      },
                      {
                        breakpoint: 639,
                        settings: {
                          centerPadding: '0px',
                          slidesToShow:1,
                          slidesToScroll: 1
                        }
                      }
                    ]
                  });
                });
            </script>
        </div>    
        <div class="block-questions">
            <div class="container">
                <div class="title">Câu hỏi thường gặp</div>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="section-ask-questions">
                            <div class="row-fluid">
                                <ul class="vote-section span1">
                                    <li class="vote-answer">
                                        <a class="vote-up"></a>
                                    </li>
                                    <li class="vote-number">
                                        <span>10</span>
                                    </li>
                                    <li class="vote-answer">
                                        <a class="vote-down"></a>
                                    </li>
                                </ul>
                                <div class="section-content-right span11">
                                    <div class="section-question">
                                        <div class="row-fluid">
                                            <div class="span2">Câu hỏi</div>
                                            <div class="span10">
                                                <p>Lorem Ipsum là gì?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-answer">
                                        <div class="row-fluid">
                                            <div class="span2">Trả lời</div>
                                            <div class="span10">
                                                <p>Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. </p>
                                                <div class="time-post">2 giờ trước</div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <div class="section-ask-questions">
                            <div class="row-fluid">
                                <ul class="vote-section span1">
                                    <li class="vote-answer">
                                        <a class="vote-up"></a>
                                    </li>
                                    <li class="vote-number">
                                        <span>10</span>
                                    </li>
                                    <li class="vote-answer">
                                        <a class="vote-down"></a>
                                    </li>
                                </ul>
                                <div class="section-content-right span11">
                                    <div class="section-question">
                                        <div class="row-fluid">
                                            <div class="span2">Câu hỏi</div>
                                            <div class="span10">
                                                <p>Lorem Ipsum là gì?</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-answer">
                                        <div class="row-fluid">
                                            <div class="span2">Trả lời</div>
                                            <div class="span10">
                                                <p>Lorem Ipsum chỉ đơn giản là một đoạn văn bản giả, được dùng vào việc trình bày và dàn trang phục vụ cho in ấn. </p>
                                                <div class="time-post">2 giờ trước</div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="section-send-question span4">
                        <form>
                            <h4>Bạn không thấy câu hỏi cần tìm?</h4>
                            <div>
                                <textarea id="form_send_question" placeholder="Đặt câu hỏi ở đây"></textarea>
                            </div>
                        </form>
                        <div class="button">
                            <a class="btn btn-vl btn-vl-small btn-vl-blue btn-vl-block">Gửi câu hỏi</a>
                        </div>
                    </div>
                </div>
                <div class="button">
                    <a class="btn btn-vl btn-vl-small btn-vl-white">Xem tất cả câu trả lời <b>(20)</b></a>
                </div>
            </div>
        </div>  
        <div class="blog-introduce-more">
            <h4 class="title">Khách hàng đã xem dịch vụ cũng đã mua thêm</h4>
            <div class=" slide-view-item container">
                <div class="row-fluid slide-items">
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        10.000.000<i>₫</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 đ</span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(8 nhận xét)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                    <div class="slide-item">
                        <div class="item-content">
                            <div class="img-item">
                                <img src="/img/thue-ngay/3.png">
                            </div>
                            <h4 class="post-title">
                                <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">Viết giới thiệu sản phẩm dịch vụ</a>
                            </h4>
                            <div class="item-price">                                        
                                <span class="amount-1">
                                    <a href="<?php echo $view['router']->generate("show_service_pack", array('hash' => $entity->getHash())); ?>" title="">
                                        <?php echo number_format($entity->getPrice(), 0, '', '.'); ?> <i>đ</i></span>
                                    </a>
                                <span class="amount-2">10.000.000 <i>₫</i></span>
                                <span class="sale-off">-80%</span>
                            </div>
                            <div class="item-rating">
                                <div class="rating-box">
                                    <div class="rating"></div>
                                </div>
                                <span class="count-user-rate">(1)</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
            $(document).ready(function(){
                $('.slide-items').slick({
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 5,
                    responsive: [
                      {
                        breakpoint: 976,
                        settings: {
                          centerMode: true,  
                          centerPadding: '0px',
                          slidesToShow: 2
                          }
                      },
                      {
                        breakpoint: 639,
                        settings: {
                          centerPadding: '0px',
                          slidesToShow:1,
                          slidesToScroll: 1
                        }
                      }
                    ]
                  });
                });
            </script>
        </div>     
    </div>
</div>
<script type="text/javascript">
    var price = <?php echo $entity->getPrice(); ?>;
</script>
<?php $view['slots']->stop(); ?>