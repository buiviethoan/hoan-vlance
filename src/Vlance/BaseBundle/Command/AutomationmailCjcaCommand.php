<?php
namespace Vlance\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vlance\BaseBundle\Utils\ResizeImage; 

/**
 * CRON FREQUENCY:  30 mins
 * DOCUMENTATION:   Cr.Job.Create-Award
 * FLOW:
 * - CJCA.M01:
 * - CJCA.M02:
 */
class AutomationmailCjcaCommand extends ContainerAwareCommand {
    protected $test_email = "tuan.tran@vlance.vn";
    protected $interval = 30;
    protected $test = false;
    protected $debug = false;
    protected $em = null;
    protected $from_noreply = "";
    protected $container;
    protected $monolog;
    
    protected function configure() 
    {
        $this->setName('vlance:automationmail:cjca')
                ->setDescription("Send automation mail to improve awarded jobs' conversion rate")
                ->addOption('test', NULL, InputOption::VALUE_NONE, "Send only to test_accounts.")
                ->addOption('debug', NULL, InputOption::VALUE_NONE, "Display debugging information.");
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) 
    {
        $this->_init_config($input, $output);
        
        // Client post 1st job, within 30 minutes, send cjca_m01
        $this->cjca_m01();
        $this->cjca_m02();
    }
    
    protected function _init_config(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getContainer();
        $this->monolog = $this->container->get("monolog.logger.command");
        $this->from_noreply = $this->container->get('vlance_base.helper')->getParameter('vlance_email.from.noreply');
        $this->em = $this->container->get('doctrine')->getManager();
        
        if($input->getOption('debug')){
            $this->debug = true;
        }
        
        if($input->getOption('test')){
            $this->log("In test mode");
            $this->test = true;
            $this->debug = true; // Enable debug in testing mode
        }
    }
    
    /**
     * Logging
     * 
     * @param type $text
     * @param type $title
     */
    protected function log($text = "", $title = "", $error_log = false)
    {
        if($title !== ""){
            $sep = "---- " . $title . " ----\n";
            echo $sep;
            $this->monolog->info($sep);
        }
        $this->monolog->info($text);
        
        if($error_log){
            error_log($text);
        }
        if($this->debug){
            print_r($text);
            echo "\n";
        }
    }
    
    protected function _mixpanel($people, $event)
    {
        require_once($this->container->getParameter('kernel.root_dir') . '/../src/Vlance/Lib/Mixpanel/lib/Mixpanel.php');
        $parameters = $this->container->getParameter("vlance_system");
        $mixpanel = \Mixpanel::getInstance($parameters['mixpanel_token']['prod']);
        $mixpanel->identify($people);
        $mixpanel->track($event);
    }
    
    protected function sendmail($template, $context, $sender, $receiver, $copy = NULL)
    {
        if($template && $sender && $receiver){
            try {
                if($this->test){
                    $receiver = $this->test_email;
                }
                $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $sender, $receiver);
                $this->log("Mail sent with success.");
                // Send a copy to $copy, without pixel_tracking
                if($copy){
                    if($context['pixel_tracking']){
                       unset($context['pixel_tracking']);
                    }
                    $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $sender, $copy);
                }
            } catch (Exception $ex) {
                $this->log("Mail sent with fail.");
                /**
                 * @todo Write log for email exception
                 */
            }
        }
    }
    
    // Every 30 minutes, Select only client who posted their first job
    protected function cjca_m01(){
        $this->container->get('router')->getContext()->setHost('www.vlance.vn');
        $template = "VlanceBaseBundle:Email/cjca:cjca_m01.html.twig";
        
        // SELECT id, account_id, count(*) AS jobNumbers FROM job WHERE account_id IN (SELECT account_id FROM job WHERE created_at > NOW() - INTERVAL 30 MINUTE) GROUP BY account_id HAVING jobNumbers = 1
        $sql_query = "SELECT id, account_id, count(*) AS jobNumbers "
                    . "FROM job "
                    . "WHERE "
                        . "account_id IN (SELECT account_id FROM job WHERE created_at > NOW() - INTERVAL " . $this->interval . " MINUTE) "
                    . "GROUP BY account_id "
                    . "HAVING jobNumbers = 1;";
        
        $this->log($sql_query);
        
        $jobs = $this->em->getConnection()->fetchAll($sql_query);
        
        $this->log(count($jobs));
        if(count($jobs) > 0){
            $parameters = $this->container->getParameter("vlance_system");
            foreach($jobs as $j){
                /* $j is an Array(
                        [id] => xxxx
                        [account_id] => xxxxxx
                        [jobNumbers] => 1 (yes, it's "ONE")
                )*/
                $job_owner = $this->em->getRepository('VlanceAccountBundle:Account')->find($j['account_id']);
                $job = $this->em->getRepository('VlanceJobBundle:Job')->find($j['id']);
                
                $context = array(
                    'job_owner' => $job_owner->getFullname(),
                    'job_id'    => $job->getId(),
                    'job_name'  => $job->getTitle(),
                    'mixpanel_tracking' => base64_encode(json_encode(
                        array(
                            'event' => "Notification CJCA-M01 Open",
                            'properties' => array(
                                'token' => $parameters['mixpanel_token']['prod'],
                                'distinct_id' => $job_owner->getId(),
                                'campaign'  => 'CJCA'
                            )
                        )
                    ))
                );
                
                $this->log($j);
                $this->_mixpanel($job_owner->getId(), "Notification CJCA-M01 Sent");
                $this->sendmail($template, $context, $this->from_noreply, $job_owner->getEmail());
            }
        }
    }
    
    // If get first bid, send an email
    protected function cjca_m02(){
        $this->container->get('router')->getContext()->setHost('www.vlance.vn');
        $template = "VlanceBaseBundle:Email/cjca:cjca_m02.html.twig";
        
        // Job with first bid within 30 minutes
        $sql_query =    "SELECT b.id AS first_bid_id, b.job_id, j.account_id AS client_id, b.created_at AS first_bid_at " .
                        "FROM (" .
                            "SELECT min(id) as id, created_at, job_id " .
                            "FROM bid " .
                            "WHERE job_id IS NOT NULL " .
                            "GROUP BY job_id" .
                        ") as b " .
                        "LEFT JOIN job j ON j.id = b.job_id " .
                        "WHERE b.created_at > NOW() - INTERVAL " . $this->interval . " MINUTE;";
        
        $this->log($sql_query);
        
        $bids = $this->em->getConnection()->fetchAll($sql_query);
        
        $this->log(count($bids));
        if(count($bids) > 0){
            $parameters = $this->container->getParameter("vlance_system");
            
            // Redefine const since Command can not get const from /app.php or /app_dev.php
            define('DS', DIRECTORY_SEPARATOR);
            define('ROOT_DIR', $this->container->getParameter('kernel.root_dir') . DS . ".." . DS . "web");
            define('UPLOAD_DIR', ROOT_DIR . DS . 'uploads');
            define('ROOT_URL', $this->container->get('router')->getContext()->getHost());
            define('UPLOAD_URL', ROOT_URL . '/uploads');
            
            foreach($bids as $b){
                /* $b is an Array(
                    [first_bid_id] => xxxxx
                    [job_id] => xxxx
                    [client_id] => xxxxx
                    [first_bid_at] => yyyy-mm-dd hh:mm:ss
                )*/
                $job_owner = $this->em->getRepository('VlanceAccountBundle:Account')->find($b['client_id']);
                $job = $this->em->getRepository('VlanceJobBundle:Job')->find($b['job_id']);
                $bid = $this->em->getRepository('VlanceJobBundle:Bid')->find($b['first_bid_id']);
                
                // Get skills id
                $skills = array();
                foreach($job->getSkills() as $skill){
                    $skills[] = $skill->getId();
                }
                
                //Get only top random 4 freelancers
                $freelancers = $this->em->getRepository('VlanceAccountBundle:Account')->getRandSuggestedFreelancers(array(
                        'category'  => $job->getCategory()->getId(),
                        'skills'    => $skills
                    ), 4);
                $suggest_freelancers = array();

                $hashString = $parameters['hash_string'];
                foreach($freelancers as $fl){
                    $avatar = ResizeImage::resize_image($fl->getFullPath(), '80x80', 80, 80, 1);
                    if(!$avatar){
                        $avatar = ROOT_URL . "/" .'img/unknown.png';
                    }
                    $suggest_freelancers[] = array(
                        'id'            => $fl->getId(),
                        'accountHash'   => md5($fl->getId() . "@$hashString"),
                        'fullName'      => $fl->getFullname(),
                        'rating'        => ($fl->getNumReviews() > 0)?round($fl->getScore() / $fl->getNumReviews(), 2):0,
                        'avatar'        => $avatar,
                        'isCertificated'=> $fl->getIsCertificated()
                    );
                }
                
                $context = array(
                    'job_owner'             => $job_owner->getFullname(),
                    'job_id'                => $job->getId(),
                    'jobHash'               => md5($job->getId() . "@$hashString"),
                    'job_name'              => $job->getTitle(),
                    'suggest_freelancers'   => $suggest_freelancers,
                    'mixpanel_tracking'        => base64_encode(json_encode(
                        array(
                            'event'     => "Notification CJCA-M02 Open",
                            'properties'=> array(
                                'token'         => $parameters['mixpanel_token']['prod'],
                                'distinct_id'   => $job_owner->getId(),
                                'campaign'      => 'CJCA'
                            )
                        )
                    ))
                );
                
                $this->_mixpanel($job_owner->getId(), "Notification CJCA-M02 Sent");
                $this->sendmail($template, $context, $this->from_noreply, $job_owner->getEmail());
            }
        }
    }
}