<?php
namespace Vlance\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vlance\BaseBundle\Entity\CustomQuery;
use Vlance\BaseBundle\Entity\CustomQueryResult;

class StatisticCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
                ->setName('vlance:statistic')
                ->setDescription('Vlance command for statistic');
//                ->addArgument('action', InputArgument::REQUIRED, 'Action that you want to execute')
//                ->addOption('test', NULL, InputOption::VALUE_NONE, "Run test for this command");
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $queries = $em->getRepository('VlanceBaseBundle:CustomQuery')->findBy(array('enable' => 1));
        $output->writeln("Find " . count($queries) . " queries");
        foreach ($queries as $query) {
            /* @var $query \Vlance\BaseBundle\Entity\CustomQuery */
            $output->write("Run query title: {$query->getTitle()}");
            $queryBuilder = $em->getConnection()->prepare($query->getQuery());
            $queryBuilder->execute();
            $results = $queryBuilder->fetch(\Doctrine\ORM\AbstractQuery::HYDRATE_SCALAR);
            $queryResult = new CustomQueryResult();
            $queryResult->setValue($results[0]);
            $query->addResult($queryResult);
            $queryResult->setQuery($query);
            $em->persist($queryResult);
            $em->flush();
            $output->writeln(" result: {$results[0]}");
        }
    }
}
