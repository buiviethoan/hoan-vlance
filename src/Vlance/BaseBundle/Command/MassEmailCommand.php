<?php
namespace Vlance\BaseBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Vlance\BaseBundle\Command\BaseCommand;
use Vlance\BaseBundle\Entity\MassEmail;

/**
 * CRON FREQUENCY:  5 mins
 */
class MassEmailCommand extends BaseCommand {
    protected function config() 
    {
        $this->setName('vlance:massemail:sendmassemail')
             ->setDescription("Send mass email campaigns.");
    }
    
    protected function exec(InputInterface $input, OutputInterface $output) 
    {
        $this->sendMassEmail($input, $output);
    }
        
    protected function sendMassEmail()
    {
        $em = $this->em;
        $campaigns = $this->em->getRepository('VlanceBaseBundle:MassEmail')->findBy(array('enabled' => 1));
        if(count($campaigns) == 0){
            $this->log("Nothing happen.");
            return true;
        }
        $this->log(count($campaigns), "Number of enabled Campaigns");
        
        foreach($campaigns as $campaign){
            // Check if campaign is READY or was temporarily PAUSED
            if($campaign->getScheduleAt() < new \DateTime()){
                if((is_null($campaign->getSentAt()) && $campaign->getStatus() == MassEmail::STATUS_READY) || $campaign->getStatus() == MassEmail::STATUS_PAUSED){
                    /* @var $campaign \Vlance\BaseBundle\Entity\MassEmail */
                    $this->log($campaign->getCampaignName(), "Campaign name");
                    $this->log($campaign->getReceiverQuery(), "SQL Query");

                    // Check if campaign is ready to send
                    //$this->log($campaign->getEnabled());
                    $this->log($campaign->getScheduleAt() ? date_format($campaign->getScheduleAt(), "Y/m/d H:i:s") : "");
                    $this->log($campaign->getSentAt() ? date_format($campaign->getSentAt(), "Y/m/d H:i:s") : "");        

                    $campaign->setSentAt(new \DateTime());
                    $campaign->setStatus(MassEmail::STATUS_RUNNING);
                    $em->persist($campaign);
                    $em->flush();

                    // Build receipts id list
                    $rsm = new ResultSetMapping();
                    $rsm->addEntityResult('VlanceAccountBundle:Account', 'a');
                    $rsm->addFieldResult('a', 'id', 'id');
                    $rsm->addFieldResult('a', 'email', 'email');
                    $rsm->addFieldResult('a', 'full_name', 'fullName');
                    $query = $em->createNativeQuery($campaign->getReceiverQuery(), $rsm);
                    $result = $query->getArrayResult();             // Return 2 dimensions array

                    $this->log(count($result), "Number of Receipts");

                    // Initialising some variables before sending email
                    $counter = (int)(is_null($campaign->getSentSuccessCounter()) ? 0 : $campaign->getSentSuccessCounter());

                    // Initialising email template
                    $template = 'VlanceBaseBundle:Email/massemail:content.html.twig';
                    $from = array(
                        'name' => $campaign->getFromName(),
                        'email' => $campaign->getFromEmail());
                    $subject = $campaign->getHeader();
                    $content = $campaign->getHtmlContent();
                    $tracking_id = $campaign->getTrackingId();
                    $campaign_id = $campaign->getId();

                    $email_in_session_counter = 0;
                    $status_paused = false;
                    foreach($result as $receipt){
                        /**
                         * %%full_name%%
                         */
                        // Break if 1000 emails were sent in this batch, pause it, and relaunch in next cron session
                        if($email_in_session_counter >= 1000) { 
                            $campaign->setStatus(MassEmail::STATUS_PAUSED);
                            $em->persist($campaign);
                            $em->flush();
                            $status_paused = true;
                            break;
                        }

                        // Check if mail was sent to this user before
                        $sent_list = explode(";",$campaign->getProgression());  

                        if(!in_array((string)($receipt['id']), $sent_list)){
                            //$this->log($receipt['id'], "Receipt ID");

                            $tracking_properties = array(
                                // for mixpanel
                                'tracking_id'   => $tracking_id,
                                'campaign_id'   => $campaign_id,
                                // for ga
                                'ec'            => 'massemail',     // event category
                                'ea'            => 'open',          // event action
                                'el'            => $tracking_id,    // event label
                                'cs'            => 'massemail',     // Campaign source
                                'cm'            => 'email',         // Campaign medium
                                'cn'            => $tracking_id     // Campaign name
                            );

                            $mixpanel_tracking  = $this->buildTrackingCodeMixpanel("MassEmail Open", $tracking_properties, $receipt['id']);
                            $this->log($mixpanel_tracking);
                            $ga_tracking = $this->buildTrackingCodeGa($tracking_properties, $receipt['id']);
                            $this->log($ga_tracking);

                            $context = array(
                                'subject'       => str_replace("%%full_name%%", $receipt['fullName'], $subject),
                                'content'       => str_replace("%%full_name%%", $receipt['fullName'], $content),
                                'tracking_id'   => $tracking_id,
                                'id'            => $campaign_id,
                                'pixel_tracking'=> array(
                                        'mixpanel'  => $campaign->getTrackOpen() ? $mixpanel_tracking : "",
                                        'ga'        => $ga_tracking
                                )
                            );
                            //$this->log($context);

                            // Set receipt email then send email
                            if($this->isTestMode()){
                                $to = "tuan.tran@vlance.vn";
                            } else {
                                $to = $receipt['email'];   
                            }

                            try {
                                $this->container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
                                //print_r("#" . $receipt['id'] . " - ");
                                $counter++;
                                $campaign->setSentSuccessCounter($counter);

                                array_push($sent_list, $receipt['id']);
                                $campaign->setProgression(implode(";", $sent_list));

                                $em->persist($campaign);
                                $em->flush();
                            } catch (Exception $ex) {
                                /**
                                 * @todo Write log for email exception
                                 */
                            }

                            // Tracking mixpanel Sent event
                            if($campaign->getTrackSent()){
                                $this->mixpanel('MassEmail Sent', array('campaign'  => $campaign->getTrackingId()), $receipt['id']);
                            }
                            //$this->log("Massemail #" . $campaign->getId() . " - sent to UID #" . $receipt['id'] . " " . $receipt['email']);


                            // In test mode, send only 1 email then stop
                            if($this->isTestMode()){
                                break;
                            }
                            
                            $email_in_session_counter++;
                        }
                    }
                    if($status_paused == false){
                        $campaign->setStatus(MassEmail::STATUS_COMPLETE);
                        $em->persist($campaign);
                        $em->flush();
                    }
                    return; // Run only one campaign each time
                }
            }
        }
        $this->log("Sending completed.");
    }
}