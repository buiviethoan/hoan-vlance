<?php
namespace Vlance\BaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * CRON FREQUENCY:  5 mins
 */
class BaseCommand extends ContainerAwareCommand {
    protected $test = false;
    protected $debug = false;
    protected $em = null;
    protected $container;
    protected $mixpanel;
    protected $monolog;
    
    protected function configure() 
    {
        $this->setName('vlance:basecommand:base')
             ->setDescription("Base command class")
             ->addOption('test', NULL, InputOption::VALUE_NONE, "Send only to test_accounts.")
             ->addOption('debug', NULL, InputOption::VALUE_NONE, "Display debugging information.");
        $this->config();
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) 
    {
        $this->init_config($input, $output);
        $this->exec($input, $output);
    }
    
    protected function init_config(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getContainer();
        $this->monolog = $this->container->get("monolog.logger.command");
        $this->em = $this->container->get('doctrine')->getManager();
     
        $context = $this->container->get('router')->getContext();
        $context->setHost('www.vlance.vn');
        $context->setScheme('http');
        
        if($input->getOption('debug')){
            $this->log("In debug mode");
            $this->debug = true;
        }
        
        if($input->getOption('test')){
            $this->log("In test mode");
            $this->test = true;
        }
        
        $parameters = $this->container->getParameter("vlance_system");
        
        require_once($this->container->getParameter('kernel.root_dir') . '/../src/Vlance/Lib/Mixpanel/lib/Mixpanel.php');
        $token = ($this->isTestMode()) ? $parameters['mixpanel_token']['dev'] : $parameters['mixpanel_token']['prod'];
        $mixpanel = \Mixpanel::getInstance($token);
        $this->mixpanel = $mixpanel;
    }
    
    /**
     * Logging
     * 
     * @param type $text
     * @param type $title
     */
    protected function log($text = "", $title = "", $error_log = false)
    {
        if($title !== ""){
            $sep = "---- " . $title . " ----\n";
            echo $sep;
            $this->monolog->info($sep);
        }
        $this->monolog->info($text);
        
        if($error_log){
            error_log($text);
        }
        if($this->debug){
            print_r($text);
            echo "\n";
        }
    }
    
    /**
     * Set monologger
     */
    protected function setLogger($logger)
    {
        if($this->get($logger)){
            $this->monolog = $this->get($logger);
        }
    }
    
    /**
     * Track to Mixpanel
     * 
     * @param type $event
     * @param type $properties
     * @param type $distinct_id
     * @return type
     */
    protected function mixpanel($event, $properties = array(), $distinct_id = null)
    {
        if(!is_null($distinct_id)){
            $this->mixpanel->identify($distinct_id);
        }
        $this->mixpanel->track($event, $properties);
        return $this->mixpanel;
    }
    
    /**
     * Build tracking code for Mixpanel
     * 
     * @param type $event
     * @param type $properties
     * @param type $distinct_id
     * @return type
     */
    protected function buildTrackingCodeMixpanel($event, $properties, $distinct_id)
    {
        $parameters = $this->container->getParameter("vlance_system");
        $properties['token'] = ($this->isTestMode()) ? $parameters['mixpanel_token']['dev'] : $parameters['mixpanel_token']['prod'];
        $properties['distinct_id'] = $distinct_id;
        
        $mixpanel_data = base64_encode(json_encode(
            array(
                'event' => $event,
                'properties' => $properties
            )
        ));
        return "http://api.mixpanel.com/track/?data=" . $mixpanel_data . "&ip=1&img=1";
    }
    
    /**
     *  Build tracking code for GA
     * 
     * @param type $properties
     * @param type $distinct_id
     * @return type
     */
    protected function buildTrackingCodeGa($properties, $distinct_id)
    {
        return "http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=" . $properties['ec'] . "&ea=" 
                . $properties['ea'] . "&el=" . $properties['el'] . "&cs=" . $properties['cs'] . "&cm=" . $properties['cm'] . "&cn=" . $properties['cn'];
    }
    
    protected function isTestMode()
    {
        return $this->test;
    }
        
    /**
     * Overwrite configure() function
     */
    protected function config() {}
    
    /**
     * Overwrite configure() function
     */
    protected function exec(InputInterface $input, OutputInterface $output) {}
}