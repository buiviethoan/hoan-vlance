<!DOCTYPE html>
<html>
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="vi">
    
            <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="vLance.vn" />
    <link rel="publisher" href="http://plus.google.com/110723961725253795351/"/>
    <title>Freelancer Việt Nam - Lựa chọn số 1 của doanh nghiệp - vLance.vn</title>
    <meta property="og:site_name" content="vLance.vn" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />    
    <meta property="og:title" name="title" content="Freelancer Việt Nam - Lựa chọn số 1 của doanh nghiệp - vLance.vn" />
    <meta property="og:image" content="http://www.vlance.vn/newsletter/img/share_vlance_home.jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="628" />
    <meta property="og:url" content="http://www.vlance.vn" />
    <meta property="og:description" itemprop="description" name="description" content="Thuê freelancer lập trình, thiết kế, marketing, viết lách, dịch thuật, v.v. trên toàn Việt Nam với giá hợp lý, giúp tiết kiệm 50% chi phí cho doanh nghiệp." />
    <meta property="og:keywords" itemprop="keywords" name="keywords" content="freelancer, freelancer là gì, freelancer việt nam, tuyển freelancer, freelance, tìm việc freelance, tìm việc làm thêm, việc làm tự do, việc làm online" />
    
    <link href="http://www.vlance.vn/rss.xml" rel="alternate" type="application/rss+xml" title="Việc freelance mới nhất cho Freelancer"/>
    <link itemprop="sameAs" href="http://www.facebook.com/vlance.vn"/>
    <link itemprop="sameAs" href="https://twitter.com/vlancevn"/>
    <link itemprop="sameAs" href="https://www.linkedin.com/company/vlance-vn"/>
    <link itemprop="sameAs" href="https://plus.google.com/+VlanceVn"/>
    
        <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Organization",
        "name" : "vLance.vn",
        "url": "http://www.vlance.vn/",
        "logo": "http://www.vlance.vn/img/logo.png",
        "contactPoint" : [
            { 
                "@type" : "ContactPoint",
                "telephone" : "+84-4-66841818",
                "contactType" : "customer service",
                "contactOption" : "TollFree",
                "areaServed" : "VN",
                "availableLanguage" : ["English","Vietnamese"]
            }
        ],
        "sameAs" : [ 
            "http://www.facebook.com/vlance.vn",
            "https://twitter.com/vlancevn",
            "https://www.linkedin.com/company/vlance-vn",
            "https://plus.google.com/+VlanceVn"
        ] 
    }
    </script>
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,vietnamese' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/vlance.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/vlance-responsive.css" rel="stylesheet" type="text/css" media="all" />
    <link href="/css/slider_home.min.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE 8]>
    <link href="/css/vlance-ie8.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    <!--[if IE 7]>
    <link href="/css/vlance-ie7.css" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript" src="/js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/locales/bootstrap-datepicker.vn.min.js"></script>
    <script type="text/javascript" src="/js/hinclude.min.js"></script>
    <script type="text/javascript" src="/js/jquery.localisation.min.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/scriptsall.js"></script>
    <script type="text/javascript" src="/js/common.min.js"></script>
    
    <!--[if IE]>
    <script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
    <script>
        $(function() {
            $('input, textarea').placeholder();

        });
    </script>
    <style type="text/css">
        input, textarea { color: #000; }
        .placeholder { color: #aaa; }
    </style>
    <![endif]-->
               
        
    </head>

    <body class="one-column error-page-500">
    
        <div id="fb-root"></div>
            
    <div class="navbar navbar-static-top">
        <div class="container">
            <div class="row-fluid">
                <div class="span3 emblem">
                    <div class="logo"><a href="/">
                        <img src="/img/logo.png" 
                             alt="vLance.vn - Thuê Freelancer Việt Nam nhanh và hiệu quả" 
                             title="vLance.vn - Thuê Freelancer Việt Nam nhanh và hiệu quả"/></a></div>
                </div>
                <div class="span6" id="topmenu">
                    <div class="menu-inner">
                        <ul class="nav no-space">
                            <li class="menu-item first ">
                                <a class="" title="Tìm Việc" href="/viec-lam-freelance">Tìm Việc</a>
                            </li>
                            <li class="menu-item second ">
                                <a class="" title="Tìm Freelancer" href="/freelancers">Tìm Freelancer</a>
                            </li>
                            <li class="menu-item last">
                                <a class="tro-giup" title="Trợ giúp" href="/tro-giup">Trợ giúp</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="span3 btn-connect">
                    <div class="login-vlance">
                        <div class="network-socail">
                            <a rel="nofollow" class="btn-facebook" title="Login Facebook" onclick="fb_login()"><img src="/img/icon-facebook.png">Facebook</a>
                            <a rel="nofollow" class="btn-google-plus" title="Login Google" onclick="google_login()"><img src="/img/icon-google+.png">Google</a>                        </div>
                        <div class="create-account-vlance">
                            <a href="/a/register">
                                   Đăng ký /
                            </a>
                        </div>
                        <p><a rel="nofollow" href="/login">Đăng nhập</a></p>    
                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- about page -->
		<div class="error-section container">
                    <div class="row-fluid">
                        <h1>Trang web đang được bảo trì</h1><br/>
                        <p>Thời gian bảo trì dự kiến:<br/>từ <b>23:00 26/04/2016</b> đến <b>02:00 27/04/2016</b></p>
                        <p>Mời bạn vui lòng truy cập lại sau.<br/>Xin lỗi vì sự gián đoạn này.</p>
                        <div class="map-error-500"></div>
                    </div>
                </div>
		<!-- End about page -->
		
		
		<!-- Footer -->
		        <div class="block">
    <footer>
    <div class="footer-links-parter">
        <div class="container">
            <div class="row-fluid vl-relationship">
                <div class="vl-funding span5">
                    <p>Tài trợ</p>
                    <div class="google-footer">
                        <a href="https://www.google.com/" target="_blank" rel="nofollow" title="Google" alt="Google"></a>
                    </div>
                    <div class="aws-footer">
                        <a href="https://aws.amazon.com/" target="_blank" rel="nofollow" title="Amazon web services" alt="Amazon web services"></a>
                    </div>
                    <div class="mixpanel-footer">
                        <a href="https://mixpanel.com/f/partner" target="_blank" rel="nofollow" title="Mobile Analytics" alt="Mobile Analytics"></a>
                    </div>
                </div>
                <div class="vlance-partner span7">
                    <p>Đối tác</p>
                    <div class="magestore-footer">
                        <a href="http://www.magestore.com/" target="_blank" rel="nofollow" title="Magestore" alt="Magestore"></a>
                    </div>
                    <div class="money-lover-footer">
                        <a href="http://moneylover.me/" target="_blank" rel="nofollow" title="Money Lover" alt="Money Lover"></a>
                    </div>
                    <div class="topcv-footer">
                        <a href="http://www.topcv.vn/" target="_blank" rel="nofollow" title="TopCv" alt="TopCv"></a>
                    </div>
                    <div class="ybox-footer">
                        <a href="http://www.ybox.vn/" target="_blank" rel="nofollow" title="Ybox.vn" alt="Ybox.vn"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-links-section grey-bg">
        <div class="container">
            <div class="row-fluid">
                <div class="job-category span2">
                    <h4><a href="/j" title="Việc freelance">Việc freelance</a></h4>
                    <div class="row-fluid">
                                                <ul class="first span12">
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-it-va-lap-trinh_chash_lap-trinh-web">Lập trình web</a></li>
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-it-va-lap-trinh_chash_ung-dung-di-dong">Lập trình di động</a></li>
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-thiet-ke_chash_thiet-ke-do-hoa">Đồ họa & Thiết kế</a></li>
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-it-va-lap-trinh_chash_toi-uu-cho-cong-cu-tim-kiem-seo">SEO</a></li>
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-thiet-ke_chash_thiet-ke-website">Thiết kế website</a></li>
                            <li><a href="/viec-lam-freelance/cpath_cac-cong-viec-viet-lach_chash_quan-ly-blog-fanpage">Quản lý fanpage</a></li>
                        </ul>
                    </div>
                </div>
                <div class="span2">
                    <h4><a href="/" title="Làm freelance">Làm freelance</a></h4>
                    <div class="row-fluid">
                        <ul class="first span12">
                            <li> <a href="/viec/it-va-lap-trinh">IT và lập trình</a></li>
                            <li><a href="/viec/thiet-ke">Thiết kế</a></li>
                            <li><a href="/viec/marketing-ban-hang">Marketing & Bán hàng</a></li>
                            <li><a href="/viec/viet-lach-dich-thuat">Viết lách & dịch thuật</a></li>
                        </ul>
                    </div>
                </div>
                <div class="span2">
                    <h4><a href="/" title="Freelancer">Freelancer</a></h4>
                    <div class="row-fluid">
                        <ul class="first span12">
                            <li><a href="/freelancers/cpath_cac-cong-viec-it-va-lap-trinh_chash_lap-trinh-web">Thiết kế web</a></li>
                            <li><a href="/freelancers/cpath_cac-cong-viec-it-va-lap-trinh_chash_ung-dung-di-dong">Lập trình mobile</a></li>
                            <li><a href="/freelancers/cpath_cac-cong-viec-thiet-ke">Thiết kế đồ họa</a></li>
                            <li><a href="/freelancers/cpath_cac-cong-viec-it-va-lap-trinh_chash_toi-uu-cho-cong-cu-tim-kiem-seo">SEO</a></li>
                            <li><a href="/freelancers/cpath_cac-cong-viec-viet-lach">Viết lách & dịch thuật</a></li>
                            <li><a href="/freelancers/cpath_cac-cong-viec-marketing-kinh-doanh">Marketing</a></li>
                        </ul>
                    </div>
                </div>
                <div class="about-us span2">
                    <h4><a href="/" title="vLance.vn">vLance.vn</a></h4>
                    <ul>
                        <li><a href="/page/gioi-thieu-ve-vlance" title="Giới thiệu về vLance">Giới thiệu về vLance</a></li>
                        <li><a href="/page/dieu-khoan-su-dung" title="Điều khoản sử dụng">Điều khoản sử dụng</a></li>
                        <li><a href="/page/dieu-khoan-su-dung-danh-cho-khach-hang" title="Điều khoản khách hàng">Điều khoản khách hàng</a></li>
                        <li><a href="/page/dieu-khoan-su-dung-danh-cho-freelancer" title="Điều khoản freelancer">Điều khoản freelancer</a></li>
                                                <li><a href="/doi-tac?slug=doi-tac" title="Đối tác">Đối tác</a></li>
                        <li><a rel="nofollow" target="_blank" href="/page/lien-he" title="Liên hệ">Liên hệ</a></li>
                        <li><a target="_blank" href="http://blog.vlance.vn" title="Blog vLance">Blog vLance</a></li>
                    </ul>
                </div>
                <div class="information span2">
                    <h4>Bạn nên biết</h4>
                    <ul>
                        <li><a href="/tro-giup" title="Trợ giúp">Trợ giúp</a></li>
                        <li><a href="/page/cau-hoi-thuong-gap">Câu hỏi thường gặp</a></li>
                        <li><a href="/page/quy-dinh-bao-mat" title="Quy định bảo mật">Quy định bảo mật</a></li>
                        <li><a href="/page/huong-dan-thanh-toan">Hướng dẫn thanh toán</a></li>
                        <li><a href="/page/huong-dan-su-dung-cho-khach-hang">Hướng dẫn sử dụng</a></li>
                        <li><a href="/page/goi-y-hoan-thien-ho-so">Gợi ý hoàn thiện hồ sơ</a></li>
                        <li><a href="/page/xac-thuc-ho-so">Xác thực hồ sơ</a></li>
                    </ul>
                </div>
                <div class="social span2">
                    <h4 class="row-fluid">Kết nối ngay</h4>
                    <a rel="nofollow" href="http://www.facebook.com/vlance.vn" target="_blank" class="i-facebook"></a>
                    <a rel="nofollow" href="https://www.linkedin.com/company/vlance-vn" target="_blank" class="i-linkedin"></a>
                    <a rel="nofollow" href="https://twitter.com/vlancevn" target="_blank" class="i-twitter"></a>
                    <a rel="nofollow" href="https://plus.google.com/+VlanceVn" target="_blank" class="i-google"></a>
                    <a rel="nofollow" href="https://www.youtube.com/user/vlancevietnam" target="_blank" class="i-youtube"></a>
                </div>
                <div class="span2" style="margin-top: 15px;">
                    <h4 class="row-fluid">Hỗ trợ thanh toán</h4>
                    <div class="nlh-partner">
                        <ul class="">
                            <li><a rel="nofollow" class="visacrd" title="Thẻ thanh toán VisaCard" href="http://www.vietnam-visa.com/" target="_blank"></a></li>
                            <li><a rel="nofollow" class="mastercrd" title="Thẻ thanh toán MasterCard" href="http://www.mastercard.com/" target="_blank"></a></li>
                            <!--<li><a rel="nofollow" class="paypal" title="Hệ thống thanh toán trực tuyến Paypal" href="https://www.paypal.com/" target="_blank"></a></li>-->
                            <li><a rel="nofollow" class="vietcombank" title="Ngân hàng TMCP Ngoại Thương Việt Nam" href="http://www.vietcombank.com.vn/" target="_blank"></a></li>			
                        </ul>
                    </div>
                </div>
                <div class="span2" style="margin-top: 15px;">
                    <h4 class="row-fluid">Ngôn ngữ</h4>
                    <div class="switch-language dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Tiếng Việt <b class="caret"></b></a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                <li><a href="/language/en">English</a></li>
                <li><a href="/language/vn">Tiếng Việt</a></li>
            </ul>
</div>                </div>
            </div>
        </div>
    </div>
    <div class="bottom-logo">
        <div class="container">
            <p class="vlance-logo row-fluid">
                <a href="/" title="Thuê freelancer Việt Nam nhanh và hiệu quả"></a>
            </p>
            <p class="copyright">Freelancer Việt Nam - Lựa chọn số 1 của doanh nghiệp</p>
            <p class="copyright">© 2013 - 2017 vLance.vn</p>
            <p class="address">Chủ quản: Công ty TNHH vLance Việt Nam - Giấy phép đăng ký kinh doanh số 0106252670 - Cấp ngày 05/08/2013 - Tại Sở Kế hoạch và Đầu tư Hà Nội.</p>
            <p class="address">Địa chỉ: Tầng 4, số 2, ngõ 68, đường Nam Đồng, phường Nam Đồng, quận Đống Đa, Hà Nội</p>
            <p class="address">Hỗ trợ: <a href="mailto:hotro@vlance.vn">hotro@vlance.vn</a> - ĐT: (+84)-024.6684.1818</p>            
        </div>
    </div>
</footer>    </div>
    
    </body>
</html>
