<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('partner_accesstrade_page.og.title', array(), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans('partner_accesstrade_page.og.title', array(), 'vlance')); ?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost(). $view['assets']->getUrl('newsletter/img/logo_share.png'));  ?>
<?php $view['slots']->set('og_image', 'http://' . $request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg')); ?>
<?php $view['slots']->set('og_url', 'http://' . $request->getHost() . $view['router']->generate('vlance_homepage')); ?>
<?php $view['slots']->set('og_description', $view['translator']->trans('partner_accesstrade_page.og.description', array(), 'vlance')); ?>

<?php $view['slots']->start('content') ?>

<div class="partner-accesstrade">
    <div class="main_content_accesstrade">
        <div class="jumbotron-accesstrade-section">
            <div class="container">
                <h1 class="lh7">Kiếm thêm thu nhập<br/>
                    từ <span>blog, website</span> của bạn
                </h1>
                <p class="lh2">Đăng ký làm đối tác Accesstrade để kiếm tiền online thật hiệu quả</p>
                <div class="jumbotron-accesstrade-button">
                    <a class="btn btn-accesstrade-register">Đăng ký ngay</a>
                </div>
                <div class="partner-logo">
                    <p>Chương trình hợp tác bởi</p>
                    <img src="/img/partner-accesstrade/vlance-accesstrade-logo.png" alt="Logo vLance-Accesstrade" title="Logo vLance-Accesstrade">
                </div>
            </div>
        </div>
        <div class="guide-accesstrade-section">
            <div class="container">
                <h3>Cách thức tham gia</h3>
                <div class="row-fluid">
                    <div class="guide-accesstrade span3">
                        <div class="guide-step step1-partner">
                            <h4>Bước 1</h4>
                            <p>Đăng ký làm đối tác</p>
                        </div>
                    </div>
                    <div class="guide-accesstrade span3">
                        <div class="guide-step step2-partner">
                            <h4>Bước 2</h4>
                            <p>Chọn chiến dịch</p>
                        </div>
                    </div>        
                    <div class="guide-accesstrade span3">
                        <div class="guide-step step3-partner">
                            <h4>Bước 3</h4>
                            <p>Đăng quảng cáo<br/>
                               lên blog, website...
                            </p>
                        </div>
                    </div>
                    <div class="guide-accesstrade span3">
                        <div class="guide-step step4-partner">
                            <h4>Bước 4</h4>
                            <p>Nhận hoa hồng<br/>
                               cho mỗi đơn hàng*
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator"></div>
        <div class="profit-partner-section">
            <div class="container">
                <h3>Lợi ích khi tham gia</h3>
                <div class="row-fluid">
                    <div class="profit-text span7">
                        <ul>
                            <li>
                                <p><i class="fa fa-check" aria-hidden="true"></i>Kiếm thêm thu nhập từ blog, website...<p/>
                            </li>
                            <li>
                                <p><i class="fa fa-check" aria-hidden="true"></i>
                                Nhận tiền lên đến 50.000 VNĐ cho mỗi đơn hàng thanh toán.<p/>
                            </li>
                            <li>
                                <p><i class="fa fa-check" aria-hidden="true"></i>
                                Hợp tác với rất nhiều các thương hiệu uy tín như Adayroi, Tiki, ANZ, CocCoc...<p/>
                            </li>
                        </ul>
                    </div>
                    <div class="profit-register span5">
                        <a class="btn btn-accesstrade-register">Đăng ký ngay</a>
                        <p>Để kiếm được đơn hàng đầu tiên ngay hôm nay</p>
                    </div>
                </div>    
            </div>
        </div>
        <div class="carousel-caption-section">
            <div class="container">
                <div class="note-accesstrade">
                    <p class="line-partner"></p>
                    <p>*Mức hoa hồng được tính theo 3 hình thức chính: CPS (Cost Per Sale) tính theo tỉ lệ % trên mỗi đơn hàng thành công. CPL (Cost Per Lead) tính theo mỗi form đăng ký, điền thông tin thành công. CPI (Cost Per Install) tính theo mỗi lượt cài đặt ứng dụng thành công.</p>
                </div>
            </div>
        </div>
    </div>    
</div>

<?php $view['slots']->stop(); ?>