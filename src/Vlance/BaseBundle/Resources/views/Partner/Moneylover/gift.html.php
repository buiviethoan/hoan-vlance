<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', "Hướng dẫn kích hoạt tài khoản PREMIUM của Money Lover | vLance.vn"); ?>

<?php $view['slots']->set('description', "Hướng dẫn kích hoạt tài khoản Premium của Money Lover với các thành viên vLance.vn nhận được mã quà tặng sau khi mua Credit.");?>
<?php $view['slots']->set('keyword', "hướng dẫn, kích hoạt tài khoản, Money Lover, premium");?>

<?php $view['slots']->set('og_title', "Hướng dẫn kích hoạt tài khoản PREMIUM của Money Lover | vLance.vn");?>
<?php $view['slots']->set('og_description', "Hướng dẫn kích hoạt tài khoản Premium của Money Lover với các thành viên vLance.vn nhận được mã quà tặng sau khi mua Credit.");?>
<?php $view['slots']->set('og_keyword', "hướng dẫn, kích hoạt tài khoản, Money Lover, premium");?>
<?php $view['slots']->set('og_url', $view['router']->generate('partner_moneylover_gift', array(), true));?>

<?php $view['slots']->start('content') ?>

<div class="cms-section container " data-display-track="show-money-lover-gift-guide">
    <div class="span12">
        <h1>Hướng dẫn kích hoạt tài khoản PREMIUM của Money Lover</h1>
        <div class="wrapper-content">
            <p>
                <b>Bước 1: Tải ứng dụng</b><br/>
                Tải ứng dụng Money Lover tại <b><a href="https://moneylover.me/download" target="_blank" rel="nofollow">https://moneylover.me/download</a></b>.<br/>
                Link sẽ tự động xác định thiết bị của bạn và đưa vào Chợ ứng dụng phù hợp.<br/>
                Hỗ trợ: Android, iOS, Windows Phone, Windows PC 8.1 trở lên.<br/>
                <br/>
                <b>Bước 2: Đăng kí tài khoản Money Lover</b><br/>
                Sau khi cài đặt, mở ứng dụng và đăng kí một tài khoản với email của bạn. Việc đăng kí tài khoản sẽ giúp bạn đồng bộ dữ liệu và sử dụng bản PREMIUM trên nhiều thiết bị và nền tảng.<br/>
                <br/>
                <b>Bước 3: Kích hoạt tài khoản PREMIUM</b><br/>
                Truy cập website <b><a href="https://happy.moneylover.me" target="_blank" rel="nofollow">https://happy.moneylover.me</a></b> nhập email và mã kích hoạt (không phân biệt viết hoa và thường).<br/>
                <?php if($code): ?>
                    </p><p style="text-align: center;margin: 30px 0;border-radius: 5px;">
                        <span style="padding: 15px 30px; background: #CCFFC5; border-radius: 5px;"><span style="padding: 0;">
                            <b>Mã kích hoạt của bạn:</b> <b class="text-red"><?php echo $code->getCode(); ?></b>
                        </span></span>
                    </p>
                    <p style="text-align: center;">Hạn kích hoạt: <b><?php echo date('d-m-Y', $code->getExpiredAt()->getTimestamp()); ?></b></p>
                    <p>
                <?php endif; ?>
                Sau khi kích hoạt thành công, bạn mở ứng dụng và tab vào GO PREMIUM (Mua ứng dụng) để ứng dụng nhận diện trạng thái tài khoản của bạn.<br/>
                <br/>
                <b>Thông tin thêm</b><br/>
                1- Bạn có thể sử dụng PREMIUM trên nhiều thiết bị và nền tảng. Ứng dụng có bản quyền trọn đời, chỉ cần đăng nhập trên thiết bị khác là có thể sử dụng tất cả các tính năng và dữ liệu cũ của bạn.<br/>
                2- Ngoài tính năng nhập tay, Money Lover hỗ trợ liên kết trực tiếp đến các dịch vụ (PayPal, Uber, CGV) và các Ngân hàng (Vietcombank, Viettin Bank) và nhiều dịch vụ khác. Chức năng này đang được thử nghiệm trước khi ra mắt chính thức.<br/>
                3- Hỗ trợ từ Money Lover Team <a href="mailto:contact@moneylover.me" target="_blank" rel="nofollow">contact@moneylover.me</a>.
            </p>
        </div>
    </div>
</div>
<?php $view['slots']->stop(); ?>