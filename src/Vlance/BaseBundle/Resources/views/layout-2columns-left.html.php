<?php
$current_route = $app->getRequest()->attributes->get('_route');
?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php
$filterblockqc=$_SESSION['filteraccpage'];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route))); ?>
    <body class="two-columns-left <?php if($current_route === 'job_show_freelance_job'){echo 'job_show ' .$current_route ; }
              elseif($current_route === 'account_show_freelancer'){echo 'account_show ' .$current_route ; }
              else{echo $current_route;} ?>">
        <?php /* Header block */ ?>
        <?php
        echo $view['actions']->render(
                $view['router']->generate('vlance_base_block_header', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
        );
        ?>
        
        <?php /* check tài khoản login */ ?>
        <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?>
            <div class="admin-control form-actions form-tabs">
                <div class="container">
                    <?php echo $view['actions']->render(
                        $view['router']->generate(
                            'vlance_base_block_admin', 
                            array(
                                '_route_params' => serialize($app->getRequest()->attributes->get('_route_params')), 
                                'routeName' => $current_route
                            ), 
                            true), 
                        array('strategy' => 'esi')
                    ); ?>
                </div>
            </div>
        <?php endif; ?>
        
        <?php /* Messages block */ ?>
        <?php $params = $app->getRequest()->attributes->get('_route_params');?>
        <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_messages', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route))); ?>
        <div class="content-section">
            <?php $view['slots']->output('top_block_listing_page', '') ?>
            
            <div class="container link-to-thuengay">
                <div class="row-fluid">
                    <div class="span12">
                        <?php if($view['vlance']->autoHideBanner('21-09-2017 00:00:00', '29-09-2017 23:59:59') === true): ?>
                        <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150--banner-thuengay.jpg') ?>"/>
                        </a>
                        <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150--banner-thuengay.jpg') ?>"/>
                        </a>
                        <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                        </a>
                        <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250--banner-thuengay.jpg') ?>"/>
                        </a>
                        <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0px auto 0;width: 320px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150--banner-thuengay.jpg') ?>"/>
                        </a>
                    <?php else: ?>
                        <a class="screen-1200" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 20px auto 0;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/1200x150.jpg') ?>"/>
                        </a>
                        <a class="screen-960" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x150.jpg') ?>"/>
                        </a>
                        <a class="screen-750" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 20px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                        </a>
                        <a class="screen-620" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 0;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/960x250.jpg') ?>"/>
                        </a>
                        <a class="screen-320" href="https://www.thuengay.vn/thue-dich-vu?ref=vlance-setting-page-top" target="_blank" title="Thuengay.vn - Thuê thiết kế & marketing từ A đến Z" style="margin: 0 auto 0;width: 320px;">
                            <img src="<?php echo $view['assets']->getUrl('media/banner/thuengay/320x150.jpg') ?>"/>
                        </a>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            
            <div class="row-fluid container">
                <div class="col2-left span3">
                    <div class="layered-navigation">
                        <?php /* Left block */ ?>
                        <?php
                        echo $view['actions']->render(
                                $view['router']->generate('vlance_base_block_left', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
                        );
                        ?>
                        
                        <?php if( $filterblockqc >= 6  /*&& strlen($params['filters'])>0*/):?>
                        <div class="block ads-1">
                            <div class="control-group ">
                                <div class="layered-block ln-category">
                                    <div class="block-content subcategory_city">
                                        <?php /* <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                        <!-- vLance.app job list 160x600 -->
                                        <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px;background-color: #fff;margin: 0px 11px 10px;" data-ad-client="ca-pub-7001499363485748" data-ad-slot="1609643316"></ins>
                                        <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script> */ ?>
                                        <div style="display:inline-block;width:160px;height:600px;background-color: #fff;margin: 0px 11px 10px;">
                                            <a rel="nofollow" href="https://www.thuengay.vn/cong-cu/alo-ngay?ref=vlance_banner_left" target="_blank">
                                                <image src="../img/ad/banner-alo-ngay-160x600.jpg"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if( $filterblockqc >= 8 /*&& strlen($params['filters'])>0*/):?>
                        <div class="block ads-2">
                            <div class="control-group ">
                                <div class="layered-block ln-category">
                                    <div class="block-content subcategory_city">
                                        <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                        <!-- vLance.app job list 160x600 -->
                                        <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px;background-color: #fff;margin: 0px 11px 10px;" data-ad-client="ca-pub-7001499363485748" data-ad-slot="4563109716"></ins>
                                        <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <?php endif;?>
                        <?php if( $filterblockqc >= 10  /*&& strlen($params['filters'])>0*/):?>
                        <div class="block ads-3">
                            <div class="control-group ">
                                <div class="layered-block ln-category">
                                    <div class="block-content subcategory_city">
                                        <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                        <!-- vLance.app job list 160x600 -->
                                        <ins class="adsbygoogle" style="display:inline-block;width:160px;height:600px;background-color: #fff;margin: 0px 11px 10px;" data-ad-client="ca-pub-7001499363485748" data-ad-slot="6039842918"></ins>
                                        <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                        </script>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col2-right span9">

                    <?php /* Main content block */ ?>
                    <div id="main_content">
                        <?php $view['slots']->output('content') ?>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <?php /* Footer block */ ?>
            <?php
            echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
            );
            ?>
        </footer>
        <div class="row hidden">
            <?php /* Hidden block use for popup content */ ?>
            <?php
            echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_beforebodyend', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
            );
            ?>
        </div>
        
        <div class="row hidden">
            <?php echo $view->render('VlanceBaseBundle:Block:add_category_mini_vip.html.php', array()); ?>
        </div>
    </body>
</html>
