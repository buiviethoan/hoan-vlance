<?php
    $current_route = $app->getRequest()->attributes->get('_route');
?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" >
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route))); ?>
    <body class="two-columns-right <?php if($current_route === 'job_show_freelance_job'){echo 'job_show ' .$current_route ; }
              elseif($current_route === 'account_show_freelancer'){echo 'account_show ' .$current_route ; }
              else{echo $current_route;} ?>">
        <?php /* Header block */ ?>
        <?php
        echo $view['actions']->render(
                $view['router']->generate('vlance_base_block_header', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
        );
        ?>
        
        <?php /* check tài khoản login */ ?>
        <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?>
            <div class="admin-control form-actions form-tabs">
                <div class="container">
                    <?php echo $view['actions']->render(
                        $view['router']->generate(
                            'vlance_base_block_admin', 
                            array(
                                '_route_params' => serialize($app->getRequest()->attributes->get('_route_params')), 
                                'routeName' => $current_route
                            ), 
                            true), 
                        array('strategy' => 'esi')
                    ); ?>
                </div>
            </div>
        <?php endif; ?>
        
        <?php /* Messages block */?>
        <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_messages', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route))); ?>

        <div class="top_content">
            <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_featurefreelancer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true)); ?>
        </div>
        <div class="content-section container">
            <div class="row-fluid">
                    <div class="row-fluid">
                        <div class="col2-left span8">
                            
                            <?php /* Main content block */?>
                            <div id="main_content">
                                <?php $view['slots']->output('content') ?>
                            </div>
                        </div>
                        <?php /* Right block */ ?>
                        <div id="right_content" class="col2-right span4">
                            <?php /* Right block */ ?>
                            <?php
                            echo $view['actions']->render(
                                $view['router']->generate('vlance_base_block_right', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
                            );
                            ?>
                        </div>
                    </div>
            </div>
        </div>
        <footer>
            <?php /* Footer block */ ?>
            <?php
            echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
            );
            ?>
        </footer>
        <div class="row hidden">
            <?php /* Hidden block use for popup content */ ?>
            <?php
            echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_beforebodyend', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
            );
            ?>
        </div>
        
        <div class="row hidden">
            <?php echo $view->render('VlanceBaseBundle:Block:add_category_mini_vip.html.php', array()); ?>
        </div>
    </body>
</html>
