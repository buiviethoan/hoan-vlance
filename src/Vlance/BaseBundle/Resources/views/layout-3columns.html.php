<?php
    $current_route = $app->getRequest()->attributes->get('_route');
?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8"/>
        <meta name="robots" content="noindex,nofollow" />
        <title><?php $view['slots']->output('title', $view['translator']->trans('vLance.vn - Làm freelance, kiếm thêm thu nhập')) ?></title>
        
        <?php /*foreach ($view['assetic']->stylesheets(
                    array(
                        'bundles/vlancebase/css/*.css',
                    ),
                    array('cssrewrite')
                ) as $url): ?>
        <link href="<?php echo $view->escape($url) ?>" rel="stylesheet" type="text/css" media="all" />
        <?php endforeach;?>
        
        <?php foreach ($view['assetic']->javascripts(array(
            '@VlanceBaseBundle/Resources/public/js/jquery-1.10.1.js',
            '@VlanceBaseBundle/Resources/public/js/bootstrap.js',
            '@VlanceBaseBundle/Resources/public/js/hinclude.js',
            )) as $url): ?>
        <script type="text/javascript" src="<?php echo $view->escape($url) ?>"></script>
        <?php endforeach;*/?>
        <link href="<?php echo $view['assets']->getUrl('css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo $view['assets']->getUrl('css/vlance.css') ?>" rel="stylesheet" type="text/css" media="all" />
        <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery-1.10.1.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/hinclude.min.js') ?>"></script>
        <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('routeName' => $current_route)));?>
    </head>
    <body class="three-columns <?php if($current_route === 'job_show_freelance_job'){echo 'job_show ' .$current_route ; }
              elseif($current_route === 'account_show_freelancer'){echo 'account_show ' .$current_route ; }
              else{echo $current_route;} ?>">
        <?php /* Messages block */?>
        <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_messages', array('routeName' => $current_route)));?>

        <div class="container">
            <div class="row">
                <div class="span12">
                    <div id="header_content">
                        <?php /* Header block */?>
                        <hx:include src="<?php echo $view['router']->generate('vlance_base_block_header', array('routeName' => $current_route))?>"><?php echo $view['translator']->trans('Loading ....'); ?></hx:include>
                    </div>
                     <!--check tài khoản login-->
                    <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                        <?php
                        echo $view['actions']->render(
                                $view['router']->generate('vlance_base_block_admin', array('_route_params' => serialize($app->getRequest()->attributes->get('_route_params')), 'routeName' => $current_route), true), array('strategy' => 'esi')
                        );
                        ?>
                    <?php endif; ?>
                    <div class="row">
                        <?php /* Left block */ ?>
                        <div id="left_content" class="span3">
                            <hx:include src="<?php echo $view['router']->generate('vlance_base_block_left', array('routeName' => $current_route))?>"><?php echo $view['translator']->trans('Loading ....'); ?></hx:include>
                        </div>
                        <div class="span6 main-col">
                            
                            <?php /* Main content block */?>
                            <div id="main_content">
                                <?php $view['slots']->output('content') ?>
                            </div>
                        </div>
                        <?php /* Right block */ ?>
                        <div id="right_content" class="span3">
                            <hx:include src="<?php echo $view['router']->generate('vlance_base_block_right', array('routeName' => $current_route))?>"><?php echo $view['translator']->trans('Loading ....'); ?></hx:include>
                        </div>
                    </div>
                    <div id="footer_content">
                        <?php /* Footer block */?>
                        <hx:include src="<?php echo $view['router']->generate('vlance_base_block_footer', array('routeName' => $current_route))?>"><?php echo $view['translator']->trans('Loading ....'); ?></hx:include>
                    </div>
                </div>
            </div>
            <div class="hidden">
                <?php /* Hidden block use for popup content */?>
                <hx:include src="<?php echo $view['router']->generate('vlance_base_block_beforebodyend', array('routeName' => $current_route))?>"><?php echo $view['translator']->trans('Loading ....'); ?></hx:include>
            </div>
            
            <div class="hidden">
                <?php echo $view->render('VlanceBaseBundle:Block:add_category_mini_vip.html.php', array()); ?>
            </div>
        </div>
    </body>
</html>
