<!-- Modal -->
<div id="block_new_feature" class="modal in fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block; width: 499px; height: 520px;">
    <div>
        <p class="new_feature_success_title">Chúc mừng <?php echo $account->getFullName()?>!</p>
        <p class="new_feature_success_content">Hồ sơ của bạn đã được vlance <b style="color: red">xác thực</b></p>
        <img class="new_feature_success_icon" src="/img/popup/certificated_big.png"/>
        <p class="new_feature_success_content">Chúc bạn thành công!</p>
        <a href="<?php echo $view['router']->generate("cms_page", array('slug' => 'xac-thuc-ho-so')) ?>" style="position: absolute; bottom: 30px; right: 165px; text-decoration: underline;">Tìm hiểu thêm</a>
        <button id="new_feature_button" class="btn-flat btn-light-blue" style="position: absolute; bottom: 20px; right: 30px">Thật tuyệt vời</button>
    </div>
</div>
<div id="new_feature_background" class="modal-backdrop fade in" style="background-color: #d5d5d5;opacity: 0.9;"></div>
<script type="text/javascript">
    jQuery(function($) {
        var closeFeature = function() {
            $("#new_feature_background").hide();
            $("#block_new_feature").hide();
        }
        $("#new_feature_background").click(function(){
            closeFeature();
        });
        $("#new_feature_button").click(function() {
            closeFeature();
        })
    });
</script>