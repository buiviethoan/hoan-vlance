<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php use Vlance\PaymentBundle\Entity\OnePay; ?>
<?php use Vlance\AccountBundle\Entity\Account; ?>

<?php // Processing user id and cookie ?>
<?php $vl_uid = ''; ?>
<?php if(isset($_COOKIE['vluid'])): ?>
    <?php $vl_uid = base64_decode($_COOKIE['vluid']); ?>
<?php endif; ?>

<?php if(is_object($app->getSecurity()->getToken()) && ($current_user = $app->getSecurity()->getToken()->getUser())): ?>
    <?php if(is_object($current_user)): ?>
        <?php $vl_uid = $current_user->getId(); ?>
        <?php setcookie('vluid', base64_encode($vl_uid), time()+90*24*3600, "/"); ?>
    <?php endif;?>
<?php endif;?>
<?php $ga_user_id = ($vl_uid == '') ? "'vlance.vn'" : "{'userId': '" . $vl_uid . "'}" ; ?>
<?php // END Processing user id and cookie ?>
    
<?php $host = $app->getRequest()->getHost();?>
<?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
    <?php // PRODUCTION MODE ?>
    <?php //- Google analytics tracking code ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-34003187-3', <?php echo $ga_user_id; ?>);
        ga('send', 'pageview');
    </script>
    <?php //- End Google analytics tracking code ?>

    <?php /* //Google Tag Manager ?>
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WM92T4"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WM92T4');</script>
     <?php //End Google Tag Manager */ ?>

    <!-- Facebook Conversion Code for Visit (any page) -->
    <script>(function() {
      var _fbq = window._fbq || (window._fbq = []);
      if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
      }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6024217527885', {'value':'0.00','currency':'VND'}]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6024217527885&amp;cd[value]=0.00&amp;cd[currency]=VND&amp;noscript=1" /></noscript>
<?php else: ?>
    <?php // DEVELOPMENT MODE ?>
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-34003187-5', <?php echo $ga_user_id; ?>); // Localhost site ID
    ga('send', 'pageview');

    </script>
    <!-- End Google Analytics -->
<?php endif;?>

<?php // Notification on top page ?>
<?php /*  if ($app->getUser()) :?> */ ?>
<?php $promo_ml_start = new \DateTime("2016-02-22 00:00:00"); ?>
<?php $promo_ml_end   = new \DateTime("2016-02-29 00:00:00"); ?>
<?php $now            = new \DateTime("now"); ?>
<?php if($promo_ml_start < $now && $now < $promo_ml_end): ?>
<div class="alert alert-error toppage bottompage">
    <div class="container" style="text-align: center">
        Tặng ngay <div title="Tặng mobile app Money Lover" style="display:inline-block;background: url(/img/logo_footer_partners.png);background-repeat: no-repeat;height: 20px;background-position: 0px -131px;width: 80px;margin-bottom: -7px;background-size: 100%;"></div> phiên bản PREMIUM khi mua Credit. <a href="<?php echo $view['router']->generate('credit_balance',array()); ?>?ref=ml20162">Xem chi tiết</a>
        <?php // vLance is hiring freelance <img src="/media/magento-logo.png" style="margin-top: -5px;" /> developers, hourly rate: $10+/h. Signup <strong><a target="_blank" href="https://goo.gl/vMOS7e">here</a></strong>. ?>
    </div>
</div>
<?php endif; ?>
<?php /* endif; */ ?>

<div class="alert alert-warning toppage bottompage">
    <div class="container" style="text-align: center">
        Bạn làm <b>Copywriting</b> & muốn nhận 100-200K/bài viết mỗi ngày? Đăng ký dành cho <b><a rel="nofollow" href="https://goo.gl/eQn2yz">Freelancer / cá nhân</a></b> hoặc <b><a rel="nofollow" href="https://goo.gl/7YZSwi">Agency</a></b>. <em>(Hết hạn: 20/7/2016)</em>
    </div>
</div>
<?php // END Notification on top page ?>
    
<?php /** MENU BAR  **/ ?>
<?php // Not connected ?>
<?php if (!$app->getUser()) :?>
    <?php // Facebook js SDK, just after opening <body> tag ?>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '197321233758025',    // App ID from the app dashboard
                channelUrl : '/web/fbchannel.php', // Channel file for x-domain comms
                status     : true,                 // Check Facebook Login status
                xfbml      : true                  // Look for social plugins on the page
            });
            <?php // Additional initialization code such as adding Event Listeners goes here ?>
        };

      <?php // Load the SDK asynchronously?>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/all.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function fb_login() {
            window.location.href = "https://www.facebook.com/dialog/oauth?%20client_id=197321233758025&scope=email&redirect_uri=<?php echo /* chỗ này phải thêm domain vào */ $view['router']->generate('login_facebook', array(), true) ?>";
        }

        function fb_logout() {
            FB.logout(function(response) {
                console.log('Logout thành công');
            });
        }
    </script>
    <?php // End facebook js SDK ?>
    
    <?php // login by Linkedin ?>
    <script type="text/javascript">
        function linkedin_login() {
            window.location.href = "https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=<?php echo $view['vlance']->getParameter('vlance_system.linkedin.api_key') ?>&scope=r_basicprofile%20r_emailaddress&state=<?php echo $view['vlance']->getParameter('vlance_system.linkedin.state') ?>&redirect_uri=<?php echo $view['router']->generate('login_linkedin', array(), true) ?>";
        }
        function google_login() {
            window.location.href = "https://accounts.google.com/o/oauth2/auth?scope=email%20profile&state=%2Fprofile&redirect_uri=<?php echo /* Chỗ này phải thêm domain vào */ $view['router']->generate('login_google', array(), true) ?>&response_type=code&client_id=<?php echo $view['vlance']->getParameter('vlance_system.google.client_id') ?>&approval_prompt=force";
        }
    </script>
    <?php // End linkedin js SDK ?>
        
    <div class="navbar navbar-static-top">
        <div class="container">
            <div class="row-fluid">
                <div class="span3 emblem">
                    <<?php echo ($routeName == 'vlance_homepage')?"h1":"div"; ?> class="logo"><a href="<?php echo $view['router']->generate('vlance_homepage') ?>">
                        <img src="<?php echo $view['assets']->getUrl('img/logo.png') ?>" 
                             alt="vLance.vn - Thuê Freelancer Việt Nam nhanh và hiệu quả" 
                             title="<?php echo $view['translator']->trans('site.slogan', array(), 'vlance') ?>"/></a></<?php echo ($routeName == 'vlance_homepage')?"h1":"div"; ?>>
                </div>
                <div class="span6" id="topmenu">
                    <div class="menu-inner">
                        <ul class="nav no-space">
                            <li class="menu-item first <?php if($routeName == 'freelance_job_list'){ echo "active"; }  ?>">
                                <a class="" title="<?php echo $view['translator']->trans('site.menu.find_job', array(), 'vlance') ?>" href="<?php echo $view['router']->generate('freelance_job_list'); ?>"><?php echo $view['translator']->trans('site.menu.find_job', array(), 'vlance') ?></a>
                            </li>
                            <li class="menu-item second <?php if($routeName == 'freelancer_list'){ echo "active"; }  ?>">
                                <a class="" title="<?php echo $view['translator']->trans('site.menu.find_freelancer', array(), 'vlance') ?>" href="<?php echo $view['router']->generate('freelancer_list'); ?>"><?php echo $view['translator']->trans('site.menu.find_freelancer', array(), 'vlance') ?></a>
                            </li>
                            <li class="menu-item last">
                                <a class="tro-giup" title="<?php echo $view['translator']->trans('site.menu.help', array(), 'vlance') ?>" href="/tro-giup"><?php echo $view['translator']->trans('site.menu.help', array(), 'vlance') ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="span3 btn-connect">
                    <div class="login-vlance">
                        <div class="network-socail">
                            <a rel="nofollow" class="btn-facebook" title="Login Facebook" onclick="fb_login()"><img src="/img/icon-facebook.png">Facebook</a>
                            <a rel="nofollow" class="btn-google-plus" title="Login Google" onclick="google_login()"><img src="/img/icon-google+.png">Google</a>                        </div>
                        <div class="create-account-vlance">
                            <a href="<?php echo $view['router']->generate('account_register') ?>">
                                   <?php echo $view['translator']->trans('site.menu.register', array(), 'vlance') ?> /
                            </a>
                        </div>
                        <p><a rel="nofollow" href="<?php echo $view['router']->generate('fos_user_security_login')?>"><?php echo $view['translator']->trans('site.menu.login', array(), 'vlance') ?></a></p>    
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php // Logged in user ?>
<?php else:?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=197321233758025";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
    <div class="navbar navbar-static-top navbar-mini">
        <div class="upper-section row-fluid">
            <div class="container">
                <div class="logo span2">
                    <a href="<?php echo $view['router']->generate('vlance_homepage') ?>">
                        <img src="<?php echo $view['assets']->getUrl('img/logo_small.png') ?>" width="89" height="52" 
                             alt="<?php echo $view['translator']->trans('site.slogan', array(), 'vlance') ?>"/></a>
                </div>
                <div class="span10 span10-b">
                    <div class="pull-right nav-account">
                        <div class="inbox-menu pull-right">
                            <?php echo $view['actions']->render($view['router']->generate('new_message')) ?>
                        </div>
                        <div class="account-menu dropdown pull-right">
                            <div class="avata">             
                                <?php $resize = ResizeImage::resize_image($app->getUser()->getFullPath(), '48x48', 48, 48, 1); ?>
                                <a href="<?php echo $view['router']->generate($current_user->isTypeClient()?"account_show_client":"account_show_freelancer",array('hash' => $current_user->getHash())); ?>">
                                    <img width="48" height="48" src="<?php echo $resize ? $resize : $view['assets']->getUrl('img/unknown.png'); ?>" alt="<?php print_r($app->getUser()->getFullName()) ?>" title="<?php print_r($app->getUser()->getFullName()) ?>" />
                                </a>
                            </div>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php print_r($app->getUser()->getFullName()) ?> <b class="caret"></b></a>
                            <div class="credit-balance"><a href="<?php echo $view['router']->generate('credit_balance') ?>"><span data-credit="balance"><?php echo $current_user->getCredit()->getBalance(); ?></span> CREDIT</a></div>
                            <div id="popover-in"></div>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo $view['router']->generate($current_user->isTypeClient()?"account_show_client":"account_show_freelancer",array('hash' => $current_user->getHash())); ?>"><?php echo $view['translator']->trans('site.menu.account', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate("account_basic_edit",array('id' => $current_user->getId())); ?>"><?php echo $view['translator']->trans('site.menu.edit_account', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('account_verify_information'); ?>?dropdown"><?php echo $view['translator']->trans('site.menu.verify_info', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('credit_balance'); ?>?dropdown"><?php echo $view['translator']->trans('site.menu.manage_credit', array(), 'vlance') ?></a></li>
                                
                                <li class="divider"></li>
                                <li><a href="<?php echo $view['router']->generate('config_user', array()); ?>"><?php echo $view['translator']->trans('site.menu.options', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('email_invite'); ?>?dropdown"><?php echo $view['translator']->trans('site.menu.email_invite', array(), 'vlance') ?> <span class="label-tag">Mới</span></a></li>
                                
                                <li class="divider"></li>
                                <li><a href="<?php echo $view['router']->generate('fos_user_security_logout') ?>"><?php echo $view['translator']->trans('site.menu.logout', array(), 'vlance') ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-section row-fluid menu_scoll">
            <div class="container">
                <ul class="nav-menu span12">
                    <li class="tf200 first <?php if( $routeName  == 'jobs_bid_project' || $routeName == 'jobs_create_project'){ echo "active"; }  ?>">
                        <?php if($current_user->getType() == Vlance\AccountBundle\Entity\Account::TYPE_FREELANCER) : ?>
                            <a href="<?php echo $view['router']->generate('jobs_bid_project'); ?>"><i class="fa fa-home"></i> <?php echo $view['translator']->trans('site.menu.workspace', array(), 'vlance') ?></a>
                        <?php else : ?>
                            <a href="<?php echo $view['router']->generate('jobs_create_project'); ?>"><i class="fa fa-home"></i> <?php echo $view['translator']->trans('site.menu.workspace', array(), 'vlance') ?></a>
                        <?php endif; ?>
                    </li>
                    <li class="tf200 <?php if($routeName == 'job_new'){ echo "active";} ?>">
                        <a href="<?php echo $view['router']->generate('job_new'); ?>" onclick="vtrack('Click post job', {'location':'Menu'})"><?php echo $view['translator']->trans('site.menu.new_job', array(), 'vlance') ?></a>
                    </li>
                    <li class="tf200 nav-contest-new <?php if($routeName == 'job_contest_new'){ echo "active";} ?>">
                        <a href="<?php echo $view['router']->generate('job_contest_new'); ?>" onclick="vtrack('Click post contest', {'location':'Menu'})"><?php echo $view['translator']->trans('site.menu.new_contest', array(), 'vlance') ?><span class="label-new">Mới</span></a>
                    </li>
                    <li class="tf200 <?php if($routeName == 'freelance_job_list'){ echo "active";} ?>">
                        <a href="<?php echo $view['router']->generate('freelance_job_list'); ?>"><?php echo $view['translator']->trans('site.menu.find_job', array(), 'vlance') ?></a>
                    </li>
                    <li class="tf200 <?php if($routeName == 'freelancer_list'){ echo "active";} ?>">
                        <a href="<?php echo $view['router']->generate('freelancer_list'); ?>"><?php echo $view['translator']->trans('site.menu.find_freelancer', array(), 'vlance') ?></a>
                    </li>
                    <?php if($current_user->getTelephone() && !($current_user->getTelephoneVerifiedAt())): ?>
                    <li class="tf200">
                        <a class="tf200 last verify-tel-menu" href="<?php echo $view['router']->generate('account_verify_information'); ?>"
                                onclick="vtrack('Click verify telephone', {'location':'menu'})">
                            <i class="icon-ok-circle icon-white"></i> <?php echo $view['translator']->trans('profile.edit_verify_information.verify_telephone.cta', array(), 'vlance') ?>
                        </a>
                    </li>
                    <?php else: ?>
                    <li class="tf200 last <?php if($routeName == 'email_invite'){ echo "active";} ?>">
                        <a href="<?php echo $view['router']->generate('email_invite'); ?>?menu"
                           onclick="vtrack('Click invite', {'location':'<?php echo $app->getRequest()->get('routeName')?>'})"><?php echo $view['translator']->trans('site.menu.email_invite', array(), 'vlance') ?><span class="label-new">Mới</span></a>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('.i32-mail').click(function(e) {
                $('.dropdown-menu').addClass('menuhiden');
                $('.block-new-message').toggle();
                e.stopPropagation();
            });
            $('body').click(function() {
                $('.dropdown-menu').removeClass('menuhiden');
                $('.block-new-message').hide();
            });
            $('.block-new-message').click(function(e){
                e.stopPropagation();
            });
        });

        $(function(){
            $(window).scroll(function(){
              var aTop = $('.navbar .upper-section.row-fluid').height();
              if($(this).scrollTop()>=aTop){
                  $('.menu_scoll').addClass('menu_scoll_display');
                  $('.pull-right.nav-account').addClass('account-mail-menu');
                  $('account-mail-menu').show();
              };
              if($(this).scrollTop() <= (aTop)){
                  $('.menu_scoll').removeClass('menu_scoll_display');
                  $('.menu_scoll').addClass('menu_scoll');
                  $('.pull-right.nav-account').removeClass('account-mail-menu');
                  $('account-mail-menu').hide();
              }
            });
          });
    </script>
    
    <?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
    <?php if(HasPermission::hasAdminPermission($acc) == TRUE): ?> 
    <div class="has_permission"><div class="container"><?php echo $view['translator']->trans('site.has_permission', array(), 'vlance') ?></div></div>
    <div class="admin-control form-actions">
        <div class="container">
            <?php $view['slots']->output('admin_menu', '') ?>
        </div>
    </div>
    <?php endif; ?>    
<?php endif;?>
<?php /** END MENU BAR  **/ ?>
