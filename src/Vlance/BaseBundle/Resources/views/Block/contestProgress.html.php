<div class="progress-job-wrapper">
    <div class="progress-job">
        <ul>
            <li class="first <?php echo $is_open?>"><div class="number"><div class="first">1</div></div><div class="tally"><?php echo $view['translator']->trans('view_contest_page.progress.step1', array(), 'vlance') ?></div><i></i></li>
            <li class="<?php echo $is_choose?>"><div class="number"><div class="<?php echo $is_choose?>">2</div></div><div class="tally"><?php echo $view['translator']->trans('view_contest_page.progress.step2', array(), 'vlance') ?></div><i></i></li>
            <li class="<?php echo $is_qualified?>"><div class="number"><div class="<?php echo $is_qualified?>">3</div></div><div class="tally"><?php echo $view['translator']->trans('view_contest_page.progress.step3', array(), 'vlance') ?></div><i></i></li>
            <li class="last <?php echo $is_awarded?>"><div class="number"><div class="<?php echo $is_awarded?>">4</div></div><div class="tally"><?php echo $view['translator']->trans('view_contest_page.progress.step4', array(), 'vlance') ?></div><i class="<?php echo $is_finished?>"></i></li>
        </ul>
    </div>
</div>
