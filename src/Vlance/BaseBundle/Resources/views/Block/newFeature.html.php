<!-- Modal -->
<img src="/img/popup/help_xacthuc.png" slide="4" style="position: absolute; top: 185px; right: 250px;z-index: 10000; display: none"/>
<div id="block_new_feature" class="modal in fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block; width: 499px;">
    <div>
        <img src="/img/popup/new_feature_1.png" slide="1"/>
        <img src="/img/popup/new_feature_2.png" slide="2"/>
        <img src="/img/popup/new_feature_3.png" slide="3"/>
        <a href="<?php echo $view['router']->generate("cms_page", array('slug' => 'xac-thuc-ho-so')) ?>" style="position: absolute; bottom: 30px; right: 165px; text-decoration: underline; display: none">Tìm hiểu thêm</a>
        <button id="new_feature_button" class="btn-flat btn-light-blue" style="position: absolute; bottom: 20px; right: 30px">Xem tiếp</button>
    </div>
</div>
<div id="new_feature_background" class="modal-backdrop fade in" style="background-color: #d5d5d5;opacity: 0.9;"></div>
<script type="text/javascript">
    jQuery(function($) {
        var state = 1;
        var showSlide = function() {
            $("#block_new_feature img").hide();
            $("#block_new_feature img[slide=" + state + "]").show();
        }
        var closeFeature = function() {
            $("#new_feature_background").hide();
            $("#block_new_feature").hide();
            $("img[slide=4]").hide();
        }
//        $("#new_feature_background").click(function(){
//            closeFeature();
//        });
        $("#new_feature_button").click(function() {
            if (state == 3) {
                closeFeature();
                return false;
            }
            state++;
            if (state == 3) {
                $("#new_feature_button").html('Đã hiểu, đóng');
                $("#block_new_feature a").show();
                $("img[slide=4]").show();
            }
            showSlide();
        })
        showSlide();
    });
</script>