<?php // if(count($app->getRequest()->getSession()->getFlashBag()->all())>0): ?>
    <div id="messages">
        <div class="body-messages">
            <?php foreach ($app->getRequest()->getSession()->getFlashBag()->all() as $type => $messages) {?>
                    <?php foreach ($messages as $message) { ?>
                        <!--<div class="">-->
                            <div class="flash-<?php echo $type?> alert alert-<?php echo $type?>">  
                                    <a class="close" data-dismiss="alert">×</a>  
                                    <div><?php echo $message?></div>

                            </div>
                        <!--</div>-->
                    <?php } ?>
            <?php } ?>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            setTimeout( "$('#messages').hide();", 7000);
        });
    </script>
<?php // endif;?>