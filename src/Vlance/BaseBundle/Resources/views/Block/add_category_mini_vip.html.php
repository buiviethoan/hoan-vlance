<?php use Vlance\AccountBundle\Entity\Account; ?>

<?php if(is_object($app->getSecurity()->getToken())): ?>
    <?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
    <?php $now = new \DateTime('now'); ?>
    <?php if(is_object($acc)): ?>
        <?php if($acc->getTypeVIP() === Account::TYPE_VIP_MINI): ?>
            <?php if(\Vlance\BaseBundle\Utils\JobCalculator::ago($acc->getExpiredDateVip(), $now) !== false): ?>
                <?php if(is_null($acc->getCategoryVIP())): ?>
                    <div class="mini-vip-backdrop container"></div>
                    <div id="mini-vip-category" class="container">
                        <div class="content">
                            <div class="text-message">Bạn đã mua gói Mini VIP. <br/> Hãy chọn lĩnh vực:</div>
                            <div>
                                <select id="category-vip" class="input-category">
                                    <?php foreach($view['vlance_account']->getCategoryAdminList() as $cat): ?>
                                        <option value="<?php echo $cat->getId(); ?>"><?php echo $cat->getTitle(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div style="margin-top: 10px;"><button id="btn-category-vip" class="btn btn-large btn-primary" style="padding-left: 40px; padding-right: 40px;"> Cập nhật</button></div>
                        </div>
                    </div>
                    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/form/vip/vip.min.js') ?>"></script>
                    <script type="text/javascript">
                        var cat_url = "<?php echo $view['router']->generate("update_category_vip", array()); ?>";
                    </script>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
