<?php foreach ($languages as $lang): ?>
    <?php if($languages[$active]['title'] == $lang['title']): ?>
        <span><?php echo $lang['title']; ?></span>
    <?php else:?>
        <a href="<?php echo $lang['url']; ?>"><?php echo $lang['title']; ?></a>
    <?php endif; ?>
<?php endforeach; ?>
