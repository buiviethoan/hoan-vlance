<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php use Vlance\AccountBundle\Entity\AccountVIP; ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<?php $view['slots']->start('content'); ?>
<div class="container" style="margin-top: 10px;">
    <div class="accordion" id="accordion2">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    Nâng cấp VIP
                </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <div class="row-fluid" style="text-align: center; margin-bottom: 10px;">
                        <div class="span3">ID/Email Freelancer</div>
                        <div class="span3">Gói VIP</div>
                        <div class="span2" style="margin-left: 40px;">Hình thức thanh toán</div>
                        <div class="span2">Thời hạn VIP</div>
                    </div>
                    <div id="admin-upgrade-vip" class="container row-fluid" style="text-align: center;">
                        <div class="span3">
                            <input type="text" id="id-account" name="id-account" placeholder="ID/Email">
                        </div>
                        <div class="span3">
                            <div class="row-fluid">
                                <select id="package-vip" style="height: 42px;" disabled>
                                    <option value="1" selected>VIP3 (360k-$17.90)</option>
                                    <option value="2">VIP6 (600k-$27.90)</option>
                                    <option value="3">VIP12 (960k-$42.90)</option>
                                    <option value="4">MiniVIP3 (180k-$8.95)</option>
                                    <option value="5">MiniVIP6 (300k-$13.95)</option>
                                    <option value="6">MiniVIP12 (480k-$21.45)</option>
                                </select>
                            </div>
                            <div class="row-fluid category-input" style="display: none;">
                                <p style="text-align: left; margin-left: 30px; margin-top: 10px;">Chọn Lĩnh vực</p>
                                <select id="category-vip" style="height: 42px;">
                                    <?php foreach($view['vlance_account']->getCategoryAdminList() as $cat): ?>
                                        <option value="<?php echo $cat->getId(); ?>"><?php echo $cat->getTitle(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="payment-methods span2">
                            <select id="payment-method" style="height: 42px; width: initial;" disabled>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_BANK_TRANSFER; ?>" selected>Chuyển khoản</option>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_PAYPAL; ?>" >Paypal</option>
                                <option value="<?php echo AccountVIP::PAYMENT_METHOD_OTHER; ?>" >Khác</option>
                            </select>
                        </div>
                        <div class="expired-vip-update span2" style="padding: 10px;">Thời hạn VIP trước</div>
                        <div class="button-submit span2">
                            <button id="btn-vip-submit" class="btn btn-large" disabled>Nâng cấp</button>
                            <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion" id="accordion3">
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseTwo">
                    Hủy VIP
                </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid" style="text-align: center; margin-bottom: 10px;">
                        <div class="span3">ID/Email Freelancer</div>
                        <div class="span2" style="margin-left: 35px;">Gói VIP</div>
                        <div class="span2" style="margin-left: 55px;">Lĩnh vực</div>
                        <div class="span2" style="margin-left: 110px;">Thời hạn VIP</div>
                    </div>
                    <div id="admin-upgrade-vip-cancel" class="container row-fluid" style="text-align: center;">
                        <div class="span3">
                            <input type="text" id="id-account-cancel" name="id-account" placeholder="ID/Email">
                        </div>
                        <div class="type-vip span2" style="padding: 10px;">Gói VIP</div>
                        <div class="category-mini-vip span3" style="padding: 10px;">Lĩnh vực MINI VIP</div>
                        <div class="expired-vip span2" style="padding: 10px;">Thời hạn VIP</div>
                        <div class="button-cancel-submit span2">
                            <button id="btn-vip-cancel-submit" class="btn btn-large" style="padding-left: 35px; padding-right: 35px;" disabled>Hủy</button>
                            <div class="load-spinner"><img src="<?php echo $view['assets']->getUrl('img/loading.gif') ?>"/></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#id-account').on('keypress input', function(event){
            $('#admin-upgrade-vip label.error').remove();
            if($('#id-account').val() == ""){
                document.getElementById('package-vip').disabled = true;
                document.getElementById('btn-vip-submit').disabled = true;
                document.getElementById('payment-method').disabled = true;
                $('.expired-vip-update').html('<span>Thời hạn VIP trước</span>');
            }

            if(event.keyCode === 13){
                searchAccount();
            }
        });

        function searchAccount(){
            $('#admin-upgrade-vip label.error').remove();
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_account", array()); ?>",
                type: "POST",
                data: {
                    account: $('#id-account').val(),
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label class="error" style="color: red;">ID/Email không đúng</label>').insertAfter('#id-account');
                    } else {
                        document.getElementById('package-vip').disabled = false;
                        document.getElementById('btn-vip-submit').disabled = false;
                        document.getElementById('payment-method').disabled = false;

                        if(typeof result.type !== 'undefined' && typeof result.color !== 'undefined'){
                            $('.expired-vip-update').html('<span style="color:' + result.color + '">' + result.time + '</span>');
                        }
                    }
                }
            });
        }

//        $('#package-vip').on('change', function(){
//            if(parseInt($('#package-vip').val()) > 3){
//                $('.category-input').slideDown();
//            } else {
//                $('.category-input').slideUp();
//            }
//        });

        $('#btn-vip-submit').on('click', function(){
            $('.button-submit').addClass('loading');
            $('#btn-vip-submit').css('display', 'none');
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_upgrade_code", array()); ?>",
                type: "POST",
                data: {
                    account: $('#id-account').val(),
                    package: $('#package-vip').val(),
                    method: $('#payment-method').val(),
                    category: $('#category-vip').val(),
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label style="color: red;">Đã có lỗi xảy ra</label>').insertAfter('#id-account');
                        $('.button-submit.loading').removeClass('loading');
                        $('#btn-vip-submit').css('display', 'block');
                    } else {
                        $('.button-submit.loading').removeClass('loading');
                        $('#btn-vip-submit').css('display', 'block');
                        $('.expired-vip-update').html('<b>' + result.time + '</b>');
                    }
                }
            });
        });

//        ----------------------------------------------------------------------
//        Cancel VIP
        $('#id-account-cancel').on('keypress input', function(event){
            $('#id-account-cancel label.error').remove();
            if($('#id-account-cancel').val() === ""){
                document.getElementById('btn-vip-cancel-submit').disabled = true;
                $('.type-vip').html('<span>Gói VIP</span>');
                $('.category-mini-vip').html('<span>Lĩnh vực MINI VIP</span>');
                $('.expired-vip').html('<span>Thời hạn VIP</span>');
            }

            if(event.keyCode === 13){
                searchAccountCancel();
            }
        });

        function searchAccountCancel(){
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_account", array()); ?>",
                type: "POST",
                data: {
                    account: $('#id-account-cancel').val(),
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label style="color: red;">ID/Email không đúng</label>').insertAfter('#id-account-cancel');
                    } else {
                        document.getElementById('btn-vip-cancel-submit').disabled = false;

                        if(typeof result.type !== 'undefined'){
                            if(result.type === 0){
                                $('.type-vip').html('<span>MINI VIP</span>');
                            } else if(result.type === 1){
                                $('.type-vip').html('<span>VIP</span>');
                            } else {
                                $('.type-vip').html('<span>' + result.type + '</span>');
                            }
                        }

                        if(typeof result.category !== 'undefined'){
                            $('.category-mini-vip').html('<span>' + result.category + '</span>');
                        }

                        if(typeof result.type !== 'undefined' && typeof result.color !== 'undefined'){
                            $('.expired-vip').html('<span style="color:' + result.color + '">' + result.time + '</span>');
                        }
                    }
                }
            });
        }
        
        $('#btn-vip-cancel-submit').on('click', function(){
            $('.button-cancel-submit').addClass('loading');
            $('#btn-vip-cancel-submit').css('display', 'none');
            $.ajax({
                url: "<?php echo $view['router']->generate("submit_admin_upgrade_cancel", array()); ?>",
                type: "POST",
                data: {
                    account: $('#id-account-cancel').val()
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error !== 0){
                        $('<label style="color: red;">Đã có lỗi xảy ra</label>').insertAfter('#id-account-cancel');
                        $('.button-cancel-submit.loading').removeClass('loading');
                        $('#btn-vip-cancel-submit').css('display', 'block');
                    } else {
                        $('.button-cancel-submit.loading').removeClass('loading');
                        $('#btn-vip-cancel-submit').css('display', 'block');
                        if(typeof result.type !== 'undefined'){
                            $('.type-vip').html('<span style="color:' + result.color + '">' + result.type + '</span>');
                        }

                        if(typeof result.category !== 'undefined'){
                            $('.category-mini-vip').html('<span style="color:' + result.color + '">' + result.category + '</span>');
                        }

                        if(typeof result.type !== 'undefined' && typeof result.color !== 'undefined'){
                            $('.expired-vip').html('<span style="color:' + result.color + '">' + result.time + '</span>');
                        }
                    }
                }
            });
        });
    });
</script>
<?php $view['slots']->stop(); ?>