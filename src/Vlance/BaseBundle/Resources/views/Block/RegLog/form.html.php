<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<script type="text/javascript">
    var logged = '<?php if(is_object($acc)){echo "true";} else {echo "false";}?>';
</script>

<div class="block-lr" style="margin-bottom: 20px;">
    <div class="row-fluid">
        <div class="span2">
            <div class="sprites-job-posting sprites-6"></div>
        </div> 
        <div class="span10">
            <h4>Thông tin người đăng dịch vụ</h4>
            <div class="row-fluid block-type-user">
                <div class="span6">
                    <label class="radio checked"><input id="new_user" type="radio" name="typeofuser" value="new" checked> Tôi là người mới</label>
                </div>
                <div class="span6 right-check-account">
                    <label class="radio"><input id="old_user" type="radio" name="typeofuser" value="old"> Tôi đã có tài khoản</label>
                </div>
            </div> 
        </div>    
    </div>
    <div class="row-fluid">
        <div class="block-form-lr span10 offset2">
            <div class="block-form-reg">
                <form id="reg-form" method="post">
                    <div class="row-fluid field">
                        <div class="span6">
                            <label for="reg_name">Họ và tên</label>
                            <input required id="reg_name" type="text" name="reg_name" placeholder="Ví dụ: Trần Văn A">
                        </div>
                        <div class="span6 input-email">
                            <label for="reg_email">Email</label>
                            <input required id="reg_email" type="email" name="reg_email" placeholder="Ví dụ: name@domain.com">
                        </div>
                    </div>
                    <div class="row-fluid field">
                    <div class="span6">
                            <label for="reg_pass">Mật khẩu</label>
                            <input required id="reg_pass" type="password" name="reg_pass" placeholder="********">
                        </div>
                        <div class="span6 re-pass">
                            <label for="reg_re_pass">Nhập lại mật khẩu</label>
                            <input required id="reg_re_pass" type="password" name="reg_re_pass" placeholder="********">
                        </div>
                    </div>
                </form>
            </div>
            <div class="block-form-log deactive">
                <form id="log-form" method="post">
                    <div class="row-fluid field">
                        <div class="span6 block-username-log">
                            <label for="login_email">Email</label>
                            <input required type="text" id="login_email" name="login_email" placeholder="Email">
                        </div>
                        <div class="span6 block-password-log">
                            <label for="login_pass">Mật khẩu</label>
                            <input required type="password" id="login_pass" name="login_pass" placeholder="Mật khẩu đăng nhập">
                        </div>
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                var reg_form_in_sp = '#reg-form';
                var reg_form_in_sp_params = {"url":"<?php echo $view['router']->generate('register_job') ?>"};
                var login_form_in_sp = '#log-form';
                var login_form_in_sp_params = {"url":"<?php echo $view['router']->generate('login_job') ?>"};
            </script>
            <div class="row-fluid">
                <p class="login-socail-network-label">Hoặc dùng luôn tài khoản sau</p>
                <div class="login-socail-network">
                    <a class="btn-facebook btn-social-fb-post-job" onclick="fb_login()"><span><img src="/img/icon-facebook.png">Facebook</span></a>
                    <a class="btn-google-plus btn-social-nfb-post-job" onclick="google_login()"><span><img src="/img/icon-google+.png">Google</span></a>
                    <a class="btn-linkedin btn-social-nfb-post-job" onclick="linkedin_login()"><span><img src="/img/icon-linkedin.png">LinkedIn</span></a>
                </div>
            </div>
        </div>    
    </div>
</div>

