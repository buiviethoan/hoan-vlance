<?php if (!empty($blocks)) : ?>
    <?php foreach ($blocks as $block) : ?>
        <div class="block">
        <?php
            echo $view['actions']->render(
                        $view['router']->generate($block, array('routeName' => $routeName), true),
                        array('strategy' => 'esi')
                ); 
        ?>
        </div>
    <?php endforeach;?>
<?php endif;?>