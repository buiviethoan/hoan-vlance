<?php if (!empty($blocks)) : ?>
    <?php foreach ($blocks as $block) : ?>
        <div class="block block-beforeend">
            <?php
            echo $view['actions']->render(
                    $view['router']->generate($block, array('routeName' => $routeName)), array('strategy' => 'esi')
            );
            ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php // Print userreport code in production environment only.  ?>
<?php $host = $app->getRequest()->getHost(); ?>
<?php //if ($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
    <!--Start of Zopim Live Chat Script-->
<!--    <script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
    $.src='//v2.zopim.com/?2GsDXQdZjhBdNrYyN2LIU609ENqIS9mC';z.t=+new Date;$.
    type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
    </script>-->
    <!--End of Zopim Live Chat Script-->
    
    <!--Start of Tawk.to Script-->
    <?php if($routeName !== 'job_show_freelance_job' && $routeName !== 'job_onsite_view' && $routeName !== 'job_contest_view' && $routeName !== 'account_show_freelancer'): ?>
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/593e4436282a395fdf74c430/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
    <?php endif; ?>
    <!--End of Tawk.to Script-->
    
    <?php // Userreport tracking ?>
    <script type="text/javascript">
        var _urq = _urq || [];
        _urq.push(['setGACode', 'UA-34003187-3']);
        _urq.push(['initSite', '454cf2ce-9366-48df-8efc-13f19b9f42e1']);

        (function() {
            var ur = document.createElement('script');
            ur.type = 'text/javascript';
            ur.async = true;
            ur.src = ('https:' == document.location.protocol ? 'https://cdn.userreport.com/userreport.js' : 'http://cdn.userreport.com/userreport.js');
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ur, s);
        })();
    </script>
    
<?php //endif; ?>
    
<?php // JS/HTML event tracking ?>
<script type="text/javascript">
    <?php $view['slots']->output('JSBeforeBodyEnd') ?>
</script>
<?php foreach ($app->getRequest()->getSession()->getJSBag()->all() as $type => $jsbag):?>
    <?php foreach ($jsbag as $js) :?>
        <?php echo $js;?>
    <?php endforeach;?>
<?php endforeach;?>
<?php // END JS/HTML event tracking ?>