<?php /*$acc_login = $app->getSecurity()->getToken()->getUser();?>
<?php if(is_object($acc_login)): */ ?>
<div class="progress-job-wrapper">
    <div class="progress-job">
        <ul>
            <li class="first <?php echo $is_open?>"><div class="number"><div class="first">1</div></div><div class="tally"><?php echo $view['translator']->trans('view_job_page.progress.step1', array(), 'vlance') ?></div><i></i></li>
            <li class="<?php echo $is_awarded?>"><div class="number"><div class="<?php echo $is_awarded?>">2</div></div><div class="tally"><?php echo $view['translator']->trans('view_job_page.progress.step2', array(), 'vlance') ?></div><i></i></li>
            <li class="<?php echo $is_funded?>"><div class="number"><div class="<?php echo $is_funded?>">3</div></div><div class="tally"><?php echo $view['translator']->trans('view_job_page.progress.step3', array(), 'vlance') ?></div><i></i></li>
            <li class="last <?php echo $is_done?>"><div class="number"><div class="<?php echo $is_done?>">4</div></div><div class="tally"><?php echo $view['translator']->trans('view_job_page.progress.step4', array(), 'vlance') ?></div><i class="<?php echo $is_reviewed?>"></i></li>
        </ul>
    </div>
</div>
<?php /* endif; */?>
