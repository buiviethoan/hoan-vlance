<?php if($profile):?>
    <?php if(is_null($profile->getFeaturedUntil()) || $profile->getFeaturedUntil() < new \DateTime()):?>
        <div class="block-feature-freelancer container">
            <div class="feature-freelancer guider-block" data-display-track="show-feature-freelancer-block">
                <div class="row-fluid">
                    <div class="content inner span6">
                        <div class="container-image">
                            <img src="/img/credit/feature-freelancer.jpg" title="Freelancer nổi bật" alt="Freelancer nổi bật">
                        </div>    
                    </div>
                    <div class="content detail span6">
                        <h4><?php echo $view['translator']->trans('list_freelancer.right_content.heading_guider_block', array(), 'vlance') ?></h4>
                        <p><i class="fa fa-check"></i></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_one_description', array(), 'vlance') ?></p>
                        <p><i class="fa fa-check"></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_two_description', array(), 'vlance') ?></span></p>
                        <p><i class="fa fa-check"></i><?php echo $view['translator']->trans('list_freelancer.right_content.guider_block_three_description', array(), 'vlance') ?></p>
                        <p class="cta-button">
                            <a class="btn btn-large btn-primary hidejs" style="display:none" data-toggle="modal" href="#pay-feature-freelancer"
                                    data-request='{"fid":"<?php echo is_object($profile) ? $profile->getId() : -1;?>", "pid":<?php echo is_object($profile) ? $profile->getId() : -1;?>}'
                                    onclick="vtrack('Click - Feature freelancer', {
                                                'location': 'profile page',
                                                'authenticated':'<?php echo is_object($profile)?'true':'false';?>', 
                                                'category':'<?php echo $profile->getCategory()?$profile->getCategory()->getTitle():'';?>', 
                                                'freelancer':'<?php echo $profile->getFullname();?>', 
                                                'freelancer_id':'<?php echo $profile->getId();?>'})"><?php echo $view['translator']->trans('list_freelancer.right_content.featured', array(), 'vlance') ?></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>    
        <?php echo $view->render('VlancePaymentBundle:Credit:popup_pay_featurefreelancer.html.php', array('acc' => $profile, 'form_name' => 'pay-feature-freelancer')); ?>
    <?php endif; ?>
<?php endif; ?>