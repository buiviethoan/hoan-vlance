<?php $current_route = $app->getRequest()->get('routeName'); ?>
<head>
    <?php $host = $app->getRequest()->getHost();?>
    <?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
    
        <!-- Facebook Pixel Code -->
        <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '171997586474021');
        fbq('track', "PageView");</script>
        <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=171997586474021&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->

    <?php endif; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="vi">
    
    <?php // Set index & follow for following pages only
    if($current_route == 'vlance_homepage'
            || $current_route == 'fos_user_security_login'
            || $current_route == 'account_register'
            || $current_route == 'freelancer_list'
            || $current_route == 'freelance_job_list'
            || $current_route == 'contest_list'
            || $current_route == 'account_show_freelancer'
            || $current_route == 'account_show_client'
            || $current_route == 'job_show_freelance_job'
            || $current_route == 'job_show_onsite'
            || $current_route == 'cms_page'
            || $current_route == 'cms_partners'
            || $current_route == 'category_show'
            || $current_route == 'job_new'
            || $current_route == 'job_contest_new'
            || $current_route == 'community_post'
            || $current_route == 'community_post_new'
            || $current_route == 'community_post_show'
            || $current_route == 'huong_dan_thue_freelancer'
            || $current_route == 'huong_dan_dang_viec'
            || $current_route == 'huong_dan_chon_freelancer'
            || $current_route == 'huong_dan_nap_tien'
            || $current_route == 'huong_dan_quan_ly_du_an'
            || $current_route == 'huong_dan_ket_thuc_du_an'
            || $current_route == 'huong_dan_lien_he_truc_tiep_freelancer'
            || $current_route == 'huong_dan_xac_thuc_thong_tin_tai_khoan'
            ): ?>
        <meta name="robots" content="<?php $view['slots']->output('index_follow', 'index,follow') ?>" />
    <?php else: ?>
        <meta name="robots" content="noindex,nofollow" />
    <?php endif; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="vLance.vn" />
    <link rel="publisher" href="http://plus.google.com/110723961725253795351/"/>
    <title><?php $view['slots']->output('title', $view['translator']->trans('home_page.og.title', array(), 'vlance')) ?></title>
    <meta property="og:site_name" content="vLance.vn" />
    <meta property="og:locale" content="<?php echo $view['translator']->trans('home_page.og.locale', array(), 'vlance');?>" />
    <meta property="og:type" content="website" />    
    <meta property="og:title" name="title" content="<?php $view['slots']->output('og_title', $view['translator']->trans('home_page.og.title', array(), 'vlance')) ?>" />
    <meta property="og:image" content="<?php $view['slots']->output('og_image', 'http://www.vlance.vn'.$view['assets']->getUrl('newsletter/img/share_vlance_home.jpg')) ?>" />
    <meta property="og:image:width" content="<?php $view['slots']->output('og_image_width', '1200') ?>" />
    <meta property="og:image:height" content="<?php $view['slots']->output('og_image_height', '630') ?>" />
    <meta property="og:url" content="<?php $view['slots']->output('og_url', 'http://www.vlance.vn') ?>" />
    <meta property="og:description" itemprop="description" name="description" content="<?php $view['slots']->output('og_description', $view['translator']->trans('home_page.og.description', array(), 'vlance')) ?>" />
    <meta property="fb:app_id" content="197321233758025" />
    <meta itemprop="keywords" name="keywords" content="<?php $view['slots']->output('og_keyword', "freelancer, freelancer là gì, freelancer việt nam, tuyển freelancer, freelance, tìm việc freelance, tìm việc làm thêm, việc làm tự do, việc làm online") ?>" />
    
    <link href="http://www.vlance.vn/rss.xml" rel="alternate" type="application/rss+xml" title="Việc freelance mới nhất cho Freelancer"/>
    <link itemprop="sameAs" href="http://www.facebook.com/vlance.vn"/>
    <link itemprop="sameAs" href="https://twitter.com/vlancevn"/>
    <link itemprop="sameAs" href="https://www.linkedin.com/company/vlance-vn"/>
    <link itemprop="sameAs" href="https://plus.google.com/+VlanceVn"/>
    
    <?php // google meta info starts ?>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Organization",
        "name" : "vLance.vn",
        "alternateName": "vLance.vn",
        "url": "http://www.vlance.vn/",
        "logo": "http://www.vlance.vn/img/logo.png",
        "contactPoint" : [
            { 
                "@type" : "ContactPoint",
                "telephone" : "+84-4-66841818",
                "contactType" : "customer service",
                "contactOption" : "TollFree",
                "areaServed" : "VN",
                "availableLanguage" : ["English","Vietnamese"]
            }
        ],
        "sameAs" : [ 
            "http://www.facebook.com/vlance.vn",
            "https://twitter.com/vlancevn",
            "https://www.linkedin.com/company/vlance-vn",
            "https://plus.google.com/+VlanceVn"
        ] 
    }
    </script>
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700&subset=latin,vietnamese' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo $view['assets']->getUrl('css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo $view['assets']->getUrl('css/bootstrap-datepicker.min.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo $view['assets']->getUrl('css/vlance.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo $view['assets']->getUrl('css/vlance-responsive.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo $view['assets']->getUrl('css/slider_home.min.css') ?>" rel="stylesheet" type="text/css" media="all" />
    <!--[if IE 8]>
    <link href="<?php echo $view['assets']->getUrl('css/vlance-ie8.css') ?>" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    
    <!--[if IE 7]>
    <link href="<?php echo $view['assets']->getUrl('css/vlance-ie7.css') ?>" media="screen" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery-1.10.1.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/bootstrap-datepicker.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/locales/bootstrap-datepicker.vn.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/hinclude.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery.localisation.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery.cookie.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/scriptsall.js') ?>"></script>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/common.min.js') ?>"></script>
    
    <!--[if IE]>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/jquery.placeholder.min.js') ?>"></script>
    <script>
        $(function() {
            $('input, textarea').placeholder();

        });
    </script>
    <style type="text/css">
        input, textarea { color: #000; }
        .placeholder { color: #aaa; }
    </style>
    <![endif]-->
    <?php foreach ($css as $url) :?>
    <link href="<?php echo $view['assets']->getUrl($view->escape($url)) ?>" rel="stylesheet" type="text/css" media="all" />
    <?php endforeach; ?>
    <?php foreach ($js as $url) :?>
    <script type="text/javascript" src="<?php echo $view['assets']->getUrl($view->escape($url)) ?>"></script>
    <?php endforeach; ?>
       
    <?php /* start Mixpanel tracking */ ?>
    <?php $host = $app->getRequest()->getHost();?>
    <?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
        <?php $mixpanel_token = $view['vlance']->getParameter("vlance_system.mixpanel_token.prod"); ?>
    <?php else: ?>
        <?php $mixpanel_token = $view['vlance']->getParameter("vlance_system.mixpanel_token.dev"); ?>
    <?php endif; ?>
    
    <script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
    mixpanel.init("<?php echo $mixpanel_token; ?>");</script>
    <!-- end Mixpanel -->
    
    <?php if($this->container->getParameter('kernel.environment') != 'dev' && ($host == 'www.vlance.vn' || $host == 'vlance.vn')): ?>
        <?php /* start Sumo.me */ ?>
        <script src="//load.sumome.com/" data-sumo-site-id="c35bd57eeb5c9a757f46a26b14d425040e1cca43bdc3ba5cdfaba1d411ae0914" async="async"></script>    
    <?php endif;?>
</head>
