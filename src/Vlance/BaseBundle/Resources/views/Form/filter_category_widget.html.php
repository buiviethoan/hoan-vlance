<div class="layered-block ln-category">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.classification', array(), 'vlance') ?></h2>
    <div class="block-content subcategory">
        <ul class="category unstyled">
            <li class="cat-item <?php echo $view_all['selected']?>">
                <a href="<?php echo $view_all['url']?>">
                    <?php echo $view_all['title'] ?>
                    <?php /*(<?php echo $view_all['num']?>)*/ ?>
                </a>
            </li>
            <?php foreach($categories as $category_data) : ?>
                <?php if ($category_data['num'] > 0): ?>
                    <li class="cat-item <?php echo $category_data['selected']?>">
                        <a href="<?php echo $category_data['url']?>">
                            <?php echo $category_data['title'].' ' ?>(<?php echo $category_data['num']?>)
                        </a>
                    </li>
                    <?php if($category_data['sub']) : ?>
                        <ul class="subcategory unstyled">
                            <?php foreach($category_data['sub'] as $sub_cat) : ?>
                                <?php if($sub_cat['num'] > 0) : ?>
                                    <li class="sub-item">
                                        <a href="<?php echo $sub_cat['url']?>">
                                            <i class="span1 <?php echo $sub_cat['checked']?>"></i>
                                            <div><?php echo $sub_cat['title'].' ' ?>(<?php echo $sub_cat['num']?>)</div>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>


<?php 
/*
<div class="layered-block ln-rating">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.quality', array(), 'vlance') ?></h2>
    <div class="block-content">
        <a class="rating-block rating-from row-fluid">
            <div class="rating-label span5"><span>Ít nhất 5</span></div>
            <div class="rating-select span6">
                <div class="rating-box">
                    <div class="rating" style="width:100%"></div>
                </div>
            </div>
        </a>
        <a class="rating-block rating-from row-fluid">
            <div class="rating-label span5"><span>Ít nhất 4</span></div>
            <div class="rating-select span6">
                <div class="rating-box">
                    <div class="rating" style="width:80%"></div>
                </div>
            </div>
        </a>
        <a class="rating-block rating-from row-fluid">
            <div class="rating-label span5"><span>Ít nhất 3</span></div>
            <div class="rating-select span6">
                <div class="rating-box">
                    <div class="rating" style="width:60%"></div>
                </div>
            </div>
        </a>
    </div>
</div>
<!--<div class="layered-block ln-skill"></div>-->
<div class="layered-block ln-feedback">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.review', array(), 'vlance') ?></h3>
    <div class="block-content">
        <div class="feedback-select">
            <a href="#">Ít nhất 15 đánh giá</a>
        </div>
        <div class="feedback-select">
            <a href="#">Ít nhất 10 đánh giá</a>
        </div>
        <div class="feedback-select">
            <a href="#">Ít nhất 5 đánh giá</a>
        </div>
    </div>
</div>
*/
?>