id="<?php echo $view->escape($id) ?>"
name="<?php echo $view->escape($full_name) ?>"
<?php if ($read_only): ?>readonly="readonly" <?php endif ?>
<?php if ($disabled): ?>disabled="disabled" <?php endif ?>
<?php if ($required): ?>required="required" <?php endif ?>
<?php if ($max_length): ?>maxlength="<?php echo $view->escape($max_length) ?>" <?php endif ?>
<?php if ($pattern): ?>pattern="<?php echo $view->escape($pattern) ?>" <?php endif ?>
<?php foreach ($attr as $k => $v): ?>
    <?php if($k == 'complex_attr'):?>
        <?php if(isset($v['name']) && isset($v['value'])):?>
        <?php printf('%s="%s" ', $view->escape($v['name']), $view->escape(in_array($v['name'], array('placeholder', 'title', 'x-moz-errormessage', 'error')) ? $view['translator']->trans($v['value'], isset($v['%var%'])?array('%var%' => $v['%var%']):array(), $translation_domain) : $v['value'])) ?>
        <?php endif;?>
    <?php else:?>
        <?php printf('%s="%s" ', $view->escape($k), $view->escape(in_array($k, array('placeholder', 'title', 'x-moz-errormessage', 'data-content', 'error')) ? $view['translator']->trans($v, array(), $translation_domain) : $v)) ?>
    <?php endif;?>
<?php endforeach; ?>
