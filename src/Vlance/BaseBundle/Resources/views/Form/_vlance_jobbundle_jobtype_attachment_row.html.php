<div<?php if(isset($wrapper_class)):?> class="<?php echo $wrapper_class;?>"<?php endif;?>>
    <?php echo $view['form']->errors($form);?>
    <?php echo $view['form']->widget($form);?>
</div>