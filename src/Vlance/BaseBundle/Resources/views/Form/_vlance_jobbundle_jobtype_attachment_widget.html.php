<a href="#" onclick="return false;" data-toggle="collapse" data-target="#file_upload">
    <?php echo $view['translator']->trans($label, array(), 'vlance') ?>
</a>
<div id="file_upload" class="collapse out">
    <input type="file" <?php echo $view['form']->block($form, 'widget_attributes')?>/>
</div>