<input type="text" id="<?php echo "{$id}_typeahead" ?>"
    data-source='<?php echo $view->render("VlanceBaseBundle:Form/EntityPicker:data_source.html.php", array('accounts' => $accounts));?>'
    placeholder="<?php echo $view['translator']->trans('admin.form.accountpicker')?>" autocomplete="off" />
<input type="hidden" <?php echo $view['form']->block($form, 'widget_attributes')?> <?php if (defined($value) && $value):?>value="<?php echo $value?>"<?php endif;?> />

<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo "{$id}_typeahead" ?>').typeahead({
            source: function() {
                var data = this.$element.data('source');
                return data;
            },
            matcher: function (item) {
                return ~item.email.toLowerCase().indexOf(this.query.toLowerCase());
            },
            sorter: function (items) {
                var beginswith = []
                  , caseSensitive = []
                  , caseInsensitive = []
                  , item

                while (item = items.shift()) {
                  if (!item.email.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
                  else if (~item.email.indexOf(this.query)) caseSensitive.push(item)
                  else caseInsensitive.push(item)
                }

                return beginswith.concat(caseSensitive, caseInsensitive)
            },
            highlighter: function (item) {
                var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&')
                var heading = item.email.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                    return '<strong>' + match + '</strong>'
                });

                var builder = $('<div></div>').addClass('clearfix');

                var text = $('<div></div>').addClass('pull-left').append(
                  $('<blockquote></blockquote>')
                      .append($('<p></p>').html(heading))
                      .append($('<div></div>')
                          .append($('<small/>').html(item))
                      )
                );
                builder.append(text);

                return builder;
            },
            updater: function (item) {
                $('#<?php echo $id;?>').val(item.id);
                return item.email;
            },
            getter: function(pk) {
                var source = $.isFunction(this.source) ? this.source() : this.source;
                return $.grep(source, function(item){
                    return item.id == pk;
                })[0];
            },
            setter: function(item) {
                return item.id;
            }
        });

        <?php if (defined($value) && $value) : ?>
            var data = $('#<?php echo "{$id}_typeahead" ?>').data('source');
            var item = $.grep(data, function(item){
                    return item.id == <?php echo $value ?>;
                })[0];
            $('#<?php echo "{$id}_typeahead" ?>').val(item.id);
        <?php endif; ?>
    });
</script>
