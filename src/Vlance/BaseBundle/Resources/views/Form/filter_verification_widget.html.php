<div class="layered-block ln-category">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.classification_status', array(), 'vlance') ?></h2>
    <div class="block-content subcategory_status">
        <ul class="subcategory unstyled">
            <?php ?>
            <?php foreach($status as $item) : ?>
                <?php // if($item['num'] > 0) : ?>
                    <li class="sub-item">
                        <a href="<?php print $item['url'] ?>">
                            <i class="spanb <?php echo $item['checked'] ?>"></i>
                            <div>
                                <?php if($item['status'] == 'nap-tien'):?>
                                    <span class="label-tag label-green"><?php print $item['title'].' ';?></span>
                                <?php else: ?>
                                    <?php print $item['title'].' ';?>
                                <?php endif;?></div>
                        </a>
                    </li>
                <?php // endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
