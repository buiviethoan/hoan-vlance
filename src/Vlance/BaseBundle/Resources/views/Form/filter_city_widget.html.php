<div class="layered-block ln-category ln-category-city showcity hidecity">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.classification_city', array(), 'vlance') ?></h2>
    <div class="block-content subcategory_city">
        <ul class="subcategory unstyled">
            <?php foreach($cities as $city) : ?>
                <?php if($city['num'] > 0) : ?>
                    <li class="sub-item <?php echo $city['checked'] ?>">
                        <a href="<?php print $city['url'] ?>">
                            <i class="span1 <?php echo $city['checked'] ?>"></i>
                            <div><?php print $city['title'].' ';?>
                                <?php if($city['title'] != 'Toàn quốc'){ ?>
                                    (<?php echo $city['num'];?>)
                                <?php } ?>
                            </div>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="bottomcity"><span class="buttoncity"></span></div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".buttoncity").click(function() {
            $('.ln-category-city').toggleClass( "hidecity" );
            $('.ads-2').toggleClass("showpopup");
            $('.ads-3').toggleClass("showpopup");
//            document.getElementById("#more-comment-fd-" + jQuery(this).attr("data-id")).innerText ="Rút gọn";
        });
        <?php if(count($cities) <=10): ?>
            $(".buttoncity").click();
            $("#bottomcity").hide();
        <?php endif; ?>
    });
    
</script>