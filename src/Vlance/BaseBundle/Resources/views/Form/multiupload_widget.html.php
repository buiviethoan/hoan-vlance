<?php //var_dump($attr);die; ?>
<?php if(isset($attr['singlefile'])): ?>
    <div id="vlance_jobbundle_contestapplytype_files_0">
        <div class="control-group ">
            <input type="file" id="vlance_jobbundle_contestapplytype_files_0_file" name="vlance_jobbundle_contestapplytype[files][0][file]" required="required" value="">
        </div>
    </div>
<?php else: ?>
    <a class="addupload" href="#" onclick="return false;" data-toggle="collapse" data-target="#<?php echo $id;?>_file_upload_container">
        <?php echo $view['translator']->trans($label, array(), 'vlance') ?>
    </a>
    <div class="clear"></div>
    <?php 
    /**
     * Display file uploaded
     */
    if (isset($value)) : ?>
        <div id="<?php echo $id;?>" class="control-group fileupload_wrapper ">
            <?php foreach ($value as $idx => $file) : ?>
            <?php /* @var $file \Vlance\BaseBundle\Entity\File */ ?>
            <?php if ($file instanceof \Vlance\BaseBundle\Entity\File) :?>
        <div id="<?php echo "{$id}_attach_{$idx}";?>" class="attach">
            <ul>
                <li class="row-fluid">
                    <div class="i32 i32-attach span1"></div>
                    <a href="<?php echo $file->getDownloadUrl() ?>" class="span9" title="<?php echo $file->getFilename(); ?>"><?php echo $file->getFilename(); ?></a>
                    <a href="#formdelete-<?php echo $idx;?>" role="button" data-toggle="modal" class="span1 offset1" title="<?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?>"><?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?></a>
                    <input id="<?php echo "{$id}_{$idx}";?>" name="<?php echo $full_name;?>[<?php echo $idx;?>][file]" class="hide"/>
                </li>
            </ul>
            <div id="formdelete-<?php echo $idx;?>" class="modal hide fade form-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <h3 id="myModalLabel"><?php echo $view['translator']->trans('edit_job_page.popup_delete', array(), 'vlance') ?></h3>
                </div>
                <div class="modal-body">
                    <strong><?php echo $file->getFilename(); ?></strong>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><?php echo $view['translator']->trans('common.close_popup_no', array(), 'vlance') ?></button>
                    <button type="button" class="btn btn-primary" role="confirm" target="<?php echo "{$id}_attach_{$idx}";?>" data-dismiss="modal" aria-hidden="true"><?php echo $view['translator']->trans('common.delete', array(), 'vlance') ?></button>
                </div>
            </div>
        </div>
            <?php endif;?>
            <?php endforeach; ?>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('button[role="confirm"]').click(function() {
                jQuery(this).parent().parent().modal('hide');
                jQuery("#" + jQuery(this).attr('target')).remove();
            });
        });
    </script>
    <?php endif; ?>
    <?php
        /**
         * Display input file to upload new file
         */
    ?>
    <div id="<?php echo $id;?>_file_upload_container" class="collapse out">
        <div class="support_file_attach">Định dạng được hỗ trợ: png, jpg, gif, pdf, psd, xls, xlsx, doc, docx, ppt, pptx, ods, odt, zip, rar</div>
        <div class="support_file_attach">Kích thước tối đa: 10MB</div>
        <div id="<?php echo $id;?>_collection_holder" <?php echo $view['form']->block($form, 'widget_attributes') ?>>
        </div>
        <a href="#" class="btn add_input_file" id="<?php echo $id;?>_job_new_file"><i class="icon-plus"></i> <?php echo $view['translator']->trans('job.new.add_file', array(), 'vlance'); ?></a>
    </div>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            var required_one = false;
            <?php if (isset($attr)):?>
                <?php if(is_array($attr)):?>
                    <?php if(isset($attr['required_one'])):?>
                        <?php if($attr['required_one']):?>
                            required_one = true;
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endif; ?>
                
            var newButton = jQuery('');
            function add_file_input(holder, id) {
                var input_content = holder.data('prototype');
                var index = holder.data('index');
                var new_file = input_content.replace(/__name__label__/g, '').replace(/__name__/g, index);
                var new_item = jQuery("<div></div>").addClass('new_file_input').append(new_file);
                add_remove_file_input(new_item);

                holder.append(new_item);
                jQuery(id + '_' + index.toString() + '_file').on('change', function() {
                    if(this.files[0]){
                        jQuery(this).attr('file_size',this.files[0].size);
                    }
                });
                holder.data('index', index + 1);
            }

            function add_remove_file_input(new_item) {
                var remove_button = jQuery('<a href="#" class="remove">x</a>');
                new_item.append(remove_button);
                remove_button.on('click', function(e) {
                    e.preventDefault();
                    if(required_one){
                        if(jQuery('.new_file_input input').length == 1){
                            alert('File đính kèm là bắt buộc.');
                        } else{
                            new_item.remove();
                        }
                    } else {
                        new_item.remove();
                    }
                })
            }

            if (jQuery('#<?php echo $id;?>')) {
                jQuery('#<?php echo $id;?>_collection_holder').data('index', jQuery('#<?php echo $id;?> input').size());
            } else {
                jQuery('#<?php echo $id;?>_collection_holder').data('index', 0);
            }
            jQuery('#<?php echo $id;?>_job_new_file').click(function(e) {
                add_file_input(jQuery('#<?php echo $id;?>_collection_holder'), '#<?php echo $id;?>');
                e.preventDefault();
            });
            
            if(required_one){
                jQuery('#<?php echo $id;?>_file_upload_container').collapse('show');
                add_file_input(jQuery('#<?php echo $id;?>_collection_holder'), '#<?php echo $id;?>');
            }
        });
    </script>
<?php endif; ?>