<div class="layered-block ln-category ln-category-skill showskill hideskill">
    <h2 class="layered-heading"><?php echo $view['translator']->trans('list_job.menu_left.classification_skill', array(), 'vlance') ?></h2>
    <div class="block-content subcategory_skill">
        <ul class="subcategory unstyled">
            <?php foreach($skills as $skill) : ?>
                <?php if($skill['num'] > 0) : ?>
                    <li class="sub-item <?php echo $skill['checked'] ?>">
                        <a href="<?php print $skill['url'] ?>">
                            <i class="span1 <?php echo $skill['checked'] ?>"></i>
                            <div><?php print $skill['title'].' ';?>
                                <?php if($skill['title'] != 'Tất cả'){ ?>
                                    (<?php echo $skill['num'];?>)
                                <?php } ?>
                            </div>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="bottomskill"><span class="buttonskill"></span></div>
    
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".buttonskill" ).click(function() {
            $('.ln-category-skill').toggleClass( "hideskill" );
            $('.layered-navigation').toggleClass("hide-adv");
        });
        <?php if(count($skills) <=10): ?>
            $(".buttonskill").click();
            $("#bottomskill").hide();
        <?php endif; ?>
    });
    
</script>