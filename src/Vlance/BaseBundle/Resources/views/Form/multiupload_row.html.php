<?php if (isset($prototype)): ?>
    <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>
<?php endif ?>
<div<?php if(isset($wrapper_class)):?> class="<?php echo $wrapper_class;?>"<?php endif;?>>
    <?php echo $view['form']->errors($form);?>
    <?php echo $view['form']->widget($form, array('attr' => $attr));?>
</div>