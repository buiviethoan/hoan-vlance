<?php if (isset($errors) && sizeof($errors) >0):?>
<ul class="error">
    <?php foreach($errors as $error):?>
        <li><?php echo $error->getMessage();?></li>
    <?php endforeach;?>
</ul>
<?php endif;?>