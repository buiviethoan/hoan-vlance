<?php if (isset($prepend) || isset($append)):?>
<div class="datepicker-widget <?php if (isset($prepend) && $prepend): ?>input-prepend <?php endif; ?>input-append">
    <?php if (isset($prepend) && $prepend): ?><span class="add-on"><?php echo $view['translator']->trans($prepend) ?></span><?php endif; ?>
    <input type="text" <?php echo $view['form']->block($form, 'widget_attributes')?> 
        <?php if (isset($value) && !empty($value)):?>value="<?php echo $value?>" <?php endif;?>/>
    <?php if (isset($append) && $append): ?><span class="add-on"><i class="i32 i32-calendar"></i></span><?php endif; ?>
</div>
<?php else:?>
    <input type="text" <?php echo $view['form']->block($form, 'widget_attributes')?> 
        <?php if (isset($value) && !empty($value)):?>value="<?php echo $value?>" <?php endif;?>/>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#<?php echo $id?>').datepicker({
            format:   'dd/mm/yyyy',
            language: 'vn',
            autoclose: true,
            weekStart: 1,
            <?php if (isset($start_date)) : ?>
            startDate: '<?php echo $start_date;?>',
            <?php endif;?>
            <?php if (isset($end_date)) : ?>
            endDate: '<?php echo $end_date;?>'
            <?php else: ?>
            endDate: '<?php echo date("d/m/Y", (time() + 86400 * 30)) // today + 30 days, TODO: Do this in correct way ?>'
            <?php endif;?>
        });
    });
</script>