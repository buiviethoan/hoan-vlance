<div class="control-group <?php if(isset($wrapper_class)):?><?php echo $wrapper_class;?><?php endif;?>">
    <?php echo $view['form']->label($form);?>
    <?php echo $view['form']->widget($form);?>
    <?php echo $view['form']->errors($form);?>
</div>