<?php if (isset($prepend) || isset($append)):?>
<div class="<?php echo (isset($prepend) && $prepend)?"input-prepend ":" "; ?> <?php echo (isset($append) && $append)?"input-append ":" "; ?>">
    <?php if (isset($prepend) && $prepend): ?><span class="add-on"><?php echo $view['translator']->trans($prepend) ?></span><?php endif; ?>
    <input type="<?php echo isset($type)?$type:"text"?>" <?php echo $view['form']->block($form, 'widget_attributes')?> <?php if (isset($value)):?>value="<?php echo $value;?>"<?php endif;?>/>
    <?php if (isset($append) && $append): ?><span class="add-on"><?php echo $view['translator']->trans($append, array(), 'vlance') ?></span><?php endif; ?>
</div>
<?php else:?>
    <input type="<?php echo isset($type)?$type:"text"?>" <?php echo $view['form']->block($form, 'widget_attributes')?> <?php if (isset($value)):?>value="<?php echo $value;?>"<?php endif;?>/>
<?php endif; ?>
