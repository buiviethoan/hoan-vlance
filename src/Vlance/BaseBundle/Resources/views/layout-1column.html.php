<?php use Vlance\BaseBundle\Utils\HasPermission; ?>
<?php $current_user = $app->getSecurity()->getToken()->getUser(); ?>
<?php
    $current_route = $app->getRequest()->attributes->get('_route');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('routeName' => $current_route, '_route_params' => $app->getRequest()->attributes->get('_route_params'))));?>
    <body class="one-column 
        <?php if($current_route === 'job_show_freelance_job' || $current_route === 'job_onsite_view'){echo 'job_show ' .$current_route ; }
              elseif($current_route === 'job_onsite_new' || $current_route === 'job_new' || $current_route === 'job_onsite_edit'){echo 'job_post ' .$current_route ; }
              elseif($current_route === 'account_show_freelancer'){echo 'account_show ' .$current_route ; }
              else{echo $current_route;} ?>" >
        <?php // print_r($routeName) ?>
        <div class="main">
            <?php /* Header block */?>
            <?php if(in_array($current_route, array("thuengay_homepage", "service_pack_list", "show_service_pack", "service_payment", "choose_freelancer"))):?>
                <?php echo $view['actions']->render(
                    $view['router']->generate('block_header_thuengay', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
                    ); ?>
            <?php elseif(in_array($current_route, array("community_post", "community_post_show", "community_post_new"))):?>
                <?php echo $view['actions']->render(
                    $view['router']->generate('block_header_community', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
                    ); ?>
            <?php else:?>
                <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_header', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
                    );?>
            <?php endif; ?>
            
            <?php /* check tài khoản login */ ?>
            <?php if(HasPermission::hasAdminPermission($current_user) == TRUE): ?>
                <div class="admin-control form-actions form-tabs">
                    <div class="container">
                        <?php echo $view['actions']->render(
                            $view['router']->generate(
                                'vlance_base_block_admin', 
                                array(
                                    '_route_params' => serialize($app->getRequest()->attributes->get('_route_params')), 
                                    'routeName' => $current_route
                                ), 
                                true), 
                            array('strategy' => 'esi')
                        ); ?>
                    </div>
                </div>
            <?php endif; ?>
            
            <?php /* Messages block */?>
            <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_messages', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route)));?>

            <?php /* Main content block */?>
            <div class="top_content">
                <?php
                    echo $view['actions']->render(
                        $view['router']->generate('vlance_base_block_top', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true), array('strategy' => 'esi')
                    );
                ?>
            </div>
            <div class="main_content">
                <?php $view['slots']->output('content') ?>
            </div>
            <?php /* End Main content block */?>

            <div id="footer_content">
                <?php /* Footer block */?>
                <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true),
                    array('strategy' => 'esi')
                ); ?>
            </div>
            
            <div class="row hidden">
                <?php /* Hidden block use for popup content */?>
                <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_beforebodyend', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true),
                    array('strategy' => 'esi')
                ); ?>
            </div>
            
            <div class="row hidden">
                <?php echo $view->render('VlanceBaseBundle:Block:add_category_mini_vip.html.php', array()); ?>
            </div>
        </div>
    </body>
</html>
