<!DOCTYPE html>
<?php
    $current_route = $app->getRequest()->attributes->get('_route');
?>
<html>
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('routeName' => $current_route, '_route_params' => $app->getRequest()->attributes->get('_route_params'))));?>
    <body class="one-column error-page">
        <?php /* Header block */?>
            <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_header', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
            ); ?>
        <!-- about page -->
		<div class="error-section container">
            <div class="row-fluid">
                <h1>LỖI 404</h1>
                <p>Có vẻ như bạn đã lạc đường, hãy theo đường <a href="<?php echo $view['router']->generate('vlance_homepage') ?>">link</a> này để trở lại trang chủ</p>
                <div class="map-error">
                    <div>Bạn đang ở đây</div>
                </div>
            </div>
		</div>
		<!-- End about page -->
		
		
		<!-- Footer -->
        <?php /* Footer block */?>
        <?php echo $view['actions']->render(
            $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true),
            array('strategy' => 'esi')
        ); ?>
        <!-- javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
    </body>
</html>