<!DOCTYPE html>
<?php
    $current_route = $app->getRequest()->attributes->get('_route');
?>
<html>
    <?php echo $view['actions']->render($view['router']->generate('vlance_base_block_head', array('routeName' => $current_route, '_route_params' => $app->getRequest()->attributes->get('_route_params'))));?>
    <body class="one-column error-page">
        <?php /* Header block */?>
            <?php echo $view['actions']->render(
                    $view['router']->generate('vlance_base_block_header', array('routeName' => $current_route), true),
                    array('strategy' => 'esi')
            ); ?>
        <!-- about page -->
		<div class="error-section container">
                    <div class="row-fluid">
                        <h1><?php echo $view['translator']->trans('error.page500.header', array(), 'vlance') ?></h1>
                        <p><?php echo $view['translator']->trans('error.page500.error_message1', array(), 'vlance') ?></p>
                        <p><?php echo $view['translator']->trans('error.page500.back_to_homepage', array("%%a_open%%" => "<a href='" . $view['router']->generate('vlance_homepage') . "'>", "%%a_close%%" => "</a>"), 'vlance') ?></p>
                        <p><?php echo $view['translator']->trans('error.page500.error_message2', array(), 'vlance') ?></p>
                        <div class="map-error-500"></div>
                    </div>
		</div>
        <!-- End about page -->
		
		
		<!-- Footer -->
        <?php /* Footer block */?>
        <?php echo $view['actions']->render(
            $view['router']->generate('vlance_base_block_footer', array('_route_params' => $app->getRequest()->attributes->get('_route_params'), 'routeName' => $current_route), true),
            array('strategy' => 'esi')
        ); ?>
        <!-- javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
    </body>
</html>