<?php if($type == 'js'):?>
<script type="text/javascript">
/* <![CDATA[ */
jQuery(document).ready(function() {
    data = JSON.parse('<?php echo $data?>');
    if(typeof data['function'] !== 'undefined'){
        if(data['function'] == "people.set"){
            mixpanel.people.set(data['params']['info']);
        }
    }
    if(data['identify']){
        if(data['event']['name'] == "Registration"){
            mixpanel.alias(data['identify']);
        }
        else {
            mixpanel.identify(data['identify']);
        }
    }
    if(typeof data['event'] !== 'undefined'){
        if(data['event']['properties']){
            // Mixpanel
            if(data['track_mixpanel'] === true){
                console.log("log here 1");
                mixpanel.track(data['event']['name'], data['event']['properties']);
            }
            // Google Analytics
            if(data['track_ga'] === true){
                ga('send', 'event', 'general', data['event']['name'], data['event']['properties']);
            }
        } else{
            // Mixpanel
            if(data['track_mixpanel'] === true){
                mixpanel.track(data['event']['name']);
            }
            // Google Analytics
            if(data['track_mixpanel'] === true){
                ga('send', 'event', 'general', data['event']['name']);
            }
        }
    }
    if(typeof data['revenue'] !== 'undefined'){
        mixpanel.people.track_charge(data['revenue']);
    }
});
/* ]]> */
</script>
<?php endif;?>

<?php if($type == 'html'):?>
    <?php echo $html;?>
<?php endif; ?>
