<?php
namespace Vlance\BaseBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class OverrideFormTypeCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $formTypeDefinition = $container->getDefinition('form.type.form');
        $formTypeDefinition->setClass('Vlance\BaseBundle\Form\Type\FormType');
    }
}
