<?php

namespace Vlance\BaseBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PartnerMoneyloverCodeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PartnerMoneyloverCodeRepository extends EntityRepository
{
    public function getAvailableCodes()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder->select('c')->from('VlanceBaseBundle:PartnerMoneyloverCode','c');
        $and = $queryBuilder->expr()->andX();
        $and->add($queryBuilder->expr()->isNull('c.account'));
        $and->add($queryBuilder->expr()->gte("c.expiredAt", ":currentDate"));
        $queryBuilder->andWhere($and)->setParameter('currentDate', new \DateTime());
        return $queryBuilder->getQuery()->getResult();
    }
}
