<?php

namespace Vlance\BaseBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PartnerMoneyloverCode
 *
 * @ORM\Table(name="partner_moneylover_code")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\PartnerMoneyloverCodeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PartnerMoneyloverCode
{
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Vlance\AccountBundle\Entity\Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $account;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="code", type="string", length=255)
     */
    protected $code;

    /**
     * Information is stored as an array in JSON format
     * @var string
     *
     * @ORM\Column(name="detail", type="text", nullable=true)
     */
    private $detail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="expired_at", type="datetime", nullable=true)
     */
    protected $expiredAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(name="offered_at", type="datetime", nullable=true)
     */
    protected $offeredAt;
    
    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PartnerMoneyloverCode
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set detail
     *
     * @param string $detail
     * @return PartnerMoneyloverCode
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    
        return $this;
    }

    /**
     * Get detail
     *
     * @return string 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return PartnerMoneyloverCode
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set offeredAt
     *
     * @param \DateTime $offeredAt
     * @return PartnerMoneyloverCode
     */
    public function setOfferedAt($offeredAt)
    {
        $this->offeredAt = $offeredAt;
    
        return $this;
    }

    /**
     * Get offeredAt
     *
     * @return \DateTime 
     */
    public function getOfferedAt()
    {
        return $this->offeredAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PartnerMoneyloverCode
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return PartnerMoneyloverCode
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return PartnerMoneyloverCode
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     * @return PartnerMoneyloverCode
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;
    
        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime 
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }
}