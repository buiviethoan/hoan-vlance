<?php

namespace Vlance\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EmailLog
 *
 * @ORM\Table(name="email_log")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\EmailLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailLog
{   

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="from", type="string", length=256)
     */
    protected $from = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="to", type="string", length=256)
     */
    protected $to = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=256)
     */
    protected $header = null;

    /**
     * @var string
     *
     * @ORM\Column(name="html_content", type="text")
     */
    private $htmlContent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="text_content", type="text")
     */
    private $textContent;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set from
     *
     * @param string $from
     * @return EmailLog
     */
    public function setFrom($from)
    {
        $this->from = $from;
    
        return $this;
    }

    /**
     * Get from
     *
     * @return string 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param string $to
     * @return EmailLog
     */
    public function setTo($to)
    {
        $this->to = $to;
    
        return $this;
    }

    /**
     * Get to
     *
     * @return string 
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set header
     *
     * @param string $header
     * @return EmailLog
     */
    public function setHeader($header)
    {
        $this->header = $header;
    
        return $this;
    }

    /**
     * Get header
     *
     * @return string 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set htmlContent
     *
     * @param string $htmlContent
     * @return EmailLog
     */
    public function setHtmlContent($htmlContent)
    {
        $this->htmlContent = $htmlContent;
    
        return $this;
    }

    /**
     * Get htmlContent
     *
     * @return string 
     */
    public function getHtmlContent()
    {
        return $this->htmlContent;
    }

    /**
     * Set textContent
     *
     * @param string $textContent
     * @return EmailLog
     */
    public function setTextContent($textContent)
    {
        $this->textContent = $textContent;
    
        return $this;
    }

    /**
     * Get textContent
     *
     * @return string 
     */
    public function getTextContent()
    {
        return $this->textContent;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return EmailLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return EmailLog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}