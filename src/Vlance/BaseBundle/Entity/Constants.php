<?php

namespace Vlance\BaseBundle\Entity;

class Constants {
    const STATUS_ENABLE = 'enable';
    const STATUS_DISABLE = 'disable';
}
