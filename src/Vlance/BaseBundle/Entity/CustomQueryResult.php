<?php

namespace Vlance\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;

/**
 * CustomQueryResult
 *
 * @ORM\Table(name="custom_query_result")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\CustomQueryResultRepository")
 */
class CustomQueryResult
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="CustomQuery", inversedBy="results", cascade={"persist"})
     * @ORM\JoinColumn(name="query_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $query;
    
    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    protected $value;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param $value
     * @return CustomQueryResult
     */
    public function setValue( $value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CustomQueryResult
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set query
     *
     * @param \Vlance\BaseBundle\Entity\CustomQuery $query
     * @return CustomQueryResult
     */
    public function setQuery(\Vlance\BaseBundle\Entity\CustomQuery $query = null)
    {
        $this->query = $query;
    
        return $this;
    }

    /**
     * Get query
     *
     * @return \Vlance\BaseBundle\Entity\CustomQuery 
     */
    public function getQuery()
    {
        return $this->query;
    }
}