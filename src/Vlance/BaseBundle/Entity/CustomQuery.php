<?php

namespace Vlance\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;

/**
 * CustomQuery
 *
 * @ORM\Table(name="custom_query")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\CustomQueryRepository")
 */
class CustomQuery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
    
    /**
     * @var text
     *
     * @ORM\Column(name="query", type="text")
     */
    protected $query;
    
    /**
     * @ORM\OneToMany(targetEntity="CustomQueryResult", mappedBy="query", cascade={"persist"})
     */
    protected $results;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean", options={"default" : "1"})
     */
    protected $enable = 1;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CustomQuery
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set query
     *
     * @param string $query
     * @return CustomQuery
     */
    public function setQuery($query)
    {
        $this->query = $query;
    
        return $this;
    }

    /**
     * Get query
     *
     * @return string 
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Add results
     *
     * @param \Vlance\BaseBundle\Entity\CustomQueryResult $results
     * @return CustomQuery
     */
    public function addResult(\Vlance\BaseBundle\Entity\CustomQueryResult $results)
    {
        $this->results[] = $results;
    
        return $this;
    }

    /**
     * Remove results
     *
     * @param \Vlance\BaseBundle\Entity\CustomQueryResult $results
     */
    public function removeResult(\Vlance\BaseBundle\Entity\CustomQueryResult $results)
    {
        $this->results->removeElement($results);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResults()
    {
        return $this->results;
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Set enable
     *
     * @param boolean $enable
     * @return CustomQuery
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    
        return $this;
    }

    /**
     * Get enable
     *
     * @return boolean 
     */
    public function getEnable()
    {
        return $this->enable;
    }
}