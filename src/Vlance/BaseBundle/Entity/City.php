<?php

namespace Vlance\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\CityRepository")
 */
class City implements \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;
    
     /**
     * @ORM\OneToMany(targetEntity="Vlance\JobBundle\Entity\Job", mappedBy="city")
     */
    protected $jobs;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\AccountBundle\Entity\Account", mappedBy="city")
     */
    protected $accounts;
    
    /**
     * @var string
     * 
     * @Gedmo\Slug(fields={"name"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * Set id
     * @param integer $id
     * @return City
     */
    
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __toString() {
        return $this->getName();
    }

    public function serialize() {
        return serialize(array(
            'id' => $this->id,
            'name' => $this->name,
        ));
    }

    public function unserialize($serialized) {
        $data = unserialize($serialized);
        $this->id = $data['id'];
        $this->name = $data['name'];
        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->accounts = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     * @return City
     */
    public function addJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Vlance\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\Vlance\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    
     /**
     * Add account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return City
     */
    public function addAccount(\Vlance\AccountBundle\Entity\Account $account)
    {
        $this->accounts[] = $account;
    
        return $this;
    }

    /**
     * Remove account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     */
    public function removeAccount(\Vlance\AccountBundle\Entity\Account $account)
    {
        $this->accounts->removeElement($account);
    }

    /**
     * Get accounts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
    /**
     * Get num job open in category
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getfilterJobs () {
        $num = $this->getJobs()->filter(
                    function($entry) {
                        if($entry->getPublish() == Job::PUBLISH) {
                            return true;
                        }
                        return false;
                    }
                );
        return count($num);
    }
    
    /**
     * Get num account type = freelancer in category
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getfilterFreelancer () {
        $num = $this->getAccounts()->filter(
                    function($entry) {
                        if($entry->getType() == Account::TYPE_FREELANCER && $entry->isEnabled() == 1) {
                            return true;
                        }
                        return false;
                    }
                );
        return count($num);
    }
    
    /**
     * Set hash
     *
     * @param string $hash
     * @return City
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }
}