<?php

namespace Vlance\BaseBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallBacks
 */

class File {
    
    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    protected $filename;
    
    protected $filename_prefix = '';
    
    protected $filename_subfix = '';
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="stored_name", type="string", length=255)
     */
    protected $storedName;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string")
     */
    protected $path;

    /**
     * @var string
     * @ORM\Column(name="mime_type", type="string")
     */
    protected $mimeType;

    /**
     * @var float
     * @ORM\Column(name="size", type="decimal")
     */
    protected $size;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    
    /**
     * @Assert\File(maxSize="10240k")
     */
    protected $file;
    
    /**
     * Which type this file is uploaded (job, message, porfolio)
     */
    protected $type = 'job';
    
    /**
     * Set filename
     *
     * @param string $filename
     * @return MessageFile
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename() {
        return $this->filename;
    }
     /**
     * Set storedName
     *
     * @param string $storedName
     * @return MessageFile
     */
    public function setStoreName($storedName) {
        $this->storedName = $storedName;

        return $this;
    }

    /**
     * Get storedName
     *
     * @return string 
     */
    public function getStoredName() {
        return $this->storedName;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath() {
        return $this->path;
    }
    
    /**
     * Set mimeType
     *
     * @param string $mimeType
     * @return File
     */
    public function setMimeType($mimeType) {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mimeType
     *
     * @return string 
     */
    public function getMimeType() {
        return $this->mimeType;
    }

    /**
     * Set size
     *
     * @param float $size
     * @return File
     */
    public function setSize($size) {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return float 
     */
    public function getSize() {
        return $this->size;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return JobFile
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR . DS . "{$this->type}";
    }
    
    public function __toString() {
        return $this->path;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL . "/{$this->type}";
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadTmpDir() {
        return UPLOAD_TMP_DIR;
    }
    
    public function getDownloadUrl() {
        return $this->getUploadUrl() . '/' . $this->path . '/' . $this->storedName;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {
            $this->storedName = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->getClientOriginalExtension();
            
            $this->path = date('m-Y', time());
            
            $this->filename = $this->getFile()->getClientOriginalName();
            $this->size = $this->getFile()->getClientSize();
            $this->mimeType = $this->getFile()->getClientMimeType();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null !== $this->getFile()) {
            $this->getFile()->move($this->getUploadDir() . '/' . $this->path, $this->storedName);
            $this->file = null;
        }
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function remove() {
        $full_path = $this->getUploadDir() . '/' . $this->path . '/' . $this->storedName;
        @unlink($full_path);
    }
}