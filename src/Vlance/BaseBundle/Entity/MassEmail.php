<?php

namespace Vlance\BaseBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * MassEmail
 *
 * @ORM\Table(name="mass_email")
 * @ORM\Entity(repositoryClass="Vlance\BaseBundle\Entity\MassEmailRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MassEmail
{   
    const STATUS_NEW        = 1;
    const STATUS_READY      = 2;
    const STATUS_PAUSED     = 3;
    const STATUS_RUNNING    = 4;
    const STATUS_COMPLETE   = 5;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="campaign_name", type="string", length=256)
     */
    protected $campaignName = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="tracking_id", type="string", length=256)
     */
    protected $trackingId = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="receiver_query", type="text")
     */
    protected $receiverQuery = null;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="receiver_counter", type="integer", nullable=true)
     */
    private $receiverCounter = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=256)
     */
    protected $fromName = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=256)
     */
    protected $fromEmail = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="test_email", type="string", length=256, nullable=true)
     */
    protected $testEmail = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="header", type="string", length=256)
     */
    protected $header = null;

    /**
     * @var string
     *
     * @ORM\Column(name="html_content", type="text")
     */
    private $htmlContent;
    
    /**
     * @var string
     *
     * @ORM\Column(name="text_content", type="text", nullable=true)
     */
    private $textContent;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="sent_success_counter", type="integer", nullable=true)
     */
    private $sentSuccessCounter = 0;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="schedule_at", type="datetime", nullable=true)
     */
    private $scheduleAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $enabled = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    protected $status = self::STATUS_NEW;
    
    /**
     * @var string
     *
     * @ORM\Column(name="progression", type="text", nullable=true)
     */
    protected $progression;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="track_sent", type="boolean", options={"default":"0"})
     * @Assert\Type(type="boolean")
     */
    protected $trackSent = false;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="track_open", type="boolean", options={"default":"1"})
     * @Assert\Type(type="boolean")
     */
    protected $trackOpen = true;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set campaignName
     *
     * @param string $campaignName
     * @return MassEmail
     */
    public function setCampaignName($campaignName)
    {
        $this->campaignName = $campaignName;
    
        return $this;
    }

    /**
     * Get campaignName
     *
     * @return string 
     */
    public function getCampaignName()
    {
        return $this->campaignName;
    }

    /**
     * Set trackingId
     *
     * @param string $trackingId
     * @return MassEmail
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;
    
        return $this;
    }

    /**
     * Get trackingId
     *
     * @return string 
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set receiverQuery
     *
     * @param string $receiverQuery
     * @return MassEmail
     */
    public function setReceiverQuery($receiverQuery)
    {
        $this->receiverQuery = $receiverQuery;
    
        return $this;
    }

    /**
     * Get receiverQuery
     *
     * @return string 
     */
    public function getReceiverQuery()
    {
        return $this->receiverQuery;
    }

    /**
     * Set fromName
     *
     * @param string $fromName
     * @return MassEmail
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    
        return $this;
    }

    /**
     * Get fromName
     *
     * @return string 
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     * @return MassEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    
        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string 
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set header
     *
     * @param string $header
     * @return MassEmail
     */
    public function setHeader($header)
    {
        $this->header = $header;
    
        return $this;
    }

    /**
     * Get header
     *
     * @return string 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set htmlContent
     *
     * @param string $htmlContent
     * @return MassEmail
     */
    public function setHtmlContent($htmlContent)
    {
        $this->htmlContent = $htmlContent;
    
        return $this;
    }

    /**
     * Get htmlContent
     *
     * @return string 
     */
    public function getHtmlContent()
    {
        return $this->htmlContent;
    }

    /**
     * Set textContent
     *
     * @param string $textContent
     * @return MassEmail
     */
    public function setTextContent($textContent)
    {
        $this->textContent = $textContent;
    
        return $this;
    }

    /**
     * Get textContent
     *
     * @return string 
     */
    public function getTextContent()
    {
        return $this->textContent;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return MassEmail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return MassEmail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set receiverCounter
     *
     * @param integer $receiverCounter
     * @return MassEmail
     */
    public function setReceiverCounter($receiverCounter)
    {
        $this->receiverCounter = $receiverCounter;
    
        return $this;
    }

    /**
     * Get receiverCounter
     *
     * @return integer 
     */
    public function getReceiverCounter()
    {
        return $this->receiverCounter;
    }

    /**
     * Set sentSuccessCounter
     *
     * @param integer $sentSuccessCounter
     * @return MassEmail
     */
    public function setSentSuccessCounter($sentSuccessCounter)
    {
        $this->sentSuccessCounter = $sentSuccessCounter;
    
        return $this;
    }

    /**
     * Get sentSuccessCounter
     *
     * @return integer 
     */
    public function getSentSuccessCounter()
    {
        return $this->sentSuccessCounter;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     * @return MassEmail
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
    
        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime 
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set testEmail
     *
     * @param string $testEmail
     * @return MassEmail
     */
    public function setTestEmail($testEmail)
    {
        $this->testEmail = $testEmail;
    
        return $this;
    }

    /**
     * Get testEmail
     *
     * @return string 
     */
    public function getTestEmail()
    {
        return $this->testEmail;
    }

    /**
     * Set scheduleAt
     *
     * @param \DateTime $scheduleAt
     * @return MassEmail
     */
    public function setScheduleAt($scheduleAt)
    {
        $this->scheduleAt = $scheduleAt;
    
        return $this;
    }

    /**
     * Get scheduleAt
     *
     * @return \DateTime 
     */
    public function getScheduleAt()
    {
        return $this->scheduleAt;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return MassEmail
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set trackSent
     *
     * @param boolean $trackSent
     * @return MassEmail
     */
    public function setTrackSent($trackSent)
    {
        $this->trackSent = $trackSent;
    
        return $this;
    }

    /**
     * Get trackSent
     *
     * @return boolean 
     */
    public function getTrackSent()
    {
        return $this->trackSent;
    }

    /**
     * Set trackOpen
     *
     * @param boolean $trackOpen
     * @return MassEmail
     */
    public function setTrackOpen($trackOpen)
    {
        $this->trackOpen = $trackOpen;
    
        return $this;
    }

    /**
     * Get trackOpen
     *
     * @return boolean 
     */
    public function getTrackOpen()
    {
        return $this->trackOpen;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MassEmail
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set progression
     *
     * @param string $progression
     * @return MassEmail
     */
    public function setProgression($progression)
    {
        $this->progression = $progression;
    
        return $this;
    }

    /**
     * Get progression
     *
     * @return string 
     */
    public function getProgression()
    {
        return $this->progression;
    }
}