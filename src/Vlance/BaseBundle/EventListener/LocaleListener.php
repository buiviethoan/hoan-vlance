<?php
namespace Vlance\BaseBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;

    public function __construct($defaultLocale = 'vn')
    {
        $this->defaultLocale = $defaultLocale;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        // try to see if the locale has been set as a _locale routing parameter
        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST) {
//            if (($locale = $request->attributes->get('_locale'))) {
//                $request->getSession()->set('_locale', $locale);
//                setcookie("lang", $locale, time()+60*60*24*365, "/");
//            } else {
//                $lang = $this->getAutodetectLocale($request);
//                $request->getSession()->set("_locale", $lang);
//                $request->setLocale($lang);
//                setcookie("lang", $lang, time()+60*60*24*365, "/");

                // if no explicit locale has been set on this request, use one from the session
                if ($request->getSession()->has('_locale')) {
                    $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
                } else {
                    if (isset($_SERVER['SYMFONY__LOCALE'])) {
                        $request->getSession()->set('_locale', $_SERVER['SYMFONY__LOCALE']);
                        $request->setLocale($_SERVER['SYMFONY__LOCALE']);
                    }
                }
//            }        
        }
    }
    
    protected function getAutodetectLocale($request)
    {
        if(!isset($_COOKIE['lang'])) { // IF COOKIE IS NOT SET
            return $this->checkLocation($request);
        } else {        // IF COOKIE IS SET
            if($_COOKIE['lang'] == "vn" || $_COOKIE['lang'] == "en"){
                return $_COOKIE['lang'];
            } else{
                return $this->checkLocation($request);
            }
        }
    }
    
    /**
     * Execute the post request
     */
    protected function checkLocation($request)
    {
        if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
            if(preg_match("/vi/i", $_SERVER['HTTP_ACCEPT_LANGUAGE'])){
                $lang = "vn";
            } else{
                $lang = "en";
            }
        } else{
            $lang = "vn";
        }

        // Detect language by GeoIP
        $location = "vn";
        if($lang == "en" && (rand(1,10) > 8)){
            $ips = $request->getClientIps();
            error_log("20%");
            error_log(json_encode($ips));
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "http://freegeoip.net/json/" . (end($ips) == "127.0.0.1"?"":end($ips)));
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
            $result = curl_exec($ch);
            error_log($result);
            curl_close($ch);
            
            $location = "vn";
            $decode_response = json_decode($result, true);
            if(!is_null($decode_response)){
                //error_log($decode_response['country_code']);
                if($decode_response['country_code'] == "VN"){
                    $location = "vn";
                } else{
                    $location = "en";
                }
            } else{
                $location = "vn";
            }
        }

        if($lang == "vn" || $location == "vn"){
            return "vn";
        } else{
            return "en";
        }
        return $lang;
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }
}