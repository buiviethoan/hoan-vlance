<?php
namespace Vlance\BaseBundle\EventListener;

use Vlance\BaseBundle\Entity\File as BaseFile;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\EventListener\ResizeFormListener;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Doctrine\Common\Collections\ArrayCollection;

class MultiUploadListener extends ResizeFormListener
{
    
    protected $_om;
    
    public function __construct($type, array $options = array(), $allowAdd = false, $allowDelete = false, ObjectManager $om = null) {
        parent::__construct($type, $options, $allowAdd, $allowDelete);
        $this->_om = $om;
    }
    
    public function preSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        if (null === $data || '' === $data) {
            $data = array();
        }

        if (!is_array($data) && !($data instanceof \Traversable && $data instanceof \ArrayAccess)) {
            throw new UnexpectedTypeException($data, 'array or (\Traversable and \ArrayAccess)');
        }
        
        // Remove all empty rows
        if ($this->allowDelete && is_array($form->getData())) {
            foreach ($form->getData() as $name => $child) {
                if (!isset($data[$name])) {
                    if ($child instanceof BaseFile) {
                        $this->_om->remove($child);
                    }
                }
            }
        }

        // Add all additional rows
        if ($this->allowAdd) {
            foreach ($data as $name => $value) {
                if (!$form->has($name)) {
                    $form->add($name, $this->type, array_replace(array(
                        'property_path' => '['.$name.']',
                    ), $this->options));
                }
            }
        }
    }
}
