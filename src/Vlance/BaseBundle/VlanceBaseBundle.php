<?php

namespace Vlance\BaseBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Vlance\BaseBundle\DependencyInjection\Compiler\OverrideFormTypeCompilerPass;

class VlanceBaseBundle extends Bundle
{
    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(new OverrideFormTypeCompilerPass());
    }
}
