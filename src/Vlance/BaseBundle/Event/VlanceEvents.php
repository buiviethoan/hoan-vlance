<?php
namespace Vlance\BaseBundle\Event;
class VlanceEvents {    
    /**
     * Defini the events for transaction
     */
    const TRANS_DEPOSIT_BEFORE_CREATE   = 'vlance.transaction.deposit.create.before';
    const TRANS_DEPOSIT_AFTER_CREATE    = 'vlance.transaction.deposit.create.after';
    const TRANS_ESCROW_BEFORE_CREATE    = 'vlance.transaction.escrow.create.before';
    const TRANS_ESCROW_AFTER_CREATE     = 'vlance.transaction.escrow.create.after';
}