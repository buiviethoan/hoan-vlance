<?php

namespace Vlance\BaseBundle\Utils;

use Vlance\AccountBundle\Entity\Account;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;

class HasPermission {
        
    public static function hasPermission($role){
        if($role->hasRole($role::ROLE_SUPER_ADMIN)){
            return TRUE;
        }
        return FALSE;
    }
    
    public static function hasAdminPermission($current_user){
        if(is_object($current_user)){
          if(self::hasPermission($current_user) == TRUE){  
          return TRUE;
          }
        }
        return FALSE;
    }
    
}
