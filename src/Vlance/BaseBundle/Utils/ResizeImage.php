<?php

namespace Vlance\BaseBundle\Utils;

/**
 * Resize Image
 *
 * Takes the source image and resizes it to the specified width & height or proportionally if crop is off.
 * @access public
 * @author Jay Zawrotny <jayzawrotny@gmail.com>
 * @license Do whatever you want with it.
 * @param string $source_image The location to the original raw image.
 * @param string $destination_filename The location to save the new image.
 * @param int $width The desired width of the new image
 * @param int $height The desired height of the new image.
 * @param int $quality The quality of the JPG to produce 1 - 100
 * @param bool $crop Whether to crop the image or not. It always crops from the center.
 */

class ResizeImage {

    public static function resize_image($source_image, $destination_filename, $width = 200, $height = 150, $crop = 1, $margin_right = 10, $margin_bottom = 10)
    {
        if(!file_exists($source_image)){ return false;}
        if(!is_file($source_image)){ return false;}
        if( ! $image_data = getimagesize( $source_image ) ) {return false;}
        
        if (!file_exists(UPLOAD_DIR.DS.$destination_filename)) {
            mkdir(UPLOAD_DIR.DS.$destination_filename, 0777, true);
        }
        switch( $image_data['mime'] )
            {
                case 'image/gif':
                        $get_func = 'imagecreatefromgif';
                        $suffix = ".gif";
                        break;
                case 'image/jpeg';
                        $get_func = 'imagecreatefromjpeg';
                        $suffix = ".jpg";
                        break;
                case 'image/png':
                        $get_func = 'imagecreatefrompng';
                        $suffix = ".png";
                        break;
                default:
                    return false;
            }
        
        $file_name = basename($source_image,$suffix).$crop;
        if (file_exists(UPLOAD_DIR.DS.$destination_filename.DS.$file_name.$suffix)) {
             return UPLOAD_URL.'/'.$destination_filename.'/'.$file_name.$suffix;
        }
        try{
            $img_original = call_user_func( $get_func, $source_image );
        } catch (\Exception $e) { 
            return FALSE;
        }
        $old_width = $image_data[0];
        $old_height = $image_data[1];
        $new_width = $width;
        $new_height = $height;
        $src_x = 0;
        $src_y = 0;
        $current_ratio = round( $old_width / $old_height, 2 );
        $desired_ratio_after = round( $width / $height, 2 );
        $desired_ratio_before = round( $height / $width, 2 );

        if( $old_width < $width || $old_height < $height )
        {
            /**
             * The desired image size is bigger than the original image. 
             * Best not to do anything at all really.
             */
            //return $source_image;
        }


        /**
         * If the crop option is left on, it will take an image and best fit it
         * so it will always come out the exact specified size.
         */
        if( $crop == 1)
        {
            /**
             * create empty image of the specified size
             */
            $new_image = imagecreatetruecolor( $width, $height );
            /**
             * Landscape Image
             */
            if( $current_ratio > $desired_ratio_after )
            {
                    $new_width = $old_width * $height / $old_height;
            }

            /**
             * Nearly square ratio image.
             */
            if( $current_ratio > $desired_ratio_before && $current_ratio < $desired_ratio_after )
            {
                    if( $old_width > $old_height )
                    {
                            $new_height = max( $width, $height );
                            $new_width = $old_width * $new_height / $old_height;
                    }
                    else
                    {
                            $new_height = $old_height * $width / $old_width;
                    }
            }

            /**
             * Portrait sized image
             */
            if( $current_ratio < $desired_ratio_before  )
            {
                    $new_height = $old_height * $width / $old_width;
            }

            /**
             * Find out the ratio of the original photo to it's new, thumbnail-based size
             * for both the width and the height. It's used to find out where to crop.
             */
            $width_ratio = $old_width / $new_width;
            $height_ratio = $old_height / $new_height;

            /**
             * Calculate where to crop based on the center of the image
             */
            $src_x = floor( ( ( $new_width - $width ) / 2 ) * $width_ratio );
            $src_y = round( ( ( $new_height - $height ) / 2 ) * $height_ratio );
        }
        /**
         * Don't crop the image, just resize it proportionally
         */
        else
        {
                if( $old_width > $old_height )
                {
                        $ratio = max( $old_width, $old_height ) / max( $width, $height );
                }else{
                        $ratio = max( $old_width, $old_height ) / min( $width, $height );
                }

                $new_width = $old_width / $ratio;
                $new_height = $old_height / $ratio;

                $new_image = imagecreatetruecolor( $new_width, $new_height );
        }

        /**
         * Where all the real magic happens
         */
        imagecopyresampled( $new_image, $img_original, 0, 0, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height );
        
        /**
         * watermark image
         */
         
        $new_image = self::watermark_image($new_image, $margin_right, $margin_bottom);
        /**
         * Save it as a JPG File with our $destination_filename param.
         */
        imagejpeg( $new_image,UPLOAD_DIR.DS.$destination_filename.DS.$file_name.$suffix, 100 );

        /**
         * Destroy the evidence!
         */
        imagedestroy( $new_image );
        imagedestroy( $img_original );
        
        return UPLOAD_URL.'/'.$destination_filename.'/'.$file_name.$suffix;

    }
    public static function watermark_image($image, $margin_right = 10, $margin_bottom = 10){
        return $image;
//        $watermark = imagecreatefrompng('../img/logo.png');
//        $dst_x = imagesx($image);
//      
//        $dst_y = imagesy($image);
//
//        $src_x = imagesx($watermark);
//        $src_y = imagesy($watermark);
//        $new_image = imagecopy($image, $watermark, $dst_x - $src_x -$margin_right, $dst_y - $src_y - $margin_bottom, 0, 0, $src_x, $src_y);
//        return $new_image;
    }
    
}
