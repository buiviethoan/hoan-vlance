<?php

namespace Vlance\BaseBundle\Utils;

use Vlance\JobBundle\Entity\Job;

class GetStatus {
    
    public static function getStatusJob($status = 1)
    {
        switch ($status) {
            case Job::JOB_OPEN:
                $status = 'Chưa thực hiện';
                break;
            case Job::JOB_AWARDED:
                $status = 'Đã được giao';
                break;
            case Job::JOB_WORKING:
                $status = 'Đang thực hiện';
                break;
            case Job::JOB_CLOSED:
                $status = 'Dự án đã kết thúc';
                break;
            case Job::JOB_FINISH_REQUEST:
                $status = 'Yêu cầu kết thúc';
                break;
            case Job::JOB_FINISHED:
                $status = 'Kết thúc';
                break;
            case Job::JOB_CANCEL_REQUEST:
                $status = 'Khách hàng yêu cầu hủy bỏ dự án';
                break;
            case Job::JOB_CANCELED:
                $status = 'Dự án bạn đã hủy';
                break;
            case Job::JOB_CONTEST_QUALIFIED:
                $status = 'Cuộc thi vòng 2';
                break;
            default :
        }
         return $status;
    }
    public static function getPaymentJob($status = 0) {
        switch ($status) {
            case Job::WAITING_ESCROW:
                $payment_status = 'Đang chờ nạp tiền đặt cọc';
                break;
            case Job::ESCROWED:
                $payment_status = 'Đã đặt cọc';
                break;
            case Job::PAIED:
                $payment_status = 'Đã chuyển tiền';
                break;
            case Job::REFUNED:
                $payment_status = 'Hoàn trả lại tiền';
                break;
            default:
        
        }
        return $payment_status;
    }
    
}
