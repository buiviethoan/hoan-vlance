<?php

namespace Vlance\BaseBundle\Utils;

class JobCalculator {
    
    /**
     * Tinh so gio con lai
     * @param DateTime $firstDate 
     * @param DateTime $secondDate
     */
    
    public static function convertHours($firstDate, $secondDate) {
        $firstDateTimeStamp = $firstDate->format("U");
        $secondDateTimeStamp = $secondDate->format("U");
        $rv = (($secondDateTimeStamp - $firstDateTimeStamp))/3600;
        if($rv < 0) {
            return false;
        } else {
            return $rv;
        }
    }

    public static function ago($date_finish, $date_start) {
        $diff = self::remain($date_finish, $date_start);
        $days = $diff->format('%R%a');
        $h = $diff->format('%R%h');
        $i = $diff->format('%R%i');
        // TODO: Dịch các text trong func này,
        // Có thể dùng cách sau http://stackoverflow.com/questions/9098363/symfony2-translation-parameter-not-working
        if($days < 0) {
            return false;
        }
        elseif($days == 0) {
            if($h > 0 && $i > 0) {
                return $diff->h.' giờ '.$diff->i.' phút'; // 2 giờ 10 phút
            }
            elseif($h > 0 && $i <= 0) {
                return $diff->h. ' giờ'; // 2 giờ
            }
            elseif($h <= 0 && $i > 0) {
                return $diff->i. 'phút'; // 10 phút
            }
            else {
                return false;
            }
            
        } else {
            if($h > 0) {
                return $diff->format('%a').' ngày '.$diff->h.' giờ'; // 22 ngày 3 giờ
            } else {
                return $diff->format('%a'). ' ngày'; // 22 ngày
            }
        }
        
    }
    
    public static function countDays($date_start) {
        $now = new \DateTime('now');
        $diff = self::remain($now, $date_start);
        $days = $diff->format('%R%a');
        $h = $diff->format('%R%h');
        $i = $diff->format('%R%i');
        
        if($days < 0) {
            return false;
        }
        elseif($days == 0) {
            if($h > 0 && $i > 0) {
                return $diff->h.' giờ '.$diff->i.' phút trước'; // 2 giờ 10 phút
            }
            elseif($h > 0 && $i <= 0) {
                return $diff->h. ' giờ trước'; // 2 giờ
            }
            elseif($h <= 0 && $i > 0) {
                return $diff->i. ' phút trước'; // 10 phút
            }
            else {
                return false;
            }
        } elseif($days == 1) {
            return 'Hôm qua';
        } else {
            return $date_start->format('d/m/Y');
        }
    }
    
    /**
     * Số ngày còn lại, trả về dưới dạng DateInterval object
     * 
     * @param \Vlance\BaseBundle\Utils\DateTime $date
     * @return \DateInterval
     */
    public static function remain($date_finish, $date_start){
        
        // Nếu date nhập vào không hợp lệ thì trả về 1 DateInterval bằng 0.
        if(!($date_finish instanceof \DateTime)){
            return new \DateInterval('P0D'); // P0D = Period 0 Day
        } else{
            return $date_start->diff($date_finish);
        }
    }
    
    /**
     * Số ngày còn lại, hiển thị dưới dạng text
     * 
     * @param \Vlance\BaseBundle\Utils\DateTime $date
     * @return string
     */
    public static function remainFormat($date){
        $diff = self::remain($date);

        // TODO: Dịch các text trong func này,
        // Có thể dùng cách sau http://stackoverflow.com/questions/9098363/symfony2-translation-parameter-not-working
        
        if($diff->y > 0){
            return $diff->y . " năm " . $diff->m . " tháng";  // 1 năm 2 tháng
        }
        elseif($diff->m > 0){
            return $diff->m . " tháng " . $diff->d . " ngày"; // 1 tháng 2 ngày
        }
        elseif($diff->d > 0){
            return $diff->d . " ngày " . $diff->h . " giờ";   // 1 ngày 2 giờ
        }
        elseif($diff->h > 0){
            return $diff->h . " giờ " . $diff->i . " phút";   // 6 giờ 20 phút
        }
        elseif($diff->i > 0){
            return $diff->i . " phút " . $diff->s . " giây";  // 15 phút 20 giây
        }
        elseif($diff->s > 0){
            return "dưới 1 phút";                             // dưới 1 phút
        }
        else{                   
            return "Đã hết hạn";                              // Đã hết hạn
        }
    }
    
    public static function cut($str, $len, $charset='UTF-8') {
        $str = html_entity_decode($str, ENT_QUOTES, $charset);
        if(mb_strlen($str, $charset)> $len){
            $arr = explode(' ', $str);
            $str = mb_substr($str, 0, $len, $charset);
            $arrRes = explode(' ', $str);
            $last = $arr[count($arrRes)-1];
            unset($arr);
            if(strcasecmp($arrRes[count($arrRes)-1], $last))
            {
                unset($arrRes[count($arrRes)-1]);
            }
            return implode(' ', $arrRes)."...";
        }
        return $str;

    }
    public static function replacement($string) {
        $patterns = array();
        //check tel
        $patterns[0] = '/\d([-\s]?\d){9,11}/';
        //check email
        $patterns[1] = '/([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})/';
        //check skype,yahoo...
//        $patterns[2] = '/[a-z][a-z0-9\.,\-_]{15,31}/';
        
        $replace = '[được ẩn bởi vlance]';
        $subject = preg_replace($patterns, $replace, $string);
        return $subject;
        
    }
    
     public static function report_abuse($string) {
        $patterns = array();
//        $patterns[0] = '/\d([-\s]?\d){6,11}/'; 
        $patterns[0] = '/\(?([0-9]{2,4})\)?([ .-]?)([0-9]{3})?([ .-]?)\2([0-9]{3,5})/';
        $patterns[1] = '/([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})/';
//        $patterns[2] = '/\b([khong|không]{4,})|([mot|một]{4,})|([hai]{4,})|([ba]{4,})|([bốn|bon]{4,})|([nam|năm]{4,})|([sáu|sau]{4,})|([bẩy|bảy|bay]{4,})|([tám|tam]{4,})|([chín|chin]{4,})\b/i';
//        $patterns[3] = '/((http?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?)|([a-zA-Z0-9]+\.){2,}/i';
//        $patterns[4] = '/(lien he)|(liên hệ)|(di động)|(điện thoại)/i';
//        $patterns[5] = '/\b(email)|(mail)|(viber)|(skype)|(zalo)|(ym)|(dd)|(dđ)|(tel)\b/i';
        $subject = false;
        
        foreach($patterns as $pattern) {
            if($subject == TRUE) {
               return TRUE; 
            }
            $subject = preg_match($pattern, $string);
        }
        return $subject;
    }
    
    /**
    * Create a web friendly URL slug from a string.
    *
    * Although supported, transliteration is discouraged because
    * 1) most web browsers support UTF-8 characters in URLs
    * 2) transliteration causes a loss of information
    *
    * @author Sean Murphy <sean@iamseanmurphy.com>
    * @copyright Copyright 2012 Sean Murphy. All rights reserved.
    * @license http://creativecommons.org/publicdomain/zero/1.0/
    *
    * @param string $str
    * @param array $options
    * @return string
    */
   public static function url_slug($str, $options = array()) {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
        $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => true,
        );
        // Merge options
        $options = array_merge($defaults, $options);
         $utf8 = array(
	            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'd'=>'đ|Đ',
	            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
	            'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
	            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
	            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
	            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
	            );
        // Transliterate characters to ASCII
        if ($options['transliterate']) {
                foreach($utf8 as $ascii=> $uni) {
                    $str = preg_replace("/($uni)/i", $ascii, $str);
                }
//            $str = str_replace(array_keys($char_map), $char_map, $str);
        }
        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
        // Remove delimiter from ends
        $str = trim($str, $options['delimiter']);
        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }
    public static function vlance_number_format($number, $decimals = 0, $dec_point = ",", $thousands_sep = ".") {
        $result = number_format($number, $decimals, $dec_point, $thousands_sep);
        return $result;
    }

}
