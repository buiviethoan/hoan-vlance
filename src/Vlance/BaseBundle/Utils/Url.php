<?php

namespace Vlance\BaseBundle\Utils;

class Url {

    /**
     * This is url with old format (separate with '-')
     * we will remove this when google indexed all new url
     * @param type $url
     * @return type
     */
    public static function pasteParameterOldFormat($url) {
        $params = array();
        $tmp = explode('-', $url);
        if (count($tmp) % 2 === 0) {
            for ($i = 0; $i < count($tmp); $i+=2) {
                if (strstr($tmp[$i + 1], ',')) {
                    $params[$tmp[$i]] = explode(',', $tmp[$i + 1]);
                } else {
                    $params[$tmp[$i]] = $tmp[$i + 1];
                }
            }
        }
        return $params;
    }
    
    public static function pasteParameter($url) {
        $params = array();
        $tmp = explode('_', $url);
        if (count($tmp) % 2 === 0) {
            for ($i = 0; $i < count($tmp); $i+=2) {
                if (strstr($tmp[$i + 1], ',')) {
                    $params[$tmp[$i]] = explode(',', $tmp[$i + 1]);
                } else {
                    $params[$tmp[$i]] = $tmp[$i + 1];
                }
            }
        }
        return $params;
    }

//    public static function buildUrl($params) {
//        $url = array();
//        foreach ($params as $key => $value) {
//            $url[] = $key;
//            if (is_array($value)) {
//                $url[] = implode(',', $value);
//            } else {
//                $url[] = $value;
//            }
//        }
//        return implode('-', $url);
//    }
    
    public static function buildUrl($params) {
        $url = array();
        foreach ($params as $key => $value) {
            $url[] = $key;
            if (is_array($value)) {
                $url[] = implode(',', $value);
            } else {
                $url[] = $value;
            }
        }
        return implode('_', $url);
    }
}
