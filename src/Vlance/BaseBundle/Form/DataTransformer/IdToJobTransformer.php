<?php

namespace Vlance\BaseBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class IdToJobTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function reverseTransform($id) {
        if (!$id) {
            return null;
        }
        
        $job = $this->om
            ->getRepository('VlanceJobBundle:Job')
            ->find($id)
        ;

        if (null === $job) {
            throw new TransformationFailedException(sprintf(
                'A job with id "%s" does not exist!',
                $id
            ));
        }

        return $job;
    }

    public function transform($job) {
        if (null === $job) {
            return "";
        }
        return $job->getId();
    }
}
