<?php

namespace Vlance\BaseBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class IdToAccountTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function reverseTransform($id) {
        if (!$id) {
            return new \Vlance\AccountBundle\Entity\Account();
        }
        
        $account = $this->om
            ->getRepository('VlanceAccountBundle:Account')
            ->find($id)
        ;

        if (null === $account) {
            throw new TransformationFailedException(sprintf(
                'An account with id "%s" does not exist!',
                $id
            ));
        }

        return $account;
    }

    public function transform($account) {
        if (null === $account) {
            return "";
        }
        return $account->getId();
    }
}
