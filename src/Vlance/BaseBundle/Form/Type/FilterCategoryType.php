<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Vlance\BaseBundle\Utils\Url;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FilterCategoryType extends AbstractType {
    
    private $repository;
    private $filters;
    private $router;
    private $route;
    private $call_func;
    
    public function __construct(EntityManager $em, Session $session, RouterInterface $router) {
        $this->repository = $em->getRepository('VlanceAccountBundle:Category');
        $this->filters = $session->get('filter');
        $this->route = $session->get('route');
        $this->call_func = $session->get(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC) 
                ? $session->get(\Vlance\AccountBundle\Entity\CategoryRepository::SESS_FUNC) : \Vlance\AccountBundle\Entity\CategoryRepository::FREELANCER_FUNC;
        $this->router = $router;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'choices' => array(),
            'multiple' => true,
            'expanded' => false,
        ));
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $cat_tmp = $this->repository->{$this->call_func}($this->filters);
        $categories = array();
        $sum_all = 0;
        foreach($cat_tmp as $cat) {
            $data_tmp = array();
            $data_tmp['title'] = $cat['title'];
//            $objs = $cat->{$this->call_func}();
            //Tinh tong so cong viec parent category
            if($cat[$this->call_func] > 0) {
                $data_tmp['num'] = $cat[$this->call_func];
            } else {
                $data_tmp['num'] = 0;
            }
            
            $sum_all += $data_tmp['num'];
            $tmp = $this->filters;
            $data_tmp['selected'] = (isset($tmp['cpath']) && $tmp['cpath'] == $cat['hash']) ? 'selected' : '';
            unset($tmp['chash']);
            $tmp['cpath'] = $cat['hash'];
            $data_tmp['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
            
            $data_tmp['sub'] = array();
            if($data_tmp['selected'] == 'selected') {
                $subcat = $this->repository->{$this->call_func}($this->filters, $cat['hash']);
                foreach($subcat as $sub) {
                    $tmp = $this->filters;
                    $data_tmp['sub'][$sub['hash']]['title'] = $sub['title'];
                    $data_tmp['sub'][$sub['hash']]['num'] = $sub[$this->call_func];
                    $idx = $this->isChecked($sub['hash']);
                    if($idx !== FALSE) {
                        if(is_array($tmp['chash'])) {
                            unset($tmp['chash'][$idx]);
                        } else {
                            unset($tmp['chash']);
                        }
                        $data_tmp['sub'][$sub['hash']]['checked'] = 'checked';
                    } else {
                        if (isset($tmp['chash'])) {
                            if (is_array($tmp['chash'])) {
                                $tmp['chash'][$sub['hash']] = $sub['hash'];
                            } else {
                                $tmp['chash'] = array($tmp['chash'], $sub['hash']);
                            }
                        } else {
                            $tmp['chash'] = $sub['hash'];
                        }
                        $data_tmp['sub'][$sub['hash']]['checked'] = ' ';
                    }
                    $data_tmp['sub'][$sub['hash']]['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
                }
                
            }
            
            $categories[$cat['hash']] = $data_tmp;
        }
        $tmp = $this->filters;
        $checked_all = 'selected';
        if(isset($tmp['chash'])) {
            unset($tmp['chash']);
            $checked_all = '';
        }
        if(isset($tmp['cpath'])) {
            unset($tmp['cpath']);
            $checked_all = '';
        }
        $view_all = array('title' => 'Tất cả', 'num' => $sum_all,'url' => $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp))), 'selected' => $checked_all);
        
        $view->vars = array_replace($view->vars, array(
            'categories' => $categories,
            'view_all' => $view_all,
        ));
        
//        foreach ($cat_tmp as $category) {
//            $data_tmp = array();
//            $data_tmp['title'] = $category->getTitle();
//            $data_tmp['is_top'] = ($category->getParent()) ? false : true;
//            $cat_parent = ($category->getParent() ? 0 : $category->getId());
//            $objs = $category->{$this->call_func}();
//            $data_tmp['num'] = $objs;
//            $tmp = $this->filters;
//            
//            if ($data_tmp['is_top']) {
//                $data_tmp['selected'] = (isset($tmp['cat.path']) && $tmp['cat.path'] == $category->getPath().'/%') ? 'selected' : ' ';
//                unset($tmp['cat.path']);
//                $tmp['cat.path'] = $category->getPath().'/%';
//                
//            } else {
//                $idx = $this->isChecked($category->getPath());
//                if ($idx !== FALSE) {
//                    if(is_array($tmp['cat.path'])) {
//                        unset($tmp['cat.path'][$idx]);
//                    } else {
//                        unset($tmp['cat.path']);
//                    }
//                    $data_tmp['checked'] = 'checked';
//                    $categories[$category->getParent()->getId()]['selected'] = 'selected';
//                } else {
//                    if (isset($tmp['cat.path'])) {
//                        if (is_array($tmp['cat.path'])) {
//                            $tmp['cat.path'][] = $category->getPath();
//                        } else {
//                            $tmp['cat.path'] = array($tmp['cat.path'], $category->getPath());
//                        }
//                    } else {
//                        $tmp['cat.path'] = $category->getPath();
//                    }
//                    $data_tmp['checked'] = ' ';
//                }
//            }
//            $data_tmp['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
//            $categories[$category->getId()] = $data_tmp;
//        }
//        
//        $view->vars = array_replace($view->vars, array(
//            'categories' => $categories,
//        ));
    }
    
    public function getName() {
        return 'filter_category';
    }
    
    public function getParent() {
        return 'choice';
    }
    
    protected function isChecked($category_hash) {
        if (isset($this->filters['chash'])) {
            if (is_array($this->filters['chash'])) {
                return array_search($category_hash, $this->filters['chash']);
            } else {
                return $category_hash == $this->filters['chash'];
            }
        } else {
            return false;
        }
    }
}