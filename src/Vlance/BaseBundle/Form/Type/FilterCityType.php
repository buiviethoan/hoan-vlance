<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Vlance\BaseBundle\Utils\Url;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FilterCityType extends AbstractType {
    
    private $repository;
    private $filters;
    private $router;
    private $route;
    private $call_func;
    
    public function __construct(EntityManager $em, Session $session, RouterInterface $router) {
        $this->repository = $em->getRepository('VlanceBaseBundle:City');
        $this->filters = $session->get('filter');
        $this->route = $session->get('route');
        $this->call_func = $session->get(\Vlance\BaseBundle\Entity\CityRepository::SESS_FUNC) ? $session->get(\Vlance\BaseBundle\Entity\CityRepository::SESS_FUNC) : \Vlance\BaseBundle\Entity\CityRepository::JOB_FUNC;
        $this->router = $router;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'choices' => array(),
            'multiple' => true,
            'expanded' => false,
        ));
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $cities_tmp = $this->repository->{$this->call_func}($this->filters);
        $cities = array();
        $cities['toan-quoc'] = array();
        
        $t = 0;
        foreach($cities_tmp as $city ) {
            $data_tmp = array();
            $data_tmp['title'] = $city['name'];
            $data_tmp['num'] = $city[$this->call_func];
            $t += $city[$this->call_func];
            $data_tmp['checked'] = "";
            $tmp = $this->filters;
            $idx = $this->isChecked($city['hash']);
            if($idx !== false) {
                if(is_array($tmp['city'])) {
                    unset($tmp['city'][$idx]);
                } else {
                    unset($tmp['city']);
                }
                $data_tmp['checked'] = 'checked';
            } else {
                if(isset($tmp['city'])) {
                    if(is_array($tmp['city'])) {
                        $tmp['city'][] = $city['hash'];
                    } else {
                        $tmp['city'] = array($tmp['city'],$city['hash']);
                    }
                } else {
                    $tmp['city'] = $city['hash'];
                }
                
            }
            $data_tmp['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
            $cities[$city['hash']] = $data_tmp;
        }
        
        $tmp = $this->filters;
        $check_all = 'checked';
        if(isset($tmp['city'])) {
            unset($tmp['city']);
            $check_all = '';
        }
        $cities['toan-quoc'] = array('title' => 'Toàn quốc', 'num' => 1, 'checked' => $check_all, 'url' => $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp))));
        $view->vars = array_replace($view->vars, array(
            'cities' => $cities,
        ));

    }
    
    public function getName() {
        return 'filter_city';
    }
    
    public function getParent() {
        return 'choice';
    }
    
    protected function isChecked($chash) {
        if (isset($this->filters['city'])) {
            if (is_array($this->filters['city'])) {
                return array_search($chash, $this->filters['city']);
            } else {
                return $chash == $this->filters['city'];
            }
        } else {
            return false;
        }
    }
}