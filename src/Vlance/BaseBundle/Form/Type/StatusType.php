<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vlance\JobBundle\Entity\Job;


class StatusType extends AbstractType
{
    public function getName()
    {
        return 'status';
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'choices' => array(
                Job::STATUS_ENABLE => ''
            )
        ));
    }
}