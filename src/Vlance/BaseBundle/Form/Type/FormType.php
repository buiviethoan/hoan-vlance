<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\FormType as BaseType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;


class FormType extends BaseType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'prepend'       => null,
            'append'        => null,
            'wrapper_class' => null,
        ));
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array(
            'prepend' => $options['prepend'],
            'append'    => $options['append'],
            'wrapper_class'    => $options['wrapper_class'],
        ));
    }
}
