<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Vlance\BaseBundle\Utils\Url;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FilterSkillType extends AbstractType {
    
    private $repository;
    private $filters;
    private $router;
    private $route;
    private $call_func;
    
    public function __construct(EntityManager $em, Session $session, RouterInterface $router) {
        $this->repository = $em->getRepository('VlanceAccountBundle:Skill');
        $this->filters = $session->get('filter');
        $this->route = $session->get('route');
        $this->call_func = $session->get(\Vlance\AccountBundle\Entity\SkillRepository::SESS_FUNC) ? $session->get(\Vlance\AccountBundle\Entity\SkillRepository::SESS_FUNC) : \Vlance\AccountBundle\Entity\SkillRepository::JOB_FUNC;
        $this->router = $router;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'choices' => array(),
            'multiple' => true,
            'expanded' => false,
        ));
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $skills_tmp = $this->repository->{$this->call_func}($this->filters);
        $skills = array();
        $skills['tat-ca'] = array();
        foreach($skills_tmp as $skill ) {
            $data_tmp = array();
            $data_tmp['title'] = $skill['title'];
            $data_tmp['num'] = $skill[$this->call_func];
            $data_tmp['checked'] = "";
            $tmp = $this->filters;
            $idx = $this->isChecked($skill['hash']);
            if($idx !== false) {
                if(is_array($tmp['kynang'])) {
                    unset($tmp['kynang'][$idx]);
                } else {
                    unset($tmp['kynang']);
                }
                $data_tmp['checked'] = 'checked';
            } else {
                if(isset($tmp['kynang'])) {
                    if(is_array($tmp['kynang'])) {
                        $tmp['kynang'][] = $skill['hash'];
                    } else {
                        $tmp['kynang'] = array($tmp['kynang'],$skill['hash']);
                    }
                } else {
                    $tmp['kynang'] = $skill['hash'];
                }
                
            }
            $data_tmp['url'] = $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp)));
            $skills[$skill['hash']] = $data_tmp;
        }
        
        $tmp = $this->filters;
        $check_all = 'checked';
        if(isset($tmp['kynang'])) {
            unset($tmp['kynang']);
            $check_all = '';
        }
        $skills['tat-ca'] = array('title' => 'Tất cả', 'num' => 1, 'checked' => $check_all, 'url' => $this->router->generate($this->route, array('filters' => Url::buildUrl($tmp))));
        $view->vars = array_replace($view->vars, array(
            'skills' => $skills,
        ));

    }
    
    public function getName() {
        return 'filter_skill';
    }
    
    public function getParent() {
        return 'choice';
    }
    
    protected function isChecked($chash) {
        if (isset($this->filters['kynang'])) {
            if (is_array($this->filters['kynang'])) {
                return array_search($chash, $this->filters['kynang']);
            } else {
                return $chash == $this->filters['kynang'];
            }
        } else {
            return false;
        }
    }
}