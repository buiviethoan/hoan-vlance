<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Vlance\BaseBundle\Form\DataTransformer\IdToJobTransformer;

class JobPickerType extends AbstractType
{
    protected $_em;
    
    public function __construct(EntityManager $em) {
        $this->_em = $em;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
        $resolver->setRequired(array('class', 'builder', 'matcher'));
        $resolver->setOptional(array('em', 'primaryKey', 'multiple'));
    }
    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $transformer = new IdToJobTransformer($this->_em);
        $builder->addModelTransformer($transformer);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $jobConditions = array();
        $jobConditions['publish'] = array('eq', '1');
        $jobs = $this->_em->getRepository('VlanceJobBundle:Job')->getFields(array('j.id', 'j.title'), $jobConditions);
//        echo count($accounts);die;
        $view->vars = array_replace($view->vars, array('jobs' => $jobs, 'builder' => $options['builder']));
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'jobpicker';
    }
    
    public function getParent() {
        return 'text';
    }
}
