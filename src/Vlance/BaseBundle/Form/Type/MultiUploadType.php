<?php

namespace Vlance\BaseBundle\Form\Type;

use Vlance\BaseBundle\Form\DataTransformer\MultiuploadTransformer;
use Vlance\BaseBundle\EventListener\MultiUploadListener;
use Vlance\BaseBundle\Entity\File as BaseFile;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MultiUploadType extends AbstractType {
    
    protected $om;
    
    public function __construct(ObjectManager $om) {
        $this->om = $om;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['allow_add'] && $options['prototype']) {
            $prototype = $builder->create($options['prototype_name'], $options['type'], array_replace(array(
                'label' => $options['prototype_name'].'label__',
            ), $options['options']));
            $builder->setAttribute('prototype', $prototype->getForm());
        }

        $resizeListener = new MultiUploadListener(
            $options['type'],
            $options['options'],
            $options['allow_add'],
            $options['allow_delete'],
            $this->om
        );

        $builder->addEventSubscriber($resizeListener);
        
//        $transformer = new MultiuploadTransformer($this->om, $options['class']);
//        $builder->addViewTransformer($transformer);
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'type' => new BaseFile(),
            'prototype' => true,
            'allow_add' => true,
            'allow_delete' => true
        ));
    }
    
    public function getName() {
        return 'multiupload';
    }
    
    public function getParent() {
        return 'collection';
    }
}