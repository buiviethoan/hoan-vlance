<?php

namespace Vlance\BaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;
use Vlance\BaseBundle\Utils\Url;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Vlance\BaseBundle\Form\DataTransformer\IdToAccountTransformer;

class AccountPickerType extends AbstractType
{
    protected $_em;
    
    public function __construct(EntityManager $em) {
        $this->_em = $em;
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
        $resolver->setRequired(array('class', 'builder', 'matcher'));
        $resolver->setOptional(array('em', 'primaryKey', 'multiple'));
    }
    
    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options) {
        $transformer = new IdToAccountTransformer($this->_em);
        $builder->addModelTransformer($transformer);
    }
    
    public function buildView(FormView $view, FormInterface $form, array $options) {
        parent::buildView($view, $form, $options);
        $accountConditions = array();
        $accountConditions['type'] = array('in', array("freelancer", "client"));
        $accountConditions['enabled'] = array('eq', '1');
        $accounts = $this->_em->getRepository('VlanceAccountBundle:Account')->getFields(array('a.id', 'a.email', 'a.fullName'), $accountConditions);
//        echo count($accounts);die;
        $view->vars = array_replace($view->vars, array('accounts' => $accounts, 'builder' => $options['builder']));
    }
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'accountpicker';
    }
    
    public function getParent() {
        return 'text';
    }
}
