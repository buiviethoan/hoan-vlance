<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Aws\Sns\SnsClient;

class ErrorController extends Controller {

    /**
     * To use this action, in other action use code:
     * <code>
     * return $this->redirect($this->generateUrl("500_page"), 301);
     * </code>
     * @Route("/error-way.html", name="500_page")
     * @Template("VlanceBaseBundle:Error:error500.html.php", engine="php")
     */
    public function erro500Action() {
        return array();
    }

        /**
     * To use this action, in other action use code:
     * <code>
     * return $this->redirect($this->generateUrl("500_page"), 301);
     * </code>
     * @Route("/error-way.html", name="503_page")
     * @Template("VlanceBaseBundle:Error:error503.html.php", engine="php")
     */
    public function erro503Action() {
        return array();
    }
    
    /**
     * To use this action, in other action use code:
     * <code>
     * return $this->redirect($this->generateUrl("404_page"), 301);
     * </code>
     * @Route("/wrong-way.html", name="404_page")
     * @Template("VlanceBaseBundle:Error:error404.html.php", engine="php")
     */
    public function erro404Action() {
        return array();
    }

    /**
     *
     * @Route("/sns-subcribe", name="sns_subcribe")
     * @Template()
     */
    public function snsSubcribeAction() {
        $snsConfig = array(
            'key' => 'AKIAI7SFZ4QKWICGO7IQ',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        $subcribeData = array(
            // TopicArn is required
            'TopicArn' => 'arn:aws:sns:us-east-1:536080403937:vlance-ses-bounces-topic',
            // Protocol is required
            'Protocol' => 'http',
            'Endpoint' => 'http://dev.vlance.vn/sns-bounces',
        );

        $snsClient = SnsClient::factory($snsConfig);
        try {
            $result = $snsClient->subscribe($subcribeData);
        } catch (\Exception $ex) {
            return new Response($ex->getMessage());
        }
        return new Response(print_r($result, true));
    }

    /**
     *
     * @Route("/sns-bounces", name="sns_bundle")
     * @Template()
     */
    public function snsBouncesAction(Request $request) {
        $logger = $this->get("monolog.logger.sns");
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger->info(print_r($data, true));
        $snsConfig = array(
            'key' => 'AKIAI7SFZ4QKWICGO7IQ',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        $message = json_decode($data['Message'], true);
        if ($data['Type'] == 'SubscriptionConfirmation') {
            $confirmationData = array(
                // TopicArn is required
                'TopicArn' => $data['TopicArn'],
                // Token is required
                'Token' => $data['Token'],
            );
            $snsClient = SnsClient::factory($snsConfig);
            try {
                $snsClient->confirmSubscription($confirmationData);
            } catch (\Exception $ex) {
                return new Response("subscribe error");
            }
        } elseif (isset($message['notificationType']) && $message['notificationType'] == 'Bounce') {
            $email = $message['mail']['destination'][0];
            $em = $this->getDoctrine()->getManager();
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($email);
            if ($account) {
                // Temporarily disabled
                /*$account->setEmailVerified(false);
                $em->persist($account);
                $em->flush();
                $logger->error("Email %email% is set to un-verified", array('%email%' => $email));*/
            } else {
                $messageId = $message['mail']['messageId'];
                /* @var $email_invite \Vlance\AccountBundle\Entity\EmailInvite */
                $email_invite = $em->getRepository('VlanceAccountBundle:EmailInvite')->findOneBy(array('email' => $email, 'messageId' => $messageId));
                if ($email_invite) {
                    $email_invite->setVerified(\Vlance\AccountBundle\Entity\EmailInvite::EMAIL_INVITE_ERROR);
                    $em->persist($email_invite);
                    $em->flush();
                }
                $logger->error("Email %email% does not exist in system vLance.vn", array('%email%' => $email));
            }
        }
        return new Response("");
    }

    /**
     *
     * @Route("/sns-complains", name="sns_complains")
     * @Template()
     */
    public function snsComplainsAction(Request $request) {
        $logger = $this->get("monolog.logger.sns");
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger->info(print_r($data, true));
        $snsConfig = array(
            'key' => 'AKIAI7SFZ4QKWICGO7IQ',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        if ($data['Type'] == 'SubscriptionConfirmation') {
            $confirmationData = array(
                // TopicArn is required
                'TopicArn' => $data['TopicArn'],
                // Token is required
                'Token' => $data['Token'],
            );
            $snsClient = SnsClient::factory($snsConfig);
            try {
                $snsClient->confirmSubscription($confirmationData);
            } catch (\Exception $ex) {
                return new Response("subscribe error");
            }
        } elseif (isset($data['notificationType']) && $data['notificationType'] == 'Complaint') {
            $email = $data['complaint']['complainedRecipients']['emailAddress'];
            $em = $this->getDoctrine()->getManager();
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($email);
            if ($account) {
                $account->setEmailVerified(false);
                $em->persist($account);
                $em->flush();
                $logger->error("Email %email% is set to un-verified", array('%email%' => $email));
            } else {
                $logger->error("Email %email% does not exist", array('%email%' => $email));
            }
        }
        return new Response("");
    }

    /**
     *
     * @Route("/sns-delivery", name="sns_delivery")
     * @Template()
     */
    public function snsDeliveryAction(Request $request) {
        $logger = $this->get("monolog.logger.sns");
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
        /* @var $logger \Psr\Log\LoggerInterface */
        $logger->info(print_r($data, true));
        $snsConfig = array(
            'key' => 'AKIAI7SFZ4QKWICGO7IQ',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        $message = json_decode($data['Message'], true);
        if ($data['Type'] == 'SubscriptionConfirmation') {
            $confirmationData = array(
                // TopicArn is required
                'TopicArn' => $data['TopicArn'],
                // Token is required
                'Token' => $data['Token'],
            );
            $snsClient = SnsClient::factory($snsConfig);
            try {
                $snsClient->confirmSubscription($confirmationData);
            } catch (\Exception $ex) {
                return new Response("subscribe error");
            }
        } elseif (isset($message['notificationType']) && $message['notificationType'] == 'Delivery') {
                $email = $message['delivery']['recipients'][0];
                $messageId = $message['mail']['messageId'];
                $em = $this->getDoctrine()->getManager();
                /* @var $email_invite \Vlance\AccountBundle\Entity\EmailInvite */

                $email_invite = $em->getRepository('VlanceAccountBundle:EmailInvite')->findOneBy(array('email' => $email, 'messageId' => $messageId));
            if ($email_invite) {
                $email_invite->setVerified(\Vlance\AccountBundle\Entity\EmailInvite::EMAIL_INVITE_SENT);
                $em->persist($email_invite);
                //increase 1 bid for account invite 
                /* @var $account_invite \Vlance\AccountBundle\Entity\Account */
                $account_invite = $email_invite->getAccount();
                $max = $this->container->getParameter('max_email_invite');
                if($account_invite->getMonthlyBids() < $max) {
                    $account_invite->setNumCurrBids($account_invite->getNumCurrBids() + 1);
                    $account_invite->setMonthlyBids(($account_invite->getMonthlyBids() + 1));
                    $em->persist($account_invite);
                }
                
                $em->flush();
                $logger->info("email Delivery");
            } 

        }
        $logger->info("handle delivery notification ok");
        return new Response("");
    }

    /**
     * /!\ NOTE: This function must be at the end of this controller
     * This action to handle all unknow url -> redirect or display a 404 page
     * See app/config/routing.yml to know how to define a route
     * 
     * @Route("/{anyRoute}", name="error_index", requirements={"anyRoute" = ".+"})
     * @Template("VlanceBaseBundle:Error:error404.html.php", engine="php")
     */
    public function indexAction(Request $request, $anyRoute = "") {
        
    }

}
