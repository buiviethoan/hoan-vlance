<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Vlance\JobBundle\Entity\Job;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Partner controller.
 *
 * @Route("")
 */
class PartnerController extends Controller
{
    /**
     * @Route("/api/partner/topcv", name="api_partner_topcv")
     * @Template(engine="php")
     */
    public function topcvAction(Request $request)
    {
        @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=get_jobs&el=get_jobs&cs=web&cm=none&cn=none');
        
        $parameters = $this->container->getParameter("vlance_system");
        $api_key = $parameters['api']['partner']['topcv']['api_key'];
        
        if(!($request->request->get('query'))){
            return new JsonResponse(array(
                'error' => 100,
                'errorMessage'  => "Query không hợp lệ"
            ));
        }
        $query = json_decode($request->request->get('query'), true);
        
        if(!isset($query['api_key'])){
            @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=error_send_job&el=101&cs=web&cm=none&cn=none');
            return new JsonResponse(array(
                'error' => 101,
                'errorMessage'  => "Thiếu API Key"
            ));
        }
        
        if((string)($query['api_key']) != (string)($api_key)){
            @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=error_send_job&el=102&cs=web&cm=none&cn=none');
            return new JsonResponse(array(
                'error' => 102,
                'errorMessage'  => "API Key không hợp lệ"
            ));
        }
        
        if(!isset($query['skills'])){
            @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=error_send_job&el=103&cs=web&cm=none&cn=none');
            return new JsonResponse(array(
                'error' => 103,
                'errorMessage'  => "Skills không hợp lệ"
            ));
        }
        
        if(count($query['skills']) <= 0){
            @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=error_send_job&el=104&cs=web&cm=none&cn=none');
            return new JsonResponse(array(
                'error' => 104,
                'errorMessage'  => "Cần tối thiểu 1 skill"
            ));
        }
        
        $em = $this->getDoctrine()->getManager();
        $skills = $query['skills'];
        foreach($skills as $key => $skill){
            $skill_obj = $em->getRepository('VlanceAccountBundle:Skill')->findOneBy(array('hash' => strtolower($skill)));
            if(!$skill_obj){
                unset($skills[$key]);
            }
        }
        
        if(count($skills) <= 0){
            @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=error_send_job&el=105&cs=web&cm=none&cn=none');
            return new JsonResponse(array(
                'error' => 105,
                'errorMessage'  => "Cần tối thiểu 1 skill"
            ));
        }
        
        $last_seven_days = date('Y-m-d', time() - 7 * 24 * 60 * 60);
        $case_when = "";
        foreach($skills as $skill){
            $case_when .= ($case_when == "" ? "" : " OR ") . 's.hash="' . strtolower($skill) . '"';
        }
        
        $sql_query =  'SELECT j.id, j.title, j.created_at,  SUM(CASE WHEN ' . $case_when . ' THEN 1 ELSE 0 END) AS rel '
                    . 'FROM job j '
                    . 'LEFT JOIN skill_job sj ON j.id = sj.job_id '
                    . 'LEFT JOIN skill s ON s.id = sj.skill_id '
                    . 'WHERE j.publish_option = 1 AND j.is_private = 0 AND j.created_at > "' . $last_seven_days .'" '
                    . 'GROUP BY j.id HAVING rel > 0 '
                    . 'ORDER BY rel DESC, j.id DESC limit 10;';
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('Vlance\JobBundle\Entity\Job', 'j');
        $rsm->addFieldResult('j', 'id', 'id');
        $rsm_query = $em->createNativeQuery($sql_query, $rsm);
        $job_ids = $rsm_query->getArrayResult();

        $job_array = array();
        foreach ($job_ids as $j){
            /* @var $job Job */
            $job = $em->getRepository('VlanceJobBundle:Job')->findOneBy(array('id' => $j['id']));
            $skill_set = array();
            foreach($job->getSkills() as $s){
                $skill_set[] = $s->getTitle();
            }
            $job_array[] = array(
                'id'            => $job->getId(),
                'title'         => $job->getTitle(),
                'description'   => $job->getDescription(),
                'budget'        => $job->getBudget(),
                'close_at'      => $job->getCloseAt()->getTimestamp(),
                'url'           => $this->generateUrl('job_show_freelance_job', array('hash' => $job->getHash()), UrlGeneratorInterface::ABSOLUTE_URL) . "?ref=topcv",
                'skills'        => $skill_set
            );
        }

        @file_get_contents('http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=partner_topcv&ea=send_job&el=' . count($job_array) . '_jobs&cs=web&cm=none&cn=none');
        
        return new JsonResponse(array(
            'error' => 0,
            'jobs'  => $job_array
        ));
    }
    
    /**
     * @Route("/partner/moneylover/gift", name="partner_moneylover_gift")
     * @Template("VlanceBaseBundle:Partner/Moneylover:gift.html.php", engine="php")
     */
    public function moneyloverGiftAction(Request $request)
    {
        // Track ref
        $auth_user = $this->get('security.context')->getToken()->getUser();
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "partner_moneylover_gift",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }

        $em = $this->getDoctrine()->getManager();
        if(is_object($auth_user)){
            if($auth_user->getId()){
                $user = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $auth_user->getId()));
                $code = $em->getRepository('VlanceBaseBundle:PartnerMoneyloverCode')->findOneBy(array('account' => $user));
            }
        }
        
        return array(
            'code'  => isset($code) ? $code : null
        );
    }
    
    /**
     * @Route("/partner/accesstrade", name="partner_accesstrade")
     * @Template("VlanceBaseBundle:Partner:accesstrade.html.php", engine="php")
     */
    public function accesstradeAction(Request $request)
    {
        
        return array();
    }
}
