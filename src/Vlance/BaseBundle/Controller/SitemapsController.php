<?php
namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\JobBundle\Entity\Job;
use Vlance\AccountBundle\Entity\Account;
use Symfony\Component\HttpFoundation\Response;

class SitemapsController extends Controller
{
        /**
     * @Route("/rss.xml", name="projects_rss_feed")
     * @Template("VlanceBaseBundle:Sitemaps:rss.xml.twig")
     
     */
    public function rssAction() 
    {
        $em = $this->getDoctrine()->getManager();
        $hostname = $this->getRequest()->getHost();

        // urls from job
        $jobs = $em->getRepository('VlanceJobBundle:Job')->getFields(
                'j.hash hash, j.createdAt createdAt, j.title title, j.description description, j.id id', 
                array('publish' => array('0' => 'eq','1' => Job::PUBLISH), 'isPrivate' => array('eq', '0')), 
                array('createdAt' => 'DESC'),
                10);
        
        $list = array();
        foreach ($jobs as $job) {
            $list[] = array(
                'loc' => $this->generateUrl('job_show_freelance_job', array('hash' => $job['hash'])), 
                'createdAt' => $job['createdAt']->format('r'), 
                'title' => $job['title'], 
                'id' => $job['id'],
                'description' => $job['description'], 
                'priority' => '0.7'
            ); 
        }
        
        $response = new Response;
        $content_xml = $this->render('VlanceBaseBundle:Sitemaps:rss.xml.twig', array('date' => date('r'), 'jobs' => $list, 'hostname' => $hostname));
        $response->headers->set('Content-Type', 'text/xml');
        $content = $content_xml->getContent();
            
        $response->setContent($content);
        return $response;
    }

    /**
     * @Route("/sitemap.{_format}", name="sample_sitemaps_sitemap", Requirements={"_format" = "xml.gz"})
     * @Template("VlanceBaseBundle:Sitemaps:sitemap.xml.twig")
     
     */
    public function sitemapAction($_format = "xml.gz") 
    {
        $em = $this->getDoctrine()->getManager();
        
        $urls = array();
        $hostname = $this->getRequest()->getHost();

        // add some urls homepage
        $urls[] = array('loc' => $this->get('router')->generate('vlance_homepage'), 'lastmod'=>date("c"), 'changefreq' => 'daily', 'priority' => '1.0');
        
        // urls seo page for category
        $week = date('W');
        $year = date('Y');
        $date1 = date('c', strtotime($year."W".$week));
        foreach ($em->getRepository('VlanceAccountBundle:Category')->findBy(array('online' => true)) as $category) {
            $urls[] = array('loc' => $this->get('router')->generate('category_show',array('seoUrl' => $category->getSeoUrl())), 'lastmod'=>$date1, 'changefreq' => 'daily', 'priority' => '0.9');
        }
        
        //list job
        $urls[] = array('loc' => $this->generateUrl('freelance_job_list'), 'lastmode' => date("c"), 'changefreq' => 'daily', 'priority' => 0.9);
        $urls[] = array('loc' => $this->generateUrl('contest_list'), 'lastmode' => date("c"), 'changefreq' => 'daily', 'priority' => 0.9);
            
        //list account
        $urls[] = array('loc' => $this->generateUrl('freelancer_list'), 'lastmode' => date("c"), 'changefreq' => 'daily', 'priority' => 0.9);

        //registry
        $urls[] = array('loc' => $this->generateUrl('account_register'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);

        // urls cms page
        foreach ($em->getRepository('VlanceCmsBundle:Page')->findAll() as $expert) {
            $urls[] = array('loc' => $this->get('router')->generate('cms_page',array('slug' => $expert->getUrl())), 'lastmod'=>$expert -> getCreatedAt() -> format('c'), 'changefreq' => 'monthly', 'priority' => '0.9');
        }
        
        // urls from job Update 20160705
        $services = $em->getRepository('VlanceAccountBundle:Service')->getFields('s.seoUrl seoUrl, s.updatedAt updatedAt', array('online' => array('eq', '1')));
        foreach ($services as $expert) {
            $urls[] = array('loc' => $this->generateUrl('service_landing_page', array('seoUrl' => $expert['seoUrl'])), 'lastmod' => $expert['updatedAt'] ->format('c'), 'changefreq' => 'weekly', 'priority' => '0.7'); 
        }
        
        // urls from job Update 20160705
        $jobs = $em->getRepository('VlanceJobBundle:Job')->getFields('j.hash hash, j.updatedAt updatedAt', array('publish' => array('0' => 'eq','1' => Job::PUBLISH), 'isPrivate' => array('eq', '0'), 'type' => array('eq', Job::TYPE_BID)));
        foreach ($jobs as $expert) {
            $urls[] = array('loc' => $this->generateUrl('job_show_freelance_job', array('hash' => $expert['hash'])), 'lastmod' => $expert['updatedAt'] ->format('c'), 'changefreq' => 'weekly', 'priority' => '0.7'); 
        }
        
        // urls from contest Update 20160705
        $contests = $em->getRepository('VlanceJobBundle:Job')->getFields('j.hash hash, j.updatedAt updatedAt', array('publish' => array('0' => 'eq','1' => Job::PUBLISH), 'isPrivate' => array('eq', '0'), 'type' => array('eq' , Job::TYPE_CONTEST)));
        foreach ($contests as $expert) {
            $urls[] = array('loc' => $this->generateUrl('job_contest_view', array('hash' => $expert['hash'])), 'lastmod' => $expert['updatedAt'] ->format('c'), 'changefreq' => 'weekly', 'priority' => '0.7'); 
        }
        
        // urls from freelancer
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->getFields('a.hash hash, a.updatedAt updatedAt', array('enabled' => array('0' => 'eq','1' => true)));
        foreach ($accounts as $expert) {
            $urls[] = array('loc' => $this->generateUrl('account_show_freelancer', array('hash' => $expert['hash'])), 'lastmod' => $expert['updatedAt'] ->format('c'), 'changefreq' => 'weekly', 'priority' => '0.7'); 
        }
        
        // url hướng dẫn sử dụng Update 20160705
        $urls[] = array('loc' => $this->generateUrl('huong_dan_thue_freelancer'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_dang_viec'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_chon_freelancer'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_nap_tien'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_quan_ly_du_an'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_ket_thuc_du_an'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        $urls[] = array('loc' => $this->generateUrl('huong_dan_lien_he_truc_tiep_freelancer'), 'lastmode' => date("c"), 'changefreq' => 'monthly', 'priority' => 0.5);
        
        $response = new Response;
        $content_xml = $this->render('VlanceBaseBundle:Sitemaps:sitemap.xml.twig', array('urls' => $urls, 'hostname' => $hostname));
        
        if($_format == "xml.gz"){
            $response->headers->set('Content-Type', 'application/gzip');
            $content = gzencode($content_xml->getContent(), 9);
        } else{
            $response->headers->set('Content-Type', 'application/xml');
            $content = $content_xml->getContent();
        }
        $response->setContent($content);
        return $response;
    }
}