<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\Query\ResultSetMapping;
use Vlance\BaseBundle\Entity\MassEmail;

/**
 * Mass Email controller.
 *
 * @Route("/mass_email")
 */
class MassEmailController extends Controller
{
    /**
     * @Route("/send_preview/{id}", name="admin_mass_email_send_preview")
     * @Template(engine="php")
     */
    public function adminSendPreviewAction(Request $request, $id)
    {        
        //Check if it is Admin
        if (false === $this->get('security.context')->isGranted('ROLE_VLANCE_SUPER_ADMIN')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        if(!isset($id) || !is_numeric($id)){
            $this->get('session')->getFlashBag()->add('error', "Mass Email campagin not found");
            return $this->redirect('/admin/AdminMassEmail');
        }
        
        // Retrieve the MassEmail campaign
        $em = $this->getDoctrine()->getManager();
        /* @var $campaign \Vlance\BaseBundle\Entity\MassEmail */
        $campaign = $em->getRepository('VlanceBaseBundle:MassEmail')->findOneBy(array('id' => $id));
        
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('VlanceAccountBundle:Account', 'a');
        $rsm->addFieldResult('a', 'id', 'id');

        $query = $em->createNativeQuery($campaign->getReceiverQuery(), $rsm);
        $result = $query->getArrayResult(); // Return 2 dimensions array
        $receipt_ids = array_map('current', $result); // Flatten the 2 dimensions array
        
        require_once($this->container->getParameter('kernel.root_dir') . '/../src/Vlance/Lib/Mixpanel/lib/Mixpanel.php');
        $parameters = $this->container->getParameter("vlance_system");
        $mixpanel = \Mixpanel::getInstance($parameters['mixpanel_token']['prod']);
        
        $template = 'VlanceBaseBundle:Email/massemail:content.html.twig';
        $receipts = $em->getRepository('VlanceAccountBundle:Account')->findBy(array('id' => $receipt_ids));
        $counter = (int)(is_null($campaign->getSentSuccessCounter()) ? 0 : $campaign->getSentSuccessCounter());
        $campaign->setSentAt(new \DateTime());
        
        foreach($receipts as $receipt){
            $context = array(
                'receipt'       => $receipt,
                'subject'       => $campaign->getHeader(),
                'content'       => $campaign->getHtmlContent(),
                'tracking_id'   => $campaign->getTrackingId(),
                'id'            => $campaign->getId(),
                'pixel_tracking' => base64_encode(json_encode(
                        array(
                            'event' => "MassEmail Open",
                            'properties' => array(
                                'token' => $parameters['mixpanel_token']['prod'],
                                'distinct_id' => $receipt->getId(),
                                'campaign'  => $campaign->getTrackingId()
                            )
                        )
                    )),
            );
            $from = $campaign->getFromEmail();
            // $to = $receipt->getEmail();   
            $to = "tuan.tran@vlance.vn";
            $this->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from, $to);
            
            $mixpanel->identify($receipt->getId());
            $mixpanel->track('MassEmail Sent', array('campaign'  => $campaign->getTrackingId()));
            $counter++;
            
            $campaign->setSentSuccessCounter($counter);
            $em->persist($campaign);
            $em->flush();
            break;
        }
        
        return $this->redirect('/admin/AdminMassEmail');
    }
}
