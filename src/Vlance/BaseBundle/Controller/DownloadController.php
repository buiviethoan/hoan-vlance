<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class DownloadController extends Controller {

    /**
     * @Route("/d/j/{job_id}/{file_id}", name="download_job")
     */
    public function downloadJobAction($job_id, $file_id = 0) {
        if (!$job_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        if (!$file_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('VlanceJobBundle:JobFile')->find($file_id);
        if($file->getJob()->getId() != $job_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $path = $file->getUploadDir().DS.$file->getPath().DS.$file->getStoredName();
        $filename = $file->getFilename();
        if(file_exists($path)) {
            $content = file_get_contents($path);
            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

            $response->setContent($content);
            return $response;
        }
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.download.not_exit', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_show', array('id' => $job_id)));
        
    }
    
    /**
     * @Route("/d/bid/{bid_id}/{file_id}", name="download_bid")
     */
    public function downloadBidAction($bid_id = 0, $file_id = 0) {
        if (!$bid_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        if (!$file_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('VlanceJobBundle:BidFile')->find($file_id);
        if($file->getBid()->getId() != $bid_id) {
             $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $path = $file->getUploadDir().DS.$file->getPath().DS.$file->getStoredName();
        $filename = $file->getFilename();
        if(file_exists($path)) {
            $content = file_get_contents($path);


            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

            $response->setContent($content);
            return $response;
        }
        $bid_entity = $em->getRepository('VlanceJobBundle:Bid')->find($bid_id);
        if (!$bid_entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $job_id = $bid_id->getJob()->getId();
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.download.not_exit', array(), 'vlance'));
        return $this->redirect($this->generateUrl('job_show', array('id' => $job_id)));
        
    }
    
    
    /**
     * @Route("/file/{encoded_url}", name="download_file_encoded")
     */
    public function downloadBidEncodedAction($encoded_url = 0) {
        return $this->redirect(base64_decode($encoded_url), 302);
    }
    
    
    /**
     * @Route("/d/message/{message_id}/{file_id}", name="download_message")
     */
    public function downloadMessageAction($message_id = 0, $file_id = 0) {
        if (!$message_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        if (!$file_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('VlanceJobBundle:MessageFile')->find($file_id);
        if($file->getMessage()->getId() != $message_id) {
             $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $path = $file->getUploadDir().DS.$file->getPath().DS.$file->getStoredName();
        $filename = $file->getFilename();
        if(file_exists($path)) {
            $content = file_get_contents($path);
        
            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

            $response->setContent($content);
            return $response;
        }
        /* Neu file khong ton tai thong bao cho nguoi dung */
        $message_entity = $em->getRepository('VlanceJobBundle:Message')->find($message_id);
        if(!$message_entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $bid_entity = $message_entity->getBid();
        $job_id = $bid_entity->getJob()->getId();
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.download.not_exit', array(), 'vlance'));
        return $this->redirect($this->generateUrl('workshop_bid', array('job_id' => $job_id, 'bid' => $bid_entity->getId())));
        
    }
    /**
     * @Route("/d/portfolio/{portfolio_id}", name="download_portfolio")
     */
    public function downloadPortfolioAction($portfolio_id = 0) {
        if (!$portfolio_id) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository('VlanceAccountBundle:Portfolio')->find($portfolio_id);
        if(!$file) {
             $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        if(!$file->getStoredName()){
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('download.file.not_set', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $path = $file->getUploadDir().DS.$file->getPath().DS.$file->getStoredName();
        $filename = $file->getFilename();
        if(file_exists($path)) {
            $content = file_get_contents($path);

            $response = new Response();
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

            $response->setContent($content);
            return $response;
        }
        $account = $file->getAccount();
        if(!$account) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.download.not_exit', array(), 'vlance'));
        return $this->redirect($this->generateUrl('account_show', array('id' => $account->getId())));
    }

}
