<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Vlance\BaseBundle\Utils\HasPermission;
use Vlance\AccountBundle\Entity\Account;
use Vlance\JobBundle\Entity\Job;
use Vlance\BaseBundle\Utils\JobCalculator;

class BlockController extends Controller
{
    /**
     * @Route("/block/head/{routeName}")
     * @Template(engine="php")
     */
    public function headAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        /**
         * Load layout for default
         */
        $default = array();
        if (isset($this->container->parameters['layout']['default']) && isset($this->container->parameters['layout']['default'][$routeName]) && isset($this->container->parameters['layout']['default'][$routeName]['head'])) {
            $default = $this->container->parameters['layout']['default'][$routeName]['head'];
        }
        
        $js = array();
        if (isset($default['js'])) {
            $js = $default['js'];
        }
        $css = array();
        if (isset($default['css'])) {
            $css = $default['css'];
        }
        
        /**
         * Load layout for current route
         */
        $current_route = array();
        if (isset($this->container->parameters['layout'][$routeName]) && isset($this->container->parameters['layout'][$routeName]['head'])) {
            $current_route = $this->container->parameters['layout'][$routeName]['head'];
        }
        if (isset($current_route['js'])) {
            $js = array_unique(array_merge($js, $current_route['js']));
        }
        if (isset($current_route['css'])) {
            $css = array_unique(array_merge($css, $current_route['css']));
        }
        
        /*
         * Update user last login time
         */
        $em = $this->getDoctrine()->getManager();
        if (true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $current_user = $this->get('security.context')->getToken()->getUser();
            $current_user->setLastLogin(new \DateTime());

            $em->persist($current_user);
            $em->flush();
        }
        
        return array('js' => $js, 'css' => $css);
    }
    
    /**
     * @Route("/block/messages/{routeName}")
     * @Template(engine="php")
     */
    public function messagesAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        return array();
    }
    
    /**
     * @Route("/block/header/{routeName}")
    * @Template("VlanceBaseBundle:Block:header_homepage_new.html.php", engine="php")
     */
    public function headerAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }

        $return_array = array('routeName' => $routeName);
        
        $em = $this->getDoctrine()->getManager();
        $top_banner = $em->getRepository('VlanceCmsBundle:Block')->findOneByHash('top-banner');
        if($top_banner){
            if($top_banner->getContent() && $top_banner->getEnabled()){
                $now = new \DateTime("now");
                if(($top_banner->getStartAt() < $now || is_null($top_banner->getStartAt())) && ($top_banner->getEndAt() > $now || is_null($top_banner->getEndAt()))){
                    $return_array['top_banner'] = $top_banner;
                }
            }
        }
        
        return $return_array;
    }
    
    /**
     * @Route("/block/header_thuengay/{routeName}", name="block_header_thuengay")
    * @Template("VlanceBaseBundle:Block/header:page_thuengay.html.php", engine="php")
     */
    public function headerThuengayAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
//        return array();
        return array('routeName' => $routeName);
    }
    
    /**
     * @Route("/block/header_community/{routeName}", name="block_header_community")
     * @Template("VlanceBaseBundle:Block/header:page_community.html.php", engine="php")
     */
    public function headerCommunityAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
//        return array();
        return array('routeName' => $routeName);
    }
    
    /**
     * @Route("/block/admin/{routeName}/{_route_params}")
     * @Template(engine="php")
     */
    public function adminAction(Request $request, $routeName = "", $_route_params = "")
    {
        $current_user = $this->get('security.context')->getToken()->getUser();
        if (!HasPermission::hasAdminPermission($current_user)) {
            return new Response("");
        }
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'admin');
        return array('blocks' => $blocks, 'routeName' => $routeName, '_route_params' => unserialize($_route_params['_route_params']));
    }
    
    /**
     * @Route("/block/left/{routeName}")
     * @Template(engine="php")
     */
    public function leftAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'left');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/block/right/{routeName}")
     * @Template(engine="php")
     */
    public function rightAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'right');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/block/top/{routeName}")
     * @Template(engine="php")
     */
    public function topAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'top');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/block/footer/{routeName}")
     * @Template(engine="php")
     */
    public function footerAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'footer');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/thuengay/block/footer/{routeName}", name="thuengay_block_footer")
     * @Template("VlanceBaseBundle:Block/thuengay:footer.html.php", engine="php")
     */
    public function footerthuengayAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'footer');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/block/language")
     * @Template(engine="php")
     */
    public function languageAction(Request $request)
    {
        $lang = $request->getSession()->get('_locale');
        $languages = array(
            0 => array(
                'title' => 'English',
                'url'   => $this->generateUrl('switch_language', array('lang' => 'en'))
            ),
            1 => array(
                'title' => 'Tiếng Việt',
                'url'   => $this->generateUrl('switch_language', array('lang' => 'vn'))
            )
        );
        return array('active' => (($lang == "en")?0:1), 'languages' => $languages);
    }
    
    /**
     * @Route("/block/beforeBodyEnd/{routeName}")
     * @Template(engine="php")
     */
    public function beforeBodyEndAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $blocks = $this->loadBlockConfig($routeName, 'beforeBodyEnd');
        return array('blocks' => $blocks, 'routeName' => $routeName);
    }
    
    /**
     * @Route("/block/filter/{routeName}")
     * @Template(engine="php")
     */
    public function filterAction(Request $request, $routeName = "")
    {
        if (!$routeName) {
            $routeName = $request->attributes->get('_route');
        }
        $attributes = $this->container->parameters['layout'][$routeName]['filter'];
        $form_builder = $this->createFormBuilder();
        foreach ($attributes as $attribute) {
            $form_builder->add($attribute, 'filter_' . $attribute, array('label' => false));
        }
        return array('form' => $form_builder->getForm()->createView());
    }
        
    /**
     * Status: Obsoleted on 09/05/2016
     * Display a gray layer to cover all page, show only the new feature
     * @Route("/block/new-feature", name="block_new_feature")
     * @Template(engine="php")
     */
    public function newFeatureAction()
    {
        // if user is not logged in, don't display popup
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new Response("");
        }
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->get('security.context')->getToken()->getUser();
        // if this user can't be notified, don't display popup
        if ($account->getNewFeature() == \Vlance\AccountBundle\Entity\Account::SHOWED_NEW_FEATURE) {
            return new Response("");
        }
        $em = $this->getDoctrine()->getManager();
        if ($account->getNewFeature() == \Vlance\AccountBundle\Entity\Account::HAVE_NEW_FEATURE) {
            $account->setNewFeature(\Vlance\AccountBundle\Entity\Account::SHOWED_NEW_FEATURE);
            $em->persist($account);
            $em->flush();
            return $this->render('VlanceBaseBundle:Block:newFeatureSuccess.html.php', array('account' => $account));
        }
        $account->setNewFeature(\Vlance\AccountBundle\Entity\Account::SHOWED_NEW_FEATURE);
        $em->persist($account);
        $em->flush();
        return array('account' => $account);
    }
    
    /**
     * Display a gray layer to cover all page, show only the new feature
     * @Route("/block/job-progress", name="vlance_base_block_job_progress")
     * @Template(engine="php")
     */
    public function jobProgressAction()
    {
        $job_id = $this->get('session')->get('current_job');
        $view = array(
            'is_open'       => 'active',
            'is_awarded'    => 'unactive',
            'is_funded'     => 'unactive',
            'is_done'       => 'unactive',
            'is_reviewed'   => 'unactive'
        );
        if($job_id){
            $em = $this->getDoctrine()->getManager();
            $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
            
            //Khong ton tai job
            if ($job) {
                if($job->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_AWARDED){
                    $view['is_open'] = 'done';
                    $view['is_awarded'] = 'active';
                }
                
                if($job->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::ESCROWED){
                    $view['is_open'] = 'done';
                    $view['is_awarded'] = 'done';
                    $view['is_funded'] = 'active';
                }
                
                if($job->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_FINISHED && ($job->getCancelStatus() == Job::STATUS_CANCEL_NONE || $job->getCancelStatus() == Job::STATUS_CANCELLED_DONE_SUCCESS)){
                    $view['is_open'] = 'done';
                    $view['is_awarded'] = 'done';
                    $view['is_funded'] = 'done';
                    $view['is_done'] = 'active';
                    $feedbacks = $job->getFeedBacksJob();
                    if(count($feedbacks) > 0){
                        $view['is_done'] = 'done';
                        $view['is_reviewed'] = 'active';
                    }
                }
            }
        }
        return $view;
    }
    
    /**
     * Display a gray layer to cover all page, show only the new feature
     * @Route("/block/contest-progress", name="vlance_base_block_contest_progress")
     * @Template(engine="php")
     */
    public function contestProgressAction()
    {
        $job_id = $this->get('session')->get('current_job');
        $view = array(
            'is_open'           => 'unactive',
            'is_choose'         => 'unactive',
            'is_qualified'      => 'unactive',
            'is_awarded'        => 'unactive',
            'is_finished'       => 'unactive'
        );
        $now = new \DateTime('now');
        if($job_id){
            $em = $this->getDoctrine()->getManager();
            $job = $em->getRepository('VlanceJobBundle:Job')->find($job_id);
            
            //Khong ton tai job
            if ($job) {
                if($job->getPaymentStatus() == \Vlance\JobBundle\Entity\Job::ESCROWED){
                    if(JobCalculator::ago($job->getCloseAt(), $now) === false){
                        $view['is_open']        = 'done';
                        $view['is_choose']      = 'active';
                    } else {
                        $view['is_open']        = 'active';
                    }
                }

                if($job->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_CONTEST_QUALIFIED){
                    $view['is_open']        = 'done';
                    $view['is_choose']      = 'done';
                    $view['is_qualified']   = 'active';
                }
                
                if($job->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_AWARDED){
                    $view['is_open']        = 'done';
                    $view['is_choose']      = 'done';
                    $view['is_qualified']   = 'done';
                    $view['is_awarded']     = 'active';
                }
                
                if($job->getStatus() == \Vlance\JobBundle\Entity\Job::JOB_FINISHED && ($job->getCancelStatus() == Job::STATUS_CANCEL_NONE || $job->getCancelStatus() == Job::STATUS_CANCELLED_DONE_SUCCESS)){
                    $view['is_open']        = 'done';
                    $view['is_choose']      = 'done';
                    $view['is_qualified']   = 'done';
                    $view['is_awarded']     = 'done';
                    $view['is_finished']    = 'active';
                }
            }
        }
        
        return $view;
    }
 
    protected function loadBlockConfig($routeName, $blockName) {
        /**
         * Load layout for default
         */
        $default = array();
        if (isset($this->container->parameters['layout']['default']) && isset($this->container->parameters['layout']['default'][$blockName])) {
            $default = $this->container->parameters['layout']['default'][$blockName];
        }
        
        /**
         * Load layout for current route
         */
        $current_route = array();
        if (isset($this->container->parameters['layout'][$routeName]) && isset($this->container->parameters['layout'][$routeName][$blockName])) {
            $current_route = $this->container->parameters['layout'][$routeName][$blockName];
        }
        
        $total = array_unique(array_merge($default, $current_route));
        foreach ($total as $idx => $item) {
            if ($item[0] == '-') {
                unset($total[$idx]);
                $item = substr($item, 1);
                $tmp = array_search($item, $total);
                if ($tmp !== FALSE) {
                    unset($total[$tmp]);
                }
            }
        }
        return $total;
    }
    
        /**
     * 
     * Display block feature freelancer in Freelancer view page
     * 
     * @Route("/block/featurefreelancer", name="vlance_base_block_featurefreelancer")
     * @Template(engine="php")
     */
    public function featurefreelancerAction(Request $request)
    {
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
            $current_user = $this->get('security.context')->getToken()->getUser();
            $profile_id = $this->get('session')->get('current_profile_view');
            if($profile_id){
                $em = $this->getDoctrine()->getManager();
                $profile = $em->getRepository('VlanceAccountBundle:Account')->find($profile_id);
                if($current_user->getId() == $profile->getId()){
                    return array('profile' => $profile);
                }
            }
        }
        return array('profile' => '');
    }
}
