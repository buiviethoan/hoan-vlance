<?php

namespace Vlance\BaseBundle\Controller\AdminMassEmail;

use Admingenerated\VlanceBaseBundle\BaseAdminMassEmailController\NewController as BaseNewController;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Vlance\BaseBundle\Form\Type\AdminMassEmail\NewType;

/**
 * NewController
 */
class NewController extends BaseNewController
{
    public function indexAction()
    {
        

        $MassEmail = $this->getNewObject();

        $this->preBindRequest($MassEmail);
        $form = $this->createForm($this->getNewType(), $MassEmail, $this->getFormOptions($MassEmail));

        return $this->render('VlanceBaseBundle:AdminMassEmailNew:index.html.twig', $this->getAdditionalRenderParameters($MassEmail) + array(
            "MassEmail" => $MassEmail,
            "form" => $form->createView(),
        ));
    }
    
    public function createAction()
    {
        $MassEmail = $this->getNewObject();

        $this->preBindRequest($MassEmail);
        $form = $this->createForm($this->getNewType(), $MassEmail, $this->getFormOptions($MassEmail));
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            try {
                $this->preSave($form, $MassEmail);
                $this->saveObject($MassEmail);
                $this->postSave($form, $MassEmail);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_edit", array('pk' => $MassEmail->getId()) ));
            } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $MassEmail);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('VlanceBaseBundle:AdminMassEmailNew:index.html.twig', $this->getAdditionalRenderParameters($MassEmail) + array(
            "MassEmail" => $MassEmail,
            "form" => $form->createView(),
        ));
    }

    /**
     * This method is here to make your life better, so overwrite it
     *
     * @param \Exception $exception throwed exception
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function onException(\Exception $exception, \Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        if ($this->container->getParameter('kernel.debug')) {
            throw $exception;
        }
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function preBindRequest(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function postSave(\Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }


    /**
     * Get additional parameters for rendering.
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     * return array
     */
    protected function getAdditionalRenderParameters(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        return array();
    }

    /**
     * Get optional form options.
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     * return array
     **/
    protected function getFormOptions(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        return array();
    }

    protected function getNewType()
    {
        $type = new NewType();
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    protected function getNewObject()
    {
        return new \Vlance\BaseBundle\Entity\MassEmail;
    }

    protected function saveObject(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        $em = $this->getDoctrine()->getManagerForClass('Vlance\BaseBundle\Entity\MassEmail');
        $em->persist($MassEmail);
        $em->flush();
    }
}
