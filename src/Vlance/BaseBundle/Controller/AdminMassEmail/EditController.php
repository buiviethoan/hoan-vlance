<?php

namespace Vlance\BaseBundle\Controller\AdminMassEmail;

use Admingenerated\VlanceBaseBundle\BaseAdminMassEmailController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Vlance\BaseBundle\Form\Type\AdminMassEmail\EditType;

/**
 * EditController
 */
class EditController extends BaseEditController
{
    public function indexAction($pk)
    {
        $MassEmail = $this->getObject($pk);

        
        

        if (!$MassEmail) {
            throw new NotFoundHttpException("The Vlance\BaseBundle\Entity\MassEmail with id $pk can't be found");
        }

        $this->preBindRequest($MassEmail);
        $form = $this->createForm($this->getEditType(), $MassEmail, $this->getFormOptions($MassEmail));

        return $this->render('VlanceBaseBundle:AdminMassEmailEdit:index.html.twig', $this->getAdditionalRenderParameters($MassEmail) + array(
            "MassEmail" => $MassEmail,
            "form" => $form->createView(),
        ));
    }

    public function updateAction($pk)
    {
        $MassEmail = $this->getObject($pk);

        

        if (!$MassEmail) {
            throw new NotFoundHttpException("The Vlance\BaseBundle\Entity\MassEmail with id $pk can't be found");
        }

        $this->preBindRequest($MassEmail);
        $form = $this->createForm($this->getEditType(), $MassEmail, $this->getFormOptions($MassEmail));
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            try {
                
                $this->preSave($form, $MassEmail);
                $this->saveObject($MassEmail);
                $this->postSave($form, $MassEmail);

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans("action.object.edit.success", array(), 'Admingenerator') );

                if($this->get('request')->request->has('save-and-add'))
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_new" ));
                if($this->get('request')->request->has('save-and-list'))
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_list" ));
                else
                  return new RedirectResponse($this->generateUrl("Vlance_BaseBundle_AdminMassEmail_edit", array('pk' => $pk) ));

                        } catch (\Exception $e) {
                $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
                $this->onException($e, $form, $MassEmail);
            }

        } else {
            $this->get('session')->getFlashBag()->add('error',  $this->get('translator')->trans("action.object.edit.error", array(), 'Admingenerator') );
        }

        return $this->render('VlanceBaseBundle:AdminMassEmailEdit:index.html.twig', $this->getAdditionalRenderParameters($MassEmail) + array(
            "MassEmail" => $MassEmail,
            "form" => $form->createView(),
        ));
    }

    /**
     * This method is here to make your life better, so overwrite it
     *
     * @param \Exception $exception throwed exception
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function onException(\Exception $exception, \Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        if ($this->container->getParameter('kernel.debug')) {
            throw $exception;
        }
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function preBindRequest(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     */
    public function postSave(\Symfony\Component\Form\Form $form, \Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
    }


    protected function getEditType()
    {
        $type = new EditType();
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }


    /**
     * Get additional parameters for rendering.
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     * return array
     **/
    protected function getAdditionalRenderParameters(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        return array();
    }

    /**
     * Get optional form options.
     *
     * @param \Vlance\BaseBundle\Entity\MassEmail $MassEmail your \Vlance\BaseBundle\Entity\MassEmail object
     * return array
     **/
    protected function getFormOptions(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        return array();
    }

    /**
     * Get object Vlance\BaseBundle\Entity\MassEmail with identifier $pk
     *
     * @param mixed $pk
     * @return Vlance\BaseBundle\Entity\MassEmail
     */
    protected function getObject($pk)
    {
        return $this->getQuery($pk)->getOneOrNullResult();
    }

    /**
     * Get query from query builder
     *
     * @param mixed $pk
     * @return Doctrine\ORM\Query
     */
    protected function getQuery($pk)
    {
        return $this->getQueryBuilder($pk)->getQuery();
    }

    /**
     * Creates a QueryBuilder instance filtering on id
     * property.
     *
     * @param mixed $pk
     * @return Doctrine\ORM\QueryBuilder
     */
    protected function getQueryBuilder($pk)
    {
        return $this->getDoctrine()
             ->getManagerForClass('Vlance\BaseBundle\Entity\MassEmail')
             ->getRepository('Vlance\BaseBundle\Entity\MassEmail')
             ->createQueryBuilder('q')
             ->where('q.id = :pk')
             ->setParameter(':pk', $pk);
    }

    protected function saveObject(\Vlance\BaseBundle\Entity\MassEmail $MassEmail)
    {
        $em = $this->getDoctrine()->getManagerForClass('Vlance\BaseBundle\Entity\MassEmail');
        $em->persist($MassEmail);
        $em->flush();
    }

    protected function getVersions()
    {
        return $this->get('session')->get('Vlance\BaseBundle\AdminMassEmailEdit\Versions', array());
    }
    
    protected function setVersions($versions = array())
    {
        $this->get('session')->set('Vlance\BaseBundle\AdminMassEmailEdit\Versions', $versions);
    }
    
    protected function saveVersion($pk, $version)
    {
        $versions = $this->getVersions();
        $versions[$pk] = $version;
        $this->setVersions($versions);
    }
    
    public function testAction($pk)
    {
        
    }
}
