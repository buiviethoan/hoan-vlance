<?php

namespace Vlance\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Vlance\BaseBundle\Entity\TrackingAction;
use Vlance\BaseBundle\Entity\TrackingEvent;

/**
 * Mass Email controller.
 *
 * @Route("/tracking")
 */
class TrackingController extends Controller
{
    /**
     * @Route("/event/{action_hash}", name="tracking_event")
     * @Template(engine="php")
     */
    public function hasheventAction(Request $request, $action_hash)
    {
        // ex: "tuan" = "vXaMOF18U5C2fij5poE9Qw=="
        $action_name = $this->get('vlance_base.helper')->decrypt($action_hash);
        if($action_name == false){
            return new JsonResponse(false);
        }

        $this->get('vlance_base.helper')->trackevent($action_name);
        return new JsonResponse(true);
    }
}
