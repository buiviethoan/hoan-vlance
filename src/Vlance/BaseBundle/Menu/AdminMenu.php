<?php

namespace Vlance\BaseBundle\Menu;

use Admingenerator\GeneratorBundle\Menu\DefaultMenuBuilder;
use Knp\Menu\FactoryInterface;

class AdminMenu extends DefaultMenuBuilder {
    public function navbarMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array('id' => 'main_navigation', 'class' => 'nav'));
        
        $dropdownBase = $this->addDropdown($menu, 'Base', true);
        $this->addLinkRoute($dropdownBase, 'Vlance Payment Method', 'Vlance_PaymentBundle_AdminPaymentMethod_list');
        $this->addLinkRoute($dropdownBase, 'Category', 'Vlance_AccountBundle_AdminCategory_list');
        $this->addLinkRoute($dropdownBase, 'Service', 'Vlance_AccountBundle_AdminService_list');
        $this->addLinkRoute($dropdownBase, 'Skill', 'Vlance_AccountBundle_AdminSkill_list');
        $this->addLinkRoute($dropdownBase, 'City', 'Vlance_BaseBundle_AdminCity_list');
        
        $dropdownAccount = $this->addDropdown($menu, 'Account', true);
        $this->addLinkRoute($dropdownAccount, 'Account', 'Vlance_AccountBundle_AdminAccount_list');
        $this->addLinkRoute($dropdownAccount, 'PersonId', 'Vlance_AccountBundle_AdminPersonId_list');
        $this->addLinkRoute($dropdownAccount, 'TaxId', 'Vlance_AccountBundle_AdminTaxId_list');
        $this->addLinkRoute($dropdownAccount, 'Portfolio', 'Vlance_AccountBundle_AdminPortfolio_list');
        $this->addLinkRoute($dropdownAccount, 'EmailInvite', 'Vlance_AccountBundle_AdminEmailInvite_list');
        
        $dropdownJob = $this->addDropdown($menu, 'Job', true);
        $this->addLinkRoute($dropdownJob, 'Job', 'Vlance_JobBundle_AdminJob_list');
        $this->addLinkRoute($dropdownJob, 'Job cash', 'Vlance_PaymentBundle_AdminJobCash_list');
        $this->addLinkRoute($dropdownJob, 'Bid', 'Vlance_JobBundle_AdminBid_list');
        $this->addLinkRoute($dropdownJob, 'Message', 'Vlance_JobBundle_AdminMessage_list');
        $this->addLinkRoute($dropdownJob, 'Feedback', 'Vlance_WorkshopBundle_AdminFeedback_list');

        $dropdownPayment = $this->addDropdown($menu, 'Payment', true);
        $this->addLinkRoute($dropdownPayment, 'User Payment Method', 'Vlance_PaymentBundle_AdminUserPaymentMethod_list');
        $this->addLinkRoute($dropdownPayment, 'User Cash Account', 'Vlance_PaymentBundle_AdminUserCash_list');
        $this->addLinkRoute($dropdownPayment, 'Cash Transaction', 'Vlance_PaymentBundle_AdminTransaction_list');
        $this->addLinkRoute($dropdownPayment, '1Pay Transaction', 'Vlance_PaymentBundle_AdminOnePay_list');
        
        $dropdownCredit = $this->addDropdown($menu, 'Credit', true);
        $this->addLinkRoute($dropdownCredit, 'Credit Account', 'Vlance_PaymentBundle_AdminCreditAccount_list');
        $this->addLinkRoute($dropdownCredit, 'Credit Buy Transaction', 'Vlance_PaymentBundle_AdminCreditBuyTransaction_list');
        $this->addLinkRoute($dropdownCredit, 'Credit Spending Transaction', 'Vlance_PaymentBundle_AdminCreditExpenseTransaction_list');
        
        $dropdownOrder = $this->addDropdown($menu, 'Service Pack', true);
        $this->addLinkRoute($dropdownOrder, 'Service Pack', 'Vlance_ServiceBundle_AdminServicePack_list');
        $this->addLinkRoute($dropdownOrder, 'Service Addon', 'Vlance_ServiceBundle_AdminServiceAddon_list');
        $this->addLinkRoute($dropdownOrder, 'Order', 'Vlance_ServiceBundle_AdminOrder_list');

        $dropdownCms = $this->addDropdown($menu, 'Cms', true);
        $this->addLinkRoute($dropdownCms, 'Block', 'Vlance_CmsBundle_AdminCmsBlock_list');
        $this->addLinkRoute($dropdownCms, 'Page', 'Vlance_CmsBundle_AdminCmsPage_list');
        $this->addLinkRoute($dropdownCms, 'CommunityPost', 'Vlance_CmsBundle_AdminCommunityPost_list');
        // $this->addLinkRoute($dropdownCms, 'Poll', 'Vlance_CmsBundle_AdminPoll_list');
        // $this->addLinkRoute($dropdownCms, 'Testimonial', 'Vlance_CmsBundle_AdminTestimonial_list');
        
        $dropdownOther = $this->addDropdown($menu, 'Other', true);
        $this->addLinkRoute($dropdownOther, 'Mass Email', 'Vlance_BaseBundle_AdminMassEmail_list');
        $this->addLinkRoute($dropdownOther, 'CustomQuery - Queries', 'Vlance_BaseBundle_AdminCustomQuery_list');
        $this->addLinkRoute($dropdownOther, 'CustomQuery - Results', 'Vlance_BaseBundle_AdminCustomQueryResult_list');
        
        return $menu;
    }
}