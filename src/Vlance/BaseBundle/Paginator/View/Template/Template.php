<?php
namespace Vlance\BaseBundle\Paginator\View\Template;

use Pagerfanta\View\Template\DefaultTemplate as BaseTemplate;
use Vlance\BaseBundle\Utils\Url;

class Template extends BaseTemplate
{
    private $routeGenerator;
    protected $_params = array();
    protected $_routeName;
    
    static protected $defaultOptions = array(
        'previous_message'   => 'previous',
        'next_message'       => 'next',
        'css_disabled_class' => 'disabled',
        'css_dots_class'     => 'dots',
        'css_current_class'  => 'current',
        'dots_text'          => '...',
        'container_template' => '%pages%',
        'page_template'      => '<a href="%href%">%text%</a>',
        'span_template'      => '<span class="%class%">%text%</span>'
    );
    
    public function __construct(array $params = array(), $routeName = "") {
        $this->_params = $params;
        $this->_routeName = $routeName;
        parent::__construct();
    }
//    Ẩn nút next và nút previous
    public function previousDisabled()
    {
//        return $this->generateSpan($this->option('css_disabled_class'), $this->option('previous_message'));
    }

    public function previousEnabled($page)
    {
//        return $this->pageWithText($page, $this->option('previous_message'));
    }

    public function nextDisabled()
    {
//        return $this->generateSpan($this->option('css_disabled_class'), $this->option('next_message'));
    }

    public function nextEnabled($page)
    {
//        return $this->pageWithText($page, $this->option('next_message'));
    }
//   End Ẩn nút next và nút previous
    
    public function setRouteGenerator($routeGenerator)
    {
        $this->routeGenerator = $routeGenerator;
    }
    
    protected function generateRoute($page)
    {
        $tmp = $this->_params;
        if (isset($tmp['page'])) {
            $tmp['page'] = $page;
        } else {
            $tmp['page'] = $page;
        }
        return $this->getRouteGenerator()->generate($this->_routeName, array('filters' => Url::buildUrl($tmp)));
    }
    
    private function getRouteGenerator()
    {
        if (!$this->routeGenerator) {
            throw new \RuntimeException('There is no route generator.');
        }

        return $this->routeGenerator;
    }
}