<?php

namespace Vlance\BaseBundle\Helper;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Templating\Helper\Helper as BaseHelper;
use Doctrine\ORM\EntityManager;

use Vlance\AccountBundle\Entity\Account;
use Vlance\BaseBundle\Entity\PartnerMoneyloverCode as PartnerMoneyloverCode;
use Vlance\JobBundle\Entity\Bid;
use Vlance\JobBundle\Entity\Message;
use Vlance\BaseBundle\Utils\JobCalculator;

class Helper extends BaseHelper {
    protected $_em;
    protected $_container;
    protected $test = false;
    
    public function __construct(ContainerInterface $container, EntityManager $em) {
        $this->_em = $em;
        $this->_container = $container;
    }
    
    public function getName() {
        return 'vlance';
    }
    
    /**
     * Retrieve city list
     * 
     * @return array
     */
    public function getCityList(){
        $repo = $this->_em->getRepository('VlanceBaseBundle:City');
        $cities = $repo->findBy(array(), array('name' => 'ASC'));
        $list = array();
        foreach($cities as $city) {
            $list[$city->getId()] = $city;
        }
        
        if(isset($list[0]) && isset($list[15]) && isset($list[31]) && isset($list[24])){
            $top_list = array();
            $top_list[] = $list[0];
            $top_list[] = $list[24];
            $top_list[] = $list[31];
            $top_list[] = $list[15];
            unset($list[31]);
            unset($list[24]);
            unset($list[15]);
            unset($list[0]);
            $list = array_merge($top_list, $list);
        }
        return $list;
    }
    
    /**
     * Retrieve city list
     * 
     * @return array
     */
    public function getCityOnsiteList(){
        $repo = $this->_em->getRepository('VlanceBaseBundle:City');
        $cities = $repo->findBy(array(), array('name' => 'ASC'));
        $list = array();
        foreach($cities as $city) {
            $list[$city->getId()] = $city;
        }
        
        if(isset($list[0]) && isset($list[15]) && isset($list[31]) && isset($list[24])){
            $top_list = array();
            $top_list[] = $list[24];
            $top_list[] = $list[31];
            $top_list[] = $list[15];
            unset($list[31]);
            unset($list[24]);
            unset($list[15]);
            unset($list[0]);
            $list = array_merge($top_list, $list);
        }
        return $list;
    }
    
    /**
     * Get vLance's commission rate
     * 
     * @return type
     */
    public function getVlanceCommission() {
        $vlance_parameter = $this->_container->getParameter('vlance_system');
        return $vlance_parameter['commission'];
    }
    
    /**
     * Get app's parameters
     * 
     * @param type $path
     * @return type
     */
    public function getParameter($path) {
        $paths = explode('.', $path);
        if ($this->_container->hasParameter($paths[0])) {
            $root = $this->_container->getParameter($paths[0]);
            for ($i = 1; $i < count($paths); $i++) {
                if (isset($root[$paths[$i]])) {
                    $root = $root[$paths[$i]];
                } else {
                    return null;
                }
            }
            return $root;
        } else {
            return null;
        }
    }
    
    /**
     * Format number with point decimal
     * 
     * @param type $number
     * @return type
     */
    public function formatNumber($number){
        return number_format($number, 0, ",", ".");
    }
    
    /**
     * Format currency in VNĐ with point decimal
     * 
     * @param type $number
     * @param type $currency
     * @return type
     */
    public function formatCurrency($number, $currency){
        return number_format($number, 0, ',', '.') . " " . $currency;
    }
    
    /**
     * Convert New line character with html <p> tag
     * 
     * @param type $string
     * @return string
     */
    public function nl2p($string)
    {
        $paragraphs = '';

        foreach (explode("\n", $string) as $line) {
            if (trim($line)) {
                $paragraphs .= '<p>' . $line . '</p>';
            }
        }

        return $paragraphs;
    }
    
    /**
     * Verify if user is authenticated
     * 
     * @return mixed
     */
    public function isAuthen(){
        return is_object($this->_container->get('security.context')->getToken()->getUser());
    }
    
    /**
     * Send tracking event to statistics tools
     * 
     * @param int $user
     * @param string $event
     * @param array $properties
     * @param boolean $update_identify
     * @param mixed $tool = 'all' || array('ga','mixpanel')
     * @return type
     */
    public function trackAll($user = NULL, $event, $properties = NULL, $update_identify = true, $tool = 'all'){
        /* If event is temporarily disabled, don't track it */
        $temporarily_disable = array(
            "Edit basic profile", 
            "Edit profile",
            "After login page",
            "Show feature bid block"
        );
        if(in_array($event, $temporarily_disable)){
            return false;
        }
        /* END If event is temporarily disabled, don't track it */
        
        $identify = NULL;
        
        /* @var $user \Vlance\AccountBundle\Entity\Account */
        if(is_null($user)){
            $user = $this->_container->get('security.context')->getToken()->getUser();
        }
        
        if(is_object($user)){
            // Don't track admin's activities
            if($user->isAdmin()){
                return NULL;
            } else{
                if($update_identify){
                    $info = $this->buildProfile($user);
                    // Add profiles of "client" only
                    if($user->getType() == "client"){
                        // Mixpanel
                        $this->_container->get('session')->getJSBag()->add('mixpanel', $this->_container->get('templating')
                               ->render('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
                            array(
                                'type' => 'js',
                                'data' => json_encode(array(
                                    'function' => "people.set",
                                    'params' => array(
                                        'id' => $user->getId(),
                                        'info' => $info)
                                ))
                            )
                        ));
                    }
                }
                $identify = $user->getId();
            }
        }
        
        $this->_container->get('session')->getJSBag()->add('mixpanel', $this->_container->get('templating')
               ->render('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
            array(
                'type' => 'js',
                'data' => json_encode(array(
                    'identify' => $identify,
                    'event' => array(
                        'name' => $event,
                        'properties' => $properties),
                    'track_ga' => ($tool == 'all' || (is_array($tool) && in_array('ga',$tool))) ? true : false,
                    'track_mixpanel' => ($tool == 'all' || (is_array($tool) && in_array('mixpanel',$tool))) ? true : false
                ))
            )
        ));
    }
    
    /**
     * Tracking event with Mixpanel
     * 
     * @param type $user
     * @param type $event
     * @param type $properties
     * @param type $update_identify
     */
    public function trackMixpanel($user = NULL, $event, $properties = NULL, $update_identify = true){
        $this->trackAll($user, $event, $properties, $update_identify, array('mixpanel'));
    }
    
    protected function buildProfile($user)
    {
        return array(
            '$last_name'        => $user->getFullName(),
            '$name'             => $user->getFullName(),
            '$email'            => $user->getEmail(),
            '$phone'            => $user->getTelephone(),
            'address'           => $user->getAddress(),
            'avatar'            => $user->getPath() ? 1: 0,
            'banned_to'         => $user->getBannedTo() instanceof \DateTime ? date_format($user->getBannedTo(), 'Y-m-d\TH:i:s') : null,
            'category'          => ($user->getCategory())?$user->getCategory()->getTitle():NULL,
            'category_path'     => ($user->getCategory())?$user->getCategory()->getPath():NULL,
            'certificated'      => $user->getIsCertificated(),
            'city'              => ($user->getCity())?$user->getCity()->getName():NULL,
            'confirmation_token'=> $user->getConfirmationToken(),
            'created_at'        => $user->getCreatedAt() instanceof \DateTime ? date_format($user->getCreatedAt(), 'Y-m-d\TH:i:s') : null,
            //'description'       => $user->getDescription(),
            'enabled'           => $user->isEnabled(),
            'in_come'           => $user->getInCome(),
            'locked'            => $user->isLocked(),
            'missing_client_info' => $user->getIsMissingClientInfo(),
            'num_bid'           => $user->getNumBid(),
            'num_job'           => count($user->getJobs()),
            'num_job_hired'     => count($user->getJobsHired()),
            'num_job_published' => count($user->getJobs()),
            'num_job_deposit'   => $user->getNumJobDeposit(),   // Cần update
            'num_job_finish'    => $user->getNumJobFinish(),    // Cần update
            'num_reviews'       => $user->getNumReviews(),
            'out_come'          => $user->getOutCome(),
            'title'             => $user->getTitle(),
            'type'              => $user->getType(),
            'telephone_verified_at' => $user->getTelephoneVerifiedAt() instanceof \DateTime ? date_format($user->getTelephoneVerifiedAt(), 'Y-m-d\TH:i:s') : null,
            'updated_at'        => $user->getUpdatedAt() instanceof \DateTime ? date_format($user->getUpdatedAt(), 'Y-m-d\TH:i:s') : null,
            'website'           => $user->getWebsite()
        );
    }
    
    /**
     * Tracking event with Google Analytics
     * 
     * @param type $user
     * @param type $event
     * @param type $properties
     * @param type $update_identify
     */
    public function trackGa($user = NULL, $event, $properties = NULL, $update_identify = true){
        $this->trackAll($user, $event, $properties, $update_identify, array('ga'));
    }
    
    public function buildTrackingCodeEmailMixpanel($event, $properties, $distinct_id)
    {
        $parameters = $this->_container->getParameter("vlance_system");
        $properties['token'] = ($this->isTestMode()) ? $parameters['mixpanel_token']['dev'] : $parameters['mixpanel_token']['prod'];
        $properties['distinct_id'] = $distinct_id;
        
        $mixpanel_data = base64_encode(json_encode(
            array(
                'event' => $event,
                'properties' => $properties
            )
        ));
        return "http://api.mixpanel.com/track/?data=" . $mixpanel_data . "&ip=1&img=1";
    }
    
    /**
     *  Build tracking code for GA
     * 
     * @param type $properties
     * @param type $distinct_id
     * @return type
     */
    public function buildTrackingCodeEmailGa($properties, $distinct_id)
    {
        return "http://www.google-analytics.com/collect?v=1&tid=UA-34003187-3&cid=34003187&t=event&ec=" . $properties['ec'] . "&ea=" 
                . $properties['ea'] . "&el=" . $properties['el'];
    }
    
    protected function isTestMode()
    {
        return $this->test;
    }
    
    /**
     * Track revenue with Mixpanel
     * 
     * @param type $user
     * @param type $amount
     * @param type $action
     * @param type $channel
     * @return type
     */
    public function mixpanelTrackRevenue($user = NULL, $amount, $action = "Commission", $channel = NULL){
        $identify = NULL;
        
        if(is_array($channel)){
            if($channel['channel'] == "SMS" && isset($channel['telephone'])){
                $sms_rate = 0.6 * 0.85 / 1.1; // VNS, VMS
                $pattern = '/^(086|096|097|098|0162|0163|0164|0165|0166|0167|0168|0169)/'; // VTS
                if(preg_match($pattern, (string)($channel['telephone']))){
                    $sms_rate = 0.65 * 0.85 / 1.1;
                }
                $amount = (int)$amount * $sms_rate;
            }
        }
        
        if(is_null($user)){
            $user = $this->_container->get('security.context')->getToken()->getUser();
        }
        
        if(is_object($user)){
            // Don't track admin's activities
            if($user->isAdmin()){
                return NULL;
            } else{
                require_once __DIR__.'/../../Lib/Mixpanel/lib/Mixpanel.php';
                $mp = \Mixpanel::getInstance($this->getParameter('vlance_system.mixpanel_token.prod'));
                $info = $this->buildProfile($user);
                $mp->people->set($user->getId(), $info);
                $identify = $user->getId();
            }
        }
        
        $this->_container->get('session')->getJSBag()->add('mixpanel', $this->_container->get('templating')
                ->render('VlanceBaseBundle:Mixpanel:mixpanel.html.php', 
            array(
                'type' => 'js',
                'data' => json_encode(array(
                    'identify'  => $identify,
                    'revenue'   => $amount,
                    'stream'    => $action
            )))));
    }
    
    /**
     * Track email event with Mixpanel
     * 
     * @param type $event_name
     * @param type $user_id
     * @param type $campaign
     * @return type
     */
    public function mixpanelTrackMail($event_name, $user_id, $campaign = "Base"){
        $parameters = $this->getParameter("vlance_system");
        return base64_encode(json_encode(
            array(
                'event' => $event_name,
                'properties' => array(
                    'token' => $parameters['mixpanel_token']['prod'],
                    'distinct_id' => $user_id,
                    'campaign'  => $campaign
                )
            )
        ));
    }
    
    /**
     * Tracking event
     * 
     * @param \Vlance\BaseBundle\Helper\Request $request
     * @param type $action_name
     * @return \Vlance\BaseBundle\Helper\JsonResponse
     */
    protected function trackevent(Request $request, $action_name)
    {
        $em = $this->getDoctrine()->getManager();
        /* @var $campaign \Vlance\BaseBundle\Entity\TrackingAction */
        $action = $em->getRepository('VlanceBaseBundle:TrackingAction')->findOneBy(array('name' => $action_name));
        if(is_null($action)){
            $action = new TrackingAction();
            $action->setName($action_name);
            $em->persist($action);
            $em->flush();
        }
        
        $event = new TrackingEvent();
        $event->setAction($action);
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->_container->get('security.context')->getToken()->getUser();
        if(is_object($account)){
            $event->setAccount($account);
        }
        $em->persist($event);
        $em->flush();
        return new JsonResponse(true);
    }
    
    /**
     * Encrypt message for public usage
     * 
     * @param type $message
     * @return type
     */
    public function encrypt($message){
        $method = "aes-256-cbc";
        $password = "EncodePassword";
        $iv = "BaseBundleEncode";
        return str_replace(array('+', '/'), array('-', '_'), base64_encode(openssl_encrypt($message , $method, $password, OPENSSL_RAW_DATA, $iv)));
    }
    
    /**
     * Decrypt message from public
     * 
     * @param type $message
     * @return type
     */
    public function decrypt($message){
        $method = "aes-256-cbc";
        $password = "EncodePassword";
        $iv = "BaseBundleEncode";
        return openssl_decrypt(base64_decode(str_replace(array('-', '_'), array('+', '/'), $message)), $method, $password, OPENSSL_RAW_DATA, $iv);
    }
    
    public function getIsTestDepositClient()
    {
        return false; //Update 04/08/2017 Tat job DC
        /* @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $this->_container->get('security.context')->getToken()->getUser();
        if(is_object($account)){
            // 20% new account from 4/12/2015
            if($account->getId() % 10 == 2 || $account->getId() % 10 == 7){ // 20% only
                // if($account->getType() == 'client'){
                    $testing_date = new \DateTime('2015-12-04 19:00:00');
                    if($account->getCreatedAt() > $testing_date){
                        return true;
                    }
                // }
            } 
            // 10% more, new account from 23/12/2015
            // So mod: 2, 4, 7
            elseif($account->getId() % 10 == 4){
                $testing_date = new \DateTime('2015-12-23 10:00:00');
                if($account->getCreatedAt() > $testing_date){
                    return true;
                }
            }
            // 20% more, new account from 19/06/2016
            // So mod: 2, 4, 6, 7, 9
            elseif($account->getId() % 10 == 6 || $account->getId() % 10 == 9){
                $testing_date = new \DateTime('2016-06-19 0:00:00');
                if($account->getCreatedAt() > $testing_date){
                    return true;
                }
            }
            // 50% more, new account from 16/05/2017
            // So mod: 0, 1, 2, 4, 5, 6, 7, 8, 9
            elseif($account->getId() % 10 == 0 || $account->getId() % 10 == 1 || $account->getId() % 10 == 3 || $account->getId() % 10 == 5 || $account->getId() % 10 == 8){
                $testing_date = new \DateTime('2017-05-16 0:00:00');
                if($account->getCreatedAt() > $testing_date){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Send user Money Lover Premium code via Email
     * 
     * @param int $account_id
     */
    public function partnerMoneyloverSendCode($em, $account_id, $detail_note = array())
    {
        // Validation Account
        if(!isset($account_id)){
            return array('error' => 101);
        }
        
        // User is not admin
        if((int)$account_id <= 1000){
            return array('error' => 102);
        }
        
        // Can not find the user having $account_id
        $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $account_id));
        if(!$account){
            return array('error' => 103);
        }
        
        // User already got premium code
        $assigned_code = $em->getRepository('VlanceBaseBundle:PartnerMoneyloverCode')->findOneByAccount($account);
        if($assigned_code){
            return array('error' => 104);
        }
        
        // No code available
        $available_codes = $em->getRepository('VlanceBaseBundle:PartnerMoneyloverCode')->getAvailableCodes();
        if(count($available_codes) <= 0){
            return array('error' => 105);
        }
        
        // Get one code, then assign it to the selected user
        /* @var $code PartnerMoneyloverCode */
        $code = reset($available_codes);
        $code->setAccount($account);
        $code->setOfferedAt(new \DateTime());
        $code->setDetail(json_encode($detail_note));
        try {
            $em->persist($code);
            $em->flush();
        }catch (Exception $ex) {
            return array('error' => 106);
        }
        
        // Send notification email to the user
        $parameters = $this->_container->getParameter("vlance_system");
        $from_vlance_gift = $parameters['email']['gift'];
        $template = "VlanceBaseBundle:Email/Partner/Moneylover:gift.html.twig";

        $context = array(
            'user'          => $account,
            'code'          => $code->getCode(),
            'expired_date'  => date('d-m-Y', $code->getExpiredAt()->getTimestamp()),
            'pixel_tracking'=> $this->mixpanelTrackMail("Moneylover Gift Email Open", $account->getId(), "Partner Money Lover")
        );
        try {
            $this->_container->get('vlance.mailer')->sendTWIGMessageBySES($template, $context, $from_vlance_gift, $account->getEmail());
        } catch (Exception $ex) {
            return array('error' => 107);
        }
        
        // If success
        return array('error' => 0, 'data' => array(
            'code'          => $code->getCode(),
            'expired_date'  => date('d-m-Y', $code->getExpiredAt()->getTimestamp())
        ));
    }
    
    /**
     * Send user Money Lover Premium code via Email
     * 
     * @param int $account_id
     */
    public function getAvailableForPartnerMoneyloverCode($em, $account_id)
    {
        // Validation Account
        if(!isset($account_id)){
            return false;
        }
        
        // User is not admin
        if((int)$account_id <= 1000){
            return false;
        }
        
        // Can not find the user having $account_id
        $account = $em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => $account_id));
        if(!$account){
            return false;
        }
        
        // User already got premium code
        $assigned_code = $em->getRepository('VlanceBaseBundle:PartnerMoneyloverCode')->findOneByAccount($account);
        if($assigned_code){
            return false;
        }
        
        return true;
    }
    
    public function createMessage(Bid $related_bid, Account $sender, Account $receiver, $content, $status = Message::NEW_MESSAGE, $notify = true)
    {
        // Validation
        if(!$related_bid instanceof Bid || !$sender instanceof Account || !$receiver instanceof Account || !$content){
            return false;
        }
        if($status != Message::NEW_MESSAGE && $status != Message::READ_MESSAGE){
            return false;
        }
        // END Validation
        
        $message = new Message();
        $message->setBid($related_bid);
        $message->setSender($sender);
        $message->setReceiver($receiver);
        $message->setContent($content);
        $message->setStatus($status);
        $message->setIsNotified($notify);
        $this->_em->persist($message);
        $this->_em->flush();
        return $message;
    }
    
    public function createSystemMessage(Bid $related_bid, Account $receiver, $content)
    {
        $acc_sys = $this->_em->getRepository('VlanceAccountBundle:Account')->findOneBy(array('id' => Account::USER_VLANCE_SYS));
        return $this->createMessage($related_bid, $acc_sys, $receiver, $content);
    }
    
    /*
     * Ẩn banner tự động
     * Cách sử dụng + ví dụ: Ngày bật - Ngày tắt
     *  <?php if($view['vlance']->autoHideBanner('16-05-2017 00:00:00', '20-05-2017 00:00:00') === true): ?> //Xét ngày giờ ẩn banner
            <div class="slide-reviews-homepage img-content">
                <h2 class="title">Hơn 50 gói dịch vụ<br/> viết bài</h2>
                <div class="button">
                    <a class="btn btn-vl btn-vl-large btn-vl-blue" onclick="vtrack('Click see all service packs', {'location':'Homepage'})" href="<?php echo $view['router']->generate("service_pack_list", array('filters' => Url::buildListingFilters(array('category' => 'viet-lach')))); ?>" title="Xem dịch vụ">Xem dịch vụ</a>
                </div>
            </div>
        <?php endif; ?>
     */
    
    public function autoHideBanner($startTime, $finishTime){
        $now = new \DateTime('now');
        $start = new \DateTime($startTime);
        $finish = new \DateTime($finishTime);
        
        if($start < $finish){
            if(($start < $now) && ($now < $finish)){
                return true;
            }
        }

        return false;
    }
}