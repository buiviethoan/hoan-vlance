<?php

namespace Vlance\BaseBundle\Mailer;

use Vlance\AccountBundle\Entity\Account;
use Vlance\CmsBundle\Entity\Poll;
use Vlance\BaseBundle\Entity\EmailLog;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\TwigSwiftMailer as BaseMailer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Aws\Ses\SesClient;

class TwigSwiftMailer extends BaseMailer
{
    protected $_container;
    
    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters, ContainerInterface $container) {
        parent::__construct($mailer, $router, $twig, $parameters);
        $this->_container = $container;
        $this->twig->addExtension(new \Twig_Extension_StringLoader());
    }
    
    // Overwrite from the BaseMailer
    public function sendConfirmationChangeEmail(UserInterface $user)
    {
        $template = "VlanceAccountBundle:Email:confirmation_change_email.html.twig";
        error_log($template);
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        error_log($url);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );
        
        error_log($user->getEmail());

        error_log($this->parameters['from_email']['confirmation']);
        $this->sendMessage($template, $context, $this->parameters['from_email']['confirmation'], $user->getEmail());
        //$this->sendTWIGMessageBySES($template, $context, $this->parameters['from_email']['confirmation'], $user->getEmail());
    }
    
    public function sendMessageNotification(UserInterface $sender, UserInterface $receiver)
    {
        $template = $this->parameters['template']['messageNotification'];
        $context = array(
            'sender' => $sender,
            'receiver' => $receiver
        );

        $this->sendMessage($template, $context, $this->parameters['from_email']['noreply'], $receiver->getEmail());
    }
    
    /**
     * @todo remove this function, use sendTWIGMessageBySES
     * @param \Vlance\CmsBundle\Entity\Poll $poll
     * @param \Vlance\AccountBundle\Entity\Account $account
     */
    public function sendPollNewsletter(Poll $poll, Account $account) {
        $templateName = $this->parameters['template']['poll'];
        $context = array(
            'account' => $account,
            'poll' => $poll
        );
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        // $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        /**
         * Use php mail function - will fixed later
         */
        $to = $account->getEmail();
        $this->sendEmail($to, $subject, $htmlBody, $this->parameters['from_email']['hotro']);
        // $headers  = 'MIME-Version: 1.0' . "\r\n";
        // $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        // $headers .= 'From: vLance <' . $this->parameters['from_email']['noreply'] . '>' . "\r\n";
        // if (mail($to, $subject, $htmlBody, $headers)) {
        //     echo "send ok\n";
        //        } else {
        //            echo "shit\n";
        //        };

        //        $this->sendMessage($template, $context, $this->parameters['from_email']['noreply'], $account->getEmail());
    }
    
    /**
     * @todo remove this function, use sendTWIGMessageBySES
     * @param type $account
     * @param type $message
     * @param type $action
     * @return type
     */
    public function sendCertificate($account, $message, $action = 'accept') {
        $templateName = $this->parameters['template']['certificate'][$action];
        $context = array(
            'account' => $account,
            'message' => $message,
            'action'  => $action,
        );
        
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        // $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);
        $to = $account->getEmail();
        $bcc[] = "vlanceantispam@gmail.com";
        return $this->sendEmailBySES($this->parameters['from_email']['hotro'], $to, $subject, $htmlBody, $bcc);
    }
    
    /**
     * @todo remove this function, use sendTWIGMessageBySES
     * @param type $templateName
     * @param type $context
     * @param type $to
     */
    public function sendMessageCommandline($templateName, $context, $to) {
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        // $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        /**
         * Use php mail function - will fixed later
         */
        $this->sendEmail($to, $subject, $htmlBody, $this->parameters['from_email']['hotro']);
    }
    
    /**
     * Send an email to multi receiver from noreply@vlance.vn
     * @param string $templateName template name
     * @param array $context variable used in template
     * @param array $to list of receivers
     * @return type
     */
    public function sendBatchMessage($templateName, $context, array $to) {
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        // $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);
        return $this->sendEmailBySES($this->parameters['from_email']['noreply'], $this->parameters['from_email']['noreply'], $subject, $htmlBody, $to);
    }
    
    /**
     * @todo remove this function, use sendEmailBySES instead of
     * @param type $to
     * @param type $subject
     * @param type $content
     * @param type $from
     */
    protected function sendEmail($to, $subject, $content, $from) {
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: vLance <' . $from . '>' . "\r\n";
        if (mail($to, $subject, $content, $headers)) {
            echo "sent\n";
        } else {
            echo "shit\n";
        };
    }
    
    /**
     * This function used to send an email to 1 receiver - this is main function to send email, all other will be deleted
     * @param string $templateName template name for .html.twig file
     * @param array $context variables used in template file
     * @param string $from from email
     * @param string $to receiver email address
     * @return type
     */
    public function sendTWIGMessageBySES($templateName, $context, $from, $to, array $bcc = array(), $checkEmail = true) {
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $htmlBody = $template->renderBlock('body_html', $context);
        try {
            return $this->sendEmailBySES($from, 'hoan.bui@vlance.vn', $subject, $htmlBody, $bcc, $checkEmail);
        } catch (Exception $ex) {
            return false;
        }
        return false;
    }
    
    public function sendEmailBySES($from, $to, $subject, $content, array $bcc = array(), $checkEmail = true) {
        $sesConfig = array(
            'key'    => 'AKIAJ5N6L4V7RUXJAGEA',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        // Check if email is verified
        if($checkEmail && array_search('vlanceantispam@gmail.com', array($to)) === FALSE && array_search('hotro@vlance.vn', array($to)) === false ) {
            $em = $this->_container->get('doctrine')->getManager();
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($to);
            if ($account) {
                if (!$account->getEmailVerified()) {
                    return false;
                }
            } 
        }
        /*
         * this code to test on local - must be comment when update to server
         */
        // $to = "nvt2302@gmail.com";
        // $bcc = array();
        // $content = "This email is sent to $to but in test we don't send<br/> $content";
        /**
         * end code for local
         */
        
        $email = array(
            // Source is required
            'Source' => (is_array($from)) ? ('=?utf-8?B?' . base64_encode($from['name']). '?=' . " <" . $from['email'] . ">") : ('vLance <' . $from . '>'),
            // Destination is required
            // Set to addresses in bcc addresses to make sure that receivers can't see other email address
            'Destination' => array(
                'ToAddresses' => array($to),
                'BccAddresses' => $bcc,
            ),
            // Message is required
            'Message' => array(
                // Subject is required
                'Subject' => array(
                    // Data is required
                    'Data' => $subject,
                    'Charset' => 'UTF-8',
                ),
                // Body is required
                'Body' => array(
                    'Text' => array(
                        // Data is required
                        'Data' => strip_tags($content),
                        'Charset' => 'UTF-8',
                    ),
                    'Html' => array(
                        // Data is required
                        'Data' => $content,
                        'Charset' => 'UTF-8',
                    ),
                ),
            ),
            'ReplyToAddresses' => array($this->parameters['from_email']['hotro']),
        );
        $sesClient = SesClient::factory($sesConfig);
        try {
            $to_address = $email['Destination']['ToAddresses']['0'];
            $last_string = substr(strrchr($to_address, "@"), 1);

            if($last_string != "facebook.com"){
                $result = $sesClient->sendEmail($email);
            }
            // Logging email, temoparily commented
            /*$em = $this->_container->get('doctrine')->getManager();
            $emaillog = new EmailLog();
            $emaillog->setFrom('vLance <' . $from . '>');
            $emaillog->setTo((string)$to);
            $emaillog->setHeader($subject);
            $emaillog->setHtmlContent($content);
            $emaillog->setTextContent(strip_tags($content));
            $em->persist($emaillog);
            $em->flush();*/
        } catch (Exception $ex) {
            return $ex;
        }
        
        return $result;
    }
    
    public function sendEmailAttachBySES($templateName, $context, $from, $to, $file_attach = '', $file_name, array $bcc = array()) {
        
        $template = $this->twig->loadTemplate($templateName);
        $subject = $template->renderBlock('subject', $context);
        $htmlBody = $template->renderBlock('body_html', $context);
        
        $sesConfig = array(
            'key'    => 'AKIAI7SFZ4QKWICGO7IQ',
            'secret' => '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',
            'region' => 'us-east-1'
        );
        // Check if email is verified
       if(array_search('vlanceantispam@gmail.com', array($to)) === FALSE && array_search('hotro@vlance.vn', array($to)) === false ) {
            $em = $this->_container->get('doctrine')->getManager();
            /* @var $account \Vlance\AccountBundle\Entity\Account */
            $account = $em->getRepository('VlanceAccountBundle:Account')->findOneByEmail($to);
            if ($account) {
                if (!$account->getEmailVerified()) {
                    return false;
                }
            } else {
                return false;
            }
        }
        /*
         * this code to test on local - must be comment when update to server
         */
        $file_size = filesize($file_attach);
        $handle = fopen($file_attach, "r");
        $file_content = fread($handle, $file_size);
        $file_content = file_get_contents($file_attach);
        $file_content = chunk_split(base64_encode($file_content));
        fclose($handle);
        
        $header = "";
        $uid = md5(uniqid(time()));
        $header = 'From: vLance <' . $from . '>' . "\r\n";
        $header .= "Reply-To: ".$from."\r\n";
        $header .= "To: ".$to."\r\n";
        $header .= "Bcc: ".$to."\r\n";
        $header .= "Subject: ".$subject."\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
        $header .= "--".$uid."\r\n";
        
        $header .= "Content-type:text/html; charset=UTF-8\r\n";
        $header .= "Content-Transfer-Encoding: quoted-printable\r\n\r\n";
        $header .= $htmlBody."\r\n\r\n";
        $header .= "--".$uid."\r\n";
        
        $header .= "Content-Type: ".mime_content_type($file_attach)."; name=\"".$file_name."\"\r\n";
        $header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$file_name."\"; size=".filesize($file_attach)."\r\n\r\n";
        $header .= $file_content."\r\n";
        $header .= "--".$uid."--";
        
        $msg = array(
            'Source' => 'vLance <' . $from . '>',
            'Destinations' => array(
                'ToAddresses' => $to,
//                'BccAddresses' => array(implode(',', $bcc)),
            ),
            'RawMessage' => array(
                'Data' => base64_encode($header),
            ),
        );
        
        $sesClient = SesClient::factory($sesConfig);
        try{
            $result = $sesClient->sendRawEmail($msg);
            //save the MessageId which can be used to track the request
            $msg_id = $result->get('MessageId');
        } catch (Exception $ex) {
            return $ex;
        }
        
        return $result;
    }
    /*
     * Not tested yet
    public function isSendable($email)
    {
        return $this->isEmailValid($email);
    }
    
    public function isEmailValid($email)
    {
        if(preg_match(`@facebook.com$`, $email)){
            return false;
        } else
            return true;
    }*/
}
