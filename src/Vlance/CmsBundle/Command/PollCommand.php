<?php
namespace Vlance\CmsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Vlance\PaymentBundle\Entity\UserCash;

use Vlance\AccountBundle\Entity\Account;

class PollCommand extends ContainerAwareCommand {
    
    protected function configure() {
        $this
                ->setName('vlance:cms:poll')
                ->setDescription('Vlance command for account')
                ->addArgument('poll', InputArgument::REQUIRED, 'Poll id that you want to send newsletter');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->getContainer()->get('router')->getContext()->setHost('www.vlance.vn');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $poll = $em->getRepository('VlanceCmsBundle:Poll')->find($input->getArgument('poll'));
        $limited = explode(",", $poll->getLimited());
        $accounts = $em->getRepository('VlanceAccountBundle:Account')->findBy(array("id" => $limited));
        foreach ($accounts as $account) {
            $this->getContainer()->get('vlance.mailer')->sendPollNewsletter($poll, $account);
        }
    }
}
