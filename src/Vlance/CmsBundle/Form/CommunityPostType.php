<?php

namespace Vlance\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommunityPostType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('title', null, array(
                'label' => 'form.post.title.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder'       => 'form.post.title.placeholder',
                    'data-toggle'       => 'popover',
                    'data-placement'    => 'top',
                    'data-content'      => 'form.job.popover.title',
                    'data-trigger'      => 'hover',
                    'class'             => 'popovers-input span12'
                    )
                ))
            ->add('description', null, array(
                'label' => 'form.post.description.label',
                'translation_domain' => 'vlance',
                'required' => true,
                'attr' => array(
                    'placeholder'       => 'form.post.description.placeholder',
                    'data-toggle'       => 'popover',
                    'data-placement'    => 'top',
                    'data-trigger'      => 'hover',
                    'class'             => 'popovers-input span12',
                    'rows'              => '14')
                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Vlance\CmsBundle\Entity\CommunityPost',
            'cascade_validation' => true,
        ));
    }

    public function getName() {
        return 'vlance_cmsbundle_communityposttype';
    }

}
