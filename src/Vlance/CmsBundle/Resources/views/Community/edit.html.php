<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $view['slots']->start('content')?>
<div class="community-post-new">
    <div class="container">
        <?php echo $view->render('VlanceCmsBundle:Community/edit:form.html.php', array('form' => $form, 'entity' => $entity)) ?>
    </div>    
</div>
<?php $view['slots']->stop();?>