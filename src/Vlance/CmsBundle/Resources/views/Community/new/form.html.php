<form id="community-post-form" action="<?php echo $view['router']->generate('new_post_submit') ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
    <div class="form-title">
        <?php echo $view['form']->row($form['title']); ?>
    </div>
    <div class="clear"></div>

    <div class="form-desc">
        <?php echo $view['form']->row($form['description']); ?>
    </div>
    <div class="clear"></div>

    <div class="form-tag">
        <label for="vlance_cmsbundle_communityposttype_tag"><?php echo $view['translator']->trans('form.post.tag.label', array(), 'vlance') ?></label>
        <div class="form-add-tag">    
            <div class="inner-tag">
                <input type="text" id="vlance_cmsbundle_communityposttype_tag" name="hiddenTagListA" placeholder="<?php echo $view['translator']->trans('form.post.tag.placeholder', array(), 'vlance') ?>" class="tm-input" required/>
                <?php /* <input type="text" id="vlance_cmsbundle_communityposttype_tag" data-provide="typeahead" name="hiddenTagListA" placeholder="<?php echo $view['translator']->trans('form.post.tag.placeholder', array(), 'vlance') ?>" class="tm-input"/>
                <script type="text/javascript"> 
                    var tags = <?php echo $view['actions']->render($view['router']->generate('tag_list')) ?>;
                </script> */ ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div>
        <?php echo $view['form']->row($form['_token'])?>
    </div>
    <div class="clear"></div>
     
    <div class="form-submit">
        <input id="btn-submit-community-post" type="submit" class="btn btn-vl btn-vl-small btn-vl-green" value="Gửi bài viết"/>
    </div>
</form>

<script type="text/javascript">
    var upload_image = "<?php echo $view['router']->generate('upload_image', array()) ?>";
</script>
