<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $view['slots']->start('content')?>
<div class="community-post-new">
    <div class="container">
        <div class="row-fluid">
            <div class="content-text">
                <h1>Đăng bài viết</h1>
                <div class="category-desc">
                    <p>Bạn có thể chia sẻ những kinh nghiệm quý báu của mình với cộng đồng bằng cách gửi bài viết ở bên dưới hoặc email tới hòm thư <b>community@vlance.vn</b><br/><br/></p>
                    <p>Với mỗi bài viết chất lượng được đăng bạn sẽ nhận được tiền thưởng lên tới <b>100.000 VNĐ</b>.</p>
                </div>
            </div>  
        </div>
        
        <?php echo $view->render('VlanceCmsBundle:Community/new:form.html.php', array('form' => $form, /*'acc' => $acc*/)) ?>

    </div>    
</div>
<?php $view['slots']->stop();?>