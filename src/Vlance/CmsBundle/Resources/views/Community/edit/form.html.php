<form id="community-post-form" action="<?php echo $view['router']->generate('community_post_update', array('hash' => $entity->getHash())) ?>" method="post" <?php echo $view['form']->enctype($form) ?>>
    <div class="form-title">
        <?php echo $view['form']->row($form['title']); ?>
    </div>
    <div class="clear"></div>

    <div class="form-desc">
        <?php echo $view['form']->row($form['description']); ?>
    </div>
    <div class="clear"></div>

    <div class="form-tag">
        <?php $tag_arr = array(); ?>
        <?php foreach($entity->getTags() as $t): ?>
            <?php $tag_arr[] = $t->getTitle(); ?>
        <?php endforeach; ?>
        <?php $tag_arr = implode(',', $tag_arr); ?>
        <label for="vlance_cmsbundle_communityposttype_tag"><?php echo $view['translator']->trans('form.post.tag.label', array(), 'vlance') ?></label>
        <div class="form-add-tag">    
            <div class="inner-tag">
                <input style="text-align: left;" type="text" id="vlance_cmsbundle_communityposttype_tag" name="hiddenTagListA" placeholder="<?php echo $view['translator']->trans('form.post.tag.placeholder', array(), 'vlance') ?>" 
                       class="tm-input" required value="<?php echo $tag_arr; ?>"/>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div>
        <?php echo $view['form']->row($form['_token'])?>
    </div>
    <div class="clear"></div>
     
    <div class="form-submit">
        <input id="btn-submit-community-post" type="submit" class="btn btn-vl btn-vl-small btn-vl-green" value="Lưu chỉnh sửa"/>
    </div>
</form>

<script type="text/javascript">
    var upload_image = "<?php echo $view['router']->generate('upload_image', array()) ?>";
</script>
