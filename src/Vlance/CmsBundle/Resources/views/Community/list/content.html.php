<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>

<?php if($pos % 3 == 0):?>
<div class="row-fluid">
<?php endif;?>
    <div class="show-case-item span4">
        <div class="item-content">
            <div class="item-img">
                <?php $resize = ResizeImage::resize_image($entity->getFullPath(), '336x197', 336, 197, 1);?>
                <?php if($resize) : ?>
                    <a href="<?php echo $view['router']->generate('community_post_show',array('hash' => $entity->getHash())) ?>"><img src="<?php echo $resize; ?>"/></a>
                <?php else: ?>
                    <a href="<?php echo $view['router']->generate('community_post_show',array('hash' => $entity->getHash())) ?>"><img src="../media/Trang-community-1.jpg"></a>
                <?php endif; ?>

            </div>
            <div>
                <a href="<?php echo $view['router']->generate('community_post_show',array('hash' => $entity->getHash())) ?>" class="item-title"><?php echo $entity->getTitle() ?></a>
            </div>
        </div>
        <div class="properties-post row-fluid">
            <div class="span7">
                <?php $writer = $entity->getAccount(); ?>
                <span>
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $writer->getHash())) ?>">
                        <?php $resize = ResizeImage::resize_image($writer->getFullPath(), '20x20', 20, 20, 1);?>
                        <?php if($resize) : ?>
                        <img width="20" height="20" src="<?php echo $resize; ?>" alt="<?php echo $writer->getFullName(); ?>" title="<?php echo $writer->getFullName(); ?>" />
                        <?php else: ?>
                            <img width="20" height="20" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $writer->getFullName(); ?>"  title="<?php echo $writer->getFullName(); ?>" />
                        <?php endif; ?>
                    </a>
                </span>

                <span class="post-like">
                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                    <p id="show-likes">
                        <?php if($like > 999): ?>
                            <?php echo number_format($like/1000,1).'k'; ?>
                        <?php elseif ($like > 999999): ?>
                            <?php echo number_format($like/1000000,1).'m'; ?>
                        <?php elseif ($like > 999999999): ?>
                            <?php echo number_format($like/1000000000,1).'b'; ?>
                        <?php else: ?>
                            <?php echo $like; ?>
                        <?php endif; ?>
                    </p>
                </span>
                <span class="post-view"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo $entity->getView() ?></span>
            </div>
            <div class="post-datetime span5">
                <?php echo $time; ?>
            </div>    
        </div>
    </div>
<?php if($pos % 3 == 2 || $pos == count($entities) - 1): ?>
</div>
<?php endif; ?> 