<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>

<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $view['slots']->start('content'); ?>
<div class="community-view">
    <div class="container">
        <div class="head-view row-fluid">
            <?php $writer = $entity->getAccount(); ?>
            <div class="span7">
                <div class="avata">
                    <a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $writer->getHash())) ?>">
                        <?php $resize = ResizeImage::resize_image($writer->getFullPath(), '40x40', 40, 40, 1);?>
                        <?php if($resize) : ?>
                        <img src="<?php echo $resize; ?>" alt="<?php echo $writer->getFullName(); ?>" title="<?php echo $writer->getFullName(); ?>" />
                        <?php else: ?>
                            <img width="40" height="40" src = "<?php echo $view['assets']->getUrl('img/unknown.png') ?>" alt="<?php echo $writer->getFullName(); ?>"  title="<?php echo $writer->getFullName(); ?>" />
                        <?php endif; ?>
                    </a>
                </div>
                <p><a href="<?php echo $view['router']->generate('account_show_freelancer',array('hash' => $writer->getHash())) ?>"><?php echo $writer->getFullName(); ?></a></p>
            </div>
<!--            <div class="sumome-share span5">
                <p>social network</p>
            </div>-->
        </div>
        
        <div class="content-post">
            <div class="row-fluid">
                <div class="title-post">
                    <h1><?php echo $entity->getTitle(); ?></h1>
                </div>
                <?php if(HasPermission::hasAdminPermission($acc) == TRUE ) : ?>    
                <div class="edit-link-post">
                    <h3>
                        <a href="<?php echo $view['router']->generate('community_post_edit',array('hash' => $entity->getHash())) ?>" class="edit-link" title="Chỉnh sửa">Chỉnh sửa(User)</a> || 
                        <a target="_blank" href="<?php echo $view['router']->generate('Vlance_CmsBundle_AdminCommunityPost_edit',array('pk' => $entity->getId())) ?>" class="edit-link" title="Chỉnh sửa">Chỉnh sửa(Admin)</a>
                    </h3>
                </div>
                <?php endif; ?>
            </div>
            <div class="properties-post">
                <?php /* <span class="datetime-post"><?php echo date_format($entity->getCreatedAt(),"d F Y"); ?></span> */ ?>
                <span class="datetime-post"><?php echo date_format($entity->getCreatedAt(),"d/m/Y"); ?></span>
                
                <span class="like-post"> 
                <?php if(is_object($acc)): ?>
                    <label id="icon-btn-like">
                        <i id="icon-like" class="fa fa-heart <?php if($liker == true) echo "liked"?>" aria-hidden="true"></i>
                    </label>
                <?php else: ?>
                    <label>
                        <i class="fa fa-heart" aria-hidden="true"></i>
                    </label>
                <?php endif; ?>
                </span>
                <span class="show-like">
                    <label id="show-likes" class="<?php if(is_object($acc) && $liker == true) echo "liked"; ?>">
                        <?php $l = $like; ?>
                        <?php if($l > 999): ?>
                            <?php echo number_format($l/1000,1).'k'; ?>
                        <?php elseif ($l > 999999): ?>
                            <?php echo number_format($l/1000000,1).'m'; ?>
                        <?php elseif ($l > 999999999): ?>
                            <?php echo number_format($l/1000000000,1).'b'; ?>
                        <?php else: ?>
                            <?php echo $l; ?>
                        <?php endif; ?>
                    </label>
                </span>
                <script type="text/javascript">
                    var pid = "<?php echo $entity->getId(); ?>";
                    var url = "<?php echo $view['router']->generate('like_post',array()); ?>";
                </script>
                
                <span class="view-post"><i class="fa fa-eye" aria-hidden="true"></i> 
                    <?php $n = $entity->getView(); ?>
                    <?php if($n > 999): ?>
                        <?php echo number_format($n/1000,1).'k'; ?>
                    <?php elseif ($n > 999999): ?>
                        <?php echo number_format($n/1000000,1).'m'; ?>
                    <?php elseif ($n > 999999999): ?>
                        <?php echo number_format($n/1000000000,1).'b'; ?>
                    <?php else: ?>
                        <?php echo $n; ?>
                    <?php endif; ?>
                </span>
                <?php if(!is_null($entity->getTags())): ?>
                    <?php foreach($entity->getTags() as $t): ?>
                        <span class="tag-post"><?php echo $t->getTitle() ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="description-post">
                <?php echo $entity->getDescription(); ?>
            </div>
        </div>
        
        <div class="block-info-vlance">
            <div class="container">
                <div class="button">
                    <a href="#" class="btn-vl btn-vl-large btn-vl-green">Thuê freelancer</a>
                </div>
                <p>Hoặc liên lạc với vLance để được tư vấn và hỗ trợ tìm freelancer miễn phí.</p>
                <p>Website: <a href="<?php echo $view['router']->generate('vlance_homepage',array()); ?>">http://www.vlance.vn</a></p>
                <p>Số điện thoại hỗ trợ: 04 - 6684 - 1818</p>
            </div>    
        </div>
    </div>
    <div class="view-more">
        <div class="container">
            <h3>Các bài viết liên quan</h3>
            <div>
                <a href="<?php echo $view['router']->generate('community_post',array()); ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Khách hàng quản lý tiền dự án trên vLance như thế nào?
                </a>
                <a href="<?php echo $view['router']->generate('community_post',array()); ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Mẫu cam kết làm việc cho khách hàng và freelancer
                </a>
                <a href="<?php echo $view['router']->generate('community_post',array()); ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Số tiền cần nạp cho một dự án trên vLance?
                </a>
                <a href="<?php echo $view['router']->generate('community_post',array()); ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Vì sao cần liên hệ và thực hiện giao dịch qua vLance?
                </a>
            </div>
        </div>    
    </div>
    <div class="bottom-view-section">
        <div class="container">
            <h1>Cần thuê Freelancer làm việc cho bạn?</h1>
            <p>Các freelancer hàng đầu là những chuyên gia có từ 3 năm kinh nghiệm trở lên</p>
            <div class="button">
                <a href="<?php echo $view['router']->generate('freelancer_list',array()); ?>" class="btn-vl btn-vl-large btn-vl-green">Thuê freelancer</a>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop(); ?>