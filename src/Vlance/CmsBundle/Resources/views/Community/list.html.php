<?php use Vlance\BaseBundle\Utils\ResizeImage; ?>
<?php use Vlance\BaseBundle\Utils\HasPermission; ?>

<?php $view->extend('VlanceBaseBundle::layout-1column.html.php')?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<?php $view['slots']->start('content')?>

<div class="community-list">
    <div class="head-block">
        <div class="container">
            <h1>Cộng đồng - Chia sẻ kinh nghiệm</h1>
            <p>Những hướng dẫn, mẹo, nguồn cho công việc...</p>
            <div class="query-input-container">
                <input placeholder="Tìm hướng dẫn">
                <span><i class="fa fa-search" aria-hidden="true"></i></span>
            </div>
        </div>
    </div>
    <div class="body-view">
        <div class="filter-bar">
            <div id="fadeable">
                <ul class="filters">
                    <li><a class="active">Mới đăng</a></li>
                    <li><a>Phổ biến</a></li>
                </ul>
                <a href="<?php echo $view['router']->generate('community_post_new',array()) ?>" class="button btn-vl btn-vl-medium btn-vl-green">Đăng bài viết</a>
            </div>
        </div>
        <div class="container">
            <div class="show-case-list">
                <?php if(count($entities) > 0): ?>
                    <?php $pos = 0; ?>
                    <?php foreach($entities as $e): ?>
                        <?php $entity = $e['entity']; $like = $e['like']; $time = $e['time']; ?>
                        <?php if(HasPermission::hasAdminPermission($acc) == TRUE ) : ?>
                            <?php echo $view->render('VlanceCmsBundle:Community/list:content.html.php', array('entities' => $entities, 'like' => $like, 'entity' => $entity, 'time' => $time, 'pos' => $pos)) ?>
                        <?php else: ?>
                            <?php if(!is_null($entity->getPublishedAt())): ?>
                                <?php echo $view->render('VlanceCmsBundle:Community/list:content.html.php', array('entities' => $entities, 'like' => $like, 'entity' => $entity, 'time' => $time, 'pos' => $pos)) ?>
                            <?php endif; ?> 
                        <?php endif; ?> 
                        <?php $pos++; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
<!--            <div class="layered-footer results-paging">
                <span class="current">1</span>
                <a>2</a>
                <a>3</a>
                <a>4</a>
                <a>5</a>
            </div>-->
        </div>
    </div> 
</div>

<?php $view['slots']->stop();?>