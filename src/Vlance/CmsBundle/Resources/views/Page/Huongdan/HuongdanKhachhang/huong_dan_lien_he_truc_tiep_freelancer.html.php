<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-contact-fl">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng liên hệ trực tiếp</h1>
            <p class="body-text">Các bước thực hiện để liên hệ trực tiếp với freelancer mà bạn muốn.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Vào mục tìm freelancer</h3>
            <p class="body-text">Bấm vào nút Tìm freelancer hiển thị trên thanh Menu.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-lien-he-truc-tiep-voi-freelancer-buoc-1.jpg')?>" title="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 1" alt="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 1">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Danh sách các ứng viên</h3>
            <p class="body-text">Khi vào mục <b>Tìm freelancer</b>, bạn sẽ thấy danh sách các freelancer như hình sau:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-lien-he-truc-tiep-voi-freelancer-buoc-2-anh-1.jpg')?>" title="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 2-Ảnh 1" alt="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 2-Ảnh 1">
            </div>
            <p class="separater-text">Bấm vào freelancer mà bạn muốn liên hệ, sau đó bấm nút <b>Liên hệ trực tiếp</b> như hình sau:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-lien-he-truc-tiep-voi-freelancer-buoc-2-anh-2.jpg')?>" title="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 2-Ảnh 2" alt="Hướng dẫn liên hệ trực tiếp với freelancer-Bước 2-Ảnh 2">
            </div>
            <p class="separater-text">Tiếp theo, bấm vào nút <b>Liên hệ ngay</b> để xác nhận yêu cầu của bạn. Bạn sẽ cần 5 credit để thực hiện yêu cầu này.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-lien-he-truc-tiep-voi-freelancer-buoc-2-anh-3.jpg')?>" title="Hướng dẫn liên hệ trực tiếp với freelancer bước 2 ảnh 3" alt="Hướng dẫn liên hệ trực tiếp với freelancer bước 2 ảnh 3">
            </div>
            <p class="separater-text">Sau khi bạn hoàn thành các bước trên, thông tin của freelancer sẽ được gửi tới email của bạn.</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_dang_viec',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn đăng việc
                </a>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit 
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>