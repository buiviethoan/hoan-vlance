<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>


<div class="guider-deposit">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng nạp tiền</h1>
            <p class="body-text">Hiện tại vLance đang cung cấp một số hình thức nạp tiền như sau:</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Nạp tiền trực tiếp tại văn phòng vLance</h3>
            <p class="body-text">Nếu bạn ở Hà Nội và muốn giao dịch trực tiếp với chúng tôi, vLance sẵn sàng nhận nạp tiền tại địa chỉ:</p>
            <i>Văn phòng giao dịch: <b>Tầng 4, số 2, ngõ 68, đường Nam Đồng, phường Nam Đồng, quận Đống Đa, Hà Nội</b><br/>
               Số điện thoại: (04) 6684.1818
            </i>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Chuyển khoản ngân hàng</h3>
            <p class="body-text">Vui lòng làm theo hướng dẫn chuyển khoản khi thanh toán mua credit hoặc nạp tiền dự án được gửi qua email. Vui lòng cộng thêm 10% VAT khi thực hiện chuyển khoản.</p>
            
            <?php /* <p class="body-text">Bạn vui lòng điền thông tin chuyển khoản cho vLance như sau:</p>
            <br/>
            <ul class="nav nav-tabs" id="bank-list-tab">
                <li class="active"><a href="#vietcombank" data-toggle="tab">Vietcombank</a></li>
                <li><a href="#acb" data-toggle="tab">ACB</a></li>
                <li><a href="#techcombank" data-toggle="tab">Techcombank</a></li>
                <!--<li><a href="#vietinbank" data-toggle="tab">Vietinbank</a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="vietcombank">
                    <dl class="dl-horizontal">
                        <dt>Chủ tài khoản:</dt>
                        <dd>Trần Ngọc Tuân</dd>
                        <dt>Số tài khoản:</dt>
                        <dd>0011004141791</dd>
                        <dt>Ngân hàng:</dt>
                        <dd>Ngân hàng thương mại cổ phần Ngoại Thương - Vietcombank</dd>
                        <dt>Chi nhánh ngân hàng:</dt>
                        <dd>Sở giao dịch, Phòng giao dịch số 152 Thụy Khuê, Tây Hồ, Hà Nội</dd>
                        <dt>Hoặc chi nhánh:</dt>
                        <dd>Trần Quang Khải, Hà Nội</dd>
                    </dl>
                </div>
                <div class="tab-pane" id="acb">
                    <dl class="dl-horizontal">
                        <dt>Chủ tài khoản:</dt>
                        <dd>Trần Ngọc Tuân</dd>
                        <dt>Số tài khoản:</dt>
                        <dd>228085579</dd>
                        <dt>Ngân hàng:</dt>
                        <dd>Ngân hàng thương mại cổ phần Á Châu - ACB</dd>
                        <dt>Chi nhánh ngân hàng:</dt>
                        <dd>Hà Nội</dd>
                    </dl>
                </div>
                <div class="tab-pane" id="techcombank">
                    <dl class="dl-horizontal">
                        <dt>Chủ tài khoản:</dt>
                        <dd>Trần Ngọc Tuân</dd>
                        <dt>Số tài khoản:</dt>
                        <dd>19030881539011</dd>
                        <dt>Ngân hàng:</dt>
                        <dd>Ngân hàng thương mại cổ phần Kỹ Thương Việt Nam - Techcombank</dd>
                        <dt>Chi nhánh ngân hàng:</dt>
                        <dd>Hà Nội</dd>
                    </dl>
                </div>
                <!--<div class="tab-pane" id="vietinbank">
                    <dl class="dl-horizontal">
                        <dt>Chủ tài khoản:</dt>
                        <dd>Trần Ngọc Tuân</dd>
                        <dt>Số tài khoản:</dt>
                        <dd>19030881539011</dd>
                        <dt>Ngân hàng:</dt>
                        <dd>Ngân hàng TMCP Công Thương Việt Nam - Vietinbank</dd>
                        <dt>Chi nhánh ngân hàng:</dt>
                        <dd>Hà Nội</dd>
                    </dl>
                </div>-->
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('#bank-list-tab a#vietcombank').tab('show');
                    $('#bank-list-tab a').click(function (e) {
                        e.preventDefault();
                        $(this).tab('show');
                    })
            });
            </script>
            
            <p class="body-text">Vui lòng ghi nội dung chuyển khoản như sau:</p>
            <div class="id-job">VC + Mã số ID dự án của bạn. <b>Ví dụ: VC12345</b></div> */?>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Chuyển khoản Paypal</h3>
            <p class="body-text">Bạn có thể thanh toán cho vLance theo tài khoản sau: <b>invoice@vlance.vn</b><br/><br/>  
            </p>
            <p class="body-text">Số tiền được tính theo công thức:</p>
            <div class="paypal-card-service">Số tiền USD cần thanh toán = Giá trị dự án theo VND / 21.410 / (1 - 4,4%)</div>
            <p class="body-text">Vui lòng ghi nội dung chuyển khoản như sau:</p>
            <div class="id-job">VC + Mã số ID dự án của bạn. <b>Ví dụ: VC12345</b></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>4</span>Chuyển khoản qua hệ thống Smartlink</h3>
            <p class="body-text">Bạn cũng có thể sử dụng hệ thống Smartlink để thực hiện các giao dịch</p>
            <dl class="dl-horizontal">
                <dt>Số thẻ:</dt>
                <dd>97043668 07710579016</dd>
                <dt>Tên chủ thẻ:</dt>
                <dd>Trần Ngọc Tuân</dd>
            </dl>
            <p class="body-text">Vui lòng ghi nội dung chuyển khoản như sau:</p>
            <div class="id-job">VC + Mã số ID dự án của bạn. <b>Ví dụ: VC12345</b></div>
            <div class="separater-step-2"></div>
        </div>
    </div>
    <div class="block-deposit-note">
        <div class="container">
            <h3>Lưu ý:</h3>
            <p>- Tiền đã nạp cho dự án luôn được giữ trên vLance. Chỉ khi khách hàng xác nhận thanh toán cho freelancer, vLance mới chuyển tiền cho freelancer.</p>
            <p>- Khách hàng có thể chuyển tiền cho nhiều dự án trong cùng một giao dịch. Vui lòng ghi chính xác mã số ID của những dự án đó.</p>
            <p>- Trong trường hợp phát sinh phụ phí chuyển khoản, khách hàng vui lòng thanh toán chi phí này với ngân hàng.</p>
            <p>- Trong trường hợp khách hàng quên không ghi mã số ID dự án khi chuyển tiền, vui lòng gửi biên lai chuyển khoản cho <b>hotro@vlance.vn</b> để được hỗ trợ giải quyết.</p>
            <p>- Nghiêm cấm tất cả các hình thức lôi kéo làm việc hoặc thanh toán ngoài hệ thống vLance. Trong trường hợp freelancer mời bạn làm việc và thanh toán bên ngoài vLance, vui lòng thông báo cho chúng tôi ngay lập tức.</p>
        </div>    
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án cần hóa đơn VAT
                </a>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án theo từng giai đoạn
                </a>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền vào tài khoản vLance và sử dụng dần
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_ket_thuc_du_an',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn kết thúc dự án và thanh toán
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
    </div>
</div>

<?php $view['slots']->stop(); ?>