<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-manage-job">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng quản lý dự án</h1>
            <p>Sau khi nạp tiền, bạn sẽ bắt đầu làm việc với freelancer. Những gợi ý sau đây sẽ giúp bạn quản lý dự án tốt hơn.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Yêu cầu rõ ràng</h3>
            <p class="body-text">Trước khi freelancer bắt đầu làm việc, bạn cần xác nhận rõ yêu cầu để đảm bảo freelancer đã nắm rõ yêu cầu.</p>
            <p class="body-text">Ví dụ:</p>
            <p class="body-text">- Freelancer cần thực hiện những hạng mục công việc gì?<br/>
               - Freelancer cần báo cáo tiến độ như thế nào?<br/>
               - Freelancer cần bàn giao sản phẩm khi nào?<br/>
            </p>
            <div class="separater-step-2"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Nắm rõ tiến độ</h3>
            <div>    
                <p class="body-text">Bạn cần trao đổi với freelancer thường xuyên để nắm rõ tiến độ làm việc của freelancer.<br/><br/></p>
                <p class="body-text">Đầu tiên bạn vào Góc làm việc rồi bấm vào tên dự án công việc của bạn, sau đó bạn vào mục Tin nhắn.<br/>
                   Trong trang Tin nhắn bạn hãy cập nhật tiến độ làm việc và các thỏa thuận quan trọng khác như hình sau đây:
                </p>
                <div>
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-quan-ly-du-an-buoc-2.jpg')?>" title="Hướng dẫn khách hàng đăng việc - Bước 1 - Điền thông tin - Ảnh 1" alt="Hướng dẫn khách hàng đăng việc - Bước 1 - Điền thông tin - Ảnh 1">
                </div>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Liên hệ với vLance để được hỗ trợ</h3>
            <p class="body-text">Bạn có thể liên hệ với vLance qua hòm thư hotro@vlance.vn hoặc số điện thoại (04) 6684.1818 khi gặp khó khăn cần hỗ trợ.</p>
        </div>    
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Quy định đăng việc trên vLance.vn
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_chon_freelancer',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chọn freelancer hiệu quả
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án
                </a>
            </div>
        </div>
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
    </div>
</div>	

<?php $view['slots']->stop(); ?>