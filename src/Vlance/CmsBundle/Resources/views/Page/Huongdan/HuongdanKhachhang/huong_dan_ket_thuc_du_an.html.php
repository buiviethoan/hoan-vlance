<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-manage-job">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng kết thúc dự án</h1>
            <p>Quy trình kết thúc dự án và thanh toán sẽ được thực hiện như sau:</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Xác nhận kết thúc dự án</h3>
            <p class="body-text">Khi đã ưng ý với sản phẩm, bạn muốn đóng dự án và thanh toán cho freelancer bạn vào phần Tin nhắn của dự án để bấm <b>Xác nhận kết thúc dự án.</b></p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-ket-thuc-du-an-buoc-1.jpg')?>" title="Hướng dẫn khách hàng kết thúc dự án - Bước 1 - Xác nhận kết thúc dự án" alt="Hướng dẫn khách hàng kết thúc dự án - Bước 1 - Xác nhận kết thúc dự án">
            </div>
            <p class="body-text">Sau khi bạn xác nhận kết thúc dự án, tiền dự án sẽ được chuyển ngay cho freelancer</p>
            <div class="separater-step-2"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Đánh giá freelancer</h3>
            <div>    
                <p class="body-text">Khi xác nhận kết thúc dự án, bạn hãy bấm nút <b>Đánh giá freelancer</b> ngay trong trang Tin nhắn.</p>
                <div>
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-ket-thuc-du-an-buoc-2-anh-1.jpg')?>" title="Hướng dẫn khách hàng kết thúc dự án - Bước 2 - Đánh giá freelancer - Ảnh 1" alt="Hướng dẫn khách hàng kết thúc dự án - Bước 2 - Đánh giá freelancer - Ảnh 1">
                </div>
                <p class="separater-text">Sau khi bấm nút màn hình hiện lên 6 tiêu chí <b>Đánh giá chất lượng dịch vụ</b> và nhận xét về freelancer.</p>
                <div>
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-ket-thuc-du-an-buoc-2-anh-2.jpg')?>" title="Hướng dẫn khách hàng kết thúc dự án - Bước 2 - Đánh giá freelancer - Ảnh 2" alt="Hướng dẫn khách hàng kết thúc dự án - Bước 2 - Đánh giá freelancer - Ảnh 2">
                </div>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-deposit-note">
        <div class="container">
            <h3>Lưu ý:</h3>
            <p class="body-text">- Trong trường hợp xảy ra tranh chấp hoặc sự cố không thể hoàn thành dự án, số tiền của dự án sẽ được bảo toàn 100% và không chịu bất kì một phí tổn nào với vLance.vn.</p>
            <p class="body-text">- Số tiền của dự án sẽ được chuyển về tài khoản vLance của khách hàng. Khách hàng có thể sử dụng số tiền này để thanh toán cho các lần giao dịch tiếp theo trên vLance hoặc yêu cầu hoàn trả số tiền này.</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Quy định đăng việc trên vLance.vn
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_chon_freelancer',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chọn freelancer hiệu quả
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án
                </a>
            </div>
        </div>
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
    </div>
</div>	

<?php $view['slots']->stop(); ?>