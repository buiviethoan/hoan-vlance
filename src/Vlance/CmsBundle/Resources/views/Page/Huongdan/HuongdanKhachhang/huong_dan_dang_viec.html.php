<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-post-job">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng đăng việc</h1>
            <p>Đăng việc tại vLance rất nhanh chóng và đơn giản. Bạn chỉ cần thực hiện 3 bước sau:</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Vào mục đăng việc</h3>
            <p class="body-text">Bấm vào nút đăng việc hiển thị trên thanh Menu.</p>
            <p class="body-text">Mẫu đăng việc của vLance đã sẵn sàng để bạn sử dụng.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-dang-viec-buoc1-dangviec.jpg')?>" title="Hướng dẫn khách hàng đăng việc-Bước 1-Đăng việc" alt="Hướng dẫn khách hàng đăng việc-Bước 1-Đăng việc">
            </div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Điền thông tin</h3>
            <div>    
                <p class="body-text">Sau khi vào mục <b>Đăng việc</b>, bạn sẽ thấy mẫu để tiền tên công việc và mô tả như hình sau.</p>
                <div>
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-dang-viec-buoc2-dien-thong-tin-anh-1.jpg')?>" title="Hướng dẫn khách hàng đăng việc - Bước 1 - Điền thông tin - Ảnh 1" alt="Hướng dẫn khách hàng đăng việc - Bước 1 - Điền thông tin - Ảnh 1">
                </div>
                <div class="guider-step-note">
                    <h4>Viết tên việc ngắn gọn</h4>
                    <i>Ví dụ:<br/>
                    - Dịch báo Việt - Anh đề tài du lịch<br/>
                    - Thiết kế trang web bán hàng</i>
                </div>
                <div class="guider-step-note">
                    <h4>Mô tả rõ ràng</h4>
                    <i>- Nên viết mô tả bằng tiếng Việt có dấu<br/>
                    - Mỗi yêu cầu thể hiện bằng 1 gạch đầu dòng (làm việc gì, yêu cầu thế nào về kinh nghiệm...)
                    </i>
                </div>
            </div>
            <div>
                <p class="separater-text">Tiếp theo bạn cần chọn ngân sách cho công việc và thêm kỹ năng trong các danh mục như hình sau đây:</p>
                <div>
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-dang-viec-buoc2-dien-thong-tin-anh-2.jpg')?>" title="Hướng dẫn khách hàng đang việc-Bước 2-Điền thông tin-Ảnh 2" alt="Hướng dẫn khách hàng đang việc-Bước 2-Điền thông tin-Ảnh 2">
                </div>
                <div class="guider-step-note">
                    <h4>Thêm kỹ năng</h4>
                    <i>- Chọn 1 vài kỹ năng cần nhất với yêu cầu dự án và thêm vào cuối dự án.<br/>
                        - Tag kỹ năng giúp freelancer liên quan tiếp cận dự án của bạn nhanh hơn.
                    </i>
                </div>
                <div class="guider-step-note">
                    <h4>Chọn ngân sách hấp dẫn</h4>
                    <i>- Hãy đưa ra mức ngân sách bằng đúng khả năng chi trả của bạn.<br/>
                       - Con số phải thực tế. Nếu quá thấp hoặc quá cao freelancer sẽ cho rằng bạn thiếu nghiêm túc. 
                    </i>
                </div>
                <div class="guider-step-note">
                    <h4>Việc bí mật</h4>
                    <i>- Nếu bạn không muốn chia sẻ công việc với nhiều người, hãy chọn việc <b>bí mật</b>.<br/>
                       - Chỉ bạn và freelancer được mời tham gia công việc mới có thể xem thông tin. 
                    </i>
                </div>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Đăng việc lên vLance</h3>
            <p class="body-text">Sau khi hoàn tất các bước ở trên bạn hãy bấm vào nút <b>Đăng việc</b>.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-dang-viec-buoc3-dang-viec-len-vlance.jpg')?>" title="Hướng dẫn khách hàng đăng việc-Bước 3-Đăng việc lên vLance">
            </div>
            <p class="body-text">Nếu có nhu cầu chỉnh sửa sau khi đăng việc, bạn có bấm vào <b>Chỉnh sửa</b></p>
        </div>    
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Quy định đăng việc trên vLance.vn
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_chon_freelancer',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chọn freelancer hiệu quả
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án
                </a>
            </div>
        </div>
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
    </div>
</div>	

<?php $view['slots']->stop(); ?>