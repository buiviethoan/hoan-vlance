<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-choose-fl">
    <div class="block-head">
        <div class="container">
            <h1>Hướng dẫn khách hàng chọn freelancer</h1>
            <p class="body-text">Những gợi ý sau đây của vLance sẽ giúp bạn chọn freelancer một cách dễ dàng.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Xem xét các chào giá mà bạn nhận được</h3>
            <p class="body-text">Đầu tiên bạn vào <b>Góc làm việc</b>, sau đó bạn bấm vào tên dự án công việc của bạn.</b></p>
            <p class="body-text">Hãy xem một lượt những chào giá mà nhận được và chọn lọc các ứng viên tiềm năng nhất.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-chon-freelancer-buoc-1-xem-xet-chao-gia.jpg')?>" title="Hướng dẫn khách hàng chọn freelancer bước 1 xem xét chào giá" alt="Hướng dẫn khách hàng chọn freelancer bước 1 xem xét chào giá">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Nhắn tin để trao đổi thêm</h3>
            <p class="body-text">Dùng chức năng <b>Nhắn tin</b> để liên hệ và trao đổi với những freelancer mà bạn thấy tiềm năng. Việc trao đổi này sẽ giúp freelancer hiểu yêu cầu của bạn hơn và giúp bạn tìm ra được freelacer phù hợp nhất.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-chon-freelancer-buoc-2-nhan-tin-trao-doi.jpg')?>" title="Hướng dẫn khách hàng chọn freelancer bước 2 nhắn tin trao đổi" alt="Hướng dẫn khách hàng chọn freelancer bước 2 nhắn tin trao đổi">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Giao việc cho người phù hợp</h3>
            <p class="body-text">Khi đã quyết định chọn freelancer nào, bạn bấm nút <b>Giao việc</b> trên chào giá của freelancer đó.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-chon-freelancer-buoc-3-giao-viec-anh-1.jpg')?>" title="Hướng dẫn khách hàng chọn freelancer bước 3 giao việc ảnh 1" alt="Hướng dẫn khách hàng chọn freelancer bước 3 giao việc ảnh 1">
            </div>
            <p class="separater-text">Nếu bạn chưa ưng ý với những freelancer đã chào giá, bạn có thể chủ động <b>Mời freelancer</b> khác tham gia.</p>
            <div><img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-khach-hang-chon-freelancer-buoc-3-giao-viec-anh-2.jpg')?>" title="Hướng dẫn khách hàng chọn freelancer bước 3 giao việc ảnh 2" alt="Hướng dẫn khách hàng chọn freelancer bước 3 giao việc ảnh 2"></div>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_ket_thuc_du_an',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn kết thúc dự án
                </a>
                <a href="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn liên hệ trực tiếp
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nạp tiền cho dự án
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>