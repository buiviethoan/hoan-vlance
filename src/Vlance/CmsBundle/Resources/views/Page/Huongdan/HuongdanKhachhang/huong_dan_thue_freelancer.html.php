<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-client">
    <div class="container">
        <div class="title-guider-fl">Cách thuê freelancer trên vLance</div>
    </div>
    <div class="guider-step1-section">
        <div class="container">
            <div class="row-fluid">
                <div class="span7">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-thue-freelancer-buoc-1.png')?>" title="Đăng việc dễ dàng" alt="Đăng việc dễ dàng" >
                </div>
                <div class="guider-text span5">
                    <div class="title">
                        <p>Bước 1</p>
                        <h3>Đăng việc dễ dàng</h3>
                    </div>
                    <p>Đăng việc làm miễn phí</p>
                    <p>Thu hút những freelancer giỏi nhất</p>
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step2-section">
        <div class="container">
            <div class="row-fluid">
                <div class="guider-text span7">
                    <div class="title">
                        <p>Bước 2</p>
                        <h3>Chọn freelancer ưng ý</h3>
                    </div>
                    <p>So sánh các ứng viên</p>
                    <p>Chọn 01 freelancer ưng ý nhất</p>
                </div>
                <div class="span5">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-thue-freelancer-buoc-2.png')?>" title="Chọn freelance ưng ý" alt="Chọn freelance ưng ý">
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step3-section">
        <div class="container">
            <div class="row-fluid">
                <div class="span7">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-thue-freelancer-buoc-3.png')?>" title="Nạp tiền và làm việc" alt="Nạp tiền và làm việc">
                </div>
                <div class="guider-text span5">
                    <div class="title">
                        <p>Bước 3</p>
                        <h3>Nạp tiền và làm việc</h3>
                    </div>
                    <p>Nạp tiền để freelancer bắt đầu làm việc</p>
                    <p>Quản lý việc làm online</p>
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step4-section">
        <div class="container">
            <div class="row-fluid">
                <div class="guider-text span7">
                    <div class="title">
                        <p>Bước 4</p>
                        <h3>Nhận sản phẩm và thanh toán</h3>
                    </div>
                    <p>Nhận sản phẩm khi hoàn toàn hài lòng</p>
                    <p>Xác nhận thanh toán cho freelancer</p>
                </div>
                <div class="span5">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-thue-freelancer-buoc-4.png')?>" title="Nhận sản phẩm và thanh toán" alt="Nhận sản phẩm và thanh toán">
                </div>
            </div>
        </div>
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Đăng việc để thuê freelancer làm việc ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('job_new') ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Đăng việc ngay</a>
            </div>
        </div>
    </div>
    <div class="guider-more">
        <div class="container">
            <h3>Các hướng dẫn khác</h3>
            <ul class="article-list">
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_nap_tien',array()) ?>" >Hướng dẫn nạp tiền<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_dang_viec',array()) ?>" >Hướng dẫn đăng việc<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_quan_ly_du_an',array()) ?>" >Hướng dẫn quản lý dự án<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_chon_freelancer',array()) ?>" >Hướng dẫn chọn freelancer<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_ket_thuc_du_an',array()) ?>" >Hướng dẫn kết thúc dự án<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="#">Hướng dẫn liên hệ trực tiếp với freelancer<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>