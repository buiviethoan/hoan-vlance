<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn freelancer mua credit</h1>
            <p class="body-text">Freelancer có thể sử dụng credit cho các dịch vụ bổ sung trên vLance.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Các dịch vụ cần sử dụng credit trên vLance là: Đăng chào giá nổi bật, đăng hồ sơ freelancer nổi bật, gửi tin nhắn cho khách hàng, v.v... và rất nhiều các dịch vụ khác sẽ tiếp tục được bổ sung trong thời gian tới.</p>
            <p class="separater-text">Hướng dẫn này sẽ giúp freelancer nắm được 3 bước đơn giản để mua credit trên hệ thống của vLance.</p>
            <p class="separater-text">Hãy bắt đầu với mục <b>Quản lý Credit</b> như trong hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-mua-credit.jpg')?>" title="Hướng dẫn freelancer mua credit" alt="Hướng dẫn freelancer mua credit">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Chọn gói credit</h3>
            <p class="body-text">Tiếp theo, bấm vào gói credit bạn muốn mua để chọn. Hiện tại vLance có 5 gói credit để bạn lựa chọn.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-mua-credit-buoc-1.png')?>" title="Hướng dẫn freelancer mua credit bước 1" alt="Hướng dẫn freelancer mua credit bước 1">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Chọn hình thức thanh toán</h3>
            <p class="body-text">Cuối cùng, bấm chọn hình thức thanh toán phù hợp với bạn.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-mua-credit-buoc-2.jpg')?>" title="Hướng dẫn freelancer mua credit bước 2" alt="Hướng dẫn freelancer mua credit bước 2">
            </div>
            <div class="guider-step-note">
                <h4>Lưu ý:</h4>
                <i>Khi thanh toán bằng hình thức chuyển khoản, vui lòng ghi rõ gói credit bạn muốn mua và mã số ID tài khoản vLance của bạn. Ví dụ: “Credit 500k #123456”.</i>
            </div>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_lien_he_khach_hang', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn liên hệ khách hàng
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chào giá dự án
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_kiem_tien', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn kiếm tiền trên vLance
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Mua credit để sử dụng được nhiều dịch vụ trên vLance ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('credit_balance'); ?>?dropdown" class="btn btn-vl btn-vl-large btn-vl-blue">Mua credit ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>