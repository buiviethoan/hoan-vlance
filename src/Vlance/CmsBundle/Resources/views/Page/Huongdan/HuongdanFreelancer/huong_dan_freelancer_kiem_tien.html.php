<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="container">
        <div class="title-guider-fl">Cách freelancer kiếm tiền trên vLance</div>
    </div>
    <div class="guider-step1-section">
        <div class="container">
            <div class="row-fluid">
                <div class="span7">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-kiem-tien-tren-vlance-buoc-1.png')?>" title="Tạo hồ sơ" alt="Tạo hồ sơ">
                </div>
                <div class="guider-text span5">
                    <div class="title">
                        <p>Bước 1</p>
                        <h3>Tạo hồ sơ</h3>
                    </div>
                    <p>Tạo một hồ sơ chuyên nghiệp để gây ấn tượng với khách hàng<br/>
                       Xác thực thông tin cá nhân để tăng uy tín cho tài khoản</p>
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step2-section">
        <div class="container">
            <div class="row-fluid">
                <div class="guider-text span7">
                    <div class="title">
                        <p>Bước 2</p>
                        <h3>Gửi chào giá</h3>
                    </div>
                    <p>Lựa chọn công việc phù hợp với kỹ năng và kinh nghiệm<br/>
                       Đăng ký nhận việc bằng cách chào giá công việc</p>
                </div>
                <div class="span5">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-kiem-tien-tren-vlance-buoc-2.png')?>" title="Gửi chào giá" alt="Gửi chào giá">
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step3-section">
        <div class="container">
            <div class="row-fluid">
                <div class="span7">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-kiem-tien-tren-vlance-buoc-3.png')?>" title="Bắt đầu làm việc" alt="Bắt đầu làm việc">
                </div>
                <div class="guider-text span5">
                    <div class="title">
                        <p>Bước 3</p>
                        <h3>Bắt đầu làm việc</h3>
                    </div>
                    <p>Bắt đầu làm việc khi vLance xác nhận dự án đã được nạp tiền<br/>
                       Làm việc trực tuyến với khách hàng trên hệ thống của vLance</p>
                </div>
            </div>
        </div>
    </div>
    <div class="guider-step4-section">
        <div class="container">
            <div class="row-fluid">
                <div class="guider-text span7">
                    <div class="title">
                        <p>Bước 4</p>
                        <h3>Nhận thanh toán</h3>
                    </div>
                    <p>Bàn giao sản phẩm hoàn thiện cho khách hàng<br/>
                       Nhận thù lao theo đúng thỏa thuận ban đầu</p>
                </div>
                <div class="span5">
                    <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-kiem-tien-tren-vlance-buoc-4.png')?>" title="Nhận thanh toán" alt="Nhận thanh toán">
                </div>
            </div>
        </div>
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Tìm việc và chào giá để nhận việc ngay!</h3>
            <div class="button">
                <a href="/viec-lam-freelance" class="btn btn-vl btn-vl-large btn-vl-blue">Tìm việc ngay</a>
            </div>
        </div>
    </div>
    <div class="guider-more">
        <div class="container">
            <h3>Các hướng dẫn khác</h3>
            <ul class="article-list">
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_hoan_thien_ho_so', array());?>">Hướng dẫn freelancer hoàn thiện hồ sơ trên vLance<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">Hướng dẫn freelancer chào giá dự án<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_nhan_thanh_toan', array());?>">Hướng dẫn freelancer nhận thanh toán<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan', array());?>">Hướng dẫn freelancer xác thực thông tin tài khoản<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_lien_he_khach_hang', array());?>">Hướng dẫn freelancer liên hệ với khách hàng<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
                <li>
                    <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">Hướng dẫn freelancer mua credit<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>