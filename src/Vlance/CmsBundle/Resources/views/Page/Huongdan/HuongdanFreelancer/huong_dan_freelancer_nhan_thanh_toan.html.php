<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn freelancer nhận thanh toán</h1>
            <p class="body-text">Freelancer có thể rút tiền về tài khoản cá nhân sau khi đã hoàn thành công việc.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Sau khi hoàn thành công việc và được khách hàng thanh toán, freelancer có thể rút tiền về tài khoản cá nhân qua 3 bước vô cùng đơn giản.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Vào mục tùy chọn</h3>
            <p class="body-text">Bạn hãy vào mục <b>Tùy chọn</b> như trong hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-1.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 1" alt="Hướng dẫn freelancer nhận thanh toán bước 1">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Cập nhật thông tin tài khoản</h3>
            <p class="body-text">Vào mục <b>Tài khoản ngân hàng</b> để cập nhật thông tin tài khoản ngân hàng của bạn.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-2-anh-1.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 1" alt="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 1">
            </div>
            <p class="separater-text">Bấm nút <b>Thêm tài khoản ngân hàng</b>. Bạn có thể thêm một hoặc nhiều tài khoản ngân hàng.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-2-anh-2.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 2" alt="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 2">
            </div>
            <p class="separater-text">Bạn cần cập nhật đầy đủ và chính xác thông tin tài khoản ngân hàng để vLance có thể chuyển khoản nhanh chóng cho bạn.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-2-anh-3.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 3" alt="Hướng dẫn freelancer nhận thanh toán bước 2 ảnh 3">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Yêu cầu rút tiền</h3>
            <p class="body-text">Sau khi cập nhật thông tin tài khoản ngân hàng, bạn vào mục <b>Rút tiền</b> như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-3-anh-1.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 1" alt="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 1">
            </div>
            <p class="separater-text">Chọn tài khoản ngân hàng, sau đó điền số tiền muốn rút ở dòng Số tiền muốn rút. Ví dụ: 1000000 (chỉ điền số).</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-3-anh-2.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 2" alt="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 2">
            </div>
            <p class="separater-text">Cuối cùng, bạn bấm nút Rút tiền để gửi yêu cầu cho vLance.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-nhan-thanh-toan-buoc-3-anh-3.jpg')?>" title="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 3" alt="Hướng dẫn freelancer nhận thanh toán bước 3 ảnh 3">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h4>Lưu ý:</h4>
            <p>- Hệ thống của vLance sẽ giải quyết yêu cầu rút tiền của bạn trong vòng 5 ngày làm việc kể từ khi bạn yêu cầu rút tiền (không kể thứ 7 và Chủ Nhật).</p>
            <p>- Những yêu cầu rút tiền trong Thứ 7 và Chủ Nhật sẽ được giải quyết vào đầu tuần kế tiếp.</p>
            <p>- Bạn sẽ chịu hoàn toàn phụ phí chuyển khoản khi thực hiện lệnh rút tiền về tài khoản cá nhân.</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_kiem_tien', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn kiếm tiền trên vLance
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_hoan_thien_ho_so', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn hoàn thiện hồ sơ
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Tìm việc và chào giá để nhận việc ngay!</h3>
            <div class="button">
                <a href="/viec-lam-freelance" class="btn btn-vl btn-vl-large btn-vl-blue">Tìm việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>