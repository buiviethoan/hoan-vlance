<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn freelancer liên hệ với khách hàng</h1>
            <p class="body-text">Freelancer có thể dễ dàng liên hệ với khách hàng qua hệ thống của vLance.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Freelancer và khách hàng sẽ liên hệ với nhau chủ yếu qua hệ thống tin nhắn do vLance cung cấp. Hướng dẫn này sẽ giúp freelancer sử dụng hệ thống tin nhắn đó đúng cách.</p>
            <p class="separater-text">Trên vLance, chỉ khách hàng mới có thể chủ động liên hệ trước với freelancer. Nếu khách hàng liên hệ với bạn, bạn sẽ nhận được tin nhắn như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-lien-he-khach-hang.jpg')?>" title="Hướng dẫn freelancer liên hệ với khách hàng" alt="Hướng dẫn freelancer liên hệ với khách hàng">
            </div>
            <div class="separater"></div>
            <p class="separater-text">Bạn có thể dùng hệ thống tin nhắn trên để trao đổi và gửi sản phẩm cho khách hàng. Có 2 cách để gửi sản phẩm cho khách hàng là:</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Thêm tài liệu đính kèm</h3>
            <p class="body-text">Bạn có thể đăng sản phẩm lên trang tin nhắn vLance bằng cách bấm vào <b>Thêm tài liệu đính kèm</b> như hình dưới:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-lien-he-khach-hang-buoc-1.jpg')?>" title="Hướng dẫn freelancer liên hệ với khách hàng bước 1" alt="Hướng dẫn freelancer liên hệ với khách hàng bước 1">
            </div>
            <div class="guider-step-note">
                <i>Trang tin nhắn của vLance hỗ trợ đăng tài liệu có định dạng png, jpg, gif, pdf, psd, xls, xlsx, doc, docx, ppt, pptx, ods, odt, zip, rar và kích thước không quá 10MB.</i>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Gửi link sản phẩm</h3>
            <p class="body-text">Bạn có thể đăng sản phẩm lên các trang như Google Drive, Dropbox, v.v. rồi gửi đường link cho khách hàng qua trang tin nhắn của vLance. Khách hàng sẽ truy cập vào đường link đó và tải sản phẩm về.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-lien-he-khach-hang-buoc-2.jpg')?>" title="Hướng dẫn freelancer liên hệ với khách hàng bước 2" alt="Hướng dẫn freelancer liên hệ với khách hàng bước 2">
            </div>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chào giá dự án
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_nhan_thanh_toan', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nhận thanh toán
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Tìm việc và chào giá để nhận việc ngay!</h3>
            <div class="button">
                <a href="/viec-lam-freelance" class="btn btn-vl btn-vl-large btn-vl-blue">Tìm việc ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>