<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php if(is_object($app->getSecurity()->getToken()) && ($current_user = $app->getSecurity()->getToken()->getUser())): ?>
    <?php if(is_object($current_user)): ?>
        <?php $vl_uid = $current_user->getId(); ?>
        <?php setcookie('vluid', base64_encode($vl_uid), time()+90*24*3600, "/"); ?>
    <?php endif;?>
<?php endif;?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn freelancer hoàn thiện hồ sơ</h1>
            <p class="body-text">Một trang hồ sơ chuyên nghiệp có thể giúp freelancer giành được nhiều công việc.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Một trang hồ sơ đầy đủ của freelancer gồm có 3 phần là: Thông tin cá nhân, Hồ sơ làm việc và Hồ sơ năng lực.</p>
            <p class="separater-text">Bạn hãy vào mục <b>Sửa thông tin tài khoản</b> như hình dưới đây để bắt đầu cập nhật hồ sơ nhé:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ" alt="Hướng dẫn freelancer hoàn thiện hồ sơ">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Thông tin cá nhân</h3>
            <p class="body-text">Mục đầu tiên bạn cần cập nhật là <b>Thông tin cá nhân</b> như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-1-anh-1.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 1" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 1">
            </div>
            <p class="separater-text">Bấm nút <b>Chọn tệp</b> để đăng lên hình ảnh bạn muốn dùng làm ảnh đại diện. Bạn nên dùng ảnh rõ mặt và phông nền tối giản để thể hiện sự chuyên nghiệp với khách hàng.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-1-anh-2.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 2" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 2">
            </div>
            <p class="separater-text">Tiếp theo, điền thông tin vào các mục được yêu cầu như hình dưới đây, gồm: Số điện thoại, CMND, v.v. Nếu bạn không điền đầy đủ thông tin, bạn sẽ không thể chào giá.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-1-anh-3.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 3" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 1 ảnh 3">
            </div>
            <p class="separater-text">Cuối cùng, bấm nút <b>Lưu các thay đổi</b> để hoàn thành việc cập nhật thông tin cá nhân.</p>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Hồ sơ làm việc</h3>
            <p class="body-text">Mục thứ hai bạn cần cập nhật là <b>Hồ sơ làm việc</b>. Hãy giới thiệu về bản thân để khách hàng biết: Bạn là ai? Bạn tham gia vLance.vn với mục đích gì?</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-2-anh-1.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 1" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 1">
            </div>
            <p class="separater-text">Tiếp theo, cập nhật chuyên môn chính và kinh nghiệm của bạn trong phần <b>Kinh nghiệm làm việc</b>.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-2-anh-2.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 2" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 2">
            </div>
            <p class="separater-text">Sau đó, cập nhật thông tin về dịch vụ mà bạn có thể cung cấp như trong hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-2-anh-3.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 3" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 2 ảnh 3">
            </div>
            <p class="separater-text">Cuối cùng, bấm nút <b>Lưu các thay đổi</b> để hoàn thành việc cập nhật hồ sơ làm việc.</p>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Hồ sơ năng lực</h3>
            <p class="body-text">Hồ sơ năng lực là mục để đăng sản phẩm của bạn ở những dự án trước đây (trong hoặc ngoài vLance). Hãy đăng các sản phẩm đó theo form có sẵn như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-hoan-thien-ho-so-buoc-3-anh-1.jpg')?>" title="Hướng dẫn freelancer hoàn thiện hồ sơ bước 3 ảnh 1" alt="Hướng dẫn freelancer hoàn thiện hồ sơ bước 3 ảnh 1">
            </div>
            <div class="guider-step-note">
                <h4>Lưu ý:</h4>
                <i>- Hãy viết thật chi tiết về sản phẩm hoặc dự án này để người xem hiểu được những công việc thực sự bạn đã làm.</i>
                <i>- Vui lòng không đăng CV hoặc portfolio trong mục này.</i>
            </div>
            <p class="separater-text">Sau cùng, bấm nút <b>Lưu hồ sơ</b> để hoàn thành việc cập nhật hồ sơ năng lực.</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn xác thực tài khoản
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chào giá dự án
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit
                </a>
            </div>
        </div>    
    </div>
    <?php if(is_object($current_user)): ?>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Cập nhật hồ sơ trên vLance ngay bây giờ!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate("account_basic_edit",array('id' => $current_user->getId())); ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Cập nhật ngay</a>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>