<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php if(is_object($app->getSecurity()->getToken()) && ($current_user = $app->getSecurity()->getToken()->getUser())): ?>
    <?php if(is_object($current_user)): ?>
        <?php $vl_uid = $current_user->getId(); ?>
        <?php setcookie('vluid', base64_encode($vl_uid), time()+90*24*3600, "/"); ?>
    <?php endif;?>
<?php endif;?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-verify-account-information">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn xác thực thông tin tài khoản</h1>
            <p class="body-text">Xác thực thông tin giúp tăng uy tín cho tài khoản của bạn trên vLance.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Để làm việc trên vLance, freelancer cần xác thực 2 loại thông tin cơ bản là <b>Số điện thoại</b> và <b>Chứng minh nhân dân</b>. Vui lòng xem hướng dẫn sau đây để nắm được cách xác thực 2 thông tin này nhé.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Xác thực số điện thoại</h3>
            <p class="body-text">Đầu tiên, bạn vào mục <b>Xác thực số điện thoại</b> trên thanh menu như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-1-anh-1.jpg')?>" title="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 1" alt="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 1">
            </div>
            <p class="separater-text">Tiếp theo, bạn bấm nút <b>Xác thực số điện thoại</b> trong mục số 1 như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-1-anh-2.jpg')?>" title="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 2" alt="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 2">
            </div>
            <p class="separater-text">Điền số điện thoại mà bạn muốn xác thực và bấm nút <b>Nhận mã</b> như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-1-anh-3.jpg')?>" title="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 3" alt="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 3">
            </div>
            <p class="separater-text">Mã số xác thực gồm 5 chữ số sẽ được gửi đến số điện thoại mà bạn đã điền ở trên. Hãy điền mã số đó vào mục được yêu cầu và bấm nút <b>Xác nhận</b> như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-1-anh-4.jpg')?>" title="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 4" alt="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 4">
            </div>
            <p class="separater-text">Bạn đã hoàn thành việc xác thực số điện thoại!</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-1-anh-5.jpg')?>" title="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 5" alt="Hướng dẫn khách hàng xác thực thông tin tài khoản-Bước 1-Ảnh 5">
            </div>
            <div class="separater"></div>
        </div>
    </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Xác thực chứng minh nhân dân</h3>
            <p class="body-text">Bạn xác thực chứng minh nhân dân trong mục số 2 như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-2-anh-1.jpg')?>" title="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 1-Ảnh 1" alt="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 1-Ảnh 1">
            </div>
            <p class="separater-text">Hãy cập nhật đầy đủ, chính xác những thông tin được yêu cầu trong mục này.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-2-anh-2.jpg')?>" title="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 2-Ảnh 2" alt="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 2-Ảnh 2">
            </div>
            <p class="separater-text">Sau đó bấm nút <b>Lưu thông tin CMND</b> để lưu những thông tin bạn đã cập nhật.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-2-anh-3.jpg')?>" title="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 2-Ảnh 3" alt="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 2-Ảnh 3">
            </div>
            <p class="separater-text">Cuối cùng, bạn bấm nút <b>Xác thực CMND</b>. vLance sẽ gửi kết quả xác thực cho bạn trong vòng 72 tiếng.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-xac-thuc-tai-khoan-buoc-2-anh-4.jpg')?>" title="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 1-Ảnh 1" alt="Hướng dẫn freelancer xác thực thông tin tài khoản-Bước 2-Ảnh 4">
            </div>
            <div class="separater"></div>
            <p class="separater-text">Những thông tin riêng tư này của bạn hoàn toàn được bảo mật trên hệ thống của vLance. Không có bên thứ 3 nào ngoài vLance được quyền truy cập và xem những thông tin riêng tư này của bạn.</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_kiem_tien', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn kiếm tiền trên vLance
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn chào giá dự án
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit
                </a>
            </div>
        </div>    
    </div>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Xác thực thông tin tài khoản ngay!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate('account_verify_information'); ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Xác thực ngay</a>
            </div>
        </div>
    </div>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>