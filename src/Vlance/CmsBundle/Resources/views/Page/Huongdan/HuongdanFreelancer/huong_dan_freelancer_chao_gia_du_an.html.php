<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php $view['slots']->set('og_url', $meta_url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php if(is_object($app->getSecurity()->getToken()) && ($current_user = $app->getSecurity()->getToken()->getUser())): ?>
    <?php if(is_object($current_user)): ?>
        <?php $vl_uid = $current_user->getId(); ?>
        <?php setcookie('vluid', base64_encode($vl_uid), time()+90*24*3600, "/"); ?>
    <?php endif;?>
<?php endif;?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>

<div class="guider-fl">
    <div class="block-head-fl">
        <div class="container">
            <h1>Hướng dẫn freelancer chào giá dự án</h1>
            <p class="body-text">Chào giá chuyên nghiệp sẽ giúp freelancer thu hút được sự chú ý của khách hàng.</p>
        </div>
    </div>    
    <div class="block-step">
        <div class="container">
            <p class="separater-text">Nếu bạn là freelancer mới bắt đầu, hãy tham khảo 5 gợi ý sau đây của vLance để biết cách gửi một chào giá hấp dẫn, chuyên nghiệp, có thể thuyết phục khách hàng ngay từ những ấn tượng đầu tiên nhé.</p>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>1</span> Đọc, hiểu yêu cầu công việc</h3>
            <p class="body-text">Hãy đọc kỹ dự án để nắm được các thông tin cơ bản và hiểu được yêu cầu của khách hàng.</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-chao-gia-du-an-buoc-1.jpg')?>" title="Hướng dẫn freelancer chào giá dự án bước 1" alt="Hướng dẫn freelancer chào giá dự án bước 1">
            </div>
            <div class="guider-step-note">
                <h4>Gợi ý của vLance:</h4>
                <i>- Hãy liệt kê những câu hỏi liên quan mà bạn cần khách hàng giải đáp để làm sáng tỏ thêm về công việc. Những câu hỏi đúng vừa giúp bạn nhận được những phản hồi đúng từ khách hàng, đồng thời họ cũng sẽ biết bạn đã hiểu rõ yêu cầu và phạm vi công việc.</i>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>2</span> Đề xuất thuyết phục khách hàng</h3>
            <p class="body-text">Đề xuất thuyết phục khách hàng thuê bạn, bạn cần giải đáp hai câu hỏi: Vì sao bạn phù hợp với công việc này? Bạn dự định thực hiện công việc này như thế nào?</p>
            <p class="separater-text">Hãy giải đáp hai câu hỏi trên bằng cách điền vào những mục có sẵn như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-chao-gia-du-an-buoc-2.jpg')?>" title="Hướng dẫn freelancer chào giá dự án bước 2" alt="Hướng dẫn freelancer chào giá dự án bước 2">
            </div>
            <div class="guider-step-note">
                <h4>Gợi ý của vLance:</h4>
                <i>- Bắt đầu bằng một lời chào, điều đó giúp tạo thiện cảm với người mà bạn giao tiếp.</i>
                <i>- Viết ra kinh nghiệm của bạn ở dự án tương tự, nhấn mạnh những kỹ năng phù hợp với yêu cầu của khách hàng.</i>
                <i>- Liệt kê những câu hỏi mà bạn cần khách hàng giải đáp để làm sáng tỏ thêm về công việc.</i>
                <i>- Nêu tóm tắt kế hoạch của bạn cho dự án. Điều này sẽ giúp tạo niềm tin với khách hàng bởi nó cho thấy bạn hiểu yêu cầu và nghiêm túc với công việc.</i>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>3</span> Đề xuất chi phí</h3>
            <p class="body-text">Hãy đề xuất với khách hàng mức phí để thực hiện công việc bằng cách điền vào mục như hình dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-chao-gia-du-an-buoc-3.jpg')?>" title="Hướng dẫn freelancer chào giá dự án bước 3" alt="Hướng dẫn freelancer chào giá dự án bước 3">
            </div>
            <div class="guider-step-note">
                <h4>Gợi ý của vLance:</h4>
                <i>- Lựa chọn chi phí dựa trên kinh nghiệm của bạn.</i>
                <i>- Tham khảo chi phí của những dự án tương tự trên vLance.</i>
                <i>- Với dự án chưa có đủ thông tin để báo giá, hãy chủ động đề nghị khách hàng cung cấp thêm thông tin bạn cần.</i>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>4</span> Dự kiến thời gian hoàn thành</h3>
            <p class="body-text">Bạn có thể hoàn thành việc này trong mấy ngày? Hãy cho khách hàng biết điều đó trong mục sau đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-chao-gia-du-an-buoc-4.jpg')?>" title="Hướng dẫn freelancer chào giá dự án bước 3" alt="Hướng dẫn freelancer chào giá dự án bước 3">
            </div>
            <div class="guider-step-note">
                <h4>Gợi ý của vLance:</h4>
                <i>- Thời gian bạn chọn chỉ là dự kiến, hãy chủ động trao đổi với khách hàng nếu bạn cần thêm thời gian.</i>
                <i>- Khách hàng muốn công việc phải được hoàn thành đúng thời hạn với chất lượng tốt, vì thế bạn hãy cam kết về điều đó để khách hàng thêm tin tưởng.</i>
            </div>
            <div class="separater"></div>
        </div>
    </div>
    <div class="block-step">
        <div class="container">
            <h3><span>5</span> Chú ý cách trình bày nội dung chào giá</h3>
            <p class="body-text">Hãy viết đoạn văn dễ đọc, các đoạn vừa phải và nên ngắt dòng hợp lý. Một chào giá thành công trên vLance.vn được thống kê có độ dài trung bình 396 ký tự.</p>
            <p class="separater-text">Kiểm tra một lượt các thông tin đã điền, sau đó bấm nút Gửi chào giá để chào giá của bạn được gửi tới khách hàng.</p>
            <p class="separater-text">Sau khi bạn gửi chào giá, khách hàng sẽ nhận được chào giá như ví dụ dưới đây:</p>
            <div>
                <img src="<?php echo $view['assets']->getUrl('/img/huong-dan/huong-dan-freelancer-chao-gia-du-an-buoc-5.jpg')?>" title="Hướng dẫn freelancer chào giá dự án bước 3" alt="Hướng dẫn freelancer chào giá dự án bước 3">
            </div>
            <p class="separater-text">Chúc bạn chào giá thành công!</p>
        </div>
    </div>
    <div class="block-view-more">
        <div class="container">
            <h4>Xem thêm</h4>
            <div>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_lien_he_khach_hang', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn liên hệ khách hàng
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn mua credit
                </a>
                <a href="<?php echo $view['router']->generate('huong_dan_freelancer_nhan_thanh_toan', array());?>">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>Hướng dẫn nhận thanh toán
                </a>
            </div>
        </div>    
    </div>
    <?php if(is_object($current_user)): ?>
    <div class="block-post-job-section">
        <div class="container">
            <h3>Cập nhật hồ sơ trên vLance ngay bây giờ!</h3>
            <div class="button">
                <a href="<?php echo $view['router']->generate("account_basic_edit",array('id' => $current_user->getId())); ?>" class="btn btn-vl btn-vl-large btn-vl-blue">Cập nhật ngay</a>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="block-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
</div>

<?php $view['slots']->stop(); ?>