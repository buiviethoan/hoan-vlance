<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $meta_title); ?>

<?php $view['slots']->set('description', $meta_description);?>
<?php $view['slots']->set('keyword', $meta_keyword);?>

<?php $view['slots']->set('og_title', $meta_title);?>
<?php $view['slots']->set('og_description', $meta_description);?>
<?php // $view['slots']->set('og_image', $meta_image);?>
<?php $view['slots']->set('og_keyword', $meta_keyword);?>
<?php //$view['slots']->set('og_url', $url);?>
<?php //$request = $this->container->get('request'); ?>
<?php //$view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg'));?>
<?php //$view['slots']->set('og_url', 'http://'.$request->getHost() .'/page/'. $url);?>

<?php /*if($edit_link):?>
<?php $view['slots']->set('admin_menu', ''
    .'<a href="' . $edit_link . '" target="_blank" class="btn"><i class="icon-pencil"></i> Sửa bài viết</a>'
    .'<a href="' . $view['router']->generate('Vlance_CmsBundle_AdminCmsPage_list') . '" target="_blank" class="btn">'
        . '<i class="icon-list-alt"></i> Danh sách bài viết'
    .'</a>'
);?>
<?php endif; */ ?>

<?php $view['slots']->start('content') ?>
<?php // print_r($acc->getRoles()); die;?>
<div class="cms-section container ">
    <div class="span12 cms_vl-partners">
            <h1><?php echo $title;?></h1>
            <div class="define-partners">Đối tác của vLance là những công ty có kinh nghiệm trong việc hỗ trợ khách hàng làm việc hiệu quả và giúp người dùng hoàn thiện các kỹ năng để dễ dàng tìm việc.</div>
        <div class="wrapper-content">
            <div class="row-fluid">
                <div class="partner-mages span6">
                    <a href="http://www.magestore.com/" rel="nofollow" target="_blank" title="Magestore" alt="Magestore"></a>
                    <p>Magestore là công ty cung cấp ứng dụng dịch vụ magento marketing. Từ điểm khởi đầu Magestore luôn được nằm trong top những nhà cung cấp của Magento.</p>
                </div>
                <div class="partner-moneylover span6">
                    <a href="http://moneylover.me/" rel="nofollow" target="_blank" title="Money Lover" alt="Money Lover"></a>
                    <p>Money Lover là ứng dụng theo dõi thu chi và quản lí tài chính cá nhân trên di động. Ứng dụng cho phép người dùng theo dõi phân loại các khoản thu chi, xây dựng các kế hoạch chi tiêu hợp lý nhằm tiết kiệm hiệu quả hơn.</p>
                </div>
            </div>
            <div class="row-fluid">
                <?php /*
                <div class="partner-vtc span4">
                    <a href="http://vtcacademy.edu.vn/" rel="nofollow"><img src="../img/vtcacademy_logo.png"></a>
                    <p>Vtc Academy được thành lập vào năm 2010, với tầm nhìn trở thành cơ sở đào tạo và nghiên cứu công nghệ nội dung số hàng đầu tại Việt Nam.</p>
                </div> 
                 */ ?>
                <div class="partner-topcv span6">
                    <a href="http://www.topcv.vn/" rel="nofollow" target="_blank" title="TopCv" alt="TopCv"></a>
                    <p>TopCV là công cụ trực tuyến giúp mọi người viết CV một cách đơn giản mà hiệu quả. Với các mẫu CV đa dạng đã được nghiên cứu từ nhiều nguồn uy tín.</p>
                </div>
                <div class="partner-ybox span6">
                    <a href="http://www.ybox.vn/" rel="nofollow" target="_blank" title="Ybox.vn" title="Ybox.vn"></a>
                    <p>YBOX (Youth Box) là chiếc hộp thông tin dành cho giới trẻ với nhiều chuyên mục và bài viết thuộc các chủ đề nổi bật như “cơ hội, học bổng, hướng nghiệp, khởi nghiệp...”</p>
                </div>
            </div>
        </div>
    </div>
    <div class="span12 cms_funding">
        <h1>Nhà tài trợ</h1>
        <div class="define-funding">vLance nhận được tài trợ từ các công ty công nghệ lớn Google, Amazon, Mixpanel, MaGIC, Founder Institute và giải nhất cuộc thi Startup Weekend Hanoi.</div>
        <div class="wrapper-content">
            <div class="row-fluid">
                <div class="funding-google span4">
                    <a href="https://www.google.com/" rel="nofollow" target="_blank" title="Google" alt="Google"></a>
                    <p>vLance may mắn được Google tài trợ gói dịch vụ "Cloud Platform" trong nhiều năm.</p>
                </div>
                <div class="funding-aws span4">
                    <a href="https://aws.amazon.com/" rel="nofollow" target="_blank" title="Amazon web services" alt="Amazon web services"></a>
                    <p>vLance được tài trợ gói dịch vụ trị giá lớn, nhiều chức năng hỗ trợ cao cấp dành cho doanh nghiệp của Amazon.</p>
                </div>
                <?php /*
                <div class="funding-mixpanel span4">
                    <a href="https://mixpanel.com/f/partner" rel="nofollow" target="_blank"><img src="//cdn.mxpnl.com/site_media/images/partner/badge_light.png" alt="Mobile Analytics" /"></a>
                    <p>Mixpanel là công ty công nghệ lớn của Mỹ, xây dựng nền tảng phân tích dữ liệu cho website và mobile.</p>
                </div>
                 */ ?>
                <div class="funding-magic span4">
                    <a href="http://mymagic.my/en/" rel="nofollow" target="_blank" title="MaGIC" alt="MaGIC"></a>
                    <p>vLance được MaGIC tài trợ chương trình “Thúc đẩy phát triển doanh nghiệp” tại Malaysia.</p>
                </div>
            </div>
            <div class="row-fluid">
                <div class="funding-sw span6">
                    <a href="http://ictnews.vn/khoi-nghiep/startup-weekend-summer-2014-tro-lai-va-co-hoi-nhan-giai-thuong-lon-119151.ict" rel="nofollow" target="_blank" title="Start-up Weekend Hanoi 2014" alt="Start-up Weekend Hanoi 2014"></a>
                    <p>vLance đoạt được giải nhất có trị giá lớn từ cuộc thi Startup Weekend tổ chức tại Hà Nội.</p>
                </div>
                <div class="funding-founder span6">
                    <a href="https://fi.co" rel="nofollow" title="Founder Institute" alt="Founder Institute"></a>
                    <p>vLance được tài trợ khóa huấn luyện khởi nghiệp bởi tổ chức Founder Institute vô cùng uy tín.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $view['slots']->stop(); ?>