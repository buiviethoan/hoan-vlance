<?php // muốn quay lại design cũ thỳ về 1235   ?>
<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost(). $view['assets']->getUrl('newsletter/img/logo_share.png'));  ?>
<?php $view['slots']->set('og_image', 'http://' . $request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg')); ?>
<?php $view['slots']->set('og_url', 'http://' . $request->getHost() . $view['router']->generate('vlance_homepage')); ?>
<?php $view['slots']->set('og_description', $view['translator']->trans('home_page.og.description', array(), 'vlance')); ?>




<?php $view['slots']->start('content') ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
<header class="jumbotron subhead">
    <div class="container">
        <div class="jumbotron-even">
            <h1><?php echo $view['translator']->trans('home_page.banner.heading', array(), 'vlance');?></h1>
            <div class="jumbotron-content">
                <ul>
                    <li>
                        <span class="icon-bg"><i></i></span>
                        <span class="icon-bg2"><i></i></span>
                        <p><?php echo $view['translator']->trans('home_page.banner.step1', array(), 'vlance');?></p>
                        <span class="arrow"></span>
                    </li>
                    <li>
                        <span class="icon-bg"><i></i></span>
                        <span class="icon-bg2"><i></i></span>
                        <p><?php echo $view['translator']->trans('home_page.banner.step2', array(), 'vlance');?></p>
                        <span class="arrow"></span>
                    </li>
                    <li>
                        <span class="icon-bg"><i></i></span>
                        <span class="icon-bg2"><i></i></span>
                        <p><?php echo $view['translator']->trans('home_page.banner.step3', array(), 'vlance');?></p>
                        <span class="arrow"></span>
                    </li>
                    <li>
                        <span class="icon-bg"><i></i></span>
                        <span class="icon-bg2"><i></i></span>
                        <p><?php echo $view['translator']->trans('home_page.banner.step4', array(), 'vlance');?></p>
                        <span class="arrow"></span>
                    </li>
                    <li>
                        <span class="icon-bg"><i></i></span>
                        <span class="icon-bg2"><i></i></span>
                        <p><?php echo $view['translator']->trans('home_page.banner.step5', array(), 'vlance');?></p>
                        <span class="arrow"></span>
                    </li>
                </ul>
            </div>
            <div class="jumbotron-button">
                <a class="btn jumbotron-create-job" onclick="vtrack('Click post job', {'location':'Homepage'})" href="<?php echo $view['router']->generate('job_new'); ?>"><?php echo $view['translator']->trans('home_page.banner.postjob', array(), 'vlance');?></a>
            </div>
        </div>
    </div>
</header>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "http://www.vlance.vn/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.google.com/search?q=inurl%3Ahttp%3A%2F%2Fwww.vlance.vn%20{search_term_string}",
        "query-input": "required name=search_term_string"
    }
}
</script>

<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite",
  "name" : "vLance.vn",
  "alternateName" : "vLance.vn",
  "url" : "http://www.vlance.vn"
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function addblock(){
            $(".jumbotron-content").addClass('display_block');
            $(".jumbotron-even").addClass('display_block_odd');    
        }
        setTimeout(addblock, 1000);
    });
</script>

<?php /*
  <div class="home-search-section">
  <div class="container">
  <div class="big-search">
  <form action="<?php // echo $view['router']->generate('job_search'); ?>" id="big-search-block">
  <input type="search" name="search" id="big_search_input" placeholder="<?php // echo $view['translator']->trans('home_page.home_search.placeholder', array(), 'vlance') ?>"/>
  <button type="submit" class="btn btn-large btn-primary"><?php // echo $view['translator']->trans('home_page.home_search.submit', array(), 'vlance') ?></button>
  </form>
  </div>
  </div>
  </div> */ ?>
<div class="home-statistics-section">
    <div class="container">
        <div class="span4 count-job">
            <p>
                <span>
                    <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                        <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                    </a>
                </span>
            </p>
            <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
        </div>
        <div class="span3">
            <p>
                <span>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                        <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                    </a>
                </span>
            </p>
            <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
        </div>
        <div class="span5 last">
            <p>
                <span>
                    <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                        <?php echo number_format($jobStatistic['budget'], '0', ',', '.') . ' ' ?><span><?php echo $view['translator']->trans('common.currency', array(), 'vlance'); ?></span>
                    </a>
                </span>
            </p>
            <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_value', array(), 'vlance');?></p>
        </div>
    </div>
    <div class="separator-statistics"></div>
</div>
<script>
    jQuery(function($) {
        jQuery('.home-statistics-section .span4 span a').anicounter({
            number: <?php echo $accountStatistic['id'] ?>,
            step: 15
        });
        $('.home-statistics-section .span3 span a').anicounter({
            number: <?php echo $jobStatistic['id'] ?>,
            step: 4
        });
        $('.home-statistics-section .span5.last span a').anicounter({
            number: <?php echo $jobStatistic['budget'] ?>,
            suffix: ' <span><?php echo $view['translator']->trans('common.currency', array(), 'vlance'); ?></span>'
        });
    });

</script>
<div class="home-stats-section">
    <div class="border-bottom">
        <div class="container row-fluid">
            <div class="stats-col-left stats-col-left-new span12">
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://dantri.com.vn/kinh-doanh/nguoi-viet-khoi-nghiep-gan-10-trieu-usd-da-duoc-dau-tu-988768.htm" target="_blank"><i class="cat-dantri"></i></a>
                </div>
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://www.dealstreetasia.com/stories/exclusive-vlance-targets-vns-largest-freelance-service/" target="_blank"><i class="cat-tech"></i></a>
                </div>
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://songmoi.vn/kinh-te-thi-truong/san-giao-dich-%E2%80%9Ccuu-canh%E2%80%9D-cho-nghe-tu-do" target="_blank"><i class="cat-kt"></i></a>
                </div>
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://english.vietnamnet.vn/fms/society/98323/freelancers-look-at-work-from-a-fresh-perspective.html" target="_blank"><i class="cat-vietnamnet"></i></a>
                </div>
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://seatimes.com.vn/startup-weekend-hanoi-2014-vinh-danh-du-an-cong-dong-freelancer-0189808.html" target="_blank"><i class="cat-seatime"></i></a>
                </div>
                <div class="stats-col stats-row row-fluid">
                    <a rel="nofollow" href="http://ictnews.vn/khoi-nghiep/goc-doanh-nghiep/vlance-thuc-day-thi-truong-freelance-viet-nam-chuyen-nghiep-hon-116351.ict" target="_blank"><i class="cat-ict"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="separator1"></div>

<div class="home-guide-section">
    <div class="container">
        <div class="guide-block guide-freelancer span6">
            <div class="guide-inner">
                <h2><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_title', array(), 'vlance') ?></h2>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_one', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_one_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_two', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_two_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_three', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_three_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_four', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_freelancer.guide_step_four_description', array(), 'vlance') ?></p>
                </div>
                <p class="start-button">
                    <?php if (!is_object($acc)) : ?>
                        <a title="<?php echo $view['translator']->trans('home_page.guide_freelancer.guide_find_job', array(), 'vlance') ?>" href="<?php echo $view['router']->generate('freelance_job_list') ?>" class="btn btn-primary btn-large">
                            <?php echo $view['translator']->trans('home_page.guide_freelancer.guide_find_job', array(), 'vlance') ?>
                        </a>
                    <?php else : ?>
                        <a title="<?php echo $view['translator']->trans('home_page.guide_freelancer.guide_find_job', array(), 'vlance') ?>" href="<?php echo $view['router']->generate('freelance_job_list') ?>" class="btn btn-primary btn-large">
                            <?php echo $view['translator']->trans('home_page.guide_freelancer.guide_find_job', array(), 'vlance') ?>
                        </a>
                    <?php endif; ?>
                </p>
            </div>
        </div>
        <div class="guide-block guide-client span6">
            <div class="guide-inner">
                <h2><?php echo $view['translator']->trans('home_page.guide_client.guide_title', array(), 'vlance') ?></h2>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_client.guide_step_one', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_client.guide_step_one_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_client.guide_step_two', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_client.guide_step_two_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_client.guide_step_three', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_client.guide_step_three_description', array(), 'vlance') ?></p>
                </div>
                <div class="step row-fluid">
                    <h3 class="semibold"><?php echo $view['translator']->trans('home_page.guide_client.guide_step_four', array(), 'vlance') ?></h3>
                    <p><?php echo $view['translator']->trans('home_page.guide_client.guide_step_four_description', array(), 'vlance') ?></p>
                </div>
                <p class="start-button">
                    <a title="<?php echo $view['translator']->trans('home_page.guide_client.guide_find_freelancer', array(), 'vlance') ?>" href="<?php echo $view['router']->generate('freelancer_list'); ?>" class="btn btn-primary btn-large"><?php echo $view['translator']->trans('home_page.guide_client.guide_find_freelancer', array(), 'vlance') ?></a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="separator2"></div>

<div class="separator2"></div>

<div class="testimonial-section">
    <div class="container">
        <div class="jumbotext">
            <h2 class="lead"><?php echo $view['translator']->trans('home_page.testimonial.lead_title', array(), 'vlance') ?></h2>
        </div>
        <div class="row-fluid">
            <div class="testimonial span4">
                <div class="author">
                    <div class="avatar-bigbox"><img src="<?php echo $view['assets']->getUrl('media/testimonial/t1.jpg'); ?>" alt="Khách hàng thuê freelancer"/></div>
                    <div class="info-box">
                        <p class="title"><?php echo $view['translator']->trans('home_page.testimonial.t1.title', array(), 'vlance') ?></p>
                        <p class="location"><?php echo $view['translator']->trans('home_page.testimonial.t1.city', array(), 'vlance') ?></p>
                    </div>
                </div>
                <div class="balloon">
                    <div class="arrow"></div>
                    <div class="text-block">
                        <p>“<?php echo $view['translator']->trans('home_page.testimonial.t1.quote', array(), 'vlance') ?>”</p>
                    </div>
                </div>
            </div>
            <div class="testimonial span4">
                <div class="author">
                    <div class="avatar-bigbox"><img src="<?php echo $view['assets']->getUrl('media/testimonial/t2.jpg'); ?>" alt="Khách hàng thuê freelancer"/></div>
                    <div class="info-box">
                        <p class="title"><?php echo $view['translator']->trans('home_page.testimonial.t2.title', array(), 'vlance') ?></p>
                        <p class="location"><?php echo $view['translator']->trans('home_page.testimonial.t2.city', array(), 'vlance') ?></p>
                    </div>
                </div>
                <div class="balloon">
                    <div class="arrow"></div>
                    <div class="text-block">
                        <p>“<?php echo $view['translator']->trans('home_page.testimonial.t2.quote', array(), 'vlance') ?>”</p>
                    </div>
                </div>
            </div>
            <div class="testimonial span4">
                <div class="author">
                    <div class="avatar-bigbox"><img src="<?php echo $view['assets']->getUrl('media/testimonial/t3.jpg'); ?>" alt="Khách hàng thuê freelancer"/></div>
                    <div class="info-box">
                        <p class="title"><?php echo $view['translator']->trans('home_page.testimonial.t3.title', array(), 'vlance') ?></p>
                        <p class="location"><?php echo $view['translator']->trans('home_page.testimonial.t3.city', array(), 'vlance') ?></p>
                    </div>
                </div>
                <div class="balloon">
                    <div class="arrow"></div>
                    <div class="text-block">
                        <p>“<?php echo $view['translator']->trans('home_page.testimonial.t3.quote', array(), 'vlance') ?>”</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="carousel-notes" class="carousel slide">
            <div class="carousel-inner">
                <div class="item active">
                    <div class="carousel-caption">
                        <h4><?php echo $view['translator']->trans('home_page.carousel.heading', array(), 'vlance') ?></h4>
                        <p><?php echo $view['translator']->trans('home_page.carousel.p1', array('%number%' => number_format($accountStatistic['id'], '0', ',', '.')), 'vlance') ?></p>
                        <p><?php echo $view['translator']->trans('home_page.carousel.p2', array(), 'vlance') ?></p>
                        <p><?php echo $view['translator']->trans('home_page.carousel.p3', array(), 'vlance') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="separator4"></div>

<?php $view['slots']->stop(); ?>
