<?php // muốn quay lại design cũ thỳ về 1235   ?>
<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $request = $this->container->get('request'); ?>
<?php // $view['slots']->set('og_image', 'http://'.$request->getHost(). $view['assets']->getUrl('newsletter/img/logo_share.png'));  ?>
<?php $view['slots']->set('og_image', 'http://' . $request->getHost() . $view['assets']->getUrl('newsletter/img/share_vlance_home.jpg')); ?>
<?php $view['slots']->set('og_url', 'http://' . $request->getHost() . $view['router']->generate('vlance_homepage')); ?>
<?php $view['slots']->set('og_description', $view['translator']->trans('home_page.og.description', array(), 'vlance')); ?>




<?php $view['slots']->start('content') ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>
    <div class="workdii_home main_content">
        <div class="headerslider">
            <div class="main-top-overlay">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span6 col-left">
                            <h1 class="logo-big"></h1>
                            <h2>
                                Thuê dịch vụ nhanh 
                                & dễ dàng nhất ở 
                                Việt Nam
                            </h2>
                            <div class="no-pad-left">
                                <p class="desc">
                                    Bạn sẽ nhận được 5 báo giá theo yêu cầu
                                    <br>
                                    của bạn. So sánh hay thuê ngay dịch vụ 
                                    <br>
                                    tốt nhất!
                                </p>
                            </div>
                        </div>
                        <div class="span6 col-right">
                            <div class="searchbox">
                                <form>
                                    <div class="form-group">
                                        <label class="control-label">Dịch vụ bạn đang cần tìm</label>
                                        <span class="twitter-typeahead">
                                            <input class="form-control" placeholder="Sửa laptop, máy tính bàn, cài...">
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Nơi mà bạn muốn?</label>
                                        <span class="twitter-typeahead">
                                            <input class="form-control" >
                                        </span>
                                        <span class="small">Hiện tại dịch vụ đang được hỗ trợ ở Hà Nội và TP Hồ Chí Minh</span>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-blue btn-block js-search-btn">Nhận báo giá</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="how-it-works sprite-my">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12 text-center">
                        <h3>Tìm, so sánh và thuê đúng người bạn cần</h3>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="each-step">
                                    <div class="icon icon-match"></div>
                                    <p>
                                        <em>1</em>
                                        Nhận báo giá
                                    </p>
                                    <span>Nói với chúng tôi khó khăn của bạn. Chờ đợi trong vài phút, bạn sẽ nhận được ngay 5 báo giá và cách làm việc.</span>
                                    <span class="smaller">
                                        <strong>
                                            <i class="fa fa-check"></i>
                                            Các báo giá đã được lựa chọn
                                        </strong>
                                        <br>
                                        <strong>
                                            <i class="fa fa-check"></i>
                                            Thông điệp từ người cung cấp
                                        </strong>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="each-step">
                                    <div class="icon icon-compare"></div>
                                    <p>
                                        <em>2</em>
                                        So sánh
                                    </p>
                                    <span>Tất cả người cung cấp dịch vụ đều được đánh giá, xác nhận. Và mỗi hồ sơ bạn nhận sẽ có những thông tin sau:</span>
                                    <span class="smaller">
                                        <strong>
                                            <i class="fa fa-check"></i>
                                            Thông tin liên lạc
                                        </strong>
                                        <strong>
                                            <i class="fa fa-check"></i>
                                            Đánh giá từ khách hàng
                                        </strong>
                                    </span>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="each-step last-step">
                                    <div class="icon icon-hire"></div>
                                    <p>
                                        <em>3</em>
                                        Thuê
                                    </p>
                                    <span>Bạn sẽ quyết định người tốt nhất. Đặt ngay dịch vụ đấy, hoàn thiện bước cuối và bắt đầu làm việc.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="services-grid">
            <div class="container text-center">
                <div class="row-fluid">
                    <div class="span12">
                        <h3>Tìm người chuyên nghiệp cho dịch vụ</h3>
                        <h3>Sửa máy tính</h3>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Sửa laptop" title="sửa laptop" src="/media/sua_laptop.png">
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Sửa máy tính bàn" title="Sửa máy tính bàn" src="/media/sua_may_tinh_ban.png">
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Cài hệ điều hành" title="Cài hệ điều hành" src="/media/cai_he_dieu_hanh.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <h3>Bảo trì máy tính</h3>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Cứu dữ liệu" title="Cứu dữ liệu" src="/media/cuu_du_lieu.png">
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Diệt virus" title="Diệt virus" src="/media/diet_virus.png">
                                    </a>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="card card-title-only card-bordered">
                                    <a class="link-box img-box img-services" href="">
                                        <img alt="Tư vấn về máy tính" title="Tư vấn về máy tính" src="/media/tu_van.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="stories">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12 text-center head">
                        <h3>14.000+ khách hàng đã dùng nghĩ gì về dịch vụ</h3>
                        <p>Khách hàng đánh giá</p>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12 text-center nopadcomputer">
                        <ul class="list-unstyled">
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback1.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar1.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Nguyễn Hoàng Nam
                                                <span>Sửa laptop</span>
                                            </p>
                                        </div>
                                        <p>Dịch vụ sửa laptop rất tốt. Mình đã tìm được người sửa máy tốt. Lần sau sẽ lại lên đây để tìm dịch vụ khi cần.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn sửa laptop</a>
                                    </div>
                                </div>
                            </li>
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback2.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar2.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Ngọc Minh
                                                <span>Sửa máy tính bàn</span>
                                            </p>
                                        </div>
                                        <p>Vì không có nhiều thời gian nên thử tìm người trên trang workdii. Tìm rất nhanh và bạn sửa máy tính rất chuyên nghiệp.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn sửa máy tính bàn</a>
                                    </div>
                                </div>
                            </li>
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback3.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar3.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Ngọc Minh
                                                <span>Cài hệ điều hành</span>
                                            </p>
                                        </div>
                                        <p>Thật tuyệt, windows trông rất đẹp. Cảm ơn Hoàng Anh, mình đã dùng thử window mà bạn đã cài dùng rất nhanh.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn cài hệ điều hành</a>
                                    </div>
                                </div>
                            </li>
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback4.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar4.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Ngọc Minh
                                                <span>Cứu dữ liệu</span>
                                            </p>
                                        </div>
                                        <p>Cảm ơn Khánh, tất cả dữ liệu quan trọng của tôi đã được lấy lại. Bạn làm việc rất tốt và đúng hẹn.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn cứu dữ liệu</a>
                                    </div>
                                </div>
                            </li>
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback5.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar5.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Ngọc Minh
                                                <span>Diệt virus</span>
                                            </p>
                                        </div>
                                        <p>Máy tôi đã hết virus sau khi thuê được dịch vụ của bạn Đức Anh. Lần sau sẽ quay lại thuê tiếp nếu có trục trặc.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn diệt virus</a>
                                    </div>
                                </div>
                            </li>
                            <li class="each-story">
                                <div class="story-box">
                                    <div class="story-top" style="background-image:url(/media/feedback6.png)">
                                        <div class="story-avatar">
                                            <img src="/media/avatar6.jpg">
                                        </div>
                                    </div>
                                    <div class="story-text">
                                        <div class="story-meta">
                                            <p>Ngọc Minh
                                                <span>Tư vấn máy tính</span>
                                            </p>
                                        </div>
                                        <p>Thử tìm thuê tư vấn trên workdii để mua máy cho cửa hàng. Thấy thuê được nhanh mà dịch vụ rất tốt.</p>
                                        <p class="stars">
                                            <img src="/media/star.png">
                                        </p>
                                        <a href="">Chọn tư vấn máy tính</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-stats-section">
            <div class="border-bottom">
                <div class="container row-fluid">
                    <p>Xuất hiện trên</p>
                    <div class="stats-col-left stats-col-left-new span12">
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://dantri.com.vn/kinh-doanh/nguoi-viet-khoi-nghiep-gan-10-trieu-usd-da-duoc-dau-tu-988768.htm" target="_blank"><i class="cat-dantri"></i></a>
                        </div>
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://www.dealstreetasia.com/stories/exclusive-vlance-targets-vns-largest-freelance-service/" target="_blank"><i class="cat-tech"></i></a>
                        </div>
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://songmoi.vn/kinh-te-thi-truong/san-giao-dich-%E2%80%9Ccuu-canh%E2%80%9D-cho-nghe-tu-do" target="_blank"><i class="cat-kt"></i></a>
                        </div>
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://english.vietnamnet.vn/fms/society/98323/freelancers-look-at-work-from-a-fresh-perspective.html" target="_blank"><i class="cat-vietnamnet"></i></a>
                        </div>
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://seatimes.com.vn/startup-weekend-hanoi-2014-vinh-danh-du-an-cong-dong-freelancer-0189808.html" target="_blank"><i class="cat-seatime"></i></a>
                        </div>
                        <div class="stats-col stats-row row-fluid">
                            <a rel="nofollow" href="http://ictnews.vn/khoi-nghiep/goc-doanh-nghiep/vlance-thuc-day-thi-truong-freelance-viet-nam-chuyen-nghiep-hon-116351.ict" target="_blank"><i class="cat-ict"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vendor-pitch">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12 text-center">
                        <h2>Đăng ký với workdii để làm người cung cấp dịch vụ</h2>
                        <p>
                            Bạn có tay nghề cao? Cần tìm khách hàng tiềm năng
                            <br>
                            Hãy đăng ký trên workdii để có thể làm việc ngay.
                            <br>
                            Và bạn sẽ được dùng miễn phí ở thời điểm này.
                        </p>
                        <a class="btn btn-green " href="" >Tìm hiểu thêm</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $view['slots']->stop(); ?>
