<?php $view->extend('VlanceBaseBundle::layout-1column.html.php') ?>
<?php $view['slots']->set('title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $view['slots']->set('og_title', $view['translator']->trans('home_page.og.title', array(), 'vlance')); ?>
<?php $request = $this->container->get('request'); ?>
<?php $view['slots']->set('og_image', 'http://'.$request->getHost() . $view['assets']->getUrl('img/og/vlance.jpg'));?>
<?php $view['slots']->set('og_url', 'http://' . $request->getHost() . $view['router']->generate('vlance_homepage')); ?>
<?php $view['slots']->set('og_description', $view['translator']->trans('home_page.og.description', array(), 'vlance')); ?>

<?php $view['slots']->start('content') ?>
<?php $acc = $app->getSecurity()->getToken()->getUser(); ?>

<div class="homepage-new">
    <div class="carousel slide display-desktop">
        <!-- Carousel items -->
        <div class="carousel-inner">
<!--            Ẩn banner tự động
            Cách sử dụng + ví dụ: Ngày bật - Ngày tắt-->

            <?php if($view['vlance']->autoHideBanner('19-10-2017 00:00:00', '22-10-2017 23:59:59') === true): ?>
                <div class="item">
                    <header class="jumbotron-new carousel-subhead_promotion_credit_1">
                        <a href="<?php echo $view['router']->generate('credit_balance'); ?>?ref=vlance_banner" title="Khuyến mãi">
                            <div class="container"></div>
                        </a>    
                    </header>
                </div>
            <?php endif; ?>
            
            <div class="item">
                <header class="jumbotron-new carousel-subhead_1">
                    <div class="container">
                        <div class="jumbotron-new-even">
                            <h1 class="lh8 mp-lh6"><?php echo $view['translator']->trans('home_page.banner_new.heading_1', array(), 'vlance');?></h1>
                            <div class="jumbotron-button">
                                <a class="btn btn-vl btn-vl-special btn-vl-green" onclick="vtrack('Click post job', {'location':'Homepage'})" href="<?php echo $view['router']->generate('job_new'); ?>" title="<?php echo $view['translator']->trans('home_page.banner_new.postjob', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.postjob', array(), 'vlance');?></a>
                            </div>
                            <div class="jumbotron-new-content">
                                <ul>
                                    <li>
                                        <p class="step-job">1</p>
                                        <span><?php echo $view['translator']->trans('home_page.banner_new.step1', array(), 'vlance');?></span>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">2</p>
                                        <p><?php echo $view['translator']->trans('home_page.banner_new.step2', array(), 'vlance');?></p>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">3</p>
                                        <p><?php echo $view['translator']->trans('home_page.banner_new.step3', array(), 'vlance');?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div class="item">
                <header class="jumbotron-new carousel-subhead_2">
                    <div class="container">
                        <div class="jumbotron-new-even">
                            <h1 class="lh8 mp-lh6"><?php echo $view['translator']->trans('home_page.banner_new.heading_2', array(), 'vlance');?></h1>
                            <div class="jumbotron-button">
                                <a class="btn btn-vl btn-vl-orange btn-vl-special" onclick="vtrack('Click contest job', {'location':'Homepage'})" href="<?php echo $view['router']->generate('job_contest_new'); ?>" title="<?php echo $view['translator']->trans('home_page.banner_new.post_contest', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.post_contest', array(), 'vlance');?></a>
                            </div>
                            <div class="jumbotron-new-content">
                                <ul>
                                    <li>
                                        <p class="step-job">1</p>
                                        <span><?php echo $view['translator']->trans('home_page.banner_new.step_contest1', array(), 'vlance');?></span>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">2</p>
                                        <p><?php echo $view['translator']->trans('home_page.banner_new.step_contest2', array(), 'vlance');?></p>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">3</p>
                                        <p><?php echo $view['translator']->trans('home_page.banner_new.step_contest3', array(), 'vlance');?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
            <div class="item">
                <header class="jumbotron-new carousel-subhead_3">
                    <div class="container">
                        <div class="jumbotron-new-even">
                            <h1 class="lh8 mp-lh6"><?php echo $view['translator']->trans('home_page.banner_new.heading_3', array(), 'vlance');?> <i>₫</i></h1>
                            <div class="jumbotron-button">
                                <a class="btn btn-vl btn-vl-blue btn-vl-special" onclick="vtrack('Click contest job', {'location':'Homepage'})" href="https://www.thuengay.vn/thue-dich-vu?ref=vlhplink" target="_blank" title="<?php echo $view['translator']->trans('home_page.banner_new.post_thuengay', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.post_thuengay', array(), 'vlance');?></a>
                            </div>
                            <div class="jumbotron-new-content">
                                <ul>
                                    <li>
                                        <p class="step-job">1</p>
                                        <p><?php echo $view['translator']->trans('home_page.banner_new.step_thuengay1', array(), 'vlance');?></p>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">2</p>
                                        <span><?php echo $view['translator']->trans('home_page.banner_new.step_thuengay2', array(), 'vlance');?></span>
                                    </li>
                                    <li class="angle-right">
                                        <i class="fa fa-angle-right"></i>
                                    </li>
                                    <li>
                                        <p class="step-job">3</p>
                                        <span><?php echo $view['translator']->trans('home_page.banner_new.step_thuengay3', array(), 'vlance');?></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.carousel .carousel-inner').slick({
                infinite: true,
                lazyLoad: 'ondemand',
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 8000,
                arrow: true,
                dots: true
              });
        });
    </script>
    <div class=" slide-top-homepage display-mobile">
        <?php if($view['vlance']->autoHideBanner('19-10-2017 00:00:00', '22-10-2017 23:59:59') === true): ?>
        <div class="active item">
            <header class="jumbotron-new carousel-subhead_promotion_1">
                <a href="<?php echo $view['router']->generate('credit_balance'); ?>?ref=vlance_banner" title="Khuyến mãi">
                    <div class="container"></div>
                </a>    
            </header>
        </div>
        <?php endif; ?>
        <div class="item">
            <header class="jumbotron-new carousel-subhead_1">
                <div class="container">
                    <div class="jumbotron-new-even">
                        <h1><?php echo $view['translator']->trans('home_page.banner_new.heading_1', array(), 'vlance');?></h1>
                        <div class="jumbotron-button">
                            <a class="btn btn-vl btn-vl-small btn-vl-green" onclick="vtrack('Click post job', {'location':'Homepage'})" href="<?php echo $view['router']->generate('job_new'); ?>" title="<?php echo $view['translator']->trans('home_page.banner_new.postjob', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.postjob', array(), 'vlance');?></a>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="item">
            <header class="jumbotron-new carousel-subhead_2">
                <div class="container">
                    <div class="jumbotron-new-even">
                        <h1><?php echo $view['translator']->trans('home_page.banner_new.heading_2', array(), 'vlance');?></h1>
                        <div class="jumbotron-button">
                            <a class="btn btn-vl btn-vl-orange btn-vl-small" onclick="vtrack('Click contest job', {'location':'Homepage'})" href="<?php echo $view['router']->generate('job_contest_new'); ?>" title="<?php echo $view['translator']->trans('home_page.banner_new.post_contest', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.post_contest', array(), 'vlance');?></a>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <div class="item">
            <header class="jumbotron-new carousel-subhead_3">
                <div class="container">
                    <div class="jumbotron-new-even">
                        <h1><?php echo $view['translator']->trans('home_page.banner_new.heading_3', array(), 'vlance');?> <i>₫</i></h1>
                        <div class="jumbotron-button">
                            <a class="btn btn-vl btn-vl-blue btn-vl-small" onclick="vtrack('Click contest job', {'location':'Homepage'})" href="https://www.thuengay.vn/thue-dich-vu?ref=vlhplink" target="_blink" title="<?php echo $view['translator']->trans('home_page.banner_new.post_thuengay', array(), 'vlance') ?>"><?php echo $view['translator']->trans('home_page.banner_new.post_thuengay', array(), 'vlance');?></a>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.slide-top-homepage.display-mobile').slick({
                infinite: true,
                lazyLoad: 'ondemand',
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 8000,
                arrow: true,
                dots: true
              });
        });
    </script>
    

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "http://www.vlance.vn/",
        "potentialAction": {
            "@type": "SearchAction",
            "target": "https://www.google.com/search?q=inurl%3Ahttp%3A%2F%2Fwww.vlance.vn%20{search_term_string}",
            "query-input": "required name=search_term_string"
        }
    }
    </script>

    <script type="application/ld+json">
    {
      "@context" : "http://schema.org",
      "@type" : "WebSite",
      "name" : "vLance.vn",
      "alternateName" : "vLance.vn",
      "url" : "http://www.vlance.vn"
    }
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            function addblock(){
                $(".jumbotron-content").addClass('display_block');
                $(".jumbotron-even").addClass('display_block_odd');    
            }
            setTimeout(addblock, 1000);
        });
    </script>
    
    <?php /* <div class="step-explain-section">
            <div class="step-detail detail-image">
                <div class="container row-fluid"> 
                    <div class="image-left span6">
                        <img src="<?php echo $view['assets']->getUrl('img/homepage-new/buoc-1-tiep-can-chuyen-gia.png'); ?>" title="Đăng dự án miễn phí. Tiếp cận ngay chuyên gia" alt="Đăng dự án miễn phí. Tiếp cận ngay chuyên gia">
                    </div>
                    <div class="quotes-right span6">
                        <h4 class="lh7">Đăng dự án <span>không mất phí</span>,</br>Tiếp cận ngay <span>chuyên gia</span></h4>
                        <p class="lh3">Bạn sẽ nhanh chóng nhận được chào giá từ các freelancer hàng đầu. Hệ thống tin nhắn của vLance giúp bạn chủ động phỏng vấn và tìm ra được người phù hợp nhất.</p>
                    </div>
                </div>    
            </div>
            <div class="step-detail detail-quotes">
                <div class="container row-fluid">
                    <div class="quotes-left span6">
                        <h4 class="lh7">Cam kết <span>chặt chẽ,</span> </br>Hoàn thành <span>đúng tiến độ</span></h4>
                        <p class="lh3">Hệ thống giúp bạn tạo cam kết và đặt cọc tiền nhanh chóng cho dự án. Công việc được đảm bảo thực hiện theo đúng kế hoạch bạn đề ra.</p>
                    </div>
                    <div class="image-right span6">
                        <img src="<?php echo $view['assets']->getUrl('img/homepage-new/buoc-2-lam-viec.png'); ?>" title="Cam kết chặt chẽ. Làm việc nhanh chóng" alt="Cam kết chặt chẽ, Làm việc nhanh chóng"> 
                    </div>
                </div>    
            </div>
            <div class="step-detail detail-image">
                <div class="container row-fluid">
                    <div class="image-left span6">
                        <img src="<?php echo $view['assets']->getUrl('img/homepage-new/buoc-3-thanh-toan.png'); ?>" title="Nhận sản phẩm ưng ý. Thanh toán dễ dàng" alt="Nhận sản phẩm ưng ý. Thanh toán dễ dàng">
                    </div>
                    <div class="quotes-right span6">
                        <h4 class="lh7">Nhận kết quả <span>100% hài lòng,</span></br>Thanh toán <span>an toàn</span></h4>
                        <p class="lh3">vLance bảo vệ tiền đặt cọc của bạn từ đầu cho đến hết dự án. Bạn chỉ thanh toán khi hài lòng tuyệt đối với kết quả cuối cùng.</p>
                    </div>
                </div>    
            </div>
    </div> */?>
    <div class="top-jobs-section">
        <div class="head-title">
            <h4 class="top-job-title lh4">Dịch vụ <span>Marketing, Thiết kế</span> trọn gói từ A - Z</h4>
            <p>Được cung cấp và đảm bảo 100% bởi <img src="/img/homepage-new/logo-thuengay.png"></p>
        </div>
        <div class="job-list container">
            <div class="job-row row-fluid">
                <div class="job-item span3">
                    <div class="job-item-show job-show-1">
                        <a href="https://www.thuengay.vn/dich-vu/thiet-ke-logo-quang-cao-gdn-facebook-website?ref=vlhp" title="Thiết kế banner quảng cáo google, facebook">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div> 
                        </a>
                    </div>    
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/dich-vu/thiet-ke-logo-quang-cao-gdn-facebook-website?ref=vlhp">Thiết kế banner quảng cáo google, facebook</a></p>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-2">
                        <a href="https://www.thuengay.vn/thue-dich-vu/thiet-ke-va-video?ref=vlhp" title="Thiết kế logo, bộ nhận diện thương hiệu">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div> 
                        </a>
                    </div>    
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/dich-vu/thiet-ke-logo-nhanh-chong-phong-cach-hien-dai-chi-1-ngay?ref=vlhp">Thiết kế logo, bộ nhận diện thương hiệu</a></p>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-3">
                        <a href="https://www.thuengay.vn/thue-dich-vu/marketing-online?ref=vlhp" title="Tư vấn marketing online">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div> 
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/thue-dich-vu/marketing-online?ref=vlhp">Tư vấn marketing online</a></p>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-4">
                        <a href="https://www.thuengay.vn/dich-vu/thiet-ke-giao-dien-landing-page-theo-yeu-cau?ref=vlhp" title="Thiết kế landing-page cho chiến dịch marketing">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div> 
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/dich-vu/thiet-ke-giao-dien-landing-page-theo-yeu-cau?ref=vlhp">Thiết kế landing-page cho chiến dịch marketing</a></p>
                    </div>
                </div>
            </div>
            <div class="job-row row-fluid">
                <div class="job-item span3">
                    <div class="job-item-show job-show-5">
                        <a href="https://www.thuengay.vn/dich-vu/lam-video-gioi-thieu-ve-san-pham-motion-graphics-don-gian?ref=vlhp" title="Làm video giới thiệu sản phẩm">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div>
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/dich-vu/lam-video-gioi-thieu-ve-san-pham-motion-graphics-don-gian?ref=vlhp">Làm video giới thiệu sản phẩm</a>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-6">
                        <a href="https://www.thuengay.vn/thue-dich-vu/viet-lach?ref=vlhp" title="Viết nội dung website">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div>
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/thue-dich-vu/viet-lach?ref=vlhp">Viết nội dung website</a></p>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-7">
                        <a href="https://www.thuengay.vn/dich-vu/thiet-ke-ve-infographics?ref=vlhp" title="Thiết kế infographic">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div> 
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/dich-vu/thiet-ke-ve-infographics?ref=vlhp">Thiết kế infographic</a></p>
                    </div>
                </div>
                <div class="job-item span3">
                    <div class="job-item-show job-show-8">
                        <a href="https://www.thuengay.vn/thue-dich-vu/web-mobile?ref=vlhp" title="Cắt CSS/HTML">
                            <div class="job-item-overlay-upper tf200">
                            </div>    
                            <div class="job-item-overlay-inner tf200">
                                <span class="btn btn-vl-blue btn-top-job">Xem dịch vụ</span>
                                <span class="job-item-price"><img src="/img/homepage-new/logo-thuengay-white.png"></span>
                            </div>
                        </a>
                    </div> 
                    <div class="item-title">
                        <p><a href="https://www.thuengay.vn/thue-dich-vu/web-mobile?ref=vlhp">Cắt CSS/HTML</a></p>
                    </div>
                </div>
            </div>
            <div class="footer-job-list">
                <a href="https://www.thuengay.vn/thue-dich-vu?ref=vlhplink" title="Xem tất cả dịch vụ">
                    <div class="btn btn-block btn-vl-blue btn-vl-large">Xem tất cả dịch vụ</div>
                    <div>
                        <p>Chất lượng được đảm bảo 100% bởi <i class="fa fa-angle-right" aria-hidden="true"></i></p>
                        <img src="/img/homepage-new/logo-thuengay.png">
                    </div>
                </a>    
            </div>
        </div>
    </div>
    <div class="separator5"></div>
    <div class="reviews-section">
        <h3 class="review-client-title">Khách hàng hài lòng về chất lượng dịch vụ</h3>
        <div class="review-clients container">
            <div class="row-fluid slide-reviews">
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-02.jpg'); ?>" alt="Nguyễn Quang Nhật" title="Nguyễn Quang Nhật">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Nguyễn Quang Nhật</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Freelancer hoàn thành công việc rất tốt và chuyên nghiệp, đáp ứng tất cả những yêu cầu của tôi đưa ra. Sẽ làm việc lại với freelancer này trong tương lai.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-01.jpg'); ?>" alt="Linh Đặng" title="Linh Đặng">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Linh Đặng</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>"Freelancer làm việc nhiệt tình và chiều ý khách. Xu hướng cập nhập nhanh. Giao tiếp lễ phép lịch sự. Quá trình làm việc rất suôn sẻ và cảm thấy dễ chịu. Mong em có nhiều đơn hàng."</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-03.jpg'); ?>" alt="Đặng Đức Toàn" title="Đặng Đức Toàn">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Đặng Đức Toàn</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Nhiệt tình, trách nhiệm, đáp ứng deadline công việc. Freelancer sẵn sàng chỉnh sửa theo các yêu cầu của khách hàng, đồng thời có thể đưa ra nhiều option để khách hàng lựa chọn. Một freelancer có thể hợp tác lâu dài.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide-reviews-client">
                    <div class="client-item">
                        <div class="review-avatar">
                            <img src="<?php echo $view['assets']->getUrl('img/homepage-new/avatar-client-04.jpg'); ?>" alt="Lê Trí Minh" title="Lê Trí Minh">
                        </div>
                        <div class="review-title">
                            <h4 class="name-client">Lê Trí Minh</h4>
                            <p>Khách hàng</p>
                        </div>
                        <div class="review-quote">
                            <p>“Hoàng Anh là freelancer rất tích cực trong công việc và giao tiếp, nhanh nhẹn và thông minh. Sẵn sàng hợp tác với bạn khi có dự án mới.”</p>
                            <div class="feedback-star">
                                <img src="<?php echo $view['assets']->getUrl('img/homepage-new/star3.png'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
            $('.slide-reviews').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3,
                responsive: [
                  {
                    breakpoint: 976,
                    settings: {
                      centerMode: true,  
                      centerPadding: '0px',
                      slidesToShow: 2
                      }
                  },
                  {
                    breakpoint: 639,
                    settings: {
                      centerPadding: '0px',
                      slidesToShow:1,
                      slidesToScroll: 1
                    }
                  }
                ]
              });
            });
        </script>
    
        <div class="review-media">
            <div class="review-media-title">
                <h3 class="lh2">Vinh danh trên báo chí</h3>
            </div>
            <div class="container row-fluid">
                <div class="stats-col-left stats-col-left-new span12">
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://dantri.com.vn/kinh-doanh/nguoi-viet-khoi-nghiep-gan-10-trieu-usd-da-duoc-dau-tu-988768.htm" target="_blank"><i class="cat-dantri"></i></a>
                    </div>
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://ictnews.vn/khoi-nghiep/goc-doanh-nghiep/vlance-thuc-day-thi-truong-freelance-viet-nam-chuyen-nghiep-hon-116351.ict" target="_blank"><i class="cat-ict"></i></a>
                    </div>
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://songmoi.vn/kinh-te-thi-truong/san-giao-dich-%E2%80%9Ccuu-canh%E2%80%9D-cho-nghe-tu-do" target="_blank"><i class="cat-kt"></i></a>
                    </div>
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://seatimes.com.vn/startup-weekend-hanoi-2014-vinh-danh-du-an-cong-dong-freelancer-n89808.html" target="_blank"><i class="cat-seatime"></i></a>
                    </div>
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://english.vietnamnet.vn/fms/society/98323/freelancers-look-at-work-from-a-fresh-perspective.html" target="_blank"><i class="cat-vietnamnet"></i></a>
                    </div>
                    <div class="stats-col stats-row row-fluid">
                        <a rel="nofollow" href="http://www.dealstreetasia.com/stories/exclusive-vlance-targets-vns-largest-freelance-service/" target="_blank"><i class="cat-tech"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="professional-apply-section">
        <div class="container">
            <h3 class="title lh8 mp-lh6">
                Bạn có trên <span>3 năm kinh nghiệm</span>
                Và muốn tìm việc để <span>tăng thu nhập?</span>
            </h3>
            <div class="professional-apply-button">
                <a class="btn btn-vl btn-vl-special btn-vl-green" title="Đăng ký làm chuyên gia" href="<?php echo $view['router']->generate('account_register', array('type' => 'freelancer')) ?>">Đăng ký làm chuyên gia</a>
            </div>
        </div>    
    </div>
    <?php /*
      <div class="home-search-section">
      <div class="container">
      <div class="big-search">
      <form action="<?php // echo $view['router']->generate('job_search'); ?>" id="big-search-block">
      <input type="search" name="search" id="big_search_input" placeholder="<?php // echo $view['translator']->trans('home_page.home_search.placeholder', array(), 'vlance') ?>"/>
      <button type="submit" class="btn btn-large btn-primary"><?php // echo $view['translator']->trans('home_page.home_search.submit', array(), 'vlance') ?></button>
      </form>
      </div>
      </div>
      </div> */ ?>
    <div class="landing-statistics-section">
        <div class="container">
            <div class="span5 count-job">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelancer_list') ?>">
                            <?php echo number_format($accountStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_freelancer', array(), 'vlance');?></p>
            </div>
            <div class="span5 count-client">
                <p>
                    <span>
                        <a href="<?php echo $view['router']->generate('freelance_job_list') ?>">
                            <?php echo number_format($jobStatistic['id'], '0', ',', '.'); ?>
                        </a>
                    </span>
                </p>
                <p class="text"><?php echo $view['translator']->trans('home_page.stats.total_job', array(), 'vlance');?></p>
            </div>
        </div>
        <div class="separator-statistics"></div>
    </div>
    <script>
        jQuery(function($) {
            jQuery('.home-statistics-section .span4 span a').anicounter({
                number: <?php echo $accountStatistic['id'] ?>,
                step: 15
            });
            $('.home-statistics-section .span3 span a').anicounter({
                number: <?php echo $jobStatistic['id'] ?>,
                step: 4
            });
            $('.home-statistics-section .span5.last span a').anicounter({
                number: <?php echo $jobStatistic['budget'] ?>,
                suffix: ' <span><?php echo $view['translator']->trans('common.currency', array(), 'vlance'); ?></span>'
            });
        });

    </script>

    <div class="separator2"></div>
</div>

<?php $view['slots']->stop(); ?>
