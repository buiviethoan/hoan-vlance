<footer>
    <div class="footer-links-parter">
        <div class="container">
            <div class="row-fluid vl-relationship">
                <div class="vl-funding span5">
                    <p><?php echo $view['translator']->trans('footer.sponsor', array(), 'vlance') ?></p>
                    <div class="google-footer">
                        <a href="https://www.google.com/" target="_blank" rel="nofollow" title="Google" alt="Google"></a>
                    </div>
                    <div class="aws-footer">
                        <a href="https://aws.amazon.com/" target="_blank" rel="nofollow" title="Amazon web services" alt="Amazon web services"></a>
                    </div>
                    <div class="mixpanel-footer">
                        <a href="https://mixpanel.com/f/partner" target="_blank" rel="nofollow" title="Mobile Analytics" alt="Mobile Analytics"></a>
                    </div>
                </div>
                <div class="vlance-partner span7">
                    <p><?php echo $view['translator']->trans('footer.partner', array(), 'vlance') ?></p>
                    <div class="magestore-footer">
                        <a href="http://www.magestore.com/" target="_blank" rel="nofollow" title="Magestore" alt="Magestore"></a>
                    </div>
                    <div class="money-lover-footer">
                        <a href="http://moneylover.me/" target="_blank" rel="nofollow" title="Money Lover" alt="Money Lover"></a>
                    </div>
                    <div class="topcv-footer">
                        <a href="http://www.topcv.vn/" target="_blank" rel="nofollow" title="TopCv" alt="TopCv"></a>
                    </div>
                    <div class="ybox-footer">
                        <a href="http://www.ybox.vn/" target="_blank" rel="nofollow" title="Ybox.vn" alt="Ybox.vn"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-links-section grey-bg">
        <div class="container">
            <div class="row-fluid">
                <div class="span4 category-block">
                    <div class="job-category span6">
                        <h4><a title="<?php echo $view['translator']->trans('footer.job.heading', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.job.heading', array(), 'vlance') ?></a></h4>
                        <div class="row-fluid">
                            <?php //TODO Sau này update tự động các Ngành có nhiều việc nhất ?>
                            <ul class="first span12">
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'lap-trinh-web'));?>"><?php echo $view['translator']->trans('footer.job.web', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'ung-dung-di-dong'));?>"><?php echo $view['translator']->trans('footer.job.mobile', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'toi-uu-cho-cong-cu-tim-kiem-seo'));?>"><?php echo $view['translator']->trans('footer.job.seo', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'cac-cong-viec-marketing-kinh-doanh'));?>"><?php echo $view['translator']->trans('footer.job.marketing_online', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'dich-thuat'));?>"><?php echo $view['translator']->trans('footer.job.translate', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'cac-cong-viec-viet-lach'));?>"><?php echo $view['translator']->trans('footer.job.copywriting', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'cac-cong-viec-thiet-ke'));?>"><?php echo $view['translator']->trans('footer.job.design', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'quan-ly-blog-fanpage'));?>"><?php echo $view['translator']->trans('footer.job.fanpage', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('freelance_job_list', array());?>"><?php echo $view['translator']->trans('footer.job.contest', array(), 'vlance') ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="job-category span6">
                        <h4><a title="<?php echo $view['translator']->trans('footer.work.heading', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.work.heading', array(), 'vlance') ?></a></h4>
                        <div class="row-fluid">
                            <ul>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-marketing-kinh-doanh'));?>">Freelancer Marketing online</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-it-va-lap-trinh_chash_lap-trinh-web'));?>">Freelancer Làm web</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-it-va-lap-trinh_chash_ung-dung-di-dong'));?>">Freelancer Mobile app</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-it-va-lap-trinh_chash_toi-uu-cho-cong-cu-tim-kiem-seo'));?>">Freelancer SEO</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-thiet-ke'));?>">Freelancer Thiết kế</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-thiet-ke_chash_banner-quang-cao'));?>">Freelancer thiết kế banner</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-viet-lach'));?>">Freelancer Viết bài</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-viet-lach_chash_dich-thuat'));?>">Freelancer Dịch thuật</a></li>
                                <li><a href="<?php echo $view['router']->generate('freelancer_list', array('filters' => 'cpath_cac-cong-viec-hanh-chinh_chash_nhap-du-lieu'));?>">Freelancer nhập dữ liệu</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="span8 right-footer-block">
                    <div class="sub-col footer-fl ft-right span4">
                        <h4><a title="<?php echo $view['translator']->trans('footer.freelancer.heading', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.freelancer.heading', array(), 'vlance') ?></a></h4>
                        <div class="row-fluid">
                            <ul class="first span12">
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_kiem_tien', array());?>">Hướng dẫn freelancer kiếm tiền</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_hoan_thien_ho_so', array());?>">Hướng dẫn hoàn thiện hồ sơ</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_chao_gia_du_an', array());?>">Hướng dẫn chào giá dự án</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_nhan_thanh_toan', array());?>">Hướng dẫn nhận thanh toán</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan', array());?>">Hướng dẫn xác thực tài khoản</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_lien_he_khach_hang', array());?>">Hướng dẫn liên hệ khách hàng</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_freelancer_mua_credit', array());?>">Hướng dẫn mua credit</a></li>
                                <!--<li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'xac-thuc-ho-so'));?>"><?php echo $view['translator']->trans('footer.know.verify', array(), 'vlance') ?></a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="sub-col footer-cl ft-right span3">
                        <h4><a title="<?php echo $view['translator']->trans('footer.client.heading', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.client.heading', array(), 'vlance') ?></a></h4>
                        <div class="row-fluid">
                            <ul class="first span12">
                                <li><a href="<?php echo $view['router']->generate('huong_dan_thue_freelancer', array());?>"><?php echo $view['translator']->trans('footer.client.hire_freelancer', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_dang_viec', array());?>"><?php echo $view['translator']->trans('footer.client.post_job', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_chon_freelancer', array());?>">Hướng dẫn chọn freelancer</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_nap_tien', array());?>">Hướng dẫn nạp tiền</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_quan_ly_du_an', array());?>">Hướng dẫn quản lý dự án</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_ket_thuc_du_an', array());?>">Hướng dẫn kết thúc dự án</a></li>
                                <li><a href="<?php echo $view['router']->generate('huong_dan_lien_he_truc_tiep_freelancer', array());?>">Hướng dẫn liên hệ freelancer</a></li>
    <!--                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'huong-dan-thanh-toan'));?>"><?php echo $view['translator']->trans('footer.know.payment', array(), 'vlance') ?></a></li>
                                <li><a href="<?php echo $view['router']->generate('category_show', array('seoUrl' => 'viet-lach-dich-thuat'));?>"><?php echo $view['translator']->trans('footer.client.regulations_post_job', array(), 'vlance') ?></a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="sub-col about-us ft-right span2">
                        <h4><a title="<?php echo $view['translator']->trans('footer.vlance.heading', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.heading', array(), 'vlance') ?></a></h4>
                        <ul>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'gioi-thieu-ve-vlance'));?>" title="<?php echo $view['translator']->trans('footer.vlance.about', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.about', array(), 'vlance') ?></a></li>
                            <?php /*<li><a target="_blank" href="http://blog.vlance.vn/category/tin-tuc">Tin tức</a></li>
                            <li><a target="_blank" href="http://blog.vlance.vn/category/tuyen-dung-2">Tuyển dụng</a></li>*/?>
                            <li><a href="<?php echo $view['router']->generate('cms_partners', array());?>" title="<?php echo $view['translator']->trans('footer.vlance.partners', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.partners', array(), 'vlance') ?></a></li>
                            <li><a target="_blank" href="http://blog.vlance.vn" title="Blog vLance">Blog vLance</a></li>
                        </ul>
                    </div>
                    <div class="sub-col information ft-right span3">
                        <h4><?php echo $view['translator']->trans('footer.know.heading', array(), 'vlance') ?></h4>
                        <ul>
                            <?php /*<li><a><?php echo $view['translator']->trans('footer.know.error_message', array(), 'vlance') ?></a></li> */?>
                            <li><a href="/tro-giup" title="<?php echo $view['translator']->trans('footer.vlance.help', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.help', array(), 'vlance') ?></a></li>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'cau-hoi-thuong-gap'));?>"><?php echo $view['translator']->trans('footer.know.faq', array(), 'vlance') ?></a></li>
                            <li><a href="#" onclick="_urq.push(['Feedback_Open', 'submit/bug']);">Thông báo lỗi <i class="fa fa-bullhorn" aria-hidden="true"></i></a></li>
                            <!--<li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'huong-dan-su-dung-cho-khach-hang'));?>"><?php echo $view['translator']->trans('footer.know.guide', array(), 'vlance') ?></a></li>-->
                            <li><a rel="nofollow" target="_blank" href="<?php echo $view['router']->generate('cms_page', array('slug' => 'lien-he'));?>" title="<?php echo $view['translator']->trans('footer.vlance.contact', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.contact', array(), 'vlance') ?></a></li>
                        </ul>
                        <div class="social span12">
                            <h4 class="row-fluid"><?php echo $view['translator']->trans('footer.connect.heading', array(), 'vlance') ?></h4>
                            <div class="row-fluid">
                                <ul class="span12">
                                    <li><a rel="nofollow" href="http://www.facebook.com/vlance.vn" target="_blank" class="i-facebook">
                                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="https://plus.google.com/+VlanceVn" target="_blank" class="i-google">
                                            <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="https://www.linkedin.com/company/vlance-vn" target="_blank" class="i-linkedin">
                                            <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="https://twitter.com/vlancevn" target="_blank" class="i-twitter">
                                            <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a rel="nofollow" href="https://www.youtube.com/user/vlancevietnam" target="_blank" class="i-youtube">
                                            <i class="fa fa-youtube-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-logo">
        <div class="container">
            <div class="row-fluid">
                <div class="span9">
                    <div class="row-fluid">
                        <div class="vlance-logo span2">
                            <a href="/" title="<?php echo $view['translator']->trans('footer.copyright.logo', array(), 'vlance') ?>"></a>
                        </div>
                        <div class="copyright span8">
                            <p>Freelancer Việt Nam - Lựa chọn số 1 của doanh nghiệp</p>
                            <p>© 2013 - 2017 vLance.vn</p>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="footer-payment">
                        <div class="nlh-partner">
                            <ul class="">
                                <li>
                                    <a rel="nofollow" class="visacrd" title="Thẻ thanh toán VisaCard" href="http://www.vietnam-visa.com/" target="_blank">
                                        <i class="fa fa-cc-visa" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a rel="nofollow" class="mastercrd" title="Thẻ thanh toán MasterCard" href="http://www.mastercard.com/" target="_blank">
                                        <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a rel="nofollow" class="paypal" title="Hệ thống thanh toán trực tuyến Paypal" href="https://www.paypal.com/" target="_blank">
                                        <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li><a rel="nofollow" class="vietcombank" title="Ngân hàng TMCP Ngoại Thương Việt Nam" href="http://www.vietcombank.com.vn/" target="_blank"></a></li>			
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span9">
                    <div class="footer-rule">
                        <ul>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung'));?>" title="<?php echo $view['translator']->trans('footer.vlance.term', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.term', array(), 'vlance') ?> |</a></li>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung-danh-cho-khach-hang'));?>" title="<?php echo $view['translator']->trans('footer.vlance.term_client', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.term_client', array(), 'vlance') ?> |</a></li>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'dieu-khoan-su-dung-danh-cho-freelancer'));?>" title="<?php echo $view['translator']->trans('footer.vlance.term_freelancer', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.term_freelancer', array(), 'vlance') ?> |</a></li>
                            <li><a href="<?php echo $view['router']->generate('cms_page', array('slug' => 'quy-dinh-bao-mat'));?>" title="<?php echo $view['translator']->trans('footer.vlance.privacy', array(), 'vlance') ?>"><?php echo $view['translator']->trans('footer.vlance.privacy', array(), 'vlance') ?></a></li>
                        </ul>
                    </div>
                    <p class="address"><?php echo $view['translator']->trans('footer.copyright.registration', array(), 'vlance') ?></p>
                    <p class="address"><?php echo $view['translator']->trans('footer.copyright.address', array(), 'vlance') ?> - <?php echo $view['translator']->trans('footer.copyright.support', array("%email%" => '<a href="mailto:hotro@vlance.vn">hotro@vlance.vn</a>'), 'vlance') ?> - <?php echo $view['translator']->trans('footer.copyright.telephone', array(), 'vlance') ?></p>
                </div>
                <?php /*
                <div class="span3">
                    <div class="footer-language">
                        <?php echo $view['actions']->render(
                            $view['router']->generate('vlance_base_block_language', array(), true), array('strategy' => 'esi')
                        ); ?>
                    </div>
                </div> */ ?>
            </div>
        </div>    
    </div>
    <?php /*   Hide popup signature
    <div id="sticky_screen_right" class="sticky-screen-banner right display-desktop">
        <div class="sticky-screen-banner-inner">
            <a class="close sticky-screen-banner-close" title="Đóng" onclick="Close(this);">
                <span class="close-icon">
                    <i class="fa fa-times"></i>
                </span>
                    Đóng
            </a>
            <a class="sticky-screen-banner-link" data-banner-title="Tool chữ ký" data-banner-id="12231" data-banner-group-code="sticky_screen_right" rel="nofollow" href="http://thuengay.vn/cong-cu/tao-chu-ky-email-chuyen-nghiep/?ref=vlpopup" title="Tool chữ ký" target="_blank">
                <img src="/img/ad/banner-chu-ky-120x250.jpg" alt="Tool chữ ký" class="img-responsive lazy-common" style="display: block; background-image: none;">
            </a>
        </div>
        <script type="text/javascript">
            function Close(a){
                $(a).parent().parent().remove();
            }
        </script>
    </div>
     */?>
</footer>