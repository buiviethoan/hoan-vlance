<?php

namespace Vlance\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Cms block controller
 * 
 * @Route("/cms")
 */
class BlockController extends Controller
{
    /**
     * 
     * Display block testimonial in register page
     * 
     * @Route("/testimonial/{type}", name="cms_testimonial", defaults={"type" = "freelancer"}, requirements={"type" = "freelancer|client"})
     * @Template(engine="php")
     */
    public function testimonialAction(Request $request, $type = "freelancer")
    {
        return array();
    }
    
    /**
     * 
     * Display block testimonial in register page
     * 
     * @Route("/footer", name="cms_footer")
     * @Template(engine="php")
     */
    public function footerAction(Request $request)
    {
        return array();
    }
    
    /**
     * 
     * Display block testimonial in register page
     * 
     * @Route("/footer_homepage", name="cms_footer_homepage")
     * @Template(engine="php")
     */
    public function footerHomepageAction(Request $request)
    {
        return array();
    }
}
