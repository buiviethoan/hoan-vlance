<?php

namespace Vlance\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Vlance\CmsBundle\Entity\Poll;
use Vlance\JobBundle\Entity\Job;
use Vlance\AccountBundle\Entity\Account;

/**
 * Poll controller.
 *
 * @Route("/binh-chon")
 */
class PollController extends Controller
{
    
    /**
     * @Route("/{poll_id}/{account_id}/{value}", name="cms_poll_vote")
     * @Template(engine="php")
     */
    public function voteAction($poll_id, $account_id, $value) {
        $em = $this->getDoctrine()->getManager();
        $poll = $em->getRepository("VlanceCmsBundle:Poll")->find($poll_id);
        /**
         * @var $poll \Vlance\CmsBundle\Entity\Poll
         */
        if (!$poll) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.poll.notexist', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        if ($poll->getExpiredAt()->getTimestamp() < time()) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.poll.expired', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $limited = explode(",", $poll->getLimited());
        if (!in_array($account_id, $limited)) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.poll.account.limited', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        /** @var $account \Vlance\AccountBundle\Entity\Account */
        $account = $em->getRepository("VlanceAccountBundle:Account")->find($account_id);
        if (!$account) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.poll.account.error', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $answers = $em->getRepository("VlanceCmsBundle:PollAnswers")->findBy(array('poll' => $poll, 'account' => $account));
        if (count($answers)) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.poll.answered', array(), 'vlance'));
        } else {
            $answer = new \Vlance\CmsBundle\Entity\PollAnswers();
            $answer->setAccount($account);
            $answer->setPoll($poll);
            $answer->setAnswer($value);
            $em->persist($answer);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('controller.poll.success', array(), 'vlance'));
        }
        return $this->redirect($this->generateUrl('vlance_homepage'));
    }
}
