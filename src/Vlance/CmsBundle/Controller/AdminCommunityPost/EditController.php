<?php

namespace Vlance\CmsBundle\Controller\AdminCommunityPost;

use Admingenerated\VlanceCmsBundle\BaseAdminCommunityPostController\EditController as BaseEditController;
use Symfony\Component\Form\Form;
use Vlance\CmsBundle\Entity\CommunityPost;

/**
 * EditController
 */
class EditController extends BaseEditController
{
    /**
     *
     * @param \Symfony\Component\Form\Form $form
     * @param \Vlance\CmsBundle\Entity\CommunityPost $community_post
     */
    public function postSave(Form $form, CommunityPost $community_post) {
        $em = $this->getDoctrine()->getManager();
        //upload image for community post in list page
        if(!is_null($form->get('file')->getData())){
            $community_post->setFile($form->get('file')->getData());
            $community_post->preUpload();
            $community_post->upload();
            $em->persist($community_post);
            $em->flush();
        }
    }
}
