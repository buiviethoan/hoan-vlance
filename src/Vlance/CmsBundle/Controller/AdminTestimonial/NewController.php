<?php

namespace Vlance\CmsBundle\Controller\AdminTestimonial;

use Admingenerated\VlanceCmsBundle\BaseAdminTestimonialController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    public function preSave(\Symfony\Component\Form\Form $form, \Vlance\CmsBundle\Entity\Testimonial $Testimonial) {
        $Testimonial->setType($Testimonial->getAccount()->getType());
    }
}
