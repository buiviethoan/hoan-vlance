<?php

namespace Vlance\CmsBundle\Controller\AdminTestimonial;

use Admingenerated\VlanceCmsBundle\BaseAdminTestimonialController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    public function preSave(\Symfony\Component\Form\Form $form, \Vlance\CmsBundle\Entity\Testimonial $Testimonial) {
        $Testimonial->setType($Testimonial->getAccount()->getType());
    }
}
