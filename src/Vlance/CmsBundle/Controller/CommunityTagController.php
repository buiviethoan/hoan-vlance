<?php

namespace Vlance\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\CmsBundle\Form\CommunityPostType;
use Vlance\CmsBundle\Entity\CommunityPost;
use Vlance\CmsBundle\Entity\CommunityTag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pagerfanta\View\DefaultView;

/**
 * @Route("/tag")
 */
class CommunityTagController extends Controller
{
    /**
     * 
     * @Route("", name="tag_list")
     * @Method("GET")
     * @Template(engine="php")
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
       
        $entities = $em->getRepository('VlanceCmsBundle:CommunityTag')->findAll();
        $response = new JsonResponse();
        $data = array();
        foreach($entities as $entity) {
            $data[] = $entity->getName();
        }
        
        return $response->setData($data);
    }    
}
