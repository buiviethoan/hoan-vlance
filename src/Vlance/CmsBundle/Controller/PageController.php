<?php

namespace Vlance\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\BaseBundle\Utils\HasPermission;
use Vlance\JobBundle\Entity\Job;
use Vlance\AccountBundle\Entity\Account;

class PageController extends Controller
{
    /**
     * @Route("/h-{id}", name="index_check")
     * @Template("VlanceCmsBundle:Page:index.html.php", engine="php")
     */
    public function indexcheck($id = '') {
        return $this->redirect($this->generateUrl('vlance_homepage'));
    }
    
    /**
     * @Route("/doi-tac", name="cms_partners")
     * @Template("VlanceCmsBundle:Page:partners.html.php", engine="php")
     */
    public function partnersAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        return array(
            'title' => "Đối tác",
            'meta_title' => "Đối tác | vLance",
            'meta_title_wo_host' => "Đối tác",
            'meta_description' => "Các đối tác và nhà tài trợ của vLance.vn",
            'meta_keyword' => "Đối tác, doi tac, partner",
            'meta_url' => 'http://'.$host . $this->generateUrl('cms_partners', array())
        );
    }
    
    /**
     * @Route("/huong-dan-thue-freelancer", name="huong_dan_thue_freelancer")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_thue_freelancer.html.php", engine="php")
     */
    public function guiderhireflAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn thuê freelancer",
            'meta_title'            => "Hướng dẫn thuê freelancer | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Các bước để thuê được freelancer trên vLance hiệu quả: Bước 1: Đăng việc miễn phí, thu hút freelancer. Bước 2: So sánh các ứng viên, chọn 01 ứng viên ưng ý nhất. Bước 3: Nạp tiền để freelancer bắt đầu làm việc, quản lý việc làm online. Bước 4: Nhận sản phẩm khi hoàn toàn hài lòng, xác nhận thanh toán cho freelancer.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách thuê freelancer, cach thue freelancer",
            'meta_url'              => $this->generateUrl('huong_dan_thue_freelancer', array(), true),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic,
        );
    }
    
    /**
     * @Route("/huong-dan-dang-viec", name="huong_dan_dang_viec")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_dang_viec.html.php", engine="php")
     */
    public function guiderpostjobAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn khách hàng đăng việc",
            'meta_title'            => "Hướng dẫn khách hàng đăng việc | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Đăng việc tại vLance rất nhanh chóng và đơn giản. Bạn chỉ cần thực hiện 3 bước sau: Bước 1 Mục đăng việc. Bước 2 Điền thông tin. Bước 3 Đăng việc lên vLance.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách đăng việc, cach dang viec",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_dang_viec', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-chon-freelancer", name="huong_dan_chon_freelancer")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_chon_freelancer.html.php", engine="php")
     */
    public function guiderchooseflAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn khách hàng chọn freelancer",
            'meta_title'            => "Hướng dẫn khách hàng chọn freelancer | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Những gợi ý sau đây của vLance sẽ giúp bạn chọn freelancer một cách dễ dàng. Bước 1 Xem xét các chào giá mà bạn nhận được. Bước 2 Nhắn tin để trao đổi thêm. Bước 3 Giao việc cho người phù hợp.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách chọn freelancer, cach chọn freelancer",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_chon_freelancer', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-nap-tien", name="huong_dan_nap_tien")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_nap_tien.html.php", engine="php")
     */
    public function guiderdepositAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn khách hàng nạp tiền",
            'meta_title'            => "Hướng dẫn khách hàng nạp tiền | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Hiện tại vLance đang cung cấp một số hình thức nạp tiền như sau: Cách 1 Nạp tiền trực tiếp tại văn phòng vLance. Cách 2 Chuyển khoản ngân hàng. Cách 3 Chuyển khoản Paypal. Cách 4 Chuyển khoản qua hệ thống Smartlink",
            'meta_keyword'          => "Hướng dẫn, huong dan, hướng dẫn nạp tiền, huong dan nap tien",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_nap_tien', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-quan-ly-du-an", name="huong_dan_quan_ly_du_an")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_quan_ly_du_an.html.php", engine="php")
     */
    public function guidermanagerjobAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn khách hàng quản lý dự án",
            'meta_title'            => "Hướng dẫn khách hàng quản lý dự án | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Sau khi nạp tiền, bạn sẽ bắt đầu làm việc với freelancer. Những gợi ý sau đây sẽ giúp bạn quản lý dự án tốt hơn: Bước 1 Yêu cầu rõ ràng. Bước 2 Nắm rõ tiến độ. Bước 3 Liên hệ với vLance để được hỗ trợ.",
            'meta_keyword'          => "Hướng dẫn, huong dan, quản lý dự án, quan ly du an",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_quan_ly_du_an', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-ket-thuc-du-an", name="huong_dan_ket_thuc_du_an")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_ket_thuc_du_an.html.php", engine="php")
     */
    public function guiderfinishjobAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn khách hàng kết thúc dự án",
            'meta_title'            => "Hướng dẫn khách hàng kết thúc dự án | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Quy trình kết thúc dự án và thanh toán sẽ được thực hiện như sau: Bước 1 Xác nhận kết thúc dự án. Bước 2 Đánh giá freelancer.",
            'meta_keyword'          => "Hướng dẫn, huong dan, kết thúc dự án, ket thuc du an",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_quan_ly_du_an', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-lien-he-truc-tiep-freelancer", name="huong_dan_lien_he_truc_tiep_freelancer")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanKhachhang:huong_dan_lien_he_truc_tiep_freelancer.html.php", engine="php")
     */
    public function guidercontactflAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn liên hệ trực tiếp với freelancer",
            'meta_title'            => "Hướng dẫn liên hệ trực tiếp với freelancer | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Các bước thực hiện để liên hệ trực tiếp với freelancer mà bạn muốn. Bước 1: Vào mục tìm freelancer, bước 2: Danh sách các ứng viên.",
            'meta_keyword'          => "Hướng dẫn, huong dan, liên hệ trực tiếp với freelancer, lien he truc tiep voi freelancer",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_quan_ly_du_an', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-xac-thuc-thong-tin-tai-khoan", name="huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan.html.php", engine="php")
     */
    public function guiderverifyinforAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer xác thực thông tin tài khoản",
            'meta_title'            => "Hướng dẫn freelancer xác thực thông tin tài khoản | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Xác thực thông tin giúp tăng uy tín cho tài khoản của bạn trên vLance. Bước 1: Xác thực số điện thoại, Bước 2: Xác thực chứng minh nhân dân.",
            'meta_keyword'          => "Hướng dẫn, huong dan, xác thực thông tin tài khoản, xac thuc thong tin tai khoan",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_xac_thuc_thong_tin_tai_khoan', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-kiem-tien-tren-vlance", name="huong_dan_freelancer_kiem_tien")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_kiem_tien.html.php", engine="php")
     */
    public function guiderflmakemoneyAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer kiếm tiền trên vLance",
            'meta_title'            => "Hướng dẫn freelancer kiếm tiền trên vLance | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer kiếm tiền trên vLance. Bước 1: Tạo hồ sơ, Bước 2: Gửi chào giá, Bước 3: Bắt đầu làm việc, Bước 4: Nhận thanh toán.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer kiếm tiền trên vLance, cach freelancer kiem tien tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_kiem_tien', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-hoan-thien-ho-so-tren-vlance", name="huong_dan_freelancer_hoan_thien_ho_so")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_hoan_thien_ho_so.html.php", engine="php")
     */
    public function guiderflprofileAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer hoàn thiện hồ sơ trên vLance",
            'meta_title'            => "Hướng dẫn freelancer hoàn thiện hồ sơ trên vLance | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer hoàn thiện hồ sơ. Bước 1: Tạo hồ sơ. Bước 2: Gửi chào giá. Bước 3: Bắt đầu làm việc. Bước 4: Nhận thanh toán.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer hoàn thiện hồ sơ, cach freelancer hoan thien ho so tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_hoan_thien_ho_so', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-chao-gia-du-an", name="huong_dan_freelancer_chao_gia_du_an")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_chao_gia_du_an.html.php", engine="php")
     */
    public function guiderflbidAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer chào giá dự án trên vLance",
            'meta_title'            => "Hướng dẫn freelancer chào giá dự án trên vLance | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer chào giá dự án. Bước 1: Tạo hồ sơ. Bước 2: Gửi chào giá. Bước 3: Bắt đầu làm việc. Bước 4: Nhận thanh toán.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer chào giá dự án, cach freelancer chao gia du an tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_chao_gia_du_an', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-nhan-thanh-toan", name="huong_dan_freelancer_nhan_thanh_toan")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_nhan_thanh_toan.html.php", engine="php")
     */
    public function guiderflreceivepaymentAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer nhận thanh toán trên vLance",
            'meta_title'            => "Hướng dẫn freelancer nhận thanh toán trên vLance | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer nhận thanh toán. Bước 1: Vào mục tùy chọn. Bước 2: Nhập thông tin tài khoản ngân hàng. Bước 3: Gửi yêu cầu rút tiền. Bước 4: Nhận thanh toán.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer nhận thanh toán, cach freelancer nhan thanh toan tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_nhan_thanh_toan', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-lien-he-khach-hang", name="huong_dan_freelancer_lien_he_khach_hang")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_lien_he_khach_hang.html.php", engine="php")
     */
    public function guiderflcontactAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer liên hệ khách hàng",
            'meta_title'            => "Hướng dẫn freelancer liên hệ khách hàng | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer liên hệ khách hàng. Bước 1: Vào phần tin nhắn của dự án. Bước 2: Nhắn tin và trao đổi với khách hàng.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer nhận thanh toán, cach freelancer nhan thanh toan tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_lien_he_khach_hang', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/huong-dan-freelancer-mua-credit", name="huong_dan_freelancer_mua_credit")
     * @Template("VlanceCmsBundle:Page\Huongdan\HuongdanFreelancer:huong_dan_freelancer_mua_credit.html.php", engine="php")
     */
    public function guiderbuycreditAction()
    {
        $host = $this->get('router')->getContext()->getHost();
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'title'                 => "Hướng dẫn freelancer mua credit",
            'meta_title'            => "Hướng dẫn freelancer mua credit | vLance",
            'meta_title_wo_host'    => "Hướng dẫn",
            'meta_description'      => "Cách freelancer mua credit. Bước 1: Vào trang mua credit. Bước 2: Chọn gói credit cần mua. Bước 3: Chọn hình thức thanh toán và xác nhận mua.",
            'meta_keyword'          => "Hướng dẫn, huong dan, cách freelancer mua credit, cach freelancer mua credit tren vLance",
            'meta_url'              => 'http://'.$host . $this->generateUrl('huong_dan_freelancer_mua_credit', array()),
            'jobStatistic'          => $jobStatistic,
            'accountStatistic'      => $accountStatistic
        );
    }
    
    /**
     * @Route("/")
     * @Template("VlanceCmsBundle:Page:index_new.html.php", engine="php")
     */
    public function indexAction(Request $request)
    {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "homepage",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'jobStatistic'     => $jobStatistic,
            'accountStatistic' => $accountStatistic,
        );
    }
    
    /**
     * @Route("/thuengay_homepage")
     * @Template("VlanceServiceBundle:ServicePack:index_thuengay.html.php", engine="php")
     */
    public function indexThuengayAction(Request $request)
    {
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "homepage",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'jobStatistic'     => $jobStatistic,
            'accountStatistic' => $accountStatistic,
        );
    }

    /**
     * @Route("/workdii")
     * @Template("VlanceCmsBundle:Page:new_index_workdi.html.php", engine="php")
     */
    public function workdiiHomepageAction()
    {
        /** BETA VERSION, ACCESSIBLE BY VLANCE ADMIN ONLY **/
        $current_user = $this->get('security.context')->getToken()->getUser();
        if(HasPermission::hasAdminPermission($current_user) == FALSE) {
            return $this->redirect($this->generateUrl('vlance_homepage', array(), true));
        }
        /** BETA VERSION, ACCESSIBLE BY VLANCE ADMIN ONLY **/
        
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "isNotNull")); //TODO: s2-number-freelancer
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
        
        return array(
            'jobStatistic'     => $jobStatistic,
            'accountStatistic' => $accountStatistic,
        );
    }
    
    /**
     * @Route("/language/{lang}", name="switch_language")
     * @Template(engine="php")
     */
    public function languageAction(Request $request, $lang = "")
    {
        $referer = ($request->headers->get('referer'))?($request->headers->get('referer')):($this->generateUrl('vlance_homepage', array()));
        if($lang == "vn" || $lang == "en"){
            $request->getSession()->set("_locale", $lang);
            $request->setLocale($lang);
            setcookie("lang", $lang, time()+60*60*24*365, "/");
        }
        return $this->redirect($referer);
    }
    
    /**
     * @Route("/")
     * @Template(engine="php")
     */
    public function viewAction(Request $request, $slug){        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceCmsBundle:Page')->findOneByUrl($slug);
        if(!$entity){
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Track ref
        if(isset($request->query)){
            $query = $request->query;
            if(!is_null($query->get('ref'))){
                $auth_user = $this->get('security.context')->getToken()->getUser();
                $this->get('vlance_base.helper')->trackMixpanel(NULL, "Page view", array(
                    'ref'           => $query->get('ref'),
                    'page'          => "cms_view_page",
                    'cms_page'      => $entity ? $entity->getUrl() : "",
                    'authenticated' => is_object($auth_user) ? "TRUE" : "FALSE"
                ));
            }
        }
        
        // Get Edit Link if logged in as Super Admin
        $edit_link = "";
        if($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')){
            $user = $this->get('security.context')->getToken()->getUser();
            if($user->hasRole($user::ROLE_SUPER_ADMIN)){
                $edit_link = $this->generateUrl('Vlance_CmsBundle_AdminCmsPage_edit', array('pk' => $entity->getId()));
            }
        }

        $host = $this->get('router')->getContext()->getHost();
        return array(
            'entity'=> $entity,
            'url'   => 'http://'.$host . $this->generateUrl('cms_page', array('slug' => $entity->getUrl())),
            'title' => $entity->getTitle(),
            'content' => $entity->getContent(),
            'edit_link' => $edit_link,
            'meta_title' => (($entity->getMetaTitle())?$entity->getMetaTitle():$entity->getTitle()) . " | vLance",
            'meta_title_wo_host' => (($entity->getMetaTitle())?$entity->getMetaTitle():$entity->getTitle()),
            'meta_description' => ($entity->getMetaDescription())?$entity->getMetaDescription():str_replace('>','*',addslashes(JobCalculator::cut(JobCalculator::replacement(strip_tags($entity->getContent())),256))),
            'meta_keyword' => $entity->getMetaKeyword(),
            'meta_image' => $entity->getMetaImage(),
            'meta_url' => 'http://'.$host . $this->generateUrl('cms_page', array('slug' => $entity->getUrl()))
        );
    }
    
    /**
     * @Route("/ad/{file}", name="block_adv")
     * @Template("VlanceCmsBundle:Page:index.html.php", engine="php")
     */
    public function advAction($file = '') {
        if($file == '') {
            if(true === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                /* @var $user Account*/
                $user = $this->get('security.context')->getToken()->getUser();
                if($user->getType() == Account::TYPE_CLIENT) {
                    return $this->redirect($this->generateUrl('jobs_create_project'));
                } else {
                    return $this->redirect($this->generateUrl('jobs_bid_project'));
                }
            } 
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $em = $this->getDoctrine()->getManager();
        $statisticFields = array('id' => 'count', 'budget' => 'sum');
        $statisticConditions = array('publish' => array(0 => "eq", 1 => "1"));
        $jobStatistic = $em->getRepository('VlanceJobBundle:Job')->statistic($statisticFields, $statisticConditions);
        
        $statisticFields = array('id' => 'count');
        $statisticConditions = array('type' => array(0 => "eq", 1 => "'" . Account::TYPE_FREELANCER . "'"));
        $accountStatistic = $em->getRepository('VlanceAccountBundle:Account')->statistic($statisticFields, $statisticConditions);
                
        return array(
            'jobStatistic' => $jobStatistic,
            'accountStatistic' => $accountStatistic
            );
    }
}
