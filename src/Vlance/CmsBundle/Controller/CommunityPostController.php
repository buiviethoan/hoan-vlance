<?php

namespace Vlance\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Vlance\CmsBundle\Form\CommunityPostType;
use Vlance\CmsBundle\Entity\CommunityPost;
use Vlance\CmsBundle\Entity\CommunityTag;
use Vlance\CmsBundle\Entity\CommunityLike;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Pagerfanta\View\DefaultView;
use Vlance\BaseBundle\Utils\JobCalculator;
use Vlance\BaseBundle\Utils\HasPermission;

/**
 * @Route("/cong-dong")
 */
class CommunityPostController extends Controller
{
    /**
     * All posts
     * @Route("/", name="community_post")
     * @Method("GET")
     * @Template("VlanceCmsBundle:Community:list.html.php", engine="php")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('vlance_homepage'));
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('VlanceCmsBundle:CommunityPost')->findAll();
        $entities = array();
        
        $now = new \DateTime('now');
        if(!is_null($posts)){
            foreach($posts as $p){
                $liked = $em->getRepository('VlanceCmsBundle:CommunityLike')->findBy(array('post' => $p->getId()));
                $time = JobCalculator::countDays($p->getCreatedAt());
                $entities[] = array('entity' => $p, 'like' => count($liked), 'time' => $time);
            }
            rsort($entities);
        }
        
        return array(
            'entities' => $entities,
        );
    }
    
    /**
     * Form new post
     * 
     * @Route("/dang-bai-viet", name="community_post_new")
     * @Method("GET")
     * @Template("VlanceCmsBundle:Community:new.html.php", engine="php")
     */
    public function newAction($entity = null)
    {
        return $this->redirect($this->generateUrl('vlance_homepage'));
        // Not permission
//        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
//            return $this->redirect($this->generateUrl('fos_user_security_login'));
//        }
//        $acc = $this->get('security.context')->getToken()->getUser();
//        if(!$acc->isAdmin()) {
//            return $this->redirect($this->generateUrl("500_page"), 301);
//        }
        
        $entity = new CommunityPost();
        $form = $this->createForm('vlance_cmsbundle_communityposttype', $entity);
        
        return array(
            'entity'=> $entity,
            'form'  => $form->createView(),
//            'acc'   => $acc,
        );
    }
    
    /**
     * Submit post
     * @Route("/dang-bai-viet/submit", name="new_post_submit")
     * @Method("POST")
     * @Template("VlanceCmsBundle:Community:new.html.php", engine="php") 
     */
    public function submitAction(Request $request)
    {
//        // Not permission
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('vlance_homepage'));
        }
        $acc = $this->get('security.context')->getToken()->getUser();
        
        $em = $this->getDoctrine()->getManager();
        
        $entity = new CommunityPost();
        $form = $this->createForm('vlance_cmsbundle_communityposttype', $entity);
    
        $entity->setAccount($acc);
        $form->bind($request);
        if ($form->isValid()) {
            //Insert tags
            $tags = explode(',', $_POST['hiddenTagListA']);
            
            // Insert tags khi tai khoan dang nhap them moi tag
            foreach($tags as $tag_new) {
                if(empty($tag_new)) {
                    break;
                }
                
                $tag = $em->getRepository('VlanceCmsBundle:CommunityTag')->findOneBy(array('title' => $tag_new));

                if(!$tag){
                    $tag_add = new CommunityTag();
                    $tag_add->setTitle($tag_new);
                    $tag_add->setHash($tag_new);
                    $em->persist($tag_add);
                    $em->flush();
                    
                    $tag = $em->getRepository('VlanceCmsBundle:CommunityTag')->findOneBy(array('title' => $tag_new));    
                }
                $entity->addTag($tag);
            }
            
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('community_post_show', array('hash' => $entity->getHash()), true));
        }
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Finds and displays a Post entity.
     *
     * @Route("/{hash}", name="community_post_show")
     * @Method("GET")
     * @Template("VlanceCmsBundle:Community:show.html.php",engine="php")
     */
    public function showPostAction(Request $request, $hash)
    {
        return $this->redirect($this->generateUrl('vlance_homepage'));
        $em = $this->getDoctrine()->getManager();
        $current_user = $this->get('security.context')->getToken()->getUser();
        
        /* @var $entity CommunityPost */
        $entity = $em->getRepository('VlanceCmsBundle:CommunityPost')->findOneBy(array('hash' => $hash));
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        // Check if post is published.
        if(is_object($current_user)){
            if($current_user->isAdmin() == false){
                if($entity->getPublishedAt() == NULL) {
                    return $this->redirect($this->generateUrl('community_post', array(), true));
                }
            }
        } else {
            if($entity->getPublishedAt() == NULL) {
                return $this->redirect($this->generateUrl('community_post', array(), true));
            }
        }
        
        
        //Kiểm tra current_user đã like bài viết chưa
        $liked = $em->getRepository('VlanceCmsBundle:CommunityLike')->findBy(array('post' => $entity->getId()));
        $liker = false;
        foreach($liked as $l){
            if($l->getAccount() == $current_user){
                $liker = true;
            }
        }
        
        /** Kiểm tra số lượng người truy cập vào post **/
        if(!isset($_COOKIE['Viewer'])){
            setcookie('Viewer', $current_user, time() + 1800, '/cong-dong/'.$entity->getHash().'');
            $entity->setView($entity->getView() + 1);
            $em->persist($entity);
            $em->flush();
        }
        
        
        return array(
            'entity'    => $entity,
            'like'      => count($liked),
            'liker'     => $liker,
        );
    }

    /**
     * Like post
     *
     * @Route("/like", name="like_post")
     * @Method("POST")
     */
    public function likePostAction(Request $request)
    {
        //Check login
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new JsonResponse(array('error' => 101));
        }
        
        $em = $this->getDoctrine()->getManager();
        $current_user = $this->get('security.context')->getToken()->getUser();
        $post_id = $request->request->get('pid');
        
        $post = $em->getRepository('VlanceCmsBundle:CommunityPost')->findOneBy(array('id' => $post_id));
        if(!$post){
            return new JsonResponse(array('error' => 102));
        }
        
        $liked = $em->getRepository('VlanceCmsBundle:CommunityLike')->findOneBy(array('post' => $post_id, 'account' => $current_user->getId()));
        if($liked){
            $em->remove($liked);
            $em->flush();
            $likes = $em->getRepository('VlanceCmsBundle:CommunityLike')->findBy(array('post' => $post_id));
            return new JsonResponse(array('error' => 103, 'likes' => count($likes)));
        }
        
        $entity = new CommunityLike();
        $entity->setAccount($current_user);
        $entity->setPost($post);
        
        $em->persist($entity);
        $em->flush();
        
        $likes = $em->getRepository('VlanceCmsBundle:CommunityLike')->findBy(array('post' => $post_id));
        return new JsonResponse(array('error' => 0, 'likes' => count($likes)));
    }
    
    /**
     * Upload picture
     * @Route("/upload_image", name="upload_image")
     * @Method("POST") 
     */
    public function uploadImage()
    {
        if($_FILES["upload"]["error"] != 0){
            return new Response("Đã có lỗi xảy ra.(100)");
        }
        
        if($_FILES["upload"]["name"] == ""){
            return new Response("Đã có lỗi xảy ra.(101)");
        } 
        
        $now = new \DateTime('now');
        $date = $now->format('d-m-Y');
        
        if (!file_exists(UPLOAD_DIR . DS . "community_post" .DS . $date)) {
            mkdir(UPLOAD_DIR . DS . "community_post" .DS . $date, 0777, true);
        }
        
        $filename = sha1(uniqid(mt_rand(), true)) . basename($_FILES['upload']['name']);
        $path = "/community_post/" . $date . "/" . $filename;
        $uploadfile = UPLOAD_DIR . $path;
        
        $funcNum = $_GET['CKEditorFuncNum'] ;
        if (move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile)) {
            echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' . $funcNum . '","/uploads' . $path . '", "");</script>';
            return new Response();
        } else {
            return new Response("Đã có lỗi xảy ra.(102)");
        }
    }
    
    /**
     * Form edit post
     * 
     * @Route("/{hash}/edit", name="community_post_edit")
     * @Method("GET")
     * @Template("VlanceCmsBundle:Community:edit.html.php", engine="php")
     */
    public function editAction($hash)
    {
        return $this->redirect($this->generateUrl('vlance_homepage'));
        // Not permission
//        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
//            return $this->redirect($this->generateUrl('fos_user_security_login'));
//        }
        $acc = $this->get('security.context')->getToken()->getUser();
        if(!$acc){
            return $this->redirect($this->generateUrl("500_page"), 301);
        } else {
            if($acc->isAdmin() == false) {
                return $this->redirect($this->generateUrl("500_page"), 301);
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('VlanceCmsBundle:CommunityPost')->findOneBy(array('hash' => $hash));
        
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        
        $form = $this->createForm('vlance_cmsbundle_communityposttype', $entity);
        
        return array(
            'entity'=> $entity,
            'form'  => $form->createView(),
        );
    }
    
    /**
     * Edits an existing Community Post entity.
     *
     * @Route("/{hash}/update", name="community_post_update")
     * @Method("POST")
     * @Template("VlanceCmsBundle:Community:edit.html.php", engine="php")
     */
    public function updateCommunityPostAction(Request $request, $hash) {
        
        /**
         * Login required
         */
        if (false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('controller.login.required', array(), 'vlance'));
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        
        $em = $this->getDoctrine()->getManager();
        /* @var $entity CommunityPost */
        $entity = $em->getRepository('VlanceCmsBundle:CommunityPost')->findOneBy(array('hash' => $hash));
        
        /**
         * Entity must exist
         */
        if (!$entity) {
            return $this->redirect($this->generateUrl("404_page"), 301);
        }
        /**
         * Current user must be owner to update entity info
         */
//        $current_user = $this->get('security.context')->getToken()->getUser();
//        if(HasPermission::hasAdminPermission($current_user) == FALSE) {
//            return $this->redirect($this->generateUrl("500_page"), 301);
//        }
        
        $editForm = $this->createForm('vlance_cmsbundle_communityposttype', $entity);
        $editForm->bind($request);
        if ($editForm->isValid()) {
            //Update tags
            $tags_old = $entity->getTags();
            $tags_old_arr = array();
            $tags = explode(',', $_POST['hiddenTagListA']);
            //Remove tag
            foreach($tags_old as $t) {
                if(!in_array($t->getTitle(), $tags)){
                    $entity->removeTag($t);
                }
                $tags_old_arr[] = $t->getTitle();
            }
            // Insert tag
            foreach($tags as $tag_new) {
                if(empty($tag_new)) {
                    break;
                } elseif(!in_array($tag_new, $tags_old_arr)) {
                    $tag = $em->getRepository('VlanceCmsBundle:CommunityTag')->findOneBy(array('title' => $tag_new));

                    if(!$tag){
                        $tag_add = new CommunityTag();
                        $tag_add->setTitle($tag_new);
                        $tag_add->setHash($tag_new);
                        $em->persist($tag_add);
                        $em->flush();

                        $tag = $em->getRepository('VlanceCmsBundle:CommunityTag')->findOneBy(array('title' => $tag_new));    
                    }
                    $entity->addTag($tag);
                }
            }
//            
            $em->persist($entity);
            $em->flush();
            
            return $this->redirect($this->generateUrl('community_post_show', array('hash' => $entity->getHash()), true));
        }
    }
}
