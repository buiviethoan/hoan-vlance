<?php

namespace Vlance\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="community_post")
 * @ORM\Entity(repositoryClass="Vlance\CmsBundle\Entity\CommunityPostRepository")
 */
class CommunityPost
{
    /********** BASIC **********/
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Vlance\AccountBundle\Entity\Account", inversedBy="posts")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $account;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
    
    /**
     * @var string
     * @Gedmo\Slug(fields={"title"}, separator="-", unique=true, updatable = false)
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    protected $description;
    
    /**
     * @var string
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path;
    
    /**
     * 
     * @Assert\Image(maxSize ="5120k")
     */
    protected $file = null;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;
    /********** END BASIC **********/
    
    /********** EXTEND **********/
    /**
     * @var integer
     * @ORM\Column(name="view", type="integer")
     */
    protected $view = 0;
    
    /**
     * @ORM\ManyToMany(targetEntity="Vlance\CmsBundle\Entity\CommunityTag", inversedBy="posts")
     * @ORM\JoinTable(name="post_tag",
     * joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="cascade")},
     * inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $tags;
    
    /**
     * @ORM\OneToMany(targetEntity="Vlance\CmsBundle\Entity\CommunityLike", mappedBy="post")
     */
    protected $likes;
    /********** END EXTEND **********/
    
    /********** SEO **********/
    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="text", nullable=true)
     */
    protected $metaTitle = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text", nullable=true)
     */
    protected $metaDescription = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="text", nullable=true)
     */
    protected $metaKeyword = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_image", type="text", nullable=true)
     */
    protected $metaImage = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="meta_url", type="text", nullable=true)
     */
    protected $metaUrl = null;
    /********** END SEO **********/
    
    /********** ADMIN **********/
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime", nullable=true)
     */
    protected $publishedAt;
    /********** END SEO **********/
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CommunityPost
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return CommunityPost
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CommunityPost
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return CommunityPost
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Get full path
     *
     * @return string 
     */
    public function getFullPath()
    {
        return $this->getUploadDir() . DS . $this->path;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CommunityPost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return CommunityPost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set view
     *
     * @param integer $view
     * @return CommunityPost
     */
    public function setView($view)
    {
        $this->view = $view;
    
        return $this;
    }

    /**
     * Get view
     *
     * @return integer 
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return CommunityPost
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    
        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return CommunityPost
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    
        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     * @return CommunityPost
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->metaKeyword = $metaKeyword;
    
        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string 
     */
    public function getMetaKeyword()
    {
        return $this->metaKeyword;
    }

    /**
     * Set metaImage
     *
     * @param string $metaImage
     * @return CommunityPost
     */
    public function setMetaImage($metaImage)
    {
        $this->metaImage = $metaImage;
    
        return $this;
    }

    /**
     * Get metaImage
     *
     * @return string 
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * Set metaUrl
     *
     * @param string $metaUrl
     * @return CommunityPost
     */
    public function setMetaUrl($metaUrl)
    {
        $this->metaUrl = $metaUrl;
    
        return $this;
    }

    /**
     * Get metaUrl
     *
     * @return string 
     */
    public function getMetaUrl()
    {
        return $this->metaUrl;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return CommunityPost
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    
        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime 
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set account
     *
     * @param \Vlance\AccountBundle\Entity\Account $account
     * @return CommunityPost
     */
    public function setAccount(\Vlance\AccountBundle\Entity\Account $account = null)
    {
        $this->account = $account;
    
        return $this;
    }

    /**
     * Get account
     *
     * @return \Vlance\AccountBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add tags
     *
     * @param \Vlance\CmsBundle\Entity\CommunityTag $tags
     * @return CommunityPost
     */
    public function addTag(\Vlance\CmsBundle\Entity\CommunityTag $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param \Vlance\CmsBundle\Entity\CommunityTag $tags
     */
    public function removeTag(\Vlance\CmsBundle\Entity\CommunityTag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add likes
     *
     * @param \Vlance\CmsBundle\Entity\CommunityLike $likes
     * @return CommunityPost
     */
    public function addLike(\Vlance\CmsBundle\Entity\CommunityLike $likes)
    {
        $this->likes[] = $likes;
    
        return $this;
    }

    /**
     * Remove likes
     *
     * @param \Vlance\CmsBundle\Entity\CommunityLike $likes
     */
    public function removeLike(\Vlance\CmsBundle\Entity\CommunityLike $likes)
    {
        $this->likes->removeElement($likes);
    }

    /**
     * Get likes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     * @return CommunityPost
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    
    /**
     * 
     * @return string
     */
    public function getUploadDir() {
        return UPLOAD_DIR . "/community_post/admin";
    }
    
   /**
     * 
     * @return string
     */
    public function getUploadUrl() {
        return UPLOAD_URL. "/community_post/admin";
    }
 
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if($this->getFile() !== null) {
            $this->path = sha1(uniqid(mt_rand(), true)) . '.' . $this->getFile()->getClientOriginalExtension();
        }
    }
    
    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }
        if (!file_exists($this->getUploadDir())) {
            mkdir($this->getUploadDir(), 0777, true);
        }
        $this->getFile()->move($this->getUploadDir(), $this->path);
        $this->file = null;
    }
    
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if($this->path === null) {
            return;
        }
        $file = $this->path;
        if(file_exists($file)) {
            unlink($file);
        }
    }
}