<?php

namespace Vlance\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Cms Poll
 *
 * @ORM\Table(name="cms_poll")
 * @ORM\Entity(repositoryClass="Vlance\CmsBundle\Entity\PollRepository")
 */
class Poll
{
    const TYPE_YESNO = 'yesno';
    const TYPE_OPTIONS = 'options';
    const VALUE_YES = 1;
    const VALUE_NO = 0;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    protected $content;
    
    /**
     * @var string
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('yesno', 'options') NOT NULL")
     */
    protected $type = self::TYPE_YESNO;
    
    /**
     * @var string
     *
     * @ORM\Column(name="limited", type="text", nullable=true)
     */
    protected $limited;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expired_at", type="datetime")
     */
    protected $expiredAt;
    
    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Poll
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Poll
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Set type
     *
     * @param string $type
     * @return Poll
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set limited
     *
     * @param string $limited
     * @return Poll
     */
    public function setLimited($limited)
    {
        $this->limited = $limited;
    
        return $this;
    }

    /**
     * Get limited
     *
     * @return string 
     */
    public function getLimited()
    {
        return $this->limited;
    }
    
    /**
     * Set expiredAt
     *
     * @param \DateTime $expiredAt
     * @return Poll
     */
    public function setExpiredAt($expiredAt)
    {
        $this->expiredAt = $expiredAt;
    
        return $this;
    }

    /**
     * Get expiredAt
     *
     * @return \DateTime 
     */
    public function getExpiredAt()
    {
        return $this->expiredAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Poll
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Poll
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}