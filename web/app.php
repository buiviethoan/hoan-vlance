<?php

function error_to_exception ($errno, $errstr, $errfile, $errline) {
    throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
}

set_error_handler('error_to_exception');

use Symfony\Component\ClassLoader\ApcClassLoader;
use Symfony\Component\HttpFoundation\Request;

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';

// Use APC for autoloading to improve performance.
// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.

$loader = new ApcClassLoader('vlance.vn', $loader);
$loader->register(true);

require_once __DIR__.'/../app/AppKernel.php';
require_once __DIR__.'/../app/AppCache.php';

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
$kernel = new AppCache($kernel);
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();

/**
 * Define some global constances
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT_DIR', __DIR__);
define('UPLOAD_DIR', ROOT_DIR . DS . 'uploads');
define('JOB_UPLOAD_DIR', UPLOAD_DIR . DS . 'job');
define('PROFILE_UPLOAD_DIR', UPLOAD_DIR . DS . 'profile');

define('ROOT_URL', $request->getSchemeAndHttpHost());
define('UPLOAD_URL', ROOT_URL . '/uploads');
define('JOB_UPLOAD_URL', UPLOAD_URL . '/job');
define('PROFILE_UPLOAD_URL', UPLOAD_DIR . '/profile');

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
