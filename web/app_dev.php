<?php
//ini_set('memory_limit', "1G");
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
//if (isset($_SERVER['HTTP_CLIENT_IP'])
//    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
//    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
//) {
//    header('HTTP/1.0 403 Forbidden');
//    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
//}

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';
//require_once __DIR__.'/../app/AppCache.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();

/**
 * Define some global constances
 */
define('DS', DIRECTORY_SEPARATOR);
define('ROOT_DIR', __DIR__);
define('UPLOAD_DIR', ROOT_DIR . DS . 'uploads');
define('UPLOAD_TMP_DIR', UPLOAD_DIR . DS . 'tmp');
define('JOB_UPLOAD_DIR', UPLOAD_DIR . DS . 'job');
define('PROFILE_UPLOAD_DIR', UPLOAD_DIR . DS . 'profile');

define('UPLOAD_URL_WEB','/uploads');
define('ROOT_URL', $request->getSchemeAndHttpHost());
define('UPLOAD_URL', ROOT_URL . '/uploads');
define('UPLOAD_TMP_URL', UPLOAD_URL .'/tmp');
define('JOB_UPLOAD_URL', UPLOAD_URL . '/job');
define('PROFILE_UPLOAD_URL', UPLOAD_DIR . '/profile');


$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
