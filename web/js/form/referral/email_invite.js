$(document).ready(function() {
    if($("body").hasClass("email_invite")){
        // Initialisation
        $("#popup_gamil_contact").modal('show');

        // When confirm button is clicked
        $("#confirm-send-invites").click(function(){
            $(".popup_share_main").addClass("loading");
            setTimeout(function(){ 
                $("#popup_gamil_contact").modal('hide');
                $("#popup_notification").modal('show');
            }, 2000);
            if(typeof url != "undefined" && typeof emails != "undefined" && url != null && emails != null){
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        emails: emails,
                    }
                }).done(function(result){
                }).fail(function(result){
                }).always(function(){
                    vtrack("Invitor Send Email", {'channel' : 'gmail', 'count' : quantity});
                    $("#popup_gamil_contact").modal('hide');
                });
            } else{
                console.log("Error EI");
            }
        });

        jQuery('#share-button').click(function(){
            if(href != null){
                FB.ui({
                    method: 'share',
                    href: href,
                }
                , function(response){});
                vtrack('Invitor Copy Ref Link',{'channel':'facebook'});
            } else {
                alert("Error");
            }
        });

        jQuery('#copyToClipboard').click(function(){
            $('#prependedInput').select();
            document.execCommand('copy');
            document.getElementById("noti-ctc").innerHTML = "Đường dẫn đã được sao lưu vào bộ nhớ.";
            vtrack('Invitor Copy Ref Link',{'channel':'clipboard'});
        });
    }
});