//Mảng định dạng file upload ?>
var arr_file_upload = ['image/jpeg'];
var required;
$(document).ready(function (){   
    $('#form-create-id-number .popovers-input').popover();
    
    //check required is not undefined
    if(typeof file_required != "undefined"){
        required = file_required;
    } else {
        required = false;
    }
    
    /** Input birthday */
    var end = new Date();
    var start = new Date();
    start.setYear(end.getYear() - 100); //100 years
    
    $('#vlance_accountbundle_personidtype_dateOfBirth').datepicker({
        format:   'dd/mm/yyyy',
        language: 'vn',
        autoclose: true,
        weekStart: 1,
        startDate: start,
        endDate: end,
    });
    /** End */
    
    $("#form-create-id-number").validate({
        rules: {
            "vlance_accountbundle_personidtype[fileFront]": {
                "required": required,
                "filesize": true,
                "filetype": true,
            },
            "vlance_accountbundle_personidtype[fileBack]": {
                "required": required,
                "filesize": true,
                "filetype": true,
            },
            "vlance_accountbundle_personidtype[fullName]": {
                "required": true,
            },
            "vlance_accountbundle_personidtype[idNumber]": {
                "required": true,
                "maxlength": 12,
                "minlength": 9,
                "number": true,
            },
            "vlance_accountbundle_personidtype[issuedLocation]": {
                "required": true,
            },
            "vlance_accountbundle_personidtype[issuedDate]": {
                "required": true,
            },
        }
    });
    
    //check type
    jQuery.validator.addMethod("filetype", function(val, element){
        if(element.files[0]){
            var type = element.files[0].type;
            if($.inArray(type, arr_file_upload) == -1){
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }, "File phải đúng định dạng jpg");
    
    //check size
    jQuery.validator.addMethod("filesize", function (val, element) {
        if(element.files[0]){
            var size = element.files[0].size;
            if (size > 10485760)// checks the file more than 10 MB
            {
                return false;
            } else {
                return true;
            }
        } else {
            return true;  
        }
    }, "Dung lượng file phải nhỏ hơn 10MB");
        
    //check id number
    jQuery.validator.addMethod("number", function (val, element) {
        if(isNaN(val)){
            return false;
        } else {
            return true;
        }
    }, "CMND phải là chữ số");
    
    //Hide button verify when change value
    $("#form-create-id-number").change(function(){
        $('.block-verify').hide();
    });
    
    /** Check dimensions */
    var submit = true;
    var fileF = true;
    var fileB = true;
    $("#vlance_accountbundle_personidtype_fileFront").change(function(){
        if(this.files[0]){
            URL = window.URL || window.webkitURL;
            img = new Image();
            img.src = window.URL.createObjectURL(this.files[0]);

            img.onload = function() {
                window.URL.revokeObjectURL(img.src);

                if(img.naturalWidth < 1200 || img.naturalHeight < 1200) {
                    fileF = false;
                    $('.file_upload_front label.error').remove();
                    $('.file_upload_front label.upload_error').remove();
                    $('.file_upload_front').append("<label for='vlance_accountbundle_personidtype_fileFront' class='upload_error' style='display: block !important;position: relative;top: -5px;font-weight: normal;line-height: 21px;color: red;margin-top: 10px;'><b>Kích thước ảnh nhỏ nhất là 1200x1200 px</b></label>");
                } else {
                    fileF = true;
                    $('.file_upload_front label.upload_error').remove();
                }
            };
        } else {
            fileF = true;
            submit = true;
            $('.file_upload_front label.upload_error').remove();
        }
    });
    
    $("#vlance_accountbundle_personidtype_fileBack").change(function(){
        if(this.files[0]){
            URL = window.URL || window.webkitURL;
            img = new Image();
            img.src = window.URL.createObjectURL(this.files[0]);

            img.onload = function() {
                window.URL.revokeObjectURL(img.src);

                if(img.naturalWidth < 1200 || img.naturalHeight < 1200) {
                    fileB = false;
                    $('.file_upload_back label.error').remove();
                    $('.file_upload_back label.upload_error').remove();
                    $('.file_upload_back').append("<label for='vlance_accountbundle_personidtype_fileBack' class='upload_error' style='display: block !important;position: relative;top: -5px;font-weight: normal;line-height: 21px;color: red;margin-top: 10px;'><b>Kích thước ảnh nhỏ nhất là 1200x1200 px</b></label>");
                } else {
                    fileB = true;
                    $('.file_upload_back label.upload_error').remove();
                }
            };
        } else {
            fileB = true;
            submit = true;
            $('.file_upload_back label.upload_error').remove();
        }
    });
    
    function checkSubmit(){
        if(fileF == false){
            $('#vlance_accountbundle_personidtype_fileFront').focus();
            submit = false;
            return false;
        }
        if(fileB == false){
            $('#vlance_accountbundle_personidtype_fileBack').focus();
            submit = false;
            return false;
        }
        if(fileF == true && fileB == true){
            submit = true;
        }
    }
    /** End check dimensions */
    
//    $("#btn-submit-id-form").click(function(event) {
//        checkSubmit();
//        if(submit == true){
//            if($("#form-create-id-number").valid()){
//                $("#form-create-id-number").submit();
//                $("#btn-submit-id-form").prop('disabled', true);
//            }
//        }
//    });
    
    //submit form
    function submitFormVerifyId(){
        //if($("#form-create-id-number").valid()){
            $("#form-create-id-number").submit();
            $("#btn-submit-id-form").prop('disabled', true);
        //}
    }

    //Click submit button to open popup
    $("#btn-submit-id-form").click(function() {
        checkSubmit();
        if(submit == true){
            if($("#form-create-id-number").valid()){
                $("#popup-submit-verify-id").modal('show');
            }
        }
    });
    
    // POPUP submit when requested
    $("#btn-submit-requested").click(function() {
        checkSubmit();
        if(submit == true){
            if($("#form-create-id-number").valid()){
                $("#popup-submit-requested").modal('show');
            }
        }
    });
    
    $("#btn-submit-requested-id").click(function() {
        submitFormVerifyId();
    });
    
    /** POPUP submit + verify form **/
    $("#btn-submit-info").click(function() {
        submitFormVerifyId();
    });
    
    if(typeof action != "undefined"){
        $("#btn-request-verify-id").click(function(){
            document.getElementById('form-create-id-number').action = action;
            submitFormVerifyId();
        });
    }
    /** END **/
    
    //mở popup xác thực cmnd
    $("#verify-id").click(function() {
        $("#popup-confirm-verify-id").modal('show');
    });
    
    //gửi yêu cầu xác thực cmnd
    $("#confirm-verify-id").click(function(){
        $("#popup-confirm-verify-id").addClass("loading");
        $.ajax({
            url: url,
            type: "POST",
        }).done(function(result){
            $("#popup-confirm-verify-id").modal('hide');
            if(result.error == 0){
                verifyIdSuccess();
            } else if(result.error == 102) {
                verifyIdFailedBirthday();
            } else {
                verifyIdFailed();
            }
        }).fail(function(result){
            verifyIdFailed();
        }).always(function(){
            $("#popup-confirm-verify-id").removeClass("loading");
            $("#popup-confirm-verify-id").modal('hide');
        });
    });
    
    //reload lại trang xác thực
    $('#popup-verified-id-noti').on('hidden', function () {
        location.reload();
    });
});

function verifyIdSuccess(){
    $("#popup-verified-id-noti").modal('show');
    $(".verified-id-status").hide();
    $(".verified-id-status.success").show();
}

function verifyIdFailed(){
    $("#popup-verified-id-noti").modal('show');
    $(".verified-id-status").hide();
    $(".verified-id-status.fail").show();
}

function verifyIdFailedBirthday(){
    $("#popup-verified-id-noti").modal('show');
    $(".verified-id-status").hide();
    $(".verified-id-status.birthday").show();
}