$(document).ready(function() {
    $('#edit-acc .popovers-input').popover();
    
    $("#edit-acc").validate({
        rules: {
            "vlance_accountbundle_loginaftertype[telephone]": {
                required: true,
                maxlength: 11,
                minlength: 10,
                isNumber: true,
            },
            "vlance_accountbundle_loginaftertype[email]": {
                required: true,
                isEmail: true,
            },
        },
    });
    
    jQuery.validator.addMethod("isNumber", function(val, element){
        if(isNaN(val)){
            return false;
        } else {
            return true;
        }
    }, "Số điện thoại phải là chữ số.");
    
    jQuery.validator.addMethod("isEmail", function (val, element) {
            if(val){
            if(/\S+@\S+\.\S+/.test(val) == true){
                var emailRegex = new RegExp('@facebook.com','i');
                if(emailRegex.test(val)) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            return true;  
        }
    }, "Email không đúng");
    
    $('#vlance_accountbundle_loginaftertype_telephone').on('keypress input', function(event){
        if($('#vlance_accountbundle_loginaftertype_telephone').val().length < 11){
            return event.charCode >= 48 && event.charCode <= 57;
        } else {
            return false;
        }
    });
    
    $('#btn-update-acc').click(function(){
        if($('#edit-acc').valid()){
            $('#edit-acc').submit();
        }
    });
});