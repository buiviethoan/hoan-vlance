//Mảng định dạng file upload ?>
var arr_file_upload = ['gif','png','jpg'];

$(document).ready(function() {
    if($("body").hasClass("account_basic_edit")){
        $("#edit-acc").validate({
            rules: {
                "vlance_accountbundle_accountbasictype[telephone]": {
                    required: true,
//                    maxlength: 11,
//                    minlength: 10,
                    isnumber: true,
                },
                "vlance_accountbundle_accountbasictype[skype]": {
                    required: false,
                    whitespace: true,
                },
            },
            onfocusout: false,
            onkeyup: false,
            onclick: false
        });
        jQuery.validator.addMethod("isnumber", function(val, element){
            if(isNaN(val)){
                return false;
            } else {
                return true;
            }
        }, "Số điện thoại phải là chữ số.");
        jQuery.validator.addMethod("whitespace", function(val, element){
            if(/\s/.test(val)){
                return false;
            } else {
                return true;
            }
        }, "Skype không được chứa kí tự trống.");
    }
    
    if($("body").hasClass("account_basic_edit") || $("body").hasClass("account_update") || $("body").hasClass("account_edit") || $("body").hasClass("account_portfolio_edit") || $("body").hasClass("portfolio_edit")){
        $('#edit-acc').find("input[type=password]").val('');

        // popver khi hover vào các input
        $('#edit-acc .popovers-input').popover();
        
        if($("body").hasClass("account_basic_edit") || $("body").hasClass("account_update")){
            // Validation on submitting form
            $("#edit-acc .submit-form-edit-profile input[type=submit]").click(function(event) {
                var ext = $('#vlance_accountbundle_accountbasictype_file').val().split('.').pop().toLowerCase();
                if($.inArray(ext, arr_file_upload) == -1 && $('#vlance_accountbundle_accountbasictype_file').val()!="") {
                    $('#edit-acc .file_upload label.error').remove();
                    $('#edit-acc .file_upload').append('<label for="vlance_accountbundle_accountbasictype_file" class="error" style="display: block !important">' + $('#vlance_accountbundle_accountbasictype_file').attr('error') + '</label>');
                    $('#edit-acc .file_upload input').focus();
                    return false;
                }else{
                    $('#edit-acc .file_upload label.error').remove();
                    if($("#edit-acc").valid()){
                        $("#edit-acc .submit-form-edit-profile .button").submit();
                    }
                    else{
                        return false;
                    }
                }

                var email = $('#vlance_accountbundle_accountbasictype_email').val();
                if(email != "") {
                    var emailRegex = new RegExp('@facebook.com','i');
                    if(emailRegex.test(email)) {
                        $('#vlance_accountbundle_accountbasictype_email').after('<label class="error" for="vlance_accountbundle_accountbasictype_email">Email không được chứa "@facebook.com".</label>');
                        $('#vlance_accountbundle_accountbasictype_email').focus();
                        return false;
                    }
                }
            });
        }
        $("#edit-acc input[type=submit]").click(function() {
            validate_account_edit('#edit-acc');
            if ($('#edit-acc').valid() === true) {}
            else {
                $('#edit-acc').validate().focusInvalid();
            }
        });

        $(".form-add-skill").click(function(){
                $(this).find('input').focus();
            });
        
        if(typeof source != "undefined"){
            var tagApi = $(".tm-input").tagsManager({
                prefilled: (typeof prefilledSkills != 'undefined' ? prefilledSkills : null),
                CapitalizeFirstLetter: false,
                preventSubmitOnEnter: true,
                AjaxPush: null,
                AjaxPushAllTags: false,
                AjaxPushParameters: null,
                delimiters: [9, 13, 44],
                backspace: [8],
                blinkBGColor_1: '#FFFF9C',
                blinkBGColor_2: '#CDE69C',
                hiddenTagListName: 'hiddenTagListA',
                deleteTagsOnBackspace: true,
                tagsContainer: null,
                tagCloseIcon: '×',
                tagClass: '',
                validator: function(e, d){
                    if(inArrayCaseInsensitive(e, source) == -1){
                        return false;
                    } else {
                        return true;
                    }
                },
                onlyTagList: true
            });
            $('.tm-input').typeahead({
                source: source,
                items: '10', 
                updater: function(item){
                    $('.tm-input').val("");
                    $('.tm-input').tagsManager("pushTag", item);
                }
            });
        }


        $(".form-add-service").click(function(){
            $(this).find('input').focus();
        });
        if(typeof cat_source != "undefined"){
            var cat_tagApi = $(".tm-input-cat").tagsManager({
                prefilled: (typeof prefilledServices != 'undefined' ? prefilledServices : null),
                CapitalizeFirstLetter: false,
                preventSubmitOnEnter: true,
                AjaxPush: null,
                AjaxPushAllTags: false,
                AjaxPushParameters: null,
                delimiters: [9, 13, 44],
                backspace: [8],
                blinkBGColor_1: '#FFFF9C',
                blinkBGColor_2: '#CDE69C',
                hiddenTagListName: 'hiddenTagListB',
                deleteTagsOnBackspace: true,
                tagsContainer: null,
                tagCloseIcon: 'xóa bỏ',
                tagClass: '',
                validator: function(e, d){
                    if(inArrayCaseInsensitive(e, cat_source) == -1){
                        return false;
                    } else {
                        return true;
                    }
                },
                onlyTagList: true,
                removePosition: 'right'
            });
            $('.tm-input-cat').typeahead({
                source: cat_source,
                items: '10', 
                updater: function(item){
                    $('.tm-input-cat').val("");
                    $('.tm-input-cat').tagsManager("pushTag", item);
                }
            });
        }
    }
});

var validate_account_edit = function(form) {
    $(form).validate({
        rules: {
            "vlance_accountbundle_accounttype_type[type]": {
                required: true
            },
            "vlance_accountbundle_accounttype_type[workAvailability]": {
                required: true
            },
            "vlance_accountbundle_accounttype_type[workRhythm]": {
                required: true
            },
            "vlance_accountbundle_accounttype_type[category]":  {
                required: true
            },
            "vlance_accountbundle_accounttype_type[description]":  {
                minlength: 100,
                required: true,
                contact_leak: true
            },
            "vlance_accountbundle_accounttype_type[title]": {
                required: true,
            },
        },
        onfocusout: false,
        onkeyup: false,
        onclick: false
    });
};

function inArrayCaseInsensitive(needle, haystackArray){
    //Iterates over an array of items to return the index of the first item that matches the provided val ('needle') in a case-insensitive way.  Returns -1 if no match found.
    var defaultResult = -1;
    var result = defaultResult;
    $.each(haystackArray, function(index, value) { 
        if (result == defaultResult && value.toLowerCase() == needle.toLowerCase()) {
            result = index;
        }
    });
    return result;
}
