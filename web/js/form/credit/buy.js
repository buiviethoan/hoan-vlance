$(document).ready(function() {
    // Form buy credit
    if(typeof creditPackage != 'undefined' && typeof form_name_buy_credit != 'undefined' && typeof form_name_buy_credit_usercash != 'undefined'){
        // Initialisation
        var order = new Package(creditPackage);
        order.initiate();
        
        var popupBuyCreditSms = new BuyCreditForm(form_name_buy_credit);
        popupBuyCreditSms.initiateListener(function(){
            popupBuyCreditSms.updateAmount(order.getPackage());
            
            // On click on the pay-via-otp-sms button
            $('#pay-by-sms').on('click', function() {
                package = order.getPackage();
                if(package.amount > 50000){
                    alert("Rất tiếc, bạn không thể thanh toán các gói credit có giá trị trên 100.000 VNĐ bằng SMS.");
                } else{
                    popupBuyCreditSms.dataRequest = {'package' : order.getPackage()};
                    popupBuyCreditSms.dataConfirm = null;
                    popupBuyCreditSms.urlRequest = $(this).data('urlRequest');
                    popupBuyCreditSms.urlConfirm = $(this).data('urlConfirm');
                    $(form_name_buy_credit).modal('show');
                }
            });

            // POPUP FORM - Initialisation 
            // When the popup show
            $(form_name_buy_credit).on('show', function(){
                popupBuyCreditSms.updateAmount(order.getPackage());
                popupBuyCreditSms.formShowScreenRequest();
            });

            // When the popup hide
            $(form_name_buy_credit).on('hidden', function(){
                popupBuyCreditSms.formCleanUpScreen();
            });

            // STEP 1: Confirm telephone, request OTP number
            $(form_name_buy_credit + ' .click-contact .pay-request a.yes').click(function(){
                popupBuyCreditSms.updateTelephone($(form_name_buy_credit + ' #sms-telephone').val());

                // Check Operator
                var operator = getOperator(popupBuyCreditSms.telephone);
                if (operator == 3){  // VINAPHONE
                    $(form_name_buy_credit).addClass('redirecting');
                    setTimeout(function(){
                        popupBuyCreditSms.requestOtpVinaphone();
                    }, 4000);
                } else {            // VIETTEL, MOBIFONE, OTHER
                    $(form_name_buy_credit).addClass('loading');
                    popupBuyCreditSms.requestOtp();
                    $(form_name_buy_credit + ' .click-contact .pay-confirm .first strong').html(popupBuyCreditSms.telephone);
                    popupBuyCreditSms.formShowScreenConfirmOtp();
                }
            });

            // STEP 2: Confirm OTP code
            $(form_name_buy_credit + ' .click-contact .pay-confirm a.yes').click(function(){
                $(form_name_buy_credit).addClass('loading');
                popupBuyCreditSms.dataConfirm.otp = $(form_name_buy_credit + ' #sms-confirm-code').val();
                popupBuyCreditSms.confirmOtp();
                popupBuyCreditSms.formShowScreenConfirmOtp();
            });

            // Error Screen - Primary button
            $(form_name_buy_credit + ' .pay-dialog.pay-error .btn-primary').click(function(){
                popupBuyCreditSms.formHideError();
                $(form_name_buy_credit + ' .click-contact .pay-error .retry').hide();
                $(form_name_buy_credit + ' #sms-confirm-code').val('');
                popupBuyCreditSms.formShowScreenRequest();
                $(form_name_buy_credit + ' .pay-dialog.pay-error .btn-primary').removeClass('request');
            });
        });
   
        var popupBuyCreditUsercash = new BuyCreditUsercash(form_name_buy_credit_usercash);
        popupBuyCreditUsercash.initiateListener(function(){
            popupBuyCreditUsercash.updateAmount(order.getPackage());
            // On click on the pay-via-otp-sms button
            $('[data-toggle="modal"]').on('click', function() {
                popupBuyCreditUsercash.dataConfirm = {'package' : order.getPackage()};
                popupBuyCreditUsercash.urlConfirm = $(this).data('urlConfirm');
            });

            // POPUP FORM - Initialisation 
            // When the popup show
            $(form_name_buy_credit_usercash).on('show', function(){
                popupBuyCreditUsercash.updateAmount(order.getPackage());
                popupBuyCreditUsercash.formShowScreenConfirm();
            });

            // When the popup hide
            $(form_name_buy_credit_usercash).on('hidden', function(){
                popupBuyCreditUsercash.formCleanUpScreen();
            });

            // Confirm pay by usercash
            $(form_name_buy_credit_usercash + ' a.confirm-pay-usercash').on('click', function(){
                $(form_name_buy_credit_usercash).addClass('loading');
                popupBuyCreditUsercash.confirm();
            });

            // Hide popup
            $(form_name_buy_credit_usercash + ' .click-contact a.no').click(function(){
                popupBuyCreditUsercash.formCleanUpScreen();
            });

            // Error Screen - Primary button
            $(form_name_buy_credit_usercash + ' .pay-dialog.pay-error .btn-primary').click(function(){
                popupBuyCreditUsercash.formHideError();
                $(form_name_buy_credit_usercash + ' .click-contact .pay-error .retry').hide();
                $(form_name_buy_credit_usercash + ' #sms-confirm-code').val('');
                popupBuyCreditUsercash.formShowScreenConfirm();
                $(form_name_buy_credit_usercash + ' .pay-dialog.pay-error .btn-primary').removeClass('request');
            });
        });
        
        
        /***************/
        
        // Here is the package definition
        function Package(creditPackage){
            this.package = null;
            
            this.getPackage = function() {
                return this.package;
            };
            
            this.initiate = function(){
                var that = this;                
                $("#buyCredit input[name=buyCreditPackage]").click(function(){
                    that.package = creditPackage[$("#buyCredit input[name=buyCreditPackage]:checked").val()];
                    if(that.package.amount > 50000){
                        $('#pay-by-sms').hide();
                    } else {
                        $('#pay-by-sms').show();
                    }
                });
                this.package = creditPackage[$("#buyCredit input[name=buyCreditPackage]:checked").val()];
                
                if(that.package.amount > 50000){
                    $('#pay-by-sms').hide();
                } else {
                    $('#pay-by-sms').show();
                }
            };
        };

        // BuyCreditForm by Sms
        function BuyCreditForm(form_name_buy_credit){
            this.urlRequest = '';
            this.urlConfirm = '';
            this.telephone = '';
            this.dataRequest = null;
            this.dataConfirm = null;
            
            this.initiateListener = function(callback){
                callback();
            };
            
            this.updateAmount = function(package){
                $(form_name_buy_credit + " .pay-request .note-telephone .amount").html(package.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                $(form_name_buy_credit + " .pay-confirm .telephone-sms .amount").html(package.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
                $(form_name_buy_credit + " .pay-confirm .note-telephone .amount").html(package.amount);
                
                //Usercash option
                //$('#pay-buy-credit-usercash .usercash-amount-confirm').html(package.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            };
            
            this.updateTelephone = function(number){
                this.telephone = number;
                $(form_name_buy_credit + " .pay-confirm .note-telephone p span").text(this.telephone);
            };
            
            this.updateCreditBalance = function(new_balance){
                $('span[data-credit="balance"]').html(new_balance);
            };
            
            this.requestOtp = function(){
                var that = this;
                this.dataRequest.telephone = this.telephone;
                $.ajax({
                    url: that.urlRequest,
                    type: "POST",
                    data: {
                        data_request: that.dataRequest
                    }
                }).done(function(result){
                    $(form_name_buy_credit).removeClass('loading');
                    if(result.error == 0 && result.response.errorCode === "18"){ // Đã gửi OTP cho khách hàng.
                        that.dataConfirm = that.dataRequest;
                        that.dataConfirm.tid = result.id;
                        vtrack('Confirm telephone status', {'case': 'pay-buy-credit','status': 'SUCCESS'});
                    }
                    else{
                        that.formShowError(result);
                        $(form_name_buy_credit + ' .pay-dialog.pay-error .btn-primary').addClass('request');
                        vtrack('Confirm telephone status', {'case': 'pay-buy-credit','status': 'FAIL'});
                    }
                });
            };
            
            this.requestOtpVinaphone = function(){
                var that = this;
                this.dataRequest.telephone = this.telephone;
                $.ajax({
                    url: that.urlRequest,
                    type: "POST",
                    data: {
                        data_request: that.dataRequest,
                    }
                }).done(function(result){
                    $(form_name_buy_credit).removeClass('loading');
                    if(result.error == 0 && result.response.errorCode === "18" && result.response.redirectUrl){ // Đã gửi OTP cho khách hàng.
                        that.dataConfirm = that.dataRequest;
                        that.dataConfirm.tid = result.id;
                        vtrack('Confirm telephone status', {'case': 'pay-buy-credit','status': 'SUCCESS'});
                        window.location.href = result.response.redirectUrl;
                    }
                    else{
                        that.formShowError(result);
                        $(form_name_buy_credit + ' .pay-dialog.pay-error .btn-primary').addClass('request');
                        vtrack('Confirm telephone status', {'case': 'pay-buy-credit','status': 'FAIL'});
                    }
                });
            };
            
            this.confirmOtp = function(){
                var that = this;
                $.ajax({
                    url: that.urlConfirm,
                    type: "POST",
                    data: {
                        data_confirm: that.dataConfirm
                    }
                }).done(function(result){
                    $(form_name_buy_credit).removeClass('loading');
                    if(result.error == 0 && result.response.errorCode === "00"){ // Success
                        that.formShowScreenSuccess();
                        that.updateCreditBalance(result.credit_balance);
                        vtrack('Confirm pay status', {'case': 'pay-buy-credit','status': 'SUCCESS'});
                    }
                    else{
                        that.formShowError(result);
                        vtrack('Confirm pay status', {'case': 'pay-buy-credit','status': 'FAIL'});
                    }
                });
            };
            
            this.formCleanUpScreen = function(){
                $(form_name_buy_credit).removeClass('loading');
                this.formShowScreenRequest();
                
                $(form_name_buy_credit).modal('hide');
                $(form_name_buy_credit + ' #sms-confirm-code').removeAttr('tid');
                $(form_name_buy_credit + ' #sms-confirm-code').removeAttr('fid');
                $(form_name_buy_credit + ' #sms-confirm-code').val('');
                this.formHideError();
                
                // Cleanup data;
                this.dataConfirm = null;
            };
            
            this.formShowError = function(response){
                $(form_name_buy_credit + ' .click-contact .pay-error .retry').hide();
                $(form_name_buy_credit + ' .click-contact .pay-dialog').hide();
                $(form_name_buy_credit + ' .click-contact .pay-error .error-reason').hide();
                if(response.errorMessage !== "" && response.errorMessage != null){
                    $(form_name_buy_credit + ' .click-contact .pay-error .error-reason span').html(response.errorMessage);
                    $(form_name_buy_credit + ' .click-contact .pay-error .error-reason').show();
                } else{
                    $(form_name_buy_credit + ' .click-contact .pay-error .error-reason').hide();
                }
                $(form_name_buy_credit + ' .click-contact .pay-error').show();
                if(response.errorCode == "10" || response.errorCode == "11"){ // Incorrect OTP or insufficient balance
                    $(form_name_buy_credit + ' .click-contact .pay-error .retry').show();
                }
            };
            
            this.formHideError = function(){
                $(form_name_buy_credit + ' .click-contact .pay-error .error-reason').hide();
                $(form_name_buy_credit + ' .click-contact .pay-error').hide();
            };
            
            this.formShowScreenRequest = function(){
                $(form_name_buy_credit + ' .click-contact .pay-dialog').hide();
                $(form_name_buy_credit + ' .click-contact .pay-request').show();
            };
            this.formShowScreenConfirmOtp = function(){
                $(form_name_buy_credit + ' .click-contact .pay-dialog').hide();
                $(form_name_buy_credit + ' .click-contact .pay-confirm').show();
            };
            this.formShowScreenSuccess = function(){
                $(form_name_buy_credit + ' .click-contact .pay-dialog').hide();
                $(form_name_buy_credit + ' .click-contact .pay-result').show();
            };
        };
        
        function BuyCreditUsercash(form_name_buy_credit_usercash){
            this.urlConfirm = '';
            this.dataConfirm = null;
            
            this.initiateListener = function(callback){
                callback();
            };
            
            this.updateAmount = function(package){
                //Usercash option
                $(form_name_buy_credit_usercash + ' .usercash-amount-confirm').html(package.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            };
            
            this.updateCreditBalance = function(new_balance){
                $('span[data-credit="balance"]').html(new_balance);
            };
            
            this.updateUsercashBalance = function(new_balance){
                $('span[data-usercash="balance"]').html(new_balance);
            };
            
            this.confirm = function(){
                var that = this;
                $.ajax({
                    url: that.urlConfirm,
                    type: "POST",
                    data: {
                        data_confirm: that.dataConfirm
                    }
                }).done(function(result){
                    $(form_name_buy_credit_usercash).removeClass('loading');
                    if(result.error === 0){ // Success
                        that.formShowScreenSuccess();
                        vtrack('Confirm pay usercash', {'case': 'pay-buy-credit-usercash','status': 'SUCCESS'});
                        if(typeof result.balance !== 'undefined'){
                            that.updateCreditBalance(result.balance.credit);
                            that.updateUsercashBalance(result.balance.usercash);
                        }
                    }
                    else{
                        that.formShowError(result.errorMessage);
                        vtrack('Confirm pay usercash', {'case': 'pay-buy-credit-usercash','status': 'FAIL'});
                    }
                });
            };
            
            this.formCleanUpScreen = function(){
                $(form_name_buy_credit_usercash).removeClass('loading');
                this.formShowScreenConfirm();
                $(form_name_buy_credit_usercash).modal('hide');
            };
            
            this.formShowError = function(errorMessage){
                $(form_name_buy_credit_usercash + ' .buy-success').hide();
                $(form_name_buy_credit_usercash + ' .buy-error').show();
                $(form_name_buy_credit_usercash + ' .buy-confirm').hide();
                
                if(errorMessage !== "" && errorMessage != null){
                    $(form_name_buy_credit_usercash + ' .buy-error .error-message').html(errorMessage);
                } else{
                    $(form_name_buy_credit_usercash + ' .buy-error .error-message').html("Rất tiếc đã có lỗi xảy ra.");
                }
            };
            
            this.formShowScreenConfirm = function(){
                $(form_name_buy_credit_usercash + ' .buy-success').hide();
                $(form_name_buy_credit_usercash + ' .buy-error').hide();
                $(form_name_buy_credit_usercash + ' .buy-confirm').show();
            };
            
            this.formShowScreenSuccess = function(){
                $(form_name_buy_credit_usercash + ' .buy-success').show();
                $(form_name_buy_credit_usercash + ' .buy-error').hide();
                $(form_name_buy_credit_usercash + ' .buy-confirm').hide();
            };
        };
        
        /**
         * Pay by Bank card 
         */
        if($("#pay-by-bankcard").length > 0 && $("#pay-buy-credit-bankcard").length > 0){
            var payBankcard = new BuyCreditBankcard();
            payBankcard.initiate();
            payBankcard.initiateListener(function(){
                $("#pay-by-bankcard").click(function(){
                    package = order.getPackage();
                    payBankcard.data_request = {package: package};
                    $("#pay-buy-credit-bankcard").modal("show");
                    payBankcard.request();
                });     
            });

            /**
             * Buy Credit with bank card
             */
            function BuyCreditBankcard(){
                this.requestUrl             = "";
                this.defaultErrorMessage    = "";
                this.data_request           = {};

                /* Initiate class, set default data */
                this.initiate = function(){
                    this.defaultErrorMessage = $("#pay-buy-credit-bankcard .screen-error .message").text() == "" ? $("#pay-buy-credit-bankcard .screen-error .message").text() : "Rất tiếc, đã có lỗi xảy ra.";
                    if($("#pay-by-bankcard").data("request-url") == "" || typeof $("#pay-by-bankcard").data("request-url") == "undefined"){
                        this.showError();
                        return;
                    } else{
                        this.requestUrl = $("#pay-by-bankcard").data("requestUrl");
                    }

                };

                /* Initiate listener trigger, then bind callback method */
                this.initiateListener = function(callback){
                    callback();
                };

                /* Send request query */
                this.request = function(){
                    // Validation
                    if(this.requestUrl == ""){
                        this.showError();
                        return;
                    }

                    // Show busy screen
                    $("#pay-buy-credit-bankcard .screen-content").hide();
                    $("#pay-buy-credit-bankcard .screen-waiting").show();

                    var that = this;
                    setTimeout(function(){
                        $.ajax({
                            type: "POST",
                            url: that.requestUrl,
                            data: {
                                data_request: that.data_request
                            }
                        }).done(function(result){
                            if(typeof result.response != "undefined"){
                                if(result.response.pay_url){
                                    that.redirect(result.response.pay_url);
                                }
                            }
                        });
                    }, 2000);
                    
                };

                /* Redirect to 1pay page */
                this.redirect = function(redirectUrl){
                    // Validation
                    if(this.requestUrl == "" || typeof redirectUrl == 'undefined' || redirectUrl == ""){
                        this.showError();
                        return;
                    }

                    // Redirect to new Url
                    window.location.href = redirectUrl;
                };

                /* Display error screen with given message */
                this.showError = function(message){
                    // Validation
                    if (typeof message != 'undefined' || message == ""){
                        message = this.defaultErrorMessage;
                    }

                    // Show error message
                    $("#pay-buy-credit-bankcard .screen-error .message").html(message);
                    $("#pay-buy-credit-bankcard .screen-content").hide();
                    $("#pay-buy-credit-bankcard .screen-error").show();

                }
            }
        }
        
        /**
         * Pay by visa card 
         */
        if($("#pay-by-visacard").length > 0 && $("#pay-buy-credit-visacard").length > 0){
            var payVisacard = new BuyCreditVisacard();
            payVisacard.initiate();
            payVisacard.initiateListener(function(){
                $("#pay-by-visacard").click(function(){
                    package = order.getPackage();
                    payVisacard.data_request = {package: package};
                    $("#pay-buy-credit-visacard").modal("show");
                    payVisacard.request();
                });     
            });

            /**
             * Buy Credit with visa card
             */
            function BuyCreditVisacard(){
                this.requestUrl             = "";
                this.defaultErrorMessage    = "";
                this.data_request           = {};

                /* Initiate class, set default data */
                this.initiate = function(){
                    this.defaultErrorMessage = $("#pay-buy-credit-visacard .screen-error .message").text() == "" ? $("#pay-buy-credit-visacard .screen-error .message").text() : "Rất tiếc, đã có lỗi xảy ra.";
                    if($("#pay-by-visacard").data("request-url") == "" || typeof $("#pay-by-visacard").data("request-url") == "undefined"){
                        this.showError();
                        return;
                    } else{
                        this.requestUrl = $("#pay-by-visacard").data("requestUrl");
                    }

                };

                /* Initiate listener trigger, then bind callback method */
                this.initiateListener = function(callback){
                    callback();
                };

                /* Send request query */
                this.request = function(){
                    // Validation
                    if(this.requestUrl == ""){
                        this.showError();
                        return;
                    }

                    // Show busy screen
                    $("#pay-buy-credit-visacard .screen-content").hide();
                    $("#pay-buy-credit-visacard .screen-waiting").show();

                    var that = this;
                    setTimeout(function(){
                        $.ajax({
                            type: "POST",
                            url: that.requestUrl,
                            data: {
                                data_request: that.data_request
                            }
                        }).done(function(result){
                            if(typeof result.response != "undefined"){
                                if(result.response.pay_url){
                                    that.redirect(result.response.pay_url);
                                }
                            }
                        });
                    }, 2000);
                    
                };

                /* Redirect to 1pay page */
                this.redirect = function(redirectUrl){
                    // Validation
                    if(this.requestUrl == "" || typeof redirectUrl == 'undefined' || redirectUrl == ""){
                        this.showError();
                        return;
                    }

                    // Redirect to new Url
                    window.location.href = redirectUrl;
                };

                /* Display error screen with given message */
                this.showError = function(message){
                    // Validation
                    if (typeof message != 'undefined' || message == ""){
                        message = this.defaultErrorMessage;
                    }

                    // Show error message
                    $("#pay-buy-credit-visacard .screen-error .message").html(message);
                    $("#pay-buy-credit-visacard .screen-content").hide();
                    $("#pay-buy-credit-visacard .screen-error").show();

                }
            }
        }
    }
});