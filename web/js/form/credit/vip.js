$(document).ready(function() {    
    if(typeof accountPackage != 'undefined' && typeof form_name_upgrade_account_usercash != 'undefined'){
        var orderVIP = new PackageVip(accountPackage);
        orderVIP.initiate();

        var popupUpgradeAccountUsercash = new upgradeAccountUsercash(form_name_upgrade_account_usercash);
        popupUpgradeAccountUsercash.initiateListener(function(){
            popupUpgradeAccountUsercash.updateAmount(orderVIP.getPackage());
            $('[data-toggle="modal"]').on('click', function() {
                popupUpgradeAccountUsercash.dataConfirm = {'package' : orderVIP.getPackage()};
                popupUpgradeAccountUsercash.urlConfirm = $(this).data('urlConfirm');
            });

            // POPUP FORM - Initialisation 
            // When the popup show
            $(form_name_upgrade_account_usercash).on('show', function(){
                popupUpgradeAccountUsercash.updateAmount(orderVIP.getPackage());
                popupUpgradeAccountUsercash.formShowScreenConfirm();
            });

            // When the popup hide
            $(form_name_upgrade_account_usercash).on('hidden', function(){
                popupUpgradeAccountUsercash.formCleanUpScreen();
            });

            // Confirm pay by usercash
            $(form_name_upgrade_account_usercash + ' a.confirm-pay-upgrade-usercash').on('click', function(){
                $(form_name_upgrade_account_usercash).addClass('loading');
                popupUpgradeAccountUsercash.confirm();
            });

            // Hide popup
            $(form_name_upgrade_account_usercash + ' .click-contact a.no').click(function(){
                popupUpgradeAccountUsercash.formCleanUpScreen();
            });

            // Error Screen - Primary button
            $(form_name_upgrade_account_usercash + ' .pay-dialog.pay-error .btn-primary').click(function(){
                popupUpgradeAccountUsercash.formHideError();
                $(form_name_upgrade_account_usercash + ' .click-contact .pay-error .retry').hide();
                popupUpgradeAccountUsercash.formShowScreenConfirm();
                $(form_name_upgrade_account_usercash + ' .pay-dialog.pay-error .btn-primary').removeClass('request');
            });
        });

        function PackageVip(packageVip){
            this.packageVip = null;

            this.getPackage = function() {
                return this.packageVip;
            };

            this.initiate = function(){
                var that = this;           
                $("#upgrade-account input[name=buyPackageVip]").click(function(){
                    that.packageVip = packageVip[$("#upgrade-account input[name=buyPackageVip]:checked").val()];
                });

                this.packageVip = packageVip[$("#upgrade-account input[name=buyPackageVip]:checked").val()];
            };
        };

        function upgradeAccountUsercash(form_name_upgrade_account){
            this.urlConfirm = '';
            this.dataConfirm = null;

            this.initiateListener = function(callback){
                callback();
            };

            this.updateAmount = function(packageVip){
                $(form_name_upgrade_account + ' .usercash-amount-confirm').html(packageVip.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            };

            this.updateUsercashBalance = function(new_balance){
                $('span[data-usercash="balance"]').html(new_balance);
            };

            this.confirm = function(){
                var that = this;
                $.ajax({
                    url: that.urlConfirm,
                    type: "POST",
                    data: {
                        data_confirm: that.dataConfirm
                    }
                }).done(function(result){
                    $(form_name_upgrade_account).removeClass('loading');
                    if(result.error === 0){
                        that.formShowScreenSuccess();
                        vtrack('Confirm upgrade usercash', {'case': 'pay-upgrade-account-usercash','status': 'SUCCESS'});
                        if(typeof result.balance !== 'undefined'){
                            that.updateUsercashBalance(result.balance.usercash);
                        }
                    }
                    else{
                        that.formShowError(result.errorMessage);
                        vtrack('Confirm upgrade usercash', {'case': 'pay-upgrade-account-usercash','status': 'FAIL'});
                    }
                });
            };

            this.formCleanUpScreen = function(){
                $(form_name_upgrade_account).removeClass('loading');
                this.formShowScreenConfirm();
                $(form_name_upgrade_account).modal('hide');
            };

            this.formShowError = function(errorMessage){
                $(form_name_upgrade_account + ' .buy-success').hide();
                $(form_name_upgrade_account + ' .buy-error').show();
                $(form_name_upgrade_account + ' .buy-confirm').hide();

                if(errorMessage !== "" && errorMessage !== null){
                    $(form_name_upgrade_account + ' .buy-error .error-message').html(errorMessage);
                } else{
                    $(form_name_upgrade_account + ' .buy-error .error-message').html("Rất tiếc đã có lỗi xảy ra.");
                }
            };

            this.formShowScreenConfirm = function(){
                $(form_name_upgrade_account + ' .buy-success').hide();
                $(form_name_upgrade_account + ' .buy-error').hide();
                $(form_name_upgrade_account + ' .buy-confirm').show();
            };

            this.formShowScreenSuccess = function(){
                $(form_name_upgrade_account + ' .buy-success').show();
                $(form_name_upgrade_account + ' .buy-error').hide();
                $(form_name_upgrade_account + ' .buy-confirm').hide();
            };
        };
        
        /**
         * Pay by Bank card 
         */
        if($("#pay-upgrade-by-bankcard").length > 0 && $("#pay-upgrade-account-bankcard").length > 0){
            var payBankcard = new BuyCreditBankcard();
            payBankcard.initiate();
            payBankcard.initiateListener(function(){
                $("#pay-upgrade-by-bankcard").click(function(){
                    payBankcard.data_request = {package: orderVIP.getPackage()};
                    $("#pay-upgrade-account-bankcard").modal("show");
                    payBankcard.request();
                });     
            });

            /**
             * Buy Credit with bank card
             */
            function BuyCreditBankcard(){
                this.requestUrl             = "";
                this.defaultErrorMessage    = "";
                this.data_request           = {};

                /* Initiate class, set default data */
                this.initiate = function(){
                    this.defaultErrorMessage = $("#pay-upgrade-account-bankcard .screen-error .message").text() == "" ? $("#pay-upgrade-account-bankcard .screen-error .message").text() : "Rất tiếc, đã có lỗi xảy ra.";
                    if($("#pay-upgrade-by-bankcard").data("request-url") == "" || typeof $("#pay-upgrade-by-bankcard").data("request-url") == "undefined"){
                        this.showError();
                        return;
                    } else{
                        this.requestUrl = $("#pay-upgrade-by-bankcard").data("requestUrl");
                    }

                };

                /* Initiate listener trigger, then bind callback method */
                this.initiateListener = function(callback){
                    callback();
                };

                /* Send request query */
                this.request = function(){
                    // Validation
                    if(this.requestUrl == ""){
                        this.showError();
                        return;
                    }

                    // Show busy screen
                    $("#pay-upgrade-account-bankcard .screen-content").hide();
                    $("#pay-upgrade-account-bankcard .screen-waiting").show();

                    var that = this;
                    setTimeout(function(){
                        $.ajax({
                            type: "POST",
                            url: that.requestUrl,
                            data: {
                                data_request: that.data_request
                            }
                        }).done(function(result){
                            if(typeof result.response != "undefined"){
                                if(result.response.pay_url){
                                    that.redirect(result.response.pay_url);
                                }
                            }
                        });
                    }, 2000);
                    
                };

                /* Redirect to 1pay page */
                this.redirect = function(redirectUrl){
                    // Validation
                    if(this.requestUrl == "" || typeof redirectUrl == 'undefined' || redirectUrl == ""){
                        this.showError();
                        return;
                    }

                    // Redirect to new Url
                    window.location.href = redirectUrl;
                };

                /* Display error screen with given message */
                this.showError = function(message){
                    // Validation
                    if (typeof message != 'undefined' || message == ""){
                        message = this.defaultErrorMessage;
                    }

                    // Show error message
                    $("#pay-upgrade-account-bankcard .screen-error .message").html(message);
                    $("#pay-upgrade-account-bankcard .screen-content").hide();
                    $("#pay-upgrade-account-bankcard .screen-error").show();

                }
            }
        }
        
        /**
         * Pay by visa card 
         */
        if($("#pay-upgrade-by-visacard").length > 0 && $("#pay-upgrade-account-visacard").length > 0){
            var payVisacard = new UpgradeAccountVisacard();
            payVisacard.initiate();
            payVisacard.initiateListener(function(){
                $("#pay-upgrade-by-visacard").click(function(){
                    payVisacard.data_request = {package: orderVIP.getPackage()};
                    $("#pay-upgrade-account-visacard").modal("show");
                    payVisacard.request();
                });     
            });

            /**
             * Buy Credit with visa card
             */
            function UpgradeAccountVisacard(){
                this.requestUrl             = "";
                this.defaultErrorMessage    = "";
                this.data_request           = {};

                /* Initiate class, set default data */
                this.initiate = function(){
                    this.defaultErrorMessage = $("#pay-upgrade-account-visacard .screen-error .message").text() == "" ? $("#pay-upgrade-account-visacard .screen-error .message").text() : "Rất tiếc, đã có lỗi xảy ra.";
                    if($("#pay-upgrade-by-visacard").data("request-url") == "" || typeof $("#pay-upgrade-by-visacard").data("request-url") == "undefined"){
                        this.showError();
                        return;
                    } else{
                        this.requestUrl = $("#pay-upgrade-by-visacard").data("requestUrl");
                    }

                };

                /* Initiate listener trigger, then bind callback method */
                this.initiateListener = function(callback){
                    callback();
                };

                /* Send request query */
                this.request = function(){
                    // Validation
                    if(this.requestUrl == ""){
                        this.showError();
                        return;
                    }

                    // Show busy screen
                    $("#pay-upgrade-account-visacard .screen-content").hide();
                    $("#pay-upgrade-account-visacard .screen-waiting").show();

                    var that = this;
                    setTimeout(function(){
                        $.ajax({
                            type: "POST",
                            url: that.requestUrl,
                            data: {
                                data_request: that.data_request
                            }
                        }).done(function(result){
                            if(typeof result.response != "undefined"){
                                if(result.response.pay_url){
                                    that.redirect(result.response.pay_url);
                                }
                            }
                        });
                    }, 2000);
                    
                };

                /* Redirect to 1pay page */
                this.redirect = function(redirectUrl){
                    // Validation
                    if(this.requestUrl == "" || typeof redirectUrl == 'undefined' || redirectUrl == ""){
                        this.showError();
                        return;
                    }

                    // Redirect to new Url
                    window.location.href = redirectUrl;
                };

                /* Display error screen with given message */
                this.showError = function(message){
                    // Validation
                    if (typeof message != 'undefined' || message == ""){
                        message = this.defaultErrorMessage;
                    }

                    // Show error message
                    $("#pay-upgrade-account-visacard .screen-error .message").html(message);
                    $("#pay-upgrade-account-visacard .screen-content").hide();
                    $("#pay-upgrade-account-visacard .screen-error").show();
                }
            }
        }
    }
});