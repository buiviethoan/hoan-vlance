$(document).ready(function() {    
    /** Form Spend Credit - Feature job **/
    if(typeof form_spend_credit_feature_job != 'undefined' && typeof form_spend_credit_feature_job_params != 'undefined'){
        var feature_job_spending = new creditSpending(form_spend_credit_feature_job, form_spend_credit_feature_job_params);
        feature_job_spending.formInitiate(
            function(request){
                if(request.jid != null){
                    feature_job_spending.jid = request.jid;
                }
            },
            function(){
                feature_job_spending.preprocessData = function(data){
                    if(feature_job_spending.jid != null ){
                        data.jid = feature_job_spending.jid;
                    }
                    return data;
                };
                
                /* Binding some action */
                $("input:radio[name=feature-promote-fanpage]").click(function() {
                    service_id = $("input:radio[name=feature-promote-fanpage]:checked").val();
                    feature_job_spending.setServiceId(service_id);
                });
            }
        );
    }
    
    /** Form Spend Credit - Feature bid **/
    if(typeof form_spend_credit_feature_bid != 'undefined' && typeof form_spend_credit_feature_bid_params != 'undefined'){
        var feature_bid_spending = new creditSpending(form_spend_credit_feature_bid, form_spend_credit_feature_bid_params);
        feature_bid_spending.formInitiate(
            function(request){
                if(request.bid != null){
                    feature_bid_spending.bid = request.bid;
                }
            },
            function(){
                feature_bid_spending.preprocessData = function(data){
                    if(feature_bid_spending.bid != null ){
                        data.bid = feature_bid_spending.bid;
                    }
                    return data;
                };
            }
        );
    }
    
    /** Form Spend Credit - Feature freelancer **/
    if(typeof form_spend_credit_feature_freelancer != 'undefined' && typeof form_spend_credit_feature_freelancer_params != 'undefined'){
        var feature_freelancer_spending = new creditSpending(form_spend_credit_feature_freelancer, form_spend_credit_feature_freelancer_params);
        feature_freelancer_spending.formInitiate(
            function(request){
                if(request.fid != null){
                    feature_freelancer_spending.fid = request.fid;
                }
            },
            function(){
                feature_freelancer_spending.preprocessData = function(data){
                    if(feature_freelancer_spending.fid != null ){
                        data.fid = feature_freelancer_spending.fid;
                    }
                    return data;
                };
            }
        );
    }
    
    /** Form Spend Credit - Get freelancer's contact information **/
    if(typeof form_spend_credit_contact_freelancer != 'undefined' && typeof form_spend_credit_contact_freelancer_params != 'undefined'){
        var contact_freelancer_spending = new creditSpending(form_spend_credit_contact_freelancer, form_spend_credit_contact_freelancer_params);
        contact_freelancer_spending.formInitiate(
            function(request){
                if(request.fid != null){
                    contact_freelancer_spending.fid = request.fid;
                }
            },
            function(){
                contact_freelancer_spending.preprocessData = function(data){
                    if(contact_freelancer_spending.fid != null ){
                        data.fid = contact_freelancer_spending.fid;
                    }
                    return data;
                };
                contact_freelancer_spending.afterSuccess = function(result){
                    if(typeof result != 'undefined'){
                        if(typeof result.freelancer.avatar_url != 'undefined'
                                && typeof result.freelancer.name != 'undefined'
                                && typeof result.freelancer.email != 'undefined'
                                && typeof result.freelancer.tel != 'undefined'){
                            if(result.freelancer.avatar_url){
                                $(form_spend_credit_contact_freelancer + ' .screen-success .info-photo img[data-replace="avatar_url"]').attr('src', result.freelancer.avatar_url);
                            } else{
                                $(form_spend_credit_contact_freelancer + ' .screen-success .info-photo img[data-replace="avatar_url"]').attr('src', "/img/unknown.png");
                            }
                            $(form_spend_credit_contact_freelancer + ' .screen-success .info-detail span[data-replace="fullName"]').html(result.freelancer.name);
                            $(form_spend_credit_contact_freelancer + ' .screen-success .info-detail span[data-replace="email"]').html(result.freelancer.email);
                            $(form_spend_credit_contact_freelancer + ' .screen-success .info-detail span[data-replace="telephone"]').html(result.freelancer.tel);
                        } else{
                            contact_freelancer_spending.formShowError();
                        }
                    } else {
                        contact_freelancer_spending.formShowError();
                    }
                };
            }
        );
    }
    
    /** Form Spend Credit - Get freelancer's contact information **/
    if(typeof form_spend_credit_contact_client != 'undefined' && typeof form_spend_credit_contact_client_params != 'undefined'){
        var contact_client_spending = new creditSpending(form_spend_credit_contact_client, form_spend_credit_contact_client_params);
        contact_client_spending.formInitiate(
            function(request){
                if(request.cid != null){
                    contact_client_spending.cid = request.cid;
                }
            },
            function(){
                contact_client_spending.preprocessData = function(data){
                    if(contact_client_spending.cid != null ){
                        data.cid = contact_client_spending.cid;
                    }
                    return data;
                };
                contact_client_spending.afterSuccess = function(result){
                    if(typeof result != 'undefined'){
                        if(typeof result.client.avatar_url != 'undefined'
                                && typeof result.client.name != 'undefined'
                                && typeof result.client.email != 'undefined'
                                && typeof result.client.tel != 'undefined'){
                            if(result.client.avatar_url){
                                $(form_spend_credit_contact_client + ' .screen-success .info-photo img[data-replace="avatar_url"]').attr('src', result.client.avatar_url);
                            } else{
                                $(form_spend_credit_contact_client + ' .screen-success .info-photo img[data-replace="avatar_url"]').attr('src', "/img/unknown.png");
                            }
                            $(form_spend_credit_contact_client + ' .screen-success .info-detail span[data-replace="fullName"]').html(result.client.name);
                            $(form_spend_credit_contact_client + ' .screen-success .info-detail span[data-replace="email"]').html(result.client.email);
                            $(form_spend_credit_contact_client + ' .screen-success .info-detail span[data-replace="telephone"]').html(result.client.tel);
                        } else{
                            contact_client_spending.formShowError();
                        }
                    } else {
                        contact_client_spending.formShowError();
                    }
                };
            }
        );
    }
    
    function creditSpending(form_spend_name, form_spend_params){
        this.url        = "";
        this.cost       = 0;
        this.payer_id   = null;
        this.service_id = 0;
        this.balance    = 0; // User Credit balance

        this.setPayerId = function(pid){
            this.payer_id = pid;
        };
        
        this.setUrl = function(url){
            this.url = url;
        };
        
        this.setServiceId = function(sid){
            this.service_id = sid;
        };
        
        this.setBalance = function(balance){
            this.balance = balance;
        };
        
        this.formShow = function(){
        };
        
        this.formCleanUpScreen = function(){
            $(form_spend_name + " .load-spinner").hide();
            $(form_spend_name + " .screen-confirm").show();
            $(form_spend_name + " .screen-success").hide();
            $(form_spend_name + " .screen-error").hide();
        };
        
        this.formShowError = function(msg){
            if(typeof msg != 'undefined'){
                $(form_spend_name + " .screen-error").find('.error-message').html(msg);
            } else{
                $(form_spend_name + " .screen-error").find('.error-message').html("Rất tiếc, đã có lỗi xảy ra. Vui lòng liên hệ vLance để được hỗ trợ.");
            }
            $(form_spend_name + " .load-spinner").hide();
            $(form_spend_name + " .screen-confirm").hide();
            $(form_spend_name + " .screen-success").hide();
            $(form_spend_name + " .screen-error").show();
        };
        
        this.formShowSuccess = function(){
            $(form_spend_name + " .load-spinner").hide();
            $(form_spend_name + " .screen-confirm").hide();
            $(form_spend_name + " .screen-success").show();
            $(form_spend_name + " .screen-error").error();
        };
        
        this.updateCreditBalance = function(new_balance){
            this.balance = new_balance;
            $('span[data-credit="balance"]').html(new_balance);
        };
        
        this.formInitiate = function(callbackSetRequiredParamsFunction, injectParamsFunction){
            var that = this;

            // Initialising some essential attributes
            if(form_spend_params.url != null){this.setUrl(form_spend_params.url);}
            if(form_spend_params.service_id != null){this.setServiceId(form_spend_params.service_id);}
            if(form_spend_params.balance != null){this.setBalance(form_spend_params.balance);}


            // Binding on form is hidden, cleanup screen
            $(form_spend_name).on('hidden', function(){that.formCleanUpScreen();});
            // Binding confirm pay button
            $(form_spend_name + " .screen-confirm a.pay-credit-confirm").on('click', function(){that.confirmPay();});
            // Binding close button after success, refresh page
            $(form_spend_name + " .screen-success a.pay-credit-close").on('click', function(){location.reload();});
            // Binding feature-job button, set job id and payer id
            $('[data-toggle="modal"][href="' + form_spend_name + '"]').on('click', function() {
                request = $(this).data('request');
                if(request.pid != null){
                    that.setPayerId(request.pid);
                }
                callbackSetRequiredParamsFunction(request);
            });

            // Cleanup form screen
            that.formCleanUpScreen();
            
            // Initiate some functions
            injectParamsFunction();
        };
        
        this.confirmPay = function(){
            var that = this;
            $(form_spend_name + " .load-spinner").show();
            if(this.payer_id != null){
                var data = {
                    service_id: that.service_id,
                    pid:        that.payer_id,
                    path:       window.location.pathname
                };
                data = that.preprocessData(data);
                if(this.balance >= this.cost){
                    $.ajax({
                        url: that.url,
                        type: "POST",
                        data: data
                    }).done(function(result){
                        if(typeof result.error != 'undefined'){
                            if(result.error == 0){
                                that.formShowSuccess();
                                that.updateCreditBalance(result.credit_balance);
                                that.afterSuccess(result);
                            } else {
                                that.formShowError(result.errorMsg);
                            }
                        }
                    });
                } else {
                    this.formShowError("Không đủ credit.");
                }
            } else {
                this.formShowError();
            }
        };
        
        this.preprocessData = function(data){
            return data;
        };
        
        this.afterSuccess = function(data){
            return true;
        };
    };
});