$(document).ready(function(){
    if(typeof cat_url != "undefined"){
        $('#btn-category-vip').on('click', function(){
            document.getElementById('btn-category-vip').disabled = true;
            $.ajax({
                url: cat_url,
                type: "POST",
                data: {
                    category: $('#category-vip').val(),
                },
            }).done(function(result){
                if(typeof result.error !== 'undefined'){
                    if(result.error === 0){
                        $('.mini-vip-backdrop.container').remove();
                        $('#mini-vip-category').remove();
                        window.location.reload();
                    }
                }
            }).fail(function(result){
                $('.mini-vip-backdrop.container').remove();
                $('#mini-vip-category').remove();
                window.location.reload();
            }).always(function(){
                $('.mini-vip-backdrop.container').remove();
                $('#mini-vip-category').remove();
                window.location.reload();
            });
        });
    }
});