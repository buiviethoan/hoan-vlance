$(document).ready(function() {
    if($("body").hasClass("transaction_withdraw")){
        if(typeof user_balance != 'undefined' && typeof user_processing_balance != 'undefined'){
            var validate_popup_mesager = function() {
                $('#form-withdrawal').validate({
                    rules: {
                        "amount": {
                            required: true,
                            number: true
                        },
                        "userpaymentmethod": {
                            required: true
                        }
                    },
                });
            };
            $("#form-withdrawal .more-hourlies a.btn-primary").click(function(event) {
                validate_popup_mesager();
                if($("#form-withdrawal").valid()== true){
                    $(".amount label").remove();
                    var price = $("#amount").val().replace(/\./gi, "").replace(/,/g, "");
                    if (price < 50000) {
                        $(".amount").append("<label for='amount' class='error'>Lớn hơn hoặc bằng 50.000 VNĐ</label>")
                        return false;
                    }
                    if (price > 20000000) {
                        $(".amount").append("<label for='amount' class='error'>Nhỏ hơn hoặc bằng 20.000.000 VNĐ</label>")
                        return false;
                    }
                    if (validatePrice(price)) {
                        $("#amount").val(price);
                        $('#form-withdrawal .more-hourlies input.hide').click();
                        $(this).attr('disabled','disabled')
                    } else {
                        $(".amount").append("<label for='amount' class='error'>Giá tiền không hợp lệ</label>")
                        return false;
                    }

                }else{
                    return false;
                }
            });
        }
    }
});
