$(document).ready(function() {
    if($("body").hasClass("job_new") || $("body").hasClass("job_new_submit") || $("body").hasClass("job_edit") || $("body").hasClass("job_edit_submit")){
        /** Initialisation **/
        var submit = true;
        
        // Mixpanel: Tracking activity on the create job form
        $('#vlance_jobbundle_jobtype_title').blur(function(){
            if($(this).val()){
                vtrack('Typing job', {'element_id':$(this).attr('id')});
            }
        });
        $('#vlance_jobbundle_jobtype_description').blur(function(){
            if($(this).val()){
                vtrack('Typing job', {'element_id':$(this).attr('id'), 'valid': ($(this).val().length >= 200 ? "TRUE" : "FALSE")});
            }
        });
        // END Mixpanel: Tracking activity on the create job form
        
        
        // Service field: Add one more service into the list
        $("#vlance_jobbundle_jobtype_service").append('<option disabled>-----------------------</option>');
        $("#vlance_jobbundle_jobtype_service").append('<option value="last_option">Dịch vụ khác (hoặc không thấy dịch vụ phù hợp)</option>');
        $("#vlance_jobbundle_jobtype_service option[value='last_option']").prop('value', '');

        // Budget field: Trigger on typing budget
        $('#vlance_jobbundle_jobtype_budget').keyup(function(event){
            // kiem tra co phai nut qua trai phai ko.
            if((event.which == 37 || event.which == 39              // Left & Right
                    || event.which == 36 || event.which == 35 )){   // Home & End
                event.preventDefault();
            } else {
                // lay gia tri xong replace lai thanh so vs add them dau phan nghin
                $(this).val($(this).val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            }
        }).keyup();
        
        // Skill field:
        if(typeof source != "undefined"){
            $(".form-add-skill").click(function(){
                $(this).find('input').focus();
            });
            var tagApi = $(".tm-input").tagsManager({
                prefilled: (typeof prefilled_skills != "undefined"? prefilled_skills : null),
                CapitalizeFirstLetter: false,
                preventSubmitOnEnter: true,
                AjaxPush: null,
                AjaxPushAllTags: false,
                AjaxPushParameters: null,
                delimiters: [9, 13, 44],
                backspace: [8],
                blinkBGColor_1: '#FFFF9C',
                blinkBGColor_2: '#CDE69C',
                hiddenTagListName: 'hiddenTagListA',
                hiddenTagListId: null,
                deleteTagsOnBackspace: true,
                tagsContainer: null,
                tagCloseIcon: '×',
                tagClass: '',
                validator: function(e, d){
                    if(inArrayCaseInsensitive(e, source) == -1){
                        return false;
                    } else {
                        return true;
                    }
                },
                onlyTagList: true
            });

            $('.tm-input').typeahead({
                source: source,
                items: '10', 
                updater: function(item){
                    $('.tm-input').val("");
                    $('.tm-input').tagsManager("pushTag", item);
                }
            });

            $('#vlance_jobbundle_jobtype_description, #vlance_jobbundle_jobtype_title').blur(function(){
                var releventSkills = getSkills($(this).val(), source);
                for(i=0; i< releventSkills.length; i++){
                    $('.tm-input').tagsManager("pushTag", releventSkills[i]);
                }
            });
            
            function getSkills(text, skillList){
                var relevantSkills = Array();
                if(typeof text != "undefined" && typeof skillList != "undefined"){
                    for(i=0; i<skillList.length; i++){
                        var escaped = (skillList[i]).replace(/[^\w\s]/g, "\\$&");
                        if(new RegExp('\\b(' + escaped + ')\\b', 'i').test(text)){
                            relevantSkills.push(skillList[i]);
                        }
                    }
                }
                return relevantSkills;
            }

            function inArrayCaseInsensitive(needle, haystackArray){
                //Iterates over an array of items to return the index of the first item that matches the provided val ('needle') in a case-insensitive way.  Returns -1 if no match found.
                var defaultResult = -1;
                var result = defaultResult;
                $.each(haystackArray, function(index, value) { 
                    if (result == defaultResult && value.toLowerCase() == needle.toLowerCase()) {
                        result = index;
                    }
                });
                return result;
            }
        }
        
        /* Update 20160701: Phân loại service theo category */
        $('#vlance_jobbundle_jobtype_category').change(function(){
            var cat = document.getElementById('vlance_jobbundle_jobtype_category');
            var cat_id = cat.options[cat.selectedIndex].value;
            if(typeof catser != "undefined"){
                $.ajax({
                    url: catser,
                    type: "GET",
                    data: {
                        'cat_id' : cat_id,
                    },
                }).done(function(result){
                    if(result.error == 0){
                        $('#vlance_jobbundle_jobtype_service option:gt(0)').remove();
                        for(var i = 0; i < result.id.length; i++){
                            $option = $('<option value='+ result.id[i] +'>' + result.title[i] + '</option>');
                            $('#vlance_jobbundle_jobtype_service').append($option);
                        };
                    }
                }).fail(function(result){
                }).always(function(){
                });
            }
        });
        /* END */
        
        // Required Escrow field:
        // In testing case of Requiring freelancer escrowing when get job (50:50)
        //TODO: Check in Editing job case
        if($("#vlance_jobbundle_jobtype_requireEscrow").length > 0){
            var it_category = [2,3,8,9,10,11,40,42];
            var initialRequireEscrowVal = $("#vlance_jobbundle_jobtype_requireEscrow").prop("checked");

            var isValidCategory = function(){
                return it_category.indexOf(parseInt($("#vlance_jobbundle_jobtype_category").val())) !== -1;
            };

            var isValidBudget = function(){
                p = $("#vlance_jobbundle_jobtype_budget").val().replace(/\./gi, "").replace(/,/g, "");
                if(p === ''){
                    return true;
                } else{
                    return (p >= 2000000 && p <= 10000000);
                }
            };

            var showRequireEscrowOption = function(){
                $("#vlance_jobbundle_jobtype_requireEscrow").prop("checked", initialRequireEscrowVal);
                $("#vlance_jobbundle_jobtype_requireEscrow").parent().slideDown();
            };

            var hideRequireEscrowOption = function(){
                $("#vlance_jobbundle_jobtype_requireEscrow").parent().slideUp(function(){
                    initialRequireEscrowVal = $("#vlance_jobbundle_jobtype_requireEscrow").prop("checked");
                    $("#vlance_jobbundle_jobtype_requireEscrow").prop("checked", false);
                });
            };

            var toggleRequireEscrowOption = function(){
                if(isValidBudget() && isValidCategory()){
                    showRequireEscrowOption();
                } else{
                    hideRequireEscrowOption();
                }
            };

            // Initialising this option when first load
            if(initialRequireEscrowVal){
                showRequireEscrowOption();
            } else{
                toggleRequireEscrowOption();
            }

            // On category change, show/hide this option, in depending on the value selected
            $("#vlance_jobbundle_jobtype_category").change(function(){
                toggleRequireEscrowOption();
            });

            $("#vlance_jobbundle_jobtype_budget").focusout(function(){
                toggleRequireEscrowOption();
            });
        }
        
        // On validating, pre-submit
        $(".form-cread-job-btn .popup-validate").click(function() {
            // remove label loi
            $(".budget .input-append label.error_job").remove();
            
            //kiem tra neu validateinput = true thi moi bat loi hien thi popup
            if (validate_fileinput("#vlance_jobbundle_jobtype_files_file_upload_container .new_file_input") === false) {
                return false;
            }
            
            validate_popup_update_job('#edit-job-form');
            if ($('#edit-job-form').valid() === false) {
                submit = false;
                $('#edit-job-form').validate().focusInvalid();
                return false;
            } else {
                submit = true;
            }

            // Validating job's budget
            var price = $("#vlance_jobbundle_jobtype_budget").val().replace(/\./gi, "").replace(/,/g, "");
            if (price < 200000) {
                $(".budget .input-append label.error_job").remove();
                $(".budget .input-append").append("<label for='vlance_jobbundle_jobtype_budget' class='error_job' style='display: block;'>Không được dưới 200.000 VNĐ</label>")
                $("#vlance_jobbundle_jobtype_budget").focus();
                submit = false;
                return false;
            } else {
                submit = true;
            }
            if (price > 250000000) {
                $(".budget .input-append label.error_job").remove();
                $(".budget .input-append").append("<label for='vlance_jobbundle_jobtype_budget' class='error_job'>Không được quá 250.000.000 VNĐ</label>")
                $("#vlance_jobbundle_jobtype_budget").focus();
                submit = false;
                return false;
            } else {
                submit = true;
            }

            if (validatePrice(price) === false) {
                $(".budget .input-append label.error_job").remove();
                $(".budget .input-append").append("<label for='vlance_jobbundle_jobtype_budget' class='error_job'>Giá tiền không hợp lệ</label>")
                $("#vlance_jobbundle_jobtype_budget").focus();
                submit = false;
                return false;
            } else {
                submit = true;
            }

            //  reset popup
            $(".new_file_upload ul li").remove();
            $(".new_file_upload").css("display", "none");

            // lay text cho vao popup
            $(".new_title div").html($("#vlance_jobbundle_jobtype_title").val());
            $(".new_description div").html(nl2br($("#vlance_jobbundle_jobtype_description").val()));

            // Mỗi file upload lên được lấy lưu vào 1 thẻ li của popup.Nếu có file upload thỳ label display:block
            if ($(".fileupload_wrapper .new_file_input input").length > 0) {
                for (var i = 0; i < $(".new_file_input input").length; i++) {
                    // Chèn thêm thẻ li vào trong thẻ ul của popup
                    $(".new_file_upload ul").append("<li class='row-fluid upload" + i + "'><span class='upload" + i + "'></span></li>");
                    $(".new_file_upload ul li span.upload" + i + "").html($(".new_file_input input[type=file]").eq(i).val().replace(/.*(\/|\\)/, ''));
                }
                $(".new_file_upload").css("display", "block");
            }

            $(".new_budget div").html(($("#vlance_jobbundle_jobtype_budget").val()).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' VNĐ');
            $(".new_category div").html($("#vlance_jobbundle_jobtype_category option:selected").text());
            $(".new_city div").html($("#vlance_jobbundle_jobtype_city option:selected").text());
        });
        
        function submitJobForm(){
            // Removing "," and "." from job's budget
            $("#vlance_jobbundle_jobtype_budget").val($("#vlance_jobbundle_jobtype_budget").val().replace(/\./gi, "").replace(/,/g, ""));
            $('#btn-submit-job').prop('disabled', true);
            addLoadingStatus();
            $("#edit-job-form").submit();
        }
        
        function addLoadingStatus(){
            $("#edit-job-form").find('input[type="button"]').val($("#edit-job-form").find('input[type="button"]').val() + ".");
            setTimeout(addLoadingStatus, 1700);
        }
        
        /** Shared function **/
        // Validating Create Job & Edit Job pages with jQuery Validator
        var validate_popup_update_job = function(form) {
            $(form).validate({
                rules: {
                    "vlance_jobbundle_jobtype[category]": {
                        required: true
                    },
                    "vlance_jobbundle_jobtype[title]":  {
                        minlength: 2,
                        required: true
                    },
                    "vlance_jobbundle_jobtype[description]":  {
                        minlength: 200,
                        required: true,
                        contact_leak: true
                    },
                    "vlance_jobbundle_jobtype[city]": {
                        required: true
                    },
                    "vlance_jobbundle_jobtype[budget]": {
                        required:true,
                        minlength:5,
                        maxlength:11
                    },
                    "vlance_jobbundle_jobtype[closeAt]": {
                        required: true
                    }
                },
                onfocusout: false,
                onkeyup: false,
                onclick: false
            });
        };

        //validate khi client nhap gia
        function validatePrice(price) {
            var rePrice = /^(0|[1-9][0-9/.]*)$/;
            return rePrice.test(price);
        }
        
        // Execute in Create/New Job page only
        if($("body").hasClass("job_new") || $("body").hasClass("job_new_submit")){
            // In Create job page Show, sliding down service option when category is choosen
            if($("#vlance_jobbundle_jobtype_category").val() === null){ // run only in Create Job page, not in Edit Job page
                $("#vlance_jobbundle_jobtype_service").parent().hide();
                $("#vlance_jobbundle_jobtype_category").change(function(){
                    if($(this).val() !== null){
                        $("#vlance_jobbundle_jobtype_service").parent().slideDown();
                    }
                });
            }
        }
        
        //Edit job page only
        if($("body").hasClass("job_edit") || $("body").hasClass("job_edit_submit")){
            $("#edit-job-form").submit(function(event) {
                // Removing "," and "." from job's budget
                $("#vlance_jobbundle_jobtype_budget").val($("#vlance_jobbundle_jobtype_budget").val().replace(/\./gi, "").replace(/,/g, ""));

                $(this).find('input[type="submit"]').prop('disabled', true);
                addLoadingStatus();
            });
        }
        
        if(typeof logged != "undefined"){
            if(logged == "true"){
                $('#btn-submit-job').click(function(){
                    if(submit === true){
                        submitJobForm();
                    }
                });
            } else {
                /** START Block register and login **/
                setForm();
                
                /** Reg form in post job **/
                if(typeof reg_form_in_job != 'undefined' && typeof reg_form_in_job_params != 'undefined'){
                    var reg_in_job = new registerInJob(reg_form_in_job, reg_form_in_job_params);
                    reg_in_job.formInitiate(
                            function(){
                                $('#reg_name,#reg_email,#reg_pass,#reg_re_pass').keypress(function(event){
                                    reg_in_job.formSubmitOnEnter(event);
                                });
                            }
                            ,function(){
                                $('#btn-submit-job').click(function(event){
                                    if(reg_in_job.getIsActive()){
                                        reg_in_job.formSubmitWithJob();
                                    }
                                });
                            }
                    );
                }

                /** Login form in post job **/
                if(typeof login_form_in_job != 'undefined' && typeof login_form_in_job_params != 'undefined'){
                    var login_in_job = new loginInJob(login_form_in_job, login_form_in_job_params);
                    login_in_job.formInitiate(
                        function(){
                            $('#login_email,#login_pass').keypress(function(event){
                                login_in_job.formSubmitOnEnter(event);
                            });
                        }
                        ,function(){
                            $('#btn-submit-job').click(function(event){
                                if(login_in_job.getIsActive()){
                                    login_in_job.formSubmitWithJob();
                                }
                            });
                        }
                    );
                }

                function registerInJob(reg_form_name, reg_form_params){
                    this.url = "";
                    this.email = "";
                    this.fullname = "";
                    this.password = null;

                    this.getIsActive = function(){
                        return !($(reg_form_name).parent().hasClass('deactive'));
                    };
                    this.setUrl = function(u){
                        this.url = u;
                    };
                    this.setEmail = function(e){
                        this.email = e;
                    };
                    this.setFullname = function(n){
                        this.fullname = n;
                    };
                    this.setPassword = function(p){
                        this.password = p;
                    };

                    this.formSubmitOnEnter = function(event){
                        if(event.which == 13){
                            this.formSubmit();
                        }
                    };

                    this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                        if(reg_form_params.url != null){this.setUrl(reg_form_params.url);}

                        this.formRules();
                        this.decodeDataLoadCookie();
                        callbackWhenPressEnter();
                        callbackWhenSubmitJobForm();
                    };

                    this.formSubmit = function(){
                        that = this;
                        
                        if ($(reg_form_name).valid() === false) {
                            $(reg_form_name).validate().focusInvalid();
                            return false;
                        }

                        that.encodeDataSaveCookie();
                        that.setFullname(document.getElementById('reg_name').value);
                        that.setEmail(document.getElementById('reg_email').value);
                        that.setPassword(document.getElementById('reg_pass').value);

                        var data = {
                            email: that.email,
                            fullname: that.fullname,
                            password: that.password,
                            user: 'client',
                        };
                        $.ajax({
                            url: that.url,
                            type: "POST",
                            data: data,
                        }).done(function(result){
                            if(typeof result.error != 'undefined'){
                                if(result.error == 0){
                                    $('.block-lr').hide();
                                    $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng ký thành công</div></div></div></div>').insertBefore('.container');
                                    location.reload();
                                } else {
                                    that.formRegFailed();
                                }
                            }
                        });
                    };
                    
                    this.formSubmitWithJob = function(){
                        that = this;
                        
                        if ($(reg_form_name).valid() === false) {
                            $(reg_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        if(submit === true){
                            that.setFullname(document.getElementById('reg_name').value);
                            that.setEmail(document.getElementById('reg_email').value);
                            that.setPassword(document.getElementById('reg_pass').value);

                            var data = {
                                email: that.email,
                                fullname: that.fullname,
                                password: that.password,
                                user: 'client',
                            };
                            $.ajax({
                                url: that.url,
                                type: "POST",
                                data: data,
                            }).done(function(result){
                                if(typeof result.error != 'undefined'){
                                    if(result.error == 0){
                                        $('.block-lr').hide();
                                        submitJobForm();
                                    } else {
                                        that.formRegFailed();
                                    }
                                }
                            });
                        }
                    };

                    this.formRules = function(){
                        $(reg_form_name).validate({
                            rules: {
                                "reg_name": {
                                    required: true,
                                },
                                "reg_email": {
                                    required: true,
                                },
                                "reg_pass": {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32,
                                },
                                "reg_re_pass": {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32,
                                    equalTo : "#reg_pass",
                                },
                                focusInvalid: false,
                            }
                        });
                    };

                    this.formRegFailed = function(){
                        $(reg_form_name + ' .row-fluid .input-email label.error').remove();
                        $(reg_form_name + ' .row-fluid .input-email').append("<label for='reg_email' style='display: block !important;' class='error row-fluid'><b>Email đã được đăng kí</b></label>");
                        $('#reg_email').focus();
                        return false;
                    };

                    this.encodeDataSaveCookie = function(){
                        var obj = new Object();
                        obj.category = $('#vlance_jobbundle_jobtype_category').val();
                        obj.title = $('#vlance_jobbundle_jobtype_title').val();
                        obj.description = $('#vlance_jobbundle_jobtype_description').val();
                        obj.skill = $('input:hidden[name="hiddenTagListA"]').val();
                        obj.city = $('#vlance_jobbundle_jobtype_city').val();
                        obj.budget = $('#vlance_jobbundle_jobtype_budget').val();
                        obj.time = $('#vlance_jobbundle_jobtype_closeAt').val();

                        var jsonString = JSON.stringify(obj);
                        var encode = unescape(encodeURIComponent(jsonString));
                        var encodeData = btoa(encode);

                        $.cookie("form_job", encodeData, {expires:0.5, path:'/'});
                    };

                    this.decodeDataLoadCookie = function(){
                        that = this;
                        var stringData = $.cookie("form_job");
                        if(typeof stringData != "undefined"){
                            var dataForm = atob(stringData);
                            var decode = decodeURIComponent(escape(dataForm));
                            var parsed = JSON.parse(decode);

                            //set value
                            if(parsed.category != null){
                                document.getElementById('vlance_jobbundle_jobtype_category').value = parsed.category;
                            }
                            if(parsed.title != null){
                                document.getElementById('vlance_jobbundle_jobtype_title').value = parsed.title;
                            }
                            if(parsed.description != null){
                                document.getElementById('vlance_jobbundle_jobtype_description').value = parsed.description;
                            }
                            if(parsed.city != null){
                                document.getElementById('vlance_jobbundle_jobtype_city').value = parsed.city;
                            }
                            if(parsed.budget != null){
                                document.getElementById('vlance_jobbundle_jobtype_budget').value = parsed.budget;
                            }
                            if(parsed.time != null){
                                document.getElementById('vlance_jobbundle_jobtype_closeAt').value = parsed.time;
                            }

                            //set skill
                            if(parsed.skill != null){
                                var arr_skill = parsed.skill.split(',');
                                for(i=0; i< arr_skill.length; i++){
                                    $('.tm-input').tagsManager("pushTag", arr_skill[i]);
                                }
                            }
                            $.removeCookie("form_job", {path:'/'});
                        }
                    };
                    /** END cookie */
                }

                function loginInJob(login_form_name, login_form_params){
                    this.url = "";
                    this.email = "";
                    this.password = null;
                    
                    this.getIsActive = function(){
                        return !($(login_form_name).parent().hasClass('deactive'));
                    };
                    this.setUrl = function(u){
                        this.url = u;
                    };
                    this.setEmail = function(e){
                        this.email = e;
                    };
                    this.setPassword = function(p){
                        this.password = p;
                    };

                    this.formLoginFailed = function(){
                        $(login_form_name + ' .row-fluid label.error').remove();
                        $(login_form_name + ' .row-fluid').append("<label style='display: block !important;' class='error row-fluid'><b>Email hoặc mật khẩu không đúng</b></label>");
                        $('#login_email').focus();
                        return false;
                    };

                    this.formSubmitOnEnter = function(event){
                        if(event.which == 13){
                            this.formSubmit();
                        }
                    };

                    this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                        if(login_form_params.url != null){this.setUrl(login_form_params.url);}
                        this.formRules();
                        this.removeLaberError();
                        callbackWhenPressEnter();
                        callbackWhenSubmitJobForm();
                    };
                    
                    this.formRules = function(){
                        $(login_form_name).validate({
                            rules: {
                                "login_email": {
                                    required: true,
                                },
                                "login_pass": {
                                    required: true,
                                },
                                focusInvalid: false,
                            }
                        });
                    }
                    
                    this.formSubmit = function(){
                        that = this;
                        
                        if ($(login_form_name).valid() === false) {
                            $(login_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        this.setEmail(document.getElementById('login_email').value);
                        this.setPassword(document.getElementById('login_pass').value);

                        var data = {
                            email: that.email,
                            password: that.password,
                        };
                        $.ajax({
                            url: that.url,
                            type: "POST",
                            data: data,
                        }).done(function(result){
                            if(typeof result.error != 'undefined'){
                                if(result.error == 0){
                                    $('.block-lr').hide();
                                    $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng nhập thành công</div></div></div></div>').insertBefore('.container');
                                    location.reload();
                                } else {
                                    that.formLoginFailed();
                                }
                            }
                        });
                    };
                    
                    this.formSubmitWithJob = function(){
                        that = this;
                        if ($(login_form_name).valid() === false) {
                            $(login_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        if(submit === true){
                            this.setEmail(document.getElementById('login_email').value);
                            this.setPassword(document.getElementById('login_pass').value);

                            var data = {
                                email: that.email,
                                password: that.password,
                            };
                            $.ajax({
                                url: that.url,
                                type: "POST",
                                data: data,
                            }).done(function(result){
                                if(typeof result.error != 'undefined'){
                                    if(result.error == 0){
                                        $('.block-lr').hide();
                                        submitJobForm();
                                    } else {
                                        that.formLoginFailed();
                                    }
                                }
                            });
                        }
                    };

                    this.removeLaberError = function(){
                        $('#login_email').click(function(){
                            $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                        });
                        $('#login_pass').click(function(){
                            $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                        });
                    };
                }
                /** END Block register and login **/
            }
        }
    }
});

function setForm(){
    $('#new_user').click(function(){
        $('.block-form-reg.deactive').removeClass("deactive");
        $('.block-form-log').addClass("deactive");
        $('#old_user').parent().removeClass('checked');
        $('#new_user').parent().addClass('checked');
    });
    $('#old_user').click(function(){
        $('.block-form-log.deactive').removeClass("deactive");
        $('.block-form-reg').addClass("deactive");
        $('#new_user').parent().removeClass('checked');
        $('#old_user').parent().addClass('checked');
    });
}