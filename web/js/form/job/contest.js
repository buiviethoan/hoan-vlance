/* Config Lightbox */
//lightbox.option({
//  'resizeDuration':                 100,
//  'wrapAround':                     true,
//  'alwaysShowNavOnTouchDevices':    true,
//  'positionFromTop':                0
//});
    
$(document).ready(function(){
    $('#contest-form .popovers-input').popover();
    
    setForm();
    chosePackage();
    
    $('#contest-form').validate({
        rules: {
            "vlance_jobbundle_jobcontesttype[category]": {
                required: true,
            },
            "vlance_jobbundle_jobcontesttype[title]":  {
                minlength: 2,
                required: true,
            },
            "vlance_jobbundle_jobcontesttype[description]":  {
                minlength: 200,
                required: true,
                contact_leak: true,
            },
        }
    });
    
    /* Update 20161012: Giá tiền theo từng dịch vụ */
    $('#vlance_jobbundle_jobcontesttype_service').change(function(){
        $('#block-info-price').slideUp();
        var ser = document.getElementById('vlance_jobbundle_jobcontesttype_service');
        var ser_id = ser.options[ser.selectedIndex].value;
        if(typeof serPrice != "undefined"){
            $.ajax({
                url: serPrice,
                type: "GET",
                data: {
                    'ser_id' : ser_id,
                },
            }).done(function(result){
                if(result.error == 0){
                    $('#choice-one').find('#vlance_jobbundle_jobcontesttype_budget').val(result.price1);
                    $('#choice-two').find('#vlance_jobbundle_jobcontesttype_budget').val(result.price2);
                    $('#choice-three').find('#vlance_jobbundle_jobcontesttype_budget').val(result.price3);
                    
                    document.getElementById('price_one').innerHTML = '' + result.price1.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' <span>VNĐ</span>';
                    document.getElementById('price_two').innerHTML = '' + result.price2.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' <span>VNĐ</span>';
                    document.getElementById('price_three').innerHTML = '' + result.price3.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' <span>VNĐ</span>';
                    $('#block-info-price').slideDown();
                }
            }).fail(function(result){
            }).always(function(){
                $('#block-info-price').slideDown();
            });
        }
    });
    
    function submitContestForm(){
        $('#btn-submit-contest').prop('disabled', true);
        //addLoadingStatus();
        $("#contest-form").submit();
    }
        
    /* Register and login */
    if(typeof logged != "undefined" && logged == "false"){
        if(typeof reg_form_in_job != 'undefined' && typeof reg_form_in_job_params != 'undefined'){
            var reg_in_job = new registerInJob(reg_form_in_job, reg_form_in_job_params);
            reg_in_job.formInitiate(
                    function(){
                        $('#reg_name,#reg_email,#reg_pass,#reg_re_pass').keypress(function(event){
                            reg_in_job.formSubmitOnEnter(event);
                        });
                    }
                    ,function(){
                        $('#btn-submit-contest').click(function(event){
                            if(reg_in_job.getIsActive()){
                                reg_in_job.formSubmitWithJob();
                            }
                        });
                    }
            );
        }

        if(typeof login_form_in_job != 'undefined' && typeof login_form_in_job_params != 'undefined'){
            var login_in_job = new loginInJob(login_form_in_job, login_form_in_job_params);
            login_in_job.formInitiate(
                function(){
                    $('#login_email,#login_pass').keypress(function(event){
                        login_in_job.formSubmitOnEnter(event);
                    });
                }
                ,function(){
                    $('#btn-submit-contest').click(function(event){
                        if(login_in_job.getIsActive()){
                            login_in_job.formSubmitWithJob();
                        }
                    });
                }
            );
        }

        function registerInJob(reg_form_name, reg_form_params){
            this.url = "";
            this.email = "";
            this.fullname = "";
            this.password = null;

            this.getIsActive = function(){
                return !($(reg_form_name).parent().hasClass('deactive'));
            };
            this.setUrl = function(u){
                this.url = u;
            };
            this.setEmail = function(e){
                this.email = e;
            };
            this.setFullname = function(n){
                this.fullname = n;
            };
            this.setPassword = function(p){
                this.password = p;
            };

            this.formSubmitOnEnter = function(event){
                if(event.which == 13){
                    this.formSubmit();
                }
            };

            this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                if(reg_form_params.url != null){this.setUrl(reg_form_params.url);}

                this.formRules();
                this.decodeDataLoadCookie();
                callbackWhenPressEnter();
                callbackWhenSubmitJobForm();
            };

            this.formSubmit = function(){
                that = this;

                if ($(reg_form_name).valid() === false) {
                    $(reg_form_name).validate().focusInvalid();
                    return false;
                }

                that.encodeDataSaveCookie();
                that.setFullname(document.getElementById('reg_name').value);
                that.setEmail(document.getElementById('reg_email').value);
                that.setPassword(document.getElementById('reg_pass').value);

                var data = {
                    email: that.email,
                    fullname: that.fullname,
                    password: that.password,
                    user: 'client',
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.block-lr').hide();
                            $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng ký thành công</div></div></div></div>').insertBefore('.container');
                            location.reload();
                        } else {
                            that.formRegFailed();
                        }
                    }
                });
            };

            this.formSubmitWithJob = function(){
                that = this;

                if ($(reg_form_name).valid() === false) {
                    $(reg_form_name).validate().focusInvalid();
                    return false;
                }
                
                if($("#contest-form").valid() === false){
                    $("#contest-form").validate().focusInvalid();
                    return false;
                }
                
                that.setFullname(document.getElementById('reg_name').value);
                that.setEmail(document.getElementById('reg_email').value);
                that.setPassword(document.getElementById('reg_pass').value);

                var data = {
                    email: that.email,
                    fullname: that.fullname,
                    password: that.password,
                    user: 'client',
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.block-lr').hide();
                            submitContestForm();
                        } else {
                            that.formRegFailed();
                        }
                    }
                });
            };

            this.formRules = function(){
                $(reg_form_name).validate({
                    rules: {
                        "reg_name": {
                            required: true,
                        },
                        "reg_email": {
                            required: true,
                        },
                        "reg_pass": {
                            required: true,
                            minlength: 6,
                            maxlength: 32,
                        },
                        "reg_re_pass": {
                            required: true,
                            minlength: 6,
                            maxlength: 32,
                            equalTo : "#reg_pass",
                        },
                        focusInvalid: false,
                    }
                });
            };

            this.formRegFailed = function(){
                $(reg_form_name + ' .row-fluid .input-email label.error').remove();
                $(reg_form_name + ' .row-fluid .input-email').append("<label for='reg_email' style='display: block !important;' class='error row-fluid'><b>Email đã được đăng kí</b></label>");
                $('#reg_email').focus();
                return false;
            };

            this.encodeDataSaveCookie = function(){
                var obj = new Object();
                obj.category = $('#vlance_jobbundle_jobcontesttype_category').val();
                obj.service = $('#vlance_jobbundle_jobcontesttype_service').val();
                obj.title = $('#vlance_jobbundle_jobcontesttype_title').val();
                obj.description = $('#vlance_jobbundle_jobcontesttype_description').val();

                var jsonString = JSON.stringify(obj);
                var encode = unescape(encodeURIComponent(jsonString));
                var encodeData = btoa(encode);

                $.cookie("form_contest", encodeData, {expires:0.5, path:'/'});
            };

            this.decodeDataLoadCookie = function(){
                that = this;
                var stringData = $.cookie("form_contest");
                if(typeof stringData != "undefined"){
                    var dataForm = atob(stringData);
                    var decode = decodeURIComponent(escape(dataForm));
                    var parsed = JSON.parse(decode);

                    //set value
                    if(parsed.category != null){
                        document.getElementById('vlance_jobbundle_jobcontesttype_category').value = parsed.category;
                    }
                    if(parsed.service != null){
                        document.getElementById('vlance_jobbundle_jobcontesttype_service').value = parsed.service;
                    }
                    if(parsed.title != null){
                        document.getElementById('vlance_jobbundle_jobcontesttype_title').value = parsed.title;
                    }
                    if(parsed.description != null){
                        document.getElementById('vlance_jobbundle_jobcontesttype_description').value = parsed.description;
                    }
                    $.removeCookie("form_contest", {path:'/'});
                }
            };
            /** END cookie */
        }

        function loginInJob(login_form_name, login_form_params){
            this.url = "";
            this.email = "";
            this.password = null;

            this.getIsActive = function(){
                return !($(login_form_name).parent().hasClass('deactive'));
            };
            this.setUrl = function(u){
                this.url = u;
            };
            this.setEmail = function(e){
                this.email = e;
            };
            this.setPassword = function(p){
                this.password = p;
            };

            this.formLoginFailed = function(){
                $(login_form_name + ' .row-fluid label.error').remove();
                $(login_form_name + ' .row-fluid').append("<label style='display: block !important;' class='error row-fluid'><b>Email hoặc mật khẩu không đúng</b></label>");
                $('#login_email').focus();
                return false;
            };

            this.formSubmitOnEnter = function(event){
                if(event.which == 13){
                    this.formSubmit();
                }
            };

            this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                if(login_form_params.url != null){this.setUrl(login_form_params.url);}
                this.formRules();
                this.removeLaberError();
                callbackWhenPressEnter();
                callbackWhenSubmitJobForm();
            };

            this.formRules = function(){
                $(login_form_name).validate({
                    rules: {
                        "login_email": {
                            required: true,
                        },
                        "login_pass": {
                            required: true,
                        },
                        focusInvalid: false,
                    }
                });
            }

            this.formSubmit = function(){
                that = this;

                if ($(login_form_name).valid() === false) {
                    $(login_form_name).validate().focusInvalid();
                    return false;
                }

                this.setEmail(document.getElementById('login_email').value);
                this.setPassword(document.getElementById('login_pass').value);

                var data = {
                    email: that.email,
                    password: that.password,
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.block-lr').hide();
                            $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng nhập thành công</div></div></div></div>').insertBefore('.container');
                            location.reload();
                        } else {
                            that.formLoginFailed();
                        }
                    }
                });
            };

            this.formSubmitWithJob = function(){
                that = this;
                if ($(login_form_name).valid() === false) {
                    $(login_form_name).validate().focusInvalid();
                    return false;
                }
                
                if($("#contest-form").valid() === false){
                    $("#contest-form").validate().focusInvalid();
                    return false;
                }
                
                this.setEmail(document.getElementById('login_email').value);
                this.setPassword(document.getElementById('login_pass').value);

                var data = {
                    email: that.email,
                    password: that.password,
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.block-lr').hide();
                            submitContestForm();
                        } else {
                            that.formLoginFailed();
                        }
                    }
                });

            };

            this.removeLaberError = function(){
                $('#login_email').click(function(){
                    $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                });
                $('#login_pass').click(function(){
                    $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                });
            };
        }
    } else {
        $('#btn-submit-contest').click(function(event){
            if($("#contest-form").valid()){
                submitContestForm();
            } else {
                $("#contest-form").validate().focusInvalid();
                return false;
            }
        });
    }
    /* END */
    
});

function chosePackage(){
    $('#choice-one').click(function(){
        $('#choice-one').parent().addClass('checked');
        $('#choice-two').parent().removeClass('checked');
        $('#choice-three').parent().removeClass('checked');
        $('#num_package').val(1);
    });
    $('#choice-two').click(function(){
        $('#choice-one').parent().removeClass('checked');
        $('#choice-two').parent().addClass('checked');
        $('#choice-three').parent().removeClass('checked');
        $('#num_package').val(2);
    });
    $('#choice-three').click(function(){
        $('#choice-one').parent().removeClass('checked');
        $('#choice-two').parent().removeClass('checked');
        $('#choice-three').parent().addClass('checked');
        $('#num_package').val(3);
    });
}

function addLoadingStatus(){
    $("#contest-form").find('input[type="submit"]').val($("#contest-form").find('input[type="submit"]').val() + ".");
    setTimeout(addLoadingStatus, 1700);
}

function setForm(){
    $('#new_user').click(function(){
        $('.block-form-reg.deactive').removeClass("deactive");
        $('.block-form-log').addClass("deactive");
        $('#old_user').parent().removeClass('checked');
        $('#new_user').parent().addClass('checked');
    });
    $('#old_user').click(function(){
        $('.block-form-log.deactive').removeClass("deactive");
        $('.block-form-reg').addClass("deactive");
        $('#new_user').parent().removeClass('checked');
        $('#old_user').parent().addClass('checked');
    });
}
