$(document).ready(function(){
    $('#input-telephone').on('keypress input', function(event){
        $('#form-admin-post-job label.error').remove();
        return event.charCode >= 48 && event.charCode <= 57;
    });

    $('#submit-form').on('click', function(){
        var tele = $('#input-telephone').val();
        if(tele != ''){
            if(!isNaN(tele)){
                $('#popup-credit-admin-post').modal('show');
            }
        } else {
            $('#form-admin-post-job label.error').remove();
            $("<label for='input-telephone' style='display: block !important;' class='error'><b>Nhập số điện thoại</b></label>").insertAfter('#input-telephone');
        }
    });

    $('#confirm-request').on('click', function(){
        var tel = document.getElementById('input-telephone').value;
        $('.popup-credit-bid').html('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4>Trợ giúp đăng dự án</h4></div><div class="popup-body-credit-bid"><p>Yêu cầu của bạn đang được xử lý</p></div>')

        $.ajax({
            url: url,
            type: "POST",
            data: { telephone : tel }
        }).done(function(result){
            if(result.error == 0){
                $('#popup-credit-admin-post').modal('hide');
                $('#form-admin-post-job').html('<span style="font-size: 20px;"><b>Bạn đã gửi yêu cầu thành công. <br/> vLance sẽ liên hệ với bạn sớm nhất.</b></span>');
            } else {
                $('<div id="messages"><div class="body-messages"><div class="flash-error alert alert-error"><a class="close" data-dismiss="alert">×</a><div>Đã có lỗi xảy ra. Xin vui lòng thử lại.</div></div></div></div>').insertBefore('.block-admin-post-job');
            }
        }).fail(function(result){
        }).always(function(){
        });
    });
});