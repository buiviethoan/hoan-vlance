var arr_file_upload = ['image/jpeg', 'image/png'];

$(document).ready(function() {
    if($("body").hasClass("job_onsite_view")){
        $('#btn-confirm').click(function(){
            var values;
            $("input[name='onsite_option[]']:checked").each(function ()
            {
                values = $("input[name='onsite_option[]']:checked").map(function(){
                    return $(this).val();
                }).get();
            });
            
            if(typeof payOption !== "undefined" && typeof jobId !== "undefined"){
                $.ajax({
                    url: payOption,
                    type: "POST",
                    data: {
                        'job_id' : jobId,
                        'option' : values
                    }
                }).done(function(result){
                    if(result.error === 0){
                        $('#creditOnsite').modal('hide');
                        $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đã thanh toán thành công.</div></div></div></div>').insertBefore('.job-onsite-content');
                        location.reload();
                    } else if(result.error === 105){
                        $('#creditOnsite').modal('hide');
                        $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-error"><a class="close" data-dismiss="alert">×</a><div>Bạn không đủ Credit.</div></div></div></div>').insertBefore('.job-onsite-content');
                    } else {
                        $('#creditOnsite').modal('hide');
                        $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-error"><a class="close" data-dismiss="alert">×</a><div>Đã có lỗi xảy ra. Vui lòng thử lại.</div></div></div></div>').insertBefore('.job-onsite-content');
                    }
                }).fail(function(result){
                    $('#creditOnsite').modal('hide');
                    $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-error"><a class="close" data-dismiss="alert">×</a><div>Đã có lỗi xảy ra. Vui lòng thử lại.</div></div></div></div>').insertBefore('.job-onsite-content');
                }).always(function(){
                });
            }
        });
    }
    
    if($("body").hasClass("job_onsite_new") || $("body").hasClass("job_onsite_submit") || $("body").hasClass("job_onsite_edit") || $("body").hasClass("job_onsite_edit_submit")){
        var submit = true;
        
        if($("body").hasClass("job_onsite_new") || $("body").hasClass("job_onsite_submit")){
            $("#edit-job-form").validate({
                rules: {
                    "vlance_jobbundle_jobonsitetype[budget]": {
                        "minBudget": true,
                        "maxBudget": true,
                        "minmax": true,
                    },
                    "vlance_jobbundle_jobonsitetype[budgetMax]": {
                        "minBudget": true,
                        "maxBudget": true,
                        "maxmin":    true,
                    },   
                    "vlance_jobbundle_jobonsitetype[file]": {
                        "filetype": true,
                        "filesize": true,
                        "required": true,
                    }
                }
            });
        }
        
        if($("body").hasClass("job_onsite_edit") || $("body").hasClass("job_onsite_edit_submit")){
            $("#edit-job-form").validate({
                rules: {
                    "vlance_jobbundle_jobonsitetype[budget]": {
                        "minBudget": true,
                        "maxBudget": true,
                        "minmax": true,
                    },
                    "vlance_jobbundle_jobonsitetype[budgetMax]": {
                        "minBudget": true,
                        "maxBudget": true,
                        "maxmin":    true,
                    },   
                    "vlance_jobbundle_jobonsitetype[file]": {
                        "required": false,
                        "filetype": true,
                        "filesize": true,
                    }
                }
            });
        }
        
        /*** VALIDATE ***/
        jQuery.validator.addMethod("minBudget", function(val, element){
            var value = element.value.replace(/\./gi, "").replace(/,/g, "");
            if(value < 3000000){
                return false;
            } else {
                return true;
            }
        }, "Ngân sách tối thiểu là 3.000.000 VNĐ");
        
        jQuery.validator.addMethod("maxBudget", function(val, element){
            var value = element.value.replace(/\./gi, "").replace(/,/g, "");
            if(value > 100000000){
                return false;
            } else {
                return true;
            }
        }, "Ngân sách tối đa là 100.000.000 VNĐ");
        
        jQuery.validator.addMethod("maxmin", function(val, element){
            var valueMax = element.value.replace(/\./gi, "").replace(/,/g, "");
            var valueMin = document.getElementById('vlance_jobbundle_jobonsitetype_budget').value.replace(/\./gi, "").replace(/,/g, "");
            if(parseInt(valueMax) < parseInt(valueMin)){
                return false;
            } else {
                return true;
            }
        }, "Ngân sách phải cao hơn mức tối thiểu");
        
        jQuery.validator.addMethod("minmax", function(val, element){
            var valueMin = element.value.replace(/\./gi, "").replace(/,/g, "");
            var valueMax = document.getElementById('vlance_jobbundle_jobonsitetype_budgetMax').value.replace(/\./gi, "").replace(/,/g, "");
            if(parseInt(valueMax) < parseInt(valueMin)){
                return false;
            } else {
                return true;
            }
        }, "Ngân sách phải cao hơn mức tối thiểu");
        
        jQuery.validator.addMethod("filetype", function(val, element){
            if(element.files[0]){
                var type = element.files[0].type;
                if($.inArray(type, arr_file_upload) === -1){
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }, "File phải đúng định dạng jpg, png");
        
        jQuery.validator.addMethod("filesize", function (val, element) {
            if(element.files[0]){
                var size = element.files[0].size;
                if (size > 5120000)// checks the file more than 5 MB
                {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;  
            }
        }, "Dung lượng file ảnh phải nhỏ hơn 5MB");
        
        /*** END VALIDATE ***/
        
        // Budget field: Trigger on typing budget
        $('#vlance_jobbundle_jobonsitetype_budget').keyup(function(event){
            // kiem tra co phai nut qua trai phai ko.
            if((event.which === 37 || event.which === 39              // Left & Right
                    || event.which === 36 || event.which === 35 )){   // Home & End
                event.preventDefault();
            } else {
                // lay gia tri xong replace lai thanh so vs add them dau phan nghin
                $(this).val($(this).val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            }
        }).keyup();
        
        // Budget field: Trigger on typing budget max
        $('#vlance_jobbundle_jobonsitetype_budgetMax').keyup(function(event){
            // kiem tra co phai nut qua trai phai ko.
            if((event.which === 37 || event.which === 39              // Left & Right
                    || event.which === 36 || event.which === 35 )){   // Home & End
                event.preventDefault();
            } else {
                // lay gia tri xong replace lai thanh so vs add them dau phan nghin
                $(this).val($(this).val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            }
        }).keyup();
        
        // Skill field:
        if(typeof source !== "undefined"){
            $(".form-add-skill").click(function(){
                $(this).find('input').focus();
            });
            var tagApi = $(".tm-input").tagsManager({
                prefilled: (typeof prefilled_skills !== "undefined"? prefilled_skills : null),
                CapitalizeFirstLetter: false,
                preventSubmitOnEnter: true,
                AjaxPush: null,
                AjaxPushAllTags: false,
                AjaxPushParameters: null,
                delimiters: [9, 13, 44],
                backspace: [8],
                blinkBGColor_1: '#FFFF9C',
                blinkBGColor_2: '#CDE69C',
                hiddenTagListName: 'hiddenTagListA',
                hiddenTagListId: null,
                deleteTagsOnBackspace: true,
                tagsContainer: null,
                tagCloseIcon: '×',
                tagClass: '',
                validator: function(e, d){
                    if(inArrayCaseInsensitive(e, source) === -1){
                        return false;
                    } else {
                        return true;
                    }
                },
                onlyTagList: true
            });

            $('.tm-input').typeahead({
                source: source,
                items: '10', 
                updater: function(item){
                    $('.tm-input').val("");
                    $('.tm-input').tagsManager("pushTag", item);
                }
            });
        }
        
        $('#vlance_jobbundle_jobonsitetype_description, #vlance_jobbundle_jobonsitetype_title').blur(function(){
            var releventSkills = getSkills($(this).val(), source);
            for(i=0; i< releventSkills.length; i++){
                $('.tm-input').tagsManager("pushTag", releventSkills[i]);
            }
        });
        
        function getSkills(text, skillList){
            var relevantSkills = Array();
            if(typeof text != "undefined" && typeof skillList != "undefined"){
                for(i=0; i<skillList.length; i++){
                    var escaped = (skillList[i]).replace(/[^\w\s]/g, "\\$&");
                    if(new RegExp('\\b(' + escaped + ')\\b', 'i').test(text)){
                        relevantSkills.push(skillList[i]);
                    }
                }
            }
            return relevantSkills;
        }

        function inArrayCaseInsensitive(needle, haystackArray){
            //Iterates over an array of items to return the index of the first item that matches the provided val ('needle') in a case-insensitive way.  Returns -1 if no match found.
            var defaultResult = -1;
            var result = defaultResult;
            $.each(haystackArray, function(index, value) { 
                if (result == defaultResult && value.toLowerCase() == needle.toLowerCase()) {
                    result = index;
                }
            });
            return result;
        }
        
        $(".form-cread-job-btn .popup-validate").click(function() {
            // remove label loi
            $(".budget .input-append label.error_job").remove();
            
            //kiem tra neu validateinput = true thi moi bat loi hien thi popup
            if (validate_fileinput("#vlance_jobbundle_jobonsitetype_files_file_upload_container .new_file_input") === false) {
                return false;
            }

            if ($('#edit-job-form').valid() === false) {
                submit = false;
                $('#edit-job-form').validate().focusInvalid();
                return false;
            } else {
                submit = true;
            }

            // Validating job's budget
            var priceMin = $("#vlance_jobbundle_jobonsitetype_budget").val().replace(/\./gi, "").replace(/,/g, "");
            
            if (validatePrice(priceMin) === false) {
                $(".budget .input-append label.error_job").remove();
                $(".budget .input-append").append("<label for='vlance_jobbundle_jobonsitetype_budget' class='error_job'>Giá tiền không hợp lệ</label>")
                $("#vlance_jobbundle_jobonsitetype_budget").focus();
                submit = false;
                return false;
            } else {
                submit = true;
            }
            
            var priceMax = $("#vlance_jobbundle_jobonsitetype_budgetMax").val().replace(/\./gi, "").replace(/,/g, "");
            
            if (validatePrice(priceMax) === false) {
                $(".budget .input-append label.error_job").remove();
                $(".budget .input-append").append("<label for='vlance_jobbundle_jobonsitetype_budgetMax' class='error_job'>Giá tiền không hợp lệ</label>")
                $("#vlance_jobbundle_jobonsitetype_budgetMax").focus();
                submit = false;
                return false;
            } else {
                submit = true;
            }

            //  reset popup
            $(".new_file_upload ul li").remove();
            $(".new_file_upload").css("display", "none");

            // lay text cho vao popup
            $(".new_title div").html($("#vlance_jobbundle_jobonsitetype_title").val());
            $(".new_description div").html(nl2br($("#vlance_jobbundle_jobonsitetype_description").val()));

            // Mỗi file upload lên được lấy lưu vào 1 thẻ li của popup.Nếu có file upload thỳ label display:block
            if ($(".fileupload_wrapper .new_file_input input").length > 0) {
                for (var i = 0; i < $(".new_file_input input").length; i++) {
                    // Chèn thêm thẻ li vào trong thẻ ul của popup
                    $(".new_file_upload ul").append("<li class='row-fluid upload" + i + "'><span class='upload" + i + "'></span></li>");
                    $(".new_file_upload ul li span.upload" + i + "").html($(".new_file_input input[type=file]").eq(i).val().replace(/.*(\/|\\)/, ''));
                }
                $(".new_file_upload").css("display", "block");
            }

            $(".new_budget div").html(($("#vlance_jobbundle_jobonsitetype_budget").val()).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' VNĐ');
            $(".new_budgetMax div").html(($("#vlance_jobbundle_jobonsitetype_budgetMax").val()).toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ' VNĐ');
            $(".new_category div").html($("#vlance_jobbundle_jobonsitetype_category option:selected").text());
            $(".new_city div").html($("#vlance_jobbundle_jobonsitetype_city option:selected").text());
        });
        
        function submitJobForm(){
            // Removing "," and "." from job's budget
            $("#vlance_jobbundle_jobonsitetype_budget").val($("#vlance_jobbundle_jobonsitetype_budget").val().replace(/\./gi, "").replace(/,/g, ""));
            $("#vlance_jobbundle_jobonsitetype_budgetMax").val($("#vlance_jobbundle_jobonsitetype_budgetMax").val().replace(/\./gi, "").replace(/,/g, ""));
//            $('#btn-submit-job').prop('disabled', true);
//            addLoadingStatus();
            $('#creditOnsite').modal('hide');
            $("#edit-job-form").submit();
        }
        
        function addLoadingStatus(){
            $("#edit-job-form").find('input[type="button"]').val($("#edit-job-form").find('input[type="button"]').val() + ".");
            setTimeout(addLoadingStatus, 1700);
        }
        
        //validate khi client nhap gia
        function validatePrice(price) {
            var rePrice = /^(0|[1-9][0-9/.]*)$/;
            return rePrice.test(price);
        }     
        
        if(typeof logged != "undefined"){
            if(logged == "true"){
                $('#btn-submit-job').click(function(){
                    if(submit === true){
                        submitJobForm();
                    }
                });
            } else {
                /** START Block register and login **/
                setForm();
                
                /** Reg form in post job **/
                if(typeof reg_form_in_job != 'undefined' && typeof reg_form_in_job_params != 'undefined'){
                    var reg_in_job = new registerInJob(reg_form_in_job, reg_form_in_job_params);
                    reg_in_job.formInitiate(
                            function(){
                                $('#reg_name,#reg_email,#reg_pass,#reg_re_pass').keypress(function(event){
                                    reg_in_job.formSubmitOnEnter(event);
                                });
                            }
                            ,function(){
                                $('#btn-submit-job').click(function(event){
                                    console.log('reg');
                                    if(reg_in_job.getIsActive()){
                                        reg_in_job.formSubmitWithJob();
                                    }
                                });
                            }
                    );
                }

                /** Login form in post job **/
                if(typeof login_form_in_job != 'undefined' && typeof login_form_in_job_params != 'undefined'){
                    var login_in_job = new loginInJob(login_form_in_job, login_form_in_job_params);
                    login_in_job.formInitiate(
                        function(){
                            $('#login_email,#login_pass').keypress(function(event){
                                login_in_job.formSubmitOnEnter(event);
                            });
                        }
                        ,function(){
                            $('#btn-submit-job').click(function(event){
                                console.log('log');
                                if(login_in_job.getIsActive()){
                                    login_in_job.formSubmitWithJob();
                                }
                            });
                        }
                    );
                }

                function registerInJob(reg_form_name, reg_form_params){
                    this.url = "";
                    this.email = "";
                    this.fullname = "";
                    this.password = null;

                    this.getIsActive = function(){
                        return !($(reg_form_name).parent().hasClass('deactive'));
                    };
                    this.setUrl = function(u){
                        this.url = u;
                    };
                    this.setEmail = function(e){
                        this.email = e;
                    };
                    this.setFullname = function(n){
                        this.fullname = n;
                    };
                    this.setPassword = function(p){
                        this.password = p;
                    };

                    this.formSubmitOnEnter = function(event){
                        if(event.which == 13){
                            this.formSubmit();
                        }
                    };

                    this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                        if(reg_form_params.url != null){this.setUrl(reg_form_params.url);}

                        this.formRules();
                        this.decodeDataLoadCookie();
                        callbackWhenPressEnter();
                        callbackWhenSubmitJobForm();
                    };

                    this.formSubmit = function(){
                        that = this;
                        
                        if ($(reg_form_name).valid() === false) {
                            $(reg_form_name).validate().focusInvalid();
                            return false;
                        }

                        that.encodeDataSaveCookie();
                        that.setFullname(document.getElementById('reg_name').value);
                        that.setEmail(document.getElementById('reg_email').value);
                        that.setPassword(document.getElementById('reg_pass').value);

                        var data = {
                            email: that.email,
                            fullname: that.fullname,
                            password: that.password,
                            user: 'client',
                        };
                        $.ajax({
                            url: that.url,
                            type: "POST",
                            data: data,
                        }).done(function(result){
                            if(typeof result.error != 'undefined'){
                                if(result.error == 0){
                                    $('.block-lr').hide();
                                    $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng ký thành công</div></div></div></div>').insertBefore('.container');
                                    location.reload();
                                } else {
                                    that.formRegFailed();
                                }
                            }
                        });
                    };
                    
                    this.formSubmitWithJob = function(){
                        that = this;
                        
                        if ($(reg_form_name).valid() === false) {
                            $(reg_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        if(submit === true){
                            that.setFullname(document.getElementById('reg_name').value);
                            that.setEmail(document.getElementById('reg_email').value);
                            that.setPassword(document.getElementById('reg_pass').value);

                            var data = {
                                email: that.email,
                                fullname: that.fullname,
                                password: that.password,
                                user: 'client',
                            };
                            $.ajax({
                                url: that.url,
                                type: "POST",
                                data: data,
                            }).done(function(result){
                                if(typeof result.error !== 'undefined'){
                                    if(result.error === 0){
                                        $('.block-lr').hide();
                                        submitJobForm();
                                    } else {
                                        that.formRegFailed();
                                    }
                                }
                            });
                        }
                    };

                    this.formRules = function(){
                        $(reg_form_name).validate({
                            rules: {
                                "reg_name": {
                                    required: true,
                                },
                                "reg_email": {
                                    required: true,
                                },
                                "reg_pass": {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32,
                                },
                                "reg_re_pass": {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32,
                                    equalTo : "#reg_pass",
                                },
                                focusInvalid: false,
                            }
                        });
                    };

                    this.formRegFailed = function(){
                        $(reg_form_name + ' .row-fluid .input-email label.error').remove();
                        $(reg_form_name + ' .row-fluid .input-email').append("<label for='reg_email' style='display: block !important;' class='error row-fluid'><b>Email đã được đăng kí</b></label>");
                        $('#reg_email').focus();
                        return false;
                    };

                    this.encodeDataSaveCookie = function(){
                        var obj = new Object();
                        obj.title = $('#vlance_jobbundle_jobonsitetype_title').val();
                        obj.category = $('#vlance_jobbundle_jobonsitetype_category').val();
                        obj.description = $('#vlance_jobbundle_jobonsitetype_description').val();
                        obj.skill = $('input:hidden[name="hiddenTagListA"]').val();
                        obj.min = $('#vlance_jobbundle_jobonsitetype_budget').val();
                        obj.max = $('#vlance_jobbundle_jobonsitetype_budgetMax').val();
                        obj.start = $('#vlance_jobbundle_jobonsitetype_startedAt').val();
                        obj.duration = $('#vlance_jobbundle_jobonsitetype_duration').val();
                        obj.exp = $('#vlance_jobbundle_jobonsitetype_onsiteRequiredExp').val();
                        obj.city = $('#vlance_jobbundle_jobonsitetype_city').val();
                        obj.location = $('#vlance_jobbundle_jobonsitetype_onsiteLocation').val();
                        obj.company = $('#vlance_jobbundle_jobonsitetype_onsiteCompanyName').val();

                        var jsonString = JSON.stringify(obj);
                        var encode = unescape(encodeURIComponent(jsonString));
                        var encodeData = btoa(encode);

                        $.cookie("JO_WL", encodeData, {expires:0.5, path:'/'});
                    };

                    this.decodeDataLoadCookie = function(){
                        that = this;
                        var stringData = $.cookie("JO_WL");
                        if(typeof stringData !== "undefined"){
                            var dataForm = atob(stringData);
                            var decode = decodeURIComponent(escape(dataForm));
                            var parsed = JSON.parse(decode);

                            //set value
                            if(parsed.title !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_title').value = parsed.title;
                            }
                            if(parsed.category !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_category').value = parsed.category;
                            }
                            if(parsed.description !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_description').value = parsed.description;
                            }
                            if(parsed.min !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_budget').value = parsed.min;
                            }
                            if(parsed.max !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_budgetMax').value = parsed.max;
                            }
                            if(parsed.start !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_startedAt').value = parsed.start;
                            }
                            if(parsed.duration !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_duration').value = parsed.duration;
                            }
                            if(parsed.exp !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_onsiteRequiredExp').value = parsed.exp;
                            }
                            if(parsed.city !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_city').value = parsed.city;
                            }
                            if(parsed.location !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_onsiteLocation').value = parsed.location;
                            }
                            if(parsed.company !== null){
                                document.getElementById('vlance_jobbundle_jobonsitetype_onsiteCompanyName').value = parsed.company;
                            }
                            
                            //set skill
                            if(parsed.skill !== null){
                                var arr_skill = parsed.skill.split(',');
                                for(i=0; i< arr_skill.length; i++){
                                    $('.tm-input').tagsManager("pushTag", arr_skill[i]);
                                }
                            }
                            $.removeCookie("JO_WL", {path:'/'});
                        }
                    };
                    /** END cookie */
                }

                function loginInJob(login_form_name, login_form_params){
                    this.url = "";
                    this.email = "";
                    this.password = null;
                    
                    this.getIsActive = function(){
                        return !($(login_form_name).parent().hasClass('deactive'));
                    };
                    this.setUrl = function(u){
                        this.url = u;
                    };
                    this.setEmail = function(e){
                        this.email = e;
                    };
                    this.setPassword = function(p){
                        this.password = p;
                    };

                    this.formLoginFailed = function(){
                        $(login_form_name + ' .row-fluid label.error').remove();
                        $(login_form_name + ' .row-fluid').append("<label style='display: block !important;' class='error row-fluid'><b>Email hoặc mật khẩu không đúng</b></label>");
                        $('#login_email').focus();
                        return false;
                    };

                    this.formSubmitOnEnter = function(event){
                        if(event.which === 13){
                            this.formSubmit();
                        }
                    };

                    this.formInitiate = function(callbackWhenPressEnter, callbackWhenSubmitJobForm){
                        if(login_form_params.url !== null){this.setUrl(login_form_params.url);}
                        this.formRules();
                        this.removeLaberError();
                        callbackWhenPressEnter();
                        callbackWhenSubmitJobForm();
                    };
                    
                    this.formRules = function(){
                        $(login_form_name).validate({
                            rules: {
                                "login_email": {
                                    required: true,
                                },
                                "login_pass": {
                                    required: true,
                                },
                                focusInvalid: false,
                            }
                        });
                    }
                    
                    this.formSubmit = function(){
                        that = this;
                        
                        if ($(login_form_name).valid() === false) {
                            $(login_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        this.setEmail(document.getElementById('login_email').value);
                        this.setPassword(document.getElementById('login_pass').value);

                        var data = {
                            email: that.email,
                            password: that.password
                        };
                        $.ajax({
                            url: that.url,
                            type: "POST",
                            data: data
                        }).done(function(result){
                            if(typeof result.error !== 'undefined'){
                                if(result.error === 0){
                                    $('.block-lr').hide();
                                    $('<div id="messages"><div class="body-messages"><div class="flash-success alert alert-success"><a class="close" data-dismiss="alert">×</a><div>Bạn đăng nhập thành công</div></div></div></div>').insertBefore('.container');
                                    location.reload();
                                } else {
                                    that.formLoginFailed();
                                }
                            }
                        });
                    };
                    
                    this.formSubmitWithJob = function(){
                        that = this;
                        if ($(login_form_name).valid() === false) {
                            $(login_form_name).validate().focusInvalid();
                            return false;
                        }
                        
                        if(submit === true){
                            this.setEmail(document.getElementById('login_email').value);
                            this.setPassword(document.getElementById('login_pass').value);

                            var data = {
                                email: that.email,
                                password: that.password
                            };
                            $.ajax({
                                url: that.url,
                                type: "POST",
                                data: data
                            }).done(function(result){
                                if(typeof result.error !== 'undefined'){
                                    if(result.error === 0){
                                        $('.block-lr').hide();
                                        submitJobForm();
                                    } else {
                                        that.formLoginFailed();
                                    }
                                }
                            });
                        }
                    };

                    this.removeLaberError = function(){
                        $('#login_email').click(function(){
                            $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                        });
                        $('#login_pass').click(function(){
                            $(login_form_name + ' .row-fluid label.error.row-fluid').remove();
                        });
                    };
                }
                /** END Block register and login **/
            }
        }
    }
});

function setForm(){
    $('#new_user').click(function(){
        $('.block-form-reg.deactive').removeClass("deactive");
        $('.block-form-log').addClass("deactive");
        $('#old_user').parent().removeClass('checked');
        $('#new_user').parent().addClass('checked');
    });
    $('#old_user').click(function(){
        $('.block-form-log.deactive').removeClass("deactive");
        $('.block-form-reg').addClass("deactive");
        $('#new_user').parent().removeClass('checked');
        $('#old_user').parent().addClass('checked');
    });
}

function countCredit(){ //Trang new
    var checked = 20;
    $("input[name='onsite_option[]']:checked").each(function ()
    {
        checked += parseInt($(this).attr('data-val'));
    });

    document.getElementById('totalCredit').innerHTML = 'Tổng chi phí: <b>' + checked + ' CREDIT</b>';
    
    if(typeof credit !== 'undefined'){
        if(parseInt(credit) < checked){
            document.getElementById('sys-message').innerHTML = '<span class="text-red"><i>Bạn không đủ Credit</i></span>';
        } else {
            document.getElementById('sys-message').innerHTML = '';
        }
    }
}

function countCreditShow(){ //Trang show
    var checked = 0;
    $("input[name='onsite_option[]']:checked").each(function ()
    {
        checked += parseInt($(this).attr('data-val'));
    });

    document.getElementById('totalCredit').innerHTML = 'Tổng chi phí: <b>' + checked + ' CREDIT</b>';
    
    if(typeof creditShow !== 'undefined'){
        if(parseInt(creditShow) < checked){
            document.getElementById('sys-message').innerHTML = '<span class="text-red"><i>Bạn không đủ Credit</i></span>';
        } else {
            document.getElementById('sys-message').innerHTML = '';
        }
    }
}

function updateCredit(){
    var checked = 0;
    $("input[name='onsite_option[]']:checked").each(function ()
    {
        checked += parseInt($(this).attr('data-val'));
    });

    document.getElementById('total-amount').innerHTML = '' + checked + ' CREDIT';
    
    
    if(checked > 0){
        $('#btn-open-popup').attr('href','#creditOnsite');
    } else {
        $('#btn-open-popup').attr('href','#');
    }
    
    if(typeof creditShow !== 'undefined'){
        if(parseInt(creditShow) < checked){
            $('#systerm-message-id').slideDown();
        } else {
            $('#systerm-message-id').slideUp();
        }
    }
}