jQuery(document).ready(function($) {
    var validate_popup_mesager = function() {
        $('#send-message').validate({
            rules: {
                "vlance_jobbundle_messagetype[content]": {
                    required: true,
                    // If the job is funded, and on WORKING status, Do not check the contact leak
                    contact_leak: ($("#send-message input[type=submit]").attr('data-validate') === "1" ? true : false)
                }
            },
            onfocusout: false,
            onkeyup: false,
            onclick: false
        });
    };
    $("#send-message .input-send-message").click(function(event) {
        if(validate_fileinput(".fileupload_wrapper .new_file_input")==true){
            validate_popup_mesager();
            if($("#send-message").valid()== true){
                $("#send-message").submit();
                $("#send-message .input-send-message").attr("disabled", "disabled");
            }
        }else{
            return false;
        }
    });
    
    $(".require-credit-popup .close-popup").click(function(){
        $('.require-credit-popup').hide();
    });
});