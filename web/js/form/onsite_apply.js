$(document).ready(function() {
    $("#bid_form").validate({
        rules: {
            "input_telephone": {
                "required":  true,
                "number":    true,
                "minlength": 9,
                "maxlength": 11,
            },
            "input_skype": {
                "required": true,
            },   
            "vlance_jobbundle_jobonsitetype[file]": {
                "filesize": true,
                "required": true,
            },
        }
    });

    jQuery.validator.addMethod("filesize", function (val, element) {
        if(element.files[0]){
            var size = element.files[0].size;
            if (size > 10485760)// checks the file more than 10 MB
            {
                return false;
            } else {
                return true;
            }
        } else {
            return true;  
        }
    }, "Dung lượng file ảnh phải nhỏ hơn 10MB");
});