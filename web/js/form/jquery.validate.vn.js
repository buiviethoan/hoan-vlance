/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: vn
 */
jQuery.extend(jQuery.validator.messages, {
        required: "Bắt buộc phải nhập.",
		remote: "Hãy sửa phần này.",
		email: "Hãy điền một địa chỉ mail đúng.",
		url: "Hãy điền một địa chỉ URL đúng.",
		date: "Hãy điền ngày hợp lệ.",
		dateISO: "Hãy điền ngày hợp lệ (ISO).",
		number: "Hãy điền một số hợp lệ.",
		digits: "Hãy nhập các chữ số.",
		creditcard: "Hãy nhập số thẻ hợp lệ.",
		equalTo: "Hãy nhập các giá trị như trước.",
		maxlength: $.validator.format("Hãy nhập không quá {0} ký tự."),
		minlength: $.validator.format("Hãy nhập ít nhất {0} ký tự."),
		rangelength: $.validator.format("Hãy nhập giá trị từ {0} và {1} ký tự."),
		range: $.validator.format("Hãy nhập giá trị từ {0} và {1}."),
//		max: $.validator.format("Hãy nhập giá trị nhỏ hơn hoặc bằng {0}."),
//		min: $.validator.format("Hãy nhập giá trị lớn hơn hoặc bằng {0}.")
		max: $.validator.format("Giá tiền không hợp lệ"),
		min: $.validator.format("Giá tiền không hợp lệ")
});