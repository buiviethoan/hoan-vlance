function funcBidToRate(a, bid, rating){
    if(typeof urlRating != "undefined"){
        $.ajax({
            url: urlRating,
            type: "POST",
            data: {
                'bid'    : bid,
                'rating' : rating,
            },
        }).done(function(result){
            if(result.error == 0){
                if(typeof result.status === "undefined"){
                    $(a).parent().parent().append('<div class="fb-finish">Đã gửi đánh giá</div>');
                }
            } else {
                alert('Đã có lỗi xảy ra. Vui lòng thử lại sau.');
            }
        }).fail(function(result){
        });
    }
}

function funcLabel(pos){
    $('.fb-text-star').hide();
    if(pos === 1){
        $('.fb-text-star.star1').show();
    } else if(pos === 2){
        $('.fb-text-star.star2').show();
    } else if(pos === 3){
        $('.fb-text-star.star3').show();
    } else if(pos === 4){
        $('.fb-text-star.star4').show();
    } else if(pos === 5){
        $('.fb-text-star.star5').show();
    }

    if(pos > 0){
        $('.fb-star-rating.fb-star-on').removeClass('fb-star-on');
        for(var i = 1; i <= pos; i++){
            $('.fb-star-rating.star' + i + '').addClass('fb-star-on');
        }
    }
}

//function funcOut(){
//    for(var $i = 1; $i < 6; $i++){
//        $('.fb-text-star.star' + $i + '').hide();
//    }
//    $('.fb-star-rating').removeClass('fb-star-on');
//}
