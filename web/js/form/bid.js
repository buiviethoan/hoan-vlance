//    tinh toan trong form bid
var bid = [];
var dataRate = [];
$(document).ready(function() {
    // Mixpanel: Tracking activity on the bid form
    /*$('#vlance_jobbundle_bidtype_description').blur(function(){
        if($(this).val()){
            vtrack('Typing bid', {'element_id':$(this).attr('id'), 'valid': ($(this).val().length >= 200 ? "TRUE" : "FALSE")});
        }
    });*/
    
    $(".require-credit-popup .close-popup").click(function(){
        $('.require-credit-popup').hide();
    });
    
    $("#submit_bid_form").validate({
        rules: {
            "vlance_jobbundle_bidtype[description]": {
                required: true,
                minlength: 50,
                maxlength: 3000,
                contact_leak: true
            },
            "vlance_jobbundle_bidtype[introDescription]": {
                required: true,
                minlength: 50,
                maxlength: 3000,
                contact_leak: true
            },
            "vlance_jobbundle_bidtype[amount]": {
                required: true,
                minlength: 5,
                maxlength: 14,
            },
            vlance_jobbundle_bidtype_customers_pay: {
                required: true,
                minlength: 5,
                maxlength: 14,
            },
            agreement: {
                required: true,
            }
        },
        onfocusout: false,
        onkeyup: false,
        onclick: false
    });

    // If there is bid form  
    if(typeof placeholder != "undefined"){
        $("#vlance_jobbundle_bidtype_amount").attr('placeholder', placeholder_pay);
        $("#vlance_jobbundle_bidtype_customers_pay").attr('placeholder', placeholder_get);
        $("#vlance_jobbundle_bidtype_vlance_commision").attr('placeholder', placeholder_commision);

        $('#vlance_jobbundle_bidtype_amount').keyup(function(event) {
            // kiểm tra số nhập vào Chi phí cho dự án
            // kiểm tra có fai nút qua trái phải ko.
            if(event.which == 16 || event.which == 35 || event.which == 36 || event.which == 37 || event.which == 39){
                event.preventDefault();
            }else{
                // lay gia tri xong replace lai thanh so vs add them dau phan nghin
                $(this).val($(this).val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            }

            // end kiểm tra số nhập vào Chi phí cho dự án
            // chuyen lai thanh kieu so
            var price_client = $(this).val().replace(/\./gi, "").replace(/,/g, "");
            if (price_client.match('^(0|[1-9][0-9]*)$')) {
                price_commision = price_client * (vlance_commission/ 100); 
                price_remuneration = price_client * ((100 - vlance_commission) / 100);
                $("#vlance_jobbundle_bidtype_vlance_commision").val(Math.round(price_commision));
                $("#vlance_jobbundle_bidtype_customers_pay").val(Math.round(price_remuneration));
            } else {
                $("#vlance_jobbundle_bidtype_vlance_commision").val("");
                $("#vlance_jobbundle_bidtype_customers_pay").val("");
            }
            // chuyen gia thanh dau phan nghin cua gia tien ban nhan duoc
            $('#vlance_jobbundle_bidtype_vlance_commision').val($('#vlance_jobbundle_bidtype_vlance_commision').val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $('#vlance_jobbundle_bidtype_customers_pay').val($('#vlance_jobbundle_bidtype_customers_pay').val().replace(/[^\d]/g, '').replace(/\./gi, "").replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        }).keyup();
        
        // popver khi hover vào các input
        $('#submit_bid_form .popovers-input').popover({
            html:true,
        });
        
        $('#btn-submit-bid-form').on('click', function(){
            $('#popup-credit-bid').modal('hide');
            checkBidValid();
            formSubmit();
        });
        
        $('#btn-submit').click(function(){
            checkBidValid();
            formSubmit();
        });
    }
    
    
    
    $('.profile-job-right .profile-job-right-bottom div.profile-job').hover(function(){
       $(this).find('.links a').show();
    });
    
    if(typeof job != "undefined" && typeof urlChat != "undefined"){
        $('#btn-chat').click(function(){
            $.ajax({
                url: urlChat,
                type: "POST",
                data: {
                    'jid' : job,
                },
            }).done(function(result){
                if(result.error == 0){
                    $('#fullchat').modal('hide');
                    $("#formmessage form").attr("action", bid[0]); 
                    $("#formmessage h3 span").html(bid[2]); 
                    $("#avai-conve").html('1');
                    $('#formmessage').modal('show');
                    vtrack('show-form-message');
                } else {
                    document.getElementById('sys-message').innerHTML = '<i>Đã có lỗi xảy ra.</i>';
                }
            }).fail(function(result){
            });
        });
    }
});

function checkBidValid(){
    $(".vlance_jobbundle_bidtype_amount .input-append label").remove();  
        if($("#submit_bid_form").valid()== true){
            // chuyen ve kieu so k co dau cham
            var price = $('#vlance_jobbundle_bidtype_amount').val().replace(/\./gi, "").replace(/,/g, "");
            if (price < 50000) {
                $(".vlance_jobbundle_bidtype_amount .bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Chi phí phải lớn hơn <br/> 50.000 VNĐ</label>");
                return false;
            }

            // TODO Validating has to check once only
            if (price < quote_min) {
                $(".vlance_jobbundle_bidtype_amount .bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Chi phí không được<br/> ít hơn 25% ngân sách</label>");
                return false;
            }
            if (price > quote_max) {
                $(".vlance_jobbundle_bidtype_amount .bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Chi phí phải nhỏ hơn <br/> 2.000.000.000 VNĐ</label>");
                return false;
            }

            inputFileLength = $('#submit_bid_form').find('input[type=file]').length;
            if(inputFileLength > 0){
                for(n = 0; n < inputFileLength; n++){
                    if($('#submit_bid_form :input[type=file]').get(n).files[0].size > 10485760){
                        id = $('#submit_bid_form :input[type=file]').get(n).getAttribute('id');
                        id_parent = ($("#" + id).parent().parent().attr('id'));
                        $('#'+ id_parent +' .control-group label.error').remove();
                        $('#'+ id_parent +' .control-group').append('<label for="'+id_parent+'" class="error row-fluid" style="display: block !important">File có độ lớn hơn 10MB.</label>');
                        return false;
                    }
                }
            }

            if (validatePrice(price)) {
                //remove label lỗi ở form
                $(".vlance_jobbundle_bidtype_amount .bidtype_pay .input-append label").remove();

                if($("#vlance_jobbundle_bidtype_files_collection_holder .new_file_input input").length > 0){
                    var attach = '';
                    for(var i=0; i<$("#vlance_jobbundle_bidtype_files_collection_holder .new_file_input").length; i++){
                        attach += $("#vlance_jobbundle_bidtype_files_collection_holder .new_file_input input").eq(i).val().replace(/.*(\/|\\)/, '');
                        if(i < $("#vlance_jobbundle_bidtype_files_collection_holder .new_file_input").length - 1){
                            attach += ", ";
                        }
                    }
                    $("#attachments").html(attach);
                } else {
                    $("#attachments").html(translations['attach_empty']);
                }
            } else {
                $(".vlance_jobbundle_bidtype_amount .bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Giá tiền không hợp lệ</label>");
                return false;
            }
        }

        if(document.getElementById('bidform_telephone').disabled == false){
            /* START Check Telephone */
            var teleNumber = $('#bidform_telephone').val();
            if(teleNumber.length < 10){
                $('.input-prepend.telephone label.error').remove();
                $('.input-prepend.telephone').append('<label for="bidform_telephone" class="error contact" style="display: block !important">Số kí tự ít nhất là 10.</label>');
                return false;
            } else if(teleNumber.length > 11){
                $('.input-prepend.telephone label.error').remove();
                $('.input-prepend.telephone').append('<label for="bidform_telephone" class="error contact" style="display: block !important">Số kí tự nhiều nhất là 11.</label>');
                return false;
            } else {
                $('.input-prepend.telephone label.error').remove();
            }
            if(isNaN(teleNumber)){
                $('.input-prepend.telephone label.error').remove();
                $('.input-prepend.telephone').append('<label for="bidform_telephone" class="error contact" style="display: block !important;">Số điện thoại phải là chữ số.</label>');
                return false;
            } else {
                $('.input-prepend.telephone label.error').remove();
            }
            /* END Check Telephone */
        }

        /* Skype có chứa kí tự trống */
        var skype = $('#bidform_skype').val();
        if(/\s/.test(skype)){
            $('.input-prepend.skype label.error').remove();
            $('.input-prepend.skype').append('<label for="bidform_skype" class="error contact" style="display: block !important;">Skype có chứa kí tự trống.</label>');
            return false;
        }
}

function formSubmit()
{
    $('#vlance_jobbundle_bidtype_amount').val($('#vlance_jobbundle_bidtype_amount').val().replace(/\./gi, "").replace(/,/g, ""));/**khi submit thi gan lai gia tri kieu so cho price**/
    var price = $('#vlance_jobbundle_bidtype_amount').val().replace(/\./gi, "").replace(/,/g, "");

    // so sánh tiền nếu price nhập vào lớn hơn 2 tỷ
    if(price >2000000000){
        $('#form_bid_modal').modal("hide");
        $(".vlance_jobbundle_bidtype_amount bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Giá tiền phải nhỏ hơn 2.000.000.000 VNĐ</label>");
        $('#vlance_jobbundle_bidtype_amount').val($('#vlance_jobbundle_bidtype_amount').val().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        return false;
    }
    // so sánh tiền nếu price nhập vào lớn hơn 2 tỷ
    if(price < 50000){
        $('#form_bid_modal').modal("hide");
        $(".vlance_jobbundle_bidtype_amount bidtype_pay .input-append").append("<label for='vlance_jobbundle_bidtype_amount' style='display: block !important;' class='error row-fluid'>Giá tiền phải lớn hơn 50.000 VNĐ</label>");
        $('#vlance_jobbundle_bidtype_amount').val($('#vlance_jobbundle_bidtype_amount').val().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        return false;
    }
    $('#submit_bid_form').submit();
}

function setBid(a,b,c){
    bid[0] = a;
    bid[1] = b;
    bid[2] = c;
}

function funcBidToRate(a, bid, rating){
    if(typeof urlRating != "undefined"){
        $.ajax({
            url: urlRating,
            type: "POST",
            data: {
                'bid'    : bid,
                'rating' : rating,
            },
        }).done(function(result){
            if(result.error == 0){
                if(typeof result.status === "undefined"){
                    $(a).parent().parent().append('<div class="fb-finish">Đã gửi đánh giá</div>');
                }
                $('span[data-credit="balance"]').html(result.newBalance);
                var num = 5 - result.numRated;
                if(num > 0){
                    $('span[class="count-rated"]').html('' + 5 - result.numRated + ' lần nữa');
                } else {
                    $('.fb-note').remove();
                }
            } else {
                alert('Đã có lỗi xảy ra. Vui lòng thử lại sau.');
            }
        }).fail(function(result){
        });
    }
}

function funcLabel(id, pos){
    if(pos === 1){
        $('.fb-text-star').hide();
        $('.fb-text-star.star1-' + id +'').show();
    } else if(pos === 2){
        $('.fb-text-star').hide();
        $('.fb-text-star.star2-' + id +'').show();
    } else if(pos === 3){
        $('.fb-text-star').hide();
        $('.fb-text-star.star3-' + id +'').show();
    } else if(pos === 4){
        $('.fb-text-star').hide();
        $('.fb-text-star.star4-' + id +'').show();
    } else if(pos === 5){
        $('.fb-text-star').hide();
        $('.fb-text-star.star5-' + id +'').show();
    }
    
    if(pos > 0){
        $('.fb-star-rating.' + id + '').removeClass('fb-star-on');
        for(var i = 1; i <= pos; i++){
            $('.fb-star-rating.star' + i + '.' + id + '').addClass('fb-star-on');
        }
    }
}

//function funcOut(id){
//    for(var $i = 1; $i < 6; $i++){
//        $('.fb-text-star.star' +  + '-' + id +'').hide();
//    }
//    $('.fb-star-rating.' + id + '').removeClass('fb-star-on');
//}
