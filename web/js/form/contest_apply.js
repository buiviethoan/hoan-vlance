var arr_file_upload = ['image/gif','image/png','image/jpeg'];
var $arr = []; //lưu bid được chọn


var temp = "";

/* Config Lightbox */
if(typeof LBox != "undefined"){
    lightbox.option({
        'resizeDuration':                 100,
        'wrapAround':                     true,
        'alwaysShowNavOnTouchDevices':    true,
        'positionFromTop':                0
    });
}

$(document).ready(function(){
    $('#contest-apply-form').validate({
        rules: {
            "vlance_jobbundle_contestapplytype[files][0][file]": {
                required: true,
                filetype: true,
                filesize: true,
            },
        }
    });
    
    //Check type
    jQuery.validator.addMethod("filetype", function(val, element){
        if(element.files[0]){
            var type = element.files[0].type;
            if($.inArray(type, arr_file_upload) == -1){
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }, "File phải đúng định dạng gif, png, jpg");
    
    //Check size
    jQuery.validator.addMethod("filesize", function (val, element) {
        if(element.files[0]){
            var size = element.files[0].size;
            if (size > 10485760)// checks the file more than 10 MB
            {
                return false;
            } else {
                return true;
            }
        } else {
            return true;  
        }
    }, "Kích thước file phải nhỏ hơn 10MB ");
    
    //Check dimensions
    var submit = false;
    $('#vlance_jobbundle_contestapplytype_files_0_file').change(function(){
        if(this.files[0]){
            URL = window.URL || window.webkitURL;
            img = new Image();
            img.src = window.URL.createObjectURL(this.files[0]);

            img.onload = function() {
                window.URL.revokeObjectURL(img.src);

                if(img.naturalWidth < 500 || img.naturalHeight < 400) {
                    submit = false;
                    $('#vlance_jobbundle_contestapplytype_files_0 .control-group label.upload_error').remove();
                    $('#vlance_jobbundle_contestapplytype_files_0 .control-group').append("<label for='vlance_jobbundle_contestapplytype_files_0_file' class='upload_error' style='display: block !important;position: relative;top: -5px;font-weight: normal;line-height: 21px;color: red;margin-top: 10px;'>Kích thước ảnh nhỏ nhất là 500 x 400px</label>");
                } else {
                    submit = true;
                    $('#vlance_jobbundle_contestapplytype_files_0 .control-group label.upload_error').remove();
                }
            };
        }
    });
    
    //Chào giá của freelancer
    $('#btn-contest-apply').click(function(){
        if(submit == false){
            $('#vlance_jobbundle_contestapplytype_files_0_file').focus();
            return false;
        } else {
	    if( $("#popup-condition").value == 1){
		console.log("Cho mua thêm lượt");
		return false;
	    }else if( $("#popup-condition").value == 2){
		console.log("Không đủ credit");
		return false;
	    }else {
		$('#contest-apply-form').submit();
	    }
        }
    });
    
    //Xác nhận chọn bài dự thi
    $('#btn-confirm-contest-apply').click(function(){
        if(typeof url != "undefined" && typeof job != "undefined"){
            $('input[type="checkbox"]').prop('disabled', true);
            $('#btn-confirm-contest-apply').prop('disabled', true);
            $('#btn-confirm-contest-apply').val("Đang xác nhận");
            
            var data = $arr;
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    job: job,
                    bid: $arr,
                },
            }).done(function(result){
                if(typeof result.error != 'undefined'){
                    if(result.error == 0){
                        showSuccessMessage();
                    } else {
                        showErrorMessage();
                    }
                }
            });   
        }
    });
    
    //reload page
    $('#popup-message-success').on('hidden', function () {
        location.reload();
    });
});

//Open popup picture
function onClick(a, w, h, u){
    var modalImg = document.getElementById("imgModal");
    $('#openBigSize').modal();
    document.getElementById("titleModal").innerHTML = a.alt;
    modalImg.src = u;
    modalImg.alt = a.alt;
    var width = document.getElementById(a.id).clientWidth;
    var height = document.getElementById(a.id).clientHeight;
    document.getElementById("dimensions_image").innerHTML = "Ảnh có kích thước "+ w+'x'+h+'px.';
}

//Checkbox action
function onCheck(a){
    //Lấy giá trị của các bid được đánh dấu
    if(a.checked){
        $arr.push(document.getElementById(a.id).value);
    } else {
        removeValue($arr ,document.getElementById(a.id).value);
    }
    
    //Chỉ được chọn nhiều nhất 4 bài
    if($('input[type="checkbox"]:checked').length === 4){
        $('input[type="checkbox"]').prop('disabled', true);
        $('input[type="checkbox"]:checked').prop('disabled', false);
    } else if($('input[type="checkbox"]:checked').length < 4) {
        $('input[type="checkbox"]').prop('disabled', false);
    }
    
    //Show or hide block xác nhận và hiển thị số bài đã chọn
    if($('input[type="checkbox"]:checked').length > 0){
        $('.confirm-contest-apply.hide').slideDown();
        document.getElementById("num-confirm").innerHTML = $('input[type="checkbox"]:checked').length;
    } else {
        $('.confirm-contest-apply').slideUp();
    }
}

//Remove value
function removeValue(a, v){
    for($i = 0; $i < a.length; $i++){
        if(a[$i] === v){
            a.splice($i, 1);
            $i--;
        }
    }
    return a;
}

//Thông báo thành công hoặc lỗi xảy ra khi xác nhận bid
function showSuccessMessage(){
    document.getElementById('block-confirm-apply').innerHTML = '<div class="row-fluid"><p class="num-apply success-confirm-apply" style="text-align: center;">Xác nhận thành công</p></div>';
    location.reload();
}

function showErrorMessage(){
    document.getElementById('block-confirm-apply').innerHTML = '<div class="row-fluid"><p class="num-apply confirm-apply" style="text-align: center;">Đã có lỗi xảy ra. Vui lòng thử lại sau</p></div>';
}

//Trao thưởng
function clickAwarded(a, id){
    $('#popup-message').modal();
    temp = id;
}

function clickConfirm(){
    if(typeof award != "undefined" && typeof job != "undefined"){
        $('#popup-message').addClass('loading');
        $.ajax({
            url: award,
            type: "POST",
            data: {
                job: job,
                bid: temp,
            },
        }).done(function(result){
            if(typeof result.error != 'undefined'){
                if(result.error == 0){
                    $('#popup-message').removeClass('loading');
                    $('#popup-message').modal('hide', $('#popup-message-success').modal());
                } else {
                    $('#popup-message').removeClass('loading');
                    $('#popup-message').modal('hide', $('#popup-message-error').modal());
                }
            }
        });
    }
}
