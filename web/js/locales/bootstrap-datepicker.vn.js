/**
 * French translation for bootstrap-datepicker
 * Nico Mollet <nico.mollet@gmail.com>
 */
;(function($){
	$.fn.datepicker.dates['vn'] = {
		days: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bẩy", "Chủ nhật"],
		daysShort: ["Ch", "T.Hai", "T.Ba", "T.Tư", "T.Năm", "T.Sáu", "T.Bẩy", "Ch"],
		daysMin: ["Ch", "H", "B", "T", "N", "S", "B", "Ch"],
		months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
		monthsShort: ["T.1", "T.2", "T.3", "T.4", "T.5", "T.6", "T.7", "T.8", "T.9", "T.10", "T.11", "T.12"],
                today: 'Hôm nay'
	};
}(jQuery));
