/* 
 * Version 1.0
 * - detect number in digt or text
 * - dectet sensitive words
 * 
 * Version 2.0
 * - detect email + tricked email
 * 
 * Version 2.1
 * - detect specific group of keywords
 */

jQuery.validator.addMethod("contact_leak", function (text, element) {
    // contain specific forbidden keywords, following by ":" or " :"
    var forbidden =["liên hệ", "lien he", 
                    "tel", "mobile", "sđt", "sdt", "đt", "dt",
                    "skype", "skky", "skype id", "yahoo", "viber", "messenger",
                    "mail", "email", "emmail", "gmail",
                    "facebook", "fb"];
    for(var i=0; i<forbidden.length; i++){
        if(text.toLowerCase().indexOf(forbidden[i] + ":") > -1){ // ex: "skype:"
            return false;
        }
        if(text.toLowerCase().indexOf(forbidden[i] + " :") > -1){ // ex: "skype :"
            return false;
        }
    }
    
    // contain specific forbidden group of keywords
    var forbidden_group =  ["skype mình", "skype của", "skype là", "skype chị", "skype em", "skype anh",
                            "skype minh", "skype cua", "skype la", "skype chi",           
                            "email mình", "email của", "email là", "email chị", "email em", "skype anh",
                            "email minh", "email cua", "email la", "email chi",
                            "s ky pi", "tai khoan sk"];
    for(var i=0; i<forbidden_group.length; i++){
        if(text.toLowerCase().indexOf(forbidden_group[i]) > -1){
            return false;
        }
    }
    
    // contain telephone number in number
    // ex: 0987654321 || 01234567890
    var pattern1 = /0(([-\/\.\,;\s\+\*\#_]{0,2}1)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){9}|([-\/\.\,;\s\+\*\#_]{0,2}9)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){8})/g;
    // ex: 84987654321 || 841234567890
    var pattern2 = /84(([-\/\.\,;\s\+\*\#_]{0,2}1)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){9}|([-\/\.\,;\s\+\*\#_]{0,2}9)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){8})/g;
    // ex: +84987654321 || +841234567890
    var pattern3 = /\+84(([-\/\.\,;\s\+\*\#_]{0,2}1)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){9}|([-\/\.\,;\s\+\*\#_]{0,2}9)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){8})/g;
    // ex: 0084987654321 || 00841234567890
    var pattern4 = /0084(([-\/\.\,;\s\+\*\#_]{0,2}1)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){9}|([-\/\.\,;\s\+\*\#_]{0,2}9)([-\/\.\,;\s\+\*\#_]{0,2}[0-9]){8})/g;
    if(text.match(pattern1) || text.match(pattern2) || text.match(pattern3) || text.match(pattern4)){
        return false;
    }
    
    // contain telephone number in text
    var pattern5 = /(không|khong)(([-\/\.\,;\s\+\*\#_]{0,2}(một|mot))([-\/\.\,;\s\+\*\#_]{0,2}(không|một|hai|ba|bốn|năm|sáu|bảy|bẩy|tám|chín|khong|mot|hai|bon|nam|sau|bay|tam|chin)){9}|([-\/\.\,;\s\+\*\#_]{0,2}(chín|chin))([-\/\.\,;\s\+\*\#_]{0,2}(không|một|hai|ba|bốn|năm|sáu|bảy|bẩy|tám|chín|khong|mot|hai|bon|nam|sau|bay|tam|chin)){8})/g;
    if(text.toLowerCase().match(pattern5)){
        return false;
    }
    
    // contain email address
    // ex: abc@abc.xxx
    var parttern_email1 = /[\w\d._%+-]+@[\w\d.-]+\.[\w]{2,6}/g;
    // ex: xx @ abc . com || xx [at] xxx . com
    var parttern_email2 = /(@|at\s|\[at\]|\(at\)|\[@\])\s{0,3}[\w\d.-]+\s{0,3}\.\s{0,3}(com|net|vn|org)/g;
    if(text.toLowerCase().match(parttern_email1) || text.toLowerCase().match(parttern_email2)){
        return false;
    }
    
    return true;
}, "Vui lòng không chủ động tiết lộ hoặc đề nghị người nhận cung cấp các thông tin liên lạc như email, số điện thoại, skype, viber, facebook v.v.. trong nội dung thông điệp trên. Bạn có thể sẽ bị khóa tài khoản vĩnh viễn, nếu vi phạm quy định của vLance.");