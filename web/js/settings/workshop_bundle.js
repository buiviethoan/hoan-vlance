jQuery(document).ready(function($) {
    $('#setting__workroom__popup__reminder_finish_project__hide').click(function(e){
        e.preventDefault();
        //console.log("hello");
        //console.log($(this).data('url'));
        
        $(this).closest('.workroom-remind').hide(200);
        if(typeof $(this).data('url') != 'undefined'){
            $.ajax({
                url: $(this).data('url'),
                type: "POST",
                data: { 
                    setting: {
                        property_name : "setting__workroom__popup__reminder_finish_project__hide",
                        value         : 1
                    }
                }
            }).done(function(response){
                //console.log(response);
            });
        }
    })
});