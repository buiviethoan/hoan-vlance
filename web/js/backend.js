jQuery(document).ready(function(){
    //Click on filter label to hide the filter block
    jQuery('.filters-list legend').click(function() {
        jQuery('.filters-list fieldset').toggleClass('hide');
        jQuery('.filters-list .form-group').toggleClass('hide');
    });
    
    //Filter button by tab
    jQuery(".btn-default.active").each(function(idx, element) {
        if (idx == 0) {
            var icon = jQuery(element).attr('class');
            icon = icon.replace('btn btn-default job-tab ', "");
            icon = icon.replace(" active", "");
            //display only the button action for this tab - hide all other
            jQuery(".btn-group .job-action").hide();
            jQuery(".btn-group .job-action" + "." + icon).show();
        }
    });
});