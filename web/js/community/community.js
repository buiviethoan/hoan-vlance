$(document).ready(function() {
    /* Trang đăng bài viết */
    if($("body").hasClass("community_post_new") || $("body").hasClass("community_post_edit")){
        CKEDITOR.replace( 'vlance_cmsbundle_communityposttype_description', {
            //filebrowserBrowseUrl : '/browser/browse.php', 
            filebrowserUploadUrl : upload_image,
            removeDialogTabs : 'image:advanced',
        });
        
        $('#community-post-form .popovers-input').popover();
        $('#community-post-form').validate({
            rules: {
                "vlance_cmsbundle_communityposttype[title]": {
                    required: true,
                },
                "hiddenTagListA": {
                    required: true,
                },
                "vlance_cmsbundle_communityposttype[description]": {
                    required: true,
                    contact_leak: true,
                },
            }
        });
        
        if($("body").hasClass("community_post_edit")){
            var data = $('#vlance_cmsbundle_communityposttype_description').val();
            CKEDITOR.instances['vlance_cmsbundle_communityposttype_description'].setData(data);
        }
    }
    
    /* Trang xem bài viết */
    if($("body").hasClass("community_post_show")){
        if(typeof pid != "undefined" && typeof url != "undefined"){
            $('#icon-btn-like').click(function(){
                var num = parseInt($('#show-likes').text());
                
                if($('#icon-like').hasClass('liked')){
                    $('#icon-like').removeClass('liked');
                    $('#show-likes').removeClass('liked');
                    document.getElementById("show-likes").innerHTML = num - 1;;
                } else {
                    $('#icon-like').addClass('liked');
                    $('#show-likes').addClass('liked');
                    document.getElementById("show-likes").innerHTML = num + 1;;
                }
                
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        pid: pid,
                    }
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                        } else if(result.error == 103) {
                            $('#icon-like').removeClass('liked');
                            $('#show-likes').removeClass('liked');
                            document.getElementById("show-likes").innerHTML = result.likes;
                        }
                    }
                });
            });
        }
    }
});
