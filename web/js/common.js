/* Function to send tracking data to Mixpanel and Google Analytics */
function vtrack(event, properties, category, withmixpanel){
    category = (typeof category !== 'undefined' && category !== null) ? category : 'general';
    withmixpanel = (typeof withmixpanel === 'undefined') ? true : withmixpanel;
    if(withmixpanel){
        mixpanel.track(event, properties);
    }
    ga('send', 'event', category, event, properties);
}

function getOperator(number){
    var pattern_viettel     = /^(086|096|097|098|0162|0163|0164|0165|0166|0167|0168|0169)/;
    var pattern_mobifone    = /^(089|090|093|0120|0121|0122|0126|0128)/;
    var pattern_vinaphone   = /^(088|091|094|0123|0124|0125|0127|0129)/;
    
    if(number.match(pattern_viettel)){
        return 1; // OnePay::OPERATOR_VIETTEL
    } else if(number.match(pattern_mobifone)){
        return 2; // OnePay::OPERATOR_MOBIFONE
    } else if(number.match(pattern_vinaphone)){
        return 3; // OnePay::OPERATOR_VINAPHONE
    } else {
        return 0; // OnePay::OPERATOR_OTHER
    }
        
}
    
/* Run this code when the page is load */
jQuery(document).ready(function($){   
    $('.hidejs').show();
    $('.vlance-popover').popover();

    /* Put automatic tracking code for display event of something
     * Sending event when some elements are detected
     * Example of data source: data-display-track="show-contact-client-button"
     */
    var tracking_display = [
        //"show-contact-client-button",         // Temporarily turn off
        "create-job-require-escrow",
        //"show-feature-freelancer-block",      // Temporarily turn off
        "show-feature-job-block",
        "show-upfront-escrow-popup",
        //"show-feature-bid-block",             // Job view
        "show-block-banned-account-when-bid",   // Job view - Bid form
        //"show-block-missing-info-when-bid",   // Job view - Bid form // Temporarily turn off
        "show-require-credit-bid-not-enough",   // Job view - Bid form
        "show-require-credit-bid-enough",       // Job view - Bid form
        //"show-bid-form-bidable",                // Job view - Bid form
        //"show-bid-form-not-bidable",            // Job view - Bid form
        //"show-block-service-when-bid",          // Job view - Bid form
        "show-buy-credit-form",                 // Credit balance page
        "show-promotion-money-lover",
        "show-money-lover-gift-guide",
        "show-require-credit-respond-client-not-enough",
        "show-require-credit-respond-client-enough",
        "show-verify-id-form",
        "show-verify-tax-id-form",
        "show-contest-form",
        "contest-view-apply-form",
        "view-job-credit-bid",
    ];
    for (var i = 0; i < tracking_display.length; i++){
        if($("*[data-display-track=" + tracking_display[i] + "]").length > 0){
            vtrack(tracking_display[i], null, 'display-track', true);
        }
    }
    /* END Put automatic tracking code for display event of something */
});