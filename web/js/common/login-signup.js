$(document).ready(function(){
    if(!$("body").hasClass("community_post_new") && !$("body").hasClass("community_post") && !$("body").hasClass("community_post_show")){
        //Select form
        selectForm();

        if(typeof login_form != 'undefined' && typeof login_form_params != 'undefined'){
            var login = new loginForm(login_form, login_form_params);
            login.formInitiate(
                function(){
                    $('#log-username,#log-password').keypress(function(event){
                        login.formSubmitOnEnter(event);
                    });
                },
                function(){
                    $('#btn-submit-login').click(function(event){
                        login.formSubmit();
                    });
                }
            );
        }

        if(typeof reg_form != 'undefined' && typeof reg_form_params != 'undefined'){
            var register = new registerForm(reg_form, reg_form_params);
            register.formInitiate(
                function(){
                    $('#reg-fullname,#reg-username,#reg-password,#reg-re-password').keypress(function(event){
                        register.formSubmitOnEnter(event);
                    });
                },
                function(){
                    $('#btn-submit-register').click(function(event){
                        register.formSubmit();
                    });
                }
            );
        }

        //Login
        function loginForm(login_form_name, login_form_params){
            this.url = "";
            this.email = "";
            this.password = null;

            this.setUrl = function(u){
                this.url = u;
            };
            this.setEmail = function(e){
                this.email = e;
            };
            this.setPassword = function(p){
                this.password = p;
            };

            this.formLoginFailed = function(){
                $('<label class="error log"><b>Email hoặc mật khẩu không đúng</b></label>').insertBefore('.checkbox');
                $('#btn-submit-login').prop('disabled', false);
                $('#login-username').focus();
                return false;
            };

            this.formInitiate = function(callbackWhenPressEnter, callbackSubmit){
                if(login_form_params.url != null){this.setUrl(login_form_params.url);}
                this.removeLaberError();
                callbackWhenPressEnter();
                callbackSubmit();
            };
            
            this.formSubmitOnEnter = function(event){
                if(event.which == 13){
                    this.formSubmit();
                }
            };
            
            this.formSubmit = function(){
                that = this;
                if ($(login_form_name).valid() === false) {
                    $(login_form_name).validate().focusInvalid();
                    return false;
                }

                $('#btn-submit-login').prop('disabled', true);
                this.setEmail(document.getElementById('log-username').value);
                this.setPassword(document.getElementById('log-password').value);

                var data = {
                    email: that.email,
                    password: that.password,
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.login-box').slideUp(function(){ location.reload(); });
                        } else {
                            that.formLoginFailed();
                        }
                    }
                });
            };

            this.removeLaberError = function(){
                $(login_form_name).click(function(){
                    $(login_form_name + ' label.error.log').remove();
                });
            };
        }

        //Register
        function registerForm(reg_form_name, reg_form_params){
            this.url = "";
            this.email = "";
            this.fullname = "";
            this.password = null;
            this.type = "";

            this.setUrl = function(u){
                this.url = u;
            };
            this.setEmail = function(e){
                this.email = e;
            };
            this.setFullname = function(n){
                this.fullname = n;
            };
            this.setPassword = function(p){
                this.password = p;
            };
            this.setUser = function(t){
                this.user = t;
            };

            this.formRules = function(){
                $(reg_form_name).validate({
                    rules: {
                        "reg_password": {
                            minlength: 6,
                            maxlength: 32,
                        },
                        "reg_re_password": {
                            minlength: 6,
                            maxlength: 32,
                            equalTo : "#reg-password",
                        },
                    }
                });
            };

            this.formInitiate = function(callbackWhenPressEnter, callbackSubmit){
                if(reg_form_params.url != null){this.setUrl(reg_form_params.url);}

                this.formRules();
                callbackWhenPressEnter();
                callbackSubmit();
            };

            this.formSubmitOnEnter = function(event){
                if(event.which == 13){
                    this.formSubmit();
                }
            };

            this.formSubmit = function(){
                that = this;

                if ($(reg_form_name).valid() === false) {
                    $(reg_form_name).validate().focusInvalid();
                    return false;
                }

                $('#btn-submit-register').prop('disabled', true);
                that.setFullname(document.getElementById('reg-fullname').value);
                that.setEmail(document.getElementById('reg-username').value);
                that.setPassword(document.getElementById('reg-password').value);
                that.setUser(jQuery('input:radio[name="type"]:checked').val());

                var data = {
                    email:      that.email,
                    fullname:   that.fullname,
                    password:   that.password,
                    user:       that.user,
                };
                $.ajax({
                    url: that.url,
                    type: "POST",
                    data: data,
                }).done(function(result){
                    if(typeof result.error != 'undefined'){
                        if(result.error == 0){
                            $('.signup-box').slideUp(function(){location.reload();});
                        } else {
                            that.formRegFailed();
                        }
                    }
                });
            };

            this.formRegFailed = function(){
                $("<label for='reg-username' style='display: block !important;' class='error row-fluid'><b>Email đã được đăng kí</b></label>").insertAfter('#reg-username');
                $('#btn-submit-register').prop('disabled', false);
                $('#reg-username').focus();
                return false;
            };
        }
    }
});

function selectForm(){
    $("#btn-close-log").click(function(){
        $('.login-box').slideUp();
    });
    $("#btn-close-signup").click(function(){
        $('.signup-box').slideUp();
    });
    $("#btn-login").click(function(){
        $('.signup-box').slideUp();
        $('.login-box').slideDown();
    });
    $("#btn-register").click(function(){
        $('.login-box').slideUp();
        $('.signup-box').slideDown();
    });
}