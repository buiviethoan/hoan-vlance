jQuery(document).ready(function($){
    if(typeof url_account_json_typeahead != 'undefined' && typeof url_json_get_account != 'undefined'){
        var accounts = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('email'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: url_account_json_typeahead + '%QUERY',
                wildcard: '%QUERY'
            }
        });
        
        $('#typeahead_account .typeahead').typeahead(
            {
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name:       'best-pictures',
                display:    'email',
                limit:      10, // must be less than the array return
                source:     accounts,
                templates: {
                    empty: [
                        '<div class="empty-message">',
                          'Không có account nào phù hợp',
                        '</div>'
                        ].join('\n'),
                    suggestion: Handlebars.compile('<div>#{{id}} - {{email}}</div>')
                }
            }
        );
        $('#typeahead_account .typeahead').bind('typeahead:select', function(ev, suggestion) {
            $("#typeahead_account .bind-typeahead").val(suggestion.id);
        });
    
        if($("#typeahead_account .bind-typeahead").val()){
            $.ajax({
                url: url_json_get_account + $("#typeahead_account .bind-typeahead").val(),
                type: 'GET'
            }).success(function(res){
                if(res.error == 0){
                    $('#typeahead_account .typeahead').val(res.data.email);
                }
            });
        }
    }
});
