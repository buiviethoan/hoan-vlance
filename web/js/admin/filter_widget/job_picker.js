jQuery(document).ready(function($){
    if(typeof url_job_json_typeahead != 'undefined' && typeof url_json_get_job != 'undefined'){
        var jobs = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: url_job_json_typeahead + '%QUERY',
                wildcard: '%QUERY'
            }
        });

        $('#typeahead_job .typeahead').typeahead(
            {
                hint: false,
                highlight: true,
                minLength: 1
            },
            {
                name:       'best-pictures',
                display:    'title',
                limit:      10, // must be less than the array return
                source:     jobs,
                templates: {
                    empty: [
                        '<div class="empty-message">',
                          'Không có Job nào phù hợp',
                        '</div>'
                        ].join('\n'),
                    suggestion: Handlebars.compile('<div>#{{id}} - {{title}}</div>')
                }
            }
        );
        $('#typeahead_job .typeahead').bind('typeahead:select', function(ev, suggestion) {
            $("#typeahead_job .bind-typeahead").val(suggestion.id);
        });
    
        if($("#typeahead_job .bind-typeahead").val()){
            $.ajax({
                url: url_json_get_job + $("#typeahead_job .bind-typeahead").val(),
                type: 'GET'
            }).success(function(res){
                if(res.error == 0){
                    $('#typeahead_job .typeahead').val(res.data.title);
                }
            });
        }
    }
});
