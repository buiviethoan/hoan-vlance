-- phpMyAdmin SQL Dump
-- version 4.0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 18, 2013 at 12:05 AM
-- Server version: 5.1.69
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dev_vlance`
--

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `title`, `description`, `path`, `cat_order`) VALUES
(1, NULL, 'Các công việc IT và lập trình', 'Các công việc IT và lập trình', '1/', 0),
(2, 1, 'Lập trình web', 'Lập trình web', '1/2', 0),
(3, 1, 'Ứng dụng di động', 'Ứng dụng di động', '1/3', 0),
(4, NULL, 'Các công việc thiết kế', 'Các công việc thiết kế', '4/', 0),
(5, NULL, 'Các công việc viết lách', 'Các công việc viết lách', '5/', 0),
(6, 5, 'Viết báo & tạp chí', 'Viết báo & tạp chí', '5/6', 0),
(7, 4, 'Thiết kế Logo', 'Thiết kế Logo', '4/7', 0),
(8, 1, 'Các công việc lập trình khác', 'Các công việc lập trình khác', '1/8', 0),
(9, 1, 'Lập trình phần mềm', 'Lập trình phần mềm', '1/9', 0),
(10, 1, 'Tối ưu cho công cụ tìm kiếm - SEO', 'Tối ưu cho công cụ tìm kiếm - SEO', '1/10', 0),
(11, 1, 'Mạng & Quản trị hệ thống', 'Mạng & Quản trị hệ thống', '1/11', 0),
(13, 4, 'Thiết kế đồ hoạ', 'Thiết kế đồ hoạ', '4/13', 0),
(14, 4, 'Thiết kế Website', 'Thiết kế Website', '4/14', 0),
(15, 4, 'Banner quảng cáo', 'Banner quảng cáo', '4/15', 0),
(16, 4, 'Nhãn hiệu và bao bì', 'Nhãn hiệu và bao bì', '4/16', 0),
(17, 4, 'Làm video clip', 'Làm video clip', '4/17', 0),
(18, 4, 'Ảnh và chỉnh sửa ảnh', 'Ảnh và chỉnh sửa ảnh', '4/18', 0),
(19, 4, 'Kiến trúc và nội thất', 'Kiến trúc và nội thất', '4/19', 0),
(20, 4, 'Các việc thiết kế khác', 'Các việc thiết kế khác', '4/20', 0),
(21, 5, 'Quản lý blog & fanpage', 'Quản lý blog & fanpage', '5/21', 0),
(22, 5, 'Dịch thuật', 'Dịch thuật', '5/22', 0),
(23, 5, 'Viết sách', 'Viết sách', '5/23', 0),
(24, 5, 'Các công việc viết lách khác', 'Các công việc viết lách khác', '5/24', 0),
(25, NULL, 'Các công việc marketing & kinh doanh', 'Các công việc marketing & kinh doanh', '25/', 0),
(26, 25, 'Quảng cáo qua công cụ tìm kiếm – SEM', 'Quảng cáo qua công cụ tìm kiếm – SEM', '25/26', 0),
(27, 25, 'Quảng bá sản phẩm', 'Quảng bá sản phẩm', '25/27', 0),
(28, 25, 'Giới thiệu bán hàng', 'Giới thiệu bán hàng', '25/28', 0),
(29, 25, 'Nghiên cứu, khảo sát thị trường', 'Nghiên cứu, khảo sát thị trường', '25/29', 0),
(30, 25, 'Các công việc KD và marketing khác', 'Các công việc KD và marketing khác', '25/30', 0),
(31, NULL, 'Các công việc hành chính', 'Các công việc hành chính', '31/', 0),
(32, 31, 'Nhập dữ liệu', 'Nhập dữ liệu', '31/32', 0),
(33, 31, 'Tổ chức sự kiện', 'Tổ chức sự kiện', '31/33', 0),
(34, 31, 'Kế toán', 'Kế toán', '31/34', 0),
(35, 31, 'Tư vấn luật', 'Tư vấn luật', '31/35', 0),
(36, 31, 'Các công việc hành chính khác', 'Các công việc hành chính khác', '31/36', 0),
(37, NULL, 'Các công việc khác', 'Các công việc khác', '37/', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
