UPDATE account 
    SET `username` = CONCAT(`email`, '.dev.vlance.vn'),
    `username_canonical` = CONCAT(`email`, '.dev.vlance.vn'),
    `email` = CONCAT(`email`, '.dev.vlance.vn'),
    `email_canonical` = CONCAT(`email`, '.dev.vlance.vn') 
    WHERE `id` NOT IN (1,4,1001,1005,1006,1007,1008,1009,1010,1011,1012)
