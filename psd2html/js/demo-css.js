/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/** Page list **/
const INDEX_PAGE = 'index.html';
const SIGNUP_COMMON_PAGE = 'A2a-signup-col1.html';
const SIGNUP_FREELANCER_PAGE = 'A2b-signup-freelancer-col1.html';
const SIGNUP_CLIENT_PAGE = 'A2b-signup-client-col1.html';
const SIGNUP_CONFIRM_PAGE = 'A2d-signup-confirm-col1.html'
const LOGIN_PAGE = 'A3-login-col1.html';

$(document).ready(function(){
	/** Homepage **/
	// To Index page
	$('.navbar .logo a').attr('href', INDEX_PAGE);
	$('.bottom-logo .vlance-logo a').attr('href', INDEX_PAGE);
	// To login page
	$('.navbar .btn-connect a:first').attr('href', LOGIN_PAGE);
	$('.signup-common-page .signup-box > p > a').attr('href', LOGIN_PAGE);
	// To signup common page
	$('.navbar .btn-connect a:last').attr('href', SIGNUP_COMMON_PAGE);
	
	/** Sign-up common page **/
	// To Signup freelancer page
	$('form#signup-form').attr('action', SIGNUP_FREELANCER_PAGE);
	$('#signup-form #rad-freelancer').click(function(){$('form#signup-form').attr('action', SIGNUP_FREELANCER_PAGE);});
	// To Signup client page
	$('#signup-form #rad-client').click(function(){$('form#signup-form').attr('action', SIGNUP_CLIENT_PAGE);});

	/** Sign-up freelancer page **/
	// To Signup confirm page
	$('form#signup-freelancer-form').attr('action', SIGNUP_CONFIRM_PAGE);
	
	/** Sign-up Client page **/
	// To Signup confirm page
	$('form#signup-client-form').attr('action', SIGNUP_CONFIRM_PAGE);

});


